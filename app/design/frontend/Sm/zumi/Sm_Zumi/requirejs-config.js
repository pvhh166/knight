var config = {
	map: {
		'*': {
			jquerybootstrap: "Sm_Zumi/js/bootstrap/bootstrap.min",
			owlcarousel: "Sm_Zumi/js/owl.carousel",
			jqueryfancyboxpack: "Sm_Zumi/js/jquery.fancybox.pack",

            fancybuttons: "Sm_Zumi/js/helpers/jquery.fancybox-buttons",
            fancymedia: "Sm_Zumi/js/helpers/jquery.fancybox-media",
            fancythumbs: "Sm_Zumi/js/helpers/jquery.fancybox-thumbs",

			jqueryunveil: "Sm_Zumi/js/jquery.unveil",
			yttheme: "Sm_Zumi/js/yttheme"			
		}
	}
};