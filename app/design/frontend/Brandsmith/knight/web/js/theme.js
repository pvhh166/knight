requirejs(['jquery', 'underscore', 'slick', 'bootstrap'],function($, _, slick, bootstrap){
    'use strict';

    var knight  = {};

    var winWidth = $(window).width();

    (function ($, undefined) {

        var customUI = knight.customUI = function (opts) {
            customUI.prototype._singletonInstance = this;
            opts                                  = opts || {};
            var defaultOpts                       = {
                'element': '.elementClassOrdId'
            };
            this.opts                             = $.extend(defaultOpts, opts);
        };

        customUI.prototype.init = function () {
            //this.unsliderOld();
            this.resourceClick();
            this.mainNavigation();
            this.oldZumijQuery();
            this.filterCatalogView();
            this.setHeightCategorythum();
        };

        customUI.prototype.resourceClick = function () {
            $(document).on("click",".resourcesButton",function() {
                if($(this).parent().find('.list-files').is(":visible")) {
                    $(this).parent().find('.list-files').slideUp();
                }else {
                    $(this).parent().find('.list-files').slideDown();
                }
            });
        };

        customUI.prototype.filterCatalogView = function () {
            $(document).on("click","#shopbyfilter",function() {
                if($('#filter-nav-content').is(":visible")) {
                    $('#icon-filter').html('+');
                }else {
                    $('#icon-filter').html('-');
                }
                $('#filter-nav-content').toggle("slow", function() {
                    // Animation complete.
                });
            });
        };

        customUI.prototype.unsliderOld = function () {

            if($('.slider').length > 0) {
                $('.slider').unslider({
                    infinite: true,
                    autoplay: true,
                    delay: 5000
                });
            }
        };

        customUI.prototype.mainNavigation = function () {
            $('.mobileButton').click(function() {
                if($('.mobileMenu').css('display') == 'block'){
                    $('.mobileMenu').slideUp();
                } else {
                    $('.mobileMenu').slideDown();
                }
            });

            $('.mobileMenu .hasChildren').click(function() {
                var submenu = $(this).find('.submenu');
                if($(submenu).css('display') == 'block') {
                    $(submenu).slideUp();
                }  else {
                    $(submenu).slideDown();
                }
            });

            $('.mobileSearch').click(function() {
                if($('.searchbox-header').css('display') == 'block') {
                    $('.searchbox-header').slideUp();
                } else {
                    $('.searchbox-header').slideDown();
                }
            });

        };

        customUI.prototype.oldZumijQuery = function () {
            if($('.theTeam').length) {
                $('.closeTeam').click(function(){
                    $('.fullTeam').fadeOut();
                });
                $('.teamItem').click(function() {
                    $(this).parent().find('.fullTeam').fadeIn();
                    $(this).parent().find('.fullTeam video').height($(window).height());
                    //$('.fullTeam').fadeIn();
                });
                //$('#teamvid').height($(window).height());
            }

            if($('.jobOpening').length) {
                $('.jobOpening').click(function() {
                    var descriptionatt = $(this).find('.jobDescription');
                    if (descriptionatt.css('display') == 'block') {
                        descriptionatt.slideUp();
                    } else {
                        descriptionatt.slideDown();
                    }
                });
            }

            if($('.catalog-product-view .breadcrumbs').length > 0){
                var product_breadcrumbs = $('.catalog-product-view .breadcrumbs').html();
                jQuery('.product-info-main .breadCrumb').html(product_breadcrumbs);
            }

        };

        customUI.prototype.setHeightCategorythum = function () {
            $(document).ready(function () {
                var imgthum = $('.catalog-category-view .category-view').find('.productCat').first();
                if( imgthum.length > 0){
                    var nocatimg = $('.catalog-category-view .category-view').find('.no-catimg');
                    nocatimg.css('height', imgthum.outerHeight() - 1);
                    $(window).on('resize',  function(event) {
                        var ouh = imgthum.outerHeight() - 1;
                        var nocatimg = $('.catalog-category-view .category-view').find('.no-catimg');
                        nocatimg.css('height', ouh);
                    });
                }
            });

        };

        customUI.getInstance = function () {
            if (customUI.prototype._singletonInstance == undefined) {
                customUI.prototype._singletonInstance = new knight.customUI();
            }
            return customUI.prototype._singletonInstance;
        };

    })(jQuery);


    $(document).ready(function () {
        var knight_custom_ui = new knight.customUI();
        knight_custom_ui.init();
    });
});