define([
    'jquery',
    'Bss_Quickview/js/jquery.magnific-popup.min'
], function ($) {
    $.widget('bss.bss_config', {
        _create: function () {
            var options = this.options;
            var productUrl = options.productUrl;
            var buttonText = options.buttonText;
            var isEnabled = options.isEnabled;
            var baseUrl = options.baseUrl;
            var productImageWrapper = '.' + options.productImageWrapper;
            var productItemInfo = '.' + options.productItemInfo;
            if(isEnabled == 1){
                $('a.mailto').click(function(e){
                    e.preventDefault();
                    window.top.location.href = $(this).attr('href');
                    return true;
                });

                $(productImageWrapper).each(function(){
                    var product_type =  $(this).parents(productItemInfo).find('.productDet').attr('rel');
                    var p_link = $(this).parents(productItemInfo).find('.product-item-photo').attr('href');
                    if ($(this).parents(productItemInfo).find('.actions-primary input[name="product"]').val() !='') {
                        id_product = $(this).parents(productItemInfo).find('.actions-primary input[name="product"]').val();
                    }
                    if (!id_product) {
                        id_product = $(this).parents(productItemInfo).find('.price-box').data('product-id');
                    }
                    if (!id_product) {
                        id_product = $(this).parents(productItemInfo).data('id');
                    }
                    if (id_product) {
                        if(product_type == 'super') {
                            $(this).append('<div id="quickview"><a class="bss-quickview superlink" href="'+p_link+'"><span>Configure Now</span></a></div>');
                        }else {
                            $(this).append('<div id="quickview"><a class="bss-quickview" data-quickview-url="'+productUrl+'id/'+ id_product +'" href="javascript:void(0);" ><span>'+buttonText+'</span></a></div>');
                        }
                    }
                })
                $(document).ready(function() {
                    /*$('.bss-quickview').bind('click', function() {*/
                    $(document).on("click",".bss-quickview",function() {
                        if(!$(this).hasClass('superlink')) {
                            var bg = $(this).parent().css("background");
                            var prodUrl = $(this).attr('data-quickview-url');
                            if (prodUrl.length) {
                                openPopup(prodUrl, bg);
                            }
                        }
                    });
                    $.ajax({
                        url: baseUrl + 'bss_quickview/index/updatecart',
                        method: "POST"
                    });
                });
                function openPopup(prodUrl, bg) {
                    if (!prodUrl.length) {
                        return false;
                    }
                    var url = baseUrl + 'bss_quickview/index/updatecart';

                    $.magnificPopup.open({
                        items: {
                            src: prodUrl
                        },
                        type: 'iframe',
                        closeOnBgClick: true,
                        scrolling: false,
                        preloader: true,
                        tLoading: '',
                        callbacks: {
                            open: function() {
                                $('.mfp-preloader').css('display', 'block');
                                $("iframe.mfp-iframe").contents().find("html").addClass("bss_loader");
                                $('.mfp-bg').css('background',bg);
                            },
                            beforeClose: function() {
                                $('[data-block="minicart"]').trigger('contentLoading');
                                $.ajax({
                                    url: url,
                                    method: "POST"
                                });
                            },
                            close: function() {
                                $('.mfp-preloader').css('display', 'none');
                            }
                        }
                    });
                }
            }
        }
    });
    return $.bss.bss_config;
});
