/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'bootstrap' : 'Magento_Theme/js/bootstrap',
        'slick' : 'Magento_Theme/js/slick',
        'unslider' : 'Magento_Theme/js/unslider'
    } ,
    shim: {
        'bootstrap': {
            'deps': ['jquery']
        },
        'slick': {
            'deps': ['jquery']
        },
        'unslider': {
            'deps': ['jquery']
        }
    }
};