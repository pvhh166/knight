/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_OneStepCheckout
 * @author    Extension Team
 * @copyright Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */

define([
    'jquery',
    'underscore',
    'ko',
    'mageUtils',
    'Magento_Checkout/js/view/shipping-address/list',
    'uiLayout',
    'Brandsmith_QuickOrder/js/select2.min',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data'
], function ($, _, ko, utils, Component, layout, select2, addressList, checkoutData, selectShippingAddressAction,quote, customerData) {
    'use strict';

    var defaultRendererTemplate = {
        parent: '${ $.$data.parentName }',
        name: '${ $.$data.name }',
        component: 'Magento_Checkout/js/view/shipping-address/address-renderer/default',
        template: 'Bss_OneStepCheckout/shipping-address/address-renderer/default'
    };
    var countryData = customerData.get('directory-data');

    return Component.extend({

        /** @inheritdoc */
        initialize: function () {
            this._super()
                .initChildren();
            addressList.subscribe(function (changes) {
                    var self = this;
                    changes.forEach(function (change) {
                        if (change.status === 'added') {
                            self.availableAddressOptions().push(change.value);
                            self.selectedAddress(change.value);
                            self.renderedSelect();
                        }
                    });
                },
                this,
                'arrayChange'
            );
            return this;
        },

        /**
         * @return {exports.initObservable}
         */
        initObservable: function () {
            var shippingAddress = ko.observable(quote.shippingAddress());
            var cusData = window.checkoutConfig.customerData;
            var cusAdd = cusData.default_shipping;
            var adds = addressList();
            var selectedAdd;
            $.each(adds, function(i, item) {
                if(item.customerAddressId == cusAdd) {
                    selectedAdd = item;
                }
            });
            this._super()
                .observe({
                    selectedAddress: selectedAdd
                });
            this.availableAddressOptions = ko.computed(function () {
                return addressList();
            });
            return this;
        },

        /**
         * Create new component that will render given address in the address list
         *
         * @param {Object} address
         * @param {*} index
         */
        createRendererComponent: function (address, index) {
            var rendererTemplate, templateData, rendererComponent;

            if (index in this.rendererComponents) {
                this.rendererComponents[index].address(address);
            } else {
                // rendererTemplates are provided via layout
                rendererTemplate = address.getType() != undefined && this.rendererTemplates[address.getType()] != undefined ? //eslint-disable-line
                    utils.extend({}, defaultRendererTemplate, this.rendererTemplates[address.getType()]) :
                    defaultRendererTemplate;
                templateData = {
                    parentName: this.name,
                    name: index
                };
                rendererComponent = utils.template(rendererTemplate, templateData);
                utils.extend(rendererComponent, {
                    address: ko.observable(address)
                });
                layout([rendererComponent]);
                this.rendererComponents[index] = rendererComponent;
            }
        },

        /**
         * @param {Object} address
         * @return {*}
         */
        addressOptionsText: function (address) {

            var cusData = window.checkoutConfig.customerData;
            var cusAddId = cusData.default_shipping;

            var countryName = countryData()[address.countryId] != undefined ? countryData()[address.countryId].name : ''; //eslint-disable-line

            if(address.company) {
                var addressText = address.firstname + ' ' + address.lastname+ ', '
                    + address.company + ', '
                    + _.values(address.street).join(", ") + ', '
                    + address.city + ', '
                    + address.region + ', '
                    + address.postcode + ', '
                    + countryName;
            }else {
                var addressText = address.firstname + ' ' + address.lastname+ ', '
                    + _.values(address.street).join(", ") + ', '
                    + address.city + ', '
                    + address.region + ', '
                    + address.postcode + ', '
                    + countryName;
            }

            if(address.customerAddressId == cusAddId) {
                setTimeout(function(){ $('.select2-selection__rendered').html(addressText); }, 500);
            }
            return addressText;
        },

        renderedSelect: function () {
            $('.shipping_address_id').select2({width: '100%'});
        },
        /**
         * @param {Object} address
         */
        onAddressChange: function (data, event) {
            //eslint-disable-line eqeqeq
            var address = this.selectedAddress();
            selectShippingAddressAction(address);
            checkoutData.setSelectedShippingAddress(address.getKey());
        }



    });
});
