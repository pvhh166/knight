/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_OneStepCheckout
 * @author    Extension Team
 * @copyright Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */

define([
    'jquery',
    'ko',
    'underscore',
    'Magento_Ui/js/form/form',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Customer/js/customer-data',
    'mage/storage',
    'Magento_Checkout/js/action/set-billing-address',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'Brandsmith_QuickOrder/js/select2.min',
    'Magento_Ui/js/modal/modal'
], function (
    $,
    ko,
    _,
    Component,
    customer,
    addressList,
    quote,
    createBillingAddress,
    selectBillingAddress,
    checkoutData,
    checkoutDataResolver,
    customerData,
    storage,
    setBillingAddressAction,
    globalMessageList,
    $t,
    select2,
    modal
) {
    'use strict';

    var lastSelectedBillingAddress = null,
        popUp = null,
        newAddressOption = {
            /**
             * Get new address label
             * @returns {String}
             */
            getAddressInline: function () {
                return $t('New Address');
            },
            customerAddressId: null
        },
        countryData = customerData.get('directory-data'),
        addressOptions = addressList().filter(function (address) {
            return address.getType() == 'customer-address'; //eslint-disable-line eqeqeq
        });



    return Component.extend({
        defaults: {
            template: 'Bss_OneStepCheckout/billing-address',

        },

        currentBillingAddress: quote.billingAddress,
        addressOptions: addressOptions,
        customerHasAddresses: addressOptions.length > 1,
        isNewAddressAdded: ko.observable(false),
        isAddressFormVisible: ko.observable(false),
        availableAddressOptions:  ko.observableArray(addressOptions),

        /**
         * Init component
         */
        initialize: function () {
            var self = this,
                hasNewAddress;
            this._super();
            this.isNewAddressAdded(hasNewAddress);
            quote.paymentMethod.subscribe(function () {
                if (self.isAddressSameAsShipping()) {
                    selectBillingAddress(quote.shippingAddress());
                }
                checkoutDataResolver.resolveBillingAddress();
            }, this);
            this.isAddressFormVisible.subscribe(function (value) {
                if (value) {
                    self.getPopUp().openModal();
                }
            });
        },

        /**
         * @return {exports.initObservable}
         */
        initObservable: function () {
            this._super()
                .observe({
                    selectedAddress: null,
                    isAddressDetailsVisible: quote.billingAddress() != null,
                    //isAddressFormVisible: !customer.isLoggedIn() || addressOptions.length === 1,
                    isAddressSameAsShipping: false,
                    isAddressListVisible: true,
                    saveInAddressBook: 1
                });


            quote.billingAddress.subscribe(function (newAddress) {
                if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                    this.saveInAddressBook(newAddress.saveInAddressBook);
                } else {
                    this.saveInAddressBook(1);
                }

            }, this);

            return this;
        },

        canUseShippingAddress: ko.computed(function () {
            return !quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
        }),

        /**
         * @return {*}
         */
        getPopUp: function () {
            var self = this,
                buttons,
                popUpForm = {
                    element: "#opc-new-billing-address",
                    options: {
                        type: "popup",
                        responsive: true,
                        innerScroll: true,
                        title: $t('Billing Address')
                    }

                };

            if (!popUp) {
                buttons = popUpForm.options.buttons;
                popUpForm.options.buttons = [
                    {
                        text:  $t('Save Address'),
                        class: 'action primary action-save-address',
                        click: self.saveNewAddress.bind(self)
                    },
                    {
                        text:  $t('Cancel'),
                        class: 'action secondary action-hide-popup',
                        /** @inheritdoc */
                        click: this.onClosePopUp.bind(this)
                    }
                ];

                /** @inheritdoc */
                popUpForm.options.closed = function () {
                    self.isAddressFormVisible(false);
                };

                popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                popUpForm.options.keyEventHandlers = {
                    escapeKey: this.onClosePopUp.bind(this)
                };

                /** @inheritdoc */
                popUpForm.options.opened = function () {
                    // Store temporary address for revert action in case when user click cancel action
                    self.temporaryAddress = $.extend(true, {}, checkoutData.getBillingAddressFromData());
                };
                popUp = modal(popUpForm.options, $(popUpForm.element));
            }

            return popUp;
        },

        /**
         * Revert address and close modal.
         */
        onClosePopUp: function () {
            checkoutData.setBillingAddressFromData($.extend(true, {}, this.temporaryAddress));
            this.getPopUp().closeModal();
        },

        /**
         * Save new Billing address
         */
        saveNewAddress: function () {
            var addressData, newBillingAddress, update, isTick;
            this.source.set('params.invalid', false);
            this.source.trigger(this.dataScopePrefix + '.data.validate');

            if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
            }

            if (!this.source.get('params.invalid')) {
                addressData = this.source.get(this.dataScopePrefix);

                if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                    this.saveInAddressBook(1);
                }
                addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;

                isTick = 0;
                // addressData['save_in_address_book'] = 0;
                // console.log(addressData);
                if(addressData['save_in_address_book'] == 1) {
                    storage.post(
                        'ovcustom/customer/saveaddress',
                        JSON.stringify(addressData)
                    ).done(
                        function (response) {
                            /** Do your code here */
                            if(!response.success) {
                                isTick = 1;
                            }
                        }
                    ).fail(
                        function (response) {
                            isTick = 1;
                        }
                    );
                }
                addressData['save_in_address_book'] = isTick;

                newBillingAddress = createBillingAddress(addressData);

                // New address must be selected as a billing address
                selectBillingAddress(newBillingAddress);
                checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                checkoutData.setNewCustomerBillingAddress(addressData);
                update = true;
                this.getPopUp().closeModal();

                if(this.isNewAddressAdded()) {
                    this.availableAddressOptions.pop();
                    this.availableAddressOptions.push(newBillingAddress);
                    this.selectedAddress(newBillingAddress);
                    this.renderedSelect();
                }else{
                    this.isNewAddressAdded(true);
                    this.availableAddressOptions.push(newBillingAddress);
                    this.selectedAddress(newBillingAddress);
                    this.renderedSelect();
                }

            }
            if (!_.isUndefined(update)) {
                this.updateAddresses();
            }
        },

        /**
         * @param {Object} address
         * @return {*}
         */
        addressOptionsText: function (address) {
            var countryName = countryData()[address.countryId] != undefined ? countryData()[address.countryId].name : ''; //eslint-disable-line
            if(address.company) {
                return address.firstname + ' ' + address.lastname+ ', '
                    + address.company + ', '
                    + _.values(address.street).join(", ") + ', '
                    + address.city + ', '
                    + address.region + ', '
                    + address.postcode + ', '
                    + countryName;
            }else {
                return address.firstname + ' ' + address.lastname+ ', '
                    + _.values(address.street).join(", ") + ', '
                    + address.city + ', '
                    + address.region + ', '
                    + address.postcode + ', '
                    + countryName;
            }
        },

        /**
         * @return {Boolean}
         */
        useShippingAddress: function () {
            if (this.isAddressSameAsShipping()) {
                selectBillingAddress(quote.shippingAddress());
                this.updateAddresses();
                this.isAddressDetailsVisible(true);
            } else {
                lastSelectedBillingAddress = quote.billingAddress();
                quote.billingAddress(null);
                this.isAddressDetailsVisible(false);
            }
            checkoutData.setSelectedBillingAddress(null);

            return true;
        },

        /**
         * Update address action
         */
        updateAddress: function () {
            var addressData, newBillingAddress, update;

            if (this.selectedAddress() && this.selectedAddress() != newAddressOption) { //eslint-disable-line eqeqeq
                selectBillingAddress(this.selectedAddress());
                checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                update = true;
            } else {
                this.source.set('params.invalid', false);
                this.source.trigger(this.dataScopePrefix + '.data.validate');

                if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                    this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                }

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get(this.dataScopePrefix);

                    if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                        this.saveInAddressBook(1);
                    }
                    addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);
                    this.isAddressListVisible(true);
                    this.isAddressFormVisible(false);
                    update = true;
                }
            }
            if (!_.isUndefined(update)) {
                this.updateAddresses();
            }
        },

        /**
         * Edit address action
         */
        editAddress: function () {
            lastSelectedBillingAddress = quote.billingAddress();
            quote.billingAddress(null);
            this.isAddressDetailsVisible(false);
        },

        /**
         * Cancel address edit action
         */
        cancelAddressEdit: function () {
            this.restoreBillingAddress();

            if (quote.billingAddress()) {
                // restore 'Same As Shipping' checkbox state
                this.isAddressSameAsShipping(
                    quote.billingAddress() != null &&
                    quote.billingAddress().getCacheKey() == quote.shippingAddress().getCacheKey() && //eslint-disable-line
                    !quote.isVirtual()
                );
                this.isAddressDetailsVisible(true);
            }
        },

        /**
         * Restore billing address
         */
        restoreBillingAddress: function () {
            if (lastSelectedBillingAddress != null) {
                selectBillingAddress(lastSelectedBillingAddress);
            }
        },

        /**
         * @param {Object} address
         */
        onAddressChange: function (address) {

            this.updateAddress();

        },

        /**
         * Show address form popup
         */
        showFormPopUp: function () {
            this.isAddressFormVisible(true);
        },

        /**
         * @param {Number} countryId
         * @return {*}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /**
         * Trigger action to update shipping and billing addresses
         */
        updateAddresses: function () {
            if (window.checkoutConfig.reloadOnBillingAddress ||
                !window.checkoutConfig.displayBillingOnPaymentMethod
            ) {
                setBillingAddressAction(globalMessageList);
            }
        },

        /**
         * Get code
         * @param {Object} parent
         * @returns {String}
         */
        getCode: function (parent) {
            return _.isFunction(parent.getCode) ? parent.getCode() : 'shared';
        },

        renderedSelect: function () {
            $('.billing-address-id').select2({width: '100%'});
        }
    });
});
