<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Ced\HubIntegration\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /** @var \Magento\Framework\App\Config\ScopeConfigInterface  */
    public $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $this->mauticLog();

        /**
         * Create table 'hub_ced_items'
         */
        $table = $installer->getConnection()->newTable($installer->getTable('hub_ced_items'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                'object_type',
                Table::TYPE_TEXT,
                20,
                ['nullable' => false, 'default' => null ],
                'Object Type'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )->addColumn(
                'object_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Object Id'
            )->addIndex(
                $installer->getIdxName('hub_ced_items', ['object_type', 'object_id']),
                ['object_type', 'object_id']
            )
            ->setComment('HubSpot Ced Items')->setOption('type', 'InnoDB')->setOption('charset', 'utf8');
        if ($installer->getConnection()->isTableExists('hub_ced_items') != true) {
            $installer->getConnection()->createTable($table);
        }
    }

    public function mauticLog()
    {
        $merchantmail= $this->scopeConfig->getValue('trans_email/ident_general/email');
        $domain = $this->scopeConfig->getValue('web/secure/base_url');
        $body = '<table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                  <td>
                  <table cellpadding="0" cellspacing="0" border="0">
                   <tr>
                   <td class="email-heading">
                   <h1>You have a new installation for HubSpot M2 Extension.</h1>
                   <p> Please review your admin panel."</p>
                   </td>
                   </tr>
                  </table>
                   </td>
                  </tr>
                  <tr>
                  <td>
                  <h4>Merchant Domain: ' . $domain . '</h4>
                  </td>
                  </tr>
                  <tr>
                  <td>
                  <h4>Merchant Email: ' . $merchantmail . '</h4>
                  </td>
                  </tr>
                  <tr>
                  <td>
                  <h4>Installation Date: ' . date('Y-m-d H:i:s') . '</h4>
                 </td>
                 </tr>
                 </table>';

        $to_email = 'ankurverma@cedcoss.com';
        $subject = 'New Installation of HubSpot M2 extension';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: ' . $merchantmail . '' . "\r\n";
         mail($to_email, $subject, $body, $headers);
    }
}
