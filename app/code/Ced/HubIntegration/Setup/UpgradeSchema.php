<?php
/**
 * Created by PhpStorm.
 * User: cedcoss
 * Date: 16/9/19
 * Time: 4:45 PM
 */

namespace Ced\HubIntegration\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '4.0.0', '<')) {
            $table = $installer->getConnection()->newTable($installer->getTable('hub_ced_queued_items'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )->addColumn(
                    'object_type',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable' => false, 'default' => null ],
                    'Object Type'
                )->addColumn(
                    'batch_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Batch Id'
                )->addColumn(
                    'batch_id_count',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Batch Id Count'
                )->addColumn(
                    'total_count',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Total Count'
                )
                ->addColumn(
                    'id_json',
                    Table::TYPE_TEXT,
                    Table::MAX_TEXT_SIZE,
                    ['nullable' => false, 'default' => null ],
                    'Ids in json'
                )->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )->addIndex(
                    $installer->getIdxName('hub_ced_queued_items', ['object_type', 'batch_id']),
                    ['object_type', 'batch_id']
                )
                ->setComment('HubSpot Ced Queued Items')->setOption('type', 'InnoDB')->setOption('charset', 'utf8');
            if ($installer->getConnection()->isTableExists('hub_ced_queued_items') != true) {
                $installer->getConnection()->createTable($table);
            }
        }

        $installer->endSetup();
    }
}
