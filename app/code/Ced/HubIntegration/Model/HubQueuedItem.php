<?php
/**
 * Created by PhpStorm.
 * User: cedcoss
 * Date: 16/9/19
 * Time: 7:02 PM
 */

namespace Ced\HubIntegration\Model;

use Magento\Framework\Model\AbstractModel;

class HubQueuedItem extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Ced\HubIntegration\Model\ResourceModel\HubQueuedItem');
    }
}
