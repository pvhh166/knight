<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   Ced_HubIntegration
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Cron
 * @package Ced\HubIntegration\Model
 */
class HubItem extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Ced\HubIntegration\Model\ResourceModel\HubItem');
    }
}
