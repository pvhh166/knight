<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Model;

class Properties
{
    /**
     * @param $group
     * @return array
     */
    public function getGroupProperty($group)
    {
        $property = [];
        switch ($group) {
            case 'PRODUCT':
                $property = [
                    "name",
                    "image",
                    "price",
                    "description",
                    "product_image_url"
                ];
                break;

            case 'CONTACT':
                $property = [
                    "email",
                    "firstname",
                    "lastname",
                    "company",
                    "telephone",
                    "street",
                    "city",
                    "region",
                    "country",
                    "postcode",
                    "contact_stage",
                    "customer_group",
                    'newsletter_subscription',
                    'shopping_cart_customer_id',
                    'shipping_address_line_1',
                    'shipping_address_line_2',
                    'shipping_city',
                    'shipping_state',
                    'shipping_postal_code',
                    'shipping_country',
                    'billing_address_line_1',
                    'billing_address_line_2',
                    'billing_city',
                    'billing_state',
                    'billing_postal_code',
                    'billing_country',
                    'last_product_bought',
                    'last_product_types_bought',
                    'last_products_bought',
                    'last_products_bought_html',
                    'last_total_number_of_products_bought',
                    'product_types_bought',
                    'products_bought',
                    'total_number_of_products_bought',
                    'last_products_bought_product_1_image_url',
                    'last_products_bought_product_1_name',
                    'last_products_bought_product_1_price',
                    'last_products_bought_product_1_url',
                    'last_products_bought_product_2_image_url',
                    'last_products_bought_product_2_name',
                    'last_products_bought_product_2_price',
                    'last_products_bought_product_2_url',
                    'last_products_bought_product_3_image_url',
                    'last_products_bought_product_3_name',
                    'last_products_bought_product_3_price',
                    'last_products_bought_product_3_url',
                    'last_order_status',
                    'last_order_fulfillment_status',
                    'last_order_tracking_number',
                    'last_order_tracking_url',
                    'last_order_shipment_date',
                    'last_order_order_number',
                    'total_number_of_current_orders',
                    'categories_bought',
                    'last_categories_bought',
                    'last_skus_bought',
                    'skus_bought',
                    'total_value_of_orders',
                    'average_order_value',
                    'total_number_of_orders',
                    'first_order_value',
                    'first_order_date',
                    'last_order_value',
                    'last_order_date',
                    'average_days_between_orders',
                    'account_creation_date',
                    'monetary_rating',
                    'order_frequency_rating',
                    'order_recency_rating'
                ];
                break;

            case 'DEAL':
                $property = [
                    "deal_stage",
                    "deal_name",
                    "closed_won_reason",
                    "closed_lost_reason",
                    "close_date",
                    "amount",
                    "pipeline",
                    "abandoned_cart_url",
                    "discount_amount",
                    "increment_id",
                    "shipment_ids",
                    "tax_amount",
                    "contact_ids"
                ];
                break;

            case 'LINE_ITEM':
                $property = [
                    "product_id",
                    "deal_id",
                    "discount_amount",
                    "qty",
                    "price",
                    "name",
                    "sku"
                ];
                break;
        }

        return $property;
    }
}
