<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Model;

class DataSync
{
    const PREFIX = 'HUB_';
    const PAGE_SIZE = 400;
    /**
     * @var Properties
     */
    public $properties;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    public $sale;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory
     */
    public $quote;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    public $product;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    public $customer;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    public $country;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory
     */
    public $shipment;

    /**
     * @var \Magento\Framework\Url $urlHelper
     */
    public $urlHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfigManager;

    /**
     * @var ResourceModel\HubItem\CollectionFactory
     */
    public $hubItem;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $dateTime;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    public $groupRepository;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    public $subscriberFactory;

    /**
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    public $imageBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $store;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    public $currencyFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    public $categoryFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory
     */
    public $orderItemCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    public $timeZone;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    public $regionFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    public $request;

    public $categoryName = [];

    public $productObject = [];

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface  */
    public $scopeConfig;

    public $proFactory;

    public $quoteItemCollectionFactory;

    public $connectionManager;

    /**
     * DataSync constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentFactory
     * @param \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quote
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $order
     * @param Properties $properties
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigManager
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\Url $urlHelper
     * @param ResourceModel\HubItem\CollectionFactory $hubItemFactory
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $store
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentFactory,
        \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quote,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $order,
        \Ced\HubIntegration\Model\Properties $properties,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigManager,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Framework\Url $urlHelper,
        \Ced\HubIntegration\Model\ResourceModel\HubItem\CollectionFactory $hubItemFactory,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Store\Model\StoreManagerInterface $store,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ProductFactory $proFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory $quoteItemCollectionFactory,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager
    ) {
        $this->shipment = $shipmentFactory;
        $this->properties = $properties;
        $this->sale = $order;
        $this->quote = $quote;
        $this->dateTime = $dateTime;
        $this->product = $productFactory;
        $this->customer = $customerFactory;
        $this->country = $countryFactory;
        $this->urlHelper = $urlHelper;
        $this->scopeConfigManager = $scopeConfigManager;
        $this->hubItem = $hubItemFactory;
        $this->groupRepository = $groupRepository;
        $this->subscriberFactory = $subscriberFactory;
        $this->imageBuilder = $imageBuilder;
        $this->store = $store;
        $this->currencyFactory = $currencyFactory;
        $this->categoryFactory = $categoryFactory;
        $this->timeZone = $timezone;
        $this->regionFactory = $regionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->request = $request;
        $this->proFactory = $proFactory;
        $this->quoteItemCollectionFactory = $quoteItemCollectionFactory;
        $this->connectionManager = $connectionManager;
    }

    /**
     * Sync Recently updated or created products into hub
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProduct($time)
    {

        if ($time == 0) {
            $dateStart = date('Y-m-d H:i:s', 0);
        } else {
            $dateStart = $this->dateTime->Date('Y-m-d H:i:s', ' - ' . $time . ' minutes');
        }

        $dateEnd = $this->dateTime->Date('Y-m-d H:i:s');

        $products = $this->product->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('updated_at', ['from' => $dateStart, 'to' => $dateEnd])
            ->addMediaGalleryData();
        return $this->setProduct($products);
    }

    /**
     * Prepare Image Url
     * @param $product
     * @return mixed
     */
    private function prepareImages($product)
    {
        $productImages = $product->getMediaGalleryImages();
        $mainImage = $product->getData('image');
        if (!empty($productImages)) {
            foreach ($productImages as $image) {
                if ($mainImage == $image->getFile()) {
                    return $image->getUrl();
                }
            }
        }
        return $mainImage;
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getCategoryName($category)
    {
        if (isset($this->categoryName[$category])) {
            return $this->categoryName[$category];
        } else {
            $this->categoryName[$category] = $this->categoryFactory->create()->load($category)->getName();
            return $this->categoryName[$category];
        }
    }

    /**
     * Sync Recently updated or created Customer into hub
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getContact($time)
    {
        $allIds = $this->getAllIds($time);
        return $this->setContact($allIds);
    }

    /**
     * Convert the date/date
     * @param $date
     * @return float|int
     */
    public function getDateStamp($date)
    {
        return strtotime(date("y-m-d", strtotime($date))) * 1000;
    }

    /**
     * @param $email
     * @return string
     */
    public function getNewsLetterSubscription($email)
    {
        $checkSubscriber = $this->subscriberFactory->create()->loadByEmail($email);
        if ($checkSubscriber->isSubscribed()) {
            return "yes";
        } else {
            return "no";
        }
    }

    /**
     * @param $orderItem
     * @param $lastThreeProducts
     * @return mixed
     */
    public function getThreeLastProducts($orderItem, $lastThreeProducts)
    {
        if (empty($lastThreeProducts['last'])) {
            $lastThreeProducts['last'] = $orderItem->getData();
        } elseif ($lastThreeProducts['last']['created_at'] < $orderItem->getCreateddAt()) {
            $lastThreeProducts['third_last'] = $lastThreeProducts['second_last'];
            $lastThreeProducts['second_last'] = $lastThreeProducts['last'];
            $lastThreeProducts['last'] = $orderItem->getData();
        } elseif (empty($lastThreeProducts['second_last']) ||
            $lastThreeProducts['second_last'] ['created_at'] < $orderItem->getCreatedAt()) {
            $lastThreeProducts['third_last'] = $lastThreeProducts['second_last'];
            $lastThreeProducts['second_last'] = $orderItem->getData();
        } elseif (empty($lastThreeProducts['third_last']) ||
            $lastThreeProducts['third_last']['created_at'] < $orderItem->getCreatedAt()) {
            $lastThreeProducts['third_last'] = $orderItem->getData();
        }

        return $lastThreeProducts;
    }

    /**
     * @param $product
     * @param $orderItem
     * @param $key
     * @return mixed
     */
    public function getDataForProductHtml($product, $orderItem, $key, $last_order_products)
    {
        $imageUrl = $this->getImageUrl($product->getImage());
        if ($product->getData('visibility') ==1) {
            $productUrl="Not Visible Individually";
        } else {
            $productUrl = $product->getProductUrl();
        }
        $last_order_products[$key]["image"] = $imageUrl;
        $last_order_products[$key]["name"] = $orderItem->getData('name');
        $last_order_products[$key]["url"] = $productUrl;
        $last_order_products[$key]["price"] = $orderItem->getData('base_row_total_incl_tax');
        $last_order_products[$key]["qty"] = $orderItem->getData('qty_ordered');
        return $last_order_products;
    }

    /**
     * @param $allIds
     * @return array
     */
    public function getOrdersList($allIds)
    {
        $allOrders = $this->sale->create()->addAttributeToSelect('*')
            ->addAttributeToFilter('customer_id', ['in' => $allIds])
            ->setOrder('created_at', 'desc');

        $ordersList = [];
        foreach ($allOrders as $order) {
            $ordersList[$order->getCustomerId()][$order->getId()] = $order;
        }

        return $ordersList;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAllIds($time)
    {
        if ($time == 0) {
            $dateStart = date('Y-m-d H:i:s', 0);
        } else {
            $dateStart = $this->dateTime->Date('Y-m-d H:i:s', ' - ' . $time . ' minutes');
        }
        $dateEnd = $this->dateTime->Date('Y-m-d H:i:s');

        $customers = $this->customer->create()
            ->addAttributeToFilter('updated_at', ['from' => $dateStart, 'to' => $dateEnd]);
        $allId = $customers->getAllIds();

        $updatedOrders = $this->sale->create()->addAttributeToSelect('*')
            ->addAttributeToFilter('updated_at', ['from' => $dateStart, 'to' => $dateEnd]);

        $ordersCustomerId = $updatedOrders->getColumnValues('customer_id');

        $allIds = array_merge($allId, $ordersCustomerId);
        return $allIds;
    }

    /**
     * @param $customerLastOrder
     * @return string
     */
    public function getRecencyDateDiff($customerLastOrder)
    {
        $lastOrderDate = date('Y-m-d', strtotime($customerLastOrder->getCreatedAt()));
        $currentTime = $this->timeZone->formatDateTime(date('Y-m-d H:i:s'));
        $time = date('Y-m-d', strtotime($currentTime));
        $recencyDateDiff = date_diff(date_create($time), date_create($lastOrderDate))
            ->format('%a');
        return $recencyDateDiff;
    }

    /**
     * @param $customerFirstOrder
     * @param $customerLastOrder
     * @param $totalOrders
     * @return float|int
     */
    public function getAvgDays($customerFirstOrder, $customerLastOrder, $totalOrders)
    {
        $firstOrderDate = date('Y-m-d', strtotime($customerFirstOrder->getCreatedAt()));
        $lastOrderDate = date('Y-m-d', strtotime($customerLastOrder->getCreatedAt()));
        $dateDiff = date_diff(date_create($lastOrderDate), date_create($firstOrderDate))->format('%a');
        $avgDays = $dateDiff / $totalOrders;
        return $avgDays;
    }

    /**
     * @param $addressLine1
     * @param $address
     * @return mixed
     */
    public function getAddressLine2($addressLine1, $address)
    {
        $addressLines = str_replace(
            $addressLine1,
            "",
            $address
        );
        $addressLine2 = str_replace("\n", " ", $addressLines);
        return $addressLine2;
    }

    /**
     * @param $image
     * @return string
     */
    public function getImageUrl($image)
    {
        $url = $this->store->getStore()->getBaseUrl();
        $imageUrl = $url . "pub/media/catalog/product" . $image;
        return $imageUrl;
    }

    /**
     * @param $keyword
     * @param $value
     * @return int
     */
    public function getRating($keyword, $value)
    {
        $data = $this->scopeConfig->getValue(
            'hub_integration/rfm_settings/rfm_fields',
            'default',
            $scopeCode = null
        );
        $rfmRating = json_decode($data, true);

        switch ($keyword) {
            case 'recency':
                if ($value <= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value >= $rfmRating['from_rfm_4'][$keyword] && $value <= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value >= $rfmRating['from_rfm_3'][$keyword] && $value <= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value >= $rfmRating['from_rfm_2'][$keyword] && $value <= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            case 'frequency':
                if ($value >= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value >= $rfmRating['from_rfm_4'][$keyword] && $value <= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value >= $rfmRating['from_rfm_3'][$keyword] && $value <= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value >= $rfmRating['from_rfm_2'][$keyword] && $value <= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            case 'monetary':
                if ($value >= $rfmRating['rfm_at_5'][$keyword]) {
                    return 5;
                } elseif ($value >= $rfmRating['from_rfm_4'][$keyword] && $value <= $rfmRating['to_rfm_4'][$keyword]) {
                    return 4;
                } elseif ($value >= $rfmRating['from_rfm_3'][$keyword] && $value <= $rfmRating['to_rfm_3'][$keyword]) {
                    return 3;
                } elseif ($value >= $rfmRating['from_rfm_2'][$keyword] && $value <= $rfmRating['to_rfm_2'][$keyword]) {
                    return 2;
                } else {
                    return 1;
                }
                break;

            default:
                return 0;
        }
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getProductData($productId)
    {
        if (isset($this->productObject[$productId])) {
            return $this->productObject[$productId];
        } else {
            $this->productObject[$productId] = $this->product->create()->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', $productId)
                ->getFirstItem();
            return $this->productObject[$productId];
        }
    }

    /**
     * @param $last_order_products
     * @param $currencyCode
     * @return string
     */
    public function lastOrderHtml($last_order_products, $currencyCode)
    {
        $currencySymbol = $this->currencyFactory->create()->load($currencyCode)->getCurrencySymbol();
        if (!empty($last_order_products)) {
            $products_html = '<div class="" style="position: relative; left: 0px; top: 0px;" data-slot="separator"><hr>
                           </div><div data-slot="text"><table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>

        <td style="border-bottom: 1px solid #f4f4f4;" width="225">

            <strong>Image</strong>

        </td>

        <td style="border-bottom: 1px solid #f4f4f4;">

            <strong>Item</strong>

        </td>

        <td style="border-bottom: 1px solid #f4f4f4;" width="60">

           <strong>Qty</strong>

        </td>

        <td style="border-bottom: 1px solid #f4f4f4;" width="60">

            <strong>Cost</strong>

        </td>
    </tr>';

            foreach ($last_order_products as $single_product) {
                $products_html .= '<tr>

        <td>

            <img src="' . $single_product["image"] . '" height="100px" width="100px"/>
        </td>

        <td>

            <a href="' . $single_product["url"] . '"><strong>' . $single_product["name"] . '</strong></a>
        </td>
       
        <td>

            <strong>' . $single_product["qty"] . '</strong>
        </td>

        <td>

           <strong>' . $currencySymbol . $single_product["price"] . '</strong>

        </td>

    </tr>';
            }

            $products_html .= '</tbody></table></div><div class="" style="position: relative; left: 0px; top: 0px;" 
                                data-slot="separator"><hr></div>';
        }
        return $products_html;
    }

    /**
     * Generate SyncData For Product, Deal, Line_Item objects
     * @param $properties
     * @param $value
     * @param $objectId
     * @return array
     */
    public function attributes($properties, $value, $objectId)
    {
        $arr = [];
        foreach ($properties as $property) {
            if ($data = $value->getData($property)) {
                $arr[$property] = $data;
            }
        }
        $synData = [
            "integratorObjectId" => self::PREFIX . $objectId,
            "action" => "UPSERT",
            "changeOccurredTimestamp" => strtotime(date('Y-m-d H:i:s ', time())) . '000',
            "propertyNameToValues" => $arr
        ];

        return $synData;
    }

    /**
     * Compute Provided Customer Address object
     * @param $customer
     * @return bool|\Magento\Customer\Model\Address|\Magento\Framework\DataObject
     */
    public function address($customer)
    {
        $defaultAddress = false;
        /** @var \Magento\Customer\Model\Customer $customer */
        if ($customer->getDefaultBilling()) {
            $defaultAddress = $customer->getDefaultBillingAddress();
        } else {
            if ($customer->getDefaultShipping()) {
                $defaultAddress = $customer->getDefaultShippingAddress();
            } else {
                $address = $customer->getAddressCollection()
                    ->addFieldToFilter('parent_id', $customer->getId())
                    ->getLastItem();
                /** @var \Magento\Customer\Model\Address $address */
                if ($address && $address->getId()) {
                    $defaultAddress = $address;
                }
            }
        }
        return $defaultAddress;
    }

    /**
     * Compute Customer stage as Lead, Opportunity, Customer
     * @param $customerId
     * @return string
     */
    public function stage($customerId)
    {
        if ($this->sale->create()->addFieldToFilter('customer_id', $customerId)->getSize() > 0) {
            $stage = "customer";
        } elseif ($this->quote->create()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('items_count', ['gt' => 0])->getSize() > 0
        ) {
            $stage = "opportunity";
        } else {
            $stage = "lead";
        }
        return $stage;
    }

    /**
     * Sync Recently updated or created Deal into hub
     * @return array
     */
    public function getDeal($time)
    {
        $objectType = 'DEAL';
        $synData = [];
        $syncLineItem = [];
        if (!$this->isEnabled()) {
            return $synData;
        }
        if ($time == 0) {
            $dateStart = date('Y-m-d H:i:s', 0);
        } else {
            $dateStart = $this->dateTime->Date('Y-m-d H:i:s', ' - '.$time.' minutes');
        }

        $dateEnd = $this->dateTime->Date('Y-m-d H:i:s');
        $deals = $this->sale->create()
            ->addFieldToFilter('updated_at', ['from' => $dateStart, 'to' => $dateEnd]);
        $properties = $this->properties->getGroupProperty($objectType);
        $lineProperty = $this->properties->getGroupProperty('LINE_ITEM');
        foreach ($deals as $deal) {
            /** @var \Magento\Sales\Model\Order $deal */
            $wonReason = '';
            $lostReason = '';
            $closedDate = time();
            if ($deal->getState() == 'new') {
                $state = 'checkout_completed';
                $closedDate = strtotime('+ 15 days');
            } elseif ($deal->getState() == 'processing') {
                $state = 'processed';
                $closedDate = strtotime('+ 7 days');
            } elseif ($deal->getState() == 'complete') {
                $state = 'shipped';
                $wonReason = 'Order Shipped and Invoiced';
            } else {
                $state = 'cancelled';
                $lostReason = 'Order Refunded or Canceled';
            }
            $shipmentIds = implode(
                ', ',
                $this->shipment->create()->addFieldToFilter('order_id', $deal->getId())->getAllIds()
            );
            $dealName = "order_" . $deal->getId();
            $data = [
                "deal_stage" => $state,
                "deal_name" => $dealName,
                "pipeline" => "75e28846-ad0d-4be2-a027-5e1da6590b98",
                "amount" => $deal->getGrandTotal(),
                "shipment_ids" => $shipmentIds,
                "contact_ids" => self::PREFIX . $deal->getCustomerId(),
                "close_date" => $closedDate * 1000,
                "closed_won_reason" => $wonReason,
                "closed_lost_reason" => $lostReason
            ];
            $deal->addData($data);
            $synData[] = $this->attributes($properties, $deal, $deal->getQuoteId());

            foreach ($deal->getAllVisibleItems() as $item) {
                /** @var \Magento\Sales\Model\Order\Item $item */
                $item->addData([
                    'deal_id' => self::PREFIX . $deal->getQuoteId(),
                    'product_id' => self::PREFIX . $item->getProductId(),
                    'qty' => $item->getQtyOrdered()
                ]);
                $syncLineItem[] = $this->attributes($lineProperty, $item, $item->getQuoteItemId());
            }
        }

        $abandonedCartTime = $this->getAbandonedCartTime();
        if ($time == 0) {
            $cartDateStart = date('Y-m-d H:i:s', 0);
        } else {
            $addedTime = (int)$time + (int)$abandonedCartTime;
            $cartDateStart = $this->dateTime->Date('Y-m-d H:i:s', '-' . $addedTime . ' minutes');
        }
        $carts = $this->quote->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->addFieldToFilter('items_count', ['gt' => 0])
            ->addFieldToFilter('customer_id', ['notnull' => true])
            ->addFieldToFilter('updated_at', [
                'from' => $cartDateStart,
                'to' => $this->dateTime->Date('Y-m-d H:i:s', '-' . $abandonedCartTime . ' minutes'),
            ]);

        $baseUrl = $this->urlHelper->getUrl('checkout/cart', ['_secure' => true]);
        foreach ($carts as $cart) {
            /** @var \Magento\Quote\Model\Quote $cart */
            $data = [
                "deal_stage" => 'checkout_abandoned',
                "deal_name" => "quote_" . $cart->getId(),
                "pipeline" => "75e28846-ad0d-4be2-a027-5e1da6590b98",
                "amount" => $cart->getGrandTotal(),
                "abandoned_cart_url" => $baseUrl,
                "contact_ids" => self::PREFIX . $cart->getCustomerId()
            ];
            $cart->addData($data);
            $synData[] = $this->attributes($properties, $cart, $cart->getId());
            foreach ($cart->getAllVisibleItems() as $item) {
                $item->addData([
                    'deal_id' => self::PREFIX . $cart->getId(),
                    'product_id' => self::PREFIX . $item->getProductId()
                ]);
                $syncLineItem[] = $this->attributes($lineProperty, $item, $item->getItemId());
            }
        }
        return [['DEAL' => $synData, 'LINE_ITEM' => $syncLineItem]];
    }

    /**
     * @return array|mixed
     */
    public function getDeletedItem()
    {
        $result = [];
        if (!$this->isEnabled()) {
            return $result;
        }
        $collection = $this->hubItem->create();
        foreach ($collection as $item) {
            $result[$item->getObjectType()][] = [
                "integratorObjectId" => self::PREFIX . $item->getObjectId(),
                "action" => "DELETE",
                "changeOccurredTimestamp" => strtotime(date('Y-m-d H:i:s ', time())) . '000'
            ];
        }
        $collection->walk('delete');
        return [$result];
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $products
     * @return array
     */
    public function setProduct(\Magento\Catalog\Model\ResourceModel\Product\Collection $products)
    {
        $objectType = 'PRODUCT';
        $synData = [];
        if (!$this->isEnabled()) {
            return $synData;
        }
        $properties = $this->properties->getGroupProperty($objectType);
        foreach ($products as $product) {
            $product->setImage($this->prepareImages($product));
            $image = $this->prepareImages($product);
            $product->setData('product_image_url', $image);
            $synData[] = $this->attributes($properties, $product, $product->getId());
        }
        return $synData;
    }

    /**
     * @param $allIds
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setContact($allIds)
    {

        $objectType = 'CONTACT';
        $synData = [];
        if (!$this->isEnabled()) {
            return $synData;
        }

        $country = $this->country->create();

        $allCustomers = $this->customer->create()->addAttributeToFilter('entity_id', ['in' => $allIds]);

        $ordersList = $this->getOrdersList($allIds);

        $properties = $this->properties->getGroupProperty($objectType);

        $addressProperty = ['company', 'telephone', 'street', 'city', 'region', 'postcode', 'country'];

        foreach ($allCustomers as $customer) {
            /** @var \Magento\Customer\Model\Customer $customer */
            $arr = [];
            $customerLastOrder = [];
            $customerFirstOrder = [];
            $productsBought = [];
            $totalProductsBought = 0;
            $totalProductTypesBought = [];
            $lastOrderStatus = "";
            $categoriesBought = [];
            $lastCategoriesBought = [];
            $skusBought = [];
            $lastSkusBought = [];
            $totalOrderValues = 0;
            $carrierCode = " ";
            $trackingNumber = 0;
            $lastOrderShipDate = "";
            $shippingAddress = [];
            $billingAddress = [];
            $lastProductsBought = [];
            $lastTotalProductsBought = 0;
            $last_order_products = [];
            $key = 0;
            $lastThreeProducts['last'] = [];
            $lastThreeProducts['second_last'] = [];
            $lastThreeProducts['third_last'] = [];

            if (isset($ordersList[$customer->getId()])) {
                $totalOrders = count($ordersList[$customer->getId()]);
                $keys = array_keys($ordersList[$customer->getId()]);
                $lastOrderId = $keys[0];
                $customerLastOrder = $ordersList[$customer->getId()][$lastOrderId];
                $firstOrderId = end($keys);
                $customerFirstOrder = $ordersList[$customer->getId()][$firstOrderId];

                foreach ($ordersList[$customer->getId()] as $order) {
                    foreach ($order->getAllVisibleItems() as $orderItem) {
                        array_push(
                            $productsBought,
                            $orderItem->getData('name') . "-" . $orderItem->getData('product_id')
                        );
                        $totalProductsBought += $orderItem->getData('qty_ordered');
                        $totalProductTypesBought[$orderItem->getData('product_type')]
                            = $orderItem->getData('product_type');

                        $lastThreeProducts = $this->getThreeLastProducts($orderItem, $lastThreeProducts);

                        $product[$orderItem->getData('product_id')] = $this->getProductData($orderItem
                            ->getData('product_id'));
                        $tempCat = $product[$orderItem->getData('product_id')]->getCategoryIds();
                        if (!empty($tempCat) && is_array($tempCat)) {
                            $categoryId = $tempCat[0];
                            $category = $this->getCategoryName($categoryId);

                            $categoriesBought[$category] = $category;
                        }

                        $sku = $orderItem->getData('sku');
                        $skusBought[$sku] = $sku;

                        if ($order->getId() == $lastOrderId) {
                            $lastTotalProductsBought += $orderItem->getData('qty_ordered');
                            array_push(
                                $lastProductsBought,
                                $orderItem->getData('name') . "-" . $orderItem->getData('product_id')
                            );

                            $productId = $orderItem->getData('product_id');
                            $product[$productId] = $this->getProductData($productId);
                            $tempCat = $product[$productId]->getCategoryIds();
                            if (!empty($tempCat) && is_array($tempCat)) {
                                $categoryId = $tempCat[0];
                                $category = $this->getCategoryName($categoryId);
                                $lastCategoriesBought[$category] = $category;
                            }

                            $sku = $orderItem->getData('sku');
                            $lastSkusBought[$sku] = $sku;
                            $last_order_products = $this->getDataForProductHtml(
                                $product[$productId],
                                $orderItem,
                                $key,
                                $last_order_products
                            );
                            $key++;
                        }
                    }

                    if ($order->getId() == $lastOrderId) {
                        $break = false;
                        $shipmentCollection = $customerLastOrder->getShipmentsCollection();
                        foreach ($shipmentCollection as $shipment) {
                            $lastOrderShipDate = $shipment->getData('created_at');
                            $tracks = $shipment->getAllTracks();
                            foreach ($tracks as $track) {
                                $trackingNumber = $track->getTrackNumber();
                                $carrierCode = $track->getCarrierCode();
                                $break = true;
                                break;
                            }
                            if ($break) {
                                break;
                            }
                        }
                        $lastOrderStatus = $order->getData('status');
                    }
                    $totalOrderValues += $order->getData('grand_total');
                }

                if (!empty($lastThreeProducts['last'])) {
                    $lastProduct = $this->getProductData($lastThreeProducts['last']['product_id']);
                }
                if (!empty($lastThreeProducts['second_last'])) {
                    $secondLastProduct = $this->getProductData($lastThreeProducts['second_last']['product_id']);
                }
                if (!empty($lastThreeProducts['third_last'])) {
                    $thirdLastProduct = $this->getProductData($lastThreeProducts['third_last']['product_id']);
                }

                $shippingAddress = $order->getShippingAddress();
                $billingAddress = $order->getBillingAddress();
            }

            if (empty($shippingAddress)) {
                if ($customer->getDefaultShippingAddress()) {
                    $shippingAddress = $customer->getDefaultShippingAddress();
                } elseif ($customer->getAddresses()) {
                    foreach ($customer->getAddresses() as $address) {
                        $shippingAddress = $address;
                    }
                }
            }

            if (empty($billingAddress)) {
                if ($customer->getDefaultBillingAddress()) {
                    $billingAddress = $customer->getDefaultBillingAddress();
                } elseif ($customer->getAddresses()) {
                    foreach ($customer->getAddresses() as $address) {
                        $billingAddress = $address;
                    }
                }
            }

            $address = $this->address($customer);
            foreach ($properties as $property) {
                if ($address && in_array($property, $addressProperty) &&
                    $address->getData($property)) {
                    if ($property == 'country') {
                        $arr['country'] = $country->loadByCode($address->getCountryId())->getName();
                    } else {
                        $arr[$property] = $address->getData($property);
                    }
                } elseif ($property == 'contact_stage') {
                    $arr['contact_stage'] = $this->stage($customer->getId());
                } elseif ($property == 'customer_group') {
                    if (isset($customer['group_id'])) {
                        $arr['customer_group'] = $this->groupRepository->getById($customer->getGroupId())->getCode();
                    }
                } elseif ($property == 'newsletter_subscription') {
                    if (isset($customer['email'])) {
                        $arr['newsletter_subscription'] = $this->getNewsLetterSubscription($customer->getEmail());
                    }
                } elseif ($property == 'shopping_cart_customer_id') {
                    $arr['shopping_cart_customer_id'] = (int)$customer->getId();
                } elseif ($property == 'shipping_address_line_1') {
                    if (!empty($shippingAddress) && isset($shippingAddress->getStreet()[0])) {
                        $arr['shipping_address_line_1'] = $shippingAddress->getStreet()[0];
                    }
                } elseif ($property == 'shipping_address_line_2') {
                    if (!empty($shippingAddress) && $shippingAddress->getStreet()) {
                        $addressLine2 = $this->getAddressLine2(
                            $shippingAddress->getStreet()[0],
                            $shippingAddress->getStreetFull()
                        );
                        if ($addressLine2 != "") {
                            $arr['shipping_address_line_2'] = $addressLine2;
                        }
                    }
                } elseif ($property == 'shipping_city') {
                    if (!empty($shippingAddress) && $shippingAddress->getCity()) {
                        $arr['shipping_city'] = $shippingAddress->getCity();
                    }
                } elseif ($property == 'shipping_state') {
                    if (!empty($shippingAddress)) {
                        if ($shippingAddress->getRegion()) {
                            $arr['shipping_state'] = $shippingAddress->getRegion();
                        } elseif ($shippingAddress->getRegionCode()) {
                            $region = $this->regionFactory->create()
                                ->loadByCode($shippingAddress->getRegionCode(), $shippingAddress->getCountry())
                                ->getName();
                            $arr['shipping_state'] = $region;
                        }
                    }
                } elseif ($property == 'shipping_postal_code') {
                    if (!empty($shippingAddress) && $shippingAddress->getPostcode()) {
                        $arr['shipping_postal_code'] = $shippingAddress->getPostcode();
                    }
                } elseif ($property == 'shipping_country') {
                    if (!empty($shippingAddress)) {
                        $countryCode = $shippingAddress->getCountry() ?: $shippingAddress->getCountryId();
                        if ($countryCode) {
                            $shippingCountry = $this->country->create()->loadByCode($countryCode)->getName();
                            $arr['shipping_country'] = $shippingCountry;
                        }
                    }
                } elseif ($property == 'billing_address_line_1') {
                    if (!empty($billingAddress) && isset($billingAddress->getStreet()[0])) {
                        $arr['billing_address_line_1'] = $billingAddress->getStreet()[0];
                    }
                } elseif ($property == 'billing_address_line_2') {
                    if (!empty($billingAddress) && $billingAddress->getStreet()) {
                        $addressLine2 = $this->getAddressLine2(
                            $billingAddress->getStreet()[0],
                            $billingAddress->getStreetFull()
                        );
                        if ($addressLine2 != "") {
                            $arr['billing_address_line_2'] = $addressLine2;
                        }
                    }
                } elseif ($property == 'billing_city') {
                    if (!empty($billingAddress) && $billingAddress->getCity()) {
                        $arr['billing_city'] = $billingAddress->getCity();
                    }
                } elseif ($property == 'billing_state') {
                    if (!empty($billingAddress)) {
                        if ($billingAddress->getRegion()) {
                            $arr['billing_state'] = $billingAddress->getRegion();
                        } elseif ($billingAddress->getRegionCode()) {
                            $region = $this->regionFactory->create()
                               ->loadByCode($billingAddress->getRegionCode(), $billingAddress->getCountry())->getName();
                            $arr['billing_state'] = $region;
                        }
                    }
                } elseif ($property == 'billing_postal_code') {
                    if (!empty($billingAddress) && $billingAddress->getPostcode()) {
                        $arr['billing_postal_code'] = $billingAddress->getPostcode();
                    }
                } elseif ($property == 'billing_country') {
                    if (!empty($billingAddress)) {
                        $countryCode = $billingAddress->getCountry() ?: $billingAddress->getCountryId();
                        if ($countryCode) {
                            $billingCountry = $this->country->create()->loadByCode($countryCode)->getName();
                            $arr['billing_country'] = $billingCountry;
                        }
                    }
                } elseif ($property == 'last_product_bought') {
                    if (!empty($lastThreeProducts['last'])) {
                        $arr['last_product_bought'] =
                            $lastThreeProducts['last']['name'] . "-" . $lastThreeProducts['last']['product_id'];
                        $arr['last_product_types_bought'] = $lastThreeProducts['last']['product_type'];
                    }
                } elseif ($property == 'last_product_types_bought') {
                    if (!empty($lastThreeProducts['last'])) {
                        $arr['last_product_types_bought'] = $lastThreeProducts['last']['product_type'];
                    }
                } elseif ($property == 'last_products_bought') {
                    if (!empty($lastProductsBought)) {
                        $arr['last_products_bought'] = implode(';', $lastProductsBought);
                    }
                } elseif ($property == 'last_total_number_of_products_bought') {
                    if (isset($lastTotalProductsBought) && $lastTotalProductsBought > 0) {
                        $arr['last_total_number_of_products_bought'] = $lastTotalProductsBought;
                    }
                } elseif ($property == 'products_bought') {
                    if (!empty($productsBought)) {
                        $arr['products_bought'] = implode(';', $productsBought);
                    }
                } elseif ($property == 'total_number_of_products_bought') {
                    if (isset($totalProductsBought) && $totalProductsBought > 0) {
                        $arr['total_number_of_products_bought'] = $totalProductsBought;
                    }
                } elseif ($property == 'product_types_bought') {
                    if (!empty($totalProductTypesBought)) {
                        $arr['product_types_bought'] = implode(';', array_values($totalProductTypesBought));
                    }
                } elseif ($property == 'last_products_bought_html') {
                    if (isset($ordersList[$customer->getId()]) && isset($last_order_products)) {
                        $productHtml = $this->lastOrderHtml(
                            $last_order_products,
                            $customerLastOrder->getData('base_currency_code')
                        );
                        $arr['last_products_bought_html'] = $productHtml;
                    }
                } elseif ($property == 'last_products_bought_product_1_image_url') {
                    if (!empty($lastThreeProducts['last'])) {
                        $imageUrl = $this->getImageUrl($lastProduct->getImage());
                        $arr['last_products_bought_product_1_image_url'] = $imageUrl;
                    }
                } elseif ($property == 'last_products_bought_product_1_name') {
                    if (!empty($lastThreeProducts['last'])) {
                        $arr['last_products_bought_product_1_name'] = $lastThreeProducts['last']['name'];
                    }
                } elseif ($property == 'last_products_bought_product_1_price') {
                    if (!empty($lastThreeProducts['last'])) {
                        $arr['last_products_bought_product_1_price'] =
                            $lastThreeProducts['last']['base_row_total_incl_tax']-
                            $lastThreeProducts['last']['base_discount_amount'];
                    }
                } elseif ($property == 'last_products_bought_product_1_url') {
                    if (!empty($lastThreeProducts['last'])) {
                        if ($lastProduct->getData('visibility') ==1) {
                            $lastProductUrl = "Not Visible Individually";
                        } else {
                            $lastProductUrl = $lastProduct->getProductUrl();
                        }

                        $arr['last_products_bought_product_1_url'] = $lastProductUrl;
                    }
                } elseif ($property == 'last_products_bought_product_2_image_url') {
                    if (!empty($lastThreeProducts['second_last'])) {
                        $imageUrl = $this->getImageUrl($secondLastProduct->getImage());
                        $arr['last_products_bought_product_2_image_url'] = $imageUrl;
                    }
                } elseif ($property == 'last_products_bought_product_2_name') {
                    if (!empty($lastThreeProducts['second_last'])) {
                        $arr['last_products_bought_product_2_name'] = $lastThreeProducts['second_last']['name'];
                    }
                } elseif ($property == 'last_products_bought_product_2_price') {
                    if (!empty($lastThreeProducts['second_last'])) {
                        $arr['last_products_bought_product_2_price'] =
                            $lastThreeProducts['second_last']['base_row_total_incl_tax']-
                            $lastThreeProducts['second_last']['base_discount_amount'];
                    }
                } elseif ($property == 'last_products_bought_product_2_url') {
                    if (!empty($lastThreeProducts['second_last'])) {
                        if ($secondLastProduct->getData('visibility') ==1) {
                            $secondProductUrl = "Not Visible Individually";
                        } else {
                            $secondProductUrl = $secondLastProduct->getProductUrl();
                        }

                        $arr['last_products_bought_product_2_url'] = $secondProductUrl;
                    }
                } elseif ($property == 'last_products_bought_product_3_image_url') {
                    if (!empty($lastThreeProducts['third_last'])) {
                        $imageUrl = $this->getImageUrl($thirdLastProduct->getImage());
                        $arr['last_products_bought_product_3_image_url'] = $imageUrl;
                    }
                } elseif ($property == 'last_products_bought_product_3_name') {
                    if (!empty($lastThreeProducts['third_last'])) {
                        $arr['last_products_bought_product_3_name'] = $lastThreeProducts['third_last']['name'];
                    }
                } elseif ($property == 'last_products_bought_product_3_price') {
                    if (!empty($lastThreeProducts['third_last'])) {
                        $arr['last_products_bought_product_3_price'] =
                            $lastThreeProducts['third_last']['base_row_total_incl_tax']-
                            $lastThreeProducts['third_last']['base_discount_amount'];
                    }
                } elseif ($property == 'last_products_bought_product_3_url') {
                    if (!empty($lastThreeProducts['third_last'])) {
                        if ($thirdLastProduct->getData('visibility') ==1) {
                            $thirdProductUrl = "Not Visible Individually";
                        } else {
                            $thirdProductUrl = $thirdLastProduct->getProductUrl();
                        }

                        $arr['last_products_bought_product_3_url'] = $thirdProductUrl;
                    }
                } elseif ($property == 'last_order_status') {
                    if ($lastOrderStatus != "") {
                        $arr['last_order_status'] = $lastOrderStatus;
                    }
                } elseif ($property == 'last_order_fulfillment_status') {
                    if ($lastOrderStatus != "") {
                        $arr['last_order_fulfillment_status'] = $lastOrderStatus;
                    }
                } elseif ($property == 'last_order_tracking_number') {
                    if (isset($trackingNumber)) {
                        $arr['last_order_tracking_number'] = $trackingNumber;
                    }
                } elseif ($property == 'last_order_tracking_url') {
                    if (isset($carrierCode)) {
                        $arr['last_order_tracking_url'] = $carrierCode;
                    }
                } elseif ($property == 'last_order_shipment_date') {
                    if ($lastOrderShipDate != "") {
                        $arr['last_order_shipment_date'] = $this->getDateStamp($lastOrderShipDate);
                    }
                } elseif ($property == 'last_order_order_number') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['last_order_order_number'] = (int)$customerLastOrder->getId();
                    }
                } elseif ($property == 'total_number_of_current_orders') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['total_number_of_current_orders'] = $totalOrders;
                    }
                } elseif ($property == 'last_categories_bought') {
                    if (!empty($lastCategoriesBought)) {
                        $arr['last_categories_bought'] = implode(';', array_values($lastCategoriesBought));
                    }
                } elseif ($property == 'categories_bought') {
                    if (!empty($categoriesBought)) {
                        $arr['categories_bought'] = implode(';', array_values($categoriesBought));
                    }
                } elseif ($property == 'last_skus_bought') {
                    if (!empty($lastSkusBought)) {
                        $arr['last_skus_bought'] = implode(';', array_values($lastSkusBought));
                    }
                } elseif ($property == 'skus_bought') {
                    if (!empty($skusBought)) {
                        $arr['skus_bought'] = implode(';', array_values($skusBought));
                    }
                } elseif ($property == 'total_value_of_orders') {
                    if ($totalOrderValues > 0) {
                        $arr['total_value_of_orders'] = $totalOrderValues;
                    }
                } elseif ($property == 'average_order_value') {
                    if (isset($ordersList[$customer->getId()]) && $totalOrderValues > 0) {
                        $arr['average_order_value'] = $totalOrderValues / $totalOrders;
                    }
                } elseif ($property == 'total_number_of_orders') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['total_number_of_orders'] = $totalOrders;
                    }
                } elseif ($property == 'first_order_value') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['first_order_value'] = (float)$customerFirstOrder->getData('grand_total');
                    }
                } elseif ($property == 'first_order_date') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['first_order_date'] = $this->getDateStamp($customerFirstOrder->getCreatedAt());
                    }
                } elseif ($property == 'last_order_value') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['last_order_value'] = (float)$customerLastOrder->getData('grand_total');
                    }
                } elseif ($property == 'last_order_date') {
                    if (isset($ordersList[$customer->getId()])) {
                        $arr['last_order_date'] = $this->getDateStamp($customerLastOrder->getCreatedAt());
                    }
                } elseif ($property == 'average_days_between_orders') {
                    if (isset($ordersList[$customer->getId()])) {
                        $avgDays = $this->getAvgDays($customerFirstOrder, $customerLastOrder, $totalOrders);

                        $arr['average_days_between_orders'] = $avgDays;
                    }
                } elseif ($property == 'order_recency_rating') {
                    if (isset($ordersList[$customer->getId()])) {
                        $recencyDateDiff = $this->getRecencyDateDiff($customerLastOrder);
                        $recencyRating = $this->getRating('recency', $recencyDateDiff);
                        $arr['order_recency_rating'] = $recencyRating;
                    } else {
                        $arr['order_recency_rating'] = 1;
                    }
                } elseif ($property == 'order_frequency_rating') {
                    if (isset($ordersList[$customer->getId()])) {
                        $frequencyRating = $this->getRating('frequency', $totalOrders);
                        $arr['order_frequency_rating'] = $frequencyRating;
                    } else {
                        $arr['order_frequency_rating'] = 1;
                    }
                } elseif ($property == 'monetary_rating') {
                    if (isset($ordersList[$customer->getId()]) && $totalOrderValues > 0) {
                        $monetaryRating = $this->getRating('monetary', $totalOrderValues);
                        $arr['monetary_rating'] = $monetaryRating;
                    } else {
                        $arr['monetary_rating'] = 1;
                    }
                } elseif ($property == 'account_creation_date') {
                    $arr['account_creation_date'] = $this->getDateStamp($customer->getCreatedAt());
                } else {
                    if ($data = $customer->getData($property)) {
                        $arr[$property] = $data;
                    }
                }
            }

            $synData[] = [
                "integratorObjectId" => self::PREFIX . $customer->getId(),
                "action" => "UPSERT",
                "changeOccurredTimestamp" => strtotime(date('Y-m-d H:i:s ', time())) . '000',
                "propertyNameToValues" => $arr
            ];
        }
        return $synData;
    }
    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->scopeConfigManager->getValue('hub_integration/hubspot_integration/enable');
    }

    /**
     * @return mixed
     */
    public function getAbandonedCartTime()
    {
        return $this->scopeConfigManager
            ->getValue('hub_integration/hubspot_integration/abandoned_cart_time');
    }

    public function getProductIds()
    {
        $productIds = $this->product->create()->getAllIds();
        return $productIds;
    }

    public function getContactIds()
    {
        $customers = $this->customer->create()->getAllIds();
        $updatedOrders = $this->sale->create()->getColumnValues('customer_id');
        $allIds = array_merge($customers, $updatedOrders);
        return $allIds;
    }

    public function getDealIds()
    {
        $deals = $this->sale->create()->getAllIds();
        $quotes =  $carts = $this->quote->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->addFieldToFilter('items_count', ['gt' => 0])
            ->addFieldToFilter('customer_id', ['notnull' => true])
            ->getAllIds();

        return ['ORDER' => $deals, 'QUOTE' => $quotes, 'COUNT' => count($deals) + count($quotes)];
    }

    public function getLineItemIds()
    {
        $quoteItems = $this->quoteItemCollectionFactory->create()->getAllIds();
        return $quoteItems;
    }

    /**
     * @param $job
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function syncBulkData($job)
    {
        if (!empty($job) && isset($job['object_type'], $job['id_json']) && !empty($job['id_json'])) {
            $synData = [];
            $objectType = $job['object_type'];
            $ids = json_decode($job['id_json'], true);
            if ($objectType == 'PRODUCT') {
                $products = $this->product->create()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', ['in' => $ids])
                    ->addMediaGalleryData();
                $synData = $this->setProduct($products);
            } elseif ($objectType == 'CONTACT') {
                $synData = $this->setContact($ids);
            } elseif ($objectType == 'DEAL') {
                if (isset($ids['ORDER'])) {
                    $synData = $this->setOrder($ids['ORDER']);
                } elseif (isset($ids['QUOTE'])) {
                    $synData = $this->setQuote($ids['QUOTE']);
                }
            } elseif ($objectType == 'LINE_ITEM') {
                $synData = $this->setLineItem($ids);
            }
            $this->connectionManager->syncMessages(json_encode($synData), $objectType);
        }

        return true;
    }

    public function setOrder($ids)
    {
        $synData = [];
        $deals = $this->sale->create()
            ->addFieldToFilter('entity_id', ['in' => $ids]);
        $properties = $this->properties->getGroupProperty('DEAL');
        foreach ($deals as $deal) {
            /** @var \Magento\Sales\Model\Order $deal */
            $wonReason = '';
            $lostReason = '';
            $closedDate = time();
            if ($deal->getState() == 'new') {
                $state = 'checkout_completed';
                $closedDate = strtotime('+ 15 days');
            } elseif ($deal->getState() == 'processing') {
                $state = 'processed';
                $closedDate = strtotime('+ 7 days');
            } elseif ($deal->getState() == 'complete') {
                $state = 'shipped';
                $wonReason = 'Order Shipped and Invoiced';
            } else {
                $state = 'cancelled';
                $lostReason = 'Order Refunded or Canceled';
            }
            $shipmentIds = implode(
                ', ',
                $this->shipment->create()->addFieldToFilter('order_id', $deal->getId())->getAllIds()
            );
            $dealName = "order_" . $deal->getId();
            $data = [
                "deal_stage" => $state,
                "deal_name" => $dealName,
                "pipeline" => "75e28846-ad0d-4be2-a027-5e1da6590b98",
                "amount" => $deal->getGrandTotal(),
                "shipment_ids" => $shipmentIds,
                "contact_ids" => self::PREFIX . $deal->getCustomerId(),
                "close_date" => $closedDate * 1000,
                "closed_won_reason" => $wonReason,
                "closed_lost_reason" => $lostReason
            ];
            $deal->addData($data);
            $synData[] = $this->attributes($properties, $deal, $deal->getQuoteId());
        }

        return $synData;
    }

    public function setQuote($ids)
    {
        $synData = [];
        $properties = $this->properties->getGroupProperty('DEAL');
        $carts = $this->quote->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->addFieldToFilter('items_count', ['gt' => 0])
            ->addFieldToFilter('customer_id', ['notnull' => true])
            ->addFieldToFilter('entity_id', [
                'in' => $ids
            ]);

        $baseUrl = $this->urlHelper->getUrl('checkout/cart', ['_secure' => true]);
        foreach ($carts as $cart) {
            /** @var \Magento\Quote\Model\Quote $cart */
            $data = [
                "deal_stage" => 'checkout_abandoned',
                "deal_name" => "quote_" . $cart->getId(),
                "pipeline" => "75e28846-ad0d-4be2-a027-5e1da6590b98",
                "amount" => $cart->getGrandTotal(),
                "abandoned_cart_url" => $baseUrl,
                "contact_ids" => self::PREFIX . $cart->getCustomerId()
            ];
            $cart->addData($data);
            $synData[] = $this->attributes($properties, $cart, $cart->getId());
        }
        return $synData;
    }

    public function setLineItem($ids)
    {
        $synData = [];
        $lineProperty = $this->properties->getGroupProperty('LINE_ITEM');
        $items = $this->quoteItemCollectionFactory->create()
            ->addFieldToFilter('item_id', ['in' => $ids]);

        foreach ($items as $item) {
            if (!$item->isDeleted() && !$item->getParentItemId()) {
                $item->addData([
                    'deal_id' => self::PREFIX . $item->getQuoteId(),
                    'product_id' => self::PREFIX . $item->getProductId()
                ]);
                $synData[] = $this->attributes($lineProperty, $item, $item->getItemId());
            }
        }

        return $synData;
    }
}
