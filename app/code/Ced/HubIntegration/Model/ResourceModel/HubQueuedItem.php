<?php
/**
 * Created by PhpStorm.
 * User: cedcoss
 * Date: 16/9/19
 * Time: 7:04 PM
 */

namespace Ced\HubIntegration\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class HubQueuedItem extends AbstractDb
{
    /**
     * @return void
     */
    public function _construct()
    {
        //hub_ced_items is table and id is primary key of this table
        $this->_init('hub_ced_queued_items', 'id');
    }
}