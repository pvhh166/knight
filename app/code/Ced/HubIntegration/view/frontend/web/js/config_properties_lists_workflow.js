define([
    'jquery', 'bootstrapBundle'
], function (jQuery) {
    return function (config) {
        jQuery(document).ready(function () {

            jQuery('#modal-button').click();

            jQuery.ajax({
                url: config.PropertiesAjaxUrl,
                data: {form_key: window.FORM_KEY},
                type: 'POST'
            }).done(function (transport) {
                console.log(transport);
                var appendHtml = '<li style="font-size:15px" id="hubspot-property" class="success">' +
                    '<img style="width:5%" id="statusImage" src="'+config.SuccessImg+'">   ' + transport['message'] + '</li>';
                jQuery('#hubspot-responses').append(appendHtml);
                jQuery('.progress-bar').css('width', '34%');

                jQuery.ajax({
                    url: config.ListsAjaxUrl,
                    data: {form_key: window.FORM_KEY},
                    type: 'POST'
                }).done(function (transport) {
                    console.log(transport);
                    var appendHtml = '<li style="font-size:15px"  id="hubspot-segments" class="success">' +
                        '<img style="width:5%" src="'+config.SuccessImg+'">   ' + transport['message'] + '</li>';
                    jQuery('#hubspot-responses').append(appendHtml);
                    jQuery('.progress-bar').css('width', '67%');

                    jQuery.ajax({
                        url: config.WorkflowAjaxUrl,
                        data: {form_key: window.FORM_KEY},
                        type: 'POST'
                    }).done(function (transport) {
                        console.log(transport);
                        var appendHtml = '<li style="font-size:15px" id="hubspot-workflow" class="success">' +
                            '<img style="width:5%" id="statusImage" src="'+config.SuccessImg+'">   ' + transport['message'] + '</li>';
                        jQuery('#hubspot-responses').append(appendHtml);
                        jQuery('.progress-bar').css('width', '100%');
                      //  closeWindow();

                    });

                });

            });


            function closeWindow() {
                setTimeout(function () {
                    window.close();
                }, 3000);
            }
        });
    }
});
