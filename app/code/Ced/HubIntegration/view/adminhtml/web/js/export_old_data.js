/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

define(['jquery', 'jquery/ui', 'accordion'], function (jQuery) {
    return function (config) {
        let exportType = ['ProductToExport', 'CustomerToExport', 'DealToExport', 'LineItemToExport'];
        let total = {
            ProductToExport: config.productsTotal,
            CustomerToExport: config.contactsTotal,
            DealToExport: config.dealsTotal,
            LineItemToExport: config.lineItemsTotal
        };

        let i=0;


        //call on load
        sendRequest(i,0);

        function sendRequest(i,id) {
            document.getElementById(exportType[i]).style.display = "block";

            let totalRecords = parseInt(total[exportType[i]]);
            let countOfSuccess = 0;
            // let id = 0;
            let liFinished = document.getElementById(exportType[i] + '_liFinished');
            let updateStatus = document.getElementById(exportType[i] + '_updateStatus');
            let updateRow = document.getElementById(exportType[i] + '_updateRow');
            let statusImage = document.getElementById(exportType[i] + '_statusImage');

            if (totalRecords <= 0) {
                statusImage.src = config.errorImg;
                updateRow.innerHTML = "No Batch(s) Found.";
                if (i<3) {
                    i++;
                    sendRequest(i,0);
                }
                return false;
            }

            updateStatus.innerHTML = (id + 1) + ' Of ' + totalRecords + ' Processing';
            var request = jQuery.ajax({
                type: "GET",
                url: config.getAjaxUrl,
                data: {batchid: id, task: exportType[i]},
                success: function (data) {
                    console.log(data);
                    // let json = JSON.parse(data.responseText);
                    let json = data;
                    id++;
                    if (json.hasOwnProperty('success')) {
                        countOfSuccess++;
                        let span = document.createElement('li');
                        span.innerHTML =
                            '<img src="'+config.successImg+'"><span>' +
                            json.success + '</span>';
                        span.id = 'id-' + exportType[i] + '_' + id;
                        updateRow.parentNode.insertBefore(span, updateRow);
                    } else {
                        let span = document.createElement('li');
                        if (json.hasOwnProperty('error')) {
                            if (json.hasOwnProperty('messages')) {
                                //  errorMessage = parseErrors(json.messages);
                                errorMessage = json.messages;
                            } else if (json.hasOwnProperty('exceptionmessages')) {
                                errorMessage = json.exceptionmessages;
                            }
                            let heading = '<span>' +
                                '<img src="'+config.errorImg+'" width="1%"> ' + json.error + '.</span>';

                            span.innerHTML = '<div class="batch-container">' +
                                '<div data-role="collapsible" style="cursor: pointer;" id="' + exportType[i] + '_' + id + '" ' +
                                'onclick="enableAccordion(this.id);">' +
                                '<div data-role="trigger">' + heading + '</div></div>' +
                                '<div class="well" data-role="content" id="content-' + exportType[i] + '_' + id + '" style="display:none ;">'
                                + errorMessage.errors + '</div></div>';

                            // span.innerHTML = errorTemplate;
                            span.id = 'id-' + exportType[i] + '_' + id;
                            updateRow.parentNode.insertBefore(span, updateRow);
                        }

                    }
                },

                error: function () {
                    id++;
                    let span = document.createElement('li');
                    span.innerHTML = '<img src="'+config.errorImg+'" width="1%"><span>Something went wrong </span>';
                    span.id = 'id-' + exportType[i] + '_' + id;
                    span.style = 'background-color:#FDD';
                    updateRow.parentNode.insertBefore(span, updateRow);
                },

                complete: function () {
                    if (id < totalRecords) {
                        sendRequest(i,id);
                    } else {
                        statusImage.src = config.successImg;
                        let span = document.createElement('li');
                        span.innerHTML =
                            '<img src="'+config.successImg+'" width="1%">' +
                            '<span id="' + exportType[i] + '_updateStatus">' +
                            totalRecords + ' batch(s) processed.' + '</span>';
                        liFinished.parentNode.insertBefore(span, liFinished);
                        document.getElementById(exportType[i] + "_liFinished").style.display = "block";
                        updateStatus.innerHTML = (id) + ' of ' + totalRecords + ' Processed.';

                        if (i<3) {
                            i++;
                            sendRequest(i,0);
                        }
                    }
                },
                dataType: "json"
            });
        }






    }

});

function enableAccordion(accordionId) {
    let acc = document.getElementById("content-" + accordionId);
    if (acc.style.display === "block") {
        acc.style.display = "none";
    } else {
        acc.style.display = "block";
    }

}


