/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

define([
    'jquery',
    'prototype'
], function(jQuery){
     return function (config) {
         function generateCode() {
             left = (jQuery(window).width() / 2) - (550 / 2);
             top = (jQuery(window).height() / 2) - (550 / 2);
             window.open(config.AuthorizeUrl, 'popup', "width=550, height=550, top=" + top + ", left=" + left);
         }

         jQuery('#authorize_button').click(function () {
             generateCode();
         });
     }
});