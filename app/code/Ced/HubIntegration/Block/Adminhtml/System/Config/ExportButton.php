<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template;

class ExportButton extends \Magento\Config\Block\System\Config\Form\Field
{
    public $connectionManager;

    /** @var \Magento\Config\Model\ResourceModel\Config  */
    public $resourceConfig;

    /** @var \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory  */
    public $queuedCollectionFactory;

    const REDIRECT_URI = 'admin/hubintegration/config/exportOldData';

    public function __construct(
        Template\Context $context,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory $queuedCollectionFactory,
        array $data = []
    ) {
        $this->connectionManager = $connectionManager;
        $this->resourceConfig = $resourceConfig;
        $this->queuedCollectionFactory = $queuedCollectionFactory;
        parent::__construct($context, $data);
    }

    protected $_template = 'Ced_HubIntegration::system/config/exportbutton.phtml';

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * @return string
     */
    public function getAjaxSyncUrl()
    {
        return $this->getUrl('hubintegration/config/exportOldData');
      //  return $this->_storeManager->getStore()->getBaseUrl().$this::REDIRECT_URI;
    }

    /**
     * @return mixed
     */
    public function getConnectionStatus()
    {
        return $this->_scopeConfig->getValue('hub_integration/hubspot_integration/connection_established');
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getButtonHtml()
    {
        $buttonTitle = $this->getExportButtonTitle();

            $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                ->setData(
                    [
                        'id' => 'export_button',
                        'label' => __($buttonTitle),
                    ]
                );
        return $button->toHtml();
    }

    public function getExportButtonTitle()
    {
        $queueCollection = $this->queuedCollectionFactory->create();
        if ($queueCollection->getSize()) {
            return 'View Export Status';
        } else {
            return 'Export Old Data';
        }
    }
}
