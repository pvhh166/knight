<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Block\Adminhtml\System\Config;

class ExportOldData extends \Magento\Backend\Block\Widget\Container
{
    public $productsBatch;
    public $contactsBatch;
    public $dealsBatch;
    public $lineItemsBatch;
    public $registry;
    public $queuedItemCollectionFactory;
    public $connectionManager;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory $queuedItemCollectionFactory,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->registry = $registry;
        $this->queuedItemCollectionFactory = $queuedItemCollectionFactory;
        $this->connectionManager = $connectionManager;
        $this->productsBatch = $this->registry->registry('PRODUCT');
        $this->contactsBatch = $this->registry->registry('CONTACT');
        $this->dealsBatch = $this->registry->registry('DEAL');
        $this->lineItemsBatch = $this->registry->registry('LINE_ITEM');
        $this->_getAddButtonOptions();
    }

    public function _getAddButtonOptions()
    {
        $splitButtonOptions = [
            'label' => __('Back'),
            'class' => 'action-secondary',
            'onclick' => "setLocation('" . $this->getCreateUrl() . "')"
        ];
        $this->buttonList->add('add', $splitButtonOptions);

        $availableBatches = $this->getAvailableBatches();
        if ($availableBatches->getSize()) {
            $disable = true;
        } else {
            $disable = false;
        }

        $ExportButtonOptions = [
            'label' => __('Export'),
            'class' => 'action-secondary',
            'disabled' => $disable,
            'onclick' => "setLocation('" . $this->getAjaxUrl() . "')"
        ];
        $this->buttonList->add('export', $ExportButtonOptions);
    }

    /**
     * @return mixed
     */
    public function getConnectionStatus()
    {
        return $this->_scopeConfig->getValue('hub_integration/hubspot_integration/connection_established');
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('adminhtml/system_config/edit/section/hub_integration');
    }

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('*/config/exportOldData', ['start' => true]);
    }

    public function getPercentage()
    {
        $result = ['CONTACT' => ['percent' => 100], 'PRODUCT' => ['percent' => 100],
            'DEAL' => ['percent' => 100], 'LINE_ITEM' => ['percent' => 100]];
        $availableBatches = $this->getAvailableBatches();

        $availableBatches->getSelect()->columns(['batch_id_count' => new \Zend_Db_Expr('SUM(batch_id_count)'),
            'total_count' => new \Zend_Db_Expr('total_count')])->group('object_type');

        foreach ($availableBatches->getData() as $availBatch) {
            if (isset($availBatch['object_type']) &&
                isset($availBatch['total_count']) && isset($availBatch['batch_id_count'])) {
                $result[$availBatch['object_type']] = [
                    'percent' => (($availBatch['total_count'] - $availBatch['batch_id_count']) /
                            $availBatch['total_count']) * 100];
            }
        }

        return $result;
    }

    public function getAvailableBatches()
    {
        $availableBatches = $this->queuedItemCollectionFactory->create()
            ->addFieldToSelect(['batch_id_count', 'total_count', 'object_type']);
        return $availableBatches;
    }

    public function showProgressBar()
    {

        $show = $this->queuedItemCollectionFactory->create();
        if ($show->getSize()) {
            return true;
        }
        return false;
    }
}
