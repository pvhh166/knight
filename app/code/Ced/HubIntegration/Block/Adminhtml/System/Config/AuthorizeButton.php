<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template;

class AuthorizeButton extends \Magento\Config\Block\System\Config\Form\Field
{
    public $connectionManager;

    /** @var \Magento\Config\Model\ResourceModel\Config  */
    public $resourceConfig;

    const REDIRECT_URI = 'hubintegration/config/getcode';

    public function __construct(
        Template\Context $context,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        array $data = []
    ) {
        $this->connectionManager = $connectionManager;
        $this->resourceConfig = $resourceConfig;
        parent::__construct($context, $data);
    }

    protected $_template = 'Ced_HubIntegration::system/config/authorize.phtml';

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * @return string
     */
    public function getAjaxSyncUrl()
    {
        return $this->hubspotFetchOauthCode();
    }

    /**
     * @return string
     */
    public function hubspotFetchOauthCode()
    {
        $url = 'https://app.hubspot.com/oauth/authorize';
        $hubSpotUrl = $this->connectionManager->addQueryArguments([
            'client_id' => \Ced\HubIntegration\Helper\ConnectionManager::DEVELOPER_CLIENT_ID,
            'scope' => 'oauth%20contacts%20integration-sync%20files%20timeline',
            'optional_scope' => 'automation',
            'redirect_uri' => $this->redirectUrl()
        ], $url);

        return $hubSpotUrl;
    }

    /**
     * @return string
     */
    public function redirectUrl()
    {
        if ($this->_storeManager->getStore()->isFrontUrlSecure()) {
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB, true)
                . self::REDIRECT_URI;
        } else {
            $baseUrl =  $this->_storeManager->getStore()->getBaseUrl() .self::REDIRECT_URI;
            if (strpos($baseUrl, 'https') === false) {
                $baseUrl = str_replace("http", "https", $baseUrl);
            }
        }
        $this->resourceConfig->saveConfig(
            'hub_integration/hubspot_integration/redirect_url',
            $baseUrl,
            'default',
            0
        );
        return $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getConnectionStatus()
    {
        return $this->_scopeConfig->getValue('hub_integration/hubspot_integration/connection_established');
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getButtonHtml()
    {
        if ($this->getConnectionStatus()==true) {
            $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                ->setData(
                    [
                        'id' => 'authorize_button',
                        'label' => __('Re-Authenticate'),
                    ]
                );
        } else {
            $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                ->setData(
                    [
                        'id' => 'authorize_button',
                        'label' => __('Authenticate'),
                    ]
                );
        }
        return $button->toHtml();
    }
}
