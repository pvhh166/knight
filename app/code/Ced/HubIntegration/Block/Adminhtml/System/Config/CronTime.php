<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Block\Adminhtml\System\Config;

class CronTime implements \Magento\Framework\Option\ArrayInterface
{
    const ONCE_IN_A_DAY = '*/60 */24 * * *';
    const TWICE_A_DAY  = '*/60 */12 * * *';
    const FOUR_TIMES_A_DAY = '*/60 */6 * * *';
    const EVERY_HOUR = '*/60 */1 * * *';
    const EVERY_FIVE_MINUTES = "*/5 * * * *";
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['label'=> __('Once In A Day'),'value'=>$this::ONCE_IN_A_DAY],
            ['label'=> __('Twice A Day'), 'value'=>$this::TWICE_A_DAY],
            ['label'=> __('Four Times A Day'), 'value'=>$this::FOUR_TIMES_A_DAY],
            ['label'=> __('Every Hour'), 'value'=>$this::EVERY_HOUR]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [0 => __('Once In A Day'), 1 => __('Twice A Day'), 2 => __('Four Times A Day'), 3 => __('Every Hour')];
    }
}
