<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class ConnectionManager extends AbstractHelper
{
    const DEVELOPER_HAPIKEY = 'a642aea7-738e-4fd0-8557-380a27de27f4';
  //  const DEVELOPER_HAPIKEY = '000000000000';
    const DEVELOPER_APP_ID = '163225';
    const DEVELOPER_CLIENT_ID = '8d71cc2f-4fbc-4bde-819d-8c17d3fa69fa';
    const DEVELOPER_SECRET_ID = 'a3650c54-908c-4bcf-a996-f6907b357607';

    /**
     * Base url of hubSpot api.
     */
    private $baseUrl = "https://api.hubapi.com/";

    private $userId;

    /** @var \Magento\Config\Model\ResourceModel\Config */
    public $resourceConfig;

    /** @var \Magento\Framework\App\Cache\TypeListInterface */
    public $cache;

    public $connectionEstablished;

    public $exportStartTime = 0;

    public $exportEndTime = 0;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    public function __construct(
        Context $context,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cache
    ) {
        $this->resourceConfig = $resourceConfig;
        $this->cache = $cache;
        $this->scopeConfig = $context->getScopeConfig();
        $this->connectionEstablished = $this->getHubConfig('connection_established');
        parent::__construct($context);
    }

    /**
     * Validating OAuth 2.0.
     * @since 1.0.0
     */
    public function hubspotValidateOauthToken()
    {
        if ($this->getHubConfig('oauth_is_valid')) {
            if ($this->isAccessTokenExpired()) {
                return $this->hubspotRefreshToken();
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Refreshing access token from refresh token.
     * @return bool
     */
    public function hubspotRefreshToken()
    {
        $endpoint = 'oauth/v1/token';
        $refresh_token = $this->getHubConfig('refresh_token');
        $data = [
            'grant_type' => 'refresh_token',
            'client_id' => self::DEVELOPER_CLIENT_ID,
            'client_secret' => self::DEVELOPER_SECRET_ID,
            'refresh_token' => $refresh_token,
            'redirect_uri' => $this->getHubConfig('redirect_url')
        ];
        $body = http_build_query($data);
        return $this->hubspotOauthPostApi($endpoint, $body, 'refresh');
    }

    /**
     * Fetching access token from code.
     * @param $code
     * @return bool
     */
    public function hubspotFetchAccessTokenFromCode($code)
    {
        $endpoint = 'oauth/v1/token';
        $data = [
            'grant_type' => 'authorization_code',
            'client_id' => self::DEVELOPER_CLIENT_ID,
            'client_secret' => self::DEVELOPER_SECRET_ID,
            'code' => $code,
            'redirect_uri' => $this->getHubConfig('redirect_url')
        ];
        $this->createLog('', '', 'fetch  access token:-'.json_encode($data));

        $body = http_build_query($data);
        return $this->hubspotOauthPostApi($endpoint, $body, 'access');
    }

    /**
     * post api for oauth access and refresh token.
     * @param $endpoint
     * @param $body
     * @param $action
     * @return bool
     */
    public function hubspotOauthPostApi($endpoint, $body, $action)
    {
        $headers = ['Content-Type: application/x-www-form-urlencoded;charset=utf-8'];
        if ($action == 'refresh') {
            $access_token = $this->getHubConfig('access_token');
            $headers = ['Authorization: Bearer ' . $access_token];
        }
        $response = $this->_post($endpoint, $body, $headers);

        if ($response) {
            $status_code = $response['status_code'];
            $api_body = json_decode($response['response'], true);
            $message = '';
            if ($status_code == 200) {
                if (isset($api_body['refresh_token']) && isset($api_body['access_token']) && $api_body['expires_in']) {
                    $accountInfo = $this->getRefreshTokenInfo($api_body['refresh_token']);
                    $hubId = 0;
                    if ($accountInfo !== null) {
                        $hubUserData = json_decode($accountInfo, true);
                        if (isset($hubUserData['hub_id'])) {
                            $hubId = $hubUserData['hub_id'];
                        }
                    }
                    $keyValues = [
                        'access_token' => $api_body['access_token'],
                        'refresh_token' => $api_body['refresh_token'],
                        'token_expiry' => time() + $api_body['expires_in'] - 5,
                        'account_info' => $accountInfo,
                        'hub_id' => $hubId,
                        'oauth_is_valid' => true
                    ];
                    foreach ($keyValues as $key => $value) {
                        $this->setHubConfig($key, $value);
                    }
                    return true;
                }
            } elseif ($status_code == 400) {
                $message = $api_body['message'];
            } elseif ($status_code == 403) {
                $message = 'You are forbidden to use this scope';
            }
            $this->createLog($message, $endpoint, $response);
        }
        $this->setHubConfig('oauth_valid', false);
        return false;
    }

    /**
     * Fetching refresh token Information.
     * @param $token
     * @return mixed|null
     */
    public function getRefreshTokenInfo($token)
    {
        $endpoint = 'oauth/v1/refresh-tokens/'.$token;
        $headers = ['Content-Type: application/json'];
        $data = $this->_get($endpoint, $headers);
        if (isset($data['response']) && $data['status_code'] == 200) {
            return $data['response'];
        }
        return null;
    }

    /**
     * Get requests to HubSpot.
     * @param $endpoint
     * @param $headers
     * @return array
     */
    private function _get($endpoint, $headers)
    {
        $url = $this->baseUrl . $endpoint;
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, false);
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        $this->createLog($status_code, $url, $response);
        return ['status_code' => $status_code, 'response' => $response, 'errors' => $curl_errors];
    }

    /**
     * send post and format the response to HubSpot.
     * @param $endpoint
     * @param $post_params
     * @param $headers
     * @return array
     */
    private function _post($endpoint, $post_params, $headers)
    {
        $url = $this->baseUrl . $endpoint;
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        $this->createLog($status_code, $url, $response, $post_params);
        return ['status_code' => $status_code, 'response' => $response, 'errors' => $curl_errors];
    }

    /**
     * send post and format the response to HubSpot.
     * @param $endpoint
     * @param $post_params
     * @param $headers
     * @return array
     */
    private function _put($endpoint, $post_params, $headers)
    {
        $url = $this->baseUrl . $endpoint;
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        $this->createLog($status_code, $url, $response, $post_params);
        return ['status_code' => $status_code, 'response' => $response, 'errors' => $curl_errors];
    }

    /**
     * create customer group on HubSpot.
     * @param $groupDetails
     * @return array
     */
    public function createGroup($groupDetails)
    {
        if (is_array($groupDetails)) {
            if (isset($groupDetails['name']) && isset($groupDetails['displayName'])) {
                $url = 'properties/v1/contacts/groups';
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $groupDetails = json_encode($groupDetails);
                return $this->_post($url, $groupDetails, $headers);
            }
        }
    }

    /**
     * create customer property on HubSpot.
     * @param $propDetails
     * @return array
     */
    public function createProperty($propDetails)
    {
        if (is_array($propDetails)) {
            if (isset($propDetails['name']) && isset($propDetails['groupName'])) {
                $url = 'properties/v1/contacts/properties';
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $propDetails = json_encode($propDetails);
                return $this->_post($url, $propDetails, $headers);
            }
        }
    }

    /**
     * update customer property on HubSpot.
     * @param $propDetails
     * @return array
     */
    public function updateProperty($propDetails)
    {
        if (is_array($propDetails)) {
            if (isset($propDetails['name']) && isset($propDetails['groupName'])) {
                $url = '/properties/v1/contacts/properties/named/' . $propDetails['name'];
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $propDetails = json_encode($propDetails);
                return $this->_put($url, $propDetails, $headers);
            }
        }
    }

    /**
     * Get list of smart list
     * @return array
     */
    public function getStaticList()
    {
        $lists = [];
        $lists["select"] = "--Please Select a Static List--";
        $url = '/contacts/v1/lists/static?count=250';
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $access_token
        ];
        $response = $this->_get($url, $headers);
        if (isset($response['response'])) {
            $response = json_decode($response['response']);
        }
        if (!empty($response->lists)) {
            foreach ($response->lists as $single_list) {
                if (isset($single_list->name) && isset($single_list->listId)) {
                    $lists[$single_list->listId] = $single_list->name;
                }
            }
        }
        return $lists;
    }

    /**
     * @param $email
     * @param $list_id
     * @return array
     */
    public function listEnrollment($email, $list_id)
    {
        if (!empty($email) && !empty($list_id)) {
            $url = '/contacts/v1/lists/' . $list_id . '/add';
            $this->hubspotValidateOauthToken();
            $access_token = $this->getHubConfig('access_token');
            $headers = [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $access_token
            ];
            $emails = [];
            $emails[] = $email;
            $request = ["emails" => $emails];
            $request = json_encode($request);
            return $this->_post($url, $request, $headers);
        }
    }

    /**
     * create list on HubSpot.
     * @param $listDetails
     * @return array
     */
    public function createList($listDetails)
    {
        if (is_array($listDetails)) {
            if (isset($listDetails['name'])) {
                $url = 'contacts/v1/lists';
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $listDetails = json_encode($listDetails);
                return $this->_post($url, $listDetails, $headers);
            }
        }
    }

    /**
     * create workflow on hubspot.
     * @return array
     * @since 1.0.0
     */
    public function createWorkflow($workflow_details)
    {
        if (is_array($workflow_details)) {
            if (isset($workflow_details['name'])) {
                $url = 'automation/v3/workflows';
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $workflow = json_encode($workflow_details);
                return $this->_post($url, $workflow, $headers);
            }
        }
    }

    /**
     * Get list of All workFlows
     * @return array
     */
    public function getWorkflows()
    {
        $workflows = [];
        $workflows["select"] = "--Please Select a Workflow--";
        $url = '/automation/v3/workflows';
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $access_token
        ];
        $response = $this->_get($url, $headers);
        if (isset($response['response'])) {
            $response = json_decode($response['response']);
        }
        if (!empty($response->workflows)) {
            foreach ($response->workflows as $single_workflow) {
                if (isset($single_workflow->name) && isset($single_workflow->id)) {
                    $workflows[$single_workflow->id] = $single_workflow->name;
                }
            }
        }
        return $workflows;
    }

    /**
     * @param $email
     * @param $workflow_id
     * @return array
     */
    public function workflowEnrollment($email, $workflow_id)
    {
        if (!empty($email) && !empty($workflow_id)) {
            $url = 'automation/v2/workflows/' . $workflow_id . '/enrollments/contacts/' . $email;
            $this->hubspotValidateOauthToken();
            $access_token = $this->getHubConfig('access_token');
            $headers = [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $access_token
            ];
            return $this->_post($url, [], $headers);
        }
    }

    /**
     * getting all HubSpot properties.
     * @since 1.0.0
     */
    public function getAllHubspotProperties()
    {
        $url = '/properties/v1/contacts/properties';
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $access_token
        ];
        return $this->_get($url, $headers);
    }

    /**
     * create deal group on HubSpot.
     * @param $dealGroups
     * @return array
     */
    public function createDealGroup($dealGroups)
    {
        $url = '/properties/v1/deals/groups/';
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $access_token
        ];
        $dealGroups = json_encode($dealGroups);
        return $this->_post($url, $dealGroups, $headers);
    }

    /**
     * create deal property on HubSpot.
     * @param $propDetails
     * @return array
     */
    public function createDealProperty($propDetails)
    {
        if (is_array($propDetails)) {
            if (isset($propDetails['name']) && isset($propDetails['groupName'])) {
                $url = '/properties/v1/deals/properties/';
                $this->hubspotValidateOauthToken();
                $access_token = $this->getHubConfig('access_token');
                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $access_token
                ];
                $propDetails = json_encode($propDetails);
                return $this->_post($url, $propDetails, $headers);
            }
        }
    }

    /**
     *  create/update property mapping on HubSpot.
     * @param $setting_details
     * @return array
     */
    public function insertOrUpdateSetting($setting_details)
    {
        $url = 'extensions/ecomm/v1/settings?hapikey=' . self::DEVELOPER_HAPIKEY . '&appId=' . self::DEVELOPER_APP_ID;
        $headers = ['Content-Type: application/json'];
        return $this->_put($url, $setting_details, $headers);
    }

    /**
     * Get property mapping on hubspot.
     *
     * @since 1.0.0
     */
    public function getMappSetting()
    {
        $url = 'extensions/ecomm/v1/settings?hapikey=' . self::DEVELOPER_HAPIKEY . '&appId=' . self::DEVELOPER_APP_ID;
        $headers = ['Content-Type: application/json'];
        return $this->_get($url, $headers);
    }

    /**
     * * Sync Message on hubSpot for object CONTACT, DEAL, PRODUCT, or LINE_ITEM.
     * @param $setting_details
     * @param $objectType
     * @return array
     */
    public function syncMessages($setting_details, $objectType)
    {
        $url = 'extensions/ecomm/v1/sync-messages/' . $objectType;
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $access_token
        ];
        return $this->_put($url, $setting_details, $headers);
    }

    /**
     * Initialize import setting to set the callback uri. Importing still not completed.
     * @param $setting_details
     * @return array
     */
    public function setImportSetting($setting_details)
    {
        $url = 'extensions/ecomm/v1/import-settings?hapikey='.self::DEVELOPER_HAPIKEY.
            '&appId=' . self::DEVELOPER_APP_ID;
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json'
        ];
        return $this->_put($url, $setting_details, $headers);
    }

    /**
     * Get import setting of the callback uri.
     * @since 1.0.0
     */
    public function getImportSetting()
    {
        $url = 'extensions/ecomm/v1/import-settings?hapikey='.self::DEVELOPER_HAPIKEY.
            '&appId=' . self::DEVELOPER_APP_ID;
        $this->hubspotValidateOauthToken();
        $access_token = $this->getHubConfig('access_token');
        $headers = [
            'Content-Type: application/json'
        ];
        return $this->_get($url, $headers);
    }

    /**
     * check if access token is expired.
     * @return boolean [description]
     */
    public function isAccessTokenExpired()
    {
        $get_expiry = $this->getHubConfig('token_expiry');
        if ($get_expiry) {
            $current_time = time();
            if ($current_time > $get_expiry) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $keyValue
     */
    public function setHubConfig($field, $value)
    {
        if (!$field) {
            return null;
        }
        $path = 'hub_integration/hubspot_integration/' . $field;
        return $this->resourceConfig->saveConfig($path, $value, 'default', 0);
    }

    /**
     * Get HubSpot Related Setting From Core Config Data
     * @param $key
     * @param null $defaultValue
     * @return mixed|null
     */
    public function getHubConfig($field)
    {
        if (!$field) {
            return null;
        }
        $path = 'hub_integration/hubspot_integration/' . $field;
        return $this->scopeConfig->getValue($path);
    }

    /**
     * create log of requests.
     *
     * @param  string $status_code log status_code.
     * @param  string $url acceptable url.
     * @param  string $response
     * @since 1.0.0
     */
    public function createLog($status_code, $url, $response, $params = [])
    {
        $allowedStatus = [200, 202, 204];
        if (!in_array($status_code, $allowedStatus)) {
            $response = is_array($response) ? json_encode($response) : $response;
            $params = is_array($params) ? json_encode($params) : $params;
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hub_api.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $log = "message:" . $status_code . PHP_EOL .
                "URL: " . $url . PHP_EOL .
                "Params:" . $params . PHP_EOL .
                "Response: " . $response . PHP_EOL;
            $logger->info($log);
        }
    }

    /**
     * Add the array of arguments in url to prepare query string.
     *
     * @param  array $args arguments array.
     * @param  string $url request_uri.
     * @return string $query query_string.
     * @since 1.0.0
     */
    public function addQueryArguments($args, $url)
    {
        $query = [];
        foreach ($args as $key => $value) {
            $query[] = $key . '=' . $value;
        }
        if (!empty($query)) {
            $response = $url . '?' . implode('&', $query);
        } else {
            $response = $url;
        }
        return $response;
    }

    public function cleanCache()
    {
        $cacheType = [
            \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
            \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
            'config',
        ];
        foreach ($cacheType as $cache) {
            $this->cache->cleanType($cache);
        }
    }
}
