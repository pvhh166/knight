<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Cron;

class ExportToHubspot
{

    const CHUNK_SIZE = 400;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var \Ced\HubIntegration\Model\DataSync
     */
    public $dataSync;

    /**
     * @var \Ced\HubIntegration\Helper\ConnectionManager
     */
    public $connectionManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Ced\HubIntegration\Model\DataSync $dataSync,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->dataSync = $dataSync;
        $this->connectionManager = $connectionManager;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if ($this->scopeConfig->getValue('hub_integration/hubspot_integration/enable')) {
            $cronTime = $this->scopeConfig->getValue('hub_integration/export_settings/cron_time');
            if ($cronTime == \Ced\HubIntegration\Block\Adminhtml\System\Config\CronTime::ONCE_IN_A_DAY) {
                $time = 1440+20;
            } elseif ($cronTime == \Ced\HubIntegration\Block\Adminhtml\System\Config\CronTime::TWICE_A_DAY) {
                $time = 720+20;
            } elseif ($cronTime == \Ced\HubIntegration\Block\Adminhtml\System\Config\CronTime::FOUR_TIMES_A_DAY) {
                $time = 360+20;
            } elseif ($cronTime == \Ced\HubIntegration\Block\Adminhtml\System\Config\CronTime::EVERY_HOUR) {
                $time = 60+20;
            } elseif ($cronTime == \Ced\HubIntegration\Block\Adminhtml\System\Config\CronTime::EVERY_FIVE_MINUTES) {
                $time = 5+20;
            }
            try {
                $this->syncAllData($time);
            } catch (\Exception $e) {
                $this->connectionManager->createLog('', 'exception in Data Sync:', $e->getMessage());
            }
        }
    }

    /**
     * @param $time
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function syncAllData($time)
    {
        $products = $this->dataSync->getProduct($time);
        $productsArray = array_chunk($products, self::CHUNK_SIZE);
        foreach ($productsArray as $productKey => $productValue) {
              $this->connectionManager->syncMessages(json_encode($productValue), 'PRODUCT');
        }

        $contacts = $this->dataSync->getContact($time);
        $contactsArray = array_chunk($contacts, self::CHUNK_SIZE);
        foreach ($contactsArray as $contactKey => $contactValue) {
            $this->connectionManager->syncMessages(json_encode($contactValue), 'CONTACT');
        }

        $deals = $this->dataSync->getDeal($time);
        if (isset($deals[0])) {
            $deals = $deals[0];
        }
        $dealsArray  = array_chunk($deals['DEAL'], self::CHUNK_SIZE);
        foreach ($dealsArray as $dealKey => $dealvalue) {
            $this->connectionManager->syncMessages(json_encode($dealvalue), 'DEAL');
        }

        $lineItemsArray = array_chunk($deals['LINE_ITEM'], self::CHUNK_SIZE);
        foreach ($lineItemsArray as $lineItemKey => $lineItemValue) {
            $this->connectionManager->syncMessages(json_encode($lineItemValue), 'LINE_ITEM');
        }

        $deletedItems = $this->dataSync->getDeletedItem();
        if (isset($deletedItems[0])) {
              $deletedItems = $deletedItems[0];
        }
        foreach ($deletedItems as $objectType => $values) {
            $deletedItemsArray = array_chunk($values, self::CHUNK_SIZE);
            foreach ($deletedItemsArray as $deletedItemKey => $deletedItemValue) {
                $this->connectionManager->syncMessages(json_encode($deletedItemValue), $objectType);
            }
        }
    }
}
