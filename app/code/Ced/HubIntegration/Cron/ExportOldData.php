<?php
/**
 * Created by PhpStorm.
 * User: cedcoss
 * Date: 18/9/19
 * Time: 3:06 PM
 */

namespace Ced\HubIntegration\Cron;

class ExportOldData
{
    const EXPORT_BATCH = 5;

    public $objectType = [0 => 'PRODUCT', 1 => 'CONTACT', 2 => 'DEAL', 3 => 'LINE_ITEM'];

    public $queuedCollectionFactory;

    public $dataSync;

    public $connectionManager;

    public $dateTime;

    public function __construct(
        \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory $queuedCollectionFactory,
        \Ced\HubIntegration\Model\DataSync $dataSync,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->dataSync = $dataSync;
        $this->queuedCollectionFactory = $queuedCollectionFactory;
        $this->connectionManager = $connectionManager;
        $this->dateTime = $dateTime;
    }

    public function execute()
    {
        try {
            $batches = [];
            $batches = $this->retrieveBatches($batches, 0, self::EXPORT_BATCH);
            if (!empty($batches)) {
                foreach ($batches as $job) {
                    try {
                        $this->dataSync->syncBulkData($job);
                    } catch (\Exception $e) {
                        $this->connectionManager->createLog('400', 'exception: '.$e->getMessage(), $job);
                    }
                }
            }
            $queueBatches = $this->queuedCollectionFactory->create();
            if (!$queueBatches->getSize()) {
                $endTime = $this->dateTime->Date('Y-m-d H:i:s');
                $this->connectionManager->setHubConfig('bulk_export_end_time', $endTime);
            }
        } catch (\Exception $e) {
            $this->connectionManager->createLog('400', 'exception in Bulk export cron ', $e->getMessage());
        }

        return true;
    }

    public function retrieveBatches($batches, $objectKey, $count)
    {
        $queueCollection = $this->queuedCollectionFactory->create()->setCurPage(1)->setPageSize($count)
            ->addFieldToFilter('object_type', ['eq' => $this->objectType[$objectKey]])
            ->setOrder('batch_id', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);

        if (empty($batches)) {
            $batches = $queueCollection->getData();
        } else {
            $batches = array_merge($batches, $queueCollection->getData());
        }

        $queueCollection->walk('delete');

        if (count($batches) < self::EXPORT_BATCH && $objectKey < 3) {
            $batches = $this->retrieveBatches($batches, ++$objectKey, self::EXPORT_BATCH - count($batches));
        }
        return $batches;
    }
}