<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   Ced_HubIntegration
 * @author    CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright Copyright CEDCOMMERCE(http://cedcommerce.com/)
 * @license   http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Observer;

class ProductDeleteBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Ced\HubIntegration\Model\HubItemFactory
     */
    private $hubItem;

    /**
     * @param \Ced\HubIntegration\Model\HubItemFactory $hubItemFactory
     */
    public function __construct(
        \Ced\HubIntegration\Model\HubItemFactory $hubItemFactory
    ) {
        $this->hubItem = $hubItemFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product && $product->getId()) {
            $this->hubItem->create()
                ->setData('object_type', 'PRODUCT')
                ->setData('object_id', $product->getId())
                ->save();
        }
    }
}
