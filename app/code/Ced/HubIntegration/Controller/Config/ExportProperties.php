<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Controller\Config;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ExportProperties extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Ced\HubIntegration\Helper\ConnectionManager
     */
    public $connectionManager;

    /**
     * @var \Ced\HubIntegration\Helper\Properties
     */
    public $properties;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;

    /**
     * @var \Ced\HubIntegration\Cron\ExportToHubspot
     */
    public $exportToHubspot;

    public function __construct(
        Context $context,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        \Ced\HubIntegration\Helper\Properties $properties,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ced\HubIntegration\Cron\ExportToHubspot $exportToHubspot
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->connectionManager = $connectionManager;
        $this->properties = $properties;
        $this->exportToHubspot = $exportToHubspot;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $response = $this->resultJsonFactory->create();
        $params = $this->getRequest()->getParam('task');
        if ($params == 'group_property') {
            $result = $this->createGroup();
             $response->setData($result);
        } elseif ($params == 'list') {
            $result = $this->createList();
            $response->setData($result);
        } elseif ($params == 'workflow') {
            $result = $this->createWorkFlow();
            $response->setData($result);
        } else {
            $response->setData([
                'success' => false,
                'message' => 'Some problem occured.',
                'data'=> []
            ]);
        }
        return $response;
    }

    /**
     * @return array
     */
    private function createGroup()
    {
        $groups = $this->properties->getContactGroups();
        foreach ($groups as $key => $value) {
            $this->connectionManager->createGroup($value);
            $properties = $this->properties->getContactProperty($value['name']);
            foreach ($properties as $key1 => $value1) {
                $value1['groupName'] = $value['name'];
                $this->connectionManager->createProperty($value1);
            }
        }

        return [
            'success' => true,
            'message' => 'Contact Group & Property Created Successfully',
            'data'=> []
        ];
    }

    /**
     * @return array
     */
    private function createList()
    {
        $lists = $this->properties->getAllLists();
        foreach ($lists as $key => $value) {
            $response = $this->connectionManager->createList($value);
            if (isset($response['status_code']) && ($response['status_code'] == 200)) {
                $response = isset($response['response']) ? $response['response'] : "";
                if (!empty($response)) {
                    $response = json_decode($response, true);
                    if ($response['listId']) {
                        $path = 'hubspot/lists/'.$value['name'];
                        $this->properties->setUserOption($path, $response['listId']);
                        $this->connectionManager->cleanCache();
                    }
                }
            }
        }
        return [
            'success' => true,
            'message' => 'Contact Lists created Successfully',
            'data'=> []
        ];
    }

    /**
     * @return array
     */
    private function createWorkFlow()
    {
        $workflow = $this->properties->getWorkFlowNames();
        foreach ($workflow as $key => $name) {
            if (!$this->properties->getUserOption($key)) {
                $workFlowDetail = $this->properties->getAllWorkFlows($name);
                $response = $this->connectionManager->createWorkflow($workFlowDetail);
                if (isset($response['status_code']) && ($response['status_code'] == 200)) {
                    $response = isset($response['response']) ? $response['response'] : "";
                    if (!empty($response)) {
                        $response = json_decode($response, true);
                        if ($response['id']) {
                            $this->properties->setUserOption($key, $response['id']);
                            $this->connectionManager->cleanCache();
                        }
                    }
                }
            }
        }
        return [
            'success' => true,
            'message' => 'Work Flows created Successfully',
            'data'=> []
        ];
    }
}
