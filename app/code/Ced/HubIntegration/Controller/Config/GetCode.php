<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Controller\Config;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class GetCode extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Ced\HubIntegration\Helper\ConnectionManager
     */
    public $connectionManager;

    /**
     * @var PageFactory
     */
    public $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager
    ) {
        $this->connectionManager = $connectionManager;
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['code'])) {
            $code = $params['code'];
            $response =  $this->connectionManager->hubspotFetchAccessTokenFromCode($code);
            if ($response) {
                $this->connectionManager->setHubConfig('connection_established', 1);
                $this->connectionManager->cleanCache();
                $this->connectionManager->connectionEstablished = 1;
            } else {
                $this->connectionManager->setHubConfig('connection_established', 0);
                $this->connectionManager->cleanCache();
                $this->connectionManager->connectionEstablished = 0;
            }
        } else {
            $this->connectionManager->setHubConfig('connection_established', 0);
            $this->connectionManager->cleanCache();
            $this->connectionManager->connectionEstablished = 0;
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set('Hubspot Magento Authentication');
        return $resultPage;
    }
}
