<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_HubIntegration
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\HubIntegration\Controller\Adminhtml\Config;

use Magento\Backend\App\Action;

class ExportOldData extends \Magento\Backend\App\Action
{
    const TIME = 0;
    const CHUNK_SIZE = 400;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    public $session;

    /**
     * @var \Magento\Framework\Registry
     */
    public $registry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     * @var \Ced\HubIntegration\Helper\ConnectionManager
     */
    public $connectionManager;

    /**
     * @var \Ced\HubIntegration\Model\DataSync
     */
    public $dataSync;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    public $resource;

    /**
     * @var \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory
     */
    public $queuedItemCollectionFactory;

    public $dateTime;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ced\HubIntegration\Helper\ConnectionManager $connectionManager,
        \Ced\HubIntegration\Model\DataSync $dataSync,
        \Magento\Framework\App\ResourceConnection $resource,
        \Ced\HubIntegration\Model\ResourceModel\HubQueuedItem\CollectionFactory $queuedItemCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $context->getSession();
        $this->registry = $registry;
        $this->connectionManager = $connectionManager;
        $this->dataSync = $dataSync;
        $this->resource = $resource;
        $this->queuedItemCollectionFactory = $queuedItemCollectionFactory;
        $this->dateTime = $dateTime;
        parent::__construct($context);
    }

    public function execute()
    {
        $startTime = $this->connectionManager->getHubConfig('bulk_export_start_time');
        $endTime =  $this->connectionManager->getHubConfig('bulk_export_end_time');
        $request = $this->getRequest()->getParam('start');
        $getQueuedItems = $this->queuedItemCollectionFactory->create()->getItems();
        if ($request && empty($getQueuedItems)) {
            $insertMultiple = [];
            try {
                $products = $this->dataSync->getProductIds();
                $totalProductIdsCount = count($products);
                $productsArray = array_chunk($products, self::CHUNK_SIZE);
                foreach ($productsArray as $productBatchIds => $productIds) {
                    $insertMultiple[] = [
                        'object_type' => 'PRODUCT',
                        'batch_id' => $productBatchIds,
                        'batch_id_count' => count($productIds),
                        'total_count' => $totalProductIdsCount,
                        'id_json' => json_encode($productIds)
                    ];
                }
            } catch (\Exception $e) {
                $this->connectionManager->createLog('400', 'exception', $e->getMessage());
            }

            try {
                $contacts = $this->dataSync->getContactIds();
                $totalConatctsCount = count($contacts);
                $contactsArray = array_chunk($contacts, self::CHUNK_SIZE);
                foreach ($contactsArray as $contactBatchIds => $contactIds) {
                    $insertMultiple[] = [
                        'object_type' => 'CONTACT',
                        'batch_id' => $contactBatchIds,
                        'batch_id_count' => count($contactIds),
                        'total_count' => $totalConatctsCount,
                        'id_json' => json_encode($contactIds)
                    ];
                }
            } catch (\Exception $e) {
                $this->connectionManager->createLog('400', 'exception', $e->getMessage());
            }

            try {
                $deals = $this->dataSync->getDealIds();
                $totalDealsCount = 0;
                if (isset($deals['COUNT'])) {
                    $totalDealsCount = $deals['COUNT'];
                }
                $dealBatchIds = 0;
                if (isset($deals['ORDER'])) {
                    $dealsArray = array_chunk($deals['ORDER'], self::CHUNK_SIZE);
                    foreach ($dealsArray as $dealBatchIds => $dealIds) {
                        $insertMultiple[] = [
                            'object_type' => 'DEAL',
                            'batch_id' => $dealBatchIds++,
                            'batch_id_count' => count($dealIds),
                            'total_count' => $totalDealsCount,
                            'id_json' => json_encode(['ORDER' => $dealIds])
                        ];
                    }
                }
                if (isset($deals['QUOTE'])) {
                    $quotesArray = array_chunk($deals['QUOTE'], self::CHUNK_SIZE);
                    foreach ($quotesArray as $quoteBatchIds => $quoteIds) {
                        $insertMultiple[] = [
                            'object_type' => 'DEAL',
                            'batch_id' => $dealBatchIds++,
                            'batch_id_count' => count($quoteIds),
                            'total_count' => $totalDealsCount,
                            'id_json' => json_encode(['QUOTE' => $quoteIds])
                        ];
                    }
                }
            } catch (\Exception $e) {
                $this->connectionManager->createLog('400', 'exception', $e->getMessage());
            }

            try {
                $lineItems = $this->dataSync->getLineItemIds();
                $lineItemsCount = count($lineItems);
                $lineItemsArray = array_chunk($lineItems, self::CHUNK_SIZE);
                foreach ($lineItemsArray as $lineItemBatchIds => $lineItemIds) {
                    $insertMultiple[] = [
                        'object_type' => 'LINE_ITEM',
                        'batch_id' => $lineItemBatchIds,
                        'batch_id_count' => count($lineItemIds),
                        'total_count' => $lineItemsCount,
                        'id_json' => json_encode($lineItemIds)
                    ];
                }
            } catch (\Exception $e) {
                $this->connectionManager->createLog('400', 'exception', $e->getMessage());
            }

            try {
                $tableName = $this->resource->getTableName('hub_ced_queued_items');
                $this->resource->getConnection()->insertMultiple($tableName, $insertMultiple);
            } catch (\Exception $e) {
                $this->connectionManager->createLog('400', 'exception', $e->getMessage());
            }

            $startTime = $this->dateTime->Date('Y-m-d H:i:s');
            $endTime = 0;
            $this->connectionManager->setHubConfig('bulk_export_start_time', $startTime);
            $this->connectionManager->setHubConfig('bulk_export_end_time', $endTime);
        }

        $msg = "Please click on 'Export' to start.";
        if ($startTime) {
            $msg = "Export started on ".$startTime;
            if ($endTime) {
                $msg .= " and completed on ".$endTime.". Click on 'Export' to start again.";
            }
        }
        $this->messageManager->addNoticeMessage('Bulk export will be done through cron.');
        $this->messageManager->addNoticeMessage($msg);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ced_HubIntegration::hubspot');
        $resultPage->getConfig()->getTitle()->prepend(__('Export Old Data'));
        return $resultPage;
    }
}
