<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\CustomViewcart\Block\Cart\Item;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Shopping cart item render block
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 *
 * @method \Magento\Checkout\Block\Cart\Item\Renderer setProductName(string)
 * @method \Magento\Checkout\Block\Cart\Item\Renderer setDeleteUrl(string)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Renderer extends \Magento\Checkout\Block\Cart\Item\Renderer
{
    /**
     * Get list of all options for product
     *
     * @return array
     * @codeCoverageIgnore
     */

    /**
     * Get list of all options for product
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function getOptionList()
    {
        $serializer = ObjectManager::getInstance()->get(Json::class);
        $item = $this->getItem();
        $product = $item->getProduct();
        if($product->getTypeId() == 'super') {
            $addOptions = $item->getOptionByCode('super_custom_option');
            if ($addOptions) {
                $options = $serializer->unserialize($addOptions->getValue());
                return $options;
            }
        }
        return $this->getProductOptions();
    }
}
