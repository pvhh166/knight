<?php

namespace Brandsmith\CustomViewcart\CustomerData;

use Magento\Quote\Model\Quote\Item;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

class DefaultItemPlugin {

    protected $_checkoutSession;
    protected $helperTrafficLightStock;

    public function __construct (
        \Magento\Checkout\Model\Session $_checkoutSession,
        \Brandsmith\TrafficLightStock\Helper\Data $helperTrafficLightStock
    ) {
        $this->_checkoutSession = $_checkoutSession;
        $this->helperTrafficLightStock = $helperTrafficLightStock;
    }

    public function afterGetItemData($subject, $result)
    {
        $type = $result['product_type'];

        if($type == 'configurable') {
            $productRepository = ObjectManager::getInstance()->get('\Magento\Catalog\Model\ProductRepository');
            $productObj = $productRepository->get($result['product_sku']);

            $imageHelper  = ObjectManager::getInstance()->get('\Magento\Catalog\Helper\Image');
            $width = 75;
            $height = 75;
            $imageUrl = $imageHelper->init($productObj, 'product_page_image_small')
                ->constrainOnly(FALSE)
                ->keepAspectRatio(TRUE)
                ->keepFrame(FALSE)
                ->resize($width, $height)
                ->getUrl();
            $result['product_image']['src'] = $imageUrl;
        }
 
        if($type == 'super') {
            $escaper = ObjectManager::getInstance()->get(\Magento\Framework\Escaper::class);
            $productImage = '';
            $itemQuote = null;
            $cartData = $this->_checkoutSession->getQuote()->getAllVisibleItems();
            foreach($cartData as $item) {
                if($item->getId() == $result['item_id']) {
                    $itemQuote = $item;
                    break;
                }
            }
            $options = array();
            if($itemQuote) { 
                $serializer = ObjectManager::getInstance()->get(Json::class);
                $addOptions = $itemQuote->getOptionByCode('super_custom_option');
                if ($addOptions) {
                    $addOptions = $serializer->unserialize($addOptions->getValue());

                    $productImage = $addOptions['super_product_img'];
                    foreach ($addOptions['super_attributes_defined'] as $_option) {
                        $options_temp = array();
                        if (is_array($_option['option_selected'])) {
                            $options_super = array();
                            foreach ($_option['option_selected'] as $label => $value) {
                                $option = array
                                (
                                    'label' => $label,
                                    'value' => $value
                                );
                                $options_super[] = $option;
                            }
                            $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                            if (isset($qty_arr[$_option['product_id']])) {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                            } else {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                            }

                            $options_temp['customoptions'] = $options_super;
                        } else {
                            $options_super = array();
                            $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                            if (isset($qty_arr[$_option['product_id']])) {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                            } else {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                            }

                            $options_temp['customoptions'] = $options_super;
                        }
                        $productRepository = ObjectManager::getInstance()->get('\Magento\Catalog\Model\ProductRepository');
                        $productSimple = $productRepository->get($_option['product_simple_sku']);
                        $options_temp['product_name'] = $_option['product_name'];
                        $options_temp['product_simple_sku'] = $_option['product_simple_sku'];
                        $options_temp['product_simple_id'] = $productSimple->getId();
                        $options_temp['product_thumbnail_img'] = $_option['product_thumbnail_img'];

                        if ($itemQuote->getHasChildren()) {
                            foreach ($itemQuote->getChildren() as $child) {
                                if ($child->getSku() == $_option['product_simple_sku']) {
                                    $options_temp['gauge_first']  = $child->getDescription();
                                    $gaugeLast = $this->helperTrafficLightStock->getGauge($productSimple->getId(),false,$this->_checkoutSession->getQuote()->getCustomerId());
                                    $options_temp['gauge_last'] = $gaugeLast;
                                    break;
                                }                                
                            }
                        }
                        $options[] = $options_temp;

                    }
                }
            }
            if(!empty($productImage)) {
                $result['product_image']['src'] = $productImage;
            }
            if(is_array($options)) {
                $result['options'] = $options;
            }
        }

        return $result;
    }

}