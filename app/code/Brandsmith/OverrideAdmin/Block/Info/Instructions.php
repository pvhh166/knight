<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\OverrideAdmin\Block\Info;

/**
 * Block for Bank Transfer payment generic info
 *
 * @api
 * @since 100.0.2
 */
class Instructions extends \Magento\Payment\Block\Info\Instructions
{

    /**
     * @var string
     */
    protected $_template = 'Brandsmith_OverrideAdmin::info/instructions.phtml';

}
