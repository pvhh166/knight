<?php
namespace Brandsmith\OverrideAdmin\Block\Info;

class Checkmo extends \Magento\OfflinePayments\Block\Info\Checkmo
{
    /**
     * @var string
     */
    protected $_template = 'Brandsmith_OverrideAdmin::info/checkmo.phtml';

}