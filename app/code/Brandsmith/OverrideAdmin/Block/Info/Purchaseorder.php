<?php
namespace Brandsmith\OverrideAdmin\Block\Info;

class Purchaseorder extends \Magento\Payment\Block\Info\Instructions
{
    /**
     * @var string
     */
    protected $_template = 'Brandsmith_OverrideAdmin::info/purchaseorder.phtml';

}