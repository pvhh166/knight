<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Brandsmith\OverrideAdmin\Block\Order\Item\Renderer;

/**
 * Order item render block
 *
 * @api
 * @since 100.0.2
 */
class DefaultRenderer extends \Magento\Sales\Block\Order\Item\Renderer\DefaultRenderer
{
    /**
     * @return array
     */
    public function getItemOptions()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('quote_item_option');
        $sql = "Select * FROM " . $tableName . " WHERE `item_id` = ". $this->getOrderItem()->getQuoteItemId();
        $quoteOptionsArr = $connection->fetchAll($sql);

        $productType = $this->getOrderItem()->getProductType();
        $addOptions = [];
        if(is_array($quoteOptionsArr)) {
            foreach ($quoteOptionsArr as $item) {
                if($item['code'] == 'super_custom_option') {
                    // $superOptions[] = $item['value'];
                    $addOptions = json_decode($item['value'], true);
                }
            }
        }
        $result = [];
        $options = $this->getOrderItem()->getProductOptions();
        if ($options) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
            if($productType == 'super') {
                if(!empty($quoteOptionsArr)) {
                    $options = array();
                    if ($addOptions) {
                        $productImage = $addOptions['super_product_img'];
                        foreach ($addOptions['super_attributes_defined'] as $_option) {
                            $options_temp = array();
                            if (is_array($_option['option_selected'])) {
                                $options_super = array();
                                foreach ($_option['option_selected'] as $label => $value) {
                                    $option = array
                                    (
                                        'label' => $label,
                                        'value' => $value
                                    );
                                    $options_super[] = $option;
                                }
                                $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                                if(isset($qty_arr[ $_option['product_id']])){
                                    $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                                }else {
                                    $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                                }

                                $options_temp['customoptions'] = $options_super;
                            }else {
                                $options_super = array();
                                $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                                if(isset($qty_arr[ $_option['product_id']])){
                                    $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                                }else {
                                    $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                                }

                                $options_temp['customoptions'] = $options_super;
                            }
                            $options_temp['product_name'] = $_option['product_name'];
                            $options_temp['product_simple_sku'] = $_option['product_simple_sku'];
                            $options_temp['product_thumbnail_img'] = $_option['product_thumbnail_img'];
                            $options_temp['super_product_img'] = $addOptions['super_product_img'];
                            $options[] = $options_temp;
                        }
                    }
                   // print_r($options);die;
                    return $options;
                }
            }
        }
        return $result;
    }
}
