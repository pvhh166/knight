<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\OverrideAdmin\Model\Order\Pdf\Items\Invoice;

/**
 * Sales Order Invoice Pdf default items renderer
 */
class DefaultInvoice extends \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice
{

    /**
     * Draw item line
     *
     * @return void
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $lines = [];

       // print_r($item->getOrderItem()->getProductType());die;

        // draw Product name
        $lines[0] = [['text' => $this->string->split($this->filterManager->stripTags(html_entity_decode($item->getName())), 35, true, true),'font' => 'bold', 'feed' => 35]];

        // draw SKU
        $lines[0][] = [
            'text' => $this->string->split($this->getSku($item), 17),
            'feed' => 290,
            'align' => 'right',
        ];

        // draw QTY
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 435, 'align' => 'right'];

        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 375;
        $feedSubtotal = $feedPrice + 190;
        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right'];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right'];
                $i++;
            }
            // draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
            ];
            // draw Subtotal
            $lines[$i][] = [
                'text' => $priceData['subtotal'],
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
            ];
            $i++;
        }

        // draw Tax
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getTaxAmount()),
            'feed' => 495,
            'font' => 'bold',
            'align' => 'right',
        ];


        // custom options


        if($item->getOrderItem()->getProductType() != 'super') {
            $options = $this->getItemOptions();
            if ($options) {
                foreach ($options as $option) {
                    // draw options label
                    /*$lines[][] = [
                        'text' => $this->string->split($this->filterManager->stripTags($option['label']), 40, true, true),
                        'font' => 'italic',
                        'feed' => 35,
                    ];*/

                    $val = "";
                    if ($option['value']) {
                        if (isset($option['print_value'])) {
                            $printValue = $option['print_value'];
                        } else {
                            $printValue = $this->filterManager->stripTags($option['value']);
                        }
                        $values = explode(', ', $printValue);

                        foreach ($values as $value) {
                            //$lines[][] = ['text' => $this->string->split($value, 30, true, true), 'feed' => 40];
                            $val = $val.$value. " ";
                        }
                    }
                    $lines[][] = [
                        'text' => $this->string->split($this->filterManager->stripTags($option['label']).": ".$val , 40, true, true),
                        'font' => 'italic',
                        'feed' => 35,
                    ];

                }
            }
        }else {
            $options = $this->getSuperItemOptions();
            if ($options) {
                foreach ($options as $option) {
                    $lines[][] = [
                        'text' => $this->string->split($this->filterManager->stripTags($option['product_name']), 40, true, true),
                        'font' => 'bold',
                        'feed' => 35,
                    ];
                    $lines[][] = ['text' => $this->string->split($option['product_simple_sku'], 30, true, true),'font' => 'normal', 'feed' => 35];
                    foreach ($option['customoptions'] as $opt) {
                        if($opt['label'] == 'Component Qty') {
                            $lines[][] = ['text' => $this->string->split($opt['label'].": ". $opt['value'], 30, true, true),'font' => 'normal', 'feed' => 35];
                        }else {
                            $lines[][] = ['text' => $this->string->split($opt['label'].": ". $opt['value'], 30, true, true),'font' => 'italic', 'feed' => 35];
                        }
                    }
                    $lines[][] = [
                        'text' => ' ',
                        'font' => 'normal',
                        'feed' => 35,
                        'height' => 8
                    ];
                }
            }
        }

        $lineBlock = ['lines' => $lines, 'height' => 15];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }

    public function getSuperItemOptions() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('quote_item_option');
        $sql = "Select * FROM " . $tableName . " WHERE `item_id` = ". $this->getItem()->getOrderItem()->getQuoteItemId();
        $quoteOptionsArr = $connection->fetchAll($sql);

        $addOptions = [];
        if(is_array($quoteOptionsArr)) {
            foreach ($quoteOptionsArr as $item) {
                if($item['code'] == 'super_custom_option') {
                    // $superOptions[] = $item['value'];
                    $addOptions = json_decode($item['value'], true);
                }
            }
        }

        $result = [];
        $options = $this->getItem()->getOrderItem()->getProductOptions();
        if ($options) {
            if(!empty($quoteOptionsArr)) {
                $options = array();
                if ($addOptions) {
                    foreach ($addOptions['super_attributes_defined'] as $_option) {
                        $options_temp = array();
                        if (is_array($_option['option_selected'])) {
                            $options_super = array();
                            foreach ($_option['option_selected'] as $label => $value) {
                                $option = array
                                (
                                    'label' => $label,
                                    'value' => $value
                                );
                                $options_super[] = $option;
                            }
                            $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                            if(isset($qty_arr[ $_option['product_id']])){
                                $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                            }else {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                            }

                            $options_temp['customoptions'] = $options_super;
                        }else {
                            $options_super = array();
                            $qty_arr = isset($addOptions['addon_qty']) ? $addOptions['addon_qty'] : array();
                            if(isset($qty_arr[ $_option['product_id']])){
                                $options_super[] = array('label' => 'Component Qty', 'value' => $qty_arr[$_option['product_id']]);
                            }else {
                                $options_super[] = array('label' => 'Component Qty', 'value' => $_option['product_quantity']);
                            }

                            $options_temp['customoptions'] = $options_super;
                        }
                        $options_temp['product_name'] = $_option['product_name'];
                        $options_temp['product_simple_sku'] = $_option['product_simple_sku'];
                        $options_temp['product_thumbnail_img'] = $_option['product_thumbnail_img'];
                        $options_temp['super_product_img'] = $addOptions['super_product_img'];
                        $options[] = $options_temp;
                    }
                }
                return $options;
            }
        }
        return $result;
    }
}
