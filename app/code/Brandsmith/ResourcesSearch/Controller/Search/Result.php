<?php

/**

 * Copyright © 2017 Brandsmith. All rights reserved.

 * See COPYING.txt for license details.

 */

namespace Brandsmith\ResourcesSearch\Controller\Search;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;

class Result extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $resultPage = $this->resultPageFactory->create();
        $output = [];
        $output['error'] = 0;
        $output['messages'] = '';
        $output['html'] = '';

        $html = $resultPage->getLayout()
            ->createBlock('Brandsmith\ResourcesSearch\Block\Resources\Attachments')
            ->setTemplate('Brandsmith_ResourcesSearch::result.phtml')
            ->setRequestDefault($params)
            ->toHtml();

        $output['html'] = $html;

        $result->setData($output);
        return $result;
    }

}

