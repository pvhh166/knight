<?php

namespace Brandsmith\ResourcesSearch\Block\Resources;

use Magento\Framework\View\Element\Template;
use Amasty\ProductAttachment\Model\File\ResourceModel\CollectionFactory;
use Amasty\ProductAttachment\Model\File\FileScope\ResourceModel\FileStoreCategory;
use Magento\Framework\Url;

class Attachments extends Template
{
    private $collection;
    /**
     * @var Url
     */
    private $urlBuilder;

    /**
     * @var FileStoreCategory
     */
    private $fileStoreCategory;

    protected $_storeManager;

    public function __construct(
        Template\Context $context,
        CollectionFactory $fileCollectionFactory,
        FileStoreCategory $fileStoreCategory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Url $urlBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->urlBuilder = $urlBuilder;
        $this->fileStoreCategory = $fileStoreCategory;
        $this->_storeManager = $storeManager;
        $this->collection = $fileCollectionFactory->create();
        $this->collection->getSelect()->joinLeft(
            ['file_store' => 'amasty_file_store'],
            '`main_table`.file_id = file_store.file_id',
            ['label'=> 'file_store.label' , 'is_visible'=> 'file_store.is_visible']
        );
    }

    public function getSearchCollection($params) {
        $keyword = isset($params['keyword']) ? $params['keyword'] : "";
        $categoryId = isset($params['category']) ? $params['category'] : 0;
        $store = $this->_storeManager->getStore()->getId();
        $cateIds = [];
        if($categoryId > 0) {
            $fileStoreCategories = $this->fileStoreCategory->getFilesIdsByStoreId($categoryId, $store);
            if(count($fileStoreCategories) > 0) {
                foreach ($fileStoreCategories as $item) {
                    $cateIds[] = $item['file_id'];
                }
            }
        }

        $collection =  $this->collection;
        if(!empty($keyword)){
            //$collection = $collection->addFieldToFilter('label', array('like' => '%'.$keyword.'%'));
            $collection = $collection->addFieldToFilter(['label', 'metakeywords'],[['like' => '%'.$keyword.'%'],['like' => '%'.$keyword.'%']]);
        }
        if(count($cateIds) > 0) {
            $collection = $collection->addFieldToFilter('main_table.file_id', array('in' => $cateIds));
        }

        return $collection;
    }

    public function getDownloadUrl($fileId)
    {
        return $this->urlBuilder->getUrl(
            'amfile/file/download',
            array_merge([
                'file' => $fileId,
                '_nosid' => true,
            ], array())
        );
    }
}
