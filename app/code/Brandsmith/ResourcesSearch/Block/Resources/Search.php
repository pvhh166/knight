<?php

namespace Brandsmith\ResourcesSearch\Block\Resources;

use Magento\Framework\View\Element\Template;
use Amasty\ProductAttachment\Model\File\FileScope\ResourceModel\FileStoreCategory;

class Search extends Template
{
    /**
     * @var FileStoreCategory
     */
    private $fileStoreCategory;

    protected $_storeManager;

    protected $_eavAttribute;

    public function __construct(
        Template\Context $context,
        FileStoreCategory $fileStoreCategory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fileStoreCategory = $fileStoreCategory;
        $this->_storeManager = $storeManager;
        $this->_eavAttribute = $eavAttribute;
    }

    public function getStoreCategoryIds() {
        $attributeId = $this->_eavAttribute->getIdByCode('catalog_category', 'name');
        $store = $this->_storeManager->getStore()->getId();
        $fileStoreCategories = $this->fileStoreCategory->getStoreCategoryIds($store, $attributeId);
        return $fileStoreCategories;
    }
}
