<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Block\Adminhtml\Productrule\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     *
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $_context;

    /**
     * Product Rule Repository
     *
     * @var \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface
     */
    protected $_productruleRepository;

    /**
     * constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productruleRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productruleRepository
    )
    {
        $this->_context = $context;
        $this->_productruleRepository = $productruleRepository;
    }

    /**
     * Return Product Rule ID
     *
     * @return int|null
     */
    public function getProductruleId()
    {
        try {
            return $this->_productruleRepository->getById(
                $this->_context->getRequest()->getParam('productrule_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_context->getUrlBuilder()->getUrl($route, $params);
    }
}
