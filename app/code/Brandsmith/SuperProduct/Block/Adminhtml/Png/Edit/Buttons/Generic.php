<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Block\Adminhtml\Png\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     *
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $_context;

    /**
     * PNG Repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngRepositoryInterface
     */
    protected $_pngRepository;

    /**
     * constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository
    )
    {
        $this->_context = $context;
        $this->_pngRepository = $pngRepository;
    }

    /**
     * Return PNG ID
     *
     * @return int|null
     */
    public function getPngId()
    {
        try {
            return $this->_pngRepository->getById(
                $this->_context->getRequest()->getParam('png_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_context->getUrlBuilder()->getUrl($route, $params);
    }
}
