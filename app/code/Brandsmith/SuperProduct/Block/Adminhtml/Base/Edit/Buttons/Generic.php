<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Block\Adminhtml\Base\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $_context;

    /**
     * Base Repository
     * 
     * @var \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
     */
    protected $_baseRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
    ) {
        $this->_context        = $context;
        $this->_baseRepository = $baseRepository;
    }

    /**
     * Return Base ID
     *
     * @return int|null
     */
    public function getBaseId()
    {
        try {
            return $this->_baseRepository->getById(
                $this->_context->getRequest()->getParam('base_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_context->getUrlBuilder()->getUrl($route, $params);
    }
}
