<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Block\Adminhtml\Pnglayer;
use Magento\Framework\App\RequestInterface;
/**
 * 
 *
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Ajax extends \Magento\Backend\Block\Template
{
	/**
	 * Request instance
	 *
	 * @var \Magento\Framework\App\RequestInterface
	 */
	protected $request;

	protected $productRepository;

	protected $jsonHelper;

	protected $baseAttribute;

	/**
	 * PNG Layer repository
	 * 
	 * @var \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
	 */
	protected $pnglayerRepository;

	/**
	 * PNG Layer factory
	 * 
	 * @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory
	 */
	protected $pnglayerFactory;

	/**
	 * @param RequestInterface $request
	 */
	public function __construct(
	    RequestInterface $request,
	    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	    \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository,
	    \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory $pnglayerFactory,
	    \Magento\Framework\Json\Helper\Data $jsonHelper,
	    \Magento\Backend\Block\Template\Context $context,
	    array $data = [])
	{
	    $this->request = $request;
	    $this->productRepository = $productRepository;
	    $this->pnglayerRepository = $pnglayerRepository;
	    $this->pnglayerFactory     = $pnglayerFactory;
	    $this->jsonHelper = $jsonHelper;
	    parent::__construct($context, $data);
	}

	public function getOption()
	{
	    $postData = $this->request->getPost();
		$product = $this->productRepository->get($postData->sku);
		$baseProduct = $this->productRepository->get($postData->base_sku);
		try {
			$superProduct = $this->productRepository->get($postData->super_sku);
		} catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
			$superProduct = false;
		}
		if ($postData->sku != $product->getSku())
		{
			$skuArr = explode("_",$postData->sku,2);
			$productId = $skuArr[0];
		}
		else
			$productId = $product->getId();
		$attributeCode = $postData->attribute;
	    $options = [];
		if ($attributeCode == $this->getBaseAttribute() && $productId == $baseProduct->getId()) {
			$baseOption = $this->getBaseOption();
			$record = [
				'product_id' => $productId,
				'sku' => $postData->sku,
				'attribute' => $attributeCode,
				'rule_attribute' => null,
				'rule_option' => null,
				'option_hide' => 0,
				'area_hide' => $this->getMultiLayer(),
				'area' => 'Index ' . $this->getMultiLayer(),
				'option' => $this->baseAttribute
			];
			if ($superProduct) {
				foreach ($baseOption as $option) {
					$pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $productId, $attributeCode, $this->getBaseAttribute(), null, $option['value'], $option['value'], null);
					if ($pngLayer->getId()) {
						$id = $option['value'];
						$record['image' . $id] = $pngLayer->getImageData();
						$record['is_default'] = $pngLayer->getIsDefault();
					}
				}
			}
			$options[] = $record;
		} elseif ($product->getTypeId() == "simple") {
			$record = [
				'product_id' => $productId,
				'sku' => $postData->sku,
				'attribute' => $postData->sku,
				'rule_attribute' => null,
				'rule_option' => null,
				'option_hide' => $postData->sku,
				'area_hide' => $this->getMultiLayer(),
				'area' => 'Index ' . $this->getMultiLayer(),
				'option' => $product->getName()
			];
			if ($superProduct) {
				foreach ($this->getBaseOption() as $option) {
					$pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $productId, $postData->sku, $this->getBaseAttribute(), null, $postData->sku, $option['value'], null, $this->getMultiLayer());
					if ($pngLayer->getId()) {
						$id = $option['value'];
						$record['image' . $id] = $pngLayer->getImageData();
						$record['is_default'] = $pngLayer->getIsDefault();
					}
				}
			}
			$options[] = $record;
		} else {
			$data = $product->getTypeInstance()->getConfigurableOptions($product);
			if ($this->getRuleAttribute())
				$ruleOptions = $this->getRuleOptions();
			else $ruleOptions = [['value' => null, 'label' => '']];
			foreach ($ruleOptions as $ruleOpt) {
				$optionsList = [];
				foreach ($data as $attrs) {
					foreach ($attrs as $attribute) {
						if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
							$record = [
								'product_id' => $productId,
								'sku' => $postData->sku,
								'attribute' => $attributeCode,
								'rule_attribute' => $this->getRuleAttribute(),
								'rule_option' => $ruleOpt['value'],
								'option_hide' => $attribute['value_index'],
								'area_hide' => $this->getMultiLayer(),
								'area' => 'Index ' . $this->getMultiLayer(),
								'option' => $ruleOpt['label'] . $attribute['option_title']
							];
							if ($superProduct) {
								foreach ($this->getBaseOption() as $option) {
									$pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $productId, $attributeCode, $this->getBaseAttribute(), $this->getRuleAttribute(), $attribute['value_index'], $option['value'], $ruleOpt['value'], $this->getMultiLayer());
									if ($pngLayer->getId()) {
										$id = $option['value'];
										$record['image' . $id] = $pngLayer->getImageData();
										$record['is_default'] = $pngLayer->getIsDefault();
									}
								}
							}
							$options[] = $record;
							$optionsList[] = $attribute['value_index'];
						}
					}
				}
			}
		}

		return $this->jsonHelper->jsonEncode($options);
	}

	public function getBaseAttribute()
	{
	    $attributeCode = $this->request->getPost()->base_attribute;
	    return $attributeCode;
	}

	public function getBaseOption()
	{
	    $postData = $this->request->getPost();
		$product = $this->productRepository->get($postData->base_sku);
		$attributeCode = $postData->base_attribute;
		$data = $product->getTypeInstance()->getConfigurableOptions($product);
	    $options = [];
		$optionsList = [];
		foreach ($data as $attrs) {
			foreach ($attrs as $attribute) {
				if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
					$options[] = [
						'value' => $attribute['value_index'],
						'label' => $attribute['option_title'],
					];
					$this->baseAttribute = $attribute['super_attribute_label'];
					$optionsList[] = $attribute['value_index'];
				}
			}
		}
		return $options;
	}

	public function getMultiLayer()
	{
		$postData = $this->request->getPost();
		return $postData->multi_layer;
	}

	public function getRuleAttribute()
	{
		$attributeCode = $this->request->getPost()->rule_attribute;
		return $attributeCode ? $attributeCode : null;
	}

	public function getRuleOptions()
	{
		$postData = $this->request->getPost();
		$product = $this->productRepository->get($postData->sku);
		$attributeCode = $postData->rule_attribute;
		$data = $product->getTypeInstance()->getConfigurableOptions($product);
		$options = [];
		$optionsList = [];
		foreach ($data as $attrs) {
			foreach ($attrs as $attribute) {
				if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
					$options[] = [
						'value' => $attribute['value_index'],
						'label' => $attribute['option_title'] . ':',
					];
					$this->baseAttribute = $attribute['super_attribute_label'];
					$optionsList[] = $attribute['value_index'];
				}
			}
	    }
		return $options;
	}

	public function getFormId()
	{
		$postData = $this->request->getPost();
		$product = $this->productRepository->get($postData->sku);
		return $postData->sku . "-" . $this->getBaseAttribute() . "-" . $this->getAttribute() . $this->getRuleAttribute() . $this->getMultiLayer();
	}

	public function getAttribute()
	{
	    $attributeCode = $this->request->getPost()->attribute;
	    return $attributeCode;
	}

	public function getFormLabel()
	{
		$postData = $this->request->getPost();
		$product = $this->productRepository->get($postData->sku);
		if ($product->getTypeId() == "simple")
			$label = $product->getName();
		else
			$label = $product->getName() . " -> " . $product->getResource()->getAttribute($this->getAttribute())->getFrontend()->getLabel($product);
		return $label;
	}

	public function getSku()
	{
		$postData = $this->request->getPost();
		return $postData->sku;
	}

	public function getBaseSku()
	{
		$postData = $this->request->getPost();
		return $postData->base_sku;
	}
}
