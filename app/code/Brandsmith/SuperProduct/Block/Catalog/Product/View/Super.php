<?php
namespace Brandsmith\SuperProduct\Block\Catalog\Product\View;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Super
 * @package BrandSmith\SuperProduct\Block\Product\View\Type
 */

class Super extends Template
{

    const SWATCH_OPTION_TABLE = 'eav_attribute_option_swatch';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\BaseRepository
     */
    protected $_baseRepository;

    /**
     * Eav Entity Attribute Collection
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection
     */
    protected $_entityAttributeCollection;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    protected $_entityAttribute;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    protected $_attributeOptionCollection;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory
     */
    protected $_addonCollectionFactory;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchesHelper;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_ruleRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_catalogHelperImage;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_pnglayerCollectionFactory;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory
     */
    protected $_pngRuleCollectionFactory;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory
     */
    protected $_optionCollectionFactory;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;

    /**
     * @var SavecartprodetailFactory
     */
    protected $modelsavecartdetail;

    /**
     * @var \Magento\Wishlist\Model\Item\OptionFactory
     */
    private $optionFactory;
    /**
     * @var \Magento\Wishlist\Model\ItemFactory
     */
    protected $itemFactory;

    /**
     * @var \Brandsmith\OverrideCustom\Block\Customer\Wishlist\Item\Column\Cart
     */
    protected $wishlistPrice;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * PNG collection factory
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory
     */
    protected $_pngCollectionFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface|null
     */
    private $_objectManager = null;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * Super constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Brandsmith\SuperProduct\Model\BaseRepository $baseRepository
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection $entityAttributeCollection
     * @param \Magento\Eav\Model\Entity\Attribute $entityAttribute
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeOptionCollection
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory
     * @param \Magento\Swatches\Helper\Data $swatchesHelper
     * @param \Magento\Framework\Json\Helper\Data $JsonHelper
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Brandsmith\SuperProduct\Model\RuleRepository $RuleRepository
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
     * @param \Magento\Catalog\Helper\Image $CatalogHelperImage
     * @param \Brandsmith\SuperProduct\Model\PnglayerRepository $PnglayerRepository
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $pngLayerCollectionFactory
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $pngRuleCollectionFactory
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory $OptionCollectionFactory
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory
     * @param SavecartprodetailFactory $modelsavecartdetail
     * @param \Magento\Wishlist\Model\Item\OptionFactory $optionFactory
     * @param \Magento\Wishlist\Model\ItemFactory $itemFactory
     * @param \Brandsmith\OverrideCustom\Block\Customer\Wishlist\Item\Column\Cart $wishlistPrice
     * @param ResultFactory $resultFactory
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory $pngCollectionFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param CustomerSession $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Brandsmith\SuperProduct\Model\BaseRepository $baseRepository,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection $entityAttributeCollection,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeOptionCollection,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory,
        \Magento\Swatches\Helper\Data $swatchesHelper,
        \Magento\Framework\Json\Helper\Data $JsonHelper,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Brandsmith\SuperProduct\Model\RuleRepository $RuleRepository,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Catalog\Helper\Image $CatalogHelperImage,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $pngLayerCollectionFactory,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $pngRuleCollectionFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory $OptionCollectionFactory,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory,
        SavecartprodetailFactory $modelsavecartdetail,
        \Magento\Wishlist\Model\Item\OptionFactory $optionFactory,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Brandsmith\OverrideCustom\Block\Customer\Wishlist\Item\Column\Cart $wishlistPrice,
        ResultFactory $resultFactory,
        \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory $pngCollectionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        CustomerSession $customerSession,
        array $data = []
    ){
        $this->_registry = $registry;
        $this->_productRepository = $productRepository;
        $this->_baseRepository = $baseRepository;
        $this->_entityAttributeCollection = $entityAttributeCollection;
        $this->_entityAttribute = $entityAttribute;
        $this->_attributeOptionCollection = $attributeOptionCollection;
        $this->_addonCollectionFactory = $addonCollectionFactory;
        $this->_swatchesHelper = $swatchesHelper;
        $this->_jsonHelper = $JsonHelper;
        $this->eavConfig = $eavConfig;
        $this->_storeManager = $storeManager;
        $this->_resourceConnection = $resourceConnection;
        $this->_ruleRepository = $RuleRepository;
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->_catalogHelperImage = $CatalogHelperImage;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_uploaderPool = $uploaderPool;
        $this->_pnglayerCollectionFactory = $pngLayerCollectionFactory;
        $this->_pngRuleCollectionFactory = $pngRuleCollectionFactory;
        $this->_optionCollectionFactory = $OptionCollectionFactory;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->optionFactory = $optionFactory;
        $this->itemFactory = $itemFactory;
        $this->wishlistPrice = $wishlistPrice;
        $this->resultFactory = $resultFactory;
        $this->_pngCollectionFactory = $pngCollectionFactory;
        $this->_objectManager = $objectManager;
        $this->_customerSession = $customerSession;

        parent::__construct($context, $data);
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * @param $id
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProductById($id)
    {
        try {
            $_product = $this->_productRepository->getById($id);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            $_product = false;
        }

        return $_product;
    }

    /**
     * @param $sku
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface|\Magento\Catalog\Model\Product|null
     */
    public function getProductBySku($sku)
    {
        try {
            $_product = $this->_productRepository->get($sku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $_product = false;
        }
        return $_product;
    }

    /**
     * @param $_superProductId
     * @return bool|\Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBaseProductId($_superProductId)
    {
        try {
            $_baseProductId = $this->_baseRepository->getBySuperProductId($_superProductId);
        } catch (Exception $e) {
            $_baseProductId = false;
        }

        return $_baseProductId;
    }

    /**
     * Retrieve Addon.
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListsAddonBySuperProductId($superProductId)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->setOrder('position','ASC');
        $collection->load();
        return $collection;
    }

    public function getProductIdBySkuAddon($superProductId, $sku)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->setOrder('position','ASC');
        //$collection->load();
        if(count($collection) > 0) {
            $fItem = $collection->getFirstItem();
            return $fItem->getProductId();
        }
        return 0;
    }
    public function getProductSkuByIdAddon($superProductId, $productId)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('product_id', $productId);
        $collection->setOrder('position','ASC');
        //$collection->load();
        if(count($collection) > 0) {
            $fItem = $collection->getFirstItem();
            return $fItem->getSku();
        }
        return "";
    }

    /**
     * Retrieve Addon.
     */
    public function getAddOnRequire($superProductId, $addOnId)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('product_id', $addOnId);
        $collection->setOrder('position','ASC');
        $collection->load();
        $_addOnRequire = $collection->getFirstItem();
        return $_addOnRequire;
    }

    /**
     * Retrieve Attributes of Configurable product.
     *
     * @throws \Magento\Swatches\Helper\Data
     */
    public function getAttributesFromConfigurable($_configProduct)
    {
        $_attributes = null;
        if($_configProduct) {
            $_attributes = $this->_swatchesHelper->getAttributesFromConfigurable($_configProduct);
        }

        return $_attributes;
    }

    /**
     * Retrieve True False.
     *
     * @throws \Magento\Swatches\Helper\Data
     */
    public function getIsSwatchAttribute($_attribute)
    {
           return $this->_swatchesHelper->isSwatchAttribute($_attribute);
    }

    /**
     * Get attribute option data of a single option of the attribute
     *
     * @param   int $attributeId
     * @param   int $optionId
     * @return  \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    public function getAttributeInfo($entityType, $attributeCode)
    {
        try {
            $_attributeInfo = $this->_entityAttribute->loadByCode($entityType, $attributeCode);
        } catch (Exception $e) {
            $_attributeInfo = false;
        }

        return $_attributeInfo;
    }

    /**
     * Get all options of an attribute
     *
     * @param   int $attributeId
     * @return  \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    public function getAttributeOptionAll($attributeId)
    {
        return $this->_attributeOptionCollection
            ->setPositionOrder('asc')
            ->setAttributeFilter($attributeId)
            ->setStoreFilter()
            ->load();
    }


    public function getAttributeOptionAllByCode($_attributeCode)
    {
        $_attributeInfo = $this->getAttributeInfo('catalog_product', $_attributeCode);
        //$_attributeId = $_attributeInfo['attribute_id'];
        //$_attributeOption = $this->getAttributeOptionAll($_attributeId);

        return $_attributeInfo;
    }

    /**
     * Get attributes by code
     * Multiple entity types can have same attribute code
     * Entity types 'catalog_product' & 'catalog_category' both have 'name' attribute code
     * So, this function can return object of size greater than 1
     *
     * @return \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection
     */
    public function getAttributesByCode($code) {
        $this->_entityAttributeCollection->getSelect()->join(
            ['eav_entity_type'=>$this->_entityAttributeCollection->getTable('eav_entity_type')],
            'main_table.entity_type_id = eav_entity_type.entity_type_id',
            ['entity_type_code'=>'eav_entity_type.entity_type_code']);

        $attributes = $this->_entityAttributeCollection->setCodeFilter($code);

        return $attributes;
    }

    public function getJsonDecode($_decode)
    {
        return $this->_jsonHelper->jsonDecode($_decode);
    }

    public function getAllOption($_attributeCode)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', $_attributeCode);
        $options = $attribute->getSource()->getAllOptions();

        return $options;
    }


    public function getSwatchValue($_optionId)
    {
        $fields = array('value', 'type');
        //  $_storeId = $this->getStoreId();
        $_storeId = '0';

        $tableName = $this->_resourceConnection->getTableName(self::SWATCH_OPTION_TABLE);
        $connection = $this->_resourceConnection->getConnection();

        $sql = $connection->select()->from($tableName, $fields)
            ->where('option_id = ?', $_optionId)
            ->where('store_id = ?', $_storeId);

        $result = $connection->fetchAll($sql);

        return $result;
    }

    public function getProductThumbnail($_productId)
    {
        $imageWidth = 250;
        $imageHeight = 200;
        $_product = $this->getProductById($_productId);

        $_thumbnail = $this->_catalogHelperImage->init($_product, 'product_page_image_small')->setImageFile($_product->getFile())->resize($imageWidth, $imageHeight)->getUrl();

        return $_thumbnail;
    }

    public function getProductThumbnailBySku($sku)
    {
        $imageWidth = 250;
        $imageHeight = 200;
        $_product = $this->getProductBySku($sku);

        $_thumbnail = $this->_catalogHelperImage->init($_product, 'product_page_image_small')->setImageFile($_product->getFile())->resize($imageWidth, $imageHeight)->getUrl();

        return $_thumbnail;
    }

    public function getAttributeDependency($_superProductId, $_attributeDependency, $_productSku)
    {

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_productSku);
        $collection->addFieldToFilter('attribute_dep', $_attributeDependency);
        $collection->load();
        $dependency = $collection->getFirstItem();

        return $dependency;
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getAjaxSuperProduct()
    {
        return $this->getUrl('superproduct/index/index');
    }

    public function getAddSuperWishlist()
    {
        return $this->getUrl('superproduct/index/addsuperwishlist');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getAjaxAddCart()
    {
        return $this->getUrl('superproduct/index/cart');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getUpdateSaveCart()
    {
        return $this->getUrl('superproduct/index/updatesavecart');
    }
    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getUpdateWishlist()
    {
        return $this->getUrl('superproduct/index/updatewishlist');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getUpdateCart()
    {
        return $this->getUrl('superproduct/index/updatecart');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getImageDownload()
    {
        return $this->getUrl('superproduct/index/image');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getPngDefaultUrl()
    {
        return $this->getUrl('superproduct/index/pngdefault');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getUpdatePrice()
    {
        return $this->getUrl('superproduct/index/price');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getProductDependencyUrl()
    {
        return $this->getUrl('superproduct/index/productdependency');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getUpdateRequireImagesUrl()
    {
        return $this->getUrl('superproduct/index/updaterequireimages');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getRemovePngLayerDependency()
    {
        return $this->getUrl('superproduct/index/removepnglayerdependency');
    }

    /**
     * Retrieve Controller Url
     *
     * @return string
     */
    public function getSpecialAttributeUrl()
    {
        return $this->getUrl('superproduct/index/specialattribute');
    }

    public function getOptionImage($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption)
    {
        $_pngUrl = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption);

        if($_pngUrl->getData()){
            $_mediaUrl = $this->getMediaUrl() . 'brandsmith_superproduct/pnglayer/image/';
            $_pngUrl = $_mediaUrl.$_pngUrl['image'];

            return $_pngUrl;
        }

        return null;
    }

    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getProductDefaultImage($_superProductId, $_productId, $_baseAttribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('product_id', $_productId);
        $collection->addFieldToFilter('attribute', $_baseAttribute);
        $collection->addFieldToFilter('base_attribute', $_baseAttribute);
        $png = $collection->getFirstItem();
        return $png;
    }

    public function getProductDefaultImageUrl()
    {
        $uploader = $this->_uploaderPool->getUploader('image');
        $_url = $uploader->getBaseUrl().$uploader->getBasePath();

        return $_url;
    }



    /**
     * Retrieve Rule.
     *
     */
    public function getPngRuleBySuperProductId($superProductId, $sku, $attribute)
    {
        $collection = $this->_pngRuleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->addFieldToFilter('attribute_dep', $attribute);
        $rule = $collection->getFirstItem();

        return $rule;
    }

    public function getProductRuleJson($superProductId)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->setOrder('position','ASC');

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $item) {
                $_pngData = [];
                $_productId = $this->getProductBySku($item->getSku());
                $_productIdDep = $this->getProductBySku($item->getSkuDep());
                $_pngData['super_product_id'] = $superProductId;
                $_pngData['sku'] = $item->getSku();
                $_pngData['condition'] = $item->getCondition();
                $_pngData['sku_dep'] = $item->getSkuDep();
                $_pngData['position'] = $item->getPosition();
                $_pngData['product_id_dep'] = $_productIdDep->getId();
                $_pngData['product_id'] = $_productId->getId();
                $response[] = $_pngData;
            }
        }
        $response = json_encode($response);
        return $response;
    }



    /**
     * Retrieve Rule for json (improve speed)
     *
     */
    public function getPngRuleJson($superProductId)
    {
        $collection = $this->_pngRuleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->load();

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $item) {
                $add_on_product = $this->getProductBySku($item->getSku());
                $_pngData = [];
                $_pngData['super_product_id'] = $superProductId;
                $_pngData['sku'] = $item->getSku();
                $_pngData['attribute'] = $item->getAttribute();
                $_pngData['rule'] = $item->getAttributeDep();
                $_pngData['add_on_id'] = $add_on_product->getId();
                $response[] = $_pngData;
            }
        }
        $response = json_encode($response);
        return $response;
    }

    public function getSuperRuleJson($superProductId)
    {
        $superProductId = trim($superProductId);

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->setOrder('position','ASC');
        $collection->load();

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $item) {
                $_product = $this->getProductBySku($item->getSkuDep());
                $_pngData = [];
                $_pngData['super_product_id'] = $superProductId;
                $_pngData['rule_id'] = $item->getRuleId();
                $_pngData['sku'] = $item->getSku();
                $_pngData['attribute'] = $item->getAttribute();
                $_pngData['option'] = $item->getOption();
                $_pngData['condition'] = $item->getCondition();
                $_pngData['sku_dep'] = $item->getSkuDep();
                $_pngData['id_dep'] = $_product->getId();
                $_pngData['attribute_dep'] = $item->getAttributeDep();
                $_pngData['option_dep'] = $item->getOptionDep();
                $_pngData['condition_dep'] = $item->getConditionDep();
                $_pngData['position'] = $item->getPosition();
                $response[] = $_pngData;
            }
        }
        $response = json_encode($response);
        return $response;
    }

    public function getPngJson($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $item) {
                $_pngData = [];
                $_pngData['super_product_id'] = $superProductId;
                $_pngData['sku'] = $item->getSku();
                $_pngData['attribute'] = $item->getAttribute();
                $_pngData['base_attribute'] = $item->getBaseAttribute();
                $_pngData['default_option'] = $item->getDefaultOption();
                $_pngData['multi_layer'] = $item->getMultiLayer();
                $_pngData['status'] = $item->getStatus();
                $_pngData['position'] = $item->getPosition();
                $response[] = $_pngData;
            }
        }
        $response = json_encode($response);
        return $response;
    }

    public function getPngLayerJson($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $item) {
                $_pngData = [];
                $_pngData['super_product_id'] = $superProductId;
                $_pngData['product_id'] = $item->getProductId();
                $_pngData['attribute'] = $item->getAttribute();
                $_pngData['base_attribute'] = $item->getBaseAttribute();
                $_pngData['rule_attribute'] = $item->getRuleAttribute();
                $_pngData['option'] = $item->getOption();
                $_pngData['base_option'] = $item->getBaseOption();
                $_pngData['rule_option'] = $item->getRuleOption();
                $_pngData['image'] = $this->getProductDefaultImageUrl().$item->getImage();
                $_pngData['is_default'] = $item->getIsDefault();
                $_pngData['area'] = $item->getArea();
                $response[] = $_pngData;
            }
        }
        $response = json_encode($response);
        return $response;
    }

    public function getFabricColorJson($superProductId)
    {
        $listAddon = $this->getListsAddonBySuperProductId($superProductId);
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */

        $_attributeCode = 'fabric_type';
        $response = [];
        if(count($listAddon) > 0) {
            foreach ($listAddon as $item) {
                if($item->getType() == 'configurable') {
                    $addOnData = [];
                    //$_productConfigurable = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($item->getProductId());
                    $_productConfigurable = $this->getProductBySku($item->getSku());
                    $_children = $_productConfigurable->getTypeInstance()->getUsedProducts($_productConfigurable);

                    $_arrayFabricColour = [];
                    $_arrayFabrictype = [];
                    $_arrayColorProduct = [];
                    foreach ($_children as $child){
                        if (null !== $child->getCustomAttribute($_attributeCode)) {
                            $_attributeValue = $child->getCustomAttribute($_attributeCode)->getValue();
                            $_arrayFabrictype[] = $_attributeValue;
                           // if($_optionId == $_attributeValue) {
                                if (null !== $child->getCustomAttribute('fabric_colour')) {
                                    $_attributeValueFabricColour = $child->getCustomAttribute('fabric_colour')->getValue();
                                    $_arrayFabricColour[$_attributeValue][] = $_attributeValueFabricColour;

                                }
                           // }
                        }
                    }
                    $_arrayFabrictype = array_unique($_arrayFabrictype);
                    foreach ($_arrayFabrictype as $key=>$value) {
                        $_arrayColorProduct[$value] = array_unique($_arrayFabricColour[$value]);
                    }
                    $response[$item->getProductId()] = $_arrayColorProduct;
                }
            }
        }

        $response = json_encode($response);
        return $response;
    }

    public function getListImagesAddOnRequireJson($superProduct, $_baseAttribute) {
        $superProductId = $superProduct->getId();
        $superModel = $superProduct->getTypeInstance();
        $_listsRequireAddOn = $superModel->getListsRequireAddOn($superProductId);
        $addOnDefaultOptions = $superModel->getAddOnDefaultOptions($_listsRequireAddOn, $superProductId);

        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('attribute', $_baseAttribute);

        $response = [];
        if(count($collection) > 0) {
            foreach ($collection as $pnglayer) {
                $base_attribute_code_id = $pnglayer->getBaseOption();
                $_imgArray = array();

                foreach( $addOnDefaultOptions as $_addOnDefault ) {
                    $superProductId = $_addOnDefault['super_product_id'];
                    $_sku = $_addOnDefault['sku'];
                    $productId = $_addOnDefault['product_id'];
                    $attribute = $_addOnDefault['attribute'];
                    $_baseAttribute = $_addOnDefault['base_attribute'];
                    $option = $_addOnDefault['default_option'];
                    $_position = $_addOnDefault['position'];
                    $area = $_addOnDefault['multi_layer'];

                    $_PngDependencyRule = $superModel->getPngDependencyRule($superProductId, $_sku, $attribute , 1);

                    if($_PngDependencyRule != false) {
                        $_PngDependencyRuleAttribute = $_PngDependencyRule->getAttribute();
                        $_PngDependencyRuleDefaultOption = $_PngDependencyRule->getDefaultOption();
                        $_imgAddOnUrl = $superModel->getPngLayerImageUrlWithRule($superProductId, $productId, $attribute, $_baseAttribute, $_PngDependencyRuleAttribute, $option, $base_attribute_code_id, $_PngDependencyRuleDefaultOption, $area);
                    } else {
                        $_imgAddOnUrl = $superModel->getPngLayerImageUrl($superProductId, $productId, $attribute, $_baseAttribute, $option, $base_attribute_code_id, $area);
                    }

                    // imageUrl, imageCode, imageSku, productId, imagePosition
                    $_imgArray[] = array (
                        'image_url' => $_imgAddOnUrl,
                        'image_code' => $attribute,
                        'image_sku' => $_sku,
                        'product_id' => $productId,
                        'image_position' => $_position
                    );
                }
                $response[$base_attribute_code_id] = $_imgArray;
            }
        }
        $response = json_encode($response);
        return $response;
    }


    public function getChildSuperAttributes($_itemId, $_superProductId, $_childProductId)
    {
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
        $_superAttributes = $_superCustomOption['super_attributes'];

        $_optionSelected = null;

        foreach ($_superAttributes as $_itemAttributes) {
            if($_itemAttributes['product_id'] == $_childProductId) {
                $_optionSelected = $_itemAttributes['option_selected'];
            }
        }

        return $_optionSelected;
    }

    public function getChildSuperAttributesDisable($_itemId, $_superProductId, $_childProductId)
    {
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
        $_optionDisableSelect = [];
        if(isset($_superCustomOption['super_attributes_disable']) && $_superCustomOption['super_attributes_disable']){
            $_superAttributesDisable = $_superCustomOption['super_attributes_disable'];
            foreach ($_superAttributesDisable as $_itemAttributes) {
                if($_itemAttributes['productId'] == $_childProductId) {
                    foreach ($_itemAttributes['options_disable'] as $items) {
                        array_push($_optionDisableSelect, $items);
                    }
                }
            }
        }

        return $_optionDisableSelect;
    }

    public function getChildSuperSavecartAttributes($id, $_childProductId, $fromWishlist = 0)
    {
        if($fromWishlist == 0) { // Savecart
            $cartdata = $this->modelsavecartdetail->create()->load($id);
            $_superCustomOption = unserialize($cartdata->getQuoteConfigPrdData());
            $_superCustomOption = $_superCustomOption['super_options'];

            $_superAttributes = $_superCustomOption['super_attributes'];
        }else {
            $item = $this->itemFactory->create()->load($id);
            if (!$item->getId()) {
                return "";
            }
            $options = $this->optionFactory->create()->getCollection()->addItemFilter([$id]);
            foreach ($options as $_option) {
                $option_config = $_option->getValue();
                $option_config = json_decode($option_config, true);
            }
            $_superAttributes = isset($option_config['super_attributes']) ?$option_config['super_attributes']: [] ;
        }


        $_optionSelected = null;

        foreach ($_superAttributes as $_itemAttributes) {
            if($_itemAttributes['product_id'] == $_childProductId) {
                $_optionSelected = $_itemAttributes['option_selected'];
            }
        }

        return $_optionSelected;
    }

    public function getChildSuperSimple($_itemId, $_superProductId, $_childProductId, $id = 0, $fromWishlist = 0)
    {
        if($fromWishlist > 0) {
            $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
        }else {
            if($id > 0) {
                $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
            }else {
                $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
            }
        }

        $_superAttributes = $_superCustomOption['super_attributes'];

        $_simpleSelected = false;

        foreach ($_superAttributes as $_itemAttributes) {
            if($_itemAttributes['product_id'] == $_childProductId) {
                $_simpleSelected = true;
            }
        }

        return $_simpleSelected;
    }

    public function getChildSuperCustomQuantity($_itemId, $_superProductId, $_childProductId, $id = 0, $fromWishlist = 0)
    {
        if($fromWishlist > 0) {
            $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
        }else {
            if($id > 0) {
                $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
            }else {
                $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
            }
        }


        if( !array_key_exists('addon_qty', $_superCustomOption) ) {
            return false;
        }

        $_customQuantity = false;
        $_superAttribute = $_superCustomOption['addon_qty'];
        foreach ($_superAttribute as $key => $value) {
            if($key == $_childProductId) {
                $_customQuantity = $value;
            }
        }
        return $_customQuantity;
    }

    public function getSuperCustomOption($_itemId, $_productId)
    {
        $_quoteItemOption = $this->getQuoteItemOption($_itemId, $_productId);
        $_quoteItemOption = $_quoteItemOption->getFirstItem();
        $_quoteItemOptionValue = $this->_jsonHelper->jsonDecode($_quoteItemOption->getValue());

        return $_quoteItemOptionValue;
    }

    public function getSuperCustomOptionSavecart($id, $fromWishlist = 0)
    {
        if($fromWishlist > 0) {
            $item = $this->itemFactory->create()->load($id);
            if (!$item->getId()) {
                return "";
            }
            $options = $this->optionFactory->create()->getCollection()->addItemFilter([$id]);
            foreach ($options as $_option) {
                $option_config = $_option->getValue();
                $option_config = json_decode($option_config, true);
            }
            $_superCustomOption = $option_config;
        }else {
            $cartdata = $this->modelsavecartdetail->create()->load($id);
            $_superCustomOption = unserialize($cartdata->getQuoteConfigPrdData());
            $_superCustomOption = $_superCustomOption['super_options'];
        }


        return $_superCustomOption;
    }

    public function getSaveCartDetail($_savecartdetail_id) {
        $cartdata = $this->modelsavecartdetail->create()->load($_savecartdetail_id);
        if($cartdata) {
            return $cartdata;
        }
        return null;
    }

    public function getWishlistDetail($_wishlist_id) {
        $result = [];
        $item = $this->itemFactory->create()->load($_wishlist_id);
        if (!$item->getId()) {
            return $result;
        }
        $options = $this->optionFactory->create()->getCollection()->addItemFilter([$_wishlist_id]);
        $item->setOptions($options);
        $price = $this->wishlistPrice->getSuperProductPrice($item);
        $result['qty'] = (int)$item->getQty();
        $result['price'] = $price;
        return $result;
    }

    public function getQuoteItemOption($_itemId, $_productId)
    {
        $_code = 'super_custom_option';

        $collection = $this->_optionCollectionFactory->create();
        $collection->addFieldToFilter('item_id', $_itemId);
        $collection->addFieldToFilter('product_id', $_productId);
        $collection->addFieldToFilter('code', $_code);
        //$collection->loadData();
        return $collection;
    }

    public function getProductChosenRuleDetail($_itemId, $_superProductId, $_skuDependency, $id = 0, $fromWishlist = 0)
    {
        $condition = 1;
        $_productRuleDetail = $this->getCheckProductRule($_superProductId, $_skuDependency, $condition);
        $_productRuleDetailData['rule'] = false;
        if(count($_productRuleDetail) > 0) {
            // Return only one Rule
            if(count($_productRuleDetail) < 2) {
                $_productRuleDetailFirstItem = $_productRuleDetail->getFirstItem();
                $_duplicateRule = $_productRuleDetailFirstItem->getDuplicateRule();
                if($_duplicateRule) {
                    $_productRuleDetailData['rule'] = false;
                    return $_productRuleDetailData;
                }

                $_productId = $_productRuleDetailFirstItem->getProductId();
                if($fromWishlist > 0) {
                    $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
                }else {
                    if($id > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                    }else {
                        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                    }
                }
                $_superAttributes = $_superCustomOption['super_attributes'];
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_productRuleDetailData['rule'] = true;
                    }
                }
            }

            // Return more than one Rule
            if(count($_productRuleDetail) > 1) {
                $_productRuleCollection = $_productRuleDetail->getData();
                foreach($_productRuleCollection as $_productRule) {
                    $_productId = $_productRule['product_id'];
                    $_sku = $_productRule['sku'];
                    $_duplicateRule = $this->getCheckDuplicateRule($_superProductId, $_sku, $_skuDependency);
                    if($_duplicateRule) {
                        $_productRuleDetailData['rule'] = false;
                        return $_productRuleDetailData;
                    }

                    if($fromWishlist > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
                    }else {
                        if($id > 0) {
                            $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                        }else {
                            $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                        }
                    }
                    $_superAttributes = $_superCustomOption['super_attributes'];
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_productRuleDetailData['rule'] = true;
                        }
                    }
                }
            }
        }

        return $_productRuleDetailData;
    }

    public function getProductNotChosenRuleDetail($_itemId, $_superProductId, $_skuDependency, $id = 0 , $fromWishlist = 0)
    {
        $condition = 0;
        $_productRuleDetail = $this->getCheckProductRule($_superProductId, $_skuDependency, $condition);
        $_productRuleDetailData['rule'] = false;

        if(count($_productRuleDetail) > 0) {
            // Return only one Rule
            if(count($_productRuleDetail) < 2) {
                $_productRuleDetailFirstItem = $_productRuleDetail->getFirstItem();
                $_productId = $_productRuleDetailFirstItem->getProductId();
                if($fromWishlist > 0) {
                    $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
                }else {
                    if($id > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                    }else {
                        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                    }
                }

                $_superAttributes = $_superCustomOption['super_attributes'];
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_productRuleDetailData['rule'] = true;
                    }
                }
            }

            // Return more than one Rule
            if(count($_productRuleDetail) > 1) {
                $_productRuleCollection = $_productRuleDetail->getData();
                foreach($_productRuleCollection as $_productRule) {
                    $_productId = $_productRule['product_id'];
                    if($fromWishlist > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id,1);
                    }else {
                        if($id > 0) {
                            $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                        }else {
                            $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                        }
                    }

                    $_superAttributes = $_superCustomOption['super_attributes'];
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_productRuleDetailData['rule'] = true;
                        }
                    }
                }
            }
        }
        return $_productRuleDetailData;
    }

    /**
     * get Duplicate rule
     *
     * @return string
     */
    public function getCheckProductRule($_superProductId, $_skuDependency, $condition)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_skuDependency);
        $collection->addFieldToFilter('condition', $condition);
        $collection->load();
        return $collection;
    }

    /**
     * CheckDuplicateRule
     *
     * @return string
     */
    public function getCheckDuplicateRule($_superProductId, $_sku, $_skuDependency)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku', $_skuDependency);
        $collection->addFieldToFilter('sku_dep', $_sku);
        $collection->load();
        return count($collection) > 0 ? true : false;
    }

    public function getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId, $_conditionDep)
    {
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_productSkuDep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCodeDep);
        if($_optionIdDep) $collection->addFieldToFilter('option_dep', $_optionIdDep);
        if($_productSku) $collection->addFieldToFilter('sku', $_productSku);
        if($_attributeCode) $collection->addFieldToFilter('attribute', $_attributeCode);
        if($_optionId) $collection->addFieldToFilter('option', $_optionId);
        $collection->addFieldToFilter('condition_dep', $_conditionDep);

        $collection->load();
        return $collection;
    }

    public function getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId)
    {
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_productSkuDep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCodeDep);
        if($_optionIdDep) $collection->addFieldToFilter('option_dep', $_optionIdDep);
        if($_productSku) $collection->addFieldToFilter('sku', $_productSku);
        if($_attributeCode) $collection->addFieldToFilter('attribute', $_attributeCode);
        if($_optionId) $collection->addFieldToFilter('option', $_optionId);

        $collection->load();
        return $collection;
    }

    public function checkAttributeRule($_superProductId, $_itemId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $id = 0, $fromWishlist = 0)
    {
        $_productSku = $_attributeCode = $_optionId = null;
        $_attributesRule = $this->getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId);
        $_attributesRuleData = $_attributesRule->getData();
        
        $_rule = false;
        if( !empty($_attributesRuleData) ) {
            foreach ($_attributesRuleData as $_attributesRuleItem) {
                $_productSku = $_attributesRuleItem['sku'];
                $_product = $this->getProductBySku($_productSku);
                $_productId = $_product->getId();
                if($fromWishlist > 0) {
                    $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
                }else {
                    if($id > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                    }else {
                        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                    }
                }
                $_superAttributes = $_superCustomOption['super_attributes'];
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_optionSelected = $_itemAttributes['option_selected'];
                        $_attributeCode = $_attributesRuleItem['attribute'];

                        if( array_key_exists($_attributeCode, $_optionSelected) ) {

                            if(isset($_optionSelected[$_attributeCode])) {
                                if($_attributesRuleItem['option'] == $_optionSelected[$_attributeCode] && $_attributesRuleItem['condition_dep'] != 1) {
                                    $_rule = true;
                                }
                            }
                            if(isset($_optionSelected[$_attributeCode])) {
                                if($_attributesRuleItem['option'] != $_optionSelected[$_attributeCode]) {
                                    $_ruleWithConditionWithoutOptionId = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, null, $_productSku, $_attributeCode, null, 1);
                                    if(count($_ruleWithConditionWithoutOptionId->getData()) > 1) {
                                        $_ruleWithCondition = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, null, $_productSku, $_attributeCode, $_attributesRuleItem['option'], 1);
                                        $_ruleWithConditionFullRule = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionSelected[$_attributeCode], 1);
                                        if(!empty($_ruleWithCondition->getData()) && count($_ruleWithConditionFullRule->getData()) != 1 ) {
                                            $_rule = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if( empty($_attributesRuleData) ) {
            $_optionIdDep = $_productSku = $_attributeCode = $_optionId = null;
            $_attributesRule = $this->getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId);
            $_attributesRuleData = $_attributesRule->getData();

            if( !empty($_attributesRuleData) ) {
                foreach ($_attributesRuleData as $_attributesRuleItem) {
                    $_productSku = $_attributesRuleItem['sku'];
                    $_product = $this->getProductBySku($_productSku);
                    $_productId = $_product->getId();
                    if($fromWishlist > 0) {
                        $_superCustomOption = $this->getSuperCustomOptionSavecart($id, 1);
                    }else {
                        if($id > 0) {
                            $_superCustomOption = $this->getSuperCustomOptionSavecart($id);
                        }else {
                            $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                        }
                    }

                    $_superAttributes = $_superCustomOption['super_attributes'];
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_optionSelected = $_itemAttributes['option_selected'];
                            if(isset($_optionSelected[$_attributeCodeDep])) {
                                if($_optionSelected[$_attributeCodeDep] == $_attributesRuleItem['option']) {
                                    $_rule = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $_rule;
    }
    // Funtion check attribute code in rule
    public function checkAttributeCodeInRule($_superProductId,$_attributeCode,$getRequire=null){
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('attribute_dep', $_attributeCode);

        $collection->load();
        if($getRequire && $collection->getSize()){
            return $collection->getData();
        }
        return (count($collection)> 0)? true : false;
    }
    public function checkAttributeCodehasInRule($_superProductId,$_sku_dep,$_attributeCode,$_optionIdDep=null){
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_sku_dep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCode);
        if($_optionIdDep){
            $collection->addFieldToFilter('option_dep', $_optionIdDep);
        }

        $collection->load();
        return (count($collection)> 0)? true : false;
    }

    public function checkSpecialAttribute($_childProductSku, $_optionSelectedFabricType, $_optionIdDep)
    {
        $_fabricTypeCode = 'fabric_type';
        $_fabricColourCode = 'fabric_colour';

        $_productConfigurable = $this->getProductBySku($_childProductSku);
        $_children = $_productConfigurable->getTypeInstance()->getUsedProducts($_productConfigurable);
        $_arrayFabricColour = array();

        foreach ($_children as $child){
            if (null !== $child->getCustomAttribute($_fabricTypeCode)) {
                $_attributeValue = $child->getCustomAttribute($_fabricTypeCode)->getValue();
                if($_optionSelectedFabricType == $_attributeValue) {
                    if (null !== $child->getCustomAttribute($_fabricColourCode)) {
                        $_attributeValueFabricColour = $child->getCustomAttribute($_fabricColourCode)->getValue();
                        $_arrayFabricColour[] = $_attributeValueFabricColour;
                    }
                }
            }
        }

        $_unique = array_unique($_arrayFabricColour);
        $_array_values = array_values($_unique);
        $_data = array_search($_optionIdDep, $_array_values);
        // Found in first value
        if($_data === 0 ) {
            $_data = true;
        }
        return $_data;
    }

    /**
     * @return bool|mixed
     */
    public function getCustomerInfo()
    {
        if($this->_customerSession->isLoggedIn()) {
            $customer = $this->_customerSession->getCustomer();
            return $customer->getEmail() . ' - ' . $customer->getName();
        }

        return 'Guest';
    }
}