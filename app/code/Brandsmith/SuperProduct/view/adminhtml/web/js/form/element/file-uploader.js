/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'underscore',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/lib/validation/validator',
    'Magento_Ui/js/form/element/file-uploader',
    'jquery/file-uploader'
], function ($, _, utils, registry, uiAlert, validator, Element) {
    'use strict';

    return Element.extend({
        defaults: {
            isMultipleFiles: true,
            isShow: true,
            parentName: '${$.parentName}',
            optionHideElement: '${ $.parentName }.option_hide',
            placeholderType: 'image', // 'image', 'video'
            listens: {
                'product_form.product_form.rule_options.rule_records:reload': 'reloadFromRule',
                //'product_form.product_form_data_source:data.rules.rule_records': 'reloadFromRule',
            },
            imports: {
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
            },
        },

        /**
         * Set records to cache
         *
         * @param {Object} records - record instance
         */
        reloadFromRule: function (records) {
            //https://inchoo.net/magento-2/magento-2-ui-components-and-listener/
            var option = registry.get(this.optionHideElement).value();
            this.isMultipleFiles(true);
            this.isShow(true);
            if (this.value().length > 0) {
                if (this.value()[0].url == false)
                    this.value([]);
                else
                    this.isMultipleFiles(false);
            }
            this.ruleRecords.each(function (data, index) {
                if (data.sku == this.baseSku && data.sku_dep == this.sku && data.attribute == this.baseAttribute && data.attribute_dep == this.attribute && data.option == this.baseOption) {
                    if (data.condition_dep == 1) {
                        if (data.option_dep != option) {
                            this.isMultipleFiles(false);
                            this.isShow(false);
                            if (this.value().length == 0) this.value([{url: false}]);
                        }
                    }
                    else if (data.condition_dep == 0) {
                        if (data.option_dep == option) {
                            this.isMultipleFiles(false);
                            this.isShow(false);
                            if (this.value().length == 0) this.value([{url: false}]);
                        }
                    }
                }
                if (data.sku == this.baseSku && data.sku_dep == this.sku && data.attribute == this.baseAttribute && data.attribute_dep == this.attribute && data.sku_dep == data.attribute_dep) {
                    if (data.condition_dep == 1) {
                        if (data.option != this.baseOption) {
                            this.isMultipleFiles(false);
                            this.isShow(false);
                            if (this.value().length == 0) this.value([{url: false}]);
                        }
                    }
                    else if (data.condition_dep == 0) {
                        if (data.option == this.baseOption) {
                            this.isMultipleFiles(false);
                            this.isShow(false);
                            if (this.value().length == 0) this.value([{url: false}]);
                        }
                    }
                }
            }, this);
        },

        /**
         * Set list of observable attributes
         * @returns {exports.initObservable}
         */
        initObservable: function () {
            var self = this;

            this._super()
                .observe(['isMultipleFiles', 'isShow']);

            return this;
        },

        /**
         * Defines initial value of the instance.
         *
         * @returns {FileUploader} Chainable.
         */
        setInitialValue: function () {
            var value = this.getInitialValue();
            var option = this.source.get(registry.get(this.parentName).dataScope + '.option_hide');


            value = value.map(this.processFile, this);

            if (typeof this.ruleRecords !== 'undefined') {
                this.ruleRecords.each(function (data, index) {
                    if (data.sku == this.baseSku && data.sku_dep == this.sku && data.attribute == this.baseAttribute && data.attribute_dep == this.attribute && data.option == this.baseOption) {
                        if (data.condition_dep == 1) {
                            if (data.option_dep != option) {
                                if (value.length == 0) value = [{url: false}];
                                this.isShow(false);
                            }
                        }
                        else if (data.condition_dep == 0) {
                            if (data.option_dep == option) {
                                if (value.length == 0) value = [{url: false}];
                                this.isShow(false);
                            }
                        }
                    }
                    if (data.sku == this.baseSku && data.sku_dep == this.sku && data.attribute == this.baseAttribute && data.attribute_dep == this.attribute && data.sku_dep == data.attribute_dep) {
                        if (data.condition_dep == 1) {
                            if (data.option != this.baseOption) {
                                if (value.length == 0) value = [{url: false}];
                                this.isShow(false);
                            }
                        }
                        else if (data.condition_dep == 0) {
                            if (data.option == this.baseOption) {
                                if (value.length == 0) value = [{url: false}];
                                this.isShow(false);
                            }
                        }
                    }
                }, this);
            }

            this.initialValue = value.slice();

            this.value(value);
            this.on('value', this.onUpdate.bind(this));
            this.isUseDefault(this.disabled());

            if (value.length > 0) {
                this.isMultipleFiles(false);
            }


            return this;
        },

        /**
         * Handler which is invoked prior to the start of a file upload.
         *
         * @param {Event} e - Event object.
         * @param {Object} data - File data that will be uploaded.
         */
        onBeforeFileUpload: function (e, data) {
            this.isMultipleFiles(false);
            var file     = data.files[0],
                allowed  = this.isFileAllowed(file),
                target   = $(e.target);

            if (allowed.passed) {
                target.on('fileuploadsend', function (event, postData) {
                    postData.data.append('param_name', this.paramName);
                    postData.data.append('image', file);
                }.bind(data));

                target.fileupload('process', data).done(function () {
                    data.submit();
                });
            } else {
                this.notifyError(allowed.message);
            }
        },

        /**
         * Adds provided file to the files list.
         *
         * @param {Object} file
         * @returns {FileUploder} Chainable.
         */
        addFile: function (file) {
            file = this.processFile(file);

            this.value([file]);

            return this;
        },

        /**
         * Removes provided file from thes files list.
         *
         * @param {Object} file
         * @returns {FileUploader} Chainable.
         */
        removeFile: function (file) {
            this.value.remove(file);
            this.isMultipleFiles(true);

            return this;
        },
    });
});
