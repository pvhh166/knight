/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/form/element/abstract',
    'uiRegistry',
], function (Abstract, registry) {
    'use strict';

    return Abstract.extend({
        /**
         * Calls 'onUpdate' method of parent, if value is defined and instance's
         *     'unique' property set to true, calls 'setUnique' method
         *
         * @return {Object} - reference to instance
         */
        onUpdate: function () {
            // comment out becasue it confliceted with delete function
            // registry.get('product_form.product_form.rule_options.rule_records').reload();
            // registry.get('product_form.product_form.png_layer.png_records').reload();
            return this._super();
        }
    });
});
