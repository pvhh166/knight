/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/form/components/button',
    'uiRegistry',
    'uiLayout',
    'mageUtils',
    'jquery',
    'underscore'
], function (Element, registry, layout, utils, $, _) {
    'use strict';

    return Element.extend({
        defaults: {
            buttonClasses: {},
            additionalClasses: {},
            getUrl: '${ $.getUrl }',
            displayArea: 'outsideGroup',
            displayAsLink: false,
            elementTmpl: 'ui/form/element/button',
            template: 'ui/form/components/button/simple',
            visible: true,
            disabled: false,
            title: '',
            imports: {
                pngRules: 'product_form.product_form_data_source:data.pngrules.rule_records',
                rules: 'product_form.product_form_data_source:data.rules.rule_records',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                sku: 'product_form.product_form_data_source:data.product.sku',
            },
        },

        exportAction: function () {
            var baseSku = '',
                baseAttribute = '',
                pngkey = [],
                pngrulesku = [],
                pngruleattribute = [],
                pngruleattributedep = [],
                rulekey = [],
                rulesku = [],
                ruleattribute = [],
                rulecondition = [],
                ruleoptiondep = [],
                ruleoption = [],
                that = this;

            if (typeof this.baseList !== 'undefined') {
                this.baseList.each(function (data, index) {
                    baseSku = data.sku;
                    baseAttribute = data.base_attribute;
                }, this);
            }

            if (typeof this.pngRules !== 'undefined') {
                this.pngRules.each(function (data, index) {
                    pngkey.push(data.sku + ',' + data.attribute_dep);
                    pngrulesku.push(data.sku);
                    pngruleattribute.push(data.attribute);
                    pngruleattributedep.push(data.attribute_dep);
                }, this);
            }

            if (typeof this.rules !== 'undefined') {
                this.rules.each(function (data, index) {
                    if (data.sku_hide == baseSku && data.attribute_hide == baseAttribute) {
                        rulekey.push(data.sku_dep_hide + ',' + data.attribute_dep_hide + ',' + data.option_dep_hide + ',' + data.condition_dep);
                        rulesku.push(data.sku_dep_hide);
                        ruleattribute.push(data.attribute_dep_hide);
                        rulecondition.push(data.condition_dep);
                        ruleoptiondep.push(data.option_dep_hide);
                        ruleoption.push(data.option_hide);
                    }
                }, this);
            }

            $.ajax({
                type: 'POST',
                url: this.getUrl,
                data: {
                    sku: this.sku,
                    base_sku: baseSku,
                    base_attribute: baseAttribute,
                    pngkey: pngkey,
                    pngrulesku: pngrulesku,
                    pngruleattribute: pngruleattribute,
                    pngruleattributedep: pngruleattributedep,
                    rulekey: rulekey,
                    rulesku: rulesku,
                    ruleattribute: ruleattribute,
                    ruleoptiondep: ruleoptiondep,
                    ruleoption: ruleoption,
                },
                showLoader: true
            }).done(function (response) {
                const url = window.URL.createObjectURL(new Blob([response]));
                const link = document.createElement('a');
                var d = new Date();
                var n = d.getTime();
                link.href = url;
                link.setAttribute('download', 'Export-template-' + this.sku + '-' + n + '.csv');
                document.body.appendChild(link);
                link.click();
            }.bind(this));
            // window.location = url;
        },
    });
});
