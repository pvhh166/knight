/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'ko',
    'mageUtils',
    'underscore',
    'uiLayout',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry',
    'jquery',
    'mage/translate'
], function (ko, utils, _, layout, uiCollection, registry, $, $t) {
    'use strict';

    return uiCollection.extend({
        defaults: {
            reloadFlag: true,
            addButton: false,
            getUrl: '${ $.getUrl }',
            listSku: [],
            listName: [],
            rule: [],
            pngkey: [],
            pngrulesku: [],
            pngruleattribute: [],
            pngruleattributedep: [],
            rulekey: [],
            rulesku: [],
            ruleattribute: [],
            ruleoptiondep: [],
            ruleoption: [],
            baseAttribute: '',
            baseSku: '',
            pageSize: 1000,
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                pngRules: 'product_form.product_form_data_source:data.pngrules.rule_records',
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
                sku: 'product_form.product_form_data_source:data.product.sku',
                png: 'product_form.product_form_data_source:data.png',
            },
            listens: {
                'product_form.product_form_data_source:data.rules.mapper_data': 'reloadList',
            },
        },

    

        /**
         * Extends instance with default config, calls initialize of parent
         * class, calls initChildren method, set observe variable.
         * Use parent "track" method - wrapper observe array
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();

            return this;
        },

        /**
         * Initialize children
         *
         * @returns {Object} Chainable.
         */
        initChildren: function () {
            this.showSpinner(true);
            this.getChildItems().forEach(function (data, index) {
                this.addChild(data, this.startIndex + index);
            }, this);

            return this;
        },

        /**
         * Rerender dynamic-rows elems
         */
        getListSku: function () {
            var reload = false,
                skuLength = this.listSku.length,
                ruleLength = this.rule.length;
            this.listSku = [];
            this.listName = [];
            this.rule = [];
            this.pngkey = [];
            this.pngrulesku = [];
            this.pngruleattribute = [];
            this.pngruleattributedep = [];
            this.rulekey = [];
            this.rulesku = [];
            this.ruleattribute = [];
            this.ruleoptiondep = [];
            this.ruleoption = [];

            if (typeof this.baseList !== 'undefined') {
                this.baseList.each(function (data, index) {
                    this.baseSku = data.sku;
                    this.listSku.push(data.sku);
                    this.listName.push(data.name);
                    if (this.baseAttribute != data.base_attribute){
                        this.baseAttribute = data.base_attribute;
                        reload = true;
                    }
                }, this);
            }

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    this.listSku.push(data.sku);
                    this.listName.push(data.name);
                }, this);
            }

            if (typeof this.pngRules !== 'undefined') {
                this.pngRules.each(function (data, index) {
                    this.pngkey.push(data.sku + ',' + data.attribute_dep);
                    this.pngrulesku.push(data.sku);
                    this.pngruleattribute.push(data.attribute);
                    this.pngruleattributedep.push(data.attribute_dep);
                }, this);
            }

            if (typeof this.ruleRecords !== 'undefined') {
                this.ruleRecords.each(function (data, index) {
                    if (data.sku_hide == this.baseSku && data.attribute_hide == this.baseAttribute) {
                        this.rule.push({
                            sku: data.sku_dep_hide,
                            attribute: data.attribute_dep_hide,
                            condition: data.condition_dep
                        });
                        this.rulekey.push(data.sku_dep_hide + ',' + data.attribute_dep_hide + ',' + data.option_dep_hide + ',' + data.condition_dep);
                        this.rulesku.push(data.sku_dep_hide);
                        this.ruleattribute.push(data.attribute_dep_hide);
                        this.ruleoptiondep.push(data.option_dep_hide);
                        this.ruleoption.push(data.option_hide);
                    }
                }, this);
            }

            if (this.listSku.length != skuLength){
                reload = true;
            }

            if (this.rule.length != ruleLength){
                reload = true;
            }

            return reload;
        },

        /**
         * Rerender dynamic-rows elems
         */
        getStatus: function (result) {
            for(var index in this.png){
                if (index != 'png_records'){
                    var sku, attribute, status = 'Ready';

                    this.png[index].each(function (com) {
                        sku = com.sku;
                        attribute = com.attribute;
                        for(var i in com){
                            if (i.startsWith("image")){
                                if (com[i].length == 0)
                                    status = 'Not ready';
                            }
                        }
                    }, this);

                    for(var i in result){
                        if (result[i].sku_hide == sku && result[i].attribute_hide == attribute){
                            result[i].status = status;
                        }
                    }
                }
            }
            return result;
        },

        /**
         * Rerender dynamic-rows elems
         */
        reloadList: function (mapper) {
            if (!this.reloadFlag || mapper == 'undefined')
                return;

            this.clear();
            this.showSpinner(true);
            if (this.getListSku() && this.listSku.length > 0 && this.baseList.length > 0)
            {
                var that = this;
                setTimeout(function () {
                    if (that.reloadFlag) {
                        that.reloadFlag = false;
                        $.ajax({
                            type: 'POST',
                            url: that.getUrl,
                            data: {
                                sku: that.listSku,
                                name: that.listName,
                                super_sku: that.sku,
                                base_attribute: that.baseAttribute,
                                pngkey: that.pngkey,
                                pngrulesku: that.pngrulesku,
                                pngruleattribute: that.pngruleattribute,
                                pngruleattributedep: that.pngruleattributedep,
                                rulekey: that.rulekey,
                                rulesku: that.rulesku,
                                ruleattribute: that.ruleattribute,
                                ruleoptiondep: that.ruleoptiondep,
                                ruleoption: that.ruleoption,
                            },
                            showLoader: false
                        }).done(function (result) {
                            that.recordData(that.getStatus(result));
                            that.getChildItems().forEach(function (data, index) {
                                that.addChild(data, that.startIndex + index);
                            }, that);
                            that.reloadFlag = true;
                        }.bind(that));
                    }
                }, 3000);
            }
            else{
                this.recordData([]);
                this.showSpinner(false);
            }
        },

        addviewEvent: function (parent) {
            var registry = require('uiRegistry');
            var test = registry.get(parent);
            test.data().multi_layer = Number(test.data().multi_layer) + 1;

            this.source.set(this.dataScope + '.' + this.index + '.' + this.recordData().length, test.data());
            this.reload();
        },
    });
});
