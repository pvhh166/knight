/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'jquery',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'uiLayout'
], function (_, $, utils, registry, UiSelect, layout) {
    'use strict';

    return UiSelect.extend({
        defaults: {
            parentName: '${ $.parentName }',
            hiddenName: '${ $.parentName }.sku_dep_hide',
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                ruleRecords: 'product_form.product_form_data_source:data.productrules.rule_records',
            },
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            this._super();

            var parentGrid = registry.get('product_form.product_form.product_rule.rule_records'),
                parent = registry.get(this.parentName);

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    if (data.required == 0) {
                        this.options.push({"value": data.sku, "label": data.name});
                    }
                }, this);
            }

            if (typeof this.ruleRecords[parent.recordId] !== 'undefined' && this.options().length > 0) {
                if (parentGrid.listSku.indexOf(this.ruleRecords[parent.recordId].sku_dep_hide) > -1) {
                    this.value(this.ruleRecords[parent.recordId].sku_dep_hide);
                }
                else {
                    this.value(this.options()[0].value);
                }
            }

            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            var field = registry.get(this.hiddenName);
            if (typeof field !== 'undefined') {
                field.value(value);
            }
            return this._super();
        },
    });
});
