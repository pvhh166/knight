/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'ko',
    'mageUtils',
    'underscore',
    'uiLayout',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry',
    'jquery',
    'mage/translate'
], function (ko, utils, _, layout, uiCollection, registry, $, $t) {
    'use strict';

    return uiCollection.extend({
        defaults: {
            addButton: false,
            pageSize: 1000,
            getUrl: '${ $.getUrl }',
            mapper: [],
            listSku: [],
            listName: [],
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
            }
        },

    

        /**
         * Extends instance with default config, calls initialize of parent
         * class, calls initChildren method, set observe variable.
         * Use parent "track" method - wrapper observe array
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();
            return this;
        },

        /**
         * Initialize children
         *
         * @returns {Object} Chainable.
         */
        initChildren: function () {
            this.showSpinner(false);
            this.recordData(this.dataOption);
            this.getChildItems().forEach(function (data, index) {
                this.addChild(data, this.startIndex + index);
            }, this);

            return this;
        },

    });
});
