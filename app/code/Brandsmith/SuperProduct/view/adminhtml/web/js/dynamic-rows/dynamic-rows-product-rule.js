/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'ko',
    'mageUtils',
    'underscore',
    'uiLayout',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry',
    'jquery',
    'mage/translate'
], function (ko, utils, _, layout, uiCollection, registry, $, $t) {
    'use strict';

    return uiCollection.extend({
        defaults: {
            reloadFlag: true,
            listSku: [],
            listName: [],
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
            },
            listens: {
                'product_form.product_form_data_source:data.addon.super_selections': 'reloadFromAddOn',
            },
        },

        /**
         * Extends instance with default config, calls initialize of parent
         * class, calls initChildren method, set observe variable.
         * Use parent "track" method - wrapper observe array
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();

            return this;
        },

        /**
         * Initialize children
         *
         * @returns {Object} Chainable.
         */
        initChildren: function () {
            this.getListSku();
            if (this.listSku.length > 0) {
                this.getChildItems().forEach(function (data, index) {
                    this.addChild(data, this.startIndex + index);
                }, this);
                this.showSpinner(false);
            }

            return this;
        },

        /**
         * Rerender dynamic-rows elems
         */
        getListSku: function () {
            this.listSku = [];
            this.listName = [];

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    if (data.required == 0) {
                        this.listSku.push(data.sku);
                        this.listName.push(data.name);
                    }
                }, this);
            }
        },

        /**
         * Rerender dynamic-rows elems
         */
        reloadFromAddOn: function () {
            if (!this.reloadFlag)
                return;

            var oldSku = this.listSku;
            this.setLinks(this.imports, 'imports');
            this.getListSku();

            if (_.isEqual(oldSku, this.listSku) == false) {
                this.clear();

                // reset record Data
                this.recordData(
                    _.filter(this.recordData(), function (elem) {
                        return elem && this.listSku.includes(elem['sku']) && this.listSku.includes(elem['sku_dep']);
                    }, this)
                );

                if (this.listSku.length > 0) {
                    var that = this;
                    setTimeout(function () {
                        that.getChildItems().forEach(function (data, index) {
                            that.addChild(data, that.startIndex + index);
                        }, that);
                        that.showSpinner(false);
                    }, 3000);
                }
                else {
                    this.recordData([]);
                    this.showSpinner(false);
                }
            }
        },

        /**
         * Processing pages before addChild
         *
         * @param {Object} ctx - element context
         * @param {Number|String} index - element index
         * @param {Number|String} prop - additional property to element
         */
        processingAddChild: function (ctx, index, prop) {
            if (this.listSku.length > 0) {
                this._super();
            }

            return this;
        },
    });
});
