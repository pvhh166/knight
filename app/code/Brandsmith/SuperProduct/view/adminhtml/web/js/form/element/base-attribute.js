/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'jquery',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'uiLayout'
], function (_, $, utils, registry, UiSelect, layout) {
    'use strict';

    return UiSelect.extend({
        defaults: {
            customName: '${ $.parentName }.${ $.index }_input',
            elementTmpl: 'ui/form/element/select',
            getAttributesUrl: '${ $.getAttributesUrl }',
            caption: '',
            productId: '',
            valueId: '',
            reload: false,
            imports: {
                valueId: 'product_form.product_form_data_source:data.baseproduct.super_selections.0.option_id',
                productId: 'product_form.product_form_data_source:data.baseproduct.super_selections.0.product_id',
            },
            options: []
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            this._super();

            $.ajax({
                type: 'POST',
                url: this.getAttributesUrl,
                data: {
                    product_id: this.productId
                },
                showLoader: true
            }).done(function (attributes) {
                if (attributes[0].value == this.valueId)
                    this.reload = true;
                else 
                    this.reload = false;
                this.options(attributes);
                this.reload = true;
                this.value(this.valueId);
            }.bind(this));

            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            this._super();
            if (this.reload && value != '') registry.get('product_form.product_form.png_layer.png_records').reloadList();
        },
    });
});
