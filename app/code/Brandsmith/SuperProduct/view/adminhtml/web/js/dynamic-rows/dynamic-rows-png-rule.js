/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'ko',
    'mageUtils',
    'underscore',
    'uiLayout',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry',
    'jquery',
    'mage/translate'
], function (ko, utils, _, layout, uiCollection, registry, $, $t) {
    'use strict';

    return uiCollection.extend({
        defaults: {
            initLoad: false,
            listSku: [],
            listName: [],
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                mapper: 'product_form.product_form_data_source:data.rules.mapper_data',
            },
            listens: {
                'product_form.product_form_data_source:data.rules.mapper_data': 'reload',
            },
        },

        /**
         * Extends instance with default config, calls initialize of parent
         * class, calls initChildren method, set observe variable.
         * Use parent "track" method - wrapper observe array
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();

            return this;
        },

        /**
         * Initialize children
         *
         * @returns {Object} Chainable.
         */
        initChildren: function () {
            // remove all code
            return this;
        },

        /**
         * Rerender dynamic-rows elems
         */
        reload: function () {
            this.clear();
            this.getListSku();
            var that = this,
                flag = true;

            var id = setInterval(function(){
                if (flag == false)
                {
                    clearInterval(id);
                    return;
                }
                    
                if (that.listSku.length > 0 && that.mapper.length != 0) {
                    that.getChildItems().forEach(function (data, index) {
                        that.addChild(data, that.startIndex + index);
                    }, that);
                    that.showSpinner(false);
                    flag = false;
                }
            }, 3000);
        },

        /**
         * Rerender dynamic-rows elems
         */
        getListSku: function () {
            this.listSku = [];
            this.listName = [];

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    if (data.type == 'configurable') {
                        this.listSku.push(data.sku);
                        this.listName.push(data.name);
                    }
                }, this);
            }
        },

        /**
         * Processing pages before addChild
         *
         * @param {Object} ctx - element context
         * @param {Number|String} index - element index
         * @param {Number|String} prop - additional property to element
         */
        processingAddChild: function (ctx, index, prop) {
            if (this.listSku.length > 0) {
                this._super();
            }

            return this;
        },
    });
});
