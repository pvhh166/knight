/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'jquery',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'uiLayout'
], function (_, $, utils, registry, UiSelect, layout) {
    'use strict';

    return UiSelect.extend({
        defaults: {
            parentName: '${ $.parentName }',
            hiddenName: '${ $.parentName }.option_hide',
            imports: {
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
                mapper: 'product_form.product_form_data_source:data.rules.mapper_data',
            },
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            this._super();

            var parent = registry.get(this.parentName),
                attribute = this.ruleRecords[parent.recordId].attribute,
                sku = this.ruleRecords[parent.recordId].sku;

            if (typeof sku !== 'undefined' && typeof attribute !== 'undefined') {
                this.setOptions(this.mapper[sku][attribute]['options']);
                this.value(this.ruleRecords[parent.recordId].option_hide);
            }
            
            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            var parentGrid = registry.get('product_form.product_form.rule_options.rule_records');
            parentGrid.onReload();
            this.setHideValue(value);
            return this._super();
        },

        /**
         * Set options to dependent select
         *
         * @param {String} value
         */
        setHideValue: function (value) {
            var field = registry.get(this.hiddenName);

            if (value == '')
                return this;

            if (typeof field !== 'undefined') {
                field.value(value);
            }

            return this;
        }
    });
});
