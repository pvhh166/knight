/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'ko',
    'mageUtils',
    'underscore',
    'uiLayout',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry',
    'jquery',
    'mage/translate'
], function (ko, utils, _, layout, uiCollection, registry, $, $t) {
    'use strict';

    return uiCollection.extend({
        defaults: {
            reloadFlag: true,
            getUrl: '${ $.getUrl }',
            mapper: [],
            listSku: [],
            listName: [],
            pageSize: 1000,
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
            },
            exports: {
                mapper: 'product_form.product_form_data_source:data.rules.mapper_data',
            },
            listens: {
                'product_form.product_form_data_source:data.baseproduct.super_selections': 'reloadList',
                'product_form.product_form_data_source:data.addon.super_selections': 'reloadList',
            },
        },

    

        /**
         * Extends instance with default config, calls initialize of parent
         * class, calls initChildren method, set observe variable.
         * Use parent "track" method - wrapper observe array
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();

            return this;
        },

        /**
         * Initialize children
         *
         * @returns {Object} Chainable.
         */
        initChildren: function () {
            this.getListSku();
            if (this.listSku.length > 0)
            {
                $.ajax({
                    type: 'GET',
                    url: this.getUrl,
                    data: {
                        sku: this.listSku,
                        name: this.listName
                    },
                    showLoader: true
                }).done(function (attributes) {
                    this.mapper = attributes;
                    this.setLinks(this.exports, 'exports');
                    this.getChildItems().forEach(function (data, index) {
                        this.addChild(data, this.startIndex + index);
                    }, this);
                }.bind(this));
            }

            return this;
        },

        /**
         * Rerender dynamic-rows elems
         */
        getListSku: function () {
            this.listSku = [];
            this.listName = [];

            if (typeof this.baseList !== 'undefined') {
                this.baseList.each(function (data, index) {
                    this.listSku.push(data.sku);
                    this.listName.push(data.name);
                }, this);
            }

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    this.listSku.push(data.sku);
                    this.listName.push(data.name);
                }, this);
            }
        },

        /**
         * Rerender dynamic-rows elems
         */
        reload: function () {
            this.clear();
            this.showSpinner(true);
            this.getChildItems().forEach(function (data, index) {
                this.addChild(data, this.startIndex + index);
            }, this);
        },

        /**
         * Rerender dynamic-rows elems
         */
        reloadList: function () {
            if (!this.reloadFlag)
                return;

            var oldSku = this.listSku;
            this.setLinks(this.imports, 'imports');
            this.getListSku();

            if (_.isEqual(oldSku, this.listSku) == false) {
                this.clear();

                // reset record Data
                this.recordData(
                    _.filter(this.recordData(), function (elem) {
                        return elem && this.listSku.includes(elem['sku']) && this.listSku.includes(elem['sku_dep']);
                    }, this)
                );

                if (this.listSku.length > 0) {
                    var that = this;
                    setTimeout(function () {
                        if (that.reloadFlag) {
                            that.reloadFlag = false;
                            $.ajax({
                                type: 'POST',
                                url: that.getUrl,
                                data: {
                                    sku: that.listSku,
                                    name: that.listName
                                },
                                showLoader: true
                            }).done(function (attributes) {
                                that.mapper = attributes;
                                that.setLinks(that.exports, 'exports');
                                that.getChildItems().forEach(function (data, index) {
                                    that.addChild(data, that.startIndex + index);
                                }, this);
                                that.reloadFlag = true;
                                that.showSpinner(false);
                            }.bind(that));
                        }
                    }, 3000);
                }
                else {
                    this.recordData([]);
                    this.showSpinner(false);
                }
            }
        },

        /**
         * Processing pages before addChild
         *
         * @param {Object} ctx - element context
         * @param {Number|String} index - element index
         * @param {Number|String} prop - additional property to element
         */
        processingAddChild: function (ctx, index, prop) {
            if (this.listSku.length > 0){
                this._super();
            }

            return this;
        },

        onReload: function () {
            this.trigger('reload');
        }
    });
});
