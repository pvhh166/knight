/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/form/element/abstract',
    'uiRegistry',
], function (Abstract,uiRegistry) {
    'use strict';

    return Abstract.extend({
        defaults: {
            links: {
                value: false
            }
        },

        /**
         * Delete record handler.
         *
         * @param {Number} index
         * @param {Number} id
         */
        deleteRecord: function (index, id) {
            // before delete
            uiRegistry.get('product_form.product_form.product_rule.rule_records').reloadFlag = false;
            uiRegistry.get('product_form.product_form.rule_options.rule_records').reloadFlag = false;

            this.bubble('deleteRecord', index, id);

            // after delete
            uiRegistry.get('product_form.product_form.product_rule.rule_records').reloadFlag = true;
            uiRegistry.get('product_form.product_form.product_rule.rule_records').reloadFromAddOn();
            uiRegistry.get('product_form.product_form.rule_options.rule_records').reloadFlag = true;
            uiRegistry.get('product_form.product_form.rule_options.rule_records').reloadList();
        }
    });
});
