/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'Magento_Ui/js/form/components/insert-listing',
    'mageUtils',
    'uiRegistry',
    'underscore'
], function ($, Insert, utils, uiRegistry, _) {
    'use strict';

    return Insert.extend({
        defaults: {
            externalListingName: '${ $.ns }.${ $.ns }',
            behaviourType: 'simple',
            listSku: [],
            externalFilterMode: false,
            requestConfig: {
                method: 'POST'
            },
            externalCondition: 'nin',
            settings: {
                edit: {
                    imports: {
                        'onChangeRecord': '${ $.editorProvider }:changed'
                    }
                },
                filter: {
                    exports: {
                        'requestConfig': '${ $.externalProvider }:requestConfig'
                    }
                }
            },
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                onSelectedChange: '${ $.selectionsProvider }:selected',
                'update_url': '${ $.externalProvider }:update_url',
                'indexField': '${ $.selectionsProvider }:indexField'
            },
            exports: {
                externalFiltersModifier: '${ $.externalProvider }:params.filters_modifier'
            },
            listens: {
                externalValue: 'updateExternalFiltersModifier updateSelections',
                indexField: 'initialUpdateListing'
            },
            modules: {
                selections: '${ $.selectionsProvider }',
                externalListing: '${ $.externalListingName }'
            }
        },

        /**
         * Updates externalValue, from selectionsProvider data
         * (which only stores data of the current page rows)
         *  + from already saved data
         *  so we can avoid request to server
         *
         * @param {Array} selected - ids of selected rows
         * @param {Object} rows
         */
        getSelectedRowsData: function (selected, rows) {
            var value,
                rowIds,
                valueIds;

            if (!selected || !selected.length) {
                return;
            }

            rowIds = _.pluck(rows, this.indexField);
            valueIds = _.pluck(value, this.indexField);

            value = _.map(selected, function (item) {
                if (_.contains(rowIds, item)) {
                    return _.find(rows, function (row) {
                        return row[this.indexField] === item;
                    }, this);
                } else if (_.contains(valueIds, item)) {
                    return _.find(value, function (row) {
                        return row[this.indexField] === item;
                    }, this);
                }
            }, this);

            return value;
        },

        /**
         * Updates external value, then updates value from external value
         *
         */
        save: function () {
            var selecteds,
                skus,
                exits = false,
                message = uiRegistry.get('index = information-block-addon');

            selecteds = this.getSelectedRowsData(this.selections().getSelections().selected,this.selections().rows());
            skus = _.pluck(selecteds, 'sku');

            this.listSku = [];

            if (typeof this.baseList !== 'undefined') {
                this.baseList.each(function (data, index) {
                    this.listSku.push(data.sku);
                }, this);
            }

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    this.listSku.push(data.sku);
                }, this);
            }

            skus.each(function (data, index) {
                if (this.listSku.includes(data))
                {
                    exits = data;
                    return;
                }
            }, this);

            if (exits){
                message.set('content','This product with sku '+exits+' is exists on Base Product or Addon Products!');
                message.set('visible',true);
                return;
            }

            this.updateExternalValue().done(
                function () {
                    if (!this.realTimeLink) {
                        this.updateValue();
                    }
                }.bind(this)
            );

            uiRegistry.get('product_form.product_form.related.modal_addon').closeModal();
            message.set('content','');
            message.set('visible',false);
        }
    });
});
