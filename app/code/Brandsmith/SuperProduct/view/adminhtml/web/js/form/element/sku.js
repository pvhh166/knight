/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'jquery',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'uiLayout'
], function (_, $, utils, registry, UiSelect, layout) {
    'use strict';

    return UiSelect.extend({
        defaults: {
            attributeElement: '${ $.parentName }.attribute',
            hiddenName: '${ $.parentName }.sku_hide',
            parentName: '${ $.parentName }',
            imports: {
                addonList: 'product_form.product_form_data_source:data.addon.super_selections',
                baseSku: 'product_form.product_form_data_source:data.baseproduct.super_selections.0',
                ruleRecords: 'product_form.product_form_data_source:data.rules.rule_records',
                mapper: 'product_form.product_form_data_source:data.rules.mapper_data',
            },
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            this._super();
            
            var parentGrid = registry.get('product_form.product_form.rule_options.rule_records'),
                parent = registry.get(this.parentName);

            if (typeof this.baseSku !== 'undefined') {
                this.options.push({"value":this.baseSku.sku,"label":this.baseSku.name});
            }

            if (typeof this.addonList !== 'undefined') {
                this.addonList.each(function (data, index) {
                    this.options.push({"value":data.sku,"label":data.name});
                }, this);
            }

            if (typeof this.ruleRecords[parent.recordId] !== 'undefined' && this.options().length > 0) {
                if (parentGrid.listSku.indexOf(this.ruleRecords[parent.recordId].sku_hide) > -1){
                    this.value(this.ruleRecords[parent.recordId].sku_hide);
                }
                else{
                    this.value(this.options()[0].value);
                }
            }

            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            var parentGrid = registry.get('product_form.product_form.rule_options.rule_records');
            parentGrid.onReload();
            this.setDependentOptions(value);
            this.setHideValue(value);
            return this._super();
        },

        /**
         * Set options to dependent select
         *
         * @param {String} value
         */
        setDependentOptions: function (value) {
            var field = registry.get(this.attributeElement);

            if (value == '')
                return this;
            
            if (field){
                field.setOptions(this.mapper[value]);
            }

            return this;
        },

        /**
         * Set options to dependent select
         *
         * @param {String} value
         */
        setHideValue: function (value) {
            var field = registry.get(this.hiddenName);

            if (value == '')
                return this;

            if (typeof field !== 'undefined') {
                field.value(value);
            }
            
            return this;
        }
    });
});
