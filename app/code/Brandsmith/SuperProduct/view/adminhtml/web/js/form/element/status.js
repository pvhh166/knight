/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/form/element/abstract',
    'uiRegistry',
    'ko'
], function (Abstract, registry, ko) {
    'use strict';

    return Abstract.extend({
        defaults: {
            isReady: false,
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            this._super();
            if (this.value() == 'Ready')
                this.isReady(true);
            else this.isReady(false);
            
            return this;
        },

        /**
         * Set list of observable attributes
         * @returns {exports.initObservable}
         */
        initObservable: function () {
            var self = this;

            this._super()
                .observe(['isReady','value']);

            return this;
        },

        /**
         * Calls 'onUpdate' method of parent, if value is defined and instance's
         *     'unique' property set to true, calls 'setUnique' method
         *
         * @return {Object} - reference to instance
         */
        onUpdate: function (value) {
            console.log(value);
            if (value == 'Ready')
                this.isReady(true);
            else this.isReady(false);
            return this._super();
        }
    });
});
