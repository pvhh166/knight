/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/components/html'
], function ($, _, registry, Component) {
    'use strict';

    return Component.extend({
        defaults: {
            content:        '',
            showSpinner:    false,
            loading:        false,
            visible:        true,
            error:          false,
            template:       'ui/content/content',
            additionalClasses: {},
            imports: {
                pngRecords: 'product_form.product_form_data_source:data.png.png_records',
                pngRules: 'product_form.product_form_data_source:data.pngrules.rule_records',
                baseList: 'product_form.product_form_data_source:data.baseproduct.super_selections',
                sku: 'product_form.product_form_data_source:data.product.sku',
            },
            ignoreTmpls: {
                content: true
            }
        },

        /**
         * Sets loading property to true, makes ajax call
         *
         * @return {Object} - reference to instance
         */
        loadDataWithParamter: function (parent) {
            var baseSku = '',
                baseAttribute = '',
                ruleAttribute = '',
                sku = this.pngRecords[registry.get(parent).recordId].sku_hide,
                attribute = this.pngRecords[registry.get(parent).recordId].attribute_hide,
                multi_layer = this.pngRecords[registry.get(parent).recordId].multi_layer;

            if (typeof this.baseList !== 'undefined') {
                this.baseList.each(function (data, index) {
                    baseSku = data.sku;
                    baseAttribute = data.base_attribute;
                }, this);
            }

            if (typeof this.pngRules !== 'undefined') {
                this.pngRules.each(function (data, index) {
                    if (sku == data.sku && attribute == data.attribute_dep)
                        ruleAttribute = data.attribute;
                }, this);
            }

            this.loading(true);

            this.ajaxConfig = {
                url: this.url,
                data: {
                    FORM_KEY: window.FORM_KEY,
                    base_sku: baseSku,
                    base_attribute: baseAttribute,
                    rule_attribute: ruleAttribute,
                    multi_layer: multi_layer,
                    sku: sku,
                    super_sku: this.sku,
                    attribute: attribute
                },
                success:    this.onDataLoaded
            };

            $.ajax(this.ajaxConfig);

            return this;
        },
    });
});
