/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/lib/view/utils/async',
    'uiCollection',
    'uiRegistry',
    'underscore',
    'Magento_Ui/js/modal/modal-component'
], function ($, Collection, registry, _, modal) {
    'use strict';

    return modal.extend({
        defaults: {
            status: 'Not ready',
            //listens: {
            //    'product_form.product_form.rule_options.rule_records:reload': 'reloadStatus',
            //},
            imports: {
                png: 'product_form.product_form_data_source:data.png',
                png_records: 'product_form.product_form_data_source:data.png.png_records',
            },
        },

        /**
         * Save modal
         */
        reloadStatus: function () {
            var recordId = -1;
            for (var index in this.png) {
                if (index != 'png_records') {
                    var sku, attribute, status = 'Ready';

                    this.png[index].each(function (com) {
                        sku = com.sku;
                        attribute = com.attribute;
                        for (var i in com) {
                            if (i.startsWith("image")) {
                                if (com[i].length == 0)
                                    status = 'Not ready';
                            }
                        }
                    }, this);

                    for (var i in this.png_records) {
                        if (this.png_records[i].sku_hide == sku && this.png_records[i].attribute_hide == attribute) {
                            this.status = status;
                            recordId = i;
                        }
                    }

                    if (recordId != -1) {
                        var exports = {status: 'product_form.product_form_data_source:data.png.png_records.' + recordId + '.status'};
                        this.setLinks(exports, 'exports');
                    }
                }
            }
        },

        /**
         * Save modal
         */
        saveModal: function () {
            this.reloadStatus();

            if (this.modal) {
                this.state(false);
            } else {
                this.waitCbk = this.closeModal;
            }
        },
    });
});
