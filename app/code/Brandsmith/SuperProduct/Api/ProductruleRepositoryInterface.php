<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api;

/**
 * @api
 */
interface ProductruleRepositoryInterface
{
    /**
     * Save Product Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule);

    /**
     * Retrieve Product Rule
     *
     * @param int $productruleId
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($productruleId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId);

    /**
     * Retrieve Product Rules matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Product Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule);

    /**
     * Delete Product Rule by ID.
     *
     * @param int $productruleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productruleId);
}
