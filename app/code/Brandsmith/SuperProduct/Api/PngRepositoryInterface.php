<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api;

/**
 * @api
 */
interface PngRepositoryInterface
{
    /**
     * Save PNG.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterface $png
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\PngInterface $png);

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProduct($superProductId, $sku, $attribute, $baseAttribute, $multi_layer);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductIdView($superProductId, $sku, $attribute, $baseAttribute);


    /**
     * Retrieve PNG
     *
     * @param int $pngId
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pngId);

    /**
     * Retrieve PNG matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete PNG.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterface $png
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\PngInterface $png);

    /**
     * Delete PNG by ID.
     *
     * @param int $pngId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pngId);
}
