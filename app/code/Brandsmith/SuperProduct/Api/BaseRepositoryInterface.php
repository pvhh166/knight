<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api;

/**
 * @api
 */
interface BaseRepositoryInterface
{
    /**
     * Save Base.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterface $base
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\BaseInterface $base);

    /**
     * Retrieve Base
     *
     * @param int $baseId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($baseId);

    /**
     * Retrieve productId
     *
     * @param int $productId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($baseId);

    /**
     * Retrieve Bases matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Base.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterface $base
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\BaseInterface $base);

    /**
     * Delete Base by ID.
     *
     * @param int $baseId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($baseId);
}
