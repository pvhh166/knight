<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api;

/**
 * @api
 */
interface PngruleRepositoryInterface
{
    /**
     * Save PNG Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule);

    /**
     * Retrieve PNG Rule
     *
     * @param int $pngruleId
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pngruleId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId);

    /**
     * Retrieve Rule
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId);

    /**
     * Retrieve PNG Rules matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete PNG Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule);

    /**
     * Delete PNG Rule by ID.
     *
     * @param int $pngruleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pngruleId);
}
