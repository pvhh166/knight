<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface PngInterface
{
    /**
     * ID
     *
     * @var string
     */
    const PNG_ID = 'png_id';

    /**
     * Super Product attribute constant
     *
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Sku attribute constant
     *
     * @var string
     */
    const SKU = 'sku';

    /**
     * Attribute attribute constant
     *
     * @var string
     */
    const ATTRIBUTE = 'attribute';

    /**
     * Base Attribute attribute constant
     *
     * @var string
     */
    const BASE_ATTRIBUTE = 'base_attribute';

    /**
     * Default Option attribute constant
     *
     * @var string
     */
    const DEFAULT_OPTION = 'default_option';

    /**
     * Multi Layer attribute constant
     *
     * @var string
     */
    const MULTI_LAYER = 'multi_layer';

    /**
     * Status attribute constant
     *
     * @var string
     */
    const STATUS = 'status';

    /**
     * Position attribute constant
     *
     * @var string
     */
    const POSITION = 'position';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getPngId();

    /**
     * Set ID
     *
     * @param int $pngId
     * @return PngInterface
     */
    public function setPngId($pngId);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return PngInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Sku
     *
     * @return mixed
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param mixed $sku
     * @return PngInterface
     */
    public function setSku($sku);

    /**
     * Get Attribute
     *
     * @return mixed
     */
    public function getAttribute();

    /**
     * Set Attribute
     *
     * @param mixed $attribute
     * @return PngInterface
     */
    public function setAttribute($attribute);

    /**
     * Get Base Attribute
     *
     * @return mixed
     */
    public function getBaseAttribute();

    /**
     * Set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return PngInterface
     */
    public function setBaseAttribute($baseAttribute);

    /**
     * Get Default Option
     *
     * @return mixed
     */
    public function getDefaultOption();

    /**
     * Set Default Option
     *
     * @param mixed $defaultOption
     * @return PngInterface
     */
    public function setDefaultOption($defaultOption);

    /**
     * Get Multi Layer
     *
     * @return mixed
     */
    public function getMultiLayer();

    /**
     * Set Multi Layer
     *
     * @param mixed $defaultOption
     * @return PngInterface
     */
    public function setMultiLayer($multiLayer);

    /**
     * Get Status
     *
     * @return mixed
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param mixed $status
     * @return PngInterface
     */
    public function setStatus($status);

    /**
     * Get Position
     *
     * @return mixed
     */
    public function getPosition();

    /**
     * Set Position
     *
     * @param mixed $position
     * @return PngInterface
     */
    public function setPosition($position);
}
