<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface PnglayerInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const PNGLAYER_ID = 'pnglayer_id';

    /**
     * Super Product attribute constant
     * 
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Product attribute constant
     * 
     * @var string
     */
    const PRODUCT_ID = 'product_id';

    /**
     * Attribute attribute constant
     * 
     * @var string
     */
    const ATTRIBUTE = 'attribute';

    /**
     * Base Attribute attribute constant
     * 
     * @var string
     */
    const BASE_ATTRIBUTE = 'base_attribute';

    /**
     * Rule Attribute attribute constant
     *
     * @var string
     */
    const RULE_ATTRIBUTE = 'rule_attribute';

    /**
     * Option attribute constant
     * 
     * @var string
     */
    const OPTION = 'option';

    /**
     * Base Option attribute constant
     * 
     * @var string
     */
    const BASE_OPTION = 'base_option';

    /**
     * Rule Option attribute constant
     *
     * @var string
     */
    const RULE_OPTION = 'rule_option';

    /**
     * AREA attribute constant
     *
     * @var string
     */
    const AREA = 'area';

    /**
     * Image attribute constant
     * 
     * @var string
     */
    const IMAGE = 'image';

    /**
     * Is Default attribute constant
     * 
     * @var string
     */
    const IS_DEFAULT = 'is_default';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getPnglayerId();

    /**
     * Set ID
     *
     * @param int $pnglayerId
     * @return PnglayerInterface
     */
    public function setPnglayerId($pnglayerId);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return PnglayerInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Product
     *
     * @return mixed
     */
    public function getProductId();

    /**
     * Set Product
     *
     * @param mixed $productId
     * @return PnglayerInterface
     */
    public function setProductId($productId);

    /**
     * Get Attribute
     *
     * @return mixed
     */
    public function getAttribute();

    /**
     * Set Attribute
     *
     * @param mixed $attribute
     * @return PnglayerInterface
     */
    public function setAttribute($attribute);

    /**
     * Get Base Attribute
     *
     * @return mixed
     */
    public function getBaseAttribute();

    /**
     * Set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return PnglayerInterface
     */
    public function setBaseAttribute($baseAttribute);

    /**
     * Get Rule Attribute
     *
     * @return mixed
     */
    public function getRuleAttribute();

    /**
     * Set Rule Attribute
     *
     * @param mixed $ruleAttribute
     * @return PnglayerInterface
     */
    public function setRuleAttribute($ruleAttribute);

    /**
     * Get Option
     *
     * @return mixed
     */
    public function getOption();

    /**
     * Set Option
     *
     * @param mixed $option
     * @return PnglayerInterface
     */
    public function setOption($option);

    /**
     * Get Base Option
     *
     * @return mixed
     */
    public function getBaseOption();

    /**
     * Set Base Option
     *
     * @param mixed $baseOption
     * @return PnglayerInterface
     */
    public function setBaseOption($baseOption);

    /**
     * Get Rule Option
     *
     * @return mixed
     */
    public function getRuleOption();

    /**
     * Set Rule Option
     *
     * @param mixed $ruleOption
     * @return PnglayerInterface
     */
    public function setRuleOption($ruleOption);

    /**
     * Get Image
     *
     * @return mixed
     */
    public function getImage();

    /**
     * Set Image
     *
     * @param mixed $image
     * @return PnglayerInterface
     */
    public function setImage($image);

    /**
     * Get Is Default
     *
     * @return mixed
     */
    public function getIsDefault();

    /**
     * Set Is Default
     *
     * @param mixed $isDefault
     * @return PnglayerInterface
     */
    public function setIsDefault($isDefault);

    /**
     * set Area
     *
     * @param mixed $area
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setArea($area);

    /**
     * get Rule Option
     *
     * @return string
     */
    public function getArea();

    /**
     * Get Image URL
     *
     * @return string
     */
    public function getImageUrl();
}
