<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface PngruleInterface
{
    /**
     * ID
     *
     * @var string
     */
    const PNGRULE_ID = 'pngrule_id';

    /**
     * Super Product attribute constant
     *
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Sku attribute constant
     *
     * @var string
     */
    const SKU = 'sku';

    /**
     * Attribute attribute constant
     *
     * @var string
     */
    const ATTRIBUTE = 'attribute';

    /**
     * Dependent Attribute attribute constant
     *
     * @var string
     */
    const ATTRIBUTE_DEP = 'attribute_dep';

    /**
     * Position attribute constant
     *
     * @var string
     */
    const POSITION = 'position';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getPngruleId();

    /**
     * Set ID
     *
     * @param int $pngruleId
     * @return PngruleInterface
     */
    public function setPngruleId($pngruleId);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return PngruleInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Sku
     *
     * @return mixed
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param mixed $sku
     * @return PngruleInterface
     */
    public function setSku($sku);

    /**
     * Get Attribute
     *
     * @return mixed
     */
    public function getAttribute();

    /**
     * Set Attribute
     *
     * @param mixed $attribute
     * @return PngruleInterface
     */
    public function setAttribute($attribute);

    /**
     * Get Dependent Attribute
     *
     * @return mixed
     */
    public function getAttributeDep();

    /**
     * Set Dependent Attribute
     *
     * @param mixed $attributeDep
     * @return PngruleInterface
     */
    public function setAttributeDep($attributeDep);

    /**
     * Get Position
     *
     * @return mixed
     */
    public function getPosition();

    /**
     * Set Position
     *
     * @param mixed $position
     * @return PngruleInterface
     */
    public function setPosition($position);
}
