<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface BaseInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const BASE_ID = 'base_id';

    /**
     * Name attribute constant
     * 
     * @var string
     */
    const NAME = 'name';

    /**
     * Super Product attribute constant
     * 
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Base Attribute attribute constant
     * 
     * @var string
     */
    const BASE_ATTRIBUTE = 'base_attribute';

    /**
     * Product attribute constant
     * 
     * @var string
     */
    const PRODUCT_ID = 'product_id';

    /**
     * Sku attribute constant
     * 
     * @var string
     */
    const SKU = 'sku';

    /**
     * Type attribute constant
     * 
     * @var string
     */
    const TYPE = 'type';

    /**
     * Required attribute constant
     * 
     * @var string
     */
    const REQUIRED = 'required';

    /**
     * Qty attribute constant
     * 
     * @var string
     */
    const QTY = 'qty';

    /**
     * User Defined attribute constant
     * 
     * @var string
     */
    const USER_DEFINED = 'user_defined';

    /**
     * SIS attribute constant
     * 
     * @var string
     */
    const SIS = 'sis';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getBaseId();

    /**
     * Set ID
     *
     * @param int $baseId
     * @return BaseInterface
     */
    public function setBaseId($baseId);

    /**
     * Get Name
     *
     * @return mixed
     */
    public function getName();

    /**
     * Set Name
     *
     * @param mixed $name
     * @return BaseInterface
     */
    public function setName($name);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return BaseInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Base Attribute
     *
     * @return mixed
     */
    public function getBaseAttribute();

    /**
     * Set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return BaseInterface
     */
    public function setBaseAttribute($baseAttribute);

    /**
     * Get Product
     *
     * @return mixed
     */
    public function getProductId();

    /**
     * Set Product
     *
     * @param mixed $productId
     * @return BaseInterface
     */
    public function setProductId($productId);

    /**
     * Get Sku
     *
     * @return mixed
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param mixed $sku
     * @return BaseInterface
     */
    public function setSku($sku);

    /**
     * Get Type
     *
     * @return mixed
     */
    public function getType();

    /**
     * Set Type
     *
     * @param mixed $type
     * @return BaseInterface
     */
    public function setType($type);

    /**
     * Get Required
     *
     * @return mixed
     */
    public function getRequired();

    /**
     * Set Required
     *
     * @param mixed $required
     * @return BaseInterface
     */
    public function setRequired($required);

    /**
     * Get Qty
     *
     * @return mixed
     */
    public function getQty();

    /**
     * Set Qty
     *
     * @param mixed $qty
     * @return BaseInterface
     */
    public function setQty($qty);

    /**
     * Get User Defined
     *
     * @return mixed
     */
    public function getUserDefined();

    /**
     * Set User Defined
     *
     * @param mixed $userDefined
     * @return BaseInterface
     */
    public function setUserDefined($userDefined);
    
    /**
     * Get SIS
     *
     * @return mixed
     */
    public function getSis();

    /**
     * Set SIS
     *
     * @param mixed $sis
     * @return AddonInterface
     */
    public function setSis($sis);
}
