<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface PngSearchResultInterface
{
    /**
     * Get PNG list.
     *
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface[]
     */
    public function getItems();

    /**
     * Set PNG list.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
