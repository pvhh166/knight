<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface RuleInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const RULE_ID = 'rule_id';

    /**
     * Super Product attribute constant
     * 
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Sku attribute constant
     * 
     * @var string
     */
    const SKU = 'sku';

    /**
     * Attribute attribute constant
     * 
     * @var string
     */
    const ATTRIBUTE = 'attribute';

    /**
     * Option attribute constant
     * 
     * @var string
     */
    const OPTION = 'option';

    /**
     * Condition attribute constant
     * 
     * @var string
     */
    const CONDITION = 'condition';

    /**
     * Dependent Sku attribute constant
     * 
     * @var string
     */
    const SKU_DEP = 'sku_dep';

    /**
     * Dependent Attribute attribute constant
     * 
     * @var string
     */
    const ATTRIBUTE_DEP = 'attribute_dep';

    /**
     * Dependent Option attribute constant
     * 
     * @var string
     */
    const OPTION_DEP = 'option_dep';

    /**
     * Dependent Condition attribute constant
     * 
     * @var string
     */
    const CONDITION_DEP = 'condition_dep';

    /**
     * Position attribute constant
     * 
     * @var string
     */
    const POSITION = 'position';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getRuleId();

    /**
     * Set ID
     *
     * @param int $ruleId
     * @return RuleInterface
     */
    public function setRuleId($ruleId);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return RuleInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Sku
     *
     * @return mixed
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param mixed $sku
     * @return RuleInterface
     */
    public function setSku($sku);

    /**
     * Get Attribute
     *
     * @return mixed
     */
    public function getAttribute();

    /**
     * Set Attribute
     *
     * @param mixed $attribute
     * @return RuleInterface
     */
    public function setAttribute($attribute);

    /**
     * Get Option
     *
     * @return mixed
     */
    public function getOption();

    /**
     * Set Option
     *
     * @param mixed $option
     * @return RuleInterface
     */
    public function setOption($option);

    /**
     * Get Condition
     *
     * @return mixed
     */
    public function getCondition();

    /**
     * Set Condition
     *
     * @param mixed $condition
     * @return RuleInterface
     */
    public function setCondition($condition);

    /**
     * Get Dependent Sku
     *
     * @return mixed
     */
    public function getSkuDep();

    /**
     * Set Dependent Sku
     *
     * @param mixed $skuDep
     * @return RuleInterface
     */
    public function setSkuDep($skuDep);

    /**
     * Get Dependent Attribute
     *
     * @return mixed
     */
    public function getAttributeDep();

    /**
     * Set Dependent Attribute
     *
     * @param mixed $attributeDep
     * @return RuleInterface
     */
    public function setAttributeDep($attributeDep);

    /**
     * Get Dependent Option
     *
     * @return mixed
     */
    public function getOptionDep();

    /**
     * Set Dependent Option
     *
     * @param mixed $optionDep
     * @return RuleInterface
     */
    public function setOptionDep($optionDep);

    /**
     * Get Dependent Condition
     *
     * @return mixed
     */
    public function getConditionDep();

    /**
     * Set Dependent Condition
     *
     * @param mixed $conditionDep
     * @return RuleInterface
     */
    public function setConditionDep($conditionDep);

    /**
     * Get Position
     *
     * @return mixed
     */
    public function getPosition();

    /**
     * Set Position
     *
     * @param mixed $position
     * @return RuleInterface
     */
    public function setPosition($position);
}
