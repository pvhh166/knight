<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api\Data;

/**
 * @api
 */
interface ProductruleInterface
{
    /**
     * ID
     *
     * @var string
     */
    const PRODUCTRULE_ID = 'productrule_id';

    /**
     * Super Product attribute constant
     *
     * @var string
     */
    const SUPER_PRODUCT_ID = 'super_product_id';

    /**
     * Sku attribute constant
     *
     * @var string
     */
    const SKU = 'sku';

    /**
     * Condition attribute constant
     *
     * @var string
     */
    const CONDITION = 'condition';

    /**
     * Dependent Sku attribute constant
     *
     * @var string
     */
    const SKU_DEP = 'sku_dep';

    /**
     * Position attribute constant
     *
     * @var string
     */
    const POSITION = 'position';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getProductruleId();

    /**
     * Set ID
     *
     * @param int $productruleId
     * @return ProductruleInterface
     */
    public function setProductruleId($productruleId);

    /**
     * Get Super Product
     *
     * @return mixed
     */
    public function getSuperProductId();

    /**
     * Set Super Product
     *
     * @param mixed $superProductId
     * @return ProductruleInterface
     */
    public function setSuperProductId($superProductId);

    /**
     * Get Sku
     *
     * @return mixed
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param mixed $sku
     * @return ProductruleInterface
     */
    public function setSku($sku);

    /**
     * Get Condition
     *
     * @return mixed
     */
    public function getCondition();

    /**
     * Set Condition
     *
     * @param mixed $condition
     * @return ProductruleInterface
     */
    public function setCondition($condition);

    /**
     * Get Dependent Sku
     *
     * @return mixed
     */
    public function getSkuDep();

    /**
     * Set Dependent Sku
     *
     * @param mixed $skuDep
     * @return ProductruleInterface
     */
    public function setSkuDep($skuDep);

    /**
     * Get Position
     *
     * @return mixed
     */
    public function getPosition();

    /**
     * Set Position
     *
     * @param mixed $position
     * @return ProductruleInterface
     */
    public function setPosition($position);
}
