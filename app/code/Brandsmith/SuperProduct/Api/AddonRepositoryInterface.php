<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Api;

/**
 * @api
 */
interface AddonRepositoryInterface
{
    /**
     * Save Addon.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterface $addon
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\AddonInterface $addon);

    /**
     * Retrieve Addon
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($addonId);

    /**
     * Retrieve Addon
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($productId,$superProductId);

    /**
     * Retrieve Addon
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId);

    /**
     * Retrieve Addons matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Addon.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterface $addon
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\AddonInterface $addon);

    /**
     * Delete Addon by ID.
     *
     * @param int $addonId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($addonId);
}
