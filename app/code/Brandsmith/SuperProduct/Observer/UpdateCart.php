<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Observer;

use Magento\Catalog\Api\Data\ProductCustomOptionInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Bundle
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpdateCart implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $itemsVisible = $quote->getAllVisibleItems();
        $items = $quote->getAllItems();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach ($itemsVisible as $item) {
            if ($item->getProduct()->getTypeId() == 'super'){
                $totalPrice = 0;
                foreach ($items as $i) {
                    if ($i->getParentItemId() == $item->getId()){
                        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($i->getProductId());
                        $finalPrice = $product->getPriceModel()->getFinalPrice($item->getQty()*$i->getQty(),$product);
                        $totalPrice = $totalPrice + $finalPrice;
                    }
                    else continue;
                }
                if($totalPrice != 0){
                    $item->setCustomPrice($totalPrice);
                    $item->setOriginalCustomPrice($totalPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
            }
        }
    }
}
