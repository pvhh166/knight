<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Observer;

use Magento\Catalog\Api\Data\ProductCustomOptionInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Bundle
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Super implements ObserverInterface
{
    /**
     * @var ProductCustomOptionInterfaceFactory
     */
    protected $customOptionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Base repository
     * 
     * @var \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
     */
    protected $baseRepository;

    /**
     * Base factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory
     */
    protected $baseFactory;

    /**
     * Addon repository
     * 
     * @var \Brandsmith\SuperProduct\Api\AddonRepositoryInterface
     */
    protected $addonRepository;

    /**
     * Addon factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory
     */
    protected $addonFactory;

    /**
     * Dependency Rule repository
     * 
     * @var \Brandsmith\SuperProduct\Api\RuleRepositoryInterface
     */
    protected $ruleRepository;

    /**
     * Dependency Rule factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory
     */
    protected $ruleFactory;

    /**
     * Dependency Rule repository
     *
     * @var \Brandsmith\SuperProduct\Api\ProductRuleRepositoryInterface
     */
    protected $productRuleRepository;

    /**
     * Dependency Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\ProductRuleInterfaceFactory
     */
    protected $productRuleFactory;

    /**
     * Dependency Rule repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface
     */
    protected $pngRuleRepository;

    /**
     * Dependency Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory
     */
    protected $pngRuleFactory;

    /**
     * PNG Layer repository
     * 
     * @var \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
     */
    protected $pnglayerRepository;

    /**
     * PNG Layer factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory
     */
    protected $pnglayerFactory;

    /**
     * PNG Layer repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngRepositoryInterface
     */
    protected $pngRepository;

    /**
     * PNG Layer factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory
     */
    protected $pngFactory;

    protected $csv;


    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @param RequestInterface $request
     * @param ProductRepository $productRepository
     * @param StoreManager $storeManager
     * @param \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseFactory
     * @param \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonFactory
     * @param \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory
     * @param \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productRuleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productRuleFactory
     * @param \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngRuleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngRuleFactory
     * @param \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory $pnglayerFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param ProductCustomOptionInterfaceFactory $customOptionFactory
     */
    public function __construct(
        RequestInterface $request,
        ProductRepository $productRepository,
        StoreManager $storeManager,
        \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository,
        \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseFactory,
        \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository,
        \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonFactory,
        \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository,
        \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory,
        \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productRuleRepository,
        \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productRuleFactory,
        \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngRuleRepository,
        \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngRuleFactory,
        \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository,
        \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory $pnglayerFactory,
        \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository,
        \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory $pngFactory,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        ProductCustomOptionInterfaceFactory $customOptionFactory
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->baseRepository    = $baseRepository;
        $this->baseFactory         = $baseFactory;
        $this->addonRepository   = $addonRepository;
        $this->addonFactory        = $addonFactory;
        $this->ruleRepository    = $ruleRepository;
        $this->ruleFactory         = $ruleFactory;
        $this->productRuleRepository = $productRuleRepository;
        $this->productRuleFactory = $productRuleFactory;
        $this->pngRuleRepository = $pngRuleRepository;
        $this->pngRuleFactory = $pngRuleFactory;
        $this->pnglayerRepository = $pnglayerRepository;
        $this->pnglayerFactory     = $pnglayerFactory;
        $this->pngFactory = $pngFactory;
        $this->pngRepository = $pngRepository;
        $this->csv = $csv;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->customOptionFactory = $customOptionFactory;
    }

    /**
     * Setting Bundle Items Data to product for further processing
     *
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $subject
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return \Magento\Catalog\Model\Product
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getData('product');
        $baseSku = false;
        $baseAttribute = false;
        if (isset($this->request->getPost('baseproduct')['super_selections'])) {
            foreach ($this->request->getPost('baseproduct')['super_selections'] as $key => $option) {
                if (empty($option['product_id'])) {
                    continue;
                }
                $base = $this->baseRepository->getBySuperProductId((int)$product->getId());
                if (!$base->getId())
                    $base = $this->baseFactory->create();
                $option['super_product_id'] = (int)$product->getId();
                $option['user_defined'] = 0;
                $this->dataObjectHelper->populateWithArray($base, $option, \Brandsmith\SuperProduct\Api\Data\BaseInterface::class);
                $this->baseRepository->save($base);
                $baseSku = $base->getSku();
                $baseAttribute = $base->getBaseAttribute();
            }
        }
        else
        {
            $base = $this->baseRepository->getBySuperProductId((int)$product->getId());
            if ($base->getId()) $this->baseRepository->delete($base);
        }

        if (isset($this->request->getPost('addon')['super_selections'])) {
            $products = [];
            foreach ($this->request->getPost('addon')['super_selections'] as $key => $option) {
                if (empty($option['product_id'])) {
                    continue;
                }
                $addon = $this->addonRepository->getBySuperProductId($option['product_id'],$product->getId());
                if (!$addon->getId())
                    $addon = $this->addonFactory->create();
                $option['super_product_id'] = (int)$product->getId();
                $this->dataObjectHelper->populateWithArray($addon, $option, \Brandsmith\SuperProduct\Api\Data\AddonInterface::class);
                $this->addonRepository->save($addon);
                $products[] = $option['product_id'];
            }

            $addons = $this->addonRepository->getListBySuperProductId((int)$product->getId());
            foreach ($addons as $key => $addon) {
                if (!in_array($addon->getProductId(), $products))
                    $this->addonRepository->delete($addon);
            }
        }
        else
        {
            $addons = $this->addonRepository->getListBySuperProductId((int)$product->getId());
            foreach ($addons as $key => $addon) {
                $this->addonRepository->delete($addon);
            }
        }

        if (isset($this->request->getPost('productrules')['rule_records'])) {
            $productRuleList = [];
            foreach ($this->request->getPost('productrules')['rule_records'] as $key => $option) {
                if (empty($option['sku'])) {
                    continue;
                }
                $productRule = $this->productRuleRepository->getBySuperProductId((int)$option['productrule_id'], $product->getId());
                if (!$productRule->getId()) {
                    $productRule = $this->productRuleFactory->create();
                    unset($option['productrule_id']);
                }

                $option['super_product_id'] = (int)$product->getId();
                $this->dataObjectHelper->populateWithArray($productRule, $option, \Brandsmith\SuperProduct\Api\Data\ProductruleInterface::class);
                $this->productRuleRepository->save($productRule);
                $productRuleList[] = $productRule->getId();
            }

            $productRules = $this->productRuleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($productRules as $key => $productRule) {
                if (!in_array($productRule->getId(), $productRuleList))
                    $this->productRuleRepository->delete($productRule);
            }
        } else {
            $productRules = $this->productRuleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($productRules as $key => $productRule) {
                $this->productRuleRepository->delete($productRule);
            }
        }

        if (isset($this->request->getPost('rules')['rule_records'])) {
            $ruleList = [];
            foreach ($this->request->getPost('rules')['rule_records'] as $key => $option) {
                if (empty($option['sku'])) {
                    continue;
                }
                $rule = $this->ruleRepository->getBySuperProductId((int)$option['rule_id'],$product->getId());
                if (!$rule->getId())
                {
                    $rule = $this->ruleFactory->create();
                    unset($option['rule_id']);
                }
                    
                $option['super_product_id'] = (int)$product->getId();
                $this->dataObjectHelper->populateWithArray($rule, $option, \Brandsmith\SuperProduct\Api\Data\RuleInterface::class);
                $this->ruleRepository->save($rule);
                $ruleList[] = $rule->getId();
            }

            $rules = $this->ruleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($rules as $key => $rule) {
                if (!in_array($rule->getId(), $ruleList))
                    $this->ruleRepository->delete($rule);
            }
        }
        else
        {
            $rules = $this->ruleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($rules as $key => $rule) {
                $this->ruleRepository->delete($rule);
            }
        }

        if (isset($this->request->getPost('pngrules')['rule_records'])) {
            $pngRuleList = [];
            foreach ($this->request->getPost('pngrules')['rule_records'] as $key => $option) {
                if (empty($option['sku'])) {
                    continue;
                }
                $pngRule = $this->pngRuleRepository->getBySuperProductId((int)$option['pngrule_id'], $product->getId());
                if (!$pngRule->getId()) {
                    $pngRule = $this->pngRuleFactory->create();
                    unset($option['pngrule_id']);
                }

                $option['super_product_id'] = (int)$product->getId();
                $this->dataObjectHelper->populateWithArray($pngRule, $option, \Brandsmith\SuperProduct\Api\Data\ProductruleInterface::class);
                $this->pngRuleRepository->save($pngRule);
                $pngRuleList[] = $pngRule->getId();
            }

            $pngRules = $this->pngRuleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($pngRules as $key => $pngRule) {
                if (!in_array($pngRule->getId(), $pngRuleList))
                    $this->pngRuleRepository->delete($pngRule);
            }
        } else {
            $pngRules = $this->pngRuleRepository->getListBySuperProductId((int)$product->getId());
            foreach ($pngRules as $key => $pngRule) {
                $this->pngRuleRepository->delete($pngRule);
            }
        }

        if ($baseAttribute && isset($this->request->getPost('png')['png_records'])) {
            $pngList = [];
            foreach ($this->request->getPost('png')['png_records'] as $key => $option) {
                $png = $this->pngRepository->getBySuperProduct($product->getId(), $option['sku_hide'], $option['attribute_hide'], $baseAttribute, $option['multi_layer']);
                if (!$png->getId()) {
                    $png = $this->pngFactory->create();
                }
                unset($option['png_id']);
                $option['super_product_id'] = (int)$product->getId();
                $option['sku'] = $option['sku_hide'];
                $option['attribute'] = $option['attribute_hide'];
                $option['base_attribute'] = $baseAttribute;
                $option['status'] = $option['status'] == 'Ready' ? 1 : 0;
                $this->dataObjectHelper->populateWithArray($png, $option, \Brandsmith\SuperProduct\Api\Data\pngInterface::class);
                $this->pngRepository->save($png);
                $pngList[] = $png->getId();
            }
            $pngs = $this->pngRepository->getListBySuperProductId((int)$product->getId());
            foreach ($pngs as $key => $png) {
                if (!in_array($png->getId(), $pngList))
                    $this->pngRepository->delete($png);
            }
        } else {
            $pngs = $this->pngRepository->getListBySuperProductId((int)$product->getId());
            foreach ($pngs as $key => $png) {
                $this->pngRepository->delete($png);
            }
        }

        $pngPostData = $this->request->getPost('png');
        if ($baseAttribute && isset($pngPostData) && count($pngPostData) > 0)
        {
            foreach ($pngPostData as $key => $png) {
                if ($key == 'png_records')
                    continue;

                if (!$baseAttribute)
                    continue;

                foreach ($png as $key => $layer) {
                    $baseOption = $this->getBaseOption($baseSku,$baseAttribute);
                    foreach ($baseOption as $optionId) {
                        $key = 'image'.$optionId;
                        $layer['option'] = $layer['option_hide'] == '0' ? $optionId : $layer['option_hide'];
                        $layer['area'] = $layer['area_hide'];
                        $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$product->getId(), $layer['product_id'], $layer['attribute'], $baseAttribute, $layer['rule_attribute'], $layer['option'], $optionId, $layer['rule_option'], $layer['area']);
                        if ($pngLayer->getId())
                        {
                            if (isset($layer[$key])) {
                                if (!isset($layer[$key][0]['name'])) {
                                    $this->pnglayerRepository->delete($pngLayer);
                                    continue;
                                } else {
                                    $image = isset($layer[$key][0]['file']) ? $layer[$key][0]['file'] : $layer[$key][0]['name'];
                                }
                            }
                            else
                            {
                                $this->pnglayerRepository->delete($pngLayer);
                                continue;
                            }
                        }
                        else
                        {
                            if (isset($layer[$key]))
                            {
                                if (!isset($layer[$key][0]['name'])) {
                                    continue;
                                } else {
                                    $pngLayer = $this->pnglayerFactory->create();
                                    $image = isset($layer[$key][0]['file']) ? $layer[$key][0]['file'] : $layer[$key][0]['name'];
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        $layer['super_product_id'] = (int)$product->getId();
                        $layer['base_option'] = $optionId;
                        $layer['base_attribute'] = $baseAttribute;
                        $layer['image'] = $image;
                        $this->dataObjectHelper->populateWithArray($pngLayer, $layer, \Brandsmith\SuperProduct\Api\Data\PnglayerInterface::class);
                        $this->pnglayerRepository->save($pngLayer);
                    }
                }
            }

            $pngLayers = $this->pnglayerRepository->getListBySuperProductIdNonOption((int)$product->getId(),$this->getBaseOption($baseSku,$baseAttribute));
            foreach ($pngLayers as $key => $pngLayer) {
                $this->pnglayerRepository->delete($pngLayer);
            }

            // $rules = $this->ruleRepository->getListBySuperProductId((int)$product->getId());
            // foreach ($rules as $key => $rule) {
            //     if ($rule->getSku() == $baseSku && $rule->getAttribute() == $baseAttribute) {
            //         try {
            //             $productDep = $this->productRepository->get($rule->getSkuDep());
            //             if ($productDep->getTypeId() == 'simple') {
            //                 if ($rule->getConditionDep() == 1) {
            //                     foreach ($this->getBaseOption($baseSku, $baseAttribute) as $optionId) {
            //                         if ($rule->getOption() != $optionId) {
            //                             $pngLayer = $this->pnglayerRepository->getOnePngBySuperProductId((int)$product->getId(), $productDep->getId(), $rule->getSkuDep(), $baseAttribute, $rule->getSkuDep(), $optionId);
            //                             if ($pngLayer->getId()) {
            //                                 $this->pnglayerRepository->delete($pngLayer);
            //                             }
            //                         }
            //                     }
            //                 } else if ($rule->getConditionDep() == 0) {
            //                     foreach ($this->getBaseOption($baseSku, $baseAttribute) as $optionId) {
            //                         if ($rule->getOption() == $optionId) {
            //                             $pngLayer = $this->pnglayerRepository->getOnePngBySuperProductId((int)$product->getId(), $productDep->getId(), $rule->getSkuDep(), $baseAttribute, $rule->getSkuDep(), $optionId);
            //                             if ($pngLayer->getId()) {
            //                                 $this->pnglayerRepository->delete($pngLayer);
            //                             }
            //                         }
            //                     }
            //                 }
            //             } else {
            //                 $baseOption = $this->getBaseOption($baseSku, $baseAttribute);
            //                 foreach ($baseOption as $optionId) {
            //                     if ($rule->getOption() == $optionId) {
            //                         if ($rule->getConditionDep() == 1) {
            //                             foreach ($this->getBaseOption($rule->getSkuDep(), $rule->getAttributeDep()) as $optionDepId) {
            //                                 if ($rule->getOptionDep() != $optionDepId) {
            //                                     $pngLayer = $this->pnglayerRepository->getOnePngBySuperProductId((int)$product->getId(), $productDep->getId(), $rule->getAttributeDep(), $baseAttribute, $optionDepId, $optionId);
            //                                     if ($pngLayer->getId()) {
            //                                         $this->pnglayerRepository->delete($pngLayer);
            //                                     }

            //                                 }
            //                             }
            //                         } else if ($rule->getConditionDep() == 0) {
            //                             foreach ($this->getBaseOption($rule->getSkuDep(), $rule->getAttributeDep()) as $optionDepId) {
            //                                 if ($rule->getOptionDep() == $optionDepId) {
            //                                     $pngLayer = $this->pnglayerRepository->getOnePngBySuperProductId((int)$product->getId(), $productDep->getId(), $rule->getAttributeDep(), $baseAttribute, $optionDepId, $optionId);
            //                                     if ($pngLayer->getId()) {
            //                                         $this->pnglayerRepository->delete($pngLayer);
            //                                     }

            //                                 }
            //                             }
            //                         }
            //                     }
            //                 }
            //             }
            //         } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            //             continue;
            //         }
            //     }
            // }
        }

        if (isset($this->request->getPost('import_upload')[0])) {
            $importFile = $this->request->getPost('import_upload')[0];
            $csvData = $this->csv->getData($importFile['path'] . $importFile['file']);
            foreach ($csvData as $row => $data) {
                if ($row > 0) {
                    $productDep = $this->productRepository->get($data[1]);
                    $baseOption = $product->getResource()->getAttribute($data[3])->getSource()->getOptionId($data[6]);
                    if ($productDep->getTypeId() == 'simple') {
                        $option = $data[5];
                        $rule_option = $data[7];
                    } else {
                        $option = $productDep->getResource()->getAttribute($data[2])->getSource()->getOptionId($data[5]);
                        if ($data[4] && $data[7])
                            $rule_option = $productDep->getResource()->getAttribute($data[4])->getSource()->getOptionId($data[7]);
                        else $rule_option = $data[7];
                    }
                    $tmpData = [
                        'super_product_id' => (int)$product->getId(),
                        'product_id' => $productDep->getId(),
                        'attribute' => $data[2],
                        'base_attribute' => $data[3],
                        'rule_attribute' => $data[4],
                        'option' => $option,
                        'base_option' => $baseOption,
                        'rule_option' => $rule_option,
                        'image' => $data[8],
                        'is_default' => $data[9],
                        'area' => $data[10],
                    ];
                    $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$product->getId(), $productDep->getId(), $data[2], $data[3], $data[4], $option, $baseOption, $rule_option, $data[10]);
                    if (!$pngLayer->getId()) {
                        $pngLayer = $this->pnglayerFactory->create();
                    }
                    if ($data[8]) {
                        $this->dataObjectHelper->populateWithArray($pngLayer, $tmpData, \Brandsmith\SuperProduct\Api\Data\PnglayerInterface::class);
                        $this->pnglayerRepository->save($pngLayer);
                    } else {
                        $this->pnglayerRepository->delete($pngLayer);
                    }
                }
            }
        }

        return $product;
    }

    public function getBaseOption($sku,$attributeCode)
    {
        $product = $this->productRepository->get($sku);
        $data = $product->getTypeInstance()->getConfigurableOptions($product);
        $options = [];
        $optionsList = [];
        foreach($data as $attrs){
            foreach ($attrs as $attribute) {
                if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList))
                {
                    $options[] = $attribute['value_index'];
                    $optionsList[] = $attribute['value_index'];
                }
            }
        }
        return $options;
    }
}
