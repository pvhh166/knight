<?php
/**
 * Brandsmith_SuperProduct extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  Brandsmith
 *                     @package   Brandsmith_SuperProduct
 *                     @copyright Copyright (c) 2019
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model\Product;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Bundle\Model\ResourceModel\Selection\Collection\FilterApplier as SelectionCollectionFilterApplier;
use Magento\Bundle\Model\ResourceModel\Selection\Collection as Selections;
use Magento\Catalog\Helper\Product as ProductHelper;

class Type extends \Magento\Catalog\Model\Product\Type\AbstractType
{
    const TYPE_ID = 'super';
    const TYPE_CODE = 'super';

    const IS_SALABLE = true;

    /**
     * Process modes
     *
     * Lite validation - only received options are validated
     */
    const PROCESS_MODE_LITE = 'lite';

    /**
     * Product object customization (not stored in DB)
     *
     * @var array
     */
    protected $_customOptions = [];

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_modelConfigurableType;

    /**
     * @var \Brandsmith\SuperProduct\Helper\Data
     */
    protected $_superProductHelper;

    /**
     * @var \Magento\Catalog\Model\Product\Configuration\Item\OptionFactory
     */
    protected $_itemOptionFactory;

    /**
     * @var \Magento\Checkout\Model\Cart\RequestInfoFilterInterface
     */
    protected $_requestInfoFilter;

    /**
     * @var \Brandsmith\SuperProduct\Model\BaseRepository
     */
    protected $_baseRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory
     */
    protected $_addonCollectionFactory;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngModelRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory
     */
    protected $_pngRuleCollectionFactory;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngruleRepository
     */
    protected $_pngRuleRepository;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    protected $_productHelper;

    public function __construct(
        \Magento\Catalog\Model\Product\Option $catalogProductOption,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\Product\Type $catalogProductType,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDb,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Registry $coreRegistry,
        \Psr\Log\LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        Json $serializer = null,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Brandsmith\SuperProduct\Helper\Data $superProductHelper,
        \Magento\Catalog\Model\Product\Configuration\Item\OptionFactory $itemOptionFactory,
        \Magento\Checkout\Model\Cart\RequestInfoFilterInterface $requestInfoFilter,
        \Brandsmith\SuperProduct\Model\BaseRepository $baseRepository,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\PngRepository $pngModelRepository,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $pngRuleCollectionFactory,
        \Brandsmith\SuperProduct\Model\PngruleRepository $PngRuleRepository,
        ProductHelper $productHelper
    ) {
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_superProductHelper = $superProductHelper;
        $this->_itemOptionFactory = $itemOptionFactory;
        $this->_requestInfoFilter = $requestInfoFilter;
        $this->_baseRepository = $baseRepository;
        $this->_addonCollectionFactory = $addonCollectionFactory;
        $this->priceCurrency = $priceCurrency;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pngModelRepository = $pngModelRepository;
        $this->_uploaderPool = $uploaderPool;
        $this->_pngRuleCollectionFactory = $pngRuleCollectionFactory;
        $this->_pngRuleRepository = $PngRuleRepository;
        $this->_productHelper = $productHelper;

        parent::__construct(
            $catalogProductOption,
            $eavConfig,
            $catalogProductType,
            $eventManager,
            $fileStorageDb,
            $filesystem,
            $coreRegistry,
            $logger,
            $productRepository,
            $serializer
        );
    }


    /**
     * {@inheritdoc}
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        // method intentionally empty
    }

    /**
     * Check is product available for sale
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function isSalable($product)
    {
        $salable = self::IS_SALABLE;

        return $salable;
    }

    /**
     * Get Base Product information by Super Product Id
     *
     */
    public function getBaseProductById($_superProductId)
    {
        try {
            if(!$_superProductId) {
                throw new \Magento\Framework\Exception\LocalizedException(__($_superProductId));
            }

            $_baseProductId = $this->_baseRepository->getBySuperProductId($_superProductId);
            return $_baseProductId;

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $e->getMessage();
        }

        return $this->getSpecifyProductMessage();
    }

    /**
     * Get Base Product information by Super Product Id
     *
     */
    public function getListsRequireAddOn($_super_product_id)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_super_product_id);
        $collection->addFieldToFilter('required', 1);
        $collection->setOrder('position','ASC');
        $collection->load();
        return $collection;
    }

    /**
     * Get Base Product information by Super Product Id
     *
     */
    public function getQtyAddOn($_super_product_id, $_product_id)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_super_product_id);
        $collection->addFieldToFilter('product_id', $_product_id);
        $collection->setOrder('position','ASC');
        $collection->load();
        $addon = $collection->getFirstItem();
        return $addon;
    }

    /**
     * Get Add-on Product information by Super Product Id
     *
     */
    public function getProductAddOn($_super_product_id, $_product_id)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_super_product_id);
        $collection->addFieldToFilter('product_id', $_product_id);
        $collection->setOrder('position','ASC');
        $collection->load();
        $addon = $collection->getFirstItem();
        return $addon;
    }

    /**
     * Get Base Product information by Super Product Id
     *
     */
    public function getMinExpectedPrice($_super_product_id)
    {
        $_baseProduct = $this->getBaseProductById($_super_product_id);
        $_minBaseRegularPrice = $this->getBaseProductPriceById($_baseProduct->getProductId());

        $_totalPrice = $_minBaseRegularPrice;
        $_productPrice = null;

        $listsRequireAddOn = $this->getListsRequireAddOn($_super_product_id);

        //  If Super Product has Add-on required
        if(count($listsRequireAddOn) > 0 ) {
            foreach($listsRequireAddOn as $_item) {
                $_productId = $_item->getProductId();
                $_product = $this->productRepository->getById($_productId);
                if($_item->getType() == 'simple') {
                    $_productPrice = $_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
                }

                if($_item->getType() == 'configurable') {
                    $_productPriceInfo = $_product->getPriceInfo()->getPrice('regular_price');
                    $_productPrice = $_productPriceInfo->getMinRegularAmount()->getValue();
                }

                $_totalPrice+= $_productPrice;
            }
        }


        return $_totalPrice;
    }

    /**
     * Min Regular Price of Base Product
     *
     */
    public function getBaseProductPriceById($_baseId)
    {
        $_product = $this->productRepository->getById($_baseId);
        $_minRegularPrice = $_product->getFinalPrice();

        return $_minRegularPrice;
    }

    public function getSelectionPrice($selectionProduct, $takeTierPrice, $selectionQty = null)
    {
        $selectionProduct = $this->productRepository->getById($selectionProduct);

        $price = $selectionProduct->getFinalPrice($takeTierPrice ? $selectionQty : 1);
        return $price;
    }

    public function getCustomProductPrice()
    {
        $_price = 0;
        return $_price;
    }

    public function prepareSuperProduct($buyRequest, $product)
    {
        $request = $this->_getProductRequest($buyRequest);

        try {
            if (is_string($request)) {
                throw new \Magento\Framework\Exception\LocalizedException(__($request));
            }

            $selections = [];
            $options = $request->getSuperOption();

            if (is_array($options)) {
                $selections['products_child'] = $this->getSelectionsByIds($options, $product);
            }

            if(count($selections) > 0) {

                $_totalPrice = $this->getSelectionTotalPrice($selections);

                $result = $this->_getProductRequest($selections);
                $result->setParentProductId($product->getId());
                $result->setTotalPrice($_totalPrice);

                return $result;
            }

        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $e->getMessage();
        }

        return $this->getSpecifyOptionMessage();
    }

    /**
     * Retrieve array Product Child
     *
     */
    public function getChildProduct($_super_product_id, $_super_product_attributes)
    {
        $_child_product = array();

        foreach ($_super_product_attributes as $_array) {

            $_simple_product = $_product_id = $_product_img = $_default_qty = null;

            if($_array['product_type'] == 'simple') {
                $_product_id = $_array['product_id'];
                $_simple_product = $this->productRepository->getById($_product_id);
                $_add_on = $this->getQtyAddOn($_super_product_id, $_product_id);
                $_add_on_data = $_add_on->getData();
                $_default_qty = $_add_on_data['qty'];
            }

            if($_array['product_type'] == 'configurable') {
                $_attribute = $_array['option_selected'];
                $_product_id = $_array['product_id'];
                $_product_sku = $_array['product_sku'];

                $_attribute_info = $this->_superProductHelper->getAttributesInfo($_attribute);
                //$_product = $this->productRepository->getById($_product_id);
                $_product = $this->productRepository->get($_product_sku);
                $_simple_product = $this->_modelConfigurableType->getProductByAttributes($_attribute_info, $_product);
                $_add_on = $this->getQtyAddOn($_super_product_id, $_product_id);

                //  If this Product is Add-on
                if(empty($_add_on->getData())) {
                    $_base = $this->getBaseProductById($_super_product_id);
                    $_default_qty = $_base->getQty();
                } else {
                    $_add_on_data = $_add_on->getData();
                    $_default_qty = $_add_on_data['qty'];
                }
            }

            if(!empty($_simple_product->getThumbnail())) {
                $_product_img = $this->_productHelper->getThumbnailUrl($_simple_product);
            }

            $_child_product[] = array(
                'simple_product_id' => $_simple_product->getId(),
                'simple_product_sku' =>  $_simple_product->getSku(),
                'product_thumbnail_img' => $_product_img,
                'default_qty' => $_default_qty,
                'default_product_id' => $_product_id
            );
        }

        return $_child_product;
    }


    /**
     * Retrieve message for base product
     *
     * @return \Magento\Framework\Phrase
     */
    public function getSpecifyProductMessage()
    {
        return __('Please try again.');
    }

    /**
     * Retrieve message for specify option(s)
     *
     * @return \Magento\Framework\Phrase
     */
    public function getSpecifyOptionMessage()
    {
        return __('Please specify super product option(s).');
    }

    /**
     * @param array $array
     * @return int[]|int[][]
     */
    public function recursiveIntval(array $array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = $this->recursiveIntval($value);
            } elseif (is_numeric($value) && (int)$value != 0) {
                $array[$key] = (int)$value;
            } else {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public function getSelectionsByIds($options, $product)
    {
        sort($options);

        $_addOn = array();
        foreach ($options as $option) {
            $_addOn[] = $this->getAddOnSuperProduct($option);
        }

        return $_addOn;
    }

    public function getAddOnSuperProduct($option)
    {
        $_position = null;
        $_selection_default_qty = null;
        $_optionsCollection = [];
        $_configProductId = $option['parent_id'];
        $_configProduct = $this->productRepository->getById($_configProductId);
        $_simpleProduct = $this->_modelConfigurableType->getProductByAttributes($option['current_option_selected'], $_configProduct);

        $_super_product_id = $option['super_product_id'];
        $_add_on_product_id = $option['parent_id'];

        $_base = $this->_superProductHelper->getBaseProductById($_super_product_id);
        $_base_product_id = $_base->getData('product_id');

        //  Detect Base Product
        if($_base_product_id == $option['parent_id']) {
            $_selection_default_qty = $_base->getData('qty');
            $_position = 0;
        }

        //  Detect Add On Product
        if($_base_product_id != $option['parent_id']) {
            $_addOn = $this->_superProductHelper->getAddOnById($_super_product_id, $_add_on_product_id);
            $_selection_default_qty = $_addOn->getData('qty');
            $_position = $_addOn->getData('position');
        }

        $_optionsCollection["entity_id"] = $_simpleProduct->getId();
        $_optionsCollection["type_id"] = $_simpleProduct->getTypeId();
        $_optionsCollection["sku"] = $_simpleProduct->getSku();
        $_optionsCollection["parent_product_id"] = $option['parent_id'];
        $_optionsCollection["super_product_id"] = $option['super_product_id'];
        $_optionsCollection["product_id"] = $_simpleProduct->getId();
        $_optionsCollection["product_regular_price"] = $_simpleProduct->getId();
        $_optionsCollection["product_final_price"] = $_simpleProduct->getId();
        $_optionsCollection["position"] = $_position;
        $_optionsCollection["selection_default_qty"] = $_selection_default_qty;
        $_optionsCollection["selection_can_change_qty"] = 0;
        $_optionsCollection["is_salable"] = 1;

        return $_optionsCollection;
    }

    public function getProductsSelectedId($selections)
    {
        $_productSelected = array();

        foreach ($selections as $selection) {
            $_productSelected = $selection['parent_product_id'];
        }

        return $_productSelected;
    }

    /**
     * @param array $_result
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkIsResult($_result)
    {
        if (is_string($_result)) {
            throw new \Magento\Framework\Exception\LocalizedException(__($_result));
        }

        if (!isset($_result[0])) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We can\'t add this item to your shopping cart right now.')
            );
        }
    }

    /**
     * Get request for product add to cart procedure
     *
     * @param   \Magento\Framework\DataObject|int|array $requestInfo
     * @return  \Magento\Framework\DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _getProductRequest($requestInfo)
    {
        if ($requestInfo instanceof \Magento\Framework\DataObject) {
            $request = $requestInfo;
        } elseif (is_numeric($requestInfo)) {
            $request = new \Magento\Framework\DataObject(['qty' => $requestInfo]);
        } elseif (is_array($requestInfo)) {
            $request = new \Magento\Framework\DataObject($requestInfo);
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We found an invalid request for adding product to quote.')
            );
        }
        $this->getRequestInfoFilter()->filter($request);

        return $request;
    }

    /**
     * Getter for RequestInfoFilter
     *
     * @deprecated 100.1.2
     * @return \Magento\Checkout\Model\Cart\RequestInfoFilterInterface
     */
    public function getRequestInfoFilter()
    {
        if ($this->_requestInfoFilter === null) {
            $this->_requestInfoFilter = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Checkout\Model\Cart\RequestInfoFilterInterface::class);
        }
        return $this->_requestInfoFilter;
    }

    public function getConvertAttributes($attributes_defined_option_selected)
    {
        // $attributes_defined_option_selected = attribute code
        $_attribute_info = $this->_superProductHelper->getConvertAttributesInfo($attributes_defined_option_selected);
        return $_attribute_info;
    }

    /**
     * Retrieve Rule.
     *
     */
    public function getProductRuleBySuperProductId($superProductId, $sku)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */

        $_checkCollectionProductRule = $this->getListProductRuleBySuperProductId($superProductId, $sku);

        //  If there is more than 1 Product Rule
        if(count($_checkCollectionProductRule) > 1) {
            $_data = array(
                'collection_count' => count($_checkCollectionProductRule),
                'product_rule' => $_checkCollectionProductRule
            );
        } else {
            $collection = $this->_productruleCollectionFactory->create();
            $collection->addFieldToFilter('super_product_id', $superProductId);
            $collection->addFieldToFilter('sku', $sku);

            if(count($collection->loadData()) > 0 ) {
                $rule = $collection->getFirstItem();
                $_duplicateRule = $rule->getDuplicateRule();
                $rule = $rule->getData();

                $_data = array(
                    'collection_count' => count($collection->loadData()),
                    'duplicate_rule' => $_duplicateRule,
                    'product_rule' => $this->_getProductRequest($rule)
                );
            } else {
                $_data = array(
                    'collection_count' => count($collection->loadData())
                );
            }
        }

        return $_data;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListProductRuleBySuperProductId($superProductId, $sku)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->setOrder('position','ASC');
        $collection->load();
        return $collection;
    }

    public function getPngDefaultOptions($superProductId, $sku, $attribute)
    {
        $_defaultOptionData = $this->_pngModelRepository->getDefaultOption($superProductId, $sku, $attribute);

        if( is_object($_defaultOptionData) && !empty($_defaultOptionData->getData()) ) {
            return $_defaultOptionData;
        }
        return null;
    }

    public function getBaseDefaultImageOptions($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption)
    {
        $_image = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption);

        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }

        return null;
    }

    public function getPngLayerImageUrl($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area)
    {
        $_image = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area);

        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }

        return null;
    }

    public function getPngLayerImageUrlWithRule($superProductId, $productId, $attribute, $_baseAttribute, $_PngDependencyRuleAttribute, $option, $baseOption, $_PngDependencyRuleDefaultOption, $area)
    {
        $_image = $this->_pnglayerRepository->getBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $_PngDependencyRuleAttribute, $option, $baseOption, $_PngDependencyRuleDefaultOption, $area);

        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }

        return null;
    }

    public function getAddOnDefaultOptions($_listsRequireAddOn, $super_product_id)
    {
        $attribute = null;
        $addOnDefaultOptions = array();
        $_listsRequireAddOnData = $_listsRequireAddOn->getData();

        foreach ($_listsRequireAddOnData as $_listsRequireAddOn) {
            $sku = $_listsRequireAddOn['sku'];
            $_defaultOptions = $this->getPngDefaultOptions($super_product_id, $sku, $attribute);
            if($_defaultOptions) {
                $_defaultOptionsData = $_defaultOptions->getData();

                foreach ($_defaultOptionsData as $_defaultOptions) {
                    $addOnDefaultOptions[] = array (
                        'super_product_id' => $_defaultOptions['super_product_id'],
                        'sku' => $_defaultOptions['sku'],
                        'product_id' => $_listsRequireAddOn['product_id'],
                        'attribute' => $_defaultOptions['attribute'],
                        'base_attribute' => $_defaultOptions['base_attribute'],
                        'default_option' => $_defaultOptions['default_option'],
                        'position' => $_defaultOptions['position'],
                        'multi_layer' => $_defaultOptions['multi_layer']
                    );
                }
            }

        }

        return $addOnDefaultOptions;
    }

    public function getPngDependencyRule($superProductId, $sku, $attributeCodeDependency)
    {
        $_pngRule = $this->_pngRuleRepository->getByAttributeDependency($superProductId, $sku, $attributeCodeDependency);

        if( is_object($_pngRule) && !empty($_pngRule->getData()) ) {
            $_ruleAttribute = $_pngRule->getAttribute();
            $_defaultOptionData = $this->_pngModelRepository->getDefaultOptionFirstItem($superProductId, $sku, $_ruleAttribute);
            return $_defaultOptionData;
        }

        return null;
    }

    public function getListImagesAddOnRequire($super_product_id, $base_attribute_code_id)
    {
        $_listsRequireAddOn = $this->getListsRequireAddOn($super_product_id);
        $addOnDefaultOptions = $this->getAddOnDefaultOptions($_listsRequireAddOn, $super_product_id);

        $_imgArray = array();

        foreach( $addOnDefaultOptions as $_addOnDefault ) {
            $array_image = [];
            $superProductId = $_addOnDefault['super_product_id'];
            $_sku = $_addOnDefault['sku'];
            $productId = $_addOnDefault['product_id'];
            $attribute = $_addOnDefault['attribute'];
            $_baseAttribute = $_addOnDefault['base_attribute'];
            $option = $_addOnDefault['default_option'];
            $_position = $_addOnDefault['position'];
            $area = $_addOnDefault['multi_layer'];
            $_PngDependencyRule = $this->getPngDependencyRule($superProductId, $_sku, $attribute);

            if($_PngDependencyRule != false) {
                $_PngDependencyRuleAttribute = $_PngDependencyRule->getAttribute();
                $_PngDependencyRuleDefaultOption = $_PngDependencyRule->getDefaultOption();
                $_imgAddOnUrl = $this->getPngLayerImageUrlWithRule($superProductId, $productId, $attribute, $_baseAttribute, $_PngDependencyRuleAttribute, $option, $base_attribute_code_id, $_PngDependencyRuleDefaultOption);
            } else {
                $_imgAddOnUrl = $this->getPngLayerImageUrl($superProductId, $productId, $attribute, $_baseAttribute, $option, $base_attribute_code_id, $area);
            }

            // imageUrl, imageCode, imageSku, productId, imagePosition
            $_imgArray[] = array (
                'image_url' => $_imgAddOnUrl,
                'image_code' => $attribute,
                'image_sku' => $_sku,
                'product_id' => $productId,
                'image_position' => $_position
            );
        }
        return $_imgArray;
    }

    /**
     * Retrieve Rule.
     *
     */
    public function getPngRule($superProductId, $sku, $attribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_pngRuleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->addFieldToFilter('attribute', $attribute);
        $collection->load();
        return $collection;
    }

    public function getSimpleProductByAttributes($_attribute_info, $_productSku)
    {
        //$_product = $this->productRepository->getById($_productId);
        $_product = $this->productRepository->get($_productSku);
        $_simple_product = $this->_modelConfigurableType->getProductByAttributes($_attribute_info, $_product);

        return $_simple_product;
    }

    public function getGalleryImagesRequestUpdate($superProductId, $_productId, $_baseOptionSelected, $_attributesInfo, $sku)
    {
        $_imgUrl = array();
        $_pngLayerImageUrl = null;
        $_superProductInfo = $this->getBaseProductById($superProductId);
        $_baseProductAttribute = $_superProductInfo->getBaseAttribute();

        $_productData = $this->productRepository->get($sku);
        $_productType = $_productData->getTypeId();
       // $_productSku = trim($_productData->getSku());
        //$_productSku = trim($_productData->getSku());
        $_productSku = $sku;


        if( empty($_attributesInfo) ) {
            $array_image = [];
            $_pngInfo = $this->getPngDefaultOptions($superProductId, $_productSku, '');
            foreach ($_pngInfo as $item) {
                $array_img = [];
                $_pngLayerImageUrl = $this->getPngLayerImageUrl($superProductId, $_productId, $_productSku, $_baseProductAttribute, $_productSku, $_baseOptionSelected, $item->getMultiLayer());
                $array_img['z-index'] = $item->getPosition();
                $array_img['url']  = $_pngLayerImageUrl;
                $array_image[] = $array_img;
            }

            if(count($array_image) > 0) {
                foreach ($array_image as $img) {
                    $_imgUrl[] = array(
                        'img_url' => $img['url'],
                        'attribute_code' => $_productSku,
                        'attribute_id' => $_productSku,
                        'position' => $img['z-index'],
                        'product_sku' => $_productSku,
                        'product_id' => $_productId
                    );
                }
            }
        } else {
            foreach ($_attributesInfo as $attributeCode => $value) {
                $array_image = [];
                $_pngInfo = $this->getPngDefaultOptions($superProductId, $_productSku, $attributeCode);
                $_pngRule = $this->_pngRuleRepository->getByAttributeDependency($superProductId, $_productSku, $attributeCode);
                if( is_object($_pngRule) && !empty($_pngRule->getData()) ) {
                    $_pngRuleAttribute = $_pngRule->getAttribute();
                    $_pngRuleAttributeId = $_attributesInfo[$_pngRuleAttribute];
                     foreach ($_pngInfo as $item) {
                         $array_img = [];
                         $_pngLayerImageUrl = $this->getPngLayerImageUrlWithRule($superProductId, $_productId, $attributeCode, $_baseProductAttribute, $_pngRuleAttribute, $value, $_baseOptionSelected, $_pngRuleAttributeId, $item->getMultiLayer());
                         $array_img['z-index'] = $item->getPosition();
                         $array_img['url']  = $_pngLayerImageUrl;
                         $array_image[] = $array_img;
                     }
                } else {
                    foreach ($_pngInfo as $item) {
                        $array_img = [];
                        $_pngLayerImageUrl = $this->getPngLayerImageUrl($superProductId, $_productId, $attributeCode, $_baseProductAttribute, $value, $_baseOptionSelected, $item->getMultiLayer());
                        $array_img['z-index'] = $item->getPosition();
                        $array_img['url']  = $_pngLayerImageUrl;
                        $array_image[] = $array_img;
                    }
                }
                if(count($array_image) > 0) {
                    foreach ($array_image as $img) {
                        $_imgUrl[] = array(
                            'img_url' => $img['url'],
                            'attribute_code' => $attributeCode,
                            'attribute_id' => $value,
                            'position' => $img['z-index'],
                            'product_sku' => $_productSku,
                            'product_id' => $_productId
                        );
                    }
                }

            }
        }

        return $_imgUrl;
    }

    public function getBaseOptionSelected($superProductId, $_super_attributes)
    {
        $_baseOptionSelected = null;
        $_superProductInfo = $this->getBaseProductById($superProductId);
        $_baseProductId = $_superProductInfo->getProductId();
        $_baseProductAttribute = $_superProductInfo->getBaseAttribute();

        foreach ($_super_attributes as $_productInfo) {
            if($_baseProductId == $_productInfo['product_id']) {
                $_baseOptionSelected = $_productInfo['option_selected'][$_baseProductAttribute];
            }
        }

        return $_baseOptionSelected;
    }

}
