<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Png _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Png getResource()
 */
class Png extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\PngInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_png';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_png';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'png';

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get PNG id
     *
     * @return array
     */
    public function getPngId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::PNG_ID);
    }

    /**
     * set PNG id
     *
     * @param int $pngId
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setPngId($pngId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::PNG_ID, $pngId);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::SKU);
    }

    /**
     * set Attribute
     *
     * @param mixed $attribute
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setAttribute($attribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::ATTRIBUTE, $attribute);
    }

    /**
     * get Attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::ATTRIBUTE);
    }

    /**
     * set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setBaseAttribute($baseAttribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::BASE_ATTRIBUTE, $baseAttribute);
    }

    /**
     * get Base Attribute
     *
     * @return string
     */
    public function getBaseAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::BASE_ATTRIBUTE);
    }

    /**
     * set Default Option
     *
     * @param mixed $defaultOption
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setDefaultOption($defaultOption)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::DEFAULT_OPTION, $defaultOption);
    }

    /**
     * get Default Option
     *
     * @return string
     */
    public function getDefaultOption()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::DEFAULT_OPTION);
    }

    /**
     * set Multi Layer
     *
     * @param mixed $multiLayer
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setMultiLayer($multiLayer)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::MULTI_LAYER, $multiLayer);
    }

    /**
     * get Multi Layer
     *
     * @return string
     */
    public function getMultiLayer()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::MULTI_LAYER);
    }

    /**
     * set Status
     *
     * @param mixed $status
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setStatus($status)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::STATUS, $status);
    }

    /**
     * get Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::STATUS);
    }

    /**
     * set Position
     *
     * @param mixed $position
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     */
    public function setPosition($position)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngInterface::POSITION, $position);
    }

    /**
     * get Position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngInterface::POSITION);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Png::class);
    }
}
