<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class UploaderPool
{
    /**
     * Available Uploaders
     * 
     * @var array
     */
    protected $_uploaders = [];

    /**
     * constructor
     * 
     * @param array $uploaders
     */
    public function __construct(
        array $uploaders = []
    ) {
        $this->_uploaders = $uploaders;
    }

    /**
     * @param string $type
     * @return \Brandsmith\SuperProduct\Model\Uploader
     * @throws \Exception
     */
    public function getUploader($type)
    {
        if (!isset($this->_uploaders[$type])) {
            throw new \Exception("Uploader not found for type: ".$type);
        }
        $uploader = $this->_uploaders[$type];
        if (!($uploader instanceof \Brandsmith\SuperProduct\Model\Uploader)) {
            throw new \Exception("Uploader for type {$type} not instance of ". \Brandsmith\SuperProduct\Model\Uploader::class);
        }
        return $uploader;
    }
}
