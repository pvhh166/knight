<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

use Magento\Framework\App\Filesystem\DirectoryList;

class PnglayerRepository implements \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $_instances = [];

    /**
     * PNG Layer resource model
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer
     */
    protected $_resource;

    /**
     * PNG Layer collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory
     */
    protected $_pnglayerCollectionFactory;

    /**
     * PNG Layer interface factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory
     */
    protected $_pnglayerInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\PnglayerSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * Uploader pool
     * 
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    protected $_filesystem;

    /**
     * constructor
     * 
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $pnglayerCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory $pnglayerInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $pnglayerCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\PnglayerInterfaceFactory $pnglayerInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\PnglayerSearchResultInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
    ) {
        $this->_resource                  = $resource;
        $this->_pnglayerCollectionFactory = $pnglayerCollectionFactory;
        $this->_pnglayerInterfaceFactory  = $pnglayerInterfaceFactory;
        $this->_dataObjectHelper          = $dataObjectHelper;
        $this->_searchResultsFactory      = $searchResultsFactory;
        $this->_filesystem = $filesystem;
        $this->_uploaderPool = $uploaderPool;
    }

    /**
     * Save PNG Layer.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterface|\Magento\Framework\Model\AbstractModel $pnglayer */
        try {
            if ($pnglayer->getImage())
            {
                $uploader = $this->_uploaderPool->getUploader('image');
                $mediapath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
                $path = $mediapath . $uploader->getBasePath() . $pnglayer->getImage();
                if(!file_exists($path)) {
                    $pnglayer->setImage('');
                    return $pnglayer;
                } 
            }
            $this->_resource->save($pnglayer);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the PNG&#x20;Layer: %1',
                $exception->getMessage()
            ));
        }
        return $pnglayer;
    }

    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($superProductId, $productId, $attribute, $baseAttribute, $ruleAttribute, $option, $baseOption, $ruleOption, $area = 0)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->addFieldToFilter('product_id',$productId);
        $collection->addFieldToFilter('attribute',$attribute);
        $collection->addFieldToFilter('base_attribute',$baseAttribute);
        if ($ruleAttribute) $collection->addFieldToFilter('rule_attribute', $ruleAttribute);
        $collection->addFieldToFilter('option',$option);
        $collection->addFieldToFilter('base_option',$baseOption);
        if ($ruleOption) $collection->addFieldToFilter('rule_option', $ruleOption);
        $collection->addFieldToFilter('area', $area);
        $png = $collection->getFirstItem();
        return $png;
    }

    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOnePngBySuperProductId($superProductId, $productId, $attribute, $baseAttribute, $option, $baseOption, $area = 0)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->addFieldToFilter('product_id',$productId);
        $collection->addFieldToFilter('attribute',$attribute);
        $collection->addFieldToFilter('base_attribute',$baseAttribute);
        $collection->addFieldToFilter('option',$option);
        $collection->addFieldToFilter('base_option',$baseOption);
        $collection->addFieldToFilter('area', $area);
        $png = $collection->getFirstItem();
        return $png;
    }


    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductIdNonOption($superProductId, $baseOption)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->addFieldToFilter('base_option',array('nin' => $baseOption));
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function countListBySuperProductId($superProductId, $productId, $attribute, $baseAttribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->addFieldToFilter('product_id',$productId);
        $collection->addFieldToFilter('attribute',$attribute);
        $collection->addFieldToFilter('base_attribute',$baseAttribute);
        // $collection->load();
        return $collection->getSize();
    }

    /**
     * Retrieve PNG Layers matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection */
        $collection = $this->_pnglayerCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'pnglayer_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterface[] $pnglayers */
        $pnglayers = [];
        /** @var \Brandsmith\SuperProduct\Model\Pnglayer $pnglayer */
        foreach ($collection as $pnglayer) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayerDataObject */
            $pnglayerDataObject = $this->_pnglayerInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $pnglayerDataObject,
                $pnglayer->getData(),
                \Brandsmith\SuperProduct\Api\Data\PnglayerInterface::class
            );
            $pnglayers[] = $pnglayerDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($pnglayers);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\Collection $collection
    )
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * Delete PNG Layer by ID.
     *
     * @param int $pnglayerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pnglayerId)
    {
        $pnglayer = $this->getById($pnglayerId);
        return $this->delete($pnglayer);
    }

    /**
     * Retrieve PNG Layer.
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pnglayerId)
    {
        if (!isset($this->_instances[$pnglayerId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterface|\Magento\Framework\Model\AbstractModel $pnglayer */
            $pnglayer = $this->_pnglayerInterfaceFactory->create();
            $this->_resource->load($pnglayer, $pnglayerId);
            if (!$pnglayer->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested PNG&#x20;Layer doesn\'t exist'));
            }
            $this->_instances[$pnglayerId] = $pnglayer;
        }
        return $this->_instances[$pnglayerId];
    }

    /**
     * Delete PNG Layer.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PnglayerInterface|\Magento\Framework\Model\AbstractModel $pnglayer */
        $id = $pnglayer->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($pnglayer);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove PNG&#x20;Layer %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }
}
