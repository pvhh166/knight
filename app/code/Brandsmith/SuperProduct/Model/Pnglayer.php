<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer getResource()
 */
class Pnglayer extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_pnglayer';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_pnglayer';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'pnglayer';

    /**
     * Uploader pool
     * 
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_uploaderPool = $uploaderPool;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get PNG Layer id
     *
     * @return array
     */
    public function getPnglayerId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::PNGLAYER_ID);
    }

    /**
     * set PNG Layer id
     *
     * @param int $pnglayerId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setPnglayerId($pnglayerId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::PNGLAYER_ID, $pnglayerId);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Product
     *
     * @param mixed $productId
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setProductId($productId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::PRODUCT_ID, $productId);
    }

    /**
     * get Product
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::PRODUCT_ID);
    }

    /**
     * set Attribute
     *
     * @param mixed $attribute
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setAttribute($attribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::ATTRIBUTE, $attribute);
    }

    /**
     * get Attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::ATTRIBUTE);
    }

    /**
     * set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setBaseAttribute($baseAttribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::BASE_ATTRIBUTE, $baseAttribute);
    }

    /**
     * get Base Attribute
     *
     * @return string
     */
    public function getBaseAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::BASE_ATTRIBUTE);
    }

    /**
     * set Rule Attribute
     *
     * @param mixed $ruleAttribute
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setRuleAttribute($ruleAttribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::RULE_ATTRIBUTE, $ruleAttribute);
    }

    /**
     * get Rule Attribute
     *
     * @return string
     */
    public function getRuleAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::RULE_ATTRIBUTE);
    }

    /**
     * set Option
     *
     * @param mixed $option
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setOption($option)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::OPTION, $option);
    }

    /**
     * get Option
     *
     * @return string
     */
    public function getOption()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::OPTION);
    }

    /**
     * set Base Option
     *
     * @param mixed $baseOption
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setBaseOption($baseOption)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::BASE_OPTION, $baseOption);
    }

    /**
     * get Base Option
     *
     * @return string
     */
    public function getBaseOption()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::BASE_OPTION);
    }

    /**
     * set Rule Option
     *
     * @param mixed $ruleOption
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setRuleOption($ruleOption)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::RULE_OPTION, $ruleOption);
    }

    /**
     * get Rule Option
     *
     * @return string
     */
    public function getRuleOption()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::RULE_OPTION);
    }

    /**
     * set Area
     *
     * @param mixed $area
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setArea($area)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::AREA, $area);
    }

    /**
     * get Rule Option
     *
     * @return string
     */
    public function getArea()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::AREA);
    }

    /**
     * set Image
     *
     * @param mixed $image
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setImage($image)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::IMAGE, $image);
    }

    /**
     * set Is Default
     *
     * @param mixed $isDefault
     * @return \Brandsmith\SuperProduct\Api\Data\PnglayerInterface
     */
    public function setIsDefault($isDefault)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::IS_DEFAULT, $isDefault);
    }

    /**
     * get Is Default
     *
     * @return string
     */
    public function getIsDefault()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::IS_DEFAULT);
    }

    /**
     * get Image
     *
     * @return string
     */
    public function getImageData()
    {
        $image = [];
        $image[0]['name'] = $this->getImage();
        $image[0]['url'] = $this->getImageUrl();
        return $image;
    }

    /**
     * get Image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface::IMAGE);
    }

    /**
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageUrl()
    {
        $url = false;
        $image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $uploader = $this->_uploaderPool->getUploader('image');
                $url = $uploader->getBaseUrl() . $uploader->getBasePath() . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the Image url.')
                );
            }
        }
        return $url;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer::class);
    }
}
