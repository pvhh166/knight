<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class AddonRepository implements \Brandsmith\SuperProduct\Api\AddonRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $_instances = [];

    /**
     * Addon resource model
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon
     */
    protected $_resource;

    /**
     * Addon collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory
     */
    protected $_addonCollectionFactory;

    /**
     * Addon interface factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory
     */
    protected $_addonInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Addon $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->_resource               = $resource;
        $this->_addonCollectionFactory = $addonCollectionFactory;
        $this->_addonInterfaceFactory  = $addonInterfaceFactory;
        $this->_dataObjectHelper       = $dataObjectHelper;
        $this->_searchResultsFactory   = $searchResultsFactory;
    }

    /**
     * Save Addon.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterface $addon
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\AddonInterface $addon)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\AddonInterface|\Magento\Framework\Model\AbstractModel $addon */
        try {
            $this->_resource->save($addon);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Addon: %1',
                $exception->getMessage()
            ));
        }
        return $addon;
    }

    /**
     * Retrieve Addon.
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($addonId)
    {
        if (!isset($this->_instances[$addonId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\AddonInterface|\Magento\Framework\Model\AbstractModel $addon */
            $addon = $this->_addonInterfaceFactory->create();
            $this->_resource->load($addon, $addonId);
            if (!$addon->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Addon doesn\'t exist'));
            }
            $this->_instances[$addonId] = $addon;
        }
        return $this->_instances[$addonId];
    }

    /**
     * Retrieve Addon.
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($productId,$superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\Collection $collection */
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('product_id',$productId);
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $addon = $collection->getFirstItem(); 
        return $addon;
    }

    /**
     * Retrieve Addon.
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\Collection $collection */
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->load(); 
        return $collection;
    }

    /**
     * Retrieve Addons matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\AddonSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\Collection $collection */
        $collection = $this->_addonCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'addon_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\AddonInterface[] $addons */
        $addons = [];
        /** @var \Brandsmith\SuperProduct\Model\Addon $addon */
        foreach ($collection as $addon) {
            /** @var \Brandsmith\SuperProduct\Api\Data\AddonInterface $addonDataObject */
            $addonDataObject = $this->_addonInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $addonDataObject,
                $addon->getData(),
                \Brandsmith\SuperProduct\Api\Data\AddonInterface::class
            );
            $addons[] = $addonDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($addons);
    }

    /**
     * Delete Addon.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterface $addon
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\AddonInterface $addon)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\AddonInterface|\Magento\Framework\Model\AbstractModel $addon */
        $id = $addon->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($addon);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Addon %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Delete Addon by ID.
     *
     * @param int $addonId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($addonId)
    {
        $addon = $this->getById($addonId);
        return $this->delete($addon);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Addon\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
