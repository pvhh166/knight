<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Productrule _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Productrule getResource()
 */
class Productrule extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_productrule';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_productrule';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'productrule';

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Product Rule id
     *
     * @return array
     */
    public function getProductruleId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::PRODUCTRULE_ID);
    }

    /**
     * set Product Rule id
     *
     * @param int $productruleId
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setProductruleId($productruleId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::PRODUCTRULE_ID, $productruleId);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SKU, $sku);
    }

    /**
     * set Condition
     *
     * @param mixed $condition
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setCondition($condition)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::CONDITION, $condition);
    }

    /**
     * get Condition
     *
     * @return string
     */
    public function getCondition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::CONDITION);
    }

    /**
     * set Dependent Sku
     *
     * @param mixed $skuDep
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setSkuDep($skuDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SKU_DEP, $skuDep);
    }

    /**
     * set Position
     *
     * @param mixed $position
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     */
    public function setPosition($position)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::POSITION, $position);
    }

    /**
     * get Position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::POSITION);
    }

    /**
     * get Duplicate rule
     *
     * @return string
     */
    public function getDuplicateRule()
    {
        $collection = $this->getCollection();
        $collection->addFieldToFilter('super_product_id', $this->getSuperProductId());
        $collection->addFieldToFilter('sku', $this->getSkuDep());
        $collection->addFieldToFilter('sku_dep', $this->getSku());
        $collection->load();
        return count($collection) > 0 ? true : false;
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SUPER_PRODUCT_ID);
    }

    /**
     * get Dependent Sku
     *
     * @return string
     */
    public function getSkuDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SKU_DEP);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface::SKU);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Productrule::class);
    }
}
