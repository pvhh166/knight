<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Rule _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Rule getResource()
 */
class Rule extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\RuleInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_rule';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_rule';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'rule';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Rule::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Dependency Rule id
     *
     * @return array
     */
    public function getRuleId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::RULE_ID);
    }

    /**
     * set Dependency Rule id
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setRuleId($ruleId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::RULE_ID, $ruleId);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SKU);
    }

    /**
     * set Attribute
     *
     * @param mixed $attribute
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setAttribute($attribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::ATTRIBUTE, $attribute);
    }

    /**
     * get Attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::ATTRIBUTE);
    }

    /**
     * set Option
     *
     * @param mixed $option
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setOption($option)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::OPTION, $option);
    }

    /**
     * get Option
     *
     * @return string
     */
    public function getOption()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::OPTION);
    }

    /**
     * set Condition
     *
     * @param mixed $condition
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setCondition($condition)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::CONDITION, $condition);
    }

    /**
     * get Condition
     *
     * @return string
     */
    public function getCondition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::CONDITION);
    }

    /**
     * set Dependent Sku
     *
     * @param mixed $skuDep
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setSkuDep($skuDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SKU_DEP, $skuDep);
    }

    /**
     * get Dependent Sku
     *
     * @return string
     */
    public function getSkuDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::SKU_DEP);
    }

    /**
     * set Dependent Attribute
     *
     * @param mixed $attributeDep
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setAttributeDep($attributeDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::ATTRIBUTE_DEP, $attributeDep);
    }

    /**
     * get Dependent Attribute
     *
     * @return string
     */
    public function getAttributeDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::ATTRIBUTE_DEP);
    }

    /**
     * set Dependent Option
     *
     * @param mixed $optionDep
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setOptionDep($optionDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::OPTION_DEP, $optionDep);
    }

    /**
     * get Dependent Option
     *
     * @return string
     */
    public function getOptionDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::OPTION_DEP);
    }

    /**
     * set Dependent Condition
     *
     * @param mixed $conditionDep
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setConditionDep($conditionDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::CONDITION_DEP, $conditionDep);
    }

    /**
     * get Dependent Condition
     *
     * @return string
     */
    public function getConditionDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::CONDITION_DEP);
    }

    /**
     * set Position
     *
     * @param mixed $position
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     */
    public function setPosition($position)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::POSITION, $position);
    }

    /**
     * get Position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\RuleInterface::POSITION);
    }
}
