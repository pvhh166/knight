<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model\Rule\Source;

class Condition implements \Magento\Framework\Option\ArrayInterface
{
    const ISIS = 1;
    const ISNOT = 2;

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::ISIS,
                'label' => __('is/is')
            ],
            [
                'value' => self::ISNOT,
                'label' => __('is/not')
            ],
        ];
        return $options;

    }
}
