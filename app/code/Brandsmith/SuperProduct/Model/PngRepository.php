<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class PngRepository implements \Brandsmith\SuperProduct\Api\PngRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $_instances = [];

    /**
     * PNG resource model
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Png
     */
    protected $_resource;

    /**
     * PNG collection factory
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory
     */
    protected $_pngCollectionFactory;

    /**
     * PNG interface factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory
     */
    protected $_pngInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     *
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Png $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory $pngCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory $pngInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Png $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Png\CollectionFactory $pngCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory $pngInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterfaceFactory $searchResultsFactory
    )
    {
        $this->_resource = $resource;
        $this->_pngCollectionFactory = $pngCollectionFactory;
        $this->_pngInterfaceFactory = $pngInterfaceFactory;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save PNG.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterface $png
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\PngInterface $png)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngInterface|\Magento\Framework\Model\AbstractModel $png */
        try {
            $this->_resource->save($png);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the PNG: %1',
                $exception->getMessage()
            ));
        }
        return $png;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProduct($superProductId, $sku, $attribute, $baseAttribute, $multi_layer = 0)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->addFieldToFilter('attribute', $attribute);
        $collection->addFieldToFilter('base_attribute', $baseAttribute);
        $collection->addFieldToFilter('multi_layer', $multi_layer);
        $rule = $collection->getFirstItem();
        return $rule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('png_id', $ruleId);
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $rule = $collection->getFirstItem();
        return $rule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductIdView($superProductId, $sku, $attribute, $baseAttribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->addFieldToFilter('attribute', $attribute);
        $collection->addFieldToFilter('base_attribute', $baseAttribute);
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve PNG matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Png\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'png_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\PngInterface[] $pngs */
        $pngs = [];
        /** @var \Brandsmith\SuperProduct\Model\Png $png */
        foreach ($collection as $png) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PngInterface $pngDataObject */
            $pngDataObject = $this->_pngInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $pngDataObject,
                $png->getData(),
                \Brandsmith\SuperProduct\Api\Data\PngInterface::class
            );
            $pngs[] = $pngDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($pngs);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Png\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Png\Collection $collection
    )
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * Delete PNG by ID.
     *
     * @param int $pngId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pngId)
    {
        $png = $this->getById($pngId);
        return $this->delete($png);
    }

    /**
     * Retrieve PNG.
     *
     * @param int $pngId
     * @return \Brandsmith\SuperProduct\Api\Data\PngInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pngId)
    {
        if (!isset($this->_instances[$pngId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PngInterface|\Magento\Framework\Model\AbstractModel $png */
            $png = $this->_pngInterfaceFactory->create();
            $this->_resource->load($png, $pngId);
            if (!$png->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested PNG doesn\'t exist'));
            }
            $this->_instances[$pngId] = $png;
        }
        return $this->_instances[$pngId];
    }

    /**
     * Delete PNG.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngInterface $png
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\PngInterface $png)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngInterface|\Magento\Framework\Model\AbstractModel $png */
        $id = $png->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($png);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove PNG %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDefaultOption($superProductId, $sku, $attribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        if(!empty($attribute)){$collection->addFieldToFilter('attribute', $attribute);}
        //if(!empty($attribute)){$collection = $collection->getFirstItem();}
        return $collection;
    }

    public function getDefaultOptionFirstItem($superProductId, $sku, $attribute)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_pngCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        if(!empty($attribute)){$collection->addFieldToFilter('attribute', $attribute);}
        if(count($collection) > 0){$collection = $collection->getFirstItem();}
        return $collection;
    }
}
