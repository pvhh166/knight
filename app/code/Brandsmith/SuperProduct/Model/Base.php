<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Base _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Base getResource()
 */
class Base extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\BaseInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_base';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_base';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'base';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Base::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Base id
     *
     * @return array
     */
    public function getBaseId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::BASE_ID);
    }

    /**
     * set Base id
     *
     * @param int $baseId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setBaseId($baseId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::BASE_ID, $baseId);
    }

    /**
     * set Name
     *
     * @param mixed $name
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setName($name)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::NAME, $name);
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::NAME);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Base Attribute
     *
     * @param mixed $baseAttribute
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setBaseAttribute($baseAttribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::BASE_ATTRIBUTE, $baseAttribute);
    }

    /**
     * get Base Attribute
     *
     * @return string
     */
    public function getBaseAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::BASE_ATTRIBUTE);
    }

    /**
     * set Product
     *
     * @param mixed $productId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setProductId($productId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::PRODUCT_ID, $productId);
    }

    /**
     * get Product
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::PRODUCT_ID);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::SKU);
    }

    /**
     * set Type
     *
     * @param mixed $type
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setType($type)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::TYPE, $type);
    }

    /**
     * get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::TYPE);
    }

    /**
     * set Required
     *
     * @param mixed $required
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setRequired($required)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::REQUIRED, $required);
    }

    /**
     * get Required
     *
     * @return string
     */
    public function getRequired()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::REQUIRED);
    }

    /**
     * set Qty
     *
     * @param mixed $qty
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setQty($qty)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::QTY, $qty);
    }

    /**
     * get Qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::QTY);
    }

    /**
     * set User Defined
     *
     * @param mixed $userDefined
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     */
    public function setUserDefined($userDefined)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::USER_DEFINED, $userDefined);
    }

    /**
     * get User Defined
     *
     * @return string
     */
    public function getUserDefined()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\BaseInterface::USER_DEFINED);
    }

    /**
     * set SIS
     *
     * @param mixed $sis
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setSis($sis)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SIS, $sis);
    }

    /**
     * get SIS
     *
     * @return string
     */
    public function getSis()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SIS);
    }
}
