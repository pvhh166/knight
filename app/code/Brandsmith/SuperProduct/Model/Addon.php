<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Addon _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Addon getResource()
 */
class Addon extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\AddonInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_addon';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_addon';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'addon';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Addon::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Addon id
     *
     * @return array
     */
    public function getAddonId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::ADDON_ID);
    }

    /**
     * set Addon id
     *
     * @param int $addonId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setAddonId($addonId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::ADDON_ID, $addonId);
    }

    /**
     * set Name
     *
     * @param mixed $name
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setName($name)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::NAME, $name);
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::NAME);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Product
     *
     * @param mixed $productId
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setProductId($productId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::PRODUCT_ID, $productId);
    }

    /**
     * get Product
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::PRODUCT_ID);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SKU);
    }

    /**
     * set Type
     *
     * @param mixed $type
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setType($type)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::TYPE, $type);
    }

    /**
     * get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::TYPE);
    }

    /**
     * set Required
     *
     * @param mixed $required
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setRequired($required)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::REQUIRED, $required);
    }

    /**
     * get Required
     *
     * @return string
     */
    public function getRequired()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::REQUIRED);
    }

    /**
     * set Qty
     *
     * @param mixed $qty
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setQty($qty)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::QTY, $qty);
    }

    /**
     * get Qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::QTY);
    }

    /**
     * set User Defined
     *
     * @param mixed $userDefined
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setUserDefined($userDefined)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::USER_DEFINED, $userDefined);
    }

    /**
     * get User Defined
     *
     * @return string
     */
    public function getUserDefined()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::USER_DEFINED);
    }

    /**
     * set Position
     *
     * @param mixed $position
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setPosition($position)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::POSITION, $position);
    }

    /**
     * get Position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::POSITION);
    }

    /**
     * set SIS
     *
     * @param mixed $sis
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     */
    public function setSis($sis)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SIS, $sis);
    }

    /**
     * get SIS
     *
     * @return string
     */
    public function getSis()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\AddonInterface::SIS);
    }
}
