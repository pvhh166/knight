<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class PngruleRepository implements \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $_instances = [];

    /**
     * PNG Rule resource model
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule
     */
    protected $_resource;

    /**
     * PNG Rule collection factory
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory
     */
    protected $_pngruleCollectionFactory;

    /**
     * PNG Rule interface factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory
     */
    protected $_pngruleInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     *
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $pngruleCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngruleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $pngruleCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngruleInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterfaceFactory $searchResultsFactory
    )
    {
        $this->_resource = $resource;
        $this->_pngruleCollectionFactory = $pngruleCollectionFactory;
        $this->_pngruleInterfaceFactory = $pngruleInterfaceFactory;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save PNG Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface|\Magento\Framework\Model\AbstractModel $pngrule */
        try {
            $this->_resource->save($pngrule);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the PNG&#x20;Rule: %1',
                $exception->getMessage()
            ));
        }
        return $pngrule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_pngruleCollectionFactory->create();
        $collection->addFieldToFilter('pngrule_id', $ruleId);
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $rule = $collection->getFirstItem();
        return $rule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_pngruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve PNG Rules matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngruleSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\Collection $collection */
        $collection = $this->_pngruleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'pngrule_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface[] $pngrules */
        $pngrules = [];
        /** @var \Brandsmith\SuperProduct\Model\Pngrule $pngrule */
        foreach ($collection as $pngrule) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngruleDataObject */
            $pngruleDataObject = $this->_pngruleInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $pngruleDataObject,
                $pngrule->getData(),
                \Brandsmith\SuperProduct\Api\Data\PngruleInterface::class
            );
            $pngrules[] = $pngruleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($pngrules);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\Collection $collection
    )
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * Delete PNG Rule by ID.
     *
     * @param int $pngruleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pngruleId)
    {
        $pngrule = $this->getById($pngruleId);
        return $this->delete($pngrule);
    }

    /**
     * Retrieve PNG Rule.
     *
     * @param int $pngruleId
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pngruleId)
    {
        if (!isset($this->_instances[$pngruleId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface|\Magento\Framework\Model\AbstractModel $pngrule */
            $pngrule = $this->_pngruleInterfaceFactory->create();
            $this->_resource->load($pngrule, $pngruleId);
            if (!$pngrule->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested PNG&#x20;Rule doesn\'t exist'));
            }
            $this->_instances[$pngruleId] = $pngrule;
        }
        return $this->_instances[$pngruleId];
    }

    /**
     * Delete PNG Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface|\Magento\Framework\Model\AbstractModel $pngrule */
        $id = $pngrule->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($pngrule);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove PNG&#x20;Rule %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByAttributeDependency($superProductId, $sku, $attributeDep)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_pngruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $collection->addFieldToFilter('attribute_dep', $attributeDep);
        $rule = $collection->getFirstItem();
        return $rule;
    }

}
