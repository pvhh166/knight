<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class ProductruleRepository implements \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $_instances = [];

    /**
     * Product Rule resource model
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule
     */
    protected $_resource;

    /**
     * Product Rule collection factory
     *
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;

    /**
     * Product Rule interface factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory
     */
    protected $_productruleInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     *
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Productrule $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productruleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productruleInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterfaceFactory $searchResultsFactory
    )
    {
        $this->_resource = $resource;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
        $this->_productruleInterfaceFactory = $productruleInterfaceFactory;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save Product Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface|\Magento\Framework\Model\AbstractModel $productrule */
        try {
            $this->_resource->save($productrule);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Product&#x20;Rule: %1',
                $exception->getMessage()
            ));
        }
        return $productrule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId, $superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('productrule_id', $ruleId);
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $rule = $collection->getFirstItem();
        return $rule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->load();
        return $collection;
    }

    /**
     * Retrieve Product Rules matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_productruleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'productrule_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface[] $productrules */
        $productrules = [];
        /** @var \Brandsmith\SuperProduct\Model\Productrule $productrule */
        foreach ($collection as $productrule) {
            /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productruleDataObject */
            $productruleDataObject = $this->_productruleInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $productruleDataObject,
                $productrule->getData(),
                \Brandsmith\SuperProduct\Api\Data\ProductruleInterface::class
            );
            $productrules[] = $productruleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($productrules);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection
    )
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * Delete Product Rule by ID.
     *
     * @param int $productruleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productruleId)
    {
        $productrule = $this->getById($productruleId);
        return $this->delete($productrule);
    }

    /**
     * Retrieve Product Rule.
     *
     * @param int $productruleId
     * @return \Brandsmith\SuperProduct\Api\Data\ProductruleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($productruleId)
    {
        if (!isset($this->_instances[$productruleId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface|\Magento\Framework\Model\AbstractModel $productrule */
            $productrule = $this->_productruleInterfaceFactory->create();
            $this->_resource->load($productrule, $productruleId);
            if (!$productrule->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Product&#x20;Rule doesn\'t exist'));
            }
            $this->_instances[$productruleId] = $productrule;
        }
        return $this->_instances[$productruleId];
    }

    /**
     * Delete Product Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface|\Magento\Framework\Model\AbstractModel $productrule */
        $id = $productrule->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($productrule);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Product&#x20;Rule %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }
}
