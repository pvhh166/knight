<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class BaseRepository implements \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $_instances = [];

    /**
     * Base resource model
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Base
     */
    protected $_resource;

    /**
     * Base collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory
     */
    protected $_baseCollectionFactory;

    /**
     * Base interface factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory
     */
    protected $_baseInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Base $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory $baseCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Base $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory $baseCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->_resource              = $resource;
        $this->_baseCollectionFactory = $baseCollectionFactory;
        $this->_baseInterfaceFactory  = $baseInterfaceFactory;
        $this->_dataObjectHelper      = $dataObjectHelper;
        $this->_searchResultsFactory  = $searchResultsFactory;
    }

    /**
     * Save Base.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterface $base
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\BaseInterface $base)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface|\Magento\Framework\Model\AbstractModel $base */
        try {
            $this->_resource->save($base);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Base: %1',
                $exception->getMessage()
            ));
        }
        return $base;
    }

    /**
     * Retrieve Base.
     *
     * @param int $baseId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($baseId)
    {
        if (!isset($this->_instances[$baseId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface|\Magento\Framework\Model\AbstractModel $base */
            $base = $this->_baseInterfaceFactory->create();
            $this->_resource->load($base, $baseId);
            if (!$base->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Base doesn\'t exist'));
            }
            $this->_instances[$baseId] = $base;
        }
        return $this->_instances[$baseId];
    }

    /**
     * Retrieve productId.
     *
     * @param int $productId
     * @return \Brandsmith\SuperProduct\Api\Data\BaseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($productId)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface|\Magento\Framework\Model\AbstractModel $base */
        $base = $this->_baseInterfaceFactory->create();
        $this->_resource->load($base, $productId, 'super_product_id');
        return $base;
    }

    /**
     * Retrieve Bases matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\BaseSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Base\Collection $collection */
        $collection = $this->_baseCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'base_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface[] $bases */
        $bases = [];
        /** @var \Brandsmith\SuperProduct\Model\Base $base */
        foreach ($collection as $base) {
            /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface $baseDataObject */
            $baseDataObject = $this->_baseInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $baseDataObject,
                $base->getData(),
                \Brandsmith\SuperProduct\Api\Data\BaseInterface::class
            );
            $bases[] = $baseDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($bases);
    }

    /**
     * Delete Base.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterface $base
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\BaseInterface $base)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\BaseInterface|\Magento\Framework\Model\AbstractModel $base */
        $id = $base->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($base);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Base %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Delete Base by ID.
     *
     * @param int $baseId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($baseId)
    {
        $base = $this->getById($baseId);
        return $this->delete($base);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Base\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Base\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
