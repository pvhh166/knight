<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model\Pngrule;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $_loadedData;

    /**
     * Data persistor
     *
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->_dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Brandsmith\SuperProduct\Model\Pngrule $pngrule */
        foreach ($items as $pngrule) {
            $this->_loadedData[$pngrule->getId()] = $pngrule->getData();

        }
        $data = $this->_dataPersistor->get('brandsmith_superproduct_pngrule');
        if (!empty($data)) {
            $pngrule = $this->collection->getNewEmptyItem();
            $pngrule->setData($data);
            $this->_loadedData[$pngrule->getId()] = $pngrule->getData();
            $this->_dataPersistor->clear('brandsmith_superproduct_pngrule');
        }
        return $this->_loadedData;
    }
}
