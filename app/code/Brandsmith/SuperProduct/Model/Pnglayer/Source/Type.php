<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model\Pnglayer\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    const BASE_ATTRIBUTE = 1;
    const BASE_DEPENDENCY = 2;

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::BASE_ATTRIBUTE,
                'label' => __('Base Attribute')
            ],
            [
                'value' => self::BASE_DEPENDENCY,
                'label' => __('Base Dependency')
            ],
        ];
        return $options;

    }
}
