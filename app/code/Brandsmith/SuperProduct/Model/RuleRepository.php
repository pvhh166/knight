<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

class RuleRepository implements \Brandsmith\SuperProduct\Api\RuleRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $_instances = [];

    /**
     * Dependency Rule resource model
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule
     */
    protected $_resource;

    /**
     * Dependency Rule collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * Dependency Rule interface factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory
     */
    protected $_ruleInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\RuleSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule $resource
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Api\Data\RuleSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule $resource,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Api\Data\RuleSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->_resource              = $resource;
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->_ruleInterfaceFactory  = $ruleInterfaceFactory;
        $this->_dataObjectHelper      = $dataObjectHelper;
        $this->_searchResultsFactory  = $searchResultsFactory;
    }

    /**
     * Save Dependency Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterface $rule
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\SuperProduct\Api\Data\RuleInterface $rule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
        try {
            $this->_resource->save($rule);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Dependency&#x20;Rule: %1',
                $exception->getMessage()
            ));
        }
        return $rule;
    }

    /**
     * Retrieve Dependency Rule.
     *
     * @param int $ruleId
     * @return \Brandsmith\SuperProduct\Api\Data\RuleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($ruleId)
    {
        if (!isset($this->_instances[$ruleId])) {
            /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
            $rule = $this->_ruleInterfaceFactory->create();
            $this->_resource->load($rule, $ruleId);
            if (!$rule->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Dependency&#x20;Rule doesn\'t exist'));
            }
            $this->_instances[$ruleId] = $rule;
        }
        return $this->_instances[$ruleId];
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySuperProductId($ruleId,$superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('rule_id',$ruleId);
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $rule = $collection->getFirstItem(); 
        return $rule;
    }

    /**
     * Retrieve Rule.
     *
     * @param int $rule
     * @return \Brandsmith\SuperProduct\Api\Data\AddonInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListBySuperProductId($superProductId)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id',$superProductId);
        $collection->load(); 
        return $collection;
    }

    /**
     * Retrieve Dependency Rules matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\SuperProduct\Api\Data\RuleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\RuleSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_ruleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'rule_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface[] $rules */
        $rules = [];
        /** @var \Brandsmith\SuperProduct\Model\Rule $rule */
        foreach ($collection as $rule) {
            /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface $ruleDataObject */
            $ruleDataObject = $this->_ruleInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $ruleDataObject,
                $rule->getData(),
                \Brandsmith\SuperProduct\Api\Data\RuleInterface::class
            );
            $rules[] = $ruleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($rules);
    }

    /**
     * Delete Dependency Rule.
     *
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterface $rule
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\SuperProduct\Api\Data\RuleInterface $rule)
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
        $id = $rule->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($rule);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Dependency&#x20;Rule %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Delete Dependency Rule by ID.
     *
     * @param int $ruleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($ruleId)
    {
        $rule = $this->getById($ruleId);
        return $this->delete($rule);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
