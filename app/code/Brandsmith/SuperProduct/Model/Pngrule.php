<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Model;

/**
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule _getResource()
 * @method \Brandsmith\SuperProduct\Model\ResourceModel\Pngrule getResource()
 */
class Pngrule extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\SuperProduct\Api\Data\PngruleInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'brandsmith_superproduct_pngrule';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_superproduct_pngrule';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'pngrule';

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get PNG Rule id
     *
     * @return array
     */
    public function getPngruleId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::PNGRULE_ID);
    }

    /**
     * set PNG Rule id
     *
     * @param int $pngruleId
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setPngruleId($pngruleId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::PNGRULE_ID, $pngruleId);
    }

    /**
     * set Super Product
     *
     * @param mixed $superProductId
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setSuperProductId($superProductId)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::SUPER_PRODUCT_ID, $superProductId);
    }

    /**
     * get Super Product
     *
     * @return string
     */
    public function getSuperProductId()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::SUPER_PRODUCT_ID);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setSku($sku)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::SKU);
    }

    /**
     * set Attribute
     *
     * @param mixed $attribute
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setAttribute($attribute)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::ATTRIBUTE, $attribute);
    }

    /**
     * get Attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::ATTRIBUTE);
    }

    /**
     * set Dependent Attribute
     *
     * @param mixed $attributeDep
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setAttributeDep($attributeDep)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::ATTRIBUTE_DEP, $attributeDep);
    }

    /**
     * get Dependent Attribute
     *
     * @return string
     */
    public function getAttributeDep()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::ATTRIBUTE_DEP);
    }

    /**
     * set Position
     *
     * @param mixed $position
     * @return \Brandsmith\SuperProduct\Api\Data\PngruleInterface
     */
    public function setPosition($position)
    {
        return $this->setData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::POSITION, $position);
    }

    /**
     * get Position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->getData(\Brandsmith\SuperProduct\Api\Data\PngruleInterface::POSITION);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\SuperProduct\Model\ResourceModel\Pngrule::class);
    }
}
