<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Setup;

class Uninstall implements \Magento\Framework\Setup\UninstallInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.Generic.CodeAnalysis.UnusedFunctionParameter)
     */
    public function uninstall(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        if ($setup->tableExists('brandsmith_superproduct_base')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_base');
        }
        if ($setup->tableExists('brandsmith_superproduct_addon')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_addon');
        }
        if ($setup->tableExists('brandsmith_superproduct_rule')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_rule');
        }
        if ($setup->tableExists('brandsmith_superproduct_pnglayer')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_pnglayer');
        }
        if ($setup->tableExists('brandsmith_superproduct_productrule')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_productrule');
        }
        if ($setup->tableExists('brandsmith_superproduct_pngrule')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_pngrule');
        }
        if ($setup->tableExists('brandsmith_superproduct_png')) {
            $setup->getConnection()->dropTable('brandsmith_superproduct_png');
        }
    }
}
