<?php

namespace Brandsmith\SuperProduct\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{

    public function upgrade ( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->upgrade101($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $setup->getConnection()->addColumn($setup->getTable('brandsmith_superproduct_addon'), 'sis',
                [
                    'type' => 'integer',
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'SIS',
                ]
            );
            $setup->getConnection()->addColumn($setup->getTable('brandsmith_superproduct_base'), 'sis',
            [
                'type' => 'integer',
                'nullable' => false,
                'default' => 0,
                'comment' => 'SIS',
            ]
            );
        }

        $setup->endSetup();
    }

    /**
     * @param $setup
     */
    public function upgrade101($setup)
    {
        $setup->getConnection()->addColumn ( $setup->getTable('quote'), 'use_remove_item',
            [
                'type' => 'integer',
                'nullable' => false,
                'default' => 0,
                'comment' => 'Use Remove Item',
            ]
        );

    }

}
