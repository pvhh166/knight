<?php
namespace Brandsmith\SuperProduct\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;


class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory
     */
    protected $_addonCollectionFactory;

    /**
     * @var \Brandsmith\SuperProduct\Model\BaseRepository
     */
    protected $_baseRepository;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    protected $_entityAttribute;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $addonCollectionFactory,
        \Brandsmith\SuperProduct\Model\BaseRepository $baseRepository,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_addonCollectionFactory = $addonCollectionFactory;
        $this->_baseRepository = $baseRepository;
        $this->_entityAttribute = $entityAttribute;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
    }

    public function getAddOnById($_super_product_id, $_add_on_id)
    {
        $collection = $this->_addonCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_super_product_id);
        $collection->addFieldToFilter('product_id', $_add_on_id);
        $collection->load();
        $addon = $collection->getFirstItem();
        return $addon;
    }

    public function getBaseProductById($_super_product_id)
    {
        try {
            $_baseProductId = $this->_baseRepository->getBySuperProductId($_super_product_id);
        } catch (Exception $e) {
            $_baseProductId = false;
        }

        return $_baseProductId;
    }


    public function getAttributeInfo($entityType, $attributeCode)
    {
        try {
            $_attributeInfo = $this->_entityAttribute->loadByCode($entityType, $attributeCode);
        } catch (Exception $e) {
            $_attributeInfo = false;
        }

        return $_attributeInfo;
    }

    public function getAttributesInfo($attributes)
    {
        $entityType = 'catalog_product';
        foreach ($attributes as $_key => $_value) {
            $_attribute = $this->getAttributeInfo($entityType, $_key);
            $_attribute_data = $_attribute->getData();
            $_attribute_id = $_attribute_data['attribute_id'];
            $attributes[$_attribute_id] = $attributes[$_key];
            unset($attributes[$_key]);
        }

        return $attributes;
    }

    public function getConvertAttributesInfo($attributes)
    {
        $entityType = 'catalog_product';
        foreach ($attributes as $_key => $_value) {
            // Convert Option Label
            $_option_label = $this->getConvertOptionInfo($_value);
            $attributes[$_key] = $_option_label;

            // Covert Attribute Label
            $_attribute = $this->getAttributeInfo($entityType, $_key);
            $_attribute_data = $_attribute->getData();
            $_attribute_id = $_attribute_data['frontend_label'];
            $attributes[$_attribute_id] = $attributes[$_key];
            unset($attributes[$_key]);
        }

        return $attributes;
    }

    public function getConvertOptionInfo($option_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eav_attribute_option_value');

        $sql = "select * FROM " . $tableName . " where option_id=".$option_id;
        $result = $connection->fetchAll($sql);

        $option_label = $result[0]['value'];

        return $option_label;
    }


    /**
     * Retrieve Rule.
     *
     */
    public function getBySuperProductId($superProductId, $sku)
    {
        /** @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\Collection $collection */
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $superProductId);
        $collection->addFieldToFilter('sku', $sku);
        $rule = $collection->getFirstItem();
        return $rule;
    }
}