<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\ObjectManagerInterface;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Brandsmith\SuperProduct\Model\Product\Type;
use Magento\Bundle\Api\ProductOptionRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class Bundle customizes Bundle product creation flow
 */
class Composite extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var array
     */
    protected $modifiers = [];

    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ProductOptionRepositoryInterface
     */
    protected $optionsRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Base repository
     * 
     * @var \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
     */
    protected $baseRepository;

    /**
     * Base factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory
     */
    protected $baseFactory;

    /**
     * Addon repository
     * 
     * @var \Brandsmith\SuperProduct\Api\AddonRepositoryInterface
     */
    protected $addonRepository;

    /**
     * Addon factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory
     */
    protected $addonFactory;

    /**
     * Dependency Rule repository
     * 
     * @var \Brandsmith\SuperProduct\Api\RuleRepositoryInterface
     */
    protected $ruleRepository;

    /**
     * Dependency Rule factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory
     */
    protected $ruleFactory;

    /**
     * Dependency Rule repository
     *
     * @var \Brandsmith\SuperProduct\Api\ProductRuleRepositoryInterface
     */
    protected $productRuleRepository;

    /**
     * Dependency Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\ProductRuleInterfaceFactory
     */
    protected $productRuleFactory;

    /**
     * Dependency Rule repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface
     */
    protected $pngRuleRepository;

    /**
     * Dependency Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory
     */
    protected $pngRuleFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @param LocatorInterface $locator
     * @param ObjectManagerInterface $objectManager
     * @param ProductOptionRepositoryInterface $optionsRepository
     * @param ProductRepositoryInterface $productRepository
     * @param \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseFactory
     * @param \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonFactory
     * @param \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productRuleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productRuleFactory
     * @param \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory
     * @param \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngRuleRepository
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngRuleFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param array $modifiers
     */
    public function __construct(
        LocatorInterface $locator,
        ObjectManagerInterface $objectManager,
        ProductOptionRepositoryInterface $optionsRepository,
        ProductRepositoryInterface $productRepository,
        \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository,
        \Brandsmith\SuperProduct\Api\Data\BaseInterfaceFactory $baseFactory,
        \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository,
        \Brandsmith\SuperProduct\Api\Data\AddonInterfaceFactory $addonFactory,
        \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productRuleRepository,
        \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productRuleFactory,
        \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository,
        \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory,
        \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngRuleRepository,
        \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngRuleFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        array $modifiers = []
    ) {
        $this->locator = $locator;
        $this->objectManager = $objectManager;
        $this->optionsRepository = $optionsRepository;
        $this->productRepository = $productRepository;
        $this->baseRepository    = $baseRepository;
        $this->baseFactory         = $baseFactory;
        $this->addonRepository   = $addonRepository;
        $this->addonFactory        = $addonFactory;
        $this->productRuleRepository = $productRuleRepository;
        $this->productRuleFactory = $productRuleFactory;
        $this->ruleRepository    = $ruleRepository;
        $this->ruleFactory         = $ruleFactory;
        $this->pngRuleRepository = $pngRuleRepository;
        $this->pngRuleFactory = $pngRuleFactory;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->modifiers = $modifiers;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        if ($this->locator->getProduct()->getTypeId() === Type::TYPE_CODE) {
            foreach ($this->modifiers as $superClass) {
                /** @var ModifierInterface $superModifier */
                $superModifier = $this->objectManager->get($superClass);
                if (!$superModifier instanceof ModifierInterface) {
                    throw new \InvalidArgumentException(
                        'Type "' . $superClass . '" is not an instance of ' . ModifierInterface::class
                    );
                }
                $meta = $superModifier->modifyMeta($meta);
            }
        }
        return $meta;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function modifyData(array $data)
    {
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $product = $this->locator->getProduct();
        $modelId = $product->getId();
        $isSuperProduct = $product->getTypeId() === Type::TYPE_CODE;
        if ($isSuperProduct && $modelId) {
            $base = $this->baseRepository->getBySuperProductId((int)$product->getId());
            if ($base->getId())
                $data[$modelId]['baseproduct']['super_selections'][] = [
                    'selection_id' => $base->getProductId(),
                    'product_id' => $base->getProductId(),
                    'name' => $base->getName(),
                    'sku' => $base->getSku(),
                    'required' => $base->getRequired(),
                    'base_attribute' => $base->getBaseAttribute(),
                    'option_id' => $base->getBaseAttribute(),
                    'qty' => $base->getQty(),
                    'type' => $base->getType(),
                    'user_defined' => $base->getUserDefined(),
                    'sis' => $base->getSis(),
                    'position' => '1',
                    'delete' => '',
                ];

            $addons = $this->addonRepository->getListBySuperProductId((int)$product->getId());
            $addonList = [];
            foreach ($addons as $key => $addon) {
                $addonList[] = [
                    'selection_id' => $addon->getProductId(),
                    'product_id' => $addon->getProductId(),
                    'name' => $addon->getName(),
                    'sku' => $addon->getSku(),
                    'required' => $addon->getRequired(),
                    'sis' => $addon->getSis(),
                    'qty' => $addon->getQty(),
                    'type' => $addon->getType(),
                    'user_defined' => $addon->getUserDefined(),
                    'position' => $addon->getPosition(),
                    'delete' => '',
                ];
            }
            usort($addonList, [$this, 'sortByPosition']);
            $data[$modelId]['addon']['super_selections'] = $addonList;

            $productRules = $this->productRuleRepository->getListBySuperProductId((int)$product->getId());
            $productRuleList = [];
            foreach ($productRules as $key => $productRule) {
                $productRuleList[] = [
                    'productrule_id' => $productRule->getId(),
                    'sku' => $productRule->getSku(),
                    'sku_hide' => $productRule->getSku(),
                    'condition' => $productRule->getCondition(),
                    'sku_dep' => $productRule->getSkuDep(),
                    'sku_dep_hide' => $productRule->getSkuDep(),
                    'position' => $productRule->getPosition(),
                    'delete' => '',
                ];
            }
            usort($productRuleList, [$this, 'sortByPosition']);
            $data[$modelId]['productrules']['rule_records'] = $productRuleList;

            $rules = $this->ruleRepository->getListBySuperProductId((int)$product->getId());
            $ruleList = [];
            foreach ($rules as $key => $rule) {
                $ruleList[] = [
                    'rule_id' => $rule->getId(), 
                    'sku' => $rule->getSku(),
                    'sku_hide' => $rule->getSku(),
                    'attribute' => $rule->getAttribute(),
                    'attribute_hide' => $rule->getAttribute(),
                    'condition' => $rule->getCondition(),
                    'option' => $rule->getOption(),
                    'option_hide' => $rule->getOption(),
                    'sku_dep' => $rule->getSkuDep(),
                    'sku_dep_hide' => $rule->getSkuDep(),
                    'attribute_dep' => $rule->getAttributeDep(),
                    'attribute_dep_hide' => $rule->getAttributeDep(),
                    'condition_dep' => $rule->getConditionDep(),
                    'option_dep' => $rule->getOptionDep(),
                    'option_dep_hide' => $rule->getOptionDep(),
                    'position' => $rule->getPosition(),
                    'delete' => '',
                ];
            }
            usort($ruleList, [$this, 'sortByPosition']);
            $data[$modelId]['rules']['rule_records'] = $ruleList;

            $pngRules = $this->pngRuleRepository->getListBySuperProductId((int)$product->getId());
            $pngRuleList = [];
            foreach ($pngRules as $key => $pngRule) {
                $pngRuleList[] = [
                    'pngrule_id' => $pngRule->getId(),
                    'sku' => $pngRule->getSku(),
                    'sku_hide' => $pngRule->getSku(),
                    'attribute' => $pngRule->getAttribute(),
                    'attribute_hide' => $pngRule->getAttribute(),
                    'attribute_dep' => $pngRule->getAttributeDep(),
                    'attribute_dep_hide' => $pngRule->getAttributeDep(),
                    'position' => $pngRule->getPosition(),
                    'delete' => '',
                ];
            }
            usort($pngRuleList, [$this, 'sortByPosition']);
            $data[$modelId]['pngrules']['rule_records'] = $pngRuleList;
        }

        return $data;
    }

    function sortByPosition($a, $b)
    {
        if ($a['position'] == $b['position']) {
            return 0;
        }
        return ($a['position'] < $b['position']) ? -1 : 1;
    }
}
