<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Config\Source\ProductPriceOptionsInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form;
use Magento\Ui\Component\Modal;

/**
 * Create Ship Bundle Items and Affect Bundle Product Selections fields
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SuperPanel extends AbstractModifier
{
    const GROUP_CONTENT = 'related';
    const CODE_SUPER_DATA = 'super-items';
    const SORT_ORDER = 0;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @param LocatorInterface $locator
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
    }


    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function modifyMeta(array $meta)
    {
        $meta = $this->removeFixedTierPrice($meta);
        $path = $this->arrayManager->findPath(static::GROUP_CONTENT, $meta, null, 'children');

        $meta = $this->arrayManager->merge(
            $path,
            $meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataScope' => '',
                            'opened' => true,
                            'sortOrder' => $this->getNextGroupSortOrder(
                                $meta,
                                static::GROUP_CONTENT,
                                static::SORT_ORDER
                            )
                        ],
                    ],
                ],
                'children' => [
                    'modal' => $this->getModalOptions(),
                    'modal_addon' => $this->getModalOptions(true),
                    'modal_images' => $this->getModalImagesOptions(),
                    'modal_import' => $this->getModalImport(),
                ]
            ]
        );

        //TODO: Remove this workaround after MAGETWO-49902 is fixed
        $superItemsGroup = $this->arrayManager->get($path, $meta);
        $meta = $this->arrayManager->remove($path, $meta);
        $meta = $this->arrayManager->set($path, $meta, $superItemsGroup);

        // add base product fieldset and add-on fieldset
        $meta['base_product'] = $this->getBaseProductOptions();
        $meta['addon_product'] = $this->getAddonProductOptions();
        $meta['product_rule'] = $this->getProductRuleOptions();
        $meta['rule_options'] = $this->getRuleOptions();
        $meta['png_rule'] = $this->getPngRuleOptions();
        $meta['png_layer'] = $this->getPngLayer();

        return $meta;
    }


    /**
     * Remove option with fixed tier price from config.
     *
     * @param array $meta
     * @return array
     */
    private function removeFixedTierPrice(array $meta)
    {
        $tierPricePath = $this->arrayManager->findPath(
            ProductAttributeInterface::CODE_TIER_PRICE,
            $meta,
            null,
            'children'
        );
        $pricePath =  $this->arrayManager->findPath(
            ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PRICE,
            $meta,
            $tierPricePath
        );
        $pricePath = $this->arrayManager->slicePath($pricePath, 0, -1) . '/value_type/arguments/data/options';

        $price = $this->arrayManager->get($pricePath, $meta);
        if ($price) {
            $meta = $this->arrayManager->remove($pricePath, $meta);
            foreach ($price as $key => $item) {
                if ($item['value'] == ProductPriceOptionsInterface::VALUE_FIXED) {
                    unset($price[$key]);
                }
            }
            $meta = $this->arrayManager->merge(
                $this->arrayManager->slicePath($pricePath, 0, -1),
                $meta,
                ['options' => $price]
            );
        }

        return $meta;
    }

    /**
     * Get Base Product structure
     *
     * @return array
     */
    protected function getModalOptions($addon = false)
    {
        $addonLabel = '';
        $addonComponent = '';
        if ($addon) {
            $addonLabel = '_addon';
            $addonComponent = '-addon';
        }

        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'isTemplate' => false,
                        'componentType' => Modal::NAME,
                        'dataScope' => '',
                        'provider' => 'product_form.product_form_data_source',
                        'options' => [
                            'title' => __('Add Products to Option'),
                            'buttons' => [
                                [
                                    'text' => __('Cancel'),
                                    'actions' => ['closeModal'],
                                ],
                                [
                                    'text' => __('Add Select Products'),
                                    'class' => 'action-primary',
                                    'actions' => [
                                        [
                                            'targetName' => 'index = super_product_listing' . $addonLabel,
                                            'actionName' => 'save'
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'children' => [
                'information-block' . $addonComponent => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/html',
                                'additionalClasses' => 'message message-error',
                                'content' => __(''),
                                'visible' => false
                            ],
                        ],
                    ],
                ],
                'super_product_listing' . $addonLabel => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => false,
                                'componentType' => 'insertListing',
                                'component' => 'Brandsmith_SuperProduct/js/form/components/insert-listing' . $addonComponent,
                                'dataScope' => 'super_product_listing' . $addonLabel,
                                'externalProvider' =>
                                    'super_product_listing' . $addonLabel . '.super_product_listing' . $addonLabel . '_data_source',
                                'selectionsProvider' =>
                                    'super_product_listing' . $addonLabel . '.super_product_listing' . $addonLabel . '.product_columns.ids',
                                'ns' => 'super_product_listing' . $addonLabel,
                                'getAttributesUrl' => $this->urlBuilder->getUrl('brandsmith_superproduct/product/GetAttributes'),
                                'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                'realTimeLink' => false,
                                'dataLinks' => ['imports' => false, 'exports' => true],
                                'behaviourType' => 'simple',
                                'externalFilterMode' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $meta;
    }

    /**
     * Get Base Product structure
     *
     * @return array
     */
    protected function getModalImagesOptions()
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'isTemplate' => false,
                        'componentType' => Modal::NAME,
                        'component' => 'Brandsmith_SuperProduct/js/modal/modal-component',
                        'dataScope' => 'data.png',
                        'provider' => 'product_form.product_form_data_source',
                        'options' => [
                            'title' => __('Manage Images'),
                            'buttons' => [
                                [
                                    'text' => __('Cancel'),
                                    'actions' => ['closeModal'],
                                ],
                                [
                                    'text' => __('Save'),
                                    'class' => 'action-primary',
                                    'actions' => ['saveModal'],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'children' => [
                'upload_image_form' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/components/html',
                                'url' => $this->urlBuilder->getUrl('brandsmith_superproduct/super/png'),
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $meta;
    }

    protected function getModalImport()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Modal::NAME,
                        'options' => [
                            'title' => __('Import PNG Layer Images'),
                            'buttons' => [
                                [
                                    'text' => __('Save'),
                                    'class' => 'action-primary', // additional class
                                    'actions' => [
                                        [
                                            'targetName' => 'index = product_form', // Element selector
                                            'actionName' => 'save', // Save parent form (product)
                                        ],
                                        'closeModal', // method name
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'children' => [
                'content' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => false,
                                'componentType' => 'container',
                            ],
                        ],
                    ],
                    'children' => [
                        'fieldset' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'label' => __('Import CSV file.'),
                                        'componentType' => 'fieldset',
                                        'collapsible' => true,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                'import_upload' => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'formElement' => 'fileUploader',
                                                'componentType' => 'fileUploader',
                                                'uploaderConfig' => [
                                                    'url' => $this->urlBuilder->getUrl(
                                                        'brandsmith_superproduct/product/uploadcsv')],
                                                'dataScope' => 'data.import_upload',
                                                'sortOrder' => 10,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get Base Product structure
     *
     * @return array
     */
    protected function getBaseProductOptions()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('Base Product'),
                        'headerLabel' => __('Base Product'),
                        'opened' => true,
                        'dataScope' => 'data.baseproduct',
                        'sortOrder' => 01,
                    ],
                ],
            ],
            'children' => [
                'super_selections' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Bundle/js/components/bundle-dynamic-rows-grid',
                                'sortOrder' => 50,
                                'additionalClasses' => 'admin__field-wide',
                                'template' => 'Brandsmith_SuperProduct/dynamic-rows/templates/default',
                                'provider' => 'product_form.product_form_data_source',
                                'dataProvider' => '${ $.dataScope }' . '.super_button_proxy',
                                'identificationDRProperty' => 'product_id',
                                'identificationProperty' => 'product_id',
                                'map' => [
                                    'product_id' => 'entity_id',
                                    'name' => 'name',
                                    'sku' => 'sku',
                                    'type' => 'type_id',
                                    'price' => 'price',
                                    'delete' => '',
                                    'user_defined' => '',
                                    'selection_id' => '',
                                    'required' => '',
                                ],
                                'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                                'imports' => [
                                    'inputType' => '${$.provider}:${$.dataScope}.type',
                                ],
                                'source' => 'product',
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getSuperSelections(),
                    ]
                ],
                'modal_set' => $this->getModalSet(),
            ]
        ];
    }

    /**
     * Get super selections structure
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getSuperSelections($addon = false)
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'isTemplate' => true,
                        'component' => 'Magento_Ui/js/dynamic-rows/record',
                        'is_collection' => true,
                        'imports' => [
                            'inputType' => '${$.parentName}:inputType',
                        ],
                        'exports' => [
                            'isDefaultValue' => '${$.parentName}:isDefaultValue.${$.index}',
                        ],
                    ],
                ],
            ],
            'children' => [
                'selection_id' => $this->getHiddenColumn('selection_id', 10),
                'option_id' => $this->getHiddenColumn('option_id', 20),
                'product_id' => $this->getHiddenColumn('product_id', 30),
                'delete' => $this->getHiddenColumn('delete', 40),
                'name' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/name',
                                // 'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'label' => __('Name'),
                                'dataScope' => 'name',
                                'sortOrder' => 60,
                            ],
                        ],
                    ],
                ],
                'sku' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'label' => __('SKU'),
                                'dataScope' => 'sku',
                                'sortOrder' => 70,
                            ],
                        ],
                    ],
                ],
                'type' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'label' => __('Type'),
                                'dataScope' => 'type',
                                'sortOrder' => 80,
                            ],
                        ],
                    ],
                ],
                'base_attribute' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/base-attribute',
                                'dataScope' => 'base_attribute',
                                'getAttributesUrl' => $this->urlBuilder->getUrl('brandsmith_superproduct/product/GetAttributes'),
                                'label' => __('Base Attribute'),
                                'sortOrder' => 90,
                            ],
                        ],
                    ],
                ],
                'required' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Number::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'componentType' => Form\Field::NAME,
                                'dataScope' => 'required',
                                'label' => __('Required'),
                                'value' => '0',
                                'valueMap' => [
                                    'true' => '1',
                                    'false' => '0',
                                ],
                                'fit' => true,
                                'sortOrder' => 100,
                            ],
                        ],
                    ],
                ],
                'sis' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Number::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'componentType' => Form\Field::NAME,
                                'dataScope' => 'sis',
                                'label' => __('SIS'),
                                'value' => '0',
                                'valueMap' => [
                                    'true' => '1',
                                    'false' => '0',
                                ],
                                'fit' => true,
                                'sortOrder' => 105,
                            ],
                        ],
                    ],
                ],
                'qty' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'component' => 'Brandsmith_SuperProduct/js/form/element/qty',
                                'formElement' => Form\Element\Input::NAME,
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Number::NAME,
                                'label' => __('Default Qty'),
                                'dataScope' => 'qty',
                                'fit' => true,
                                'sortOrder' => 110,
                                'validation' => [
                                    'required-entry' => true,
                                    'validate-number' => true,
                                    'validate-greater-than-zero' => true
                                ],
                            ],
                        ],
                    ],
                ],
                'user_defined' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'dataType' => Form\Element\DataType\Price::NAME,
                                'component' => 'Magento_Bundle/js/components/bundle-user-defined-checkbox',
                                'label' => __('User Defined'),
                                'dataScope' => 'user_defined',
                                'value' => '1',
                                'valueMap' => ['true' => '1', 'false' => '0'],
                                'fit' => true,
                                'sortOrder' => 120,
                                'imports' => [
                                    'inputType' => '${$.parentName}:inputType',
                                ],
                            ],
                        ],
                    ],
                ],
                'position' => $this->getHiddenColumn('position', 130),
                'action_delete' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => 'actionDelete',
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/action-product-delete',
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'label' => '',
                                'fit' => true,
                                'sortOrder' => 140,
                            ],
                        ],
                    ],
                ],
                'duplicate' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'sortOrder' => 150,
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'dataScope' => 'duplicate',
                                'component' => 'Brandsmith_SuperProduct/js/form/components/button',
                                'actions' => [
                                    [
                                        'targetName' => 'product_form.product_form.addon_product.super_selections',
                                        'actionName' => 'duplicateEvent',
                                        'params' => ['${ $.parentName }'],
                                    ]
                                ],
                                'title' => 'Duplicate',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        if (!$addon) {
            unset($meta['children']['user_defined']);
            unset($meta['children']['duplicate']);
        }
        if ($addon)
            unset($meta['children']['base_attribute']);
        return $meta;
    }

    /**
     * Prepares configuration for the hidden columns
     *
     * @param string $columnName
     * @param int $sortOrder
     * @return array
     */
    protected function getHiddenColumn($columnName, $sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Form\Field::NAME,
                        'dataType' => Form\Element\DataType\Text::NAME,
                        'formElement' => Form\Element\Input::NAME,
                        'dataScope' => $columnName,
                        'visible' => false,
                        'additionalClasses' => ['_hidden' => true],
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    /**
     * Get configuration for the modal set: modal and trigger button for add-on product
     *
     * @return array
     */
    protected function getModalSet($addon = false)
    {
        $addonLabel = '';
        $buttonLabel = __('Select Base Product');
        if ($addon) {
            $addonLabel = '_addon';
            $buttonLabel = __('Select Add-on Products');
        }
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'sortOrder' => 60,
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'dataScope' => 'super_button_proxy' . $addonLabel,
                        'component' => 'Magento_Catalog/js/bundle-proxy-button',
                        'provider' => 'product_form.product_form_data_source',
                        'listingDataProvider' => 'super_product_listing' . $addonLabel,
                        'actions' => [
                            [
                                'targetName' => 'product_form.product_form.' . static::GROUP_CONTENT . '.modal' . $addonLabel,
                                'actionName' => 'toggleModal'
                            ],
                            [
                                'targetName' => 'product_form.product_form.' . static::GROUP_CONTENT
                                    . '.modal' . $addonLabel . '.super_product_listing' . $addonLabel,
                                'actionName' => 'render'
                            ]
                        ],
                        'title' => $buttonLabel,
                    ],
                ],
            ],
        ];
    }

    /**
     * Get Addon Product structure
     *
     * @return array
     */
    protected function getAddonProductOptions()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('Add-on Products'),
                        'headerLabel' => __('Add-on Products'),
                        'opened' => true,
                        'dataScope' => 'data.addon',
                        'sortOrder' => 02,
                    ],
                ],
            ],
            'children' => [
                'super_selections' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/addon-dynamic-rows-grid',
                                'sortOrder' => 50,
                                'additionalClasses' => 'admin__field-wide',
                                'template' => 'Brandsmith_SuperProduct/dynamic-rows/templates/default',
                                'provider' => 'product_form.product_form_data_source',
                                'dataProvider' => '${ $.dataScope }' . '.super_button_proxy_addon',
                                'identificationDRProperty' => 'product_id',
                                'identificationProperty' => 'product_id',
                                'map' => [
                                    'product_id' => 'entity_id',
                                    'name' => 'name',
                                    'sku' => 'sku',
                                    'type' => 'type_id',
                                    'price' => 'price',
                                    'delete' => '',
                                    'user_defined' => '',
                                    'selection_id' => '',
                                    'required' => '',
                                    'sis' => '',
                                ],
                                'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                                'imports' => [
                                    'inputType' => '${$.provider}:${$.dataScope}.type',
                                ],
                                'source' => 'product',
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getSuperSelections(true),
                    ]
                ],
                'modal_set' => $this->getModalSet(true),
            ]
        ];
    }

    /**
     * Get Product rules fieldset
     *
     * @return array
     */
    protected function getProductRuleOptions()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('Product Dependencies'),
                        'headerLabel' => __('Product Dependencies'),
                        'opened' => true,
                        'sortOrder' => 03,
                    ],
                ],
            ],
            'children' => [
                'rule_records' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/dynamic-rows-product-rule',
                                'template' => 'ui/dynamic-rows/templates/default',
                                'additionalClasses' => 'admin__field-wide',
                                'dataScope' => 'data.productrules',
                                'addButtonLabel' => 'Add New Rule',
                                'isDefaultFieldScope' => 'is_default',
                                'columnsHeader' => true
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getProductRuleSelections()
                    ]
                ],
            ]
        ];
    }

    /**
     * Get rule selections structure
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getProductRuleSelections()
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'isTemplate' => true,
                        'component' => 'Magento_Ui/js/dynamic-rows/record',
                        'is_collection' => true,
                    ],
                ],
            ],
            'children' => [
                'productrule_id' => $this->getHiddenColumn('productrule_id', 0),
                'sku_hide' => $this->getHiddenColumn('sku_hide', 10),
                'sku_dep_hide' => $this->getHiddenColumn('sku_dep_hide', 25),
                'delete' => $this->getHiddenColumn('delete', 40),
                'sku' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/sku_rule',
                                'dataScope' => 'sku',
                                'label' => __('Product'),
                                'sortOrder' => 50,
                            ],
                        ],
                    ],
                ],
                'condition' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Boolean::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/toggle_sku',
                                'dataScope' => 'condition',
                                'prefer' => 'toggle',
                                'value' => '1',
                                'valueMap' => [
                                    'false' => '0',
                                    'true' => '1'
                                ],
                                'sortOrder' => 70,
                            ],
                        ],
                    ],
                ],
                'sku_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/sku_rule_dep',
                                'dataScope' => 'sku_dep',
                                'label' => __('Product'),
                                'sortOrder' => 90,
                            ],
                        ],
                    ],
                ],
                'position' => $this->getHiddenColumn('position', 130),
                'action_delete' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => 'actionDelete',
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'label' => '',
                                'fit' => true,
                                'sortOrder' => 140,
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $meta;
    }

    /**
     * Get Attribute dependency rules fieldset
     *
     * @return array
     */
    protected function getRuleOptions()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('Attribute Dependencies'),
                        'headerLabel' => __('Attribute Dependencies'),
                        'opened' => true,
                        'sortOrder' => 04,
                    ],
                ],
            ],
            'children' => [
                'rule_records' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/dynamic-rows',
                                'template' => 'ui/dynamic-rows/templates/default',
                                'getUrl' => $this->urlBuilder->getUrl('brandsmith_superproduct/product/GetAttributeMapper'),
                                'additionalClasses' => 'admin__field-wide',
                                'dataScope' => 'data.rules',
                                'addButtonLabel' => 'Add New Rule',
                                'isDefaultFieldScope' => 'is_default',
                                'columnsHeader' => true
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getRuleSelections()
                    ]
                ],
            ]
        ];
    }

    /**
     * Get rule selections structure
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getRuleSelections()
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'isTemplate' => true,
                        'component' => 'Magento_Ui/js/dynamic-rows/record',
                        'is_collection' => true,
                    ],
                ],
            ],
            'children' => [
                'rule_id' => $this->getHiddenColumn('rule_id', 0),
                'sku_hide' => $this->getHiddenColumn('sku_hide', 10),
                'attribute_hide' => $this->getHiddenColumn('attribute_hide', 15),
                'option_hide' => $this->getHiddenColumn('option_hide', 20),
                'sku_dep_hide' => $this->getHiddenColumn('sku_dep_hide', 25),
                'attribute_dep_hide' => $this->getHiddenColumn('attribute_dep_hide', 30),
                'option_dep_hide' => $this->getHiddenColumn('option_dep_hide', 35),
                'delete' => $this->getHiddenColumn('delete', 40),
                'if_label' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'value' => __('IF'),
                                'sortOrder' => 45,
                            ],
                        ],
                    ],
                ],
                'sku' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/sku',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'sku',
                                'label' => __('Product'),
                                'sortOrder' => 50,
                            ],
                        ],
                    ],
                ],
                'attribute' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/attribute',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'attribute',
                                'label' => __('Attribute'),
                                'sortOrder' => 60,
                            ],
                        ],
                    ],
                ],
                'condition' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Boolean::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/toggle',
                                'dataScope' => 'condition',
                                'prefer' => 'toggle',
                                'value' => '1',
                                'valueMap' => [
                                    'false' => '0',
                                    'true' => '1'
                                ],
                                'sortOrder' => 70,
                            ],
                        ],
                    ],
                ],
                'option' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/option',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'option',
                                'label' => __('Option'),
                                'sortOrder' => 80,
                            ],
                        ],
                    ],
                ],
                'then_label' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'value' => __('THEN'),
                                'sortOrder' => 85,
                            ],
                        ],
                    ],
                ],
                'sku_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/sku_dep',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'sku_dep',
                                'label' => __('Product'),
                                'sortOrder' => 90,
                            ],
                        ],
                    ],
                ],
                'attribute_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/attribute_dep',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'attribute_dep',
                                'label' => __('Attribute'),
                                'sortOrder' => 100,
                            ],
                        ],
                    ],
                ],
                'condition_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Boolean::NAME,
                                'formElement' => Form\Element\Checkbox::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/toggle',
                                'dataScope' => 'condition_dep',
                                'prefer' => 'toggle',
                                'value' => '1',
                                'valueMap' => [
                                    'false' => '0',
                                    'true' => '1'
                                ],
                                'sortOrder' => 110,
                            ],
                        ],
                    ],
                ],
                'option_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/option_dep',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'option_dep',
                                'label' => __('Option'),
                                'sortOrder' => 120,
                            ],
                        ],
                    ],
                ],
                'position' => $this->getHiddenColumn('position', 130),
                'action_delete' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => 'actionDelete',
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'label' => '',
                                'fit' => true,
                                'sortOrder' => 140,
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $meta;
    }

    /**
     * Get Png rules fieldset
     *
     * @return array
     */
    protected function getPngRuleOptions()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('PNG Layer Dependencies'),
                        'headerLabel' => __('PNG Layer Dependencies'),
                        'opened' => true,
                        'sortOrder' => 05,
                    ],
                ],
            ],
            'children' => [
                'rule_records' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/dynamic-rows-png-rule',
                                'template' => 'ui/dynamic-rows/templates/default',
                                'additionalClasses' => 'admin__field-wide',
                                'dataScope' => 'data.pngrules',
                                'addButtonLabel' => 'Add New Rule',
                                'isDefaultFieldScope' => 'is_default',
                                'columnsHeader' => true
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getPngRuleSelections()
                    ]
                ],
            ]
        ];
    }

    /**
     * Get rule selections structure
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getPngRuleSelections()
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'isTemplate' => true,
                        'component' => 'Magento_Ui/js/dynamic-rows/record',
                        'is_collection' => true,
                    ],
                ],
            ],
            'children' => [
                'pngrule_id' => $this->getHiddenColumn('pngrule_id', 0),
                'sku_hide' => $this->getHiddenColumn('sku_hide', 10),
                'attribute_hide' => $this->getHiddenColumn('attribute_hide', 15),
                'attribute_dep_hide' => $this->getHiddenColumn('attribute_dep_hide', 30),
                'delete' => $this->getHiddenColumn('delete', 40),
                'sku' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/sku_pngrule',
                                'dataScope' => 'sku',
                                'label' => __('Product'),
                                'sortOrder' => 50,
                            ],
                        ],
                    ],
                ],
                'attribute' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/attribute_pngrule',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'attribute',
                                'label' => __('Attribute'),
                                'sortOrder' => 60,
                            ],
                        ],
                    ],
                ],
                'mod_label' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'value' => __('MODIFIES'),
                                'sortOrder' => 85,
                            ],
                        ],
                    ],
                ],
                'attribute_dep' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/attribute_pngrule_dep',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'attribute_dep',
                                'label' => __('Attribute'),
                                'sortOrder' => 100,
                            ],
                        ],
                    ],
                ],
                'position' => $this->getHiddenColumn('position', 130),
                'action_delete' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => 'actionDelete',
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'label' => '',
                                'fit' => true,
                                'sortOrder' => 140,
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $meta;
    }

    /**
     * Get PNG LAYER fieldset
     *
     * @return array
     */
    protected function getPngLayer()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'label' => __('PNG layer Images'),
                        'headerLabel' => __('PNG layer Images'),
                        'opened' => true,
                        'sortOrder' => 06,
                    ],
                ],
            ],
            'children' => [
                'action_container' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'sortOrder' => 10,
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'label' => false,
                                'template' => 'Brandsmith_SuperProduct/form/components/complex',
                            ],
                        ],
                    ],
                    'children' => [
                        'export' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'sortOrder' => 1,
                                        'formElement' => 'container',
                                        'componentType' => 'container',
                                        'dataScope' => 'export',
                                        'elementTmpl' => 'Brandsmith_SuperProduct/form/components/button/export',
                                        'component' => 'Brandsmith_SuperProduct/js/form/components/export',
                                        'getUrl' => $this->urlBuilder->getUrl('brandsmith_superproduct/product/ExportPngLayer'),
                                        'actions' => [
                                            ['targetName' => 'index = export',
                                                'actionName' => 'exportAction'],
                                        ],
                                        'title' => 'Export Template',
                                    ],
                                ],
                            ],
                        ],
                        'import' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'title' => __('Import CSV'),
                                        'formElement' => 'container',
                                        'componentType' => 'container',
                                        'elementTmpl' => 'Brandsmith_SuperProduct/form/components/button/import',
                                        'component' => 'Magento_Ui/js/form/components/button',
                                        'actions' => [
                                            [
                                                'targetName' => 'index=modal_import', // selector
                                                'actionName' => 'openModal', // method name
                                            ],
                                        ],
                                        'displayAsLink' => false,
                                        'sortOrder' => 2,
                                    ],
                                ],
                            ],
                        ],
                    ]
                ],
                'png_records' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/dynamic-rows/dynamic-rows-png',
                                'getUrl' => $this->urlBuilder->getUrl('brandsmith_superproduct/product/GetPngLayer'),
                                'template' => 'Brandsmith_SuperProduct/dynamic-rows/templates/default',
                                'additionalClasses' => 'admin__field-wide',
                                'dataScope' => 'data.png',
                                'isDefaultFieldScope' => 'is_default',
                                'sortOrder' => 20,
                                'columnsHeader' => true
                            ],
                        ],
                    ],
                    'children' => [
                        'record' => $this->getPngSelections()
                    ]
                ],
            ]
        ];
    }

    /**
     * Get PNG selections structure
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getPngSelections()
    {
        $meta = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'isTemplate' => true,
                        'component' => 'Magento_Ui/js/dynamic-rows/record',
                        'is_collection' => true,
                    ],
                ],
            ],
            'children' => [
                'png_id' => $this->getHiddenColumn('png_id', 0),
                'delete' => $this->getHiddenColumn('delete', 40),
                'sku_hide' => $this->getHiddenColumn('sku_hide', 40),
                'attribute_hide' => $this->getHiddenColumn('attribute_hide', 40),
                'option_hide' => $this->getHiddenColumn('option_hide', 20),
                'sku' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'dataScope' => 'sku',
                                'label' => __('Product'),
                                'sortOrder' => 50,
                            ],
                        ],
                    ],
                ],
                'attribute' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'dataScope' => 'attribute',
                                'label' => __('Attribute'),
                                'sortOrder' => 60,
                            ],
                        ],
                    ],
                ],
                'type' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'dataScope' => 'type',
                                'label' => __('Type'),
                                'sortOrder' => 80,
                            ],
                        ],
                    ],
                ],
                'default_option' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Select::NAME,
                                'componentType' => Form\Field::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/option_png',
                                'template' => 'Brandsmith_SuperProduct/form/element/select',
                                'dataScope' => 'default_option',
                                'label' => __('Default Option'),
                                'sortOrder' => 85,
                            ],
                        ],
                    ],
                ],
                'multi_layer' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/name',
                                'dataScope' => 'multi_layer',
                                'label' => __('View Index'),
                                'value' => '0',
                                'fit' => true,
                                'sortOrder' => 87,
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'elementTmpl' => 'ui/dynamic-rows/cells/text',
                                'dataScope' => 'options',
                                'label' => __('Options'),
                                'sortOrder' => 90,
                            ],
                        ],
                    ],
                ],
                'status' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'dataType' => Form\Element\DataType\Text::NAME,
                                'formElement' => Form\Element\Input::NAME,
                                'component' => 'Brandsmith_SuperProduct/js/form/element/status',
                                'elementTmpl' => 'Brandsmith_SuperProduct/dynamic-rows/cells/status',
                                'dataScope' => 'status',
                                'label' => __('Status'),
                                'sortOrder' => 100,
                            ],
                        ],
                    ],
                ],
                'manage_image' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'sortOrder' => 140,
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'dataScope' => 'manage_image',
                                'component' => 'Brandsmith_SuperProduct/js/form/components/button',
                                'actions' => [
                                    [
                                        'targetName' => 'product_form.product_form.' . static::GROUP_CONTENT . '.modal_images',
                                        'actionName' => 'toggleModal'
                                    ],
                                    [
                                        'targetName' => 'product_form.product_form.' . static::GROUP_CONTENT
                                            . '.modal_images.upload_image_form',
                                        'actionName' => 'loadDataWithParamter',
                                        'params' => ['${ $.parentName }'],
                                    ]
                                ],
                                'title' => 'Manage Images',
                            ],
                        ],
                    ],
                ],
                'addview' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'sortOrder' => 150,
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'dataScope' => 'addview',
                                'component' => 'Brandsmith_SuperProduct/js/form/components/button',
                                'actions' => [
                                    [
                                        'targetName' => 'product_form.product_form.png_layer.png_records',
                                        'actionName' => 'addviewEvent',
                                        'params' => ['${ $.parentName }'],
                                    ]
                                ],
                                'title' => 'Add View',
                            ],
                        ],
                    ],
                ],
                'position' => $this->getHiddenColumn('position', 130),
            ],
        ];
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Check that store is default
     *
     * @return bool
     */
    protected function isDefaultStore()
    {
        return $this->locator->getProduct()->getStoreId() == 0;
    }
}
