<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Productdependency extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_modelConfigurableType;

    /**
     * @var \Brandsmith\SuperProduct\Helper\Data
     */
    protected $_superProductHelper;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_helperPricing;

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;



    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Brandsmith\SuperProduct\Helper\Data $superProductHelper,
        \Magento\Framework\Pricing\Helper\Data $helperPricing,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_storeManager = $storeManager;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_superProductHelper = $superProductHelper;
        $this->_helperPricing = $helperPricing;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
    }

    public function execute()
    {
        $param = $this->getRequest()->getParams();

        $_super_product_id = $param['super_product_id'];
        $_sku = $param['sku'];

        $_case = null;
        $response = null;

        $_super_product = $this->_productRepository->getById($_super_product_id);
        $_product_rule = $this->getProductRuleData($_super_product, $_super_product_id, $_sku);
        $_condition_product = $this->_productRepository->get($_sku);
        $_condition_product_id = $_condition_product->getId();

        if( $_product_rule['collection_type'] == false && ($_product_rule['collection_count'] < 1) ) {

            $response = [
                'errors' => true,
                'message' => 'Empty rule!'
            ];

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($response);
            return $resultJson;
        }

        // Only 1 Collection -
        if( $_product_rule['collection_type'] == true && ($_product_rule['collection_count'] == 1) ) {
            $_product_rule_data = $_product_rule['product_rule_data'];

            $_condition = $_product_rule_data->getCondition();
            $_sku_dependency = $_product_rule_data->getSkuDep();
            $_duplicate_rule = $_product_rule['duplicate_rule'];

            $_product = $this->_productRepository->get($_sku_dependency);
            $_product_id = $_product->getId();

            // Check Condition Require or Exclude
            switch ($_condition) {
                case "1":
                    // Case 1 - Product Require
                    $_case = 'require';
                    break;
                case "0":
                    // Case 2 - Product Exclude
                    $_case = 'exclude';
                    break;
            }

            $response = [
                'errors' => false,
                'collection_count' => 'one',
                'duplicate_rule' => $_duplicate_rule,
                'case' => $_case,
                'condition' => $_condition,
                'condition_product_id' => $_condition_product_id,
                'sku_dependency' => $_sku_dependency,
                'product_id' => $_product_id
            ];
        }

        // Have More than 1
        if( $_product_rule['collection_type'] == true && ($_product_rule['collection_count'] > 1) ) {
            $_product_rule_collection = $_product_rule['product_rule_data'];
            $_duplicate_rule = null;
            $_data = array();
            foreach ($_product_rule_collection as $key => $_product_rule_data ) {

                $_condition = $_product_rule_data['condition'];
                $_sku_dependency = $_product_rule_data['sku_dep'];

                $_duplicate_rule = $this->getDuplicateRule($_super_product_id, $_sku_dependency, $_sku);
                $_product = $this->_productRepository->get($_product_rule_data['sku_dep']);
                $_product_id = $_product->getId();

                // Check Condition Require or Exclude
                switch ($_condition) {
                    case "1":
                        // Case 1 - Product Require
                        $_case = 'require';
                        break;
                    case "0":
                        // Case 2 - Product Exclude
                        $_case = 'exclude';
                        break;
                }

                $_data[$key] = array (
                    'case' => $_case,
                    'duplicate_rule' => $_duplicate_rule,
                    'condition' => $_condition,
                    'condition_product_id' => $_condition_product_id,
                    'sku_dependency' => $_sku_dependency,
                    'product_id' => $_product_id,
                );
            }

            $response = [
                'errors' => false,
                'collection_count' => 'many',
                'rule_collection' => array_values($_data)
            ];

        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;

    }

    public function getProductRuleData($_super_product, $_super_product_id, $_sku)
    {
        $_product_rule = $_super_product->getTypeInstance()->getProductRuleBySuperProductId($_super_product_id, $_sku);

        $_data = array();
        // Empty Product Dependency Rule
        if($_product_rule['collection_count'] < 1 ) {
            $_data['collection_type'] = false;
            $_data['collection_count'] = $_product_rule['collection_count'];
        }

        //  If there is more than 1 Product Rule
        if($_product_rule['collection_count'] > 1) {
            $_data['collection_type'] = true;
            $_data['collection_count'] = $_product_rule['collection_count'];
            $_data['product_rule_data'] = $_product_rule['product_rule'];
        }

        if($_product_rule['collection_count'] == 1) {
            $_data['collection_type'] = true;
            $_data['collection_count'] = $_product_rule['collection_count'];
            $_data['product_rule_data'] = $_product_rule['product_rule'];
            $_data['duplicate_rule'] = $_product_rule['duplicate_rule'];
        }

        return $_data;
    }

    /**
     * get Duplicate rule
     *
     * @return string
     */
    public function getDuplicateRule($_superProductId, $_skuDependency, $_sku)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku', $_skuDependency);
        $collection->addFieldToFilter('sku_dep', $_sku);
        $collection->load();
        return count($collection) > 0 ? true : false;
    }

}
