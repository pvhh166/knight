<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Cart as CustomerCart;

class Cart extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_storeManager;

    protected $_quote;

    protected $_product;

    protected $_cartItemFactory;

    protected $_cartRepositoryInterface;

    protected $_cartManagementInterface;

    protected $_cart;

    protected $serializer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTimes;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_modelConfigurableType;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_helperPricing;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        CustomerCart $cart,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Magento\Framework\Pricing\Helper\Data $helperPricing
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_storeManager = $storeManager;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->_cart = $cart;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_helperPricing = $helperPricing;
    }

    public function execute()
    {
        $_param = $this->getRequest()->getParams();

        if($_param['validate'] == 0 || empty($_param['super_attributes']) ) {
            $_product_collection = $this->updateArrayInArray($_param['product_collection']);

            $message = __(  "Can't add product to cart." );
            //$this->messageManager->addErrorMessage($message);
            $_response = array (
                'errors' => true,
                'product_require' => $_product_collection
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        $_super_product_id = $_param['super_product_id'];
        $_super_product_qty = $_param['product_qty'];
        $_super_product_attributes = $_param['super_attributes'];
        $_super_product_addon_qty = '';
        if(!empty($_param['addon_qty'])) {
            $_super_product_addon_qty = $_param['addon_qty'];
        }

        $_super_product = $this->_productRepository->getById($_super_product_id);
        $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);

        // Add Quote for Super Product First
        $quote = $this->_objectManager->get('\Magento\Checkout\Model\Cart');
        $_param_super_product = array(
            'product' => $_super_product_id,
            'qty' => $_super_product_qty
        );

        $_super_custom_price = $this->getTotalPrice($_getChildProduct, $_super_product_addon_qty, $_super_product_qty);
        $_super_product->setPrice($_super_custom_price);

        if(isset($_param['get_cart_price']) && $_param['get_cart_price'] == 1) {
            $_formatPrice = $this->_helperPricing->currency($_super_custom_price * $_super_product_qty, true, false);
            $_response = array (
                'errors' => false,
                'product_final_price' => $_formatPrice
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        $this->_cart->addProduct($_super_product, $_param_super_product);

        // Add Quote for Child Super Product
        foreach ($_getChildProduct as $_item) {
            $_addon_qty = null;
            $_item_id = $_item['simple_product_id'];
            $_default_product_id = $_item['default_product_id'];

            // Count Total Qty
            $_default_qty = $_item['default_qty'];
            if(!empty($_super_product_addon_qty)) {

                if (array_key_exists($_default_product_id, $_super_product_addon_qty)) {
                    $_addon_qty = $_super_product_addon_qty[$_default_product_id];
                }
            }

            if($_addon_qty != null) {
                // This Quantity defined by customer
                $_total_qty = $_addon_qty;
            } else {
                // This Quantity defined by admin
                $_total_qty = $_default_qty;
            }

            $_product = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($_item_id);
            $_params = array(
                'product' => $_item_id,
                'qty' => $_total_qty
            );

            $_product->setPrice(0);
            $this->_cart->addProduct($_product, $_params);
        }

        $this->_cart->save();

        //  Update Quote Item - add Item Option
        //  Add Product Name - Quantity to additional_options

        foreach($_param as $key => $data) {
            if($key == 'super_attributes') {
                foreach ($_param[$key] as $_i => $_value) {
                    $_productAddOn = $_super_product->getTypeInstance()->getProductAddOn($_super_product_id, $_value['product_id']);
                    $_productSku = null;

                    if( !empty($_productAddOn->getData()) ) {
                        //  Get Add-on Information
                        $_addon_data = $_productAddOn->getData();
                        $_productName = $_addon_data['name'];

                        if(!empty($_super_product_addon_qty)) {
                            if (array_key_exists($_value['product_id'], $_super_product_addon_qty)) {
                                $_productQuantity = $_super_product_addon_qty[$_value['product_id']];
                            }
                        }

                        //$_productQuantity = $_addon_data['qty'];
                        $_productSku = $_addon_data['sku'];
                    } else {
                        //  Get Base Information
                        $_baseProduct = $_super_product->getTypeInstance()->getBaseProductById($_super_product_id, $_value['product_id']);
                        $_productName = $_baseProduct->getName();
                        if(!empty($_super_product_addon_qty)) {
                            if (array_key_exists($_value['product_id'], $_super_product_addon_qty)) {
                                $_productQuantity = $_super_product_addon_qty[$_value['product_id']];
                            }
                        }
                        //$_productQuantity = $_baseProduct->getQty();
                        $_productSku = $_baseProduct->getSku();
                    }

                    // Update Sku Simple product
                    $_simpleSkuForParam = $this->setSimpleSkuForParam($_value, $_getChildProduct);
                    $_imageForAddOn = $this->setImageForAddOn($_value, $_getChildProduct);
                    $_param[$key][$_i]['product_name'] = $_productName;
                    $_param[$key][$_i]['product_sku'] = $_productSku;
                    $_param[$key][$_i]['product_simple_sku'] = $_simpleSkuForParam;
                    $_param[$key][$_i]['product_thumbnail_img'] = $_imageForAddOn;
                    $_param[$key][$_i]['product_quantity'] = $_productQuantity;
                }
            }
        }

        $this->_logger->debug("super_attributes: " . json_encode($_param['super_attributes']) );

        // Add New Param Attribute for MiniCart
        $_param['super_attributes_defined'] = $_param['super_attributes'];

        // Update Attribute Option Selected From Code to Label
        foreach ($_param['super_attributes_defined'] as $key => $value) {
            if ( $_param['super_attributes_defined'][$key]['product_type'] == 'configurable' ) {
                $attributes_defined_option_selected = $_param['super_attributes_defined'][$key]['option_selected'];
                $_convertAttributes = $_super_product->getTypeInstance()->getConvertAttributes($attributes_defined_option_selected);
                $_param['super_attributes_defined'][$key]['option_selected'] = $_convertAttributes;
            }
        }

        $_getAllItems = $quote->getQuote()->getAllItems();
        //$_getAllItems = $this->_cart->getItems();

        $parent_item_id = null;
        $_gmtTimestamp = $this->_dateTimes->gmtTimestamp();
        foreach($_getAllItems as $item) {
            // parent_item_id

            if($item->getProductId() == $_super_product_id ) {
                if(empty($item->getAdditionalData())) {
                    $item->addOption([
                        'product_id' => $_super_product_id,
                        'code'  => 'super_custom_option',
                        'value' => $this->serializer->serialize($_param)
                    ]);

                    $item->addOption([
                        'product_id' => $_super_product_id,
                        'code'  => 'super_custom_request',
                        'value' => $this->serializer->serialize(array('gmtTimestamp' => $_gmtTimestamp))
                    ]);

                    $item->saveItemOptions();
                    //$item->setCustomPrice($_super_custom_price);
                    $item->setOriginalCustomPrice($_super_custom_price);
                    $item->getProduct()->setIsSuperMode(true);
                    $item->setAdditionalData($item->getItemId());
                    $item->save();
                    $parent_item_id = $item->getAdditionalData();
                }
            }

            if($item->getProductId() != $_super_product_id && empty($item->getParentItemId()) ) {
                $item->addOption([
                    'product_id' => $item->getProductId(),
                    'code'  => 'child_custom_request',
                    'value' => $this->serializer->serialize(array('gmtTimestamp' => $_gmtTimestamp, 'product_id' => $item->getProductId(), 'super_product' => $_super_product_id))
                ]);
                $item->setParentItemId($parent_item_id);
                $item->setCustomPrice(0);
                $item->save();
            }
        }

        $message = null;

        if($_param['request'] == 1) {
            $message = __(
                'You updated '.$_super_product->getName().' to shopping cart.'
            );
        }

        if($_param['request'] != 1) {
            $message = __(
                'You added '.$_super_product->getName().' to shopping cart.'
            );
        }

        //$this->messageManager->addSuccessMessage($message);

        $response = [
            'errors' => false,
            'request' => $_param['request'],
            'messages' => $message
        ];

        if($_param['request'] == 1) {
            $response['redirectUrl'] = $this->_url->getUrl('checkout/cart');
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }

    public function updateArrayInArray($_array)
    {
        $_newArray = array();

        foreach ($_array as $_key => $_value) {
            $_newArray['product_id_'.$_key] = $_value['product_id'];
        }

        return array_unique($_newArray);
    }

    public function getTotalPrice($_getChildProduct, $_super_product_addon_qty, $_super_product_qty = 1)
    {
        $_totalCustomPrice = 0;
        foreach ($_getChildProduct as $_item) {
            $_addon_qty = null;
            $_item_id = $_item['simple_product_id'];
            $_default_product_id = $_item['default_product_id'];

            // Count Total Qty
            $_default_qty = $_item['default_qty'];
            if(!empty($_super_product_addon_qty)) {
                if (array_key_exists($_default_product_id, $_super_product_addon_qty)) {
                    $_addon_qty = $_super_product_addon_qty[$_default_product_id];
                }
            }

            if($_addon_qty != null) {
                // This Quantity defined by customer
                $_total_qty = $_addon_qty;
            } else {
                // This Quantity defined by admin
                $_total_qty = $_default_qty;
            }
            $_product = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($_item_id);
            // $_product_final_price = $_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
            $_product_final_price = min($_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),$_product->getPriceModel()->getFinalPrice($_super_product_qty*$_total_qty,$_product));
            $_totalCustomPrice += $_product_final_price*$_total_qty;
        }
        return $_totalCustomPrice;
    }

    public function setSimpleSkuForParam($_productInfo, $_getChildProduct)
    {
        $_simpleSku = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_simpleSku = $_item['simple_product_sku'];
            }
        }

        return $_simpleSku;
    }

    public function setImageForAddOn($_productInfo, $_getChildProduct)
    {
        $_thumbnailImg = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_thumbnailImg = $_item['product_thumbnail_img'];
            }
        }
        return $_thumbnailImg;
    }
}
