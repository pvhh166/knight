<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Price extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_modelConfigurableType;

    /**
     * @var \Brandsmith\SuperProduct\Helper\Data
     */
    protected $_superProductHelper;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_helperPricing;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_catalogHelperImage;

    /**
     * @var \Brandsmith\TrafficLightStock\Helper\Data 
     */
    protected $_trafficLightStockHelper;

    protected $custsession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Brandsmith\SuperProduct\Helper\Data $superProductHelper,
        \Magento\Framework\Pricing\Helper\Data $helperPricing,
        \Magento\Customer\Model\Session $custsession,
        \Magento\Catalog\Helper\Image $_catalogHelperImage,
        \Brandsmith\TrafficLightStock\Helper\Data $trafficLightStockHelper
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_storeManager = $storeManager;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_superProductHelper = $superProductHelper;
        $this->_helperPricing = $helperPricing;
        $this->custsession = $custsession;
        $this->_catalogHelperImage = $_catalogHelperImage;
        $this->_trafficLightStockHelper = $trafficLightStockHelper;
    }

    public function execute()
    {
        $param = $this->getRequest()->getParams();

        if($param['require'] == 'currency') {
            $_price = $param['data_price'];

            $_formatPrice = $this->_helperPricing->currency($_price, true, false);

            $_response = array (
                'errors' => false,
                'product_final_price' => $_formatPrice
            );

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        if($param['require'] == 'get_simple_price') {
            $_product = $this->_productRepository->get($param['product_sku']);
            $_formatPrice = '';
            
            $_product_final_price = min($_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),$_product->getPriceModel()->getFinalPrice($param['product_qty'],$_product));
            if($this->custsession->isLoggedIn()){
                $_formatPrice = $this->_helperPricing->currency($_product_final_price, true, false);
            }

            $_response = array (
                'errors' => false,
                'product_id' => $_product->getId(),
                'product_final_price' => $_formatPrice
            );

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        $_response = null;
        $_data_json = $param['data_json'];
        $_data_sku = $param['product_sku'];
        $_data_qty = $param['product_qty'];
        $_data = $this->jsonHelper->jsonDecode($_data_json);

        $_product_id = $_data[0]['product_id'];
        $_attributes = $_data[0]['attributes'];

        //$_product = $this->_productRepository->getById($_product_id);
        $_product = $this->_productRepository->get($_data_sku);
        $_attribute_info = $this->_superProductHelper->getAttributesInfo($_attributes);
        $_simple_product = $this->_modelConfigurableType->getProductByAttributes($_attribute_info, $_product);

        if(is_object($_simple_product)) {
            $_product_final_price = '';
            if($this->custsession->isLoggedIn()){
                $_product_final_price = min($_simple_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),$_simple_product->getPriceModel()->getFinalPrice($_data_qty,$_simple_product));
            }
            $_response = array (
                'errors' => false,
                'product_id' => $_product_id,
                'product_final_price' => $_product_final_price,
                'simple_sku' => $_simple_product->getSku(),
                'traffic_light_stock' => $this->_trafficLightStockHelper->getGauge($_simple_product->getId(),$_product->getId()),
                'gauge_id' => $_simple_product->getId(),
                'product_image_default' => $this->getProductThumbnail($_simple_product)
            );

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        if(!is_object($_simple_product)) {
            $this->_logger->debug('Product Not exist with this attributes information: ' . json_encode($_data));

            $_response = array (
                'errors' => true,
                'product_final_price' => 0
            );
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($_response);
        return $resultJson;

    }

    /**
     * @param $_product
     * @return string
     */
    public function getProductThumbnail($_product)
    {
        $imageWidth = 250;
        $imageHeight = 200;

        $_thumbnail = $this->_catalogHelperImage->init($_product, 'product_page_image_small')->setImageFile($_product->getFile())->resize($imageWidth, $imageHeight)->getUrl();

        return $_thumbnail;

    }

}
