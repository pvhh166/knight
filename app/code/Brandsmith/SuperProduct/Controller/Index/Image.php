<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Image extends \Magento\Framework\App\Action\Action
{
    const WIDTH = '1600';
    const HEIGHT = '1200';
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTimes;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $file;

    /**
     * Spimage repository
     *
     * @var \Brandsmith\Manage\Api\SpimageRepositoryInterface
     */
    protected $_spimageRepository;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Spimage factory
     *
     * @var \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory
     */
    protected $_spimageFactory;

    /**
     * Image constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Brandsmith\SuperProduct\Model\PnglayerRepository $PnglayerRepository
     * @param \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageFactory
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_productRepository = $productRepository;
        $this->_directoryList = $directoryList;
        $this->_dateTimes = $dateTime;
        $this->_storeManager = $storeManager;
        $this->file = $file;
        $this->_spimageRepository = $spimageRepository;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_spimageFactory = $spimageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {

        header ("Content-type: image/png");
        $_params = $this->getRequest()->getParams();
        $_root = $this->_directoryList->getRoot(); //root folder path
        $_pub = $_root . '/pub/';

        //  Convert Json To Array
        $_UrlData = $_params['url_json'];
        $_urlArray = $this->jsonHelper->jsonDecode($_UrlData);

        // Sort Position
        $position = array();
        foreach ($_urlArray as $key => $row)
        {
            $position[$key] = $row['position'];
        }
        array_multisort($position, SORT_ASC, $_urlArray);

        //  Set Image Size
        $dest_image = imagecreatetruecolor(self::WIDTH, self::HEIGHT);

        //make sure the transparency information is saved
        imagesavealpha($dest_image, true);

        //create a fully transparent background (127 means fully transparent)
        $trans_background = imagecolorallocatealpha($dest_image, 0, 0, 0, 127);

        //fill the image with a transparent background
        imagefill($dest_image, 0, 0, $trans_background);

        foreach ($_urlArray as $_url) {
            $_pubUrl = explode('media', $_url['src']);
            $_realPath = $_pub . '/media/' . $_pubUrl['1'];
            if($this->file->isExists($_realPath)) {
                $_imageCreate = imagecreatefrompng($_realPath);
                imagecopy($dest_image, $_imageCreate, 0, 0, 0, 0, self::WIDTH, self::HEIGHT);
            }
        }

        $_gmtTimestamp = $this->_dateTimes->gmtTimestamp();
        $_sku = trim($_params['url_sku']);
        $_imageName = strtolower($_gmtTimestamp . '_' . $_sku . '.png');
        $save = $_pub . 'media/generate_image/' . strtolower($_imageName);
        $_imageJpgName = strtolower($_gmtTimestamp . '_' . $_sku . '.jpg');
        $savedjpg = $_pub . 'media/generate_image/' . strtolower($_imageJpgName);
        
        //imagepng($dest_image, $_imageName);

        imagepng($dest_image, $save);

        imagejpeg($dest_image, $savedjpg, 30);

        //destroy all the image resources to free up memory
        imagedestroy($dest_image);

        $directoryName = $_pub . 'media/generate_image/';
        if(!is_dir($directoryName) && !mkdir($directoryName, 0777, true) && !is_dir($directoryName)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $directoryName));
        }

        $mediaPubFile = $_pub . $_imageName;
        if($this->file->isExists($mediaPubFile)) {
            $this->file->deleteFile($mediaPubFile);
        }

        // Remove image at root
        $rootFile = $_root . '/' . $_imageName;
        if(file_exists($rootFile)) {
            chmod($rootFile, 0755);
            unlink($rootFile);
        } else {
            $fh = @fopen($rootFile, 'rb+');
            if ($fh) {
                fclose($fh);
            }
        }

        $_mediaPath = $this->getMediaUrl();
        $_httpFilePath = $_mediaPath . 'generate_image/' . $_imageName;
        $_httpFileJpgPath = $_mediaPath . 'generate_image/' . $_imageJpgName;
        $data = [
            'sp_id' => $_params['sp_id'],
            'sp_sku' => $_params['url_sku'],
            'logged_user' => $_params['customer_info'],
            'image_thumbnail' => 'generate_image/' . $_imageName,
            'download' => $_httpFilePath
        ];

        $this->updateManageSpImages($data);

        $_response = array (
            'errors' => false,
            'file_path' => $_httpFilePath,
            'file_jpg_path' => $_httpFileJpgPath
        );


        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($_response);

        return $resultJson;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param $data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateManageSpImages($data)
    {
        $spimage = $this->_spimageFactory->create();
        $this->_dataObjectHelper->populateWithArray($spimage, $data, \Brandsmith\Manage\Api\Data\SpimageInterface::class);
        $this->_spimageRepository->save($spimage);
    }
}
