<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Quote\Model\QuoteRepository;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Wishlist\Helper\Data as WishlistHelper;

class UpdateWishlist extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_storeManager;

    protected $_quote;

    protected $_product;

    protected $_cartItemFactory;

    protected $_cartRepositoryInterface;

    protected $_cartManagementInterface;

    protected $_cart;

    protected $serializer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTimes;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var RequestQuantityProcessor
     */
    protected $quantityProcessor;

    protected $quoteRepository;


    protected $supercart;

    /**
     * @var \Magento\Wishlist\Model\ItemFactory
     */
    protected $itemFactory;
    /**
     * @var WishlistProviderInterface
     */
    protected $wishlistProvider;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        CustomerCart $cart,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestQuantityProcessor $quantityProcessor = null,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Brandsmith\SuperProduct\Controller\Index\Cart $supercart,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        WishlistProviderInterface $wishlistProvider,
        WishlistHelper $wishlistHelper
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_storeManager = $storeManager;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->_cart = $cart;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->checkoutSession = $checkoutSession;
        $this->quantityProcessor = $quantityProcessor ?: $this->_objectManager->get(RequestQuantityProcessor::class);
        $this->quoteRepository = $quoteRepository;
        $this->supercart = $supercart;
        $this->itemFactory = $itemFactory;
        $this->wishlistProvider = $wishlistProvider;
        $this->wishlistHelper = $wishlistHelper;

    }

    public function execute()
    {
        $_param = $this->getRequest()->getParams();
        $productType = isset($_param['product_type'])? $_param['product_type'] : "";
        $isAddWishlist = isset($_param['add_wishlist'])? $_param['add_wishlist'] : 0;
        $options = [];
        if($isAddWishlist != 1) {
            $item = $this->itemFactory->create()->load($_param['wishlist_id']);
            $wishlist = $this->wishlistProvider->getWishlist($item->getWishlistId());
        }else {
            $wishlist = $this->wishlistProvider->getWishlist();
        }
        if (!$wishlist) {
            $response = [
                'errors' => true,
                'messages' => 'Update error.'
            ];
            return $response;
        }

        if($productType == 'configurable') { // Config Product
            $item->delete();
            $buyRequest = new \Magento\Framework\DataObject($_param);

            $wishlist->addNewItem($_param['product'], $buyRequest);
            $this->wishlistHelper->calculate();
            $wishlist->save();
        }else {
            if($productType == 'simple') { // Simple Product
                $item->delete();
                $buyRequest = new \Magento\Framework\DataObject($_param);

                $wishlist->addNewItem($_param['product'], $buyRequest);
                $this->wishlistHelper->calculate();
                $wishlist->save();
            }else { //Super Product
                if($_param['validate'] == 0 || empty($_param['super_attributes']) ) {
                    $_product_collection = $this->updateArrayInArray($_param['product_collection']);
                    $message = __(  "Can't add product to cart." );
                    $this->messageManager->addErrorMessage($message);
                    $_response = array (
                        'errors' => true,
                        'product_require' => $_product_collection
                    );
                    $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                    $resultJson->setData($_response);
                    return $resultJson;
                }

                $_super_product_id = $_param['super_product_id'];
                $_super_product_qty = $_param['product_qty'];
                $_super_product_attributes = $_param['super_attributes'];
                $_super_product_addon_qty = '';
                if(!empty($_param['addon_qty'])) {
                    $_super_product_addon_qty = $_param['addon_qty'];
                }
                $_super_product = $this->_productRepository->getById($_super_product_id);
                $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);

                $_super_custom_price = $this->supercart->getTotalPrice($_getChildProduct, $_super_product_addon_qty);

                foreach($_param as $key => $data) {
                    if($key == 'super_attributes') {
                        foreach ($_param[$key] as $_i => $_value) {

                            $_productAddOn = $_super_product->getTypeInstance()->getProductAddOn($_super_product_id, $_value['product_id']);
                            $_productSku = null;
                            if( !empty($_productAddOn->getData()) ) {
                                //  Get Add-on Information
                                $_addon_data = $_productAddOn->getData();
                                $_productName = $_addon_data['name'];
                                $_productQuantity = $_addon_data['qty'];
                                $_productSku = $_addon_data['sku'];
                            } else {
                                //  Get Base Information
                                $_baseProduct = $_super_product->getTypeInstance()->getBaseProductById($_super_product_id, $_value['product_id']);
                                $_productName = $_baseProduct->getName();
                                $_productQuantity = $_baseProduct->getQty();
                                $_productSku = $_baseProduct->getSku();
                            }

                            // Update Sku Simple product
                            $_simpleSkuForParam = $this->setSimpleSkuForParam($_value, $_getChildProduct);
                            $_imageForAddOn = $this->setImageForAddOn($_value, $_getChildProduct);
                            $_param[$key][$_i]['product_name'] = $_productName;
                            $_param[$key][$_i]['product_sku'] = $_productSku;
                            $_param[$key][$_i]['product_simple_sku'] = $_simpleSkuForParam;
                            $_param[$key][$_i]['product_thumbnail_img'] = $_imageForAddOn;
                            $_param[$key][$_i]['product_quantity'] = $_productQuantity;
                        }
                    }
                }

                // Add New Param Attribute for MiniCart
                $_param['super_attributes_defined'] = $_param['super_attributes'];

                // Update Attribute Option Selected From Code to Label
                foreach ($_param['super_attributes_defined'] as $key => $value) {
                    if ( $_param['super_attributes_defined'][$key]['product_type'] == 'configurable' ) {
                        $attributes_defined_option_selected = $_param['super_attributes_defined'][$key]['option_selected'];
                        $_convertAttributes = $_super_product->getTypeInstance()->getConvertAttributes($attributes_defined_option_selected);
                        $_param['super_attributes_defined'][$key]['option_selected'] = $_convertAttributes;
                    }
                }

                $options['product'] = $_super_product_id;
                $options['qty'] = $_param['product_qty'];
                $options['product_simple_sku'] = $_super_product->getSku();
                $options['super_options'] = $_param;

                try {
                    if($isAddWishlist != 1) {
                        $productId = $item->getProductId();
                        $item->delete();
                    }else {
                        $productId = $_param['super_product_id'];
                    }
                    // $data = $_param ? $this->serializer->unserialize($_param) : [];
                    $buyRequest = new \Magento\Framework\DataObject($_param);
                    $result = $wishlist->addNewItem($productId, $buyRequest);

                    $this->wishlistHelper->calculate();
                    $wishlist->save();
                    $this->_eventManager->dispatch(
                        'wishlist_add_product',
                        ['wishlist' => $wishlist, 'product' => $_super_product, 'item' => $result]
                    );

                }catch (\Exception $e) {
                    $response = [
                        'errors' => true,
                        'messages' => 'Update error.'
                    ];
                    return $response;
                }
            }
        }

        if($isAddWishlist != 1) {
            $message = __("The Product has been updated.");
        }else {
            $message = __("The Product has been moved to your favourites.");
        }
        $this->messageManager->addSuccessMessage($message);
        $response = [
            'errors' => false,
            'redirectUrl' => $this->_url->getUrl('wishlist'),
            'request' => 3
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }

    public function updateArrayInArray($_array)
    {
        $_newArray = array();
        foreach ($_array as $_key => $_value) {
            $_newArray['product_id_'.$_key] = $_value['product_id'];
        }
        return array_unique($_newArray);
    }

    public function setSimpleSkuForParam($_productInfo, $_getChildProduct)
    {
        $_simpleSku = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_simpleSku = $_item['simple_product_sku'];
            }
        }

        return $_simpleSku;
    }

    public function setImageForAddOn($_productInfo, $_getChildProduct)
    {
        $_thumbnailImg = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_thumbnailImg = $_item['product_thumbnail_img'];
            }
        }
        return $_thumbnailImg;
    }


}
