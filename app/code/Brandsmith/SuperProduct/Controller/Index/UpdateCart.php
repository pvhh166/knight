<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Quote\Model\QuoteRepository;
use Magento\Framework\App\ResourceConnection;

class UpdateCart extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quote;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\Quote\Api\Data\CartItemInterfaceFactory
     */
    protected $_cartItemFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_cartRepositoryInterface;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $_cartManagementInterface;

    /**
     * @var CustomerCart
     */
    protected $_cart;

    /**
     * @var mixed
     */
    protected $serializer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTimes;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var RequestQuantityProcessor
     */
    protected $quantityProcessor;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * UpdateCart constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Brandsmith\SuperProduct\Model\PnglayerRepository $PnglayerRepository
     * @param \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Catalog\Model\ProductRepository $ProductRepository
     * @param \Magento\Quote\Model\QuoteFactory $quote
     * @param \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param CustomerCart $cart
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param RequestQuantityProcessor|null $quantityProcessor
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        CustomerCart $cart,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestQuantityProcessor $quantityProcessor = null,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_storeManager = $storeManager;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->_cart = $cart;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->checkoutSession = $checkoutSession;
        $this->quantityProcessor = $quantityProcessor ?: $this->_objectManager->get(RequestQuantityProcessor::class);
        $this->quoteRepository = $quoteRepository;
        $this->resourceConnection = $resourceConnection;
    }

    public function execute()
    {
        $_param = $this->getRequest()->getParams();

        if(trim($_param['validate']) === trim('view-cart') ) {
            try {
                $totalPrice = 0;

                foreach ($_param['data'] as $key => $item) {
                    $product = $this->_productRepository->get($item['product_sku']);
                    $productPriceWithoutQty = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
                    $productPrice = ($productPriceWithoutQty*$item['product_qty']);
                    $totalPrice += $productPrice;
                    $this->getUpdateQuoteItemOption($_param['quote_item_id'], $item['product_sku'], $item['product_id'], $item['product_qty']);
                }

                // Recalculated Quote/Cart Price
                $quote = $this->_cart->getQuote();
                $item = $quote->getItemById($_param['quote_item_id']);
                $item->setQty(1);
                $item->setCustomPrice($totalPrice);
                $item->setOriginalCustomPrice($totalPrice);
                $item->calcRowTotal();
                $item->cleanModelCache();
                $item->save();

                $quote->setTriggerRecollect(1); //Magic line.
                $quote->collectTotals();
                $quote->cleanModelCache();
                $quote->preventSaving();
                $quote->save();

                $this->messageManager->addSuccessMessage(__('Update ' . $item->getName() . ' Successfully'));
                $response = [
                    'errors' => false,
                    'messages' => 'Update Successfully'
                ];

            } catch (\Exception $e) {
                $this->_logger->info('Update View Cart: ' . $e->getMessage());
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                $response = [
                    'errors' => true,
                    'messages' => $e->getMessage()
                ];

            }

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($response);

            return $resultJson;
        }


        if($_param['validate'] == 0 || empty($_param['super_attributes']) ) {
            $_product_collection = $this->updateArrayInArray($_param['product_collection']);
            $message = __(  "Can't add product to cart." );
            $this->messageManager->addErrorMessage($message);
            $_response = array (
                'errors' => true,
                'product_require' => $_product_collection
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        $allItems = $this->_cart->getQuote()->getAllItems();
        $_requestItemId = $_param['item_id'];
        foreach ($allItems as $item) {
            $itemParentItemId = $item->getParentItemId();
            $_itemId = $item->getItemId();
            if( $_itemId == $_requestItemId || $itemParentItemId == $_requestItemId ) {
                $this->_cart->removeItem($_itemId);
            }
        }
        $this->_cart->save();

        $response = [
            'errors' => false,
            'redirectUrl' => $this->_url->getUrl('checkout/cart'),
            'request' => 1
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }

    public function updateArrayInArray($_array)
    {
        $_newArray = array();
        foreach ($_array as $_key => $_value) {
            $_newArray['product_id_'.$_key] = $_value['product_id'];
        }
        return array_unique($_newArray);
    }

    /**
     * @param $quote_item_id
     * @param $productSku
     * @param $productId
     * @param $productQty
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getUpdateQuoteItemOption($quote_item_id, $productSku, $productId, $productQty)
    {
        $connection = $this->resourceConnection->getConnection();

        // Update Table Quote Item
        $tableQuoteItem = $this->resourceConnection->getTableName('quote_item');
        $connection->update(
            $tableQuoteItem,
            ['sku' => $productQty],
            ['parent_item_id = ?' => $quote_item_id, 'sku = ?' => $productSku]
        );

        // Update Table Quote Item Option
        $tableQuoteItemOption = $this->resourceConnection->getTableName('quote_item_option');
        $sqlItemOptions = "Select * FROM " . $tableQuoteItemOption . " WHERE `item_id` = ". $quote_item_id;
        $quoteItemOptionsArr = $connection->fetchAll($sqlItemOptions);

        $addOptions = [];
        $quoteItemOptionId = [];

        foreach ($quoteItemOptionsArr as $item) {
            if($item['code'] == 'super_custom_option') {
                $addOptions = json_decode($item['value'], true);
                $quoteItemOptionId = $item['option_id'];
            }
        }

        foreach ($addOptions['super_attributes_defined'] as $key => $_option) {
            if(trim($_option['product_id']) === trim($productId)) {
                $addOptions['super_attributes_defined'][$key]['product_quantity'] = $productQty;
                $addOptions['addon_qty'][$productId] = $productQty;
            }
        }

        foreach ($addOptions['super_attributes'] as $key => $_option) {
            if(trim($_option['product_id']) === trim($productId)) {
                $addOptions['super_attributes'][$key]['product_quantity'] = $productQty;
                $addOptions['addon_qty'][$productId] = $productQty;
            }
        }

        //$addOptions['super_attributes'] = $addOptions['super_attributes_defined'];
        $addOptions = $this->serializer->serialize($addOptions);

        $connection->update(
            $tableQuoteItemOption,
            [
                'value' => $addOptions,
            ],
            ["option_id = ?" => $quoteItemOptionId]
        );

    }


}
