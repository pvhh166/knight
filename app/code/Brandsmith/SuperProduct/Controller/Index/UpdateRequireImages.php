<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class UpdateRequireImages extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_directoryList;

    protected $_dateTimes;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngModelRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\SuperProduct\Model\PngRepository $pngModelRepository
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_productRepository = $productRepository;
        $this->_directoryList = $directoryList;
        $this->_dateTimes = $dateTime;
        $this->_storeManager = $storeManager;
        $this->_pngModelRepository = $pngModelRepository;
    }

    public function execute()
    {
        $param = $this->getRequest()->getParams();
        $super_product_id = $param['super_product_id'];
        $base_attribute_code_id = $param['base_attribute_code_id'];

        $super_product = $this->_productRepository->getById($super_product_id);

        $_list_images_add_on_require = $super_product->getTypeInstance()->getListImagesAddOnRequire($super_product_id, $base_attribute_code_id);

        $response = [
            'errors' => false,
            'list_images' => $_list_images_add_on_require
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;

    }

}
