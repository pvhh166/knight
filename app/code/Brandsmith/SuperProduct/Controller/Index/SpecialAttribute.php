<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Quote\Model\QuoteRepository;

class SpecialAttribute extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_storeManager;

    protected $_quote;

    protected $_product;

    protected $_cartItemFactory;

    protected $_cartRepositoryInterface;

    protected $_cartManagementInterface;

    protected $_cart;

    protected $serializer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTimes;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var RequestQuantityProcessor
     */
    protected $quantityProcessor;

    protected $quoteRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        CustomerCart $cart,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestQuantityProcessor $quantityProcessor = null,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_storeManager = $storeManager;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->_cart = $cart;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->checkoutSession = $checkoutSession;
        $this->quantityProcessor = $quantityProcessor ?: $this->_objectManager->get(RequestQuantityProcessor::class);
        $this->quoteRepository = $quoteRepository;
    }

    public function execute()
    {
        $_param = $this->getRequest()->getParams();

        $_optionId = $_param['option_id'];
        $_attributeCode = $_param['attribute_code'];
        $_productId = $_param['product_id'];
        $_productConfigurable = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($_productId);
        $_children = $_productConfigurable->getTypeInstance()->getUsedProducts($_productConfigurable);

        $_arrayFabricColour = array();
        foreach ($_children as $child){
            if (null !== $child->getCustomAttribute($_attributeCode)) {
                $_attributeValue = $child->getCustomAttribute($_attributeCode)->getValue();
                if($_optionId == $_attributeValue) {
                    if (null !== $child->getCustomAttribute('fabric_colour')) {
                        $_attributeValueFabricColour = $child->getCustomAttribute('fabric_colour')->getValue();
                        $_arrayFabricColour[] = $_attributeValueFabricColour;
                    }
                }
            }
        }

        $_unique = array_unique($_arrayFabricColour);
        $response['errors'] = true;
        if(!empty($_arrayFabricColour)) {
            $response['errors'] = false;
            $response['data'] = array_values($_unique);
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }
}
