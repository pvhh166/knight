<?php
/**
 *
 */
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Quote\Model\QuoteRepository;

/**
 * Class Remove
 * @package Brandsmith\SuperProduct\Controller\Index
 */
class Removeitems extends \Magento\Framework\App\Action\Action
{
    /**
    * @var \Magento\Checkout\Model\Session
    */
    protected $checkoutSession;

    /**
    * @var CustomerCart
    */
    protected $cart;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $serializer;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * Remove constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param \Magento\Catalog\Model\ProductRepository $_productRepository
     * @param QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        CustomerCart $cart,
        ResourceConnection $resourceConnection,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        \Magento\Catalog\Model\ProductRepository $_productRepository,
        QuoteRepository $quoteRepository
    ) {
        parent::__construct($context);

        $this->checkoutSession = $checkoutSession;
        $this->cart = $cart;
        $this->resourceConnection = $resourceConnection;
        $this->serializer = $serializer;
        $this->_productRepository = $_productRepository;
        $this->quoteRepository = $quoteRepository;
    }

    public function execute()
    {

        $getParams = $this->getRequest()->getParams();
        $allItems = $this->checkoutSession->getQuote()->getAllItems();

        $product_ids = explode(",", $getParams['product_ids']);
        $quote_items_id = explode(",", $getParams['quote_items_id']);
        $totalPrice = 0;

        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            if(in_array($itemId, $quote_items_id)){
                $this->cart->removeItem($itemId)->save();
            }
        }

        $response = [
            'errors' => false,
            'redirectUrl' => $this->_url->getUrl('checkout/cart'),
            'request' => 1
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }


    public function getUpdateQuote($quoteItemParentItemId, $productSimpleSku, $quoteItemId)
    {
        $connection = $this->resourceConnection->getConnection();

        /**
         * Remove Item at table Quote Item
         */

        $tableQuoteItem = $connection->getTableName('quote_item');

        $connection->delete(
            $tableQuoteItem,
            ['item_id = ?' => $quoteItemId]
        );
    }

    public function getUpdateQuoteItemOption($quoteItemParentItemId, $productSimpleSku, $quoteItemId)
    {
        $connection = $this->resourceConnection->getConnection();

        /**
         * Update code super_custom_option at table Quote Item Option
         */

        $tableQuoteItemOption = $connection->getTableName('quote_item_option');

        $sqlItemOptions = "Select * FROM " . $tableQuoteItemOption . " WHERE `item_id` = ". $quoteItemParentItemId;
        $quoteItemOptionsArr = $connection->fetchAll($sqlItemOptions);

        $addOptions = [];
        $quoteItemOptionId = [];

        foreach ($quoteItemOptionsArr as $item) {
            if($item['code'] == 'super_custom_option') {
                $addOptions = json_decode($item['value'], true);
                $quoteItemOptionId = trim($item['option_id']);
            }
        }

        foreach ($addOptions['super_attributes_defined'] as $key => $_option) {
            if(trim($_option['product_simple_sku']) == trim($productSimpleSku)) {
                $productId = $_option['product_id'];
                unset($addOptions['super_attributes_defined'][$key], $addOptions['addon_qty'][$productId]);
            }
        }

        foreach ($addOptions['super_attributes'] as $key => $_option) {
            if(trim($_option['product_simple_sku']) == trim($productSimpleSku)) {
                unset($addOptions['super_attributes'][$key]);
            }
        }
        
        return $this->serializer->serialize($addOptions);
    }
}