<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class PngDefault extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_directoryList;

    protected $_dateTimes;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngModelRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngruleRepository
     */
    protected $_pngRuleRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\SuperProduct\Model\PngRepository $pngModelRepository,
        \Brandsmith\SuperProduct\Model\PngruleRepository $pngRuleRepository
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_productRepository = $productRepository;
        $this->_directoryList = $directoryList;
        $this->_dateTimes = $dateTime;
        $this->_storeManager = $storeManager;
        $this->_pngModelRepository = $pngModelRepository;
        $this->_pngRuleRepository = $pngRuleRepository;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();

        $_superProductId = $params['super_product_id'];
        //$_baseAttribute = $params['base_attribute'];
        $_basAttributeOptionId = $params['base_attribute_option_id'];
        $_productId = $params['product_id'];
        $_productSku = $params['product_sku'];
        $_productType = $params['product_type'];

        $_arrayPngImageDefault = [];
        if( $_productType == 'simple' ) {
            $_pngDefaultOption = $this->getPngDefaultOption($_superProductId, $_productSku, $_productSku);
            if(!empty($_pngDefaultOption)) {
                $_data_pngDefaultOptions = $_pngDefaultOption->getData();
                if(count($_data_pngDefaultOptions) > 0 && is_array($_data_pngDefaultOptions)) {
                    foreach ($_data_pngDefaultOptions as $_data_pngDefaultOption) {
                        $_pngImageDefault = $this->getPngImageDefault($_superProductId, $_productId, $_data_pngDefaultOption['sku'], $_data_pngDefaultOption['base_attribute'], $_data_pngDefaultOption['sku'], $_basAttributeOptionId,  $_data_pngDefaultOption['multi_layer']);
                        $_arrayPngImageDefault[] = array (
                            'image_url' => $_pngImageDefault,
                            'position' => $_data_pngDefaultOption['position'],
                            'attribute_code' => $_data_pngDefaultOption['attribute'],
                            'product_sku' => $_data_pngDefaultOption['sku']
                        );
                    }
                }
            }

        } else {
            // Product configurable
            $_productAttributes = $params['product_attributes']; // array
            foreach($_productAttributes as $key => $value) {

                $_attributeCode = $value;
                $_pngDefaultOption = $this->getPngDefaultOption($_superProductId, $_productSku, $_attributeCode);

                if(!empty($_pngDefaultOption)) {
                    $_data_pngDefaultOptions = $_pngDefaultOption->getData();
                    if(count($_data_pngDefaultOptions) > 0 && is_array($_data_pngDefaultOptions)) {
                        foreach ($_data_pngDefaultOptions as $_data_pngDefaultOption) {
                            $_PngDependencyRule = $this->getPngDependencyRule( $_superProductId, $_productSku, $_data_pngDefaultOption['attribute'] );

                            if($_PngDependencyRule != false) {
                                $_PngDependencyRuleAttribute = $_PngDependencyRule->getAttribute();
                                $_PngDependencyRuleDefaultOption = $_PngDependencyRule->getDefaultOption();
                                $_pngImageDefault = $this->getPngImageDefaultWithRule($_superProductId, $_productId, $_data_pngDefaultOption['attribute'], $_data_pngDefaultOption['base_attribute'], $_PngDependencyRuleAttribute, $_data_pngDefaultOption['default_option'], $_basAttributeOptionId, $_PngDependencyRuleDefaultOption, $_data_pngDefaultOption['multi_layer']);
                            } else {
                                $_pngImageDefault = $this->getPngImageDefault($_superProductId, $_productId, $_data_pngDefaultOption['attribute'], $_data_pngDefaultOption['base_attribute'], $_data_pngDefaultOption['default_option'], $_basAttributeOptionId, $_data_pngDefaultOption['multi_layer']);
                            }

                            $_arrayPngImageDefault[] = array (
                                'image_url' => $_pngImageDefault,
                                'position' => $_data_pngDefaultOption['position'],
                                'attribute_code' => $_data_pngDefaultOption['attribute'],
                                'product_sku' => $_data_pngDefaultOption['sku']
                            );
                        }
                    }
                }
            }
        }

        $response = [
            'errors' => false,
            'product_id' => $_productId,
            'product_type' => $_productType,
            'image_default_url' => $_arrayPngImageDefault
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }

    public function getPngDependencyRule($superProductId, $sku, $attributeCodeDependency)
    {
        $_pngRule = $this->_pngRuleRepository->getByAttributeDependency($superProductId, $sku, $attributeCodeDependency);

        if( is_object($_pngRule) && !empty($_pngRule->getData()) ) {
            $_ruleAttribute = $_pngRule->getAttribute();
            $_defaultOptionData = $this->_pngModelRepository->getDefaultOptionFirstItem($superProductId, $sku, $_ruleAttribute);
            return $_defaultOptionData;

        } else {
            $this->_logger->debug( 'Empty getPngDependencyRule: ' );
        }

        return false;
    }

    public function getPngDefaultOption($superProductId, $sku, $attributeCode)
    {
        $_defaultOptionData = $this->_pngModelRepository->getDefaultOption($superProductId, $sku, $attributeCode);

        if( is_object($_defaultOptionData) && !empty($_defaultOptionData->getData()) ) {
            return $_defaultOptionData;
        }
        return null;
    }

    public function getPngImageDefault($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area)
    {
        $_image = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area);

        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }
        return null;
    }

    public function getPngImageDefaultWithRule($superProductId, $productId, $attribute, $_baseAttribute, $ruleAttribute, $option, $baseOption, $_PngDependencyRule, $area)
    {
        $_image = $this->_pnglayerRepository->getBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $ruleAttribute, $option, $baseOption, $_PngDependencyRule, $area);

        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }
        return null;
    }

}
