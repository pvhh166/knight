<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class RemovePngLayerDependency extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_directoryList;

    protected $_dateTimes;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngModelRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngruleRepository
     */
    protected $_pngRuleRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\SuperProduct\Model\PngRepository $pngModelRepository,
        \Brandsmith\SuperProduct\Model\PngruleRepository $PngruleRepository
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_productRepository = $productRepository;
        $this->_directoryList = $directoryList;
        $this->_dateTimes = $dateTime;
        $this->_storeManager = $storeManager;
        $this->_pngModelRepository = $pngModelRepository;
        $this->_pngRuleRepository = $PngruleRepository;
    }

    public function execute()
    {
        $param = $this->getRequest()->getParams();

        $super_product_id = $param['super_product_id'];
        $sku = $param['sku'];
        $attribute = $param['attribute'];

        $super_product = $this->_productRepository->getById($super_product_id);
        $add_on_product = $this->_productRepository->get($sku);

        $png_default_option = $super_product->getTypeInstance()->getPngRule($super_product_id, $sku, $attribute);
        $png_default_option_data = $png_default_option->getData();

        $response['errors'] = true;

        if( !empty($png_default_option_data) ) {
            $_pngRule = array();

            foreach ($png_default_option_data as $png_rule_item) {
                $_pngRule[] = $png_rule_item['attribute_dep'];
            }

            $response['errors'] = false;
            $response['sku'] = $sku;
            $response['add_on_id'] = $add_on_product->getId();
            $response['png_rule'] = $_pngRule;
        } else {
            $response['message'] = 'We can\'t find rule matching the selection.';
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;

    }
}
