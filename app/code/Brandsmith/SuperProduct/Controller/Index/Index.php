<?php
namespace Brandsmith\SuperProduct\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $_logger;

    /**
     * @var \Brandsmith\SuperProduct\Model\PnglayerRepository
     */
    protected $_pnglayerRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\Pnglayer
     */
    protected $_pnglayer;

    /**
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngRepository;

    /**
     * @var \Brandsmith\SuperProduct\Model\PngRepository
     */
    protected $_pngModelRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Brandsmith\SuperProduct\Model\PnglayerRepository  $PnglayerRepository,
        \Brandsmith\SuperProduct\Model\Pnglayer $SuperProductPnglayer,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Brandsmith\SuperProduct\Model\PngRepository $pngRepository,
        \Brandsmith\SuperProduct\Model\PngRepository $pngModelRepository
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_pnglayerRepository = $PnglayerRepository;
        $this->_pnglayer = $SuperProductPnglayer;
        $this->_uploaderPool = $uploaderPool;
        $this->_productRepository = $productRepository;
        $this->_pngRepository = $pngRepository;
        $this->_pngModelRepository = $pngModelRepository;
    }

    public function execute()
    {
        $_params = $this->getRequest()->getParams();

        $_optionCode = $_params['option_code'];
        $_optionSku = $_params['option_sku'];
        $_optionId = $_params['option_id'];
        $_optionPosition = $_params['option_position'];
        $_baseProductId = $_params['base_product'];
        $baseAttribute = $_params['base_attribute'];
        $baseOption = $_params['base_option'];
        $_attributeRule = $_params['attribute_rule'];
        $_attributeRuleIndex = $_params['attribute_rule_index'];

        $_attributeDependency = $this->getAttributeDependency($_baseProductId, $_optionCode, $_optionSku, $_optionId);
        $_attributeNotHaveDepWith = $this->getAttributeNotHaveDepWith($_baseProductId, $_optionCode, $_optionSku, $_optionId);

        $_product = $this->getProductBySku($_optionSku);
        $productId = $_product->getId();
        if( !empty($_attributeRule)) {
            //$optionImage = $this->getPngLayerImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, $_attributeRule, $_attributeRuleIndex, $_optionSku);
            $optionImagePositionData = $this->_pngModelRepository->getDefaultOption($_baseProductId, $_optionSku, $_optionCode);
            $optionImagePosition = $optionImagePositionData->getPosition();
            $optionImage = $this->getPngLayerAllImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, $_attributeRule, $_attributeRuleIndex, $_optionSku);
            if($optionImagePositionData->getMultiLayer() == 1) {
                $optionImageBack = $this->getPngLayerAllImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, $_attributeRule, $_attributeRuleIndex, $_optionSku, 1);
            }
        } else {
            //$optionImage = $this->getOptionImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption);
            $optionImagePositionData = $this->_pngModelRepository->getDefaultOption($_baseProductId, $_optionSku, $_optionCode);
            $optionImagePosition = $optionImagePositionData->getPosition();
            $optionImage = $this->getOptionAllImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption);
            if($optionImagePositionData->getMultiLayer() == 1) {
                $optionImageBack = $this->getOptionAllImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, 1);
            }
        }

        if(count($_attributeDependency->getData()) > 0) {
            $_dependency = $_attributeDependency->getData();
            $_notDependency = $_attributeNotHaveDepWith->getData();

            $_response = array();

            foreach($_dependency as $_optionItem) {
                //$superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption
                $_productDependency = $this->_productRepository->get($_optionItem['sku_dep']);
                $_response[] = array(
                    'errors' => false,
                    'image' => false,
                    'rule_id' => $_optionItem['rule_id'],
                    'sku_dep' => $_optionItem['sku_dep'],
                    'id_dep' => $_productDependency->getId(),
                    'attribute_dep' => $_optionItem['attribute_dep'],
                    'option_dep' => $_optionItem['option_dep'],
                    'condition_dep' => $_optionItem['condition_dep'],
                    'option_position' => $_optionPosition
                );
            }

            foreach($_response as $_sub_response) {
                $_notDependency = $this->checkArrayHaveSameDependencyRule($_notDependency, $_sub_response['attribute_dep']);
            }

            if(count($_notDependency) > 0 ) {
                $_checkDependencyRuleRelated = $this->checkDependencyRuleRelated($_notDependency, $_optionId, $_optionSku, $_optionCode, $_optionPosition, $_baseProductId);
                $_response = array_merge($_checkDependencyRuleRelated, $_response);
            }

            //  Response PNG Layer
            $_response[] = array(
                'errors' => false,
                'image' => true,
                'not_dependency' => 1,
                'option_image' => $optionImage['image'],
                'option_image_back' => isset($optionImageBack) ? $optionImageBack['image'] : null,
                'option_image_position' => $optionImagePosition,
                'option_code' => $_optionCode,
                'option_sku' => $_optionSku,
                'product_id' => $productId
            );

            $_data = $this->jsonHelper->jsonEncode($_response);
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_data);

            return $resultJson;
        } else {

            // Attribute Have Dependency but Not this option - Remove old active Attribute Option (Not related with this option)
            $_attributeNotHaveDependency = $this->getAttributeNotHaveDependency($_baseProductId, $_optionCode, $_optionSku);

            //  Response PNG Layer
            $_responseImage[] = array(
                'errors' => true,
                'image' => true,
                'option_image' => $optionImage['image'],
                'option_image_back' => isset($optionImageBack) ? $optionImageBack['image'] : null,
                'option_image_position' => $optionImagePosition,
                'option_code' => $_optionCode,
                'option_sku' => $_optionSku,
                'product_id' => $productId
            );

            if(count($_attributeNotHaveDependency->getData()) > 0) {
                $_dependency = $_attributeNotHaveDependency->getData();
                $_response  = $this->checkDependencyRuleRelated($_dependency, $_optionId, $_optionSku, $_optionCode, $_optionPosition, $_baseProductId);
                $_response = array_merge($_response, $_responseImage);

                $_data = $this->jsonHelper->jsonEncode($_response);
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($_data);

                return $resultJson;

            } else {

                //  Empty Dependency
                $_response[] =  array (
                    'errors' => true,
                    'image' => false,
                    'not_dependency' => 2,
                    'option_id' => $_optionId,
                    'option_sku' => $_optionSku,
                    'option_code' => $_optionCode,
                    'option_position' => $_optionPosition,
                    'message' => "Empty Dependency - " . $_baseProductId
                );

                $_response = array_merge($_response, $_responseImage);

                $_data = $this->jsonHelper->jsonEncode($_response);
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($_data);

                return $resultJson;
            }
        }
    }


    public function getAttributeDependency($_baseProductId, $_optionCode, $_optionSku, $_optionId)
    {
        $_baseProductId = trim($_baseProductId);
        $_optionCode = trim($_optionCode);
        $_optionSku = trim($_optionSku);
        $_optionId = trim($_optionId);

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_baseProductId);
        $collection->addFieldToFilter('sku', $_optionSku);
        $collection->addFieldToFilter('attribute', $_optionCode);
        $collection->addFieldToFilter('option', $_optionId);
        $collection->setOrder('position','ASC');
        $collection->load();

        return $collection;
    }

    public function getAttributeNotHaveDependency($_baseProductId, $_optionCode, $_optionSku)
    {
        $_baseProductId = trim($_baseProductId);
        $_optionCode = trim($_optionCode);
        $_optionSku = trim($_optionSku);

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_baseProductId);
        $collection->addFieldToFilter('sku', $_optionSku);
        $collection->addFieldToFilter('attribute', $_optionCode);
        $collection->setOrder('position','ASC');
        $collection->load();

        return $collection;
    }

    public function getAttributeNotHaveDepWith($_baseProductId, $_optionCode, $_optionSku, $_optionId)
    {
        $_baseProductId = trim($_baseProductId);
        $_optionCode = trim($_optionCode);
        $_optionSku = trim($_optionSku);
        $_optionId = trim($_optionId);

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_baseProductId);
        $collection->addFieldToFilter('sku', $_optionSku);
        $collection->addFieldToFilter('attribute', $_optionCode);
        $collection->addFieldToFilter('option', array('neq' => $_optionId));
        $collection->setOrder('position','ASC');
        $collection->load();

        return $collection;
    }

    public function getAttributeNotHaveDepRelate($_baseProductId, $_optionCode, $_optionSku, $_optionId)
    {
        $_baseProductId = trim($_baseProductId);
        $_optionCode = trim($_optionCode);
        $_optionSku = trim($_optionSku);
        $_optionId = trim($_optionId);

        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_baseProductId);
        $collection->addFieldToFilter('sku', $_optionSku);
        $collection->addFieldToFilter('attribute', $_optionCode);
        $collection->addFieldToFilter('option', $_optionId);
        $collection->setOrder('position','ASC');
        $collection->load();

        return $collection;
    }

    public function checkArrayHaveSameDependencyRule($_notDependency, $_attribute_dep)
    {
        $i = 0;

        foreach($_notDependency as $val) {
            if($val['attribute_dep'] == $_attribute_dep) {

                unset($_notDependency[$i]);
            }
            $i++;
        }
        $_notDependency = array_values($_notDependency);

        return $_notDependency;
    }

    public function getOptionImage($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption)
    {
        $_image = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption);
        if($_image->getData('image')) {
            $uploader = $this->_uploaderPool->getUploader('image');
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
            return $url;
        }

        return null;
    }

    public function getOptionAllImage($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area = 0)
    {
        $url = null;
        $_image = $this->_pnglayerRepository->getOnePngBySuperProductId($superProductId, $productId, $attribute, $_baseAttribute, $option, $baseOption, $area);
        $uploader = $this->_uploaderPool->getUploader('image');
        if($_image->getData('image')) {
            $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
        }
        return array(
            'image' => $url
        );
    }

    public function getPngLayerImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, $_attributeRule, $_attributeRuleIndex, $_optionSku)
    {
        $url = null;
        if($_attributeRule) {
            if( !empty($_attributeRuleIndex) ) {
                $_image = $this->_pnglayerRepository->getBySuperProductId($_baseProductId, $productId, $_optionCode, $baseAttribute, $_attributeRule, $_optionId, $baseOption, $_attributeRuleIndex);

                if($_image->getData('image')) {
                    $uploader = $this->_uploaderPool->getUploader('image');
                    $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
                }
            } else {
                $_attributeDataDefault = $this->_pngRepository->getBySuperProduct($_baseProductId, $_optionSku, $_attributeRule, $baseAttribute);
                $_defaultOption = $_attributeDataDefault->getDefaultOption();
                $_image = $this->_pnglayerRepository->getBySuperProductId($_baseProductId, $productId, $_optionCode, $baseAttribute, $_attributeRule, $_optionId, $baseOption, $_defaultOption);

                if($_image->getData('image')) {
                    $uploader = $this->_uploaderPool->getUploader('image');
                    $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
                }
            }
        }

        return $url;
    }

    public function getPngLayerAllImage($_baseProductId, $productId, $_optionCode, $baseAttribute, $_optionId, $baseOption, $_attributeRule, $_attributeRuleIndex, $_optionSku, $area = 0)
    {
        $url = null;
        if($_attributeRule) {
            if( !empty($_attributeRuleIndex) ) {
                $_image = $this->_pnglayerRepository->getBySuperProductId($_baseProductId, $productId, $_optionCode, $baseAttribute, $_attributeRule, $_optionId, $baseOption, $_attributeRuleIndex, $area);
                $uploader = $this->_uploaderPool->getUploader('image');
                if($_image->getData('image')) {
                    $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
                }
            } else {
                $_attributeDataDefault = $this->_pngRepository->getBySuperProduct($_baseProductId, $_optionSku, $_attributeRule, $baseAttribute);
                $_defaultOption = $_attributeDataDefault->getDefaultOption();
                $_image = $this->_pnglayerRepository->getBySuperProductId($_baseProductId, $productId, $_optionCode, $baseAttribute, $_attributeRule, $_optionId, $baseOption, $_defaultOption, $area);
                $uploader = $this->_uploaderPool->getUploader('image');
                if($_image->getData('image')) {
                    $url = $uploader->getBaseUrl().$uploader->getBasePath().$_image->getData('image');
                }
            }
        }
        return array(
            'image' => $url
        );
    }


    public function getProductBySku($sku)
    {
        try {
            $_product = $this->_productRepository->get($sku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $_product = false;
        }
        return $_product;
    }


    public function checkDependencyRuleRelated($_dependency, $_optionId, $_optionSku, $_optionCode, $_optionPosition, $_baseProductId)
    {
        $_response = array();
        $bool = false;
        $i = 0;

        foreach ($_dependency as $_optionItem) {
            do {
                $_response[] =  array(
                    'errors' => true,
                    'image' => false,
                    'not_dependency' => 1,
                    'rule_id' => $_optionItem['rule_id'],
                    'option_id' => $_optionId,
                    'option_sku' => $_optionSku,
                    'option_code' => $_optionCode,
                    'option_position' => $_optionPosition,
                    'option_code_dependency' => $_optionItem['attribute_dep'],
                    'option_sku_dependency' => $_optionItem['sku_dep']
                );

                $_attributeNotHaveDepWith = $this->getAttributeNotHaveDepRelate($_baseProductId, $_optionItem['attribute_dep'], $_optionItem['sku_dep'], $_optionItem['option_dep']);
                $_dependency = $_attributeNotHaveDepWith->getData();

                if($_dependency && !$bool) {
                    // Rerun the 'my' index again
                    foreach ($_dependency as $_optionItem) {
                        $_response[] =  array(
                            'errors' => true,
                            'image' => false,
                            'not_dependency' => 1,
                            'rule_id' => $_optionItem['rule_id'],
                            'option_id' => $_optionId,
                            'option_sku' => $_optionSku,
                            'option_code' => $_optionCode,
                            'option_position' => $_optionPosition,
                            'option_code_dependency' => $_optionItem['attribute_dep'],
                            'option_sku_dependency' => $_optionItem['sku_dep']
                        );
                    }
                    $bool = true;
                } else {
                    $bool = false;
                }
            } while ($bool);
            $i++;
        }

        return $_response;
    }
}
