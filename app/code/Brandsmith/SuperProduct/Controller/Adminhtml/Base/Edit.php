<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Base;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Base
{
    /**
     * Initialize current Base and set it in the registry.
     *
     * @return int
     */
    protected function _initBase()
    {
        $baseId = $this->getRequest()->getParam('base_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_BASE_ID, $baseId);

        return $baseId;
    }

    /**
     * Edit or create Base
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $baseId = $this->_initBase();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_base');
        $resultPage->getConfig()->getTitle()->prepend(__('Bases'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('Bases'), __('Bases'), $this->getUrl('brandsmith_superproduct/base'));

        if ($baseId === null) {
            $resultPage->addBreadcrumb(__('New Base'), __('New Base'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Base'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Base'), __('Edit Base'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_baseRepository->getById($baseId)->getName()
            );
        }
        return $resultPage;
    }
}
