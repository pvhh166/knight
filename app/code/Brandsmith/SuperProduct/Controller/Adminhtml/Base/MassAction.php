<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Base;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Base repository
     * 
     * @var \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
     */
    protected $_baseRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * Base collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $_successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $_errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Brandsmith\SuperProduct\Model\ResourceModel\Base\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->_baseRepository    = $baseRepository;
        $this->_filter            = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_successMessage    = $successMessage;
        $this->_errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Brandsmith\SuperProduct\Api\Data\BaseInterface $base
     * @return mixed
     */
    abstract protected function _massAction(\Brandsmith\SuperProduct\Api\Data\BaseInterface $base);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $base) {
                $this->_massAction($base);
            }
            $this->messageManager->addSuccessMessage(__($this->_successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->_errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('brandsmith_superproduct/*/index');
        return $redirectResult;
    }
}
