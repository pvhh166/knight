<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Pnglayer;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * PNG Layer repository
     * 
     * @var \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
     */
    protected $_pnglayerRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * PNG Layer collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $_successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $_errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Brandsmith\SuperProduct\Model\ResourceModel\Pnglayer\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->_pnglayerRepository = $pnglayerRepository;
        $this->_filter             = $filter;
        $this->_collectionFactory  = $collectionFactory;
        $this->_successMessage     = $successMessage;
        $this->_errorMessage       = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer
     * @return mixed
     */
    abstract protected function _massAction(\Brandsmith\SuperProduct\Api\Data\PnglayerInterface $pnglayer);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $pnglayer) {
                $this->_massAction($pnglayer);
            }
            $this->messageManager->addSuccessMessage(__($this->_successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->_errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('brandsmith_superproduct/*/index');
        return $redirectResult;
    }
}
