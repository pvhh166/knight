<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Pnglayer;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Pnglayer
{
    /**
     * Initialize current PNG Layer and set it in the registry.
     *
     * @return int
     */
    protected function _initPnglayer()
    {
        $pnglayerId = $this->getRequest()->getParam('pnglayer_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_PNGLAYER_ID, $pnglayerId);

        return $pnglayerId;
    }

    /**
     * Edit or create PNG Layer
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $pnglayerId = $this->_initPnglayer();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_pnglayer');
        $resultPage->getConfig()->getTitle()->prepend(__('PNG&#x20;Layers'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('PNG&#x20;Layers'), __('PNG&#x20;Layers'), $this->getUrl('brandsmith_superproduct/pnglayer'));

        if ($pnglayerId === null) {
            $resultPage->addBreadcrumb(__('New PNG&#x20;Layer'), __('New PNG&#x20;Layer'));
            $resultPage->getConfig()->getTitle()->prepend(__('New PNG&#x20;Layer'));
        } else {
            $resultPage->addBreadcrumb(__('Edit PNG&#x20;Layer'), __('Edit PNG&#x20;Layer'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_pnglayerRepository->getById($pnglayerId)->getAttribute()
            );
        }
        return $resultPage;
    }
}
