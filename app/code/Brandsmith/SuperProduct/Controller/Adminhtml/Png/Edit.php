<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Png;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Png
{
    /**
     * Edit or create PNG
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $pngId = $this->_initPng();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_png');
        $resultPage->getConfig()->getTitle()->prepend(__('PNG'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('PNG'), __('PNG'), $this->getUrl('brandsmith_superproduct/png'));

        if ($pngId === null) {
            $resultPage->addBreadcrumb(__('New PNG'), __('New PNG'));
            $resultPage->getConfig()->getTitle()->prepend(__('New PNG'));
        } else {
            $resultPage->addBreadcrumb(__('Edit PNG'), __('Edit PNG'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_pngRepository->getById($pngId)->getSku()
            );
        }
        return $resultPage;
    }

    /**
     * Initialize current PNG and set it in the registry.
     *
     * @return int
     */
    protected function _initPng()
    {
        $pngId = $this->getRequest()->getParam('png_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_PNG_ID, $pngId);

        return $pngId;
    }
}
