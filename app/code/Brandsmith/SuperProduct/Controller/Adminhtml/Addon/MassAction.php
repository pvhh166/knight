<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Addon;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Addon repository
     * 
     * @var \Brandsmith\SuperProduct\Api\AddonRepositoryInterface
     */
    protected $_addonRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * Addon collection factory
     * 
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $_successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $_errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Brandsmith\SuperProduct\Api\AddonRepositoryInterface $addonRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Brandsmith\SuperProduct\Model\ResourceModel\Addon\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->_addonRepository   = $addonRepository;
        $this->_filter            = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_successMessage    = $successMessage;
        $this->_errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Brandsmith\SuperProduct\Api\Data\AddonInterface $addon
     * @return mixed
     */
    abstract protected function _massAction(\Brandsmith\SuperProduct\Api\Data\AddonInterface $addon);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $addon) {
                $this->_massAction($addon);
            }
            $this->messageManager->addSuccessMessage(__($this->_successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->_errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('brandsmith_superproduct/*/index');
        return $redirectResult;
    }
}
