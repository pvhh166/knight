<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Addon;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Addon
{
    /**
     * Initialize current Addon and set it in the registry.
     *
     * @return int
     */
    protected function _initAddon()
    {
        $addonId = $this->getRequest()->getParam('addon_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_ADDON_ID, $addonId);

        return $addonId;
    }

    /**
     * Edit or create Addon
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $addonId = $this->_initAddon();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_addon');
        $resultPage->getConfig()->getTitle()->prepend(__('Addons'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('Addons'), __('Addons'), $this->getUrl('brandsmith_superproduct/addon'));

        if ($addonId === null) {
            $resultPage->addBreadcrumb(__('New Addon'), __('New Addon'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Addon'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Addon'), __('Edit Addon'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_addonRepository->getById($addonId)->getName()
            );
        }
        return $resultPage;
    }
}
