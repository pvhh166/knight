<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Controller\Adminhtml\Product;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Class Save
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Magento\Catalog\Controller\Adminhtml\Product\Save
{
	/**
	 * @var RequestInterface
	 */
	protected $request;

	/**
	 * Save constructor.
	 *
	 * @param Action\Context $context
	 * @param Builder $productBuilder
	 * @param Initialization\Helper $initializationHelper
	 * @param \Magento\Catalog\Model\Product\Copier $productCopier
	 * @param \Magento\Catalog\Model\Product\TypeTransitionManager $productTypeManager
	 * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
	 */
	public function __construct(
	    \Magento\Backend\App\Action\Context $context,
	    \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder,
	    \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $initializationHelper,
	    \Magento\Catalog\Model\Product\Copier $productCopier,
	    \Magento\Catalog\Model\Product\TypeTransitionManager $productTypeManager,
	    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	    RequestInterface $request
	) {
	    $this->request = $request;
	    parent::__construct($context, $productBuilder, $initializationHelper, $productCopier, $productTypeManager, $productRepository);
	}

    public function aroundExecute($subject, \Closure $proceed)
    {
    	$productId = $this->request->getParam('id');
    	$productAttributeSetId = $this->request->getParam('set');
    	$productTypeId = $this->request->getParam('type');
        try {
            if (!isset($this->request->getPost('baseproduct')['super_selections']) && $productTypeId == 'super') {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save Super product without Base product!'));
            }

            if (!isset($this->request->getPost('addon')['super_selections']) && $productTypeId == 'super') {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save Super product without Add-on product!'));
            }

            if (isset($this->request->getPost('productrules')['rule_records']) && $productTypeId == 'super') {
                $ruleList = [];
                foreach ($this->request->getPost('productrules')['rule_records'] as $key => $option) {
                    $ruleList[] = $option['sku'] . $option['sku_dep'];
                }

                if (count($ruleList) !== count(array_flip($ruleList))) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save Super product with duplicate Product Dependencies rule!'));
                }
            }

            if (isset($this->request->getPost('rules')['rule_records']) && $productTypeId == 'super') {
                $ruleList = [];
                foreach ($this->request->getPost('rules')['rule_records'] as $key => $option) {
                    $ruleList[] = $option['sku'] . $option['attribute'] . $option['option'] . $option['condition'] . $option['sku_dep'] . $option['attribute_dep'] . $option['option_dep'] . $option['condition_dep'];
                }

                if (count($ruleList) !== count(array_flip($ruleList)))
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save Super product with duplicate Attribute Dependencies rule!'));
                }
            }

//            if (isset($this->request->getPost('pngrules')['rule_records']) && $productTypeId == 'super') {
//                $ruleList = [];
//                foreach ($this->request->getPost('pngrules')['rule_records'] as $key => $option) {
//                    $ruleList[] = $option['sku'] . $option['attribute'] . $option['attribute_dep'];
//                }
//
//                if (count($ruleList) !== count(array_flip($ruleList))) {
//                    throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save Super product with duplicate PNG Layer Dependencies rule!'));
//                }
//            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $subject->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            $subject->messageManager->addExceptionMessage($e);
            $resultRedirect = $subject->resultRedirectFactory->create();
            $redirectBack = $productId ? true : 'new';
            if ($redirectBack === 'new') {
                $resultRedirect->setPath(
                    'catalog/*/new',
                    ['set' => $productAttributeSetId, 'type' => $productTypeId]
                );
            } elseif ($redirectBack) {
                $resultRedirect->setPath(
                    'catalog/*/edit',
                    ['id' => $productId, '_current' => true, 'set' => $productAttributeSetId]
                );
            } else {
                $resultRedirect->setPath('catalog/*/');
            }
            return $resultRedirect;
        } 
        
        return $proceed();
    }
}
