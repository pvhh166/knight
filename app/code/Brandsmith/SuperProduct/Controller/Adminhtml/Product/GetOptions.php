<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;

class GetOptions extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::products';

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @param Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * Get attributes
     *
     * @return void
     */
    public function execute()
    {
        $this->storeManager->setCurrentStore(\Magento\Store\Model\Store::ADMIN_CODE);
        $product = $this->productRepository->get($this->getRequest()->getParam('sku'));
        $attributeCode = $this->getRequest()->getParam('attribute');
        $data = $product->getTypeInstance()->getConfigurableOptions($product);
        $options = [];
        $optionsList = [];
        foreach($data as $attrs){
            foreach ($attrs as $attribute) {
                if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList))
                {
                    $options[] = [
                        'value' => $attribute['value_index'],
                        'label' => $attribute['option_title'],
                    ];
                    $optionsList[] = $attribute['value_index'];
                }
            }
        }
        // $options = array_unique($options);
        $this->getResponse()->representJson($this->jsonHelper->jsonEncode($options));
    }
}
