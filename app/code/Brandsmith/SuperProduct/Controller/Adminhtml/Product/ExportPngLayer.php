<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;

class ExportPngLayer extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::products';

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * PNG Layer repository
     *
     * @var \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
     */
    protected $pnglayerRepository;

    /**
     * PNG Layer repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngRepositoryInterface
     */
    protected $pngRepository;

    /**
     * PNG Layer factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory
     */
    protected $pngFactory;

    protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;


    /**
     * @param Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository,
        \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository,
        \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory $pngFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        $this->storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
        $this->pnglayerRepository = $pnglayerRepository;
        $this->pngFactory = $pngFactory;
        $this->pngRepository = $pngRepository;
        $this->productRepository = $productRepository;
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        parent::__construct($context);
    }

    /**
     * Get attributes
     *
     * @return void
     */
    public function execute()
    {
        $sku = $this->getRequest()->getParam('sku');
        $t = time();
        $fileName = "Export-template-$sku-$t.csv";
        $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . "/" . $fileName;
        $personalData = $this->getData();
        $this->csvProcessor
            ->setDelimiter(',')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $personalData
            );

        return $this->fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );
    }

    protected function getData()
    {
        $sku = $this->getRequest()->getParam('sku');
        $baseSku = $this->getRequest()->getParam('base_sku');
        $baseAttribute = $this->getRequest()->getParam('base_attribute');
        $pngKey = count($this->getRequest()->getParam('pngkey')) > 0 ? $this->getRequest()->getParam('pngkey') : [];
        $pngRuleSkus = count($this->getRequest()->getParam('pngrulesku')) > 0 ? $this->getRequest()->getParam('pngrulesku') : [];
        $pngRuleAttributes = count($this->getRequest()->getParam('pngruleattribute')) > 0 ? $this->getRequest()->getParam('pngruleattribute') : [];
        $pngRuleAttributeDeps = count($this->getRequest()->getParam('pngruleattributedep')) > 0 ? $this->getRequest()->getParam('pngruleattributedep') : [];
        $ruleKey = count($this->getRequest()->getParam('rulekey')) > 0 ? $this->getRequest()->getParam('rulekey') : [];
        $ruleSkus = count($this->getRequest()->getParam('rulesku')) > 0 ? $this->getRequest()->getParam('rulesku') : [];
        $ruleAttributes = count($this->getRequest()->getParam('ruleattribute')) > 0 ? $this->getRequest()->getParam('ruleattribute') : [];
        $ruleOptions = count($this->getRequest()->getParam('ruleoption')) > 0 ? $this->getRequest()->getParam('ruleoption') : [];
        $ruleOptionDeps = count($this->getRequest()->getParam('ruleoptiondep')) > 0 ? $this->getRequest()->getParam('ruleoptiondep') : [];

        try {
            $superProduct = $this->productRepository->get($sku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $superProduct = false;
        }
        try {
            $baseProduct = $this->productRepository->get($baseSku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $baseProduct = false;
        }

        if (!$superProduct || !$baseProduct)
            return;
        $baseOptions = $this->getOptions($baseProduct, $baseAttribute);

        $pngList = $this->pngRepository->getListBySuperProductId($superProduct->getId());
        if (count($pngList) < 1)
            return;
        $result = [];

        // header
        $result[] = [
            'Super SKU',
            'Product SKU',
            'Attribute',
            'Base Attribute',
            'Rule Attribute',
            'Option',
            'Base Option',
            'Rule Option',
            'Image',
            'Default',
            'Area',
        ];

        foreach ($pngList as $png) {
            $product = $this->productRepository->get($png->getSku());
            if ($product->getTypeId() == 'simple') {
                // check rule
                $ruleBaseOption = false;
                $ruleFlag = false;
                if (in_array($png->getSku(), $ruleSkus) && in_array($png->getSku(), $ruleAttributes) && in_array($png->getSku(), $ruleOptionDeps)) {
                    $posRuleTrue = array_search($png->getSku() . ',' . $png->getSku() . ',' . $png->getSku() . ',1', $ruleKey);
                    if ($posRuleTrue !== false) {
                        $ruleBaseOption = $ruleOptions[$posRuleTrue];
                        $ruleFlag = true;
                    }

                    $posRuleFalse = array_search($png->getSku() . ',' . $png->getSku() . ',' . $png->getSku() . ',0', $ruleKey);
                    if ($posRuleFalse !== false) {
                        $ruleBaseOption = $ruleOptions[$posRuleFalse];
                        $ruleFlag = false;
                    }
                }
                foreach ($baseOptions as $baseOption) {
                    if ($ruleBaseOption) {
                        if ($ruleFlag) {
                            if ($baseOption['value'] != $ruleBaseOption) continue;
                        } else {
                            if ($baseOption['value'] == $ruleBaseOption) continue;
                        }
                    }
                    $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $png->getSku(), $baseOption['value'], null, $png->getMultiLayer());
                    $result[] = [
                        $sku,
                        $png->getSku(),
                        $png->getAttribute(),
                        $baseAttribute,
                        '',
                        $png->getSku(),
                        $baseOption['label'],
                        '',
                        $pngLayer->getId() ? $pngLayer->getImage() : '',
                        $pngLayer->getId() ? $pngLayer->getIsDefault() : '0',
                        $png->getMultiLayer()
                    ];
                }
            } else if ($png->getSku() == $baseSku && $png->getAttribute() == $baseAttribute) {
                foreach ($baseOptions as $baseOption) {
                    $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $baseOption['value'], $baseOption['value'], null);
                    $result[] = [
                        $sku,
                        $png->getSku(),
                        $png->getAttribute(),
                        $baseAttribute,
                        '',
                        $baseOption['label'],
                        $baseOption['label'],
                        '',
                        $pngLayer->getId() ? $pngLayer->getImage() : '',
                        $pngLayer->getId() ? $pngLayer->getIsDefault() : '0',
                        '0'
                    ];
                }
            } else {
                if (in_array($png->getSku(), $pngRuleSkus) && in_array($png->getAttribute(), $pngRuleAttributeDeps)) {
                    $pos = array_search($png->getSku() . ',' . $png->getAttribute(), $pngKey);
                    foreach ($this->getOptions($product, $png->getAttribute()) as $option) {
                        foreach ($this->getOptions($product, $pngRuleAttributes[$pos]) as $ruleOption) {
                            // check rule
                            $ruleBaseOption = false;
                            $ruleFlag = false;
                            if (in_array($png->getSku(), $ruleSkus) && in_array($png->getAttribute(), $ruleAttributes) && in_array($option['value'], $ruleOptionDeps)) {
                                $posRuleTrue = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',1', $ruleKey);
                                if ($posRuleTrue !== false) {
                                    $ruleBaseOption = $ruleOptions[$posRuleTrue];
                                    $ruleFlag = true;
                                }

                                $posRuleFalse = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',0', $ruleKey);
                                if ($posRuleFalse !== false) {
                                    $ruleBaseOption = $ruleOptions[$posRuleFalse];
                                    $ruleFlag = false;
                                }
                            }

                            foreach ($baseOptions as $baseOption) {
                                if ($ruleBaseOption) {
                                    if ($ruleFlag) {
                                        if ($baseOption['value'] != $ruleBaseOption) continue;
                                    } else {
                                        if ($baseOption['value'] == $ruleBaseOption) continue;
                                    }
                                }
                                $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, $pngRuleAttributes[$pos], $option['value'], $baseOption['value'], $ruleOption['value'], $png->getMultiLayer());
                                $result[] = [
                                    $sku,
                                    $png->getSku(),
                                    $png->getAttribute(),
                                    $baseAttribute,
                                    $pngRuleAttributes[$pos],
                                    $option['label'],
                                    $baseOption['label'],
                                    $ruleOption['label'],
                                    $pngLayer->getId() ? $pngLayer->getImage() : '',
                                    $pngLayer->getId() ? $pngLayer->getIsDefault() : '0',
                                    $png->getMultiLayer()
                                ];
                            }
                        }
                    }
                } else {
                    foreach ($this->getOptions($product, $png->getAttribute()) as $option) {
                        // check rule
                        $ruleBaseOption = false;
                        $ruleFlag = false;
                        if (in_array($png->getSku(), $ruleSkus) && in_array($png->getAttribute(), $ruleAttributes) && in_array($option['value'], $ruleOptionDeps)) {
                            $posRuleTrue = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',1', $ruleKey);
                            if ($posRuleTrue !== false) {
                                $ruleBaseOption = $ruleOptions[$posRuleTrue];
                                $ruleFlag = true;
                            }

                            $posRuleFalse = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',0', $ruleKey);
                            if ($posRuleFalse !== false) {
                                $ruleBaseOption = $ruleOptions[$posRuleFalse];
                                $ruleFlag = false;
                            }
                        }

                        foreach ($baseOptions as $baseOption) {
                            if ($ruleBaseOption) {
                                if ($ruleFlag) {
                                    if ($baseOption['value'] != $ruleBaseOption) continue;
                                } else {
                                    if ($baseOption['value'] == $ruleBaseOption) continue;
                                }
                            }
                            $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $option['value'], $baseOption['value'], null, $png->getMultiLayer());
                            $result[] = [
                                $sku,
                                $png->getSku(),
                                $png->getAttribute(),
                                $baseAttribute,
                                '',
                                $option['label'],
                                $baseOption['label'],
                                '',
                                $pngLayer->getId() ? $pngLayer->getImage() : '',
                                $pngLayer->getId() ? $pngLayer->getIsDefault() : '0',
                                $png->getMultiLayer()
                            ];
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get options
     *
     * @return void
     */
    public function getOptions($product, $attributeCode)
    {
        $data = $product->getTypeInstance()->getConfigurableOptions($product);
        $options = [];
        $optionsList = [];
        foreach ($data as $attrs) {
            foreach ($attrs as $attribute) {
                if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
                    $options[] = [
                        'value' => $attribute['value_index'],
                        'label' => $attribute['option_title'],
                    ];
                    $optionsList[] = $attribute['value_index'];
                }
            }
        }
        return $options;
    }
}
