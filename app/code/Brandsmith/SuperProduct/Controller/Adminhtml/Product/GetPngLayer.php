<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;

class GetPngLayer extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::products';

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * PNG Layer repository
     * 
     * @var \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface
     */
    protected $pnglayerRepository;

    /**
     * PNG Layer repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngRepositoryInterface
     */
    protected $pngRepository;

    /**
     * PNG Layer factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory
     */
    protected $pngFactory;


    /**
     * @param Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Brandsmith\SuperProduct\Api\PnglayerRepositoryInterface $pnglayerRepository,
        \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository,
        \Brandsmith\SuperProduct\Api\Data\PngInterfaceFactory $pngFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
        $this->pnglayerRepository = $pnglayerRepository;
        $this->pngFactory = $pngFactory;
        $this->pngRepository = $pngRepository;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * Get attributes
     *
     * @return void
     */
    public function execute()
    {
        $this->storeManager->setCurrentStore(\Magento\Store\Model\Store::ADMIN_CODE);
        $superSku = $this->getRequest()->getParam('super_sku');
        $skuList = $this->getRequest()->getParam('sku');
        $nameList = $this->getRequest()->getParam('name');
        $baseAttribute = $this->getRequest()->getParam('base_attribute');
        try {
            $superProduct = $this->productRepository->get($superSku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $superProduct = false;
        }
        $this->reloadStatus();
        $png = [];
        $i = 1;
        foreach ($skuList as $key => $sku) {
            $product = $this->productRepository->get($sku);
            if ($product->getTypeId() == 'simple')
            {
                $defaultPng = [
                    'sku' => $nameList[$key],
                    'attribute' => $nameList[$key],
                    'sku_hide' => $sku,
                    'attribute_hide' => $sku,
                    'type' => 'Base Dependency',
                    'position' => $i,
                    'options' => '1',
                    'status' => 'Not ready'
                    // 'status' => $this->getStatus($product,$sku)
                ];
                $png[$i] = $defaultPng;
                $i++;
                if ($superProduct) {
                    $pngView = $this->pngRepository->getListBySuperProductIdView($superProduct->getId(), $sku, $sku, $baseAttribute);
                    if (count($pngView) > 0)
                    {
                        $i--;
                        foreach ($pngView as $pngRow) {
                            if ($pngRow->getId()) {
                                $png[$i] = $defaultPng;
                                $png[$i]['option_hide'] = $pngRow->getDefaultOption();
                                $png[$i]['default_option'] = $pngRow->getDefaultOption();
                                $png[$i]['position'] = $pngRow->getPosition();
                                $png[$i]['multi_layer'] = $pngRow->getMultiLayer();
                                $png[$i]['status'] = $pngRow->getStatus() == 1 ? 'Ready' : 'Not ready';
                                $i++;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach ($product->getTypeInstance()->getConfigurableAttributes($product) as $attribute) {
                    $productAttribute = $attribute->getProductAttribute();
                    if ($key == 0 && $baseAttribute == $productAttribute->getAttributeCode())
                    {
                        $png[0] = [
                            'sku' => $nameList[$key],
                            'attribute' => $productAttribute->getStoreLabel($product->getStoreId()),
                            'sku_hide' => $sku,
                            'attribute_hide' => $productAttribute->getAttributeCode(),
                            'type' => 'Base Attribute',
                            'position' => 0,
                            'options' => $this->getCountOptions($product,$productAttribute->getAttributeCode()),
                            'status' => 'Not ready'
                            // 'status' => $this->getStatus($product,$productAttribute->getAttributeCode())
                        ];
                        if ($superProduct) {
                            $pngRow = $this->pngRepository->getBySuperProduct($superProduct->getId(), $sku, $productAttribute->getAttributeCode(), $baseAttribute);
                            if ($pngRow->getId()) {
                                $png[0]['option_hide'] = $pngRow->getDefaultOption();
                                $png[0]['default_option'] = $pngRow->getDefaultOption();
                                $png[0]['position'] = $pngRow->getPosition();
                                $png[$i]['multi_layer'] = $pngRow->getMultiLayer();
                                $png[0]['status'] = $pngRow->getStatus() == 1 ? 'Ready' : 'Not ready';
                            }
                        }
                    }
                    else
                    {
                        $defaultPng = [
                            'sku' => $nameList[$key],
                            'attribute' => $productAttribute->getStoreLabel($product->getStoreId()),
                            'sku_hide' => $sku,
                            'attribute_hide' => $productAttribute->getAttributeCode(),
                            'type' => 'Base Dependency',
                            'position' => $i,
                            'options' => $this->getCountOptions($product,$productAttribute->getAttributeCode()),
                            'status' => 'Not ready'
                            // 'status' => $this->getStatus($product,$productAttribute->getAttributeCode())
                        ];
                        $png[$i] = $defaultPng;
                        $i++;
                        if ($superProduct) {
                            $pngView = $this->pngRepository->getListBySuperProductIdView($superProduct->getId(), $sku, $productAttribute->getAttributeCode(), $baseAttribute);
                            if (count($pngView) > 0)
                            {
                                $i--;
                                foreach ($pngView as $pngRow) {
                                    if ($pngRow->getId()) {
                                        $png[$i] = $defaultPng;
                                        $png[$i]['option_hide'] = $pngRow->getDefaultOption();
                                        $png[$i]['default_option'] = $pngRow->getDefaultOption();
                                        $png[$i]['position'] = $pngRow->getPosition();
                                        $png[$i]['multi_layer'] = $pngRow->getMultiLayer();
                                        $png[$i]['status'] = $pngRow->getStatus() == 1 ? 'Ready' : 'Not ready';
                                        $i++;
                                    }
                                }
                            }
                        }
                    } 
                }
            }
        }
        usort($png, [$this, 'sortByPosition']);
        $this->getResponse()->representJson($this->jsonHelper->jsonEncode($png));
    }

    /**
     * Reload status
     *
     * @return void
     */
    public function reloadStatus()
    {
        $superSku = $this->getRequest()->getParam('super_sku');
        $skuList = $this->getRequest()->getParam('sku');
        $baseAttribute = $this->getRequest()->getParam('base_attribute');
        $pngKey = count($this->getRequest()->getParam('pngkey')) > 0 ? $this->getRequest()->getParam('pngkey') : [];
        $pngRuleSkus = count($this->getRequest()->getParam('pngrulesku')) > 0 ? $this->getRequest()->getParam('pngrulesku') : [];
        $pngRuleAttributes = count($this->getRequest()->getParam('pngruleattribute')) > 0 ? $this->getRequest()->getParam('pngruleattribute') : [];
        $pngRuleAttributeDeps = count($this->getRequest()->getParam('pngruleattributedep')) > 0 ? $this->getRequest()->getParam('pngruleattributedep') : [];
        $ruleKey = count($this->getRequest()->getParam('rulekey')) > 0 ? $this->getRequest()->getParam('rulekey') : [];
        $ruleSkus = count($this->getRequest()->getParam('rulesku')) > 0 ? $this->getRequest()->getParam('rulesku') : [];
        $ruleAttributes = count($this->getRequest()->getParam('ruleattribute')) > 0 ? $this->getRequest()->getParam('ruleattribute') : [];
        $ruleOptions = count($this->getRequest()->getParam('ruleoption')) > 0 ? $this->getRequest()->getParam('ruleoption') : [];
        $ruleOptionDeps = count($this->getRequest()->getParam('ruleoptiondep')) > 0 ? $this->getRequest()->getParam('ruleoptiondep') : [];

        try {
            $superProduct = $this->productRepository->get($superSku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $superProduct = false;
        }
        try {
            $baseProduct = $this->productRepository->get($skuList[0]);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $baseProduct = false;
        }

        if (!$superProduct || !$baseProduct)
            return;

        $baseOptions = $this->getOptions($baseProduct, $baseAttribute);

        $pngList = $this->pngRepository->getListBySuperProductId($superProduct->getId());

        if (count($pngList) < 1)
            return;

        foreach ($pngList as $png) {
            $product = $this->productRepository->get($png->getSku());
            $png->setStatus(1);
            if ($product->getTypeId() == 'simple') {
                // check rule
                $ruleBaseOption = false;
                $ruleFlag = false;
                if (in_array($png->getSku(), $ruleSkus) && in_array($png->getSku(), $ruleAttributes) && in_array($png->getSku(), $ruleOptionDeps)) {
                    $posRuleTrue = array_search($png->getSku() . ',' . $png->getSku() . ',' . $png->getSku() . ',1', $ruleKey);
                    if ($posRuleTrue !== false) {
                        $ruleBaseOption = $ruleOptions[$posRuleTrue];
                        $ruleFlag = true;
                    }

                    $posRuleFalse = array_search($png->getSku() . ',' . $png->getSku() . ',' . $png->getSku() . ',0', $ruleKey);
                    if ($posRuleFalse !== false) {
                        $ruleBaseOption = $ruleOptions[$posRuleFalse];
                        $ruleFlag = false;
                    }
                }
                foreach ($baseOptions as $baseOption) {
                    if ($ruleBaseOption) {
                        if ($ruleFlag) {
                            if ($baseOption['value'] != $ruleBaseOption) continue;
                        } else {
                            if ($baseOption['value'] == $ruleBaseOption) continue;
                        }
                    }
                    $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $png->getSku(), $baseOption['value'], null, $png->getMultiLayer());
                    if (!$pngLayer->getId()) $png->setStatus(0);
                }
            } else if ($png->getSku() == $skuList[0] && $png->getAttribute() == $baseAttribute) {
                foreach ($baseOptions as $baseOption) {
                    $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $baseOption['value'], $baseOption['value'], null);
                    if (!$pngLayer->getId()) $png->setStatus(0);
                }
            } else {
                if (in_array($png->getSku(), $pngRuleSkus) && in_array($png->getAttribute(), $pngRuleAttributeDeps)) {
                    $pos = array_search($png->getSku() . ',' . $png->getAttribute(), $pngKey);
                    foreach ($this->getOptions($product, $png->getAttribute()) as $option) {
                        foreach ($this->getOptions($product, $pngRuleAttributes[$pos]) as $ruleOption) {
                            // check rule
                            $ruleBaseOption = false;
                            $ruleFlag = false;
                            if (in_array($png->getSku(), $ruleSkus) && in_array($png->getAttribute(), $ruleAttributes) && in_array($option['value'], $ruleOptionDeps)) {
                                $posRuleTrue = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',1', $ruleKey);
                                if ($posRuleTrue !== false) {
                                    $ruleBaseOption = $ruleOptions[$posRuleTrue];
                                    $ruleFlag = true;
                                }

                                $posRuleFalse = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',0', $ruleKey);
                                if ($posRuleFalse !== false) {
                                    $ruleBaseOption = $ruleOptions[$posRuleFalse];
                                    $ruleFlag = false;
                                }
                            }

                            foreach ($baseOptions as $baseOption) {
                                if ($ruleBaseOption) {
                                    if ($ruleFlag) {
                                        if ($baseOption['value'] != $ruleBaseOption) continue;
                                    } else {
                                        if ($baseOption['value'] == $ruleBaseOption) continue;
                                    }
                                }
                                $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, $pngRuleAttributes[$pos], $option['value'], $baseOption['value'], $ruleOption['value'], $png->getMultiLayer());
                                if (!$pngLayer->getId()) $png->setStatus(0);
                            }
                        }
                    }
                } else {
                    foreach ($this->getOptions($product, $png->getAttribute()) as $option) {
                        // check rule
                        $ruleBaseOption = false;
                        $ruleFlag = false;
                        if (in_array($png->getSku(), $ruleSkus) && in_array($png->getAttribute(), $ruleAttributes) && in_array($option['value'], $ruleOptionDeps)) {
                            $posRuleTrue = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',1', $ruleKey);
                            if ($posRuleTrue !== false) {
                                $ruleBaseOption = $ruleOptions[$posRuleTrue];
                                $ruleFlag = true;
                            }

                            $posRuleFalse = array_search($png->getSku() . ',' . $png->getAttribute() . ',' . $option['value'] . ',0', $ruleKey);
                            if ($posRuleFalse !== false) {
                                $ruleBaseOption = $ruleOptions[$posRuleFalse];
                                $ruleFlag = false;
                            }
                        }

                        foreach ($baseOptions as $baseOption) {
                            if ($ruleBaseOption) {
                                if ($ruleFlag) {
                                    if ($baseOption['value'] != $ruleBaseOption) continue;
                                } else {
                                    if ($baseOption['value'] == $ruleBaseOption) continue;
                                }
                            }
                            $pngLayer = $this->pnglayerRepository->getBySuperProductId((int)$superProduct->getId(), $product->getId(), $png->getAttribute(), $baseAttribute, null, $option['value'], $baseOption['value'], null, $png->getMultiLayer());
                            if (!$pngLayer->getId()) $png->setStatus(0);
                        }
                    }
                }
            }
            $png->save();
        }
    }

    /**
     * Get options
     *
     * @return void
     */
    public function getOptions($product, $attributeCode)
    {
        $data = $product->getTypeInstance()->getConfigurableOptions($product);
        $options = [];
        $optionsList = [];
        foreach ($data as $attrs) {
            foreach ($attrs as $attribute) {
                if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
                    $options[] = [
                        'value' => $attribute['value_index'],
                        'label' => $attribute['option_title'],
                    ];
                    $optionsList[] = $attribute['value_index'];
                }
            }
        }
        return $options;
    }

    /**
     * Get options
     *
     * @return void
     */
    public function getCountOptions($product, $attributeCode)
    {
        $data = $product->getTypeInstance()->getConfigurableOptions($product);
        $options = [];
        $optionsList = [];
        foreach ($data as $attrs) {
            foreach ($attrs as $attribute) {
                if ($attribute['attribute_code'] == $attributeCode && !in_array($attribute['value_index'], $optionsList)) {
                    $options[] = [
                        'value' => $attribute['value_index'],
                        'label' => $attribute['option_title'],
                    ];
                    $optionsList[] = $attribute['value_index'];
                }
            }
        }
        return count($optionsList);
    }

    function sortByPosition($a, $b)
    {
        if ($a['position'] == $b['position']) {
            return 0;
        }
        return ($a['position'] < $b['position']) ? -1 : 1;
    }
}
