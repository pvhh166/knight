<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule
{
    /**
     * Edit or create PNG Rule
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $pngruleId = $this->_initPngrule();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_pngrule');
        $resultPage->getConfig()->getTitle()->prepend(__('PNG&#x20;Rules'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('PNG&#x20;Rules'), __('PNG&#x20;Rules'), $this->getUrl('brandsmith_superproduct/pngrule'));

        if ($pngruleId === null) {
            $resultPage->addBreadcrumb(__('New PNG&#x20;Rule'), __('New PNG&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(__('New PNG&#x20;Rule'));
        } else {
            $resultPage->addBreadcrumb(__('Edit PNG&#x20;Rule'), __('Edit PNG&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_pngruleRepository->getById($pngruleId)->getSku()
            );
        }
        return $resultPage;
    }

    /**
     * Initialize current PNG Rule and set it in the registry.
     *
     * @return int
     */
    protected function _initPngrule()
    {
        $pngruleId = $this->getRequest()->getParam('pngrule_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_PNGRULE_ID, $pngruleId);

        return $pngruleId;
    }
}
