<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule;

class Save extends \Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule
{
    /**
     * PNG Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory
     */
    protected $_pngruleFactory;

    /**
     * Data Object Processor
     *
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $_dataObjectProcessor;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Uploader pool
     *
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * Data Persistor
     *
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngruleRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngruleFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Brandsmith\SuperProduct\Api\PngruleRepositoryInterface $pngruleRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brandsmith\SuperProduct\Api\Data\PngruleInterfaceFactory $pngruleFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    )
    {
        $this->_pngruleFactory = $pngruleFactory;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_uploaderPool = $uploaderPool;
        $this->_dataPersistor = $dataPersistor;
        parent::__construct($context, $coreRegistry, $pngruleRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\PngruleInterface $pngrule */
        $pngrule = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['pngrule_id']) ? $data['pngrule_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $pngrule = $this->_pngruleRepository->getById((int)$id);
            } else {
                unset($data['pngrule_id']);
                $pngrule = $this->_pngruleFactory->create();
            }
            $this->_dataObjectHelper->populateWithArray($pngrule, $data, \Brandsmith\SuperProduct\Api\Data\PngruleInterface::class);
            $this->_pngruleRepository->save($pngrule);
            $this->messageManager->addSuccessMessage(__('You saved the PNG&#x20;Rule'));
            $this->_dataPersistor->clear('brandsmith_superproduct_pngrule');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('brandsmith_superproduct/pngrule/edit', ['pngrule_id' => $pngrule->getId()]);
            } else {
                $resultRedirect->setPath('brandsmith_superproduct/pngrule');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_dataPersistor->set('brandsmith_superproduct_pngrule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/pngrule/edit', ['pngrule_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the PNG&#x20;Rule'));
            $this->_dataPersistor->set('brandsmith_superproduct_pngrule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/pngrule/edit', ['pngrule_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Brandsmith\SuperProduct\Model\Uploader
     * @throws \Exception
     */
    protected function _getUploader($type)
    {
        return $this->_uploaderPool->getUploader($type);
    }
}
