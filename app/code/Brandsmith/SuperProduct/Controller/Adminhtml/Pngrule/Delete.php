<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule;

class Delete extends \Brandsmith\SuperProduct\Controller\Adminhtml\Pngrule
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('pngrule_id');
        if ($id) {
            try {
                $this->_pngruleRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The PNG&#x20;Rule has been deleted.'));
                $resultRedirect->setPath('brandsmith_superproduct/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The PNG&#x20;Rule no longer exists.'));
                return $resultRedirect->setPath('brandsmith_superproduct/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('brandsmith_superproduct/pngrule/edit', ['pngrule_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the PNG&#x20;Rule'));
                return $resultRedirect->setPath('brandsmith_superproduct/pngrule/edit', ['pngrule_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a PNG&#x20;Rule to delete.'));
        $resultRedirect->setPath('brandsmith_superproduct/*/');
        return $resultRedirect;
    }
}
