<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Productrule;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Productrule
{
    /**
     * Edit or create Product Rule
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $productruleId = $this->_initProductrule();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_productrule');
        $resultPage->getConfig()->getTitle()->prepend(__('Product&#x20;Rules'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('Product&#x20;Rules'), __('Product&#x20;Rules'), $this->getUrl('brandsmith_superproduct/productrule'));

        if ($productruleId === null) {
            $resultPage->addBreadcrumb(__('New Product&#x20;Rule'), __('New Product&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Product&#x20;Rule'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Product&#x20;Rule'), __('Edit Product&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_productruleRepository->getById($productruleId)->getSku()
            );
        }
        return $resultPage;
    }

    /**
     * Initialize current Product Rule and set it in the registry.
     *
     * @return int
     */
    protected function _initProductrule()
    {
        $productruleId = $this->getRequest()->getParam('productrule_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_PRODUCTRULE_ID, $productruleId);

        return $productruleId;
    }
}
