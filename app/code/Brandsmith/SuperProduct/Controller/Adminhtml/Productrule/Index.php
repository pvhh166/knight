<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Productrule;

class Index extends \Brandsmith\SuperProduct\Controller\Adminhtml\Productrule
{
    /**
     * Product Rules list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::productrule');
        $resultPage->getConfig()->getTitle()->prepend(__('Product&#x20;Rules'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('Product&#x20;Rules'), __('Product&#x20;Rules'));
        return $resultPage;
    }
}
