<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Productrule;

class Save extends \Brandsmith\SuperProduct\Controller\Adminhtml\Productrule
{
    /**
     * Product Rule factory
     *
     * @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory
     */
    protected $_productruleFactory;

    /**
     * Data Object Processor
     *
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $_dataObjectProcessor;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Uploader pool
     *
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * Data Persistor
     *
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productruleRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productruleFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Brandsmith\SuperProduct\Api\ProductruleRepositoryInterface $productruleRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brandsmith\SuperProduct\Api\Data\ProductruleInterfaceFactory $productruleFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    )
    {
        $this->_productruleFactory = $productruleFactory;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_uploaderPool = $uploaderPool;
        $this->_dataPersistor = $dataPersistor;
        parent::__construct($context, $coreRegistry, $productruleRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule */
        $productrule = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['productrule_id']) ? $data['productrule_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $productrule = $this->_productruleRepository->getById((int)$id);
            } else {
                unset($data['productrule_id']);
                $productrule = $this->_productruleFactory->create();
            }
            $this->_dataObjectHelper->populateWithArray($productrule, $data, \Brandsmith\SuperProduct\Api\Data\ProductruleInterface::class);
            $this->_productruleRepository->save($productrule);
            $this->messageManager->addSuccessMessage(__('You saved the Product&#x20;Rule'));
            $this->_dataPersistor->clear('brandsmith_superproduct_productrule');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('brandsmith_superproduct/productrule/edit', ['productrule_id' => $productrule->getId()]);
            } else {
                $resultRedirect->setPath('brandsmith_superproduct/productrule');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_dataPersistor->set('brandsmith_superproduct_productrule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/productrule/edit', ['productrule_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Product&#x20;Rule'));
            $this->_dataPersistor->set('brandsmith_superproduct_productrule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/productrule/edit', ['productrule_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Brandsmith\SuperProduct\Model\Uploader
     * @throws \Exception
     */
    protected function _getUploader($type)
    {
        return $this->_uploaderPool->getUploader($type);
    }
}
