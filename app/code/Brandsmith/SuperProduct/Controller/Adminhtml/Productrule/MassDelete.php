<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Productrule;

class MassDelete extends \Brandsmith\SuperProduct\Controller\Adminhtml\Productrule\MassAction
{
    /**
     * @param \Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule
     * @return $this
     */
    protected function _massAction(\Brandsmith\SuperProduct\Api\Data\ProductruleInterface $productrule)
    {
        $this->_productruleRepository->delete($productrule);
        return $this;
    }
}
