<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Rule;

class Save extends \Brandsmith\SuperProduct\Controller\Adminhtml\Rule
{
    /**
     * Dependency Rule factory
     * 
     * @var \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory
     */
    protected $_ruleFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $_dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Brandsmith\SuperProduct\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Brandsmith\SuperProduct\Api\RuleRepositoryInterface $ruleRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brandsmith\SuperProduct\Api\Data\RuleInterfaceFactory $ruleFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\SuperProduct\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->_ruleFactory         = $ruleFactory;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_dataObjectHelper    = $dataObjectHelper;
        $this->_uploaderPool        = $uploaderPool;
        $this->_dataPersistor       = $dataPersistor;
        parent::__construct($context, $coreRegistry, $ruleRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Brandsmith\SuperProduct\Api\Data\RuleInterface $rule */
        $rule = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['rule_id']) ? $data['rule_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $rule = $this->_ruleRepository->getById((int)$id);
            } else {
                unset($data['rule_id']);
                $rule = $this->_ruleFactory->create();
            }
            $this->_dataObjectHelper->populateWithArray($rule, $data, \Brandsmith\SuperProduct\Api\Data\RuleInterface::class);
            $this->_ruleRepository->save($rule);
            $this->messageManager->addSuccessMessage(__('You saved the Dependency&#x20;Rule'));
            $this->_dataPersistor->clear('brandsmith_superproduct_rule');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('brandsmith_superproduct/rule/edit', ['rule_id' => $rule->getId()]);
            } else {
                $resultRedirect->setPath('brandsmith_superproduct/rule');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_dataPersistor->set('brandsmith_superproduct_rule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/rule/edit', ['rule_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Dependency&#x20;Rule'));
            $this->_dataPersistor->set('brandsmith_superproduct_rule', $postData);
            $resultRedirect->setPath('brandsmith_superproduct/rule/edit', ['rule_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Brandsmith\SuperProduct\Model\Uploader
     * @throws \Exception
     */
    protected function _getUploader($type)
    {
        return $this->_uploaderPool->getUploader($type);
    }
}
