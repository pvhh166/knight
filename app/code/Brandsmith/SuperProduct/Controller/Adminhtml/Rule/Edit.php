<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller\Adminhtml\Rule;

class Edit extends \Brandsmith\SuperProduct\Controller\Adminhtml\Rule
{
    /**
     * Initialize current Dependency Rule and set it in the registry.
     *
     * @return int
     */
    protected function _initRule()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');
        $this->_coreRegistry->register(\Brandsmith\SuperProduct\Controller\RegistryConstants::CURRENT_RULE_ID, $ruleId);

        return $ruleId;
    }

    /**
     * Edit or create Dependency Rule
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $ruleId = $this->_initRule();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_SuperProduct::superproduct_rule');
        $resultPage->getConfig()->getTitle()->prepend(__('Dependency&#x20;Rules'));
        $resultPage->addBreadcrumb(__('SuperProduct'), __('SuperProduct'));
        $resultPage->addBreadcrumb(__('Dependency&#x20;Rules'), __('Dependency&#x20;Rules'), $this->getUrl('brandsmith_superproduct/rule'));

        if ($ruleId === null) {
            $resultPage->addBreadcrumb(__('New Dependency&#x20;Rule'), __('New Dependency&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Dependency&#x20;Rule'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Dependency&#x20;Rule'), __('Edit Dependency&#x20;Rule'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_ruleRepository->getById($ruleId)->getSku()
            );
        }
        return $resultPage;
    }
}
