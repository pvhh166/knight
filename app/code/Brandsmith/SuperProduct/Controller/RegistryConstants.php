<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Controller;

class RegistryConstants
{
    /**
     * Registry key where current Base ID is stored
     * 
     * @var string
     */
    const CURRENT_BASE_ID = 'current_base_id';

    /**
     * Registry key where current Addon ID is stored
     * 
     * @var string
     */
    const CURRENT_ADDON_ID = 'current_addon_id';

    /**
     * Registry key where current Dependency Rule ID is stored
     * 
     * @var string
     */
    const CURRENT_RULE_ID = 'current_rule_id';

    /**
     * Registry key where current PNG Layer ID is stored
     * 
     * @var string
     */
    const CURRENT_PNGLAYER_ID = 'current_pnglayer_id';
}
