<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Source;

class Base implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Base repository
     * 
     * @var \Brandsmith\SuperProduct\Api\BaseRepositoryInterface
     */
    protected $_baseRepository;

    /**
     * Search Criteria Builder
     * 
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Filter Builder
     * 
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $_filterBuilder;

    /**
     * Options
     * 
     * @var array
     */
    protected $_options;

    /**
     * constructor
     * 
     * @param \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     */
    public function __construct(
        \Brandsmith\SuperProduct\Api\BaseRepositoryInterface $baseRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder
    ) {
        $this->_baseRepository        = $baseRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filterBuilder         = $filterBuilder;
    }

    /**
     * Retrieve all Bases as an option array
     *
     * @return array
     * @throws StateException
     */
    public function getAllOptions()
    {
        if (empty($this->_options)) {
            $options = [];
            $searchCriteria = $this->_searchCriteriaBuilder->create();
            $searchResults = $this->_baseRepository->getList($searchCriteria);
            foreach ($searchResults->getItems() as $base) {
                $options[] = [
                    'value' => $base->getBaseId(),
                    'label' => $base->getName(),
                ];
            }
            $this->_options = $options;
        }

        return $this->_options;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
