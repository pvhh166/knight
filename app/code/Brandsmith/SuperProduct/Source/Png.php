<?php
/**
 * Brandsmith_SuperProduct extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_SuperProduct
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\SuperProduct\Source;

class Png implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * PNG repository
     *
     * @var \Brandsmith\SuperProduct\Api\PngRepositoryInterface
     */
    protected $_pngRepository;

    /**
     * Search Criteria Builder
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Filter Builder
     *
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $_filterBuilder;

    /**
     * Options
     *
     * @var array
     */
    protected $_options;

    /**
     * constructor
     *
     * @param \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     */
    public function __construct(
        \Brandsmith\SuperProduct\Api\PngRepositoryInterface $pngRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder
    )
    {
        $this->_pngRepository = $pngRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filterBuilder = $filterBuilder;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * Retrieve all PNG as an option array
     *
     * @return array
     * @throws StateException
     */
    public function getAllOptions()
    {
        if (empty($this->_options)) {
            $options = [];
            $searchCriteria = $this->_searchCriteriaBuilder->create();
            $searchResults = $this->_pngRepository->getList($searchCriteria);
            foreach ($searchResults->getItems() as $png) {
                $options[] = [
                    'value' => $png->getPngId(),
                    'label' => $png->getSku(),
                ];
            }
            $this->_options = $options;
        }

        return $this->_options;
    }
}
