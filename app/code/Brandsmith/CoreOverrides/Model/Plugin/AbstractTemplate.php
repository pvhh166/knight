<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\CoreOverrides\Model\Plugin;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\TemplateTypesInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Store\Model\Information as StoreInformation;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;

/**
 * Plugin to remove bug in Magento\Email\Model\AbstractTemplate setForcedArea method throwing 'Area already defined exception when saving design config
 *
 * @author      Shyam <shyam@brandsmith.co.nz>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @api
 * @since 100.0.2
 */
 class AbstractTemplate
{
     /**
      * @var \Magento\Email\Model\Template\Config
      */
     protected $emailConfig;

     public function __construct(\Magento\Email\Model\Template\Config $emailConfig){
         $this->emailConfig = $emailConfig;
     }
    /**
     * @param $templateId
     * @return \Magento\Email\Model\AbstractTemplate $subject
     */
    public function aroundSetForcedArea(\Magento\Email\Model\AbstractTemplate $subject,\Closure $proceed, $templateId) // Overriding setForcedArea()
    {

        if (!$subject->getArea()) {
            $subject->setArea($this->emailConfig->getTemplateArea($templateId));
        }
        return $subject;
    }
}
