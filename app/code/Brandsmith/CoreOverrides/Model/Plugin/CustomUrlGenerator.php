<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\CoreOverrides\Model\Plugin;

/**
 * Plugin to remove bug in Magento\Email\Model\AbstractTemplate setForcedArea method throwing 'Area already defined exception when saving design config
 *
 * @author      Shyam <shyam@brandsmith.co.nz>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @api
 * @since 100.0.2
 */
class CustomUrlGenerator
{


    /**
     * @param $templateId
     * @return \Magento\Email\Model\AbstractTemplate $subject
     */
//    public function aroundGetUrlKey(\Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $subject, callable $process, \Magento\Catalog\Model\Product $product)
//    {
//        $urlKey = $product ? $product->getUrlKey() : '';
//        if ($urlKey === '' || $urlKey === null) {
//            $product->setUrlKey($product->getName() . "-" . $product->getId());
//        }
//        return $process($product);
//    }

    public function aroundPrepareProductUrlKey(\Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $subject, callable $process, \Magento\Catalog\Model\Product $product)
    {
        $urlKey = $product ? $product->getUrlKey() : '';
        if ($urlKey === '' || $urlKey === null) {
           $urlKey= $product->formatUrlKey($product->getName().'-'.$product->getSku());
            $product->setUrlKey($urlKey);
        }
        return $product->formatUrlKey($product->getName().'-'.$product->getSku());
    }
}
