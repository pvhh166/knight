<?php

namespace Brandsmith\CoreOverrides\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    protected $customerUrl;
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        \Magento\Customer\Model\Url $customerUrl,
        array $data = []

    ){

        $this->customerUrl = $customerUrl;

        parent::__construct( $context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);

    }


    public function _isLoggedIn()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Framework\App\Http\Context');
        if($customerSession->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
           return true;
        }
        else{
            return false;
        }
    }

    public function getCustomerLoginUrl() {
        return $this->customerUrl->getRegisterUrl();
    }

    public function getRegisterUrl() {
        return $this->customerUrl->getRegisterUrl();
    }

}
