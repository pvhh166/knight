<?php
namespace Brandsmith\CompanyGroupid\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.1.0', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('customer_group'),
                'company_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'default' => null,
                    'unsigned' => true,
                    'comment' => 'Comopany Id'
                ]
            );
        }

        $setup->endSetup();
	}
}
