<?php

namespace Brandsmith\CompanyGroupid\Plugin;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\GroupFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
//use Magento\Customer\Api\Data\GroupExtensionInterface;

class CompanyPlugin
{

    private $extensionFactory;

    protected $_filterBuilder;
    protected $_groupFactory;
    protected $_groupRepository;
    protected $_searchCriteriaBuilder;

    public function __construct(FilterBuilder $filterBuilder,GroupRepositoryInterface $groupRepository, SearchCriteriaBuilder $searchCriteriaBuilder, GroupFactory $groupFactory)//, GroupExtensionInterface $extensionFactory)
    {
        $this->_filterBuilder = $filterBuilder;
        $this->_groupRepository = $groupRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_groupFactory = $groupFactory;
       // $this->extensionFactory = $extensionFactory;
    }

    public function aftersetForm(
        \Magento\Customer\Block\Adminhtml\Group\Edit\Form $forma)
    {
        $form = $forma->getForm();
        $fieldset=$form->addFieldset('base1_fieldset', ['legend' => __('Knight')]);
        if ($form->getElement('id')) {

            $element = $form->getElement('id')->getValue();
            $group = $this->_groupFactory->create();
            $group->load($element);
        $shipping = $fieldset->addField('company_id',
            'text',
            [
                'name' => 'company_id',
                'label' => __('Company Id'),
                'title' => __('Company Id'),
                'class' => 'required-entry',
                'required' => true,
                'value' =>$group->getCompanyId(),
            ]);
        }
        else{
            $shipping = $fieldset->addField('company_id',
                'text',
                [
                    'name' => 'company_id',
                    'label' => __('Company Id'),
                    'title' => __('Company Id'),
                    'class' => 'required-entry',
                    'required' => true,
                    'value' =>"",
                ]);
        }
        return $form;
    } //this shows ok!

    public function afterexecute(\Magento\Customer\Controller\Adminhtml\Group\Save $save, $result)
    {
        $companyId = $save->getRequest()->getParam('company_id');
        $code = $save->getRequest()->getParam('code');
        if(empty($code))
            $code = 'NOT LOGGED IN';
        $_filter = [ $this->_filterBuilder->setField('customer_group_code')->setConditionType('eq')->setValue($code)->create() ];
        $customerGroups = $this->_groupRepository->getList($this->_searchCriteriaBuilder->addFilters($_filter)->create())->getItems();
        $customerGroup = array_shift($customerGroups);
        if($customerGroup){
            $group = $this->_groupFactory->create();
            $group->load($customerGroup->getId());
            $group->setCode($customerGroup->getCode());
            $group->setCompanyId( $companyId );
            $group->save();
        }
        return $result;
    }

//    public function beforeExecute(\Magento\Customer\Controller\Adminhtml\Group\Save $save)
//    {
//        echo 'Local before <br>';
//        //Dont know what to do
//        return $returnValue;
//    }



}
