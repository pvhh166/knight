<?php

namespace Brandsmith\CompanyGroupid\Plugin;

use Magento\Customer\Api\Data\GroupExtensionInterface;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Api\Data\GroupExtensionFactory;

class GroupExtensionLoad
{
    /**
     * @var GroupExtensionFactory
     */
    private $extensionFactory;

    /**
     * @param GroupExtensionFactory $extensionFactory
     */
    public function __construct(GroupExtensionFactory $extensionFactory)
    {
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * Loads group entity extension attributes
     *
     * @param GroupInterface $entity
     * @param GroupExtensionInterface|null $extension
     * @return GroupExtensionInterface
     */
    public function afterGetExtensionAttributes(
        GroupInterface $entity,
        GroupExtensionInterface $extension = null
    ) {
        if ($extension === null) {
            $extension = $this->extensionFactory->create();
        }

        return $extension;
    }

    public function afterGet
    (
        GroupRepositoryInterface $subject,
        \Magento\Customer\Api\Data\GroupInterface $entity
    ) {
        $ourCustomData = $this->customDataRepository->get($entity->getId());

        $extensionAttributes = $entity->getExtensionAttributes(); /** get current extension attributes from entity **/
        $extensionAttributes->setCompanyId($ourCustomData);
        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    public function afterSave
    (
        GroupRepositoryInterface $subject,
        \Magento\Customer\Api\Data\GroupInterface $entity
    ) {
        $extensionAttributes = $entity->getExtensionAttributes(); /** get current extension attributes from entity **/
        $ourCustomData = $extensionAttributes->getCompanyId();
        $this->customDataRepository->save($ourCustomData);

        return $entity;
    }

}
