<?php

namespace Brandsmith\MessageQAmqp\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Brandsmith\MessageQ\Api\Config\ConfigInterface as QueueConfig;
use Brandsmith\MessageQAmqp\Model\Client;
use Psr\Log\LoggerInterface;

class Recurring implements InstallSchemaInterface
{
    /**
     * @var QueueConfig
     */
    private $queueConfig;
    
    /**
     * @var Client
     */
    private $client;
	
	 /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param QueueConfig $queueConfig
     * @param Client $client
     */
    public function __construct(
        QueueConfig $queueConfig,
        Client $client,
		LoggerInterface $logger
    ) {
        $this->queueConfig = $queueConfig;
        $this->client = $client;
		$this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		
		$message = "inside recurring setup";
		$this->logger->notice($message);
		
        // Skip queues setup if no AMQP queue is declared
        $queues = $this->getAmqpQueues();
		$this->logger->notice(implode(", ",$queues));
        if(count($queues) == 0) {
            return;
        }
        
        // Open AMQP connection and channel
        $channel = $this->client->getChannel();
        
        // Declare queues and exchanges
        foreach($queues as $queue) {
            // Create non passive, durable non exclusive queue with no auto delete
            $channel->queue_declare($queue, false, true, false, false);
            
            // Create direct exchange, non passive, durable and with no auto delete
            $channel->exchange_declare($queue, 'direct', false, true, false);
            
            // Bind queue to exchange
            $channel->queue_bind($queue, $queue);
        }
    }
    
    /**
     * Return the AMQP queues
     * 
     * @return string[]
     */
    protected function getAmqpQueues()
    {
        $queueConfig = $this->queueConfig;
        
        return array_filter($queueConfig->getQueueNames(), function($name) use($queueConfig) {
            return $queueConfig->getQueueBroker($name) == \Brandsmith\MessageQAmqp\Model\AmqpBroker::BROKER_CODE;
        });
    }
}
