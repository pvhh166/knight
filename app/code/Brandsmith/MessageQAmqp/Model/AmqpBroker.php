<?php

namespace Brandsmith\MessageQAmqp\Model;

use PhpAmqpLib\Message\AMQPMessage;
use Brandsmith\MessageQ\Api\Data\MessageEnvelopeInterface;
use Brandsmith\MessageQ\Api\Data\MessageEnvelopeInterfaceFactory;
use Brandsmith\MessageQ\Api\Config\ConfigInterface as QueueConfig;

class AmqpBroker implements \Brandsmith\MessageQ\Api\BrokerInterface
{
    const BROKER_CODE = 'amqp';
    
    /**
     * @var Client
     */
    protected $client;
    //public $channel;
    private $queueConfig;

    /**
     * @var MessageEnvelopeInterfaceFactory
     */
    protected $messageEnvelopeFactory;
    
    /**
     * @var string
     */
    protected $queueName;
    protected $callback;
    /**
     * @param Client $client
     */
    public function __construct(
        Client $client,
        MessageEnvelopeInterfaceFactory $messageEnvelopeFactory,
        $queueName = null
        //QueueConfig $queueConfig
    ) {
        $this->client = $client;
        $this->messageEnvelopeFactory = $messageEnvelopeFactory;
        $this->queueName = $queueName;
        //$this->queueConfig = $queueConfig;
        //$this->channel = $this->client->getChannel();
        //$this->channel->basic_qos(null, 1, null);
        //$this->channel->basic_consume($this->queueName,'',false,false,false,false,array($this, 'onResponse');
    }
    
    /**
     * {@inheritdoc}
     */
    public function enqueue(MessageEnvelopeInterface $messageEnvelope)
    {
        // Open connection and get channel
        $channel = $this->client->getChannel();
        
        // Prepare message
        $message = new AMQPMessage(
            $messageEnvelope->getContent(), [
                'content_type' => $messageEnvelope->getContentType(),
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                'timestamp' => time()
            ]
        );
        
        // Send message to exchange
        $channel->basic_publish($message, $this->queueName);
    }
    
    /**
     * {@inheritdoc}
     */
    public function peek()
    {
        // Open connection and get channel
        $channel = $this->client->getChannel();
        //$this->channel = $this->client->getChannel();

        // Get message
        //$channel->basic_consume($this->queueName,false,false, false,false,false,'onResponse');
        //$this->channel->basic_consume($this->queueName,false,false, false,false,false,array($this, 'onResponse'));
        //
        $channel->basic_qos(null, 1, null);

        // Get message
        $message = $channel->basic_get($this->queueName);
        if(!$message) {
            return false;
        }
        // Return message in envelope
        return $this->messageEnvelopeFactory->create()
            ->setBrokerRef($message->delivery_info['delivery_tag'])
            //->setCorrelationId($message->delivery_info['correlation_id'])
            ->setContent($message->body);
    }

    public function onResponse($rep)
    {
        $bob = $rep-get('reply_to');
        $message = $this->messageEnvelopeFactory->create()
            ->setBrokerRef($rep->delivery_info['delivery_tag'])
            ->setContent($rep->body);
        $this->acknowledge($message);
    }
    
    /**
     * {@inheritdoc}
     */
    public function acknowledge(MessageEnvelopeInterface $message)
    {
        // Get channel
        $channel = $this->client->getChannel();
        
        // Send ACK
        $channel->basic_ack($message->getBrokerRef());
    }
    
    /**
     * {@inheritdoc}
     */
    public function reject(MessageEnvelopeInterface $message, $requeue = true)
    {
        // Get channel
        $channel = $this->client->getChannel();
        
        // Send ACK
        $channel->basic_reject($message->getBrokerRef(), $requeue);
    }
}
