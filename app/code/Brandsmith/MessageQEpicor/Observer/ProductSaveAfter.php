<?php

namespace Brandsmith\MessageQEpicor\Observer;

use Magento\Framework\Event\ObserverInterface;
use Brandsmith\MessageQ\Api\PublisherInterface;
use Psr\Log\LoggerInterface;

class ProductSaveAfter implements ObserverInterface
{
    /**
     * @var PublisherInterface
     */
    private $publisher;
	private $sqlpub;
	private $logger;
    
    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(PublisherInterface $publisher, LoggerInterface $logger) {
        $this->publisher = $publisher;
		$this->logger = $logger;
    }
    
    /**
     * {@inheritdoc}
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		
		$event = $observer->getEvent();
		$message = "firing event is --->> " . $event->getName();
		$this->logger->notice($message);

        #$product_id=$observer->getProduct()->getId();
        #$_product=Mage::getModel('catalog/product')->load($product_id);
/*        $_product=$observer->getProduct();


    $product_data["id"]=$_product->getId();
    $product_data["name"]=$_product->getName();
    $product_data["short_description"]=$_product->getShortDescription();
    $product_data["description"]=$_product->getDescription();
    $product_data["price"]=$_product->getPrice();
    $product_data["special price"]=$_product->getFinalPrice();
    $product_data["image"]=$_product->getThumbnailUrl();
    $product_data["model"]=$_product->getSku();
    $product_data["color"]=$_product->getAttributeText('color'); //get custom attribute value*/


    #$storeId = Mage::app()->getStore()->getId();
    #$summaryData = Mage::getModel('review/review_summary')->setStoreId($storeId)  ->load($_product->getId());
    #$product_data["rating"]=($summaryData['rating_summary']*5)/100;

    #$product_data["shipping"]=Mage::getStoreConfig('carriers/flatrate/price');

    #if($_product->isSalable() == 1)
    #    $product_data["in_stock"] = 1;
    #else
    #    $product_data["in_stock"] = 0;

        //$bob = $observer->getProduct()->getData();
        //$msg = json_encode($bob,JSON_UNESCAPED_SLASHES);
		$this->publisher->publish('magento.events', $message);
        //$this->publisher->publish('product.updates', $msg);
        //$this->publisher->publish('product.updates.sql', $msg);
		#$this->publisher->publish('product.updates', $observer->getProduct());
    }
}
