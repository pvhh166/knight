<?php

namespace Brandsmith\MessageQEpicor\Observer;

use Magento\Framework\Event\ObserverInterface;
use Brandsmith\MessageQ\Api\PublisherInterface;
use Psr\Log\LoggerInterface;

class Customer implements ObserverInterface
{
	private $logger;
	private $publisher;
		
	/**
     * @param PublisherInterface $publisher
     */
    public function __construct(PublisherInterface $publisher, LoggerInterface $logger) {
		$this->logger = $logger;
		$this->publisher = $publisher;
    }
	
	/**
     * {@inheritdoc}
     */
    public function execute( \Magento\Framework\Event\Observer $observer )
	{
		$event = $observer->getEvent();
	
		$message = "Customer --->> Firing event is --->> " . $event->getName();
		//$this->logger->debug($message);
		$customer = $event->debug();
        $this->logger->debug(json_encode($customer, JSON_PRETTY_PRINT));
		#echo "<pre>"; print_r($event->debug());die("dead");
		$this->publisher->publish('magento.events', $message);
		$this->publisher->publish('user.updates', $message);
	}
}