<?php

namespace Brandsmith\MessageQEpicor\Observer;

use Magento\Framework\Event\ObserverInterface;
use Brandsmith\MessageQ\Api\PublisherInterface;
use Psr\Log\LoggerInterface;

class CartItemAdd implements ObserverInterface
{
	private $logger;
	private $publisher;
		
	/**
     * @param PublisherInterface $publisher
     */
    public function __construct(PublisherInterface $publisher, LoggerInterface $logger) {
		$this->logger = $logger;
		$this->publisher = $publisher;
    }
	
	/**
     * {@inheritdoc}
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		
		$event = $observer->getEvent();
		$data = $observer->getData('items');
		$message = "CartItemAdd --->> Firing event is --->> " . $event->getName();
		$this->logger->notice($message);
		//$item = $observer->getEvent()->getQuoteItem();
		$msg = json_encode($event->getData(),JSON_UNESCAPED_SLASHES);
		$this->logger->notice("Event Data --->> " . $msg);
		
		$this->publisher->publish('magento.events', $message);
		$this->publisher->publish('cart.updates', $message);
	}
}