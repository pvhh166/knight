<?php

namespace Brandsmith\MessageQEpicor\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Brandsmith\MessageQ\Api\PublisherInterface;
use Psr\Log\LoggerInterface;

class CheckOut implements ObserverInterface
{

    private $logger;

    private $_publisher;

    private $_orderModel;

    private $event;

    public function __construct(PublisherInterface $publisher, LoggerInterface $logger, \Magento\Sales\Model\Order $orderModel, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->logger = $logger;
        $this->_publisher = $publisher;
        $this->_orderModel = $orderModel;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $this->event = $observer->getEvent();

        try {

            $orderIds = $observer->getEvent()->getOrderIds();

            $payload = [];

            if (count($orderIds)) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($orderIds[0]);
				$_product = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$customer = $this->_customerRepositoryInterface->getById($order->getCustomerId());
				$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
				$_customer = $customerFactory->load($order->getCustomerId());

                // Fetch whole order information
                $this->_publisher->publish('checkout.order', json_encode($order->getData(), JSON_PRETTY_PRINT));

				$payload['company_code'] = $this->scopeConfig->getValue('general/store_information/order_reference', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $payload['company_no'] = $this->scopeConfig->getValue('general/store_information/company_reference', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $payload['missing_sku_ref'] = $this->scopeConfig->getValue('general/store_information/missing_sku_reference', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				$payload['company_id'] = $_customer->getCompCompanyId();
                $payload['id'] = $order->getId();
                $payload['order_number'] = $order->getIncrementId();
                $payload['real_order_id'] = $order->getRealOrderId();
                $payload['customer_note'] = $this->getOrderComment($order->getStatusHistoryCollection());
				$payload['po_number'] = $order->getBoldOrderComment();
                $payload['status'] = $order->getStatus();
				$payload['date_created'] = $order->getCreatedAt();
                $payload['currency'] = $order->getOrderCurrency();
                $payload['total'] = $order->getTotalDue();
                $payload['subtotal'] = $order->getSubtotal();
				$payload['fee_lines'] = $this->getDiscountDetails($order);

                // Fetch shipping method information
                $payload['shipping_lines'] = $this->getShippingMethodDetails($order);

                $payload['line_items'] = $this->getLineItems($order->getAllItems(), $order, $_product);
                $payload['total_line_items_quantity'] = count($payload['line_items']);

                // Fetch whole billing information
                //$this->_publisher->publish('checkout.order', json_encode($order->getBillingAddress()->getData(), JSON_PRETTY_PRINT));

                //$this->_publisher->publish('checkout.order', json_encode($order->getPayment()->getData(), JSON_PRETTY_PRINT));

                // Fetch payment detail information
                $payload['payment_details'] = $this->paymentDetails($order->getPayment());

                // Fetch billing address information
                $payload['billing_address'] = $this->getShippingDetails($order->getBillingAddress());

                // Fetch shipping address information
                $this->_publisher->publish('checkout.order', json_encode($order->getShippingAddress()->getData(), JSON_PRETTY_PRINT));

                $payload['shipping_address'] = $this->getShippingDetails($order->getShippingAddress());
                $payload['home_delivery'] = $this->getHomeDeliveryDetails($order);

                $this->_publisher->publish('checkout.order', json_encode($payload, JSON_PRETTY_PRINT));
				//$this->_publisher->publish('checkout.order', json_encode($payload));
            }

            $message = "Checkout -- Firing event is --->> " . $this->event->getName();

            $this->logger->notice($message);
            //echo "<pre>"; print_r($event->debug());die("dead");
            $this->_publisher->publish('magento.events', $message);

        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }

    private function getBillingDetails($billing)
    {
        return [
            "first_name" => $billing->getFirstname(),
            "last_name" => $billing->getLastname(),
            "address1" => $billing->getStreetLine(1),
            "address2" => $billing->getStreetLine(2),
            "address3" => $billing->getStreetLine(3),
            "city" => $billing->getCity(),
            "state" => $billing->getRegion(),
            "postcode" => $billing->getPostcode(),
            "country" => $billing->getCountryId,
            "email" => $billing->getEmail(),
            "telephone" => $billing->getTelephone()
        ];
    }

    private function getShippingDetails($shipping)
    {
        return [
            "first_name" => $shipping->getFirstname(),
            "last_name" => $shipping->getLastname(),
			"company_name" => $shipping->getCompany(),
            "address1" => $shipping->getStreetline(1),
            "address2" => $shipping->getStreetline(2),
            "address3" => $shipping->getStreetLine(3),
            "city" => $shipping->getCity(),
            "state" => $shipping->getRegion(),
            "postcode" => $shipping->getPostcode(),
            "country" => $shipping->getCountryId(),
            "email" => $shipping->getEmail(),
            "telephone" => $shipping->getTelephone()
        ];
    }

	private function getShippingMethodDetails($order){
	$shipping_lines = [];
	
        $shipping_line = array(
			"type" => "shipping",
            "shipping_method" => $order->getShippingMethod(),
            "shipping_amount" => $order->getShippingAmount(),
            "shipping_description" => $order->getShippingDescription()
        );
		$shipping_lines[] = $shipping_line;
		
	return $shipping_lines;
    }

    private function paymentDetails($payment)
    {
        return [
            "payment_method" => $payment->getMethod(),
            "payment_method_title" => $payment->getAdditionalInformation('method_title'),
            "payment_po_number" => $payment->getPoNumber()
        ];
    }

    private function getLineItems($lineitems, $order, $p)
    {
		$items = array();
		
		foreach($lineitems as $item) {
			
				
			// Gets Configurable and Simple products that dont have a parent
			if (!$item->getParentItem() && $item->getProductType() != 'super') {
			        $listitem = array(
					"type" => "item",
                    "name" => $item->getName(),
                    "quantity" => $item->getQtyOrdered(),
                    "price" => $item->getPrice(),
                    "sku" => $item->getSku()
                );
                $items[] = $listitem;
			}
			
			// Gets SuperProduct Children and finds the price based on customer group.
			// Note!!! This is a hack as the product data for superproduct children isn't stored correctly in quote_items and sales_order_item tables
			if ($item->getProductType() == 'super') {
                foreach ($item->getChildrenItems() as $child) {
                    $listitem = array(
					"type" => "item",
                    "name" => $child->getName(),
                    "quantity" => $child->getQtyOrdered(),
                    "price" => $this->getChildItemPrice($child, $order, $p),
                    "sku" => $child->getSku()
					);
                $items[] = $listitem;
                }
            }
		}
		return $items;
	}
	
	function getDiscountDetails($order) {
	$discount_lines = [];
	
		if($order->getBaseDiscountAmount() != 0) {
			$fee_line = array(
                    "type" => "discount",
                    "value" => $order->getBaseDiscountAmount()
                );
                $discount_lines[] = $fee_line;
				return $discount_lines;
			}
	
		if ($order->getRewardPointsAmount() > 0) {
			$fee_line = array(
                    "type" => "discount",
                    "value" => $order->getRewardPointsAmount() * -1
                );
                $discount_lines[] = $fee_line;
				return $discount_lines;
			}
		return $discount_lines;
	}
	
	private function getOrderComment($notes) {

        if (count($notes->getData()) > 0) {
            //We have an order comment
            $items = $notes->getData();
            $comment = $items[0]['comment'];
        }
        return $comment;
    }
	
	private function getChildItemPrice($item, $order, $prod) {
		$target = [];
		
		$product = $prod->get($item->getSku());
		$allTiers = $product->getData('tier_price');
		foreach($allTiers as $tier) { 
			if($order->getCustomerGroupId() == $tier['cust_group']) {
				$target[] = $tier;
			}
		}
		if (count($target) > 0) {
			return $target[0]['price'];
		}
		return 0;
    }
    
    function getHomeDeliveryDetails($order)
    {
        if($order->getDeliveryHome()) {
            return [
                "hd" => $order->getDeliveryHome(),
                "hd_email" => $order->getDeliveryHomeEmail(),
                "hd_mobile" => $order->getDeliveryHomeMobile(),
                "hd_phone" => $order->getShippingAddress()->getTelephone()
            ];
        } else {
            return (object)[];
        }
    }

}
