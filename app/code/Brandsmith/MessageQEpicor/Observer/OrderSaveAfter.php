<?php

namespace Brandsmith\MessageQEpicor\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Brandsmith\MessageQ\Api\PublisherInterface;
use Psr\Log\LoggerInterface;

class OrderSaveAfter implements ObserverInterface
{
    private $publisher;
    private $sqlpub;
    private $logger;

    public function __construct(PublisherInterface $publisher, LoggerInterface $logger) {
        $this->publisher = $publisher;
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $event = $observer->getEvent();

        $message = "Checkout -- Firing event is --->> " . $event->getName();
        $this->logger->notice($message);
        //echo "<pre>"; print_r($event->debug());die("dead");
        $msg = json_encode($event->debug(), JSON_PRETTY_PRINT);
        $this->publisher->publish('magento.events', $message);
        $this->publisher->publish('order.updates', $msg);
    }

}