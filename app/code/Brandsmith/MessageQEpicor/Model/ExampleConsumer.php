<?php

namespace Brandsmith\MessageQEpicor\Model;

use Magento\Framework\Api\SortOrder;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteFactory;
use MageWorx\OptionFeatures\Model\Product\Option\Value\Media\Config;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface;
use MageWorx\OptionFeatures\Model\ImageFactory;
use MageWorx\OptionDependency\Model\Config as DepConfig;
use Magento\Framework\App\State as Shyam;


class ExampleConsumer implements \Brandsmith\MessageQ\Api\ConsumerInterface
{

    protected $logger;

    protected $filesystem;

    protected $publisher;

    protected $resultRawFactory;

    protected $mediaConfig;

    protected $directoryWriteFactory;

    protected $file;

    protected $directoryList;

    protected $depends;

    protected $_productRepository;

    protected $_imagemodel;

    protected $_dependancymodel;

    protected $_objectManager;

    protected $_product;

    private $state;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Brandsmith\MessageQ\Api\PublisherInterface $publisher,
        Shyam $state,
        ImageFactory $_imagemodel,
        DepConfig $_dependancyconfig,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $fileSystem,
        RawFactory $resultRawFactory,
        Config $mediaConfig,
        Filesystem $filesystem,
        WriteFactory $directoryWriteFactory,
        DirectoryList $directoryList,
        File $file
    )
    {
        $this->publisher = $publisher;
        $this->state = $state;
        $this->_imagemodel = $_imagemodel;
        $this->_dependancyconfig = $_dependancyconfig;
        $this->_productRepository = $productRepository;
        $this->logger = $logger;
        $this->_filesystem = $fileSystem;
        $this->resultRawFactory = $resultRawFactory;
        $this->mediaConfig = $mediaConfig;
        $this->filesystem = $filesystem;
        $this->directoryWriteFactory = $directoryWriteFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
    }

    /**
     * {@inheritdoc}
     */
    public function process($message)
    {

        //print_r('object hash ---> ' . spl_object_hash ( $this ) . '\n');
		
        try {
            $this->state->setAreaCode('adminhtml');
        } catch (\Exception $e){
			print_r('wheels fell off --->>> ' . $e);
		}

        try {
            if (!$message["product"]["option"]) ;
        } catch (\Exception $e) {
            throw new \Exception('Must Supply option data' . json_encode($message));
        }

        $this->_objectManager = ObjectManager::getInstance();

        $productId = $message["product"]["id"];

        $this->_product = $this->getProductBySku($productId);

        if (!empty($message["product"]["option"]["images"])) {
            $filename = $this->saveImageFile($message["product"]["option"]["images"]);
        }

        //$valParents = $this->_dependancyconfig->getValueParents(9377);
        //$valChild = $this->_dependancyconfig->getValueChildren(9377);


        if (isset($filename)) {
            //Set option/value images
            $this->processValueImages($filename, $this->_product->getOptions(), $message["product"]["option"]['title'], $message["product"]["option"]['value']);
        }

        $this->buildDependancy($message['product']['option']['dependency'], [$message['product']['option']['title'], $message["product"]["option"]['value']], $this->_product->getOptions());


        //$this->logger->info('Product update processed: ' . json_encode($message));
        $this->publisher->publish('magento.events', 'Product update processed: ' . json_encode($message));
    }

    private function buildDependancy($args, $option, $options)
    {

        $childoptions = $this->getOptionValueIds($options, $option);
        $parentoptions = [];

        foreach ($args['parents'] as $arg) {
            $result = $this->getParentValueIds($options, [$arg['key'], $arg['value']]);
            if ($result) {
                array_push($parentoptions, $result);
            }
        }

        $this->processDependencys($parentoptions, $childoptions);

    }

    private function processDependencys($a, $b)
    {
        $result = [];

        foreach ($a as $aa) {
            $result[] = [
                'child_option_id' => $b['child_option_id'],
                'child_option_type_id' => $b['child_option_type_id'],
                'parent_option_id' => $aa['parent_option_id'],
                'parent_option_type_id' => $aa['parent_option_type_id'],
                'product_id' => $this->_product->getId()
            ];
        }

        foreach ($result as $dep)
        {
            sleep(1);
            //var_dump(json_encode($dep));
            //$fff = $this->_dependancyconfig->setData($dep);
            $this->_dependancyconfig->setData($dep);
            $this->_dependancyconfig->save();
            sleep(1);
        }

    }

    private function getParentValueIds($options, $data)
    {
        $result = [];
        foreach ($options as $option) {
            if ($option->getTitle() === $data[0]) {
                $result['parent_option_id'] = $option->getMageworxOptionId();
                foreach ($option->getValues() as $value) {
                    if ($value->getTitle() === $data[1]) {
                        $result['parent_option_type_id'] = $value->getMageworxOptionTypeId();
                    }
                }
            }
        }
        return $result;
    }

    private function getOptionValueIds($options, $data)
    {
        $result = [];
        foreach ($options as $option) {
            if ($option->getTitle() === $data[0]) {
                $result['child_option_id'] = $option->getMageworxOptionId();
                foreach ($option->getValues() as $value) {
                    if ($value->getTitle() === $data[1]) {
                        $result['child_option_type_id'] = $value->getMageworxOptionTypeId();
                        $value->setDependencyType(1);
                        $value->save();
                    }
                }
            }
        }
        return $result;
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    private function saveImageFile($payload)
    {
        $data = [];
        foreach ($payload as $image) {
            if (empty($image)) {
                continue;
            }
            $name = basename($image['image']);
            $path = $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'tmp/';
            $result = $this->file->read($image['image'], $path . $name);

            if ($result) {
                $op = $this->uploadImageFile($name);
                $op['sortorder'] = $image['sortorder'];
                $op['baseimage'] = $image['baseimage'];
                $op['replace'] = $image['replace'];
                array_push($data, $op);
            }
        }
        return $data;
    }

    private function uploadImageFile($filename)
    {
        $data = [
            'name' => $filename,
            'type' => 'image/jpeg',
            'tmp_name' => $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'tmp/' . $filename,
            'error' => 0,
        ];

        $uploader = $this->_objectManager->create(
            'Magento\MediaStorage\Model\File\Uploader', ['fileId' => $data]);
        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
        $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
        $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(true);
        /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
            ->getDirectoryRead(DirectoryList::MEDIA);
        $result = $uploader->save($mediaDirectory->getAbsolutePath($this->mediaConfig->getBaseMediaPath()));
        return $result;
    }

    private function processValueImages($payload, $options, $otitle, $vtitle)
    {
        $iModel = $this->_imagemodel->create();

        $deps = [];

        foreach ($options as $option) {
            if ($option->getTitle() === $otitle)
                var_dump('option match - process values next looking for value title');
            var_dump($this->_dependancyconfig->getChildOptionId());
            // if option type dropdown and swatch
            $option->getType();
            $values = $option->getValues();
            foreach ($values as $value) {
                $deps[][$value->getTitle()] = $value->getMageworxOptionTypeId();
                if ($value->getTitle() == $vtitle) {
                    var_dump('value match - process images');
                    $valueimage = $iModel->getCollection()->addFieldToFilter('mageworx_option_type_id', ['eq' => $value->getMageworxOptionTypeId()]);
                    $valueimage->walk('delete');
                    foreach ($payload as $data) {
                        $object = $valueimage->getNewEmptyItem();
                        $object->setValue($data["file"]);
                        $object->setSortOrder($data['sortorder']);
                        $object->setBaseImage($data['baseimage']); //If Option Type Dropdown and Swatch this becomes the swatch image if value is 1
                        $object->setColor(0);
                        $object->setReplaceMainGalleryImage($data['replace']);
                        $object->setMageworxOptionTypeId($value->getMageworxOptionTypeId()); //Sets the
                        $object->save();
                    }
                }
            }
        }
    }
}
