<?php

namespace Brandsmith\MessageQEpicor\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class StockUpdateConsumer implements \Brandsmith\MessageQ\Api\ConsumerInterface
{

    protected $publisher;

    public function __construct(
        \Brandsmith\MessageQ\Api\PublisherInterface $publisher
    ){
        $this->publisher = $publisher;
    }


    public function process($message)
    {
        // TODO: Implement process() method.
    }
}