<?php

namespace Brandsmith\MessageQEpicor\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Sales\Model\Order;

class ProductConsumer implements \Brandsmith\MessageQ\Api\ConsumerInterface
{
    protected $logger;

    protected $_productRepository;

    protected $_productFactory;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\State $appState
    ) {
        $this->logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_productFactory = $productFactory;
        $appState->setAreaCode('global');
    }

    public function process($message)
    {

        $product = $this->_productRepository->getById(1);

        $new = $this->_productFactory->create();
        $new->setName("New Product");
        $new->setSku("abc1234");
        $new->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
        $new->setVisibility(4); //4 Means ???
        $new->setPrice(123.00);
        $new->setAttributeSetId(4); //4 is the default attribute set for products
        $new->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        // If Desired, you can set a tax class like so:
        // $new->setCustomAttribute('tax_class_id', $taxClassId);

        $new = $this->_productRepository->save($new);

        var_dump("A New Product .. " . json_encode($new->getData()));
        var_dump("Product Description .. " . $product['description']);
        //var_dump("This is the message.. " . $message);
        //if(array_key_exists('gets',$message)){var_dump("contains gets");}

    }

    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    public function getProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }
}