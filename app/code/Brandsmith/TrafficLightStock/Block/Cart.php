<?php
namespace Brandsmith\TrafficLightStock\Block;
class Cart extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	protected $_registry;
	protected $_helper;

	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\Registry $registry,
		\Brandsmith\TrafficLightStock\Helper\Data $helper,
		\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
		$this->_customerSession = $customerSession;
		$this->_registry = $registry;
		$this->_helper = $helper;
	}

    public function getCustomerId(){
        return $this->_customerSession->getCustomer()->getId(); //Print current customer ID
    }

	public function getCurrentProduct(){
        return $this->getProduct() ? $this->getProduct() : $this->_registry->registry('current_product'); 
	}

	public function getCurrentProductId(){
        return $this->getProduct() ? $this->getProduct()->getId() : $this->getCurrentProduct()->getId(); 
	}
	
	public function getPopupMessege() {
		return $this->_helper->getCartWarning();
	}

	public function getStoreUrl() {
		return $this->_helper->getStoreUrl();
	}

	public function getWarningCart(){
		return $this->_helper->getCartNotice();
	}

	public function isEnableWarningCart(){
		return $this->_helper->isEnableWarningCart();
	}

	public function cartTimeLimit(){
		return $this->_helper->getCartTimeLimit();
	}
}