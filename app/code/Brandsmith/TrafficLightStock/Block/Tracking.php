<?php
namespace Brandsmith\TrafficLightStock\Block;
class Tracking extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	protected $_helper;

	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Brandsmith\TrafficLightStock\Helper\Data $helper,
		\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
		$this->_customerSession = $customerSession;
		$this->_helper = $helper;
	}

    public function getCustomerId(){
        return $this->_customerSession->getCustomer()->getId(); //Print current customer ID
	}

	public function isLoggedIn(){
        return $this->_customerSession->isLoggedIn(); 
	}
	
	public function getStatusNotice(){
        return $this->_helper->getStatusNotice(); 
	}

	public function isActiveTrafficLight() {
		return $this->_helper->isActiveTrafficLight(); 
	}
}