<?php
namespace Brandsmith\TrafficLightStock\Block;
class Display extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	protected $_registry;
	protected $_helper;

	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\Registry $registry,
		\Brandsmith\TrafficLightStock\Helper\Data $helper,
		\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
		$this->_customerSession = $customerSession;
		$this->_registry = $registry;
		$this->_helper = $helper;
	}

    public function getCustomerId(){
        return $this->_customerSession->getCustomer()->getId(); //Print current customer ID
    }

	public function getCurrentProduct(){
        return $this->getProduct() ? $this->getProduct() : $this->_registry->registry('current_product'); 
	}

	public function getCurrentProductId(){
        return $this->getProduct() ? $this->getProduct()->getId() : $this->getCurrentProduct()->getId(); 
	}
	
	public function getGauge($productId,$configId){
		if ($this->getCurrentProduct()->getTypeId() == 'simple')
		{
			$gaugeStock = $this->_helper->getGauge($productId,$configId);
			$gaugeStock = $gaugeStock ? $gaugeStock : 'none';
			return $gaugeStock; 
		}
		else
			return 'none';
	}

	public function getChannel(){
		$product = $this->getCurrentProduct();
		$productId = $product->getId();
		$customerId = $this->getCustomerId();
		$channel = [];
		if ($product->getTypeId() == 'configurable'){
			$configChild = $product->getTypeInstance()->getUsedProductIds($product);
			$getChildId = array();
            foreach ($configChild as $child){
                $channel[$child] = "customer-$customerId-product-$child";
            }
		}
		elseif ($product->getTypeId() == 'simple'){
			$channel[$productId] = "customer-$customerId-product-$productId"; 
		}
		return $channel;
	}

	public function isActiveTrafficLight() {
		return $this->_helper->isActiveTrafficLight();
	}

	public function getPopupMessege() {
		return $this->_helper->getCartWarning();
	}

	public function getStoreUrl() {
		return $this->_helper->getStoreUrl();
	}

	public function getGaugeLowNote() {
		return $this->_helper->getGaugeLowNote();
	}

	public function getGaugeMediumNote() {
		return $this->_helper->getGaugeMediumNote();
	}

	public function getGaugeHighNote() {
		return $this->_helper->getGaugeHighNote();
	}
}