<?php
namespace Brandsmith\TrafficLightStock\Controller\Load;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * helper Traffic Light Stock
     *
     * @var \Brandsmith\TrafficLightStock\Helper\Data
     */
    protected $_helperTraffic;

    /**
     * Upload constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ImageUploader $imageUploader
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\TrafficLightStock\Helper\Data $helperTraffic
    ) {
        parent::__construct($context);
        $this->_helperTraffic = $helperTraffic;
    }


    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $_param = $this->getRequest()->getParams();
        $simple_product_id = isset($_param['simple_product_id']) ? $_param['simple_product_id'] : false;
        $config_product_id = isset($_param['config_product_id']) ? $_param['config_product_id'] : false;      
        if ($simple_product_id && $config_product_id) $this->_helperTraffic->triggerToCustomer($simple_product_id,$config_product_id);
        return $this->resultFactory->create(ResultFactory::TYPE_JSON);
    }

    public function getGauge($product_id){
		$gaugeStock = $this->_helperTraffic->getGauge($product_id);
		$gaugeStock = $gaugeStock ? $gaugeStock : 'none';
        return $gaugeStock; 
    }
    
}