/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'underscore',
    'pusher',
], function ($, customerData, _, Pusher) {
    var cartData = customerData.get('cart');    
    // var pusherTracking = new Pusher('0838c334fcd4ae230387', {
    //     cluster: 'ap1'
    // });           
    var pusherTracking = new Pusher('lotus', {
        'wsHost': 'pusher.knightgroup.co.nz',
        'wssPort': '8443',
        'secret': 'admin@123',
        'appId': 'knight'
    });      
    var channelTracking = pusherTracking.subscribe('traffic-light-stock');
    function pusherListen(data) {     
        if (cartData().summary_count > 0) {
            $.each( cartData().items , function( key, item ) {
                switch(item.product_type){
                    case 'configurable':   
                    	if (item.gauge_last != item.gauge_first && item.gauge_last != false) {
                    		stockStatusChanged(item.product_sku,data.status_notice);
                    	}                 
                        channelTracking.bind('customer-'+data.customer_id+'-product-'+item.product_simple_id, function(response) {
                            if (response.gauge != item.gauge_first) {
                                stockStatusChanged(item.product_sku,data.status_notice);
                            } 
                        });
                        break;
                    case 'simple':
                    	if (item.gauge_last != item.gauge_first && item.gauge_last != false) {
                    		stockStatusChanged(item.product_sku,data.status_notice);
                        } 
                        
                        channelTracking.bind('customer-'+ data.customer_id + '-product-'+item.product_id, function(response) {
                           
                            if (response.gauge != item.gauge_first) {
                                stockStatusChanged(item.product_sku,data.status_notice);
                            }                            
                        });
                        break;
                    case 'super':
                    	if (item.options.length >0) {
                    		$.each( item.options, function( i, option ) {
	                        	if (option.gauge_last != option.gauge_first && option.gauge_last != false) {
	                                stockStatusChanged(option.product_simple_sku,data.status_notice);
	                            }
	                            channelTracking.bind('customer-'+ data.customer_id + '-product-'+option.product_simple_id, function(response) {
	                            	if (response.gauge != option.gauge_first) {
		                                stockStatusChanged(option.product_simple_sku,data.status_notice);
		                            }    
	                            });

	                        });
                    	}else{
                    		if (item.gauge_last != item.gauge_first && item.gauge_last != false) {
                                stockStatusChanged(item.product_sku,data.status_notice);
                            }
                            channelTracking.bind('customer-'+ data.customer_id + '-product-'+item.product_id, function(response) {
                            	if (response.gauge != item.gauge_first) {
	                                stockStatusChanged(item.product_sku,data.status_notice);
	                            }    
                            });
                    	}
                       
                        break;
                }
            });
        }  
    }
    function stockStatusChanged(sku,status_notice) {    
        setTimeout(function() {
            $('.alert-traffic-light .content').html(_.unescape(status_notice));   
            //update minicart 
            $('.minicart-items-wrapper li[sku="' + sku +'"]').find('.alert-traffic-light').removeClass('hide');
            $('.minicart-items-wrapper .product.options .p-item[sku="' + sku +'"]').find('.alert-traffic-light').removeClass('hide');

            //update cart page
            $('#shopping-cart-table .cartItem[sku="' + sku +'"]').find('.alert-traffic-light').removeClass('hide');
            $('#shopping-cart-table .product-child[sku="' + sku +'"]').find('.alert-traffic-light').removeClass('hide');

            //update checkout sumary cart       
            setTimeout(function() {
                $('.alert-traffic-light .content').html(_.unescape(status_notice));   
            	$('.minicart-items-wrapper .product-item-details[sku="' + sku +'"]').find('.alert-traffic-light.summary-checkout-item').removeClass('hide');
            	$('.minicart-items-wrapper .product.options.super .p-item[sku="' + sku +'"]').find('.alert-traffic-light.summary-checkout-item-super-product').removeClass('hide');
            }, 10000);
            
        },3000);        
    }
    return  function (data) {
        cartData.subscribe(function () {
            pusherListen({
                'customer_id': data.options.customer_id,
                'status_notice': data.options.status_notice
            });
        });       
    };
    
});