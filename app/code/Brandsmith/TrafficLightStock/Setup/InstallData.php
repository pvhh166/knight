<?php
 
namespace Brandsmith\TrafficLightStock\Setup;
 
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
 
class InstallData implements InstallDataInterface
{
 
    private $customerSetupFactory;
 
    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }
 
    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
 
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'sis_enable', [
            'type' => 'int', // type of attribute
            'label' => 'SIS Enable',
            'input' => 'boolean', // input type
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 21, // position of attribute
            'system' => false,
            'backend' => '',
            'default' => 1,
            'formElement' => 'checkbox',
            'valueMap' => [
                'true' => 1,
                'false' => 0
            ]
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'sis_enable')
        ->addData(['used_in_forms' => [
                'adminhtml_customer'
            ]
        ]);
        $attribute->save();

        $setup->endSetup();
    }
}