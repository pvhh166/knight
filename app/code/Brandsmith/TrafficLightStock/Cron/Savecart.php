<?php
namespace Brandsmith\TrafficLightStock\Cron;

use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Checkout\Model\Cart;

class Savecart
{
	/**
     * helper Traffic Light Stock
     *
     * @var \Brandsmith\TrafficLightStock\Helper\Data
     */
    protected $_helperTraffic;
    protected $quoteRepository;
    protected $serializer;
    protected $quoteFactory;
    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $cart;
    protected $date;
    protected $quoteItem;

	public function __construct(
		\Brandsmith\TrafficLightStock\Helper\Data $helperTraffic,
		\Magento\Checkout\Model\Session $_checkoutSession,
		\Magento\Quote\Model\QuoteRepository $quoteRepository,
		\Magento\Framework\Serialize\Serializer\Json $serializer = null,
		\Magento\Quote\Model\QuoteFactory $quoteFactory,
		SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        Cart $cart,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Quote\Model\Quote\Item $quoteItem
	) {
	    $this->_helperTraffic = $helperTraffic;
	    $this->_checkoutSession = $_checkoutSession;
	    $this->quoteRepository = $quoteRepository;
	    $this->serializer = $serializer;
	    $this->quoteFactory = $quoteFactory;
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
	    $this->cart = $cart;
        $this->date = $date;
        $this->quoteItem = $quoteItem;
  	}

	public function execute()
	{
        $limit = $this->_helperTraffic->getCartTimeLimit();
        $is_enable = $this->_helperTraffic->isEnabled();

        if($limit > 0 && $is_enable == 1){
            $quotelist = $this->getQuoteActiveTime($limit);

            if(is_array($quotelist) && count($quotelist) > 0){
                foreach ($quotelist as $quoteId) {
                    $quote = $this->quoteFactory->create()->load($quoteId);
                    $customer_id = $quote->getCustomerId();
                    $cartname = "Automatic Save";

                    $items = $quote->getAllVisibleItems();
                    if ($customer_id != null) {
                        $savecartmodel = $this->modelsavecart->create()
                                              ->setCartName($cartname)
                                              ->setCustomerId($customer_id)
                                              ->setCreatedAt(date('Y-m-d H:i:s'))
                                              ->save();
                        $saveCartId = $savecartmodel->getId();

                        foreach ($items as $item) 
                        {
                            foreach ($item->getOptions() as $option) {
                                $itemOptions = $this->serializer->unserialize($option['value']);
                               
                                if(is_array($itemOptions)) {
                                    $itemOptions['product_simple_sku'] = $item->getProduct()->getSku();
                                    if($item->getProduct()->getTypeId() == 'super') {
                                        $addOptions = $item->getOptionByCode('super_custom_option');
                                        if ($addOptions) {
                                            $options = $this->serializer->unserialize($addOptions->getValue());
                                            $itemOptions['super_options'] = $options;
                                        }
                                    }
                                }

                                $savecartdetailmodel = $this->modelsavecartdetail->create()
                                                            ->setSavecartId($saveCartId)
                                                            ->setQuotePrdId($item->getProductId())
                                                            ->setQuotePrdQty($item->getQty())
                                                            ->setQuotePrdPrice($item->getPrice())
                                                            ->setQuoteConfigPrdData(serialize($itemOptions))
                                                            ->save();
                                break;
                            }
                        }
                    }

                    $this->cart->truncate();
                    $allitems = $quote->getAllItems();

                    foreach ($allitems as $itemquote)
                    {
                        if($savecartdetailmodel->getId()){
                            $itemId = $itemquote->getItemId();
                            $quote->removeItem($itemId)->save();
                        }
                    }

                    $quotes = $this->quoteRepository->get($quote->getId());
                    $quotes->setData('is_active', 0);
                    $quotes->setData('items_count', 0);
                    $this->quoteRepository->save($quotes);

                    if ($customer_id != null) {
                        $this->_helperTraffic->triggerToCart($customer_id);
                    }
                }
            }
        }
	}
	
	private function getQuoteActiveTime($limittime) {
        $storeId = $this->_helperTraffic->getStoreId();
        $query = 'SELECT entity_id FROM quote WHERE quote.is_active = 1 AND quote.store_id = '.$storeId.' AND quote.items_count > 0 AND quote.customer_id IS NOT NULL AND quote.updated_at <= NOW() - INTERVAL '.$limittime.' MINUTE';
        
        $quoteCollection = $this->_helperTraffic->getConnection()->fetchAll($query);
        $result = [];
        

        if (count($quoteCollection) > 0) {
            foreach ($quoteCollection as $data) {
                array_push($result, $data['entity_id']);
            }
        }
        
        return $result;
    }
}