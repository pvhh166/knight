<?php
namespace Brandsmith\TrafficLightStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CataloginventoryUpdate implements ObserverInterface
{
    protected $storeManager;
    protected $helper;
    protected $quoteItemFactory;
    protected $catalogProductTypeConfigurable;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Brandsmith\TrafficLightStock\Helper\Data $helper
    ){
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getEvent()->getData('item');         
        
        // run for simple product
        $customer = $this->helper->getListCustomer();
        if (count($customer) > 0){
            foreach ($customer as $customerId) {
                $this->helper->triggerToCustomer($item->getProductId(),false,$customerId);
            }
        }

        // run for config product
        // $parentByChild = $this->catalogProductTypeConfigurable->getParentIdsByChild($item->getProductId());
        // if (count($parentByChild) > 0){
        //     foreach ($parentByChild as $configId) {
        //         foreach ($customer as $customerId) {
        //             $this->helper->triggerToCustomer($item->getProductId(),$configId,$customerId);
        //         }
        //     }
        // }
    }
}