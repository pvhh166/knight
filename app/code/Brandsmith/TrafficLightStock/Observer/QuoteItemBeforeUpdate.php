<?php
namespace Brandsmith\TrafficLightStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class QuoteItemBeforeUpdate implements ObserverInterface
{
    protected $storeManager;
    protected $helper;
    protected $quoteItemFactory;
    protected $productRepositoryFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Brandsmith\TrafficLightStock\Helper\Data $helper,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory
    ){
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->productRepositoryFactory = $productRepositoryFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getEvent()->getData('item');         
        $quoteCustomerId = $observer->getItem()->getQuote()->getCustomerId(); 
        $product = $this->productRepositoryFactory->create()->get($observer->getItem()->getSku());    
        switch($observer->getItem()->getProductType()){
            case 'configurable':                
                $observer->getItem()->setDescription($this->helper->getGauge($product->getId(), $observer->getItem()->getProductId(),$quoteCustomerId));
                break;
            case 'simple':                
                $observer->getItem()->setDescription($this->helper->getGauge($product->getId(),false,$quoteCustomerId));
                break;
            case  'super':
                break;
            
            default;
        }   
    }
}