<?php
namespace Brandsmith\TrafficLightStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class QuoteItemUpdate implements ObserverInterface
{
    protected $storeManager;
    protected $helper;
    protected $quoteItemFactory;
    protected $configurableProduct;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Brandsmith\TrafficLightStock\Helper\Data $helper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable
    ){
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->configurableProduct = $configurable;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getEvent()->getData('item');         
        $customer = $this->helper->getListCustomer();

        if ($item->getHasChildren()) {
            foreach ($item->getChildren() as $child) {
                if (count($customer) > 0){
                    foreach ($customer as $customerId) {
                        $this->helper->triggerToCustomer($child->getProductId(),false,$customerId);
                    }
                }
            }
        }

        $quoteCustomerId = $observer->getItem()->getQuote()->getCustomerId();          

        if (count($customer) > 0){
            foreach ($customer as $customerId) {
                if ($item->getParentItemId()){
                    $configQuoteItem = $this->quoteItemFactory->create()->load($item->getParentItemId());
                    /* tracking for super product */
                    $this->helper->triggerToCustomer($item->getProductId(),false,$customerId);
                    /* end tracking for super product */
                    
                    // $this->helper->triggerToCustomer($item->getProductId(),$configQuoteItem->getProductId(),$customerId);

                } else
                    $this->helper->triggerToCustomer($item->getProductId(),false,$customerId);
            }
        }
    }
}