<?php
namespace Brandsmith\TrafficLightStock\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Api\DataObjectHelper;
class CustomerSaveSIS implements ObserverInterface
{
    protected $customerRepositoryInterface;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
    * @param \Magento\Framework\ObjectManagerInterface $objectManager
    */
    public function __construct(
        DataObjectHelper $dataObjectHelper
    ){
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
    * @param \Magento\Framework\Event\Observer $observer
    * @return void
    */
    public function execute(EventObserver $observer)
    {
        $customer =  $observer->getEvent()->getCustomer();
        $request =  $observer->getEvent()->getRequest();
      
        if(isset($request->getPost('traffic_light')['sis_enable'])){
            
            $this->dataObjectHelper->populateWithArray(
                $customer,
                [
                    'sis_enable' => $request->getPost('traffic_light')['sis_enable']
                ],
                \Magento\Customer\Api\Data\CustomerInterface::class
            );
        }
        

        
        return $this;
    }
}