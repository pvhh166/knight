<?php
namespace Brandsmith\TrafficLightStock\Model\Customer;

class DataProvider 
{
    public function afterGetData(\Magento\Customer\Model\Customer\DataProvider $subject, $result)
    {
        if($result){
            $customer_id = key($result);
            if(isset( $result[$customer_id]['customer']['sis_enable'])){
                $result[$customer_id]['traffic_light']['sis_enable'] =  $result[$customer_id]['customer']['sis_enable'];
            }
            
        }
        return $result;
    }

}