<?php
namespace Brandsmith\TrafficLightStock\Plugin\Checkout\CustomerData;
 
class Cart {

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    protected $_productRepositoryFactory;

    protected $quoteItemFactory;

    protected $sessionCustomer;

    protected $helpeTrafficLightStock;

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Magento\Customer\Model\Session $sessionCustomer,
        \Brandsmith\TrafficLightStock\Helper\Data $helpeTrafficLightStock

    ) {
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->sessionCustomer = $sessionCustomer;
        $this->helpeTrafficLightStock = $helpeTrafficLightStock;
    }

    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, array $result)
    {
        //$result['extra_data'] = 'dfdfdf';
        if($result['items']){
            foreach($result['items'] as $key => $item){
                $quoteItemFactory = $this->quoteItemFactory->create()->load($item['item_id']);
                $result['items'][$key]['gauge_first'] = $quoteItemFactory->getDescription();

                if($item['product_type'] == "configurable"){
                    $product = $this->_productRepositoryFactory->create()->get($item['product_sku']);
                    $result['items'][$key]['product_simple_id'] = $product->getId();
                    if ($this->sessionCustomer->getId()) {
                        $gaugeLast = $this->helpeTrafficLightStock->getGauge($product->getId(),$item['product_id'],$this->sessionCustomer->getId());
                        $result['items'][$key]['gauge_last'] = $gaugeLast;
                    }                    
                }elseif($item['product_type'] == "simple"){                   
                    if ($this->sessionCustomer->getId()) {
                        $gaugeLast = $this->helpeTrafficLightStock->getGauge($item['product_id'],false,$this->sessionCustomer->getId());
                        $result['items'][$key]['gauge_last'] = $gaugeLast;
                    }
                }else{
                   if ($this->sessionCustomer->getId()) {
                        $gaugeLast = $this->helpeTrafficLightStock->getGauge($item['product_id']);
                        $result['items'][$key]['gauge_last'] = $gaugeLast;
                    }
                }
            }
        }       
        return $result;
    }
}