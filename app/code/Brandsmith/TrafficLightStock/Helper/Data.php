<?php
namespace Brandsmith\TrafficLightStock\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Cart;


class Data extends AbstractHelper
{
    const CONFIG_PATH_ENABLE_GAUGE = 'brandsmith_traffic_light/traffic_light/enabled';
    const CONFIG_PATH_LOW_THRESHOLD = 'brandsmith_traffic_light/traffic_light/low_threshold';
    const CONFIG_PATH_MOD_THRESHOLD = 'brandsmith_traffic_light/traffic_light/moderate_threshold';
    const CONFIG_PATH_CART_TIMELIMIT ='brandsmith_traffic_light/traffic_light/cart_time_limit';
    const CONFIG_PATH_STATUS_NOTICE ='brandsmith_traffic_light/traffic_light/status_notice';
    const CONFIG_PATH_CART_WARNING ='brandsmith_traffic_light/traffic_light/cart_warning';
    const CONFIG_PATH_CART_NOTICE ='brandsmith_traffic_light/traffic_light/saved_cart_notice';
    const CONFIG_PATH_STATUS_CART_NOTICE ='brandsmith_traffic_light/traffic_light/enable_saved_cart';
    const CONFIG_PATH_GAUGE_LOW_NOTICE = 'brandsmith_traffic_light/traffic_light/gauge_low_notice';
    const CONFIG_PATH_GAUGE_MEDIUM_NOTICE = 'brandsmith_traffic_light/traffic_light/gauge_medium_notice';
    const CONFIG_PATH_GAUGE_HIGH_NOTICE = 'brandsmith_traffic_light/traffic_light/gauge_high_notice';
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    protected $_productRepository;

    protected $_resource;

    protected $connection;

    protected $_customerSession;

    protected $_nowCustomerColection;

    /**
     * @var SerializerInterface
     */
    private $serializer;
    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $cartsession;
    protected $_modelCart;
    protected $date;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\ResourceModel\Online\Grid\Collection $nowCustomerColection,
        SerializerInterface $serializer,
        SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        Session $session,
        Cart $modelCart,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        array $data = []
    ) {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_resource = $resource;
        $this->_customerSession = $customerSession;
        $this->_nowCustomerColection = $nowCustomerColection;
        $this->serializer = $serializer;
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->cartsession = $session;
        $this->_modelCart = $modelCart;
        $this->quoteRepository = $quoteRepository;
        $this->date = $date;
    }

    public function isEnabled(){
        return $this->getStoreConfig(self::CONFIG_PATH_ENABLE_GAUGE);
    }

    public function getLow(){
        return $this->getStoreConfig(self::CONFIG_PATH_LOW_THRESHOLD);
    }

    public function getMod(){
        return $this->getStoreConfig(self::CONFIG_PATH_MOD_THRESHOLD);
    }

    public function getCartTimeLimit(){
        return $this->getStoreConfig(self::CONFIG_PATH_CART_TIMELIMIT);
    }

    public function getStatusNotice(){
        return $this->getStoreConfig(self::CONFIG_PATH_STATUS_NOTICE);
    }

    public function getCartWarning(){
        return $this->getStoreConfig(self::CONFIG_PATH_CART_WARNING);
    }

    public function getCartNotice(){
        return $this->getStoreConfig(self::CONFIG_PATH_CART_NOTICE);
    }

    public function isEnableWarningCart(){
        return $this->getStoreConfig(self::CONFIG_PATH_STATUS_CART_NOTICE);
    }

    public function getGaugeLowNote(){
        return $this->getStoreConfig(self::CONFIG_PATH_GAUGE_LOW_NOTICE);
    }

    public function getGaugeMediumNote(){
        return $this->getStoreConfig(self::CONFIG_PATH_GAUGE_MEDIUM_NOTICE);
    }

    public function getGaugeHighNote(){
        return $this->getStoreConfig(self::CONFIG_PATH_GAUGE_HIGH_NOTICE);
    }
        /**
     * Get Store Id.
     *
     * @return string
     */
    public function getStoreId(){
        return $this->_storeManager->getStore()->getStoreId();
    }

    public function getStoreUrl(){
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * Get Config.
     *
     * @return mixed
     */
    public function getStoreConfig($path){
        return $this->_scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->getStoreId());
    }


    public function getConnection(){
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection('core_write');
        }
        return $this->connection;
    }

    private function getCustomerId(){
        return $this->_customerSession->getCustomer()->getId(); //Print current customer ID
    }

    private function getRealStock($productId,$customerId){
        if ($customerId == NULL)
            return 0;
        // calculate simple product
        $query = "SELECT SUM(quote_item.qty) as totalQty FROM quote INNER JOIN quote_item ON quote_item.quote_id = quote.entity_id 
                    WHERE quote.is_active = 1 AND quote_item.parent_item_id is NULL AND quote.customer_id <> $customerId AND quote_item.product_id = $productId";
        $result = $this->getConnection()->fetchRow($query);
        $stock = (int)$result['totalQty'];

        // calculate config product
        $query = "SELECT SUM(qa.qty) as totalQty FROM quote 
                    INNER JOIN quote_item as qi ON qi.quote_id = quote.entity_id 
                    JOIN quote_item as qa ON qi.parent_item_id = qa.item_id AND qi.sku = qa.sku
                    WHERE quote.is_active = 1 AND quote.customer_id <> $customerId AND qi.product_id = $productId";
        $result = $this->getConnection()->fetchRow($query);
        $stock = $stock + (int)$result['totalQty'];

        // calculate super product
        $query = "SELECT SUM(qi.qty) as totalQty FROM quote 
                    INNER JOIN quote_item as qi ON qi.quote_id = quote.entity_id 
                    JOIN quote_item as qa ON qi.parent_item_id = qa.item_id AND qi.sku <> qa.sku
                    WHERE quote.is_active = 1 AND quote.customer_id <> $customerId AND qi.product_id = $productId";
        $result = $this->getConnection()->fetchRow($query);
        $stock = $stock + (int)$result['totalQty'];
        return (int)$stock;
    }

    private function getRealStockConfig($productId,$sku,$customerId){
        $query = "SELECT SUM(quote_item.qty) as totalQty FROM quote INNER JOIN quote_item ON quote_item.quote_id = quote.entity_id 
                    WHERE quote.is_active = 1 AND quote.customer_id <> $customerId AND quote_item.product_id = $productId AND quote_item.sku ='$sku'";
        $result = $this->getConnection()->fetchRow($query);
        return (int)$result['totalQty'];
    }

    public function getListCustomer(){
        $this->_nowCustomerColection->addFieldToFilter('visitor_type', array('eq' => 'c'));
        $this->_nowCustomerColection->load();
        $nowCustomerCollection = $this->_nowCustomerColection->getData();
        $nowCustomer = array();
        if (count($nowCustomerCollection) > 0){
            foreach ($nowCustomerCollection as $customer) {
                if (isset($customer['customer_id'])) $nowCustomer[] = $customer['customer_id'];
            }
        }
        return $nowCustomer;
    }

    public function getGauge($productId,$configId = false,$customerId = false){
        // check enable module
        if (!$this->isEnabled()){
            return false;
        }

        // get customer id
        if ($customerId === false){
            $customerId = $this->getCustomerId();
            $cusSis = $this->_customerSession->getCustomer()->getData('sis_enable');
            if ($cusSis === "0"){
                return false;
            }
        }

        // check config
        if ($configId){
            $configProduct = $this->_productRepository->getById($configId);
            $sis = $configProduct->getData('sis');
            if ($sis === "0"){
                return false;
            }
        }

        // reload data from product attribute
        $product = $this->_productRepository->getById($productId);
        $sis = $product->getData('sis');


        $sltl = (int)$product->getResource()->getAttribute('slt_low')->getFrontend()->getValue($product);
        $sltl = $sltl ? $sltl : $this->getLow();
        $sltm = (int)$product->getResource()->getAttribute('slt_mod')->getFrontend()->getValue($product);
        $sltm = $sltm ? $sltm : $this->getMod();

        // Get stock data for given product.
        $productStock = $this->_stockRegistry->getStockItem($product->getId());
        $productQty = $productStock->getQty();
        // Get quantity of product.
        $realQty = $productQty - $this->getRealStock($productId,$customerId);

        if ($sis === "0"){
            return false;
        }

        if ($realQty > $sltm)
            return 'high';
        elseif ($realQty > $sltl)
            return 'medium';
        else return 'low';
    }

    public function triggerToCustomer($productId,$configId = false,$customerId = false){

        // $options = array(
        //     'cluster' => 'ap1',
        //     'useTLS' => true
        // );
        // $pusher = new \Pusher\Pusher(
        //     '0838c334fcd4ae230387',
        //     'ca30438da5b34379e2d9',
        //     '995381',
        //     $options
        // );

        $options = array(
            'useTLS' => true,
            'port' => '8443',
            'debug' => true
          );
          $pusher = new \Pusher\Pusher(
            'lotus',
            'admin@123',
            'knight',
            $options,
            'pusher.knightgroup.co.nz',
            '1708'
          );

        if ($customerId === false){
            $customerId = $this->getCustomerId();
        }

        $gaugeStock = $this->getGauge($productId, $configId, $customerId);
        $gaugeStock = $gaugeStock ? $gaugeStock : 'none';
        $data['gauge'] =  $gaugeStock;
        
        if ($configId){
            $pusher->trigger("traffic-light-stock", "customer-$customerId-config-$configId-product-$productId", $data);
            $pusher->trigger("traffic-light-stock", "customer-$customerId-product-$productId", $data);
        }            
        else
            $pusher->trigger("traffic-light-stock", "customer-$customerId-product-$productId", $data);

    }

    public function triggerToCart($customerId){

        // $options = array(
        //     'cluster' => 'ap1',
        //     'useTLS' => true
        // );
        // $pusher = new \Pusher\Pusher(
        //     '0838c334fcd4ae230387',
        //     'ca30438da5b34379e2d9',
        //     '995381',
        //     $options
        // );

        $options = array(
            'useTLS' => true,
            'port' => '8443',
            'debug' => true
          );
          $pusher = new \Pusher\Pusher(
            'lotus',
            'admin@123',
            'knight',
            $options,
            'pusher.knightgroup.co.nz',
            '1708'
          );
        $data['cart'] = 'redirect';
        $pusher->trigger("traffic-light-stock", "cart-$customerId", $data);
    }


    public function isActiveTrafficLight(){
        if (!$this->isEnabled()){
            return false;
        }

        $cusSis = $this->_customerSession->getCustomer()->getData('sis_enable');
        if ($cusSis === "0"){
            return false;
        }

        return true;
    }
}