<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\CustomerCompany\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Install data
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * CustomerSetupFactory
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * $attributeSetFactory
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * initiate object
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * install data method
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        /**
         * customer registration form default field mobile number
         */

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_company_id', [
            'type' => 'varchar',
            'label' => 'Company Id',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1050,
            'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
            'position' => 1050,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_company_id')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_company', [
            'type' => 'varchar',
            'label' => 'Company Name',
            'input' => 'text',
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1100,
            'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
            'position' => 1100,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_company')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_phone', [
            'type' => 'varchar',
            'label' => 'Phone',
            'input' => 'text',
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1200,
            'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
            'position' => 1200,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_phone')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_street', [
            'type' => 'varchar',
            'label' => 'Street',
            'input' => 'text',
            'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\DefaultBackend::class,
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1300,
            'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
            'position' => 1300,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_street')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_city', [
            'type' => 'varchar',
            'label' => 'Town / City',
            'input' => 'text',
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1400,
            'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
            'position' => 1400,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_city')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_country_id', [
            'type' => 'varchar',
            'label' => 'Country',
            'input' => 'select',
            'source' => \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country::class,
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1500,
            'position' => 1500,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_country_id')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();


        $customerSetup->addAttribute(Customer::ENTITY, 'comp_region', [
            'type' => 'varchar',
            'label' => 'Region',
            'input' => 'text',
            'backend' => \Magento\Customer\Model\ResourceModel\Address\Attribute\Backend\Region::class,
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1600,
            'position' => 1600,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_region')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_region_id', [
            'type' => 'int',
            'label' => 'Region',
            'input' => 'hidden',
            'source' => \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Region::class,
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1700,
            'position' => 1700,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_region_id')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();

        $customerSetup->addAttribute(Customer::ENTITY, 'comp_postcode', [
            'type' => 'varchar',
            'label' => 'Postcode',
            'input' => 'text',
            'validate_rules' => '[]',
            'data' => \Magento\Customer\Model\Attribute\Data\Postcode::class,
            'required' => true,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1800,
            'position' => 1800,
            'system' => 0,
        ]);
        //add attribute to attribute set
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'comp_postcode')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit', 'adminhtml_checkout'],
            ]);

        $attribute->save();
    }
}