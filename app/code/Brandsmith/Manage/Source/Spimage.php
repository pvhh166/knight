<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Source;

class Spimage implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Spimage repository
     * 
     * @var \Brandsmith\Manage\Api\SpimageRepositoryInterface
     */
    protected $_spimageRepository;

    /**
     * Search Criteria Builder
     * 
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Filter Builder
     * 
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $_filterBuilder;

    /**
     * Options
     * 
     * @var array
     */
    protected $_options;

    /**
     * constructor
     * 
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     */
    public function __construct(
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder
    ) {
        $this->_spimageRepository     = $spimageRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filterBuilder         = $filterBuilder;
    }

    /**
     * Retrieve all Spimages as an option array
     *
     * @return array
     * @throws StateException
     */
    public function getAllOptions()
    {
        if (empty($this->_options)) {
            $options = [];
            $searchCriteria = $this->_searchCriteriaBuilder->create();
            $searchResults = $this->_spimageRepository->getList($searchCriteria);
            foreach ($searchResults->getItems() as $spimage) {
                $options[] = [
                    'value' => $spimage->getSpSku(),
                    'label' => $spimage->getSp_id(),
                ];
            }
            $this->_options = $options;
        }

        return $this->_options;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
