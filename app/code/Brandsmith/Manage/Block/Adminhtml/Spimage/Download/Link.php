<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Block\Adminhtml\Spimage\Download;

use Magento\Backend\Block\Template;
use Brandsmith\Manage\Model\SpimageRepository;

/**
 * Class Link
 * @package Brandsmith\Manage\Block\Adminhtml\Spimage\Download
 */
class Link extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var SpimageRepository
     */
    protected $spImageRepository;

    /**
     * Link constructor.
     *
     * @param \Magento\Framework\Registry $_registry
     * @param SpimageRepository $spImageRepository
     */
    public function __construct (
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $_registry,
        SpimageRepository $spImageRepository,
        array $data = []
    ) {
        $this->_registry = $_registry;
        $this->spImageRepository = $spImageRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getSpImageId()
    {
        return $this->_registry->registry('current_spimage_id');
    }

    /**
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSpImageData()
    {
        $spImageId = $this->getSpImageId();
        return $this->spImageRepository->getById($spImageId);
    }

}