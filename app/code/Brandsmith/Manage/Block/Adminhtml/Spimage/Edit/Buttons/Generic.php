<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Block\Adminhtml\Spimage\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $_context;

    /**
     * Spimage Repository
     * 
     * @var \Brandsmith\Manage\Api\SpimageRepositoryInterface
     */
    protected $_spimageRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
    ) {
        $this->_context           = $context;
        $this->_spimageRepository = $spimageRepository;
    }

    /**
     * Return Spimage ID
     *
     * @return int|null
     */
    public function getSpimageId()
    {
        try {
            return $this->_spimageRepository->getById(
                $this->_context->getRequest()->getParam('spimage_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_context->getUrlBuilder()->getUrl($route, $params);
    }
}
