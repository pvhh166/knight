<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Api\Data;

/**
 * @api
 */
interface SpimageInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const SPIMAGE_ID = 'spimage_id';

    /**
     * SP ID attribute constant
     * 
     * @var string
     */
    const SP_ID = 'sp_id';

    /**
     * SP SKU attribute constant
     * 
     * @var string
     */
    const SP_SKU = 'sp_sku';

    /**
     * Logged User attribute constant
     * 
     * @var string
     */
    const LOGGED_USER = 'logged_user';

    /**
     * Date Created attribute constant
     * 
     * @var string
     */
    const DATE_CREATED = 'date_created';

    /**
     * Image Thumbnail attribute constant
     * 
     * @var string
     */
    const IMAGE_THUMBNAIL = 'image_thumbnail';

    /**
     * Download attribute constant
     * 
     * @var string
     */
    const DOWNLOAD = 'download';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getSpimageId();

    /**
     * Set ID
     *
     * @param int $spimageId
     * @return SpimageInterface
     */
    public function setSpimageId($spimageId);

    /**
     * Get SP ID
     *
     * @return mixed
     */
    public function getSpId();

    /**
     * Set SP ID
     *
     * @param mixed $spId
     * @return SpimageInterface
     */
    public function setSpId($spId);

    /**
     * Get SP SKU
     *
     * @return mixed
     */
    public function getSpSku();

    /**
     * Set SP SKU
     *
     * @param mixed $spSku
     * @return SpimageInterface
     */
    public function setSpSku($spSku);

    /**
     * Get Logged User
     *
     * @return mixed
     */
    public function getLoggedUser();

    /**
     * Set Logged User
     *
     * @param mixed $loggedUser
     * @return SpimageInterface
     */
    public function setLoggedUser($loggedUser);

    /**
     * Get Date Created
     *
     * @return mixed
     */
    public function getDateCreated();

    /**
     * Set Date Created
     *
     * @param mixed $dateCreated
     * @return SpimageInterface
     */
    public function setDateCreated($dateCreated);

    /**
     * Get Image Thumbnail
     *
     * @return mixed
     */
    public function getImageThumbnail();

    /**
     * Set Image Thumbnail
     *
     * @param mixed $imageThumbnail
     * @return SpimageInterface
     */
    public function setImageThumbnail($imageThumbnail);

    /**
     * Get Download
     *
     * @return mixed
     */
    public function getDownload();

    /**
     * Set Download
     *
     * @param mixed $download
     * @return SpimageInterface
     */
    public function setDownload($download);

    /**
     * Get Image Thumbnail URL
     *
     * @return string
     */
    public function getImageThumbnailUrl();
}
