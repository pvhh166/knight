<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Api\Data;

/**
 * @api
 */
interface SpimageSearchResultInterface
{
    /**
     * Get Spimages list.
     *
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface[]
     */
    public function getItems();

    /**
     * Set Spimages list.
     *
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
