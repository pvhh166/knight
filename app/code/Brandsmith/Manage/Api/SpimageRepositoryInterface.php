<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Api;

/**
 * @api
 */
interface SpimageRepositoryInterface
{
    /**
     * Save Spimage.
     *
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface $spimage
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\Manage\Api\Data\SpimageInterface $spimage);

    /**
     * Retrieve Spimage
     *
     * @param int $spimageId
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($spimageId);

    /**
     * Retrieve Spimages matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\Manage\Api\Data\SpimageSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Spimage.
     *
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface $spimage
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\Manage\Api\Data\SpimageInterface $spimage);

    /**
     * Delete Spimage by ID.
     *
     * @param int $spimageId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($spimageId);
}
