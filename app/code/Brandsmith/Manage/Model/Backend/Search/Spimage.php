<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Model\Backend\Search;

/**
 * @method Spimage setQuery(string $query)
 * @method string|null getQuery()
 * @method bool hasQuery()
 * @method Spimage setStart(int $start)
 * @method int|null getStart()
 * @method bool hasStart()
 * @method Spimage setLimit(int $start)
 * @method int|null getLimit()
 * @method bool hasLimit()
 * @method Spimage setResults(array $results)
 * @method array|null getResults()
 */
class Spimage extends \Magento\Framework\DataObject
{
    /**
     * Admin Helper
     * 
     * @var \Magento\Backend\Helper\Data
     */
    protected $_backendData;

    /**
     * Spimage repository
     * 
     * @var \Brandsmith\Manage\Api\SpimageRepositoryInterface
     */
    protected $_spimageRepository;

    /**
     * Search Criteria Builder
     * 
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Filter Builder
     * 
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $_filterBuilder;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Helper\Data $backendData
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Helper\Data $backendData,
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        array $data = []
    ) {
        $this->_backendData           = $backendData;
        $this->_spimageRepository     = $spimageRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filterBuilder         = $filterBuilder;
        parent::__construct($data);
    }

    /**
     * Load search results
     *
     * @return $this
     */
    public function load()
    {
        $result = [];
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($result);
            return $this;
        }

        $this->_searchCriteriaBuilder->setCurrentPage($this->getStart());
        $this->_searchCriteriaBuilder->setPageSize($this->getLimit());
        $searchFields = ['sp_id'];
        $filters = [];
        foreach ($searchFields as $field) {
            $filters[] = $this->_filterBuilder
                ->setField($field)
                ->setConditionType('like')
                ->setValue($this->getQuery() . '%')
                ->create();
        }
        $this->_searchCriteriaBuilder->addFilters($filters);
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        $searchResults = $this->_spimageRepository->getList($searchCriteria);

        foreach ($searchResults->getItems() as $spimage) {
            $result[] = [
                'id' => 'spimage/' . $spimage->getId(),
                'type' => __('Spimage'),
                'name' => $spimage->getSp_id(),
                'description' => $spimage->getSp_id(),
                'url' => $this->_backendData->getUrl('brandsmith_manage/spimage/edit', ['id' => $spimage->getId()]),
            ];
        }
        $this->setResults($result);
        return $this;
    }
}
