<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Model;

use Brandsmith\Manage\Api\SpimageRepositoryInterface;

class SpimageRepository implements SpimageRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $_instances = [];

    /**
     * Spimage resource model
     * 
     * @var \Brandsmith\Manage\Model\ResourceModel\Spimage
     */
    protected $_resource;

    /**
     * Spimage collection factory
     * 
     * @var \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory
     */
    protected $_spimageCollectionFactory;

    /**
     * Spimage interface factory
     * 
     * @var \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory
     */
    protected $_spimageInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Brandsmith\Manage\Api\Data\SpimageSearchResultInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Brandsmith\Manage\Model\ResourceModel\Spimage $resource
     * @param \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $spimageCollectionFactory
     * @param \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\Manage\Api\Data\SpimageSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Brandsmith\Manage\Model\ResourceModel\Spimage $resource,
        \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $spimageCollectionFactory,
        \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\Manage\Api\Data\SpimageSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->_resource                 = $resource;
        $this->_spimageCollectionFactory = $spimageCollectionFactory;
        $this->_spimageInterfaceFactory  = $spimageInterfaceFactory;
        $this->_dataObjectHelper         = $dataObjectHelper;
        $this->_searchResultsFactory     = $searchResultsFactory;
    }

    /**
     * Save Spimage.
     *
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface $spimage
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Brandsmith\Manage\Api\Data\SpimageInterface $spimage)
    {
        /** @var \Brandsmith\Manage\Api\Data\SpimageInterface|\Magento\Framework\Model\AbstractModel $spimage */
        try {
            $this->_resource->save($spimage);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Spimage: %1',
                $exception->getMessage()
            ));
        }
        return $spimage;
    }

    /**
     * Retrieve Spimage.
     *
     * @param int $spimageId
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($spimageId)
    {
        if (!isset($this->_instances[$spimageId])) {
            /** @var \Brandsmith\Manage\Api\Data\SpimageInterface|\Magento\Framework\Model\AbstractModel $spimage */
            $spimage = $this->_spimageInterfaceFactory->create();
            $this->_resource->load($spimage, $spimageId);
            if (!$spimage->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Spimage doesn\'t exist'));
            }
            $this->_instances[$spimageId] = $spimage;
        }
        return $this->_instances[$spimageId];
    }

    /**
     * Retrieve Spimages matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\Manage\Api\Data\SpimageSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Brandsmith\Manage\Api\Data\SpimageSearchResultInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Brandsmith\Manage\Model\ResourceModel\Spimage\Collection $collection */
        $collection = $this->_spimageCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->_addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'spimage_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Brandsmith\Manage\Api\Data\SpimageInterface[] $spimages */
        $spimages = [];
        /** @var \Brandsmith\Manage\Model\Spimage $spimage */
        foreach ($collection as $spimage) {
            /** @var \Brandsmith\Manage\Api\Data\SpimageInterface $spimageDataObject */
            $spimageDataObject = $this->_spimageInterfaceFactory->create();
            $this->_dataObjectHelper->populateWithArray(
                $spimageDataObject,
                $spimage->getData(),
                \Brandsmith\Manage\Api\Data\SpimageInterface::class
            );
            $spimages[] = $spimageDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($spimages);
    }

    /**
     * Delete Spimage.
     *
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface $spimage
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Brandsmith\Manage\Api\Data\SpimageInterface $spimage)
    {
        /** @var \Brandsmith\Manage\Api\Data\SpimageInterface|\Magento\Framework\Model\AbstractModel $spimage */
        $id = $spimage->getId();
        try {
            unset($this->_instances[$id]);
            $this->_resource->delete($spimage);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Spimage %1', $id)
            );
        }
        unset($this->_instances[$id]);
        return true;
    }

    /**
     * Delete Spimage by ID.
     *
     * @param int $spimageId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($spimageId)
    {
        $spimage = $this->getById($spimageId);
        return $this->delete($spimage);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Brandsmith\Manage\Model\ResourceModel\Spimage\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Brandsmith\Manage\Model\ResourceModel\Spimage\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
