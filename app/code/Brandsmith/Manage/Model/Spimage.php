<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Model;

/**
 * @method \Brandsmith\Manage\Model\ResourceModel\Spimage _getResource()
 * @method \Brandsmith\Manage\Model\ResourceModel\Spimage getResource()
 */
class Spimage extends \Magento\Framework\Model\AbstractModel implements \Brandsmith\Manage\Api\Data\SpimageInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'brandsmith_manage_spimage';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_manage_spimage';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'spimage';

    /**
     * Uploader pool
     * 
     * @var \Brandsmith\Manage\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Brandsmith\Manage\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Brandsmith\Manage\Model\UploaderPool $uploaderPool,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_uploaderPool = $uploaderPool;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Brandsmith\Manage\Model\ResourceModel\Spimage::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Spimage id
     *
     * @return array
     */
    public function getSpimageId()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::SPIMAGE_ID);
    }

    /**
     * set Spimage id
     *
     * @param int $spimageId
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setSpimageId($spimageId)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::SPIMAGE_ID, $spimageId);
    }

    /**
     * set SP ID
     *
     * @param mixed $spId
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setSpId($spId)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::SP_ID, $spId);
    }

    /**
     * get SP ID
     *
     * @return string
     */
    public function getSpId()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::SP_ID);
    }

    /**
     * set SP SKU
     *
     * @param mixed $spSku
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setSpSku($spSku)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::SP_SKU, $spSku);
    }

    /**
     * get SP SKU
     *
     * @return string
     */
    public function getSpSku()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::SP_SKU);
    }

    /**
     * set Logged User
     *
     * @param mixed $loggedUser
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setLoggedUser($loggedUser)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::LOGGED_USER, $loggedUser);
    }

    /**
     * get Logged User
     *
     * @return string
     */
    public function getLoggedUser()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::LOGGED_USER);
    }

    /**
     * set Date Created
     *
     * @param mixed $dateCreated
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setDateCreated($dateCreated)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::DATE_CREATED, $dateCreated);
    }

    /**
     * get Date Created
     *
     * @return string
     */
    public function getDateCreated()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::DATE_CREATED);
    }

    /**
     * set Image Thumbnail
     *
     * @param mixed $imageThumbnail
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setImageThumbnail($imageThumbnail)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::IMAGE_THUMBNAIL, $imageThumbnail);
    }

    /**
     * get Image Thumbnail
     *
     * @return string
     */
    public function getImageThumbnail()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::IMAGE_THUMBNAIL);
    }

    /**
     * set Download
     *
     * @param mixed $download
     * @return \Brandsmith\Manage\Api\Data\SpimageInterface
     */
    public function setDownload($download)
    {
        return $this->setData(\Brandsmith\Manage\Api\Data\SpimageInterface::DOWNLOAD, $download);
    }

    /**
     * get Download
     *
     * @return string
     */
    public function getDownload()
    {
        return $this->getData(\Brandsmith\Manage\Api\Data\SpimageInterface::DOWNLOAD);
    }

    /**
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageThumbnailUrl()
    {
        $url = false;
        $imageThumbnail = $this->getImageThumbnail();
        if ($imageThumbnail) {
            if (is_string($imageThumbnail)) {
                $uploader = $this->_uploaderPool->getUploader('image');
                $url = $uploader->getBaseUrl().$uploader->getBasePath().$imageThumbnail;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the Image&#x20;Thumbnail url.')
                );
            }
        }
        return $url;
    }
}
