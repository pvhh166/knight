<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Model\Spimage;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Loaded data cache
     * 
     * @var array
     */
    protected $_loadedData;

    /**
     * Data persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     * 
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->_dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Brandsmith\Manage\Model\Spimage $spimage */
        foreach ($items as $spimage) {
            $this->_loadedData[$spimage->getId()] = $spimage->getData();

            if (isset($this->_loadedData[$spimage->getId()]['image_thumbnail'])) {
                $imageThumbnail = [];
                $imageThumbnail[0]['name'] = $spimage->getImageThumbnail();
                $imageThumbnail[0]['url'] = $spimage->getImageThumbnailUrl();
                $this->_loadedData[$spimage->getId()]['image_thumbnail'] = $imageThumbnail;
            }
        }
        $data = $this->_dataPersistor->get('brandsmith_manage_spimage');
        if (!empty($data)) {
            $spimage = $this->collection->getNewEmptyItem();
            $spimage->setData($data);
            $this->_loadedData[$spimage->getId()] = $spimage->getData();
            $this->_dataPersistor->clear('brandsmith_manage_spimage');
        }
        return $this->_loadedData;
    }
}
