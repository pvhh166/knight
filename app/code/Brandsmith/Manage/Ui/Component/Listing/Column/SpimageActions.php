<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Ui\Component\Listing\Column;

class SpimageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Url path  to edit
     * 
     * @var string
     */
    const URL_PATH_EDIT = 'brandsmith_manage/spimage/edit';

    /**
     * Url path  to delete
     * 
     * @var string
     */
    const URL_PATH_DELETE = 'brandsmith_manage/spimage/delete';

    /**
     * Url builder
     * 
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * constructor
     * 
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['spimage_id'])) {
                    /*$item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $item['download'],
                            'download' => $item['sp_sku'],
                            'target' => '_blank',
                            '__disableTmpl' => true,
                            'text' => '123-456',
                            'label' => __('Download' . $item['sp_sku'])
                        ]
                    ];*/
                    $item[$this->getData('name')] = html_entity_decode('<a href="'.$item['download'].'" download="'.$item['sp_sku'].'">Download</a>');

                }
            }
        }
        return $dataSource;
    }

    /**
     * $item[$this->getData('name')] = html_entity_decode('<a href="'.$this->urlBuilder->getUrl(self::PRODUCT_URL_PATH_EDIT, ['id' => $item['product_id']]).'">'.$item['product_id'].'</a>');
     *
     */
}
