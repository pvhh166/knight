<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Ui\Component\Listing\Column;

class Link extends \Magento\Ui\Component\Listing\Columns\Column
{
    /** Url Path */
    const PRODUCT_URL_PATH_EDIT = 'catalog/product/edit';

    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * constructor
     *
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->productRepository = $productRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['spimage_id'])) {
                    if($this->getData('name') == 'product_id_sku') {
                        $item[$this->getData('name')] = html_entity_decode('<a data-text="'.$this->getData('name').'" href="'.$this->_urlBuilder->getUrl(self::PRODUCT_URL_PATH_EDIT, ['id' => $item['sp_id']]).'" target="_blank">'.$item['sp_sku'].'</a>');
                    } else {
                        $item[$this->getData('name')] = html_entity_decode('<a data-text="'.$this->getData('name').'" href="'.$this->_urlBuilder->getUrl(self::PRODUCT_URL_PATH_EDIT, ['id' => $item['sp_id']]).'" target="_blank">'.$item['sp_id'].'</a>');
                    }
                }
            }
        }
        return $dataSource;
    }
}
