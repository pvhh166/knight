<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Controller\Adminhtml\Spimage;

class Edit extends \Brandsmith\Manage\Controller\Adminhtml\Spimage
{
    /**
     * Initialize current Spimage and set it in the registry.
     *
     * @return int
     */
    protected function _initSpimage()
    {
        $spimageId = $this->getRequest()->getParam('spimage_id');
        $this->_coreRegistry->register(\Brandsmith\Manage\Controller\RegistryConstants::CURRENT_SPIMAGE_ID, $spimageId);

        return $spimageId;
    }

    /**
     * Edit or create Spimage
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $spimageId = $this->_initSpimage();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_Manage::manage_spimage');
        $resultPage->getConfig()->getTitle()->prepend(__('Spimages'));
        $resultPage->addBreadcrumb(__('Manage'), __('Manage'));
        $resultPage->addBreadcrumb(__('Spimages'), __('Spimages'), $this->getUrl('brandsmith_manage/spimage'));

        if ($spimageId === null) {
            $resultPage->addBreadcrumb(__('New Spimage'), __('New Spimage'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Spimage'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Spimage'), __('Edit Spimage'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_spimageRepository->getById($spimageId)->getSpSku()
            );
        }
        return $resultPage;
    }
}
