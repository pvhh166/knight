<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Controller\Adminhtml\Spimage;

class Delete extends \Brandsmith\Manage\Controller\Adminhtml\Spimage
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('spimage_id');
        if ($id) {
            try {
                $this->_spimageRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Spimage has been deleted.'));
                $resultRedirect->setPath('brandsmith_manage/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Spimage no longer exists.'));
                return $resultRedirect->setPath('brandsmith_manage/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('brandsmith_manage/spimage/edit', ['spimage_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Spimage'));
                return $resultRedirect->setPath('brandsmith_manage/spimage/edit', ['spimage_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Spimage to delete.'));
        $resultRedirect->setPath('brandsmith_manage/*/');
        return $resultRedirect;
    }
}
