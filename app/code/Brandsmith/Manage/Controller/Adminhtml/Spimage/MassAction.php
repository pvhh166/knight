<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Controller\Adminhtml\Spimage;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Spimage repository
     * 
     * @var \Brandsmith\Manage\Api\SpimageRepositoryInterface
     */
    protected $_spimageRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * Spimage collection factory
     * 
     * @var \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $_successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $_errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Brandsmith\Manage\Model\ResourceModel\Spimage\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->_spimageRepository = $spimageRepository;
        $this->_filter            = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_successMessage    = $successMessage;
        $this->_errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Brandsmith\Manage\Api\Data\SpimageInterface $spimage
     * @return mixed
     */
    abstract protected function _massAction(\Brandsmith\Manage\Api\Data\SpimageInterface $spimage);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $spimage) {
                $this->_massAction($spimage);
            }
            $this->messageManager->addSuccessMessage(__($this->_successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->_errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('brandsmith_manage/*/index');
        return $redirectResult;
    }
}
