<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Controller\Adminhtml\Spimage;

class Index extends \Brandsmith\Manage\Controller\Adminhtml\Spimage
{
    /**
     * Spimages list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Brandsmith_Manage::spimage');
        $resultPage->getConfig()->getTitle()->prepend(__('Configuration Images Store'));
        $resultPage->addBreadcrumb(__('Manage'), __('Manage'));
        $resultPage->addBreadcrumb(__('Spimages'), __('Spimages'));
        return $resultPage;
    }
}
