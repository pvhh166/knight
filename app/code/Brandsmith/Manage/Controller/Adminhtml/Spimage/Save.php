<?php
/**
 * Brandsmith_Manage extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Brandsmith
 * @package   Brandsmith_Manage
 * @copyright Copyright (c) 2020
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Brandsmith\Manage\Controller\Adminhtml\Spimage;

class Save extends \Brandsmith\Manage\Controller\Adminhtml\Spimage
{
    /**
     * Spimage factory
     * 
     * @var \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory
     */
    protected $_spimageFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $_dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $_dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Brandsmith\Manage\Model\UploaderPool
     */
    protected $_uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\Manage\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Brandsmith\Manage\Api\SpimageRepositoryInterface $spimageRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brandsmith\Manage\Api\Data\SpimageInterfaceFactory $spimageFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Brandsmith\Manage\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->_spimageFactory      = $spimageFactory;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_dataObjectHelper    = $dataObjectHelper;
        $this->_uploaderPool        = $uploaderPool;
        $this->_dataPersistor       = $dataPersistor;
        parent::__construct($context, $coreRegistry, $spimageRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Brandsmith\Manage\Api\Data\SpimageInterface $spimage */
        $spimage = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['spimage_id']) ? $data['spimage_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $spimage = $this->_spimageRepository->getById((int)$id);
            } else {
                unset($data['spimage_id']);
                $spimage = $this->_spimageFactory->create();
            }
            $imageThumbnail = $this->_getUploader('image')->uploadFileAndGetName('image_thumbnail', $data);
            $data['image_thumbnail'] = $imageThumbnail;
            $this->_dataObjectHelper->populateWithArray($spimage, $data, \Brandsmith\Manage\Api\Data\SpimageInterface::class);
            $this->_spimageRepository->save($spimage);
            $this->messageManager->addSuccessMessage(__('You saved the Spimage'));
            $this->_dataPersistor->clear('brandsmith_manage_spimage');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('brandsmith_manage/spimage/edit', ['spimage_id' => $spimage->getId()]);
            } else {
                $resultRedirect->setPath('brandsmith_manage/spimage');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_dataPersistor->set('brandsmith_manage_spimage', $postData);
            $resultRedirect->setPath('brandsmith_manage/spimage/edit', ['spimage_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Spimage'));
            $this->_dataPersistor->set('brandsmith_manage_spimage', $postData);
            $resultRedirect->setPath('brandsmith_manage/spimage/edit', ['spimage_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Brandsmith\Manage\Model\Uploader
     * @throws \Exception
     */
    protected function _getUploader($type)
    {
        return $this->_uploaderPool->getUploader($type);
    }
}
