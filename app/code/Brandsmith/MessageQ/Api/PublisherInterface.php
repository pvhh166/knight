<?php

namespace Brandsmith\MessageQ\Api;

interface PublisherInterface
{
    /**
     * Publish message to queue
     */
    public function publish($queueName, $messageContent);
}
