<?php

namespace Brandsmith\MessageQ\Model;

use Brandsmith\MessageQ\Api\Data\MessageEnvelopeInterfaceFactory;
use Brandsmith\MessageQ\Api\Config\ConfigInterface as QueueConfig;
use Brandsmith\MessageQ\Api\MessageEncoderInterface;

class Publisher implements \Brandsmith\MessageQ\Api\PublisherInterface
{
    /**
     * @var MessageEnvelopeInterfaceFactory
     */
    private $messageEnvelopeFactory;
    
    /**
     * @var MessageEncoderInterface
     */
    private $messageEncoder;
    
    /**
     * @var QueueConfig
     */
    private $queueConfig;

    /**
     * @param MessageEnvelopeInterfaceFactory $messageEnvelopeFactory
     * @param MessageEncoderInterface $messageEncoder
     * @param QueueConfig $queueConfig
     */
    public function __construct(
        MessageEnvelopeInterfaceFactory $messageEnvelopeFactory,
        MessageEncoderInterface $messageEncoder,
        QueueConfig $queueConfig
    ) {
        $this->messageEnvelopeFactory = $messageEnvelopeFactory;
        $this->messageEncoder = $messageEncoder;
        $this->queueConfig = $queueConfig;
    }
    
    /**
     * {@inheritdoc}
     */
    public function publish($queueName, $messageContent)
    {
        $envelope = $this->messageEnvelopeFactory->create()
            ->setContentType($this->messageEncoder->getContentType())
            ->setContent(
                $this->messageEncoder->encode($queueName, $messageContent)
            );
        
        $this->queueConfig->getQueueBrokerInstance($queueName)
            ->enqueue($envelope);
    }
}
