<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class RewardCheckoutSubmitAllAfter
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class RewardCheckoutSubmitAllAfter implements ObserverInterface
{

    private $ruleValidator;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var \Brandsmith\RewardPoints\Model\HistoryFactory
     */
    private $historyFactory;

    /**
     * @var \Brandsmith\RewardPoints\Model\Rule\SpendFactory
     */
    private $spendRuleFactory;

    /**
     * @var \Brandsmith\RewardPoints\Model\Rule\EarningFactory
     */
    private $earningRuleFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $loger;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Brandsmith\RewardPoints\Helper\Data
     */
    private $helper;

    /**
     * @var Message\AfterPlaceOrder
     */
    private $messageManager;

    protected $_orderRepository;

    /**
     * RewardCheckoutSubmitAllAfter constructor.
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param \Brandsmith\RewardPoints\Model\HistoryFactory $historyFactory
     * @param \Brandsmith\RewardPoints\Model\Rule\SpendFactory $spendRuleFactory
     * @param \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory
     * @param \Psr\Log\LoggerInterface $loger
     * @param \Brandsmith\RewardPoints\Model\Validator $ruleValidator
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Brandsmith\RewardPoints\Helper\Data $helper
     * @param Message\AfterPlaceOrder $messageManager
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        \Brandsmith\RewardPoints\Model\HistoryFactory $historyFactory,
        \Brandsmith\RewardPoints\Model\Rule\SpendFactory $spendRuleFactory,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        \Psr\Log\LoggerInterface $loger,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Helper\Data $helper,
        \Brandsmith\RewardPoints\Observer\Message\AfterPlaceOrder $messageManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->historyFactory = $historyFactory;
        $this->spendRuleFactory = $spendRuleFactory;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->loger = $loger;
        $this->ruleValidator = $ruleValidator;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isEnabled()) {
            return $this;
        }

        if (!$this->customerSession->isLoggedIn()) {
            return $this;
        }
        /** @var \Magento\Sales\Api\Data\OrderInterface $order */
        $order = $observer->getData('order');
        /** @var \Magento\Quote\Api\Data\CartInterface $quote */
        $quote = $observer->getData('quote');

        $amount = $quote->getRewardPointsAmount();

        if($amount == 0) {
            $store = $this->storeManager->getStore($quote->getStoreId());

            $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
                ->setWebsiteId($store->getWebsiteId());

            $items = $quote->getItems();
            $ids = [];
            foreach ($items as $item) {
                $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
            }
            $ids = array_unique($ids);

            $totalPoints = 0;
            foreach ($ids as $ruleId) {
                /** @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule */
                $rule = $this->earningRuleFactory->create()->load($ruleId);
                $points = $this->updateHistory($quote, $order, $rule);

                $totalPoints += $points;
            }

            if ($totalPoints > 0) {


                $orderId = $order->getEntityId();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $order = $objectManager->create('\Magento\Sales\Model\Order')
                    ->load($orderId);

                $order->setState("processing")->setStatus("processing");

                $order->save();

                $this->messageManager->showMessage($totalPoints);
            }
        }
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Brandsmith\RewardPoints\Model\Rule\Earning $rule
     * @return float|int
     */
    private function updateHistory($quote, $order, $rule)
    {
        $name = [$quote->getCustomer()->getFirstname(), $quote->getCustomer()->getLastname()];
        try {
            /** @var \Brandsmith\RewardPoints\Model\History $model */
            $model = $this->historyFactory->create();
            $model->setCustomerId($quote->getCustomer()->getId());
            $model->setCompanyId($quote->getCustomer()->getGroupId());
            $model->setCustomerName(implode(', ', $name));

            if ($rule->getType() == 1) {
                $points = $rule->getPoints();
            } elseif ($rule->getType() == 2) {
                $subtotal = $order->getSubtotal();
                $points = floor($subtotal / $rule->getSpend() * $rule->getEarn());
            }
            $model->setPoints($points);
            $model->setRuleName($rule->getName());
            $model->setRuleSpendId($rule->getId());
            $model->setOrderId($order->getId());
            $model->setOrderIncrementId($order->getIncrementId());
            $model->setStoreId($quote->getStoreId());
            $model->setTypeRule(2);

            $model->save();
        } catch (\Exception $e) {
            $this->loger->critical($e);

            return 0;
        }

        return $points;
    }
}
