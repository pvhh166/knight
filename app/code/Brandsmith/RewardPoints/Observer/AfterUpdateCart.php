<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class RewardSalesConvertQuoteToOrder
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class AfterUpdateCart implements ObserverInterface
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    private $ruleValidator;

    private $checkoutSession;

    private $earningRuleFactory;

    private $pointsHelper;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        CheckoutSession $checkoutSession,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->ruleValidator = $ruleValidator;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->pointsHelper = $pointsHelper;
    }
    /**
     * Setup Reward Points Amount to Order(magento default logic to convert quote to order does not work ~2.1.7)
     *
     * @param  Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
	    $amount = 0;
        $quote = $observer->getData('cart')->getQuote();
        $store = $this->storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $quote_subtotal = $quote->getSubtotal();
        $items = $quote->getItems();
        $ids = [];
        $exceptCates = $this->pointsHelper->getExceptCategory();
        $exceptCates = explode(",",$exceptCates);
        if(count($items) > 0) {
            foreach ($items as $item) {
                $productCates = $item->getProduct()->getCategoryIds();
                foreach ($exceptCates as $cateId) {
                    if(in_array($cateId, $productCates)) {
                        $quote_subtotal = $quote_subtotal - ($item->getPrice()*$item->getQty());
                        continue;
                    }
                }
                $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
                $quote_subtotal = $quote_subtotal - $item->getDiscountAmount();
            }
        }

        $ids = array_unique($ids);
        if(count($ids) > 0) {
            if($quote->getRewardPointsAmount() > 0) {
                foreach ($ids as $ruleId) {
                    $rule = $this->earningRuleFactory->create()->load($ruleId);
                    $name = $rule->getName();
                    $percent = explode("%", $name)[0];
                    if (is_numeric($percent))
                        $amount = $quote_subtotal * $percent / 100;
                    try {
                        $quote->setRewardPointsAmount($amount);
                        $quote->setUsedDiscount(1);
                    } catch (\Exception $e) {
                        throw new CouldNotSaveException(__('Could not apply reward points'));
                    }
                }
            }
        }else {
            $quote->setRewardPointsAmount(0);
            $quote->setUsedDiscount(2);
        }
        $this->quoteRepository->save($quote->collectTotals());
    }
}
