<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer\Message;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class BeforeCreateCustomerAccount
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class BeforeShareWishList extends AbstractMessage implements ObserverInterface
{


    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        if ($this->helper->isEnabled() && $this->getBeforeShareWishListMessage()) {
            $this->messageManager->addNoticeMessage($this->getBeforeShareWishListMessage());
        }
    }
}
