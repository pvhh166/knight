<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer\Message;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AfterPlaceOrder
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class AfterPlaceOrder extends AbstractMessage
{

    /**
     * @param $points
     */
    public function showMessage($points)
    {
        if ($this->helper->isEnabled() && $this->getAfterPlaceOrderMessage($points)) {
            $this->messageManager->addNoticeMessage($this->getAfterPlaceOrderMessage($points));
        }
    }
}
