<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class RewardSalesConvertQuoteToOrder
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class RewardSalesConvertQuoteToOrder implements ObserverInterface
{
    /**
     * @var \Brandsmith\RewardPoints\Model\Rule\EarningFactory
     */
    private $earningRuleFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    private $ruleValidator;

    private $pointsHelper;

    public function __construct(
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper
    ) {
        $this->ruleValidator = $ruleValidator;
        $this->storeManager = $storeManager;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->pointsHelper = $pointsHelper;
    }
    /**
     * Setup Reward Points Amount to Order(magento default logic to convert quote to order does not work ~2.1.7)
     *
     * @param  Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getData('order');
        $quote = $observer->getData('quote');
        $order->setRewardPointsAmount($quote->getRewardPointsAmount());

        $store = $this->storeManager->getStore($quote->getStoreId());
        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $items = $quote->getItems();
        $ids = [];
        $subtotal = $order->getSubtotal();
        $exceptCates = $this->pointsHelper->getExceptCategory();
        $exceptCates = explode(",",$exceptCates);
        
        foreach ($quote->getAllItems() as $quoteItem) {
            $productCates = $quoteItem->getProduct()->getCategoryIds();
            foreach ($exceptCates as $cateId) {
                if(in_array($cateId, $productCates)) {
                    $subtotal = $subtotal - ($quoteItem->getPrice()*$quoteItem->getQty());
                    continue;
                }
            }
            $ids = array_merge($ids, $this->ruleValidator->process($quoteItem, $quote));
        }
        $ids = array_unique($ids);

        $totalPoints = 0;
        foreach ($ids as $ruleId) {
            /** @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule */
            $rule = $this->earningRuleFactory->create()->load($ruleId);
            if ($rule->getType() == 1) {
                $totalPoints = $rule->getPoints();
            } elseif ($rule->getType() == 2) {
                //$subtotal = $order->getSubtotal();
                $totalPoints = floor($subtotal / $rule->getSpend() * $rule->getEarn());
            }
        }
        $order->setEarnPoints($totalPoints);
    }
}
