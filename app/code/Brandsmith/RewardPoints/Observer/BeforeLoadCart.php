<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class RewardSalesConvertQuoteToOrder
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class BeforeLoadCart implements ObserverInterface
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    private $checkoutSession;


    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        CheckoutSession $checkoutSession
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->checkoutSession = $checkoutSession;
    }
    /**
     * Setup Reward Points Amount to Order(magento default logic to convert quote to order does not work ~2.1.7)
     *
     * @param  Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $this->checkoutSession->getQuote();
        $this->quoteRepository->save($quote->collectTotals());
    }
}
