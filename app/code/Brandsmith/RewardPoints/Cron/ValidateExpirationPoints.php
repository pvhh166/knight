<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Cron;

use Magento\Store\Model\ScopeInterface;
use Brandsmith\RewardPoints\Model\History;

/**
 * Class ValidateExpirationPoints
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class ValidateExpirationPoints
{
    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * ValidateExpirationPoints constructor.
     * @param \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {

        $days = $this->getExpirationDay();

        if ($days) {
            /** @var \Brandsmith\RewardPoints\Model\ResourceModel\History\Collection $collection */
            $collection = $this->collectionFactory->create();
            $collection->addExpiredFilter($days);

            /** @var \Brandsmith\RewardPoints\Model\History $item */
            foreach ($collection as $item) {
                $item->setData('is_expired', 1);

                try {
                    $item->save();

                } catch (\Exception $e) {

                }
            }
        }

    }

    /**
     * Check if need setup expiration for history table
     * @return bool|int
     */
    private function getExpirationDay()
    {
        $isActiveExpiration = (bool)$this->scopeConfig
            ->getValue(History::POINTS_EXPIRATION_XML, ScopeInterface::SCOPE_STORE);

        if (!$isActiveExpiration) {
            return false;
        }

        $expirationPeriod = $this->scopeConfig
            ->getValue(History::POINTS_EXPIRATION_PERIOD_XML, ScopeInterface::SCOPE_STORE);

        if (empty($expirationPeriod) || !$expirationPeriod) {
            return false;
        }

        return (int)$expirationPeriod;

    }
}
