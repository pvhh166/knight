<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block\Sales\Order;

class Points extends \Magento\Framework\View\Element\Template
{

    /**
     * Tax configuration model
     *
     * @var \Magento\Tax\Model\Config
     */
    public $config;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var \Magento\Framework\DataObject
     */
    public $source;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Tax\Model\Config                        $taxConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Tax\Model\Config $taxConfig,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        array $data = []
    ) {
        $this->config = $taxConfig;
        $this->historyCollectionFactory = $historyCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Check if we need display full tax total info
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->source;
    }

    public function getStore()
    {
        return $this->order->getStore();
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     * Initialize all order totals relates with tax
     *
     * @return \Magento\Tax\Block\Sales\Order\Tax
     */
    public function initTotals()
    {

        $parent = $this->getParentBlock();
        $this->order = $parent->getOrder();
        $this->source = $parent->getSource();

        $store = $this->getStore();
        $discountAmount = $this->order->getRewardPointsAmount();

        if($discountAmount > 0) {
            $rewardPoints = new \Magento\Framework\DataObject(
                [
                    'code' => 'reward_points',
                    'strong' => true,
                    'value' => -$discountAmount,
                    'label' => __('Discount'),
                ]
            );

                $parent->addTotal($rewardPoints, 'subtotal');
        }else {
            $collection = $this->historyCollectionFactory->create();
            $collection->addFieldToFilter('order_increment_id', ['eq' => $this->order->getIncrementId()]);
            $collection->addFieldToFilter('type_rule', ['eq' => 2]);
            $collection->load();
            if(count($collection) > 0) {
                $item = $collection->fetchItem();
                if ($item) {
                    $reward_point = $item->getData('points');
                }
                $rewardPoints = new \Magento\Framework\DataObject(
                    [
                        'code' => 'reward_points',
                        'strong' => false,
                        'value' => $reward_point.' points',
                        'label' => __('Reward Points'),
                        'is_formated' => true,
                    ]
                );

                $parent->addTotal($rewardPoints, 'reward_points');
            }
        }

        return $this;
    }
}
