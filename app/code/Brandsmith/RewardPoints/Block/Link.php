<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block;

use Magento\Customer\Block\Account\SortLinkInterface;

/**
 * Class Link
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class Link extends \Magento\Framework\View\Element\Html\Link implements SortLinkInterface
{
    /**
     * Template name
     *
     * @var string
     */
    protected $_template = 'Brandsmith_RewardPoints::link.phtml'; //@codingStandardsIgnoreLine

    /**
     * @var \Brandsmith\RewardPoints\Helper\Data
     */
    public $pointsHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Brandsmith\RewardPoints\Helper\Data $pointsHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper,
        array $data = []
    ) {
        $this->pointsHelper = $pointsHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    protected function _toHtml()//@codingStandardsIgnoreLine
    {
        if ($this->pointsHelper->isEnabled()) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl('points/history');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Reward Points');
    }

    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}
