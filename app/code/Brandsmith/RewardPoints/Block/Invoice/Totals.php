<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block\Invoice;

/**
 * Class Totals
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class Totals extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * @var \Magento\Framework\DataObject
     */
    private $dataObject;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * Totals constructor.
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Framework\DataObject $dataObject
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Framework\DataObject $dataObject,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        array $data = []
    ) {
        $this->dataObject = $dataObject;
        $this->historyCollectionFactory = $historyCollectionFactory;
        parent::__construct($context, $data);
    }

    public function initTotals()
    {

        $orderTotalsBlock = $this->getParentBlock();
        $order = $orderTotalsBlock->getOrder();
        $points = $order->getRewardPointsAmount();

        if ($points > 0) {
            $this->dataObject->addData([
                'code' => 'reward_total',
                'label' => __('Discount'),
                'value' => (-1) * $points,
            ]);

            $orderTotalsBlock->addTotal($this->dataObject, 'subtotal');
        }else {
            $collection = $this->historyCollectionFactory->create();
            $collection->addFieldToFilter('order_increment_id', ['eq' => $order->getIncrementId()]);
            $collection->addFieldToFilter('type_rule', ['eq' => 2]);
            $collection->load();
            if(count($collection) > 0) {
                $item = $collection->fetchItem();
                if ($item) {
                    $reward_point = $item->getData('points');
                }

                $this->dataObject->addData([
                    'code' => 'reward_total',
                    'label' => __('Reward Points'),
                    'value' => $reward_point.' points',
                    'is_formated' => true,
                ]);

                $orderTotalsBlock->addTotal($this->dataObject, 'subtotal');
            }
        }
    }
}
