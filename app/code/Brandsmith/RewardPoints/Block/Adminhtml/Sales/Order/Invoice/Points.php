<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block\Adminhtml\Sales\Order\Invoice;

class Points extends \Magento\Framework\View\Element\Template
{
    private $config;

    private $order;

    private $source;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Tax\Model\Config $taxConfig,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        array $data = []
    ) {
        $this->config = $taxConfig;
        $this->historyCollectionFactory = $historyCollectionFactory;
        parent::__construct($context, $data);
    }

    public function displayFullSummary()
    {
        return true;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function getStore()
    {
        return $this->order->getStore();
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->order = $parent->getOrder();
        $this->source = $parent->getSource();

        $store = $this->getStore();

        $value = $this->order->getRewardPointsAmount();
        if($value > 0) {
            $fee = new \Magento\Framework\DataObject(
                [
                    'code' => 'reward_points',
                    'strong' => false,
                    'value' => -$value,
                    'base_value' => -$value,
                    'label' => __('Discount'),
                ]
            );
            $parent->addTotal($fee, 'reward_points');
        }else {
            $collection = $this->historyCollectionFactory->create();
            $collection->addFieldToFilter('order_increment_id', ['eq' => $this->order->getIncrementId()]);
            $collection->addFieldToFilter('type_rule', ['eq' => 2]);
            $collection->load();
            if(count($collection) > 0) {
                $item = $collection->fetchItem();
                if ($item) {
                    $reward_point = $item->getData('points');
                }

                $fee = new \Magento\Framework\DataObject(
                    [
                        'code' => 'reward_points',
                        'strong' => false,
                        'value' => $reward_point.' points',
                        'base_value' => $reward_point .' points',
                        'label' => __('Reward Points'),
                        'is_formated' => true
                    ]
                );
                $parent->addTotal($fee, 'reward_points');
            }
        }

        return $this;
    }
}
