<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block\Adminhtml\History;

/**
 * Class Add
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Container extends \Magento\Backend\Block\Template
{

    /**
     * Get the url for save
     *
     * @return string
     */
    public function getSaveUrl()
    {
        if ($this->getTemplateId()) {
            $params = ['template_id' => $this->getTemplateId()];
        } else {
            $params = ['id' => $this->getRequest()->getParam('id')];
        }
        return $this->getUrl('*/*/save', $params);
    }

    public function getCustomerUrl()
    {
        return $this->getUrl('*/*/customers', []);
    }
}
