<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Block\Customer;

use Magento\Customer\Model\Session;
use Brandsmith\RewardPoints\Model\History as PointsHistory;
use Magento\Store\Model\ScopeInterface;

/**
 * Class History
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class History extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'points/history.phtml';

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $pointsCollectionFactory;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    private $orderConfig;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\Collection
     */
    private $points;

    /**
     * @var int
     */
    private $totalPoints = 0;

    /**
     * @var int
     */
    private $totalPointsByComany = 0;

    /**
     * @var
     */
    private $isExpirationEnable = null;

    /**
     * @var Brandsmith\CompanyManage\Model\ResourceModel\Company\CollectionFactory
     */
    private $companyCollectionFactory;

    protected $_customerRepository;

    /**
     * History constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $pointsCollectionFactory
     * @param Session $customerSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $pointsCollectionFactory,
        \Brandsmith\CompanyManage\Model\ResourceModel\Company\CollectionFactory $companyCollectionFactory,
        \Brandsmith\CompanyManage\Model\CustomerRepository $customerRepository,
        Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        array $data = []
    )
    {
        $this->pointsCollectionFactory = $pointsCollectionFactory;
        $this->companyCollectionFactory = $companyCollectionFactory;
        $this->_customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->orderConfig = $orderConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Reward Points History'));
    }

    /**
     * @return $this|bool|\Brandsmith\RewardPoints\Model\ResourceModel\History\Collection
     */
    public function getPoints()
    {
        if (!($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }

        if (!$this->points) {
            $this->points = $this->pointsCollectionFactory->create();
            $this->points->getSelect()->joinLeft(
                ['rl' => 'sales_order_grid'],
                'main_table.order_increment_id = rl.increment_id',
                ['status']
            );
            $this->points->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'main_table.customer_id',
                ['eq' => $customerId]
            )->addFieldToFilter(
                'is_deleted',
                ['eq' => 0]
            )->addFieldToFilter(
                'is_expired',
                ['eq' => 0]
            )->setOrder(
                'created_at',
                'desc'
            );

            $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;

            $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 1;

            $this->points->setPageSize($pageSize);
            $this->points->setCurPage($page);

            if ($this->isExpirationEnabled()) {
                $this->points->addExpiredField($this->getExpirationDays());
            }
            $this->calcTotal();
        }

        return $this->points;
    }

    private function calcTotal()
    {

        if (!($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }

        $points = $this->pointsCollectionFactory->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            ['eq' => $customerId]
        )->addFieldToFilter(
            'is_deleted',
            ['eq' => 0]
        )->addFieldToFilter(
            'is_expired',
            ['eq' => 0]
        )->setOrder(
            'created_at',
            'desc'
        );

        if ($this->isExpirationEnabled()) {
            $points->addExpiredField($this->getExpirationDays());
        }

        foreach ($points as $item) {
            $this->totalPoints += $item->getPoints();
        }
    }


    public function getRewardPointsTotal()
    {
        return $this->totalPoints;
    }

    public function getRewardPointsCompanyTotal()
    {
        $total = 0;
        if (!($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }
        $companyId = $this->_customerRepository->getCompanyIdByCustomerId($customerId);

        $collection = $this->companyCollectionFactory->create();
        $collection->addFieldToFilter(
            'main_table.company_id',
            ['eq' => $companyId]
        );
        $collection->getSelect()->joinLeft(
            ['rl' => 'brandsmith_companymanage_customer'],
            '`main_table`.company_id = rl.company_id',
            ['related_id']
        )->joinLeft(
            ['history_rl' => 'brandsmith_points_history'],
            '`rl`.related_id = history_rl.customer_id',
            ['points']
        );

        $collection->getSelect()->columns(['total' => new \Zend_Db_Expr('SUM(points)')])->group('main_table.company_id');

        foreach($collection as $item) {
            $total = $item->getTotal();
        }

        return $total;
    }


    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getPoints()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'points.history.pager'
            )->setCollection(
                $this->getPoints()
            );
            $this->setChild('pager', $pager);
            $this->getPoints()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    /**
     * @param string $name
     * @param string $reason
     * @return string
     */
    public function prepareReason($name, $reason)
    {
        if (!empty($reason)) {
            return $reason;
        }

        return $name;
    }

    /**
     * Check if is expiration logic is enable
     * @return bool
     */
    public function isExpirationEnabled()
    {
        if ($this->isExpirationEnable != null) {
            return $this->isExpirationEnable;
        }

        $isExpirationEnable = (bool)$this->_scopeConfig
            ->getValue(PointsHistory::POINTS_EXPIRATION_XML, ScopeInterface::SCOPE_STORE);
        $this->isExpirationEnable = $isExpirationEnable;
        if ($isExpirationEnable) {
            return $this->isExpirationEnable;
        }

        return $this->isExpirationEnable;
    }

    public function getExpirationDays()
    {
        return $this->_scopeConfig
            ->getValue(PointsHistory::POINTS_EXPIRATION_PERIOD_XML, ScopeInterface::SCOPE_STORE);
    }
}
