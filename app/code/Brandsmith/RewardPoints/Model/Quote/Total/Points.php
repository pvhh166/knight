<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model\Quote\Total;

use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;

/**
 * Class Points
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class Points extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    protected $_storeManager;

    private $ruleValidator;

    private $pointsHelper;
    /**
     * Custom constructor.
     *
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->_storeManager = $storeManager;
        $this->ruleValidator = $ruleValidator;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->pointsHelper = $pointsHelper;
    }

    /**
     * @param Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this|bool
     */
    public function collect(
        Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $address = $shippingAssignment->getShipping()->getAddress();

        if (!$quote->isVirtual()) {
            if ($address->getAddressType() != 'shipping') {
                return $this;
            }
        }

        $baseDiscount = $quote->getRewardPointsAmount();
        //$baseDiscount += $quote->getDiscountAmount();
        $discount = $this->priceCurrency->convert($baseDiscount);

        $total->addTotalAmount('reward_points', -$discount);
        $total->addBaseTotalAmount('reward_points', -$discount);


        //$total->setBaseGrandTotal($total->getBaseGrandTotal() - $discount);

        return $this;
    }

    /**
     * @param Quote $quote
     * @param Total $total
     * @return array
     */
    public function fetch(Quote $quote, Total $total)
    {

        $result = [];
        $amount = $quote->getRewardPointsAmount();
        if(abs($amount) == 0) {
            $points = $this->getEarnPoints($quote);
            $result = [
                'code' => 'earn_points',
                'title' => __('Reward Points'),
                'value' => $points
            ];
        }else {
            $result = [
                'code' => 'reward_points',
                'title' => __('Reward Points'),
                'value' => -$amount
            ];
        }


        return $result;
    }

    private function getEarnPoints($quote) {
        $totalPoints = 0;
        $store = $this->_storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $items = $quote->getItems();
        $ids = [];
        $subtotal = $quote->getSubtotal();
        $exceptCates = $this->pointsHelper->getExceptCategory();
        $exceptCates = explode(",",$exceptCates);
        if(count($items) > 0) {
            foreach ($items as $item) {
                $productCates = $item->getProduct()->getCategoryIds();
                foreach ($exceptCates as $cateId) {
                    if(in_array($cateId, $productCates)) {
                        $subtotal = $subtotal - ($item->getPrice()*$item->getQty());
                        continue;
                    }
                }
                $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
            }
            $ids = array_unique($ids);

            $totalPoints = 0;
            foreach ($ids as $ruleId) {
                /** @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule */
                $rule = $this->earningRuleFactory->create()->load($ruleId);
                $points = floor($subtotal / $rule->getSpend() * $rule->getEarn());
                $totalPoints += $points;
            }
        }
        return $totalPoints;
    }

    /**
     * Get Subtotal label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Discount');
    }
}
