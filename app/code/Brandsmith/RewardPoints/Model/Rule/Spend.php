<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model\Rule;

use Brandsmith\RewardPoints\Api\Data\RuleSpendInterface;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Class Spend
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Spend extends MagentoAbstractModel implements RuleSpendInterface, IdentityInterface
{

    /**#@+
     * Page's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Store Locator page cache tag
     */
    const CACHE_TAG = 'brandsmith_points_rule_spend';

    /**
     * @var string
     */
    protected $_cacheTag = 'brandsmith_points_rule_spend';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'brandsmith_points_rule_spend';

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Brandsmith\RewardPoints\Model\ResourceModel\Rule\Spend');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::RULE_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::RULE_ID, $id);
    }

    /**
     * Prepare statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}
