<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model;

/**
 * Class RegistryConstants
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class RegistryConstants
{
    /**
     * Key for current rule in registry
     */
    const CURRENT_REWARD_POINTS_RULE = 'current_reward_points_earning_rule';
}
