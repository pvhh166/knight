<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\DB\Select;
use Brandsmith\RewardPoints\Model\ResourceModel\Rule\Earning\Collection;
use Brandsmith\RewardPoints\Model\ResourceModel\Rule\Earning\CollectionFactory;
use Brandsmith\RewardPoints\Model\Rule\Earning;

/**
 * Class RewardConfigProvider
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class RewardConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{

    private $ruleValidator;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * @var \Brandsmith\RewardPoints\Helper\Data
     */
    private $pointsHelper;

    /**
     * @var null
     */
    private $points = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Collection
     */
    protected $collectionEarningRule;

    /**
     * @var \Brandsmith\RewardPoints\Model\Rule\EarningFactory
     */
    private $earningRuleFactory;

    protected $_orderCollectionFactory;

    public function __construct(
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        \Brandsmith\RewardPoints\Model\ResourceModel\Rule\Earning\CollectionFactory $collectionEarningRule,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->ruleValidator = $ruleValidator;
        $this->collectionEarningRule = $collectionEarningRule;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->historyCollectionFactory = $historyCollectionFactory;
        $this->pointsHelper = $pointsHelper;
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }

    public function getLastCustomerOrder() {
        if (!$this->customerSession->isLoggedIn()) {
            return 0;
        }
        $customerId = $this->customerSession->getCustomerId();
        $orders = $this->_orderCollectionFactory->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            $customerId
        )->setOrder(
            'created_at',
            'desc'
        );
        if(count($orders) > 0) {
            $lastOrder = $orders->setPageSize(1) // only get 10 products
            ->setCurPage(1)  // first page (means limit 0,10)
            ->load();
            $lastOrder = $lastOrder->getFirstItem();
            $points = $lastOrder->getRewardPointsAmount();
            if($points > 0) {
                return 1;
            }
        }
        return 0;
    }

    public function getConfig()
    {
        $amount = $this->checkoutSession->getQuote()->getRewardPointsAmount();
        $selectedPoints = $this->checkoutSession->getQuote()->getRewardPoints();
        $availablePoints = $this->getAvailablePoints();
        $maxPoints = $this->getMaxPoints();
        $percentReward = $this->getPercentRewards();
        $earnPoints = $this->getEarnPoints();
        $lastSelectedReward = $this->getLastCustomerOrder();
        if ($availablePoints > $maxPoints) {
            $points = $maxPoints;
        } else {
            $points = $availablePoints;
        }
        $selectedPoints = $selectedPoints ? $selectedPoints : 0;
        $output['reward'] = [
            'amount' => $amount,
            'selected_points' => $selectedPoints,
            'available_points' => $points,
            'max_points' => $this->getAvailablePoints(),
            'exchange' => $this->exchange,
            'is_visible' => $this->isVisible(),
            'percentReward' => $percentReward,
            'earnPoints' => $earnPoints,
            'last_selected_reward' => $lastSelectedReward
        ];
        return $output;
    }

    private function getPercentRewards() {
        $quote = $this->checkoutSession->getQuote();
        $store = $this->_storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $items = $quote->getItems();
        $ids = [];
        foreach ($items as $item) {
            $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
        }
        $ids = array_unique($ids);
        $name = "";
        foreach ($ids as $ruleId) {
            /** @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule */
            $rule = $this->earningRuleFactory->create()->load($ruleId);
            $name = $rule->getName();
            break;
        }
        $percent = explode("%",$name)[0];
        return "-".$percent.'%';
    }

    private function getEarnPoints() {
        $quote = $this->checkoutSession->getQuote();
        $store = $this->_storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $subtotal = $quote->getSubtotal();
        $items = $quote->getItems();
        $ids = [];
        $exceptCates = $this->pointsHelper->getExceptCategory();
        $exceptCates = explode(",",$exceptCates);
        foreach ($items as $item) {
            $productCates = $item->getProduct()->getCategoryIds();
            foreach ($exceptCates as $cateId) {
                if(in_array($cateId, $productCates)) {
                    $subtotal = $subtotal - ($item->getPrice()*$item->getQty());
                    continue;
                }
            }
            $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
        }
        $ids = array_unique($ids);


        $totalPoints = 0;
        foreach ($ids as $ruleId) {
            /** @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule */
            $rule = $this->earningRuleFactory->create()->load($ruleId);

            $points = floor($subtotal / $rule->getSpend() * $rule->getEarn());

            $totalPoints += $points;
        }
        return $totalPoints;
    }


    private function getVisibleReward() {

        //apply rules
        $quote = $this->checkoutSession->getQuote();
        $store = $this->_storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $items = $quote->getItems();
        $ids = [];
        foreach ($items as $item) {
            $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
        }
        $ids = array_unique($ids);
        return count($ids);
    }

    private function getAvailablePoints()
    {
        if (!$this->customerSession->isLoggedIn()) {
            $this->points = 0;
            return $this->points;
        }

        if ($this->points == null) {
            /** @var \Brandsmith\RewardPoints\Model\ResourceModel\History\Collection $collection */
            $collection = $this->historyCollectionFactory->create();
            $collection->getSelect()->reset(Select::COLUMNS)
                ->columns(['total' => new \Zend_Db_Expr('SUM(points)')])->group('customer_id');
            $collection->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomerId()]);
            $collection->load();
            $item = $collection->fetchItem();
            if ($item) {
                $this->points = $item->getData('total');
            }
            //$this->convertToCurrency($this->points);
        }
        return $this->points;
    }

    private function getMaxPoints()
    {
        $subtotal = $this->checkoutSession->getQuote()->getSubtotal();
        $rule = $this->getConvertRule();
        if (!$rule) {
            $this->exchange = 0;
            return 0;
        }
        $exchange = $rule->getPoints() / $rule->getAmount();
        $maxPoints = $subtotal * $exchange;
        $this->exchange = $exchange;
        return $maxPoints;
    }

    private function getConvertRule()
    {
        $customerGroupId = $this->customerSession->getCustomer()->getGroupId();

        return $this->pointsHelper->getRule($this->checkoutSession->getQuote()->getStoreId(), $customerGroupId);
    }

    private function isVisible()
    {
        if (!$this->customerSession->isLoggedIn()) {
            return false;
        }

        if (!$this->pointsHelper->isEnabled()) {
            return false;
        }

        //check rules for this store and customer group
        $customerGroupId = $this->customerSession->getCustomer()->getGroupId();
        $rule = $this->pointsHelper->getRule($this->_storeManager->getStore()->getId(), $customerGroupId);
        if (!$rule) {
            return false;
        }

        if(!$this->getVisibleReward()) {
            return false;
        }

        return true;
    }
}
