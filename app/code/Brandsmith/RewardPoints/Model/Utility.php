<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model;

/**
 * Class Utility
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Utility
{
    /**
     * Check if rule can be applied for specific address/quote/customer
     *
     * @param  \Brandsmith\RewardPoints\Model\Rule\Earning $rule
     * @param  \Magento\Quote\Model\Quote\Address         $address
     * @return bool
     */
    public function canProcessRule($rule, $address)
    {
        if ($rule->hasIsValidForAddress($address) && !$address->isObjectNew()) {
            return $rule->getIsValidForAddress($address);
        }

        $rule->afterLoad();
        /**
         * quote does not meet rule's conditions
         */
        if (!$rule->validate($address)) {
            $rule->setIsValidForAddress($address, false);
            return false;
        }
        /**
         * passed all validations, remember to be valid
         */
        $rule->setIsValidForAddress($address, true);
        return true;
    }
}
