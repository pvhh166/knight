<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model\Order\Invoice\Total;

/**
 * Class Points
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class Points extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $points = $invoice->getOrder()->getRewardPointsAmount();
        $invoice->setGrandTotal($invoice->getGrandTotal() - $points);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $points);
        return $this;
    }
}
