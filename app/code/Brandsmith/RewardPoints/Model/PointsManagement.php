<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model;

use Brandsmith\RewardPoints\Api\PointsManagementInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DB\Select;
use Magento\Checkout\Model\Session as CheckoutSession;


/**
 * Class PointsManagement
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class PointsManagement implements PointsManagementInterface
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * @var \Brandsmith\RewardPoints\Helper\Data
     */
    private $pointsHelper;

    private $rule;

    private $ruleValidator;

    private $storeManager;

    private $earningRuleFactory;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * PointsManagement constructor.
     *
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Brandsmith\RewardPoints\Helper\Data        $pointsHelper
     * @param ResourceModel\History\CollectionFactory    $historyCollectionFactory
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Brandsmith\RewardPoints\Helper\Data $pointsHelper,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->pointsHelper = $pointsHelper;
        $this->historyCollectionFactory = $historyCollectionFactory;
        $this->ruleValidator = $ruleValidator;
        $this->storeManager = $storeManager;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param int    $cartId
     * @param string $rewardPoints
     * @return bool
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function set($cartId, $rewardPoints)
    {

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
        }
        $quote->getShippingAddress()->setCollectShippingRates(true);
        $amount = 0;
        $store = $this->storeManager->getStore($quote->getStoreId());

        $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
            ->setWebsiteId($store->getWebsiteId());

        $quote_subtotal = $quote->getSubtotal();
        $items = $quote->getItems();
        $ids = [];

        $exceptCates = $this->pointsHelper->getExceptCategory();
        $exceptCates = explode(",",$exceptCates);
        foreach ($items as $item) {
            $productCates = $item->getProduct()->getCategoryIds();
            foreach ($exceptCates as $cateId) {
                if(in_array($cateId, $productCates)) {
                    $quote_subtotal = $quote_subtotal - ($item->getPrice()*$item->getQty());
                    continue;
                }
            }
            $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
            $quote_subtotal = $quote_subtotal - $item->getDiscountAmount();
        }

        $ids = array_unique($ids);

        foreach ($ids as $ruleId) {
            // @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule
            $rule = $this->earningRuleFactory->create()->load($ruleId);
            $name = $rule->getName();
            $percent = explode("%",$name)[0];
            if(is_numeric ( $percent ))
                $amount  = $quote_subtotal * $percent / 100;
            try {
                //$amount = $this->_validate($rewardPoints, $quote);
                //$quote->setDiscountAmount($amount);
                // $quote->setRewardPoints($rewardPoints);
                if($rewardPoints == 'discount') {
                    $quote->setRewardPointsAmount($amount);
                    $quote->setUsedDiscount(1);
                }
                if($rewardPoints == 'reward') {
                    $quote->setRewardPointsAmount(0);
                    $quote->setUsedDiscount(2);
                }
                $quote->setRewardPointsRule($ruleId);
                $this->quoteRepository->save($quote->collectTotals());
                $this->checkoutSession->replaceQuote($quote);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Could not apply reward points'));
            }


        }


        /*if($rewardPoints == "discount") {
            $rewardPoints = 0;
            $store = $this->storeManager->getStore($quote->getStoreId());
        
            $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
                ->setWebsiteId($store->getWebsiteId());
        
            $items = $quote->getItems();
            $ids = [];
            foreach ($items as $item) {
                $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
            }
            $ids = array_unique($ids);
        
            foreach ($ids as $ruleId) {
                // @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule
                $rule = $this->earningRuleFactory->create()->load($ruleId);
                $name = $rule->getName();
                $percent = explode("%",$name)[0];
                if(is_numeric ( $percent ))
                    $rewardPoints  = $quote->getSubtotal() * $percent / 100;
                try {
                    //$amount = $this->_validate($rewardPoints, $quote);
                    //$quote->setDiscountAmount($amount);
                   // $quote->setRewardPoints($rewardPoints);
                    $amount = $rewardPoints;
                    $quote->setRewardPointsAmount($amount);
                    $quote->setRewardPointsRule($ruleId);
                    $this->quoteRepository->save($quote->collectTotals());
                } catch (\Exception $e) {
                    throw new CouldNotSaveException(__('Could not apply reward points'));
                }

        
            }
        }else {
            try {
                $amount = $this->_validate($rewardPoints, $quote);
                $quote->setRewardPointsAmount($amount);
                $quote->setRewardPoints($rewardPoints);
                $quote->setRewardPointsRule($this->rule->getId());
                $this->quoteRepository->save($quote->collectTotals());
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Could not apply reward points'));
            }
        }
        */
        return true;
    }

    /**
     * @param $points
     * @param $quote
     * @return float
     * @throws \Exception
     */
    private function _validate($points, $quote)
    {
        if ($points > $this->getAvailablePoints($quote)) {
            throw new \Exception(__('Could not apply reward points'));
        }

        $rule = $this->pointsHelper->getRule($quote->getStoreId(), $quote->getCustomerGroupId());

        $this->rule = $rule;

        $subtotal = $quote->getSubtotal();

        $exchange = $rule->getPoints() / $rule->getAmount();

        $amount = round($points / $exchange, 2);

        if ($amount > $subtotal) {
            throw new \Exception(__('Could not apply reward points'));
        }

        return $amount;
    }

    /**
     * @param $quote
     * @return mixed
     */
    private function getAvailablePoints($quote)
    {

        /** @var \Brandsmith\RewardPoints\Model\ResourceModel\History\Collection $collection */
        $collection = $this->historyCollectionFactory->create();
        $collection->getSelect()->reset(Select::COLUMNS)
            ->columns(['total' => new \Zend_Db_Expr('SUM(points)')])->group('customer_id');
        $collection->addFieldToFilter('customer_id', ['eq' => $quote->getCustomerId()]);
        $collection->load();
        $item = $collection->fetchItem();
        return $item->getData('total');
    }
}
