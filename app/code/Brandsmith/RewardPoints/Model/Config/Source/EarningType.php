<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class EarningType
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class EarningType implements ArrayInterface
{

    public function toOptionArray()
    {
        $options = [
            0 => [
                'label' => 'Please select',
                'value' => 0
            ],
            1 => [
                'label' => __('Fixed'),
                'value' => 1
            ],
            2 => [
                'label' => __('For Each X Spend Customer Receive Y Points'),
                'value' => 2
            ],
            3 => [
                'label' => __('Percent'),
                'value' => 3
            ],
        ];

        return $options;
    }

}
