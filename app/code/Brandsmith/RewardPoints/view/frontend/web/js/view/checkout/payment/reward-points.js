/*
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */
define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Brandsmith_RewardPoints/js/action/set-reward-points',
        'Brandsmith_RewardPoints/js/action/set-discount',
        'Magento_Checkout/js/model/totals',
        'mage/translate',
        'jquery/ui'
    ],
    function ($, ko, Component, quote, setRewardPointsAction,setDiscountAction, totals, $t) {
        'use strict';

        var totals = quote.getTotals(),
            rewardPoints = ko.observable(null),
            isApplied = ko.observable(rewardPoints() != null),
            isVisible  = ko.observable(null);
        console.log(window.checkoutConfig.reward);

        if (totals()) {
            var value = parseInt(window.checkoutConfig.reward.selected_points);
            rewardPoints(value);
            isVisible(window.checkoutConfig.reward.is_visible);
        }
        $(document).on("click","input[name='wccf']",function() {
            var radioValue = $("input[name='wccf']:checked").val();
            if(radioValue == 'checkout_discount') {
                setDiscountAction("discount", isApplied);
            }else {
                setDiscountAction("reward", isApplied);
            }
        });


        return Component.extend(
            {
                defaults: {
                    template: 'Brandsmith_RewardPoints/checkout/payment/reward-points'
                },

                rewardPoints: rewardPoints,

                /**
                 * Applied flag
                 */
                isApplied: isApplied,

                isVisible: isVisible,

                /**
                 * RP application procedure
                 */
                apply: function () {
                    if (this.validate()) {
                        setRewardPointsAction(rewardPoints(), isApplied);
                    }
                },
                kupply: function (x) {
                    console.log(x);
                    /*if(x == '1') {
                        setDiscountAction("discount", isApplied);
                    }else {
                        setDiscountAction("reward", isApplied);
                    }*/
                    //return true;
                },

                /**
                 * Points form validation
                 *
                 * @returns {Boolean}
                 */
                validate: function () {
                    var form = '#reward-points-form';

                    return $(form).validation() && $(form).validation('isValid');
                },

                initSlider: function () {
                    var maxValue = parseInt(window.checkoutConfig.reward.available_points),
                        value = parseInt(window.checkoutConfig.reward.selected_points);

                    $("#reward-slider").slider(
                        {
                            classes: {
                                "ui-slider": "highlight"
                            },
                            range: "max",
                            min: 0,
                            max: maxValue,
                            value: value,
                            slide: function (event, ui) {
                                $("#reward-points").val(ui.value);
                                rewardPoints(ui.value);
                            },
                            stop: function (event, ui) {

                            }
                        }
                    );
                    $("#reward-points").val(value);
                },


                getCurrentPointsText: function(){
                    return $.mage.__('You have %1 point(s). You can spend maximum %2 point(s).')
                        .replace('%1', window.checkoutConfig.reward.max_points)
                        .replace('%2', window.checkoutConfig.reward.available_points);
                    //return $t('You have %1 points.(%2)', 1,2);
                },

                getPercentRewards: function(){
                    return $.mage.__(window.checkoutConfig.reward.percentReward);
                },
                getEarnPoints: function(){
                    return $.mage.__(window.checkoutConfig.reward.earnPoints);
                },
                getChecked: function(x){
                    var amount = window.checkoutConfig.reward.last_selected_reward;
                    if(x == 1) { // Set checked for discount radio
                        if(amount > 0) {
                            setDiscountAction("discount", isApplied);
                            return true;
                        }else {
                            return false;
                        }
                    }else { // Set checked for reward points
                        if(amount > 0) {
                            return false;
                        }else {
                            setDiscountAction("reward", isApplied);
                            return true;
                        }
                    }
                }

            }
        );
    }
);
