/*
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, quote, priceUtils, totals) {
        "use strict";
        return Component.extend(
            {
                defaults: {
                    template: 'Brandsmith_RewardPoints/checkout/summary/earn-points'
                },

                totals: quote.getTotals(),

                isDisplayedEarnPoints: function () {
                    //return 0;
                    var price = 0;

                    if (this.totals()) {
                        if(totals.getSegment('earn_points'))
                             price = totals.getSegment('earn_points').value;
                    }
                    return price;
                },

                getEarnPoints: function () {

                    var price = 0;
                    if (this.totals()) {
                        if(totals.getSegment('earn_points'))
                            price = totals.getSegment('earn_points').value;
                    }
                    return price + ' points';
                },

                getBaseValue: function () {
                    var price = 0;
                    if (this.totals()) {
                        price = this.totals().reward_points;
                    }
                    return priceUtils.formatPrice(price, quote.getBasePriceFormat());
                }
            }
        );
    }
);
