/*
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            "RewardPoints":'Brandsmith_RewardPoints/js/points'
        }
    }
};
