<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Controller\Adminhtml\Customer;

use Magento\Customer\Controller\Adminhtml\Index;

/**
 * Class History
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class History extends Index
{
    /**
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
