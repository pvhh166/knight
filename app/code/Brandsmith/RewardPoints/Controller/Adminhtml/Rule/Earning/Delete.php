<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Brandsmith\RewardPoints\Controller\Adminhtml\Rule\Earning;

use Brandsmith\RewardPoints\Controller\Adminhtml\Rule\AbstractRule;

/**
 * Class Delete
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Delete extends AbstractRule
{
    const ADMIN_RESOURCE = 'Brandsmith_RewardPoints::rule_earning_delete';
    /**
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->earningRuleFactory->create();
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('You deleted the rule.'));
                $this->_redirect('points/rule_earning/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('We can\'t delete the rule right now. Please review the log and try again.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_redirect('points/rule_earning/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->messageManager->addError(__('We can\'t find a rule to delete.'));
        $this->_redirect('points/rule_earning/');
    }
}
