<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Controller\Adminhtml\History;

use \Magento\Backend\App\Action;

/**
 * Class Delete
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Delete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Brandsmith_RewardPoints::history_delete';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Brandsmith\RewardPoints\Model\HistoryFactory
     */
    private $historyFactory;

    /**
     * @var \Brandsmith\RewardPoints\Model\ResourceModel\History
     */
    private $resourceHistory;

    /**
     * Delete constructor.
     *
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Brandsmith\RewardPoints\Model\HistoryFactory $historyFactory
     * @param \Brandsmith\RewardPoints\Model\ResourceModel\History $resourceHistory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brandsmith\RewardPoints\Model\HistoryFactory $historyFactory,
        \Brandsmith\RewardPoints\Model\ResourceModel\History $resourceHistory
    ) {

        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->historyFactory = $historyFactory;
        $this->resourceHistory = $resourceHistory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('history_id');

        if ($id) {
            try {
                // init model and delete

                /** @var \Brandsmith\RewardPoints\Model\History $model */
                $history = $this->historyFactory->create();
                $this->resourceHistory->load($history, $id);

                $history->setIsDeleted(1);
                $this->resourceHistory->save($history);

                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the history item.'));

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());

                // go back to edit form
                return $resultRedirect->setPath('*/*/*');
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a history item to delete.'));

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
