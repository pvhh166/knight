<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Brandsmith\RewardPoints\Controller\History;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Index extends \Brandsmith\RewardPoints\Controller\AbstractIndex
{

    /**
     * Display customer reward points history
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
