<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Controller;

use Magento\Framework\App\Action;

/**
 * Class AbstractIndex
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
abstract class AbstractIndex extends Action\Action implements IndexInterface
{
}
