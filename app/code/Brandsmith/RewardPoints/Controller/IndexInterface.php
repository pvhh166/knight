<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\RewardPoints\Controller;

/**
 * Interface IndexInterface
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 * @api
 */
interface IndexInterface extends \Magento\Framework\App\ActionInterface
{

}
