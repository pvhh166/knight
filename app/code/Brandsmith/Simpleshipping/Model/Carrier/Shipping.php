<?php
namespace Brandsmith\Simpleshipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Shipping extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'simpleshipping';

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * Shipping constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface          $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory  $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                    $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory                  $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * get allowed methods
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @return float
     */
    private function getShippingPrice()
    {
        $configPrice = $this->getConfigData('price');

        $abovePrice = $this->getConfigData('price_above');

        $belowPrice = $this->getConfigData('price_bellow');

        $bob = $configPrice + $abovePrice;

        $shippingPrice = $this->getFinalPriceWithHandlingFee($bob);

        return $shippingPrice;
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $allItems = $request->getAllItems();

        $total = 0;

        foreach($allItems as $item) {

			//$this->_logger->notice('Start of Shipping Fee Calculation');
			$message = $item->debug();
			//$this->_logger->notice(json_encode($message, JSON_PRETTY_PRINT));

            if (!$item->getParentItem() && $item->getProductType() != 'super') {
				//$this->_logger->notice('Item Quantity ' . $item->getQty());
				$qty = $item->getQty();
				
				$product = $item->getProduct();
				$tierprice = $product->getTierPrice();
				$pricing = $tierprice ? $tierprice : $item->getPrice();
				
				if(is_array($pricing) && count($pricing) > 0){
					//$price = $pricing[0]['price'];
                    $price = isset($pricing[0]) ? $pricing[0]['price'] : 0;
					//$this->_logger->notice('Using Tier Price for Shipping ' . $price);
				} else {
					$price = $pricing;
					//$this->_logger->notice('No Tier Price found so using Base Price for Shipping ' . $price);
				}
				
                if ($price < 200) {
                    $fee = 4 * $qty;
				}
                
				if ($price >= 200) {
                    $fee = 10 * $qty;
                }

                $total += $fee;
                //$this->_logger->notice('Shipping Fee ' . $fee);
				//$this->_logger->notice('Standard Item Type and Sku ' . $item->getProductType() . '  ' . $item->getSku());
			}
			
			if ($item->getProductType() == 'super') {
				
				$superqty = $item->getQty();
				
				foreach($item->getChildren() as $child) {
					//$this->_logger->notice('Child Item Quantity ' . $child->getQty());
					$qty = $child->getQty();
				
					$product = $child->getProduct();
					$tierprice = $product->getTierPrice();
					$pricing = $tierprice ? $tierprice : $item->getPrice();
				
					if(is_array($pricing) && count($pricing) > 0){
                        //$price = $pricing[0]['price'];
						$price = isset($pricing[0]) ? $pricing[0]['price'] : 0;
						//$this->_logger->notice('Using Tier Price for Shipping ' . $price);
					} else {
						$price = $pricing;
						//$this->_logger->notice('No Tier Price found so using Base Price for Shipping ' . $price);
					}
				
					if ($price < 200) {
						$fee = (4 * $qty) * $superqty;
					}
                
					if ($price >= 200) {
						$fee = (10 * $qty) * $superqty;
					}

					$total += $fee;
					//$this->_logger->notice('Shipping Fee ' . $fee);
					//$this->_logger->notice('Child Item Type and Sku ' . $child->getProductType() . '  ' . $child->getSku());
				}
			}
			
			//$this->_logger->notice('End of Shipping Fee Calculation');
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        //$amount = $this->getShippingPrice();
        $amount = $total;

        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }
}