<?php


namespace Brandsmith\CompanyManage\Api\Data;

interface CustomerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Customer list.
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface[]
     */
    public function getItems();

    /**
     * Set company_id list.
     * @param \Brandsmith\CompanyManage\Api\Data\CustomerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
