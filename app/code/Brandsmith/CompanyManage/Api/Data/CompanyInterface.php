<?php


namespace Brandsmith\CompanyManage\Api\Data;

interface CompanyInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const COMPANY_PHONE = 'company_phone';
    const POSITION = 'position';
    const COMPANY_ID = 'company_id';
    const UPDATED_AT = 'updated_at';
    const COMPANY_COUNTRY_ID = 'company_country_id';
    const COMPANY_NAME = 'company_name';
    const COMPANY_CODE = 'company_code';
    const COMPANY_POSTCODE = 'company_postcode';
    const COMPANY_REGION_ID = 'company_region_id';
    const COMPANY_CITY = 'company_city';
    const CREATED_AT = 'created_at';
    const COMPANY_STREET = 'company_street';
    const COMPANY_REGION = 'company_region';

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId();

    /**
     * Set company_id
     * @param string $companyId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyId($companyId);

    /**
     * Get company_code
     * @return string|null
     */
    public function getCompanyCode();

    /**
     * Set company_code
     * @param string $companyCode
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCode($companyCode);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface $extensionAttributes
    );

    /**
     * Get company_name
     * @return string|null
     */
    public function getCompanyName();

    /**
     * Set company_name
     * @param string $companyName
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyName($companyName);

    /**
     * Get company_phone
     * @return string|null
     */
    public function getCompanyPhone();

    /**
     * Set company_phone
     * @param string $companyPhone
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyPhone($companyPhone);

    /**
     * Get company_street
     * @return string|null
     */
    public function getCompanyStreet();

    /**
     * Set company_street
     * @param string $companyStreet
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyStreet($companyStreet);

    /**
     * Get company_city
     * @return string|null
     */
    public function getCompanyCity();

    /**
     * Set company_city
     * @param string $companyCity
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCity($companyCity);

    /**
     * Get company_country_id
     * @return string|null
     */
    public function getCompanyCountryId();

    /**
     * Set company_country_id
     * @param string $companyCountryId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCountryId($companyCountryId);

    /**
     * Get company_region
     * @return string|null
     */
    public function getCompanyRegion();

    /**
     * Set company_region
     * @param string $companyRegion
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyRegion($companyRegion);

    /**
     * Get company_region_id
     * @return string|null
     */
    public function getCompanyRegionId();

    /**
     * Set company_region_id
     * @param string $companyRegionId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyRegionId($companyRegionId);

    /**
     * Get company_postcode
     * @return string|null
     */
    public function getCompanyPostcode();

    /**
     * Set company_postcode
     * @param string $companyPostcode
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyPostcode($companyPostcode);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get position
     * @return string|null
     */
    public function getPosition();

    /**
     * Set position
     * @param string $position
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setPosition($position);
}
