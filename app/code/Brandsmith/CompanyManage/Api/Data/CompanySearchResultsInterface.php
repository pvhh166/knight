<?php


namespace Brandsmith\CompanyManage\Api\Data;

interface CompanySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Company list.
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface[]
     */
    public function getItems();

    /**
     * Set company_code list.
     * @param \Brandsmith\CompanyManage\Api\Data\CompanyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
