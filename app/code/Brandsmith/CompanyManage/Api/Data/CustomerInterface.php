<?php


namespace Brandsmith\CompanyManage\Api\Data;

interface CustomerInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMER_ID = 'customer_id';
    const POSITION = 'position';
    const COMPANY_ID = 'company_id';
    const RELATED_ID = 'related_id';

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId();

    /**
     * Set company_id
     * @param string $companyId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setCompanyId($companyId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface $extensionAttributes
    );

    /**
     * Get related_id
     * @return string|null
     */
    public function getRelatedId();

    /**
     * Set related_id
     * @param string $relatedId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setRelatedId($relatedId);

    /**
     * Get position
     * @return string|null
     */
    public function getPosition();

    /**
     * Set position
     * @param string $position
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setPosition($position);
}
