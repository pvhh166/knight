<?php


namespace Brandsmith\CompanyManage\Controller\Adminhtml\Company;

use Magento\Framework\App\Filesystem\DirectoryList;
use Brandsmith\CompanyManage\Model\ResourceModel\Company\CollectionFactory;

class Generate extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    protected $_fileFactory;

    protected $directory;

    protected $collection;

    protected $customercomp;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        CollectionFactory $collectionFactory,
        \Brandsmith\CompanyManage\Model\ResourceModel\Customer\CollectionFactory $customercomp,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
        $this->collection = $collectionFactory->create();
        //$this->collection->addAttributeToSelect(array('company_id', 'company_code','company_name'));
        $this->collection->getSelect()->joinLeft(
            ['rl' => 'brandsmith_companymanage_customer'],
            '`main_table`.company_id = rl.company_id',
            ['related_id']
        )->joinLeft(
            ['history_rl' => 'brandsmith_points_history'],
            '`rl`.related_id = history_rl.customer_id',
            ['points']
        );

        $this->collection->getSelect()->columns(['total' => new \Zend_Db_Expr('SUM(points)')])->group('main_table.company_id');
        $this->customercomp = $customercomp;
     }

     /**
      * Save action
      *
      * @return \Magento\Framework\Controller\ResultInterface
      */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $request = $this->getRequest();
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $listcompany = $this->collection;
            $name = date('m_d_Y_H_i_s');
            $filepath = 'export/quarterly_' . $name . '.csv';
            $this->directory->create('export');
            /* Open file */
            $stream = $this->directory->openFile($filepath, 'w+');
            $stream->lock();
            $columns = $this->getColumnHeader();

            foreach ($columns as $column) {
                $header[] = $column;
            }
            /* Write Header */
            $stream->writeCsv($header);

            $csv_array = [];
            
            foreach($listcompany as $company) {
                //$logger->info($company['company_id']);
                $listCustomerByCompany = $this->customercomp->create();
                $listCustomerByCompany->getSelect()->joinLeft(
                    ['history_rl' => 'brandsmith_points_history'],
                    'main_table.related_id = history_rl.customer_id',
                    ['points'=> 'history_rl.points', 'customer_name' => 'history_rl.customer_name']
                )->joinLeft(
                    ['customer_rl' => 'customer_entity'],
                    'main_table.related_id = customer_rl.entity_id',
                    ['email'=> 'customer_rl.email', 'fname'=> 'customer_rl.firstname', 'customer_rl.lastname']
                )
                ;
                $listCustomerByCompany->addFieldToFilter('main_table.company_id', array('eq' => $company['company_id']));
                $listCustomerByCompany->getSelect()->columns(['total' => new \Zend_Db_Expr('SUM(points)')])->group('main_table.customer_id');

                foreach($listCustomerByCompany as $item) {
                    $content_array = [];
                    $content_array[] = $company['company_code'];
                    $content_array[] = $company['company_name'];
                    $content_array[] = $company['total'];
                    $content_array[] = $item['email'];
                    $content_array[] = $item['fname']. ' '. $item['lastname'];
                    $content_array[] = $item['total'];
                    $csv_array[] = $content_array;
                }
            }
            
            foreach ($csv_array as $item) {
                $itemData = [];
                $itemData[] = $item[0];
                $itemData[] = $item[1];
                $itemData[] = $item[2];
                $itemData[] = $item[3];
                $itemData[] = $item[4];
                $itemData[] = $item[5];
                $stream->writeCsv($itemData);
            }
            $stream->unlock();
            $stream->close();

            $content = [];
            $content['type'] = 'filename'; // must keep filename
            $content['value'] = $filepath;
            $content['rm'] = '1'; //remove csv from var folder

            $csvfilename = 'QuarterlyReport_'.$name. '.csv';
            return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /* Header Columns */
    public function getColumnHeader() {
        $headers = ['Company Code','Company Name','Company Points','Customer Email','Customer Name','Customer Points'];
        return $headers;
    }

}
