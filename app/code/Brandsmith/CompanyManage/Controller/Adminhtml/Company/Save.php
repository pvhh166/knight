<?php


namespace Brandsmith\CompanyManage\Controller\Adminhtml\Company;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    protected $_customerRepository;

    protected $collectionFactory;

    protected $customerFactory;

    protected $customer;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Brandsmith\CompanyManage\Model\ResourceModel\Customer\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_customerRepository = $customerRepository;
        $this->collectionFactory = $collectionFactory;
        $this->customerFactory = $customerFactory;
        $this->customer = $customer;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $request = $this->getRequest();
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('company_id');
            $checkCustomerExistCompany = $this->checkCustomerExistCompany($request, $id);
            if(!empty($checkCustomerExistCompany)) {
                $this->messageManager->addErrorMessage(__($checkCustomerExistCompany. ' already belong to another company'));
                return $resultRedirect->setPath('*/*/edit', ['company_id' => $id]);
            }
            $model = $this->_objectManager->create(\Brandsmith\CompanyManage\Model\Company::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Company no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
        
            $model->setData($data);
        
            try {
                $this->_beforeSave($model, $request, $data['company_code']);
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Company.'));
                $this->dataPersistor->clear('brandsmith_companymanage_company');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['company_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Company.'));
            }
        
            $this->dataPersistor->set('brandsmith_companymanage_company', $data);
            return $resultRedirect->setPath('*/*/edit', ['company_id' => $this->getRequest()->getParam('company_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function checkCustomerExistCompany($request, $id) {
        $data = $request->getPost('data');
        $isExist = "";
        $links = isset($data['links']) ? $data['links'] : ['customer' => []];
        if (is_array($links)) {
            $linkType = 'customer';
            $cus_arr = [];
            if (isset($links[$linkType]) && is_array($links[$linkType])) {
                foreach ($links[$linkType] as $item) {
                    $cus_arr[] = $item['id'];
                }
            }
            $collection_cus = $this->collectionFactory->create();
            $collection_cus->addFieldToFilter('related_id', array('in' => $cus_arr));
            $collection_cus->addFieldToFilter('company_id', array('neq' => $id));
            if(count($collection_cus) > 0) {
                $list_cus = [];
                 foreach($collection_cus as $cus) {
                    $customer = $this->_customerRepository->getById($cus['related_id']);
                    if($customer) {
                        $list_cus[] = $customer->getEmail();
                    }
                }
                $isExist = implode(", ",$list_cus);
            }
        }

        return $isExist;
    }

    /**
     * Before model save
     * @param  \Magefan\Blog\Model\Post $model
     * @param  \Magento\Framework\App\Request\Http $request
     * @return void
     */
    protected function _beforeSave($model, $request, $company_code)
    {
        /* Prepare relative links */
        $data = $request->getPost('data');

        $links = isset($data['links']) ? $data['links'] : ['customer' => []];
        if (is_array($links)) {
            $linkType = 'customer';
            if (isset($links[$linkType]) && is_array($links[$linkType])) {
                $linksData = [];
                foreach ($links[$linkType] as $item) {
                    $customer = $this->_customerRepository->getById($item['id']);
                    if($customer){
                        $customerRes = $this->customer->load($item['id']);
                        $customerData = $customerRes->getDataModel();
                        $customerData->setCustomAttribute('comp_company_id',$company_code);
                        $customerRes->updateData($customerData);
                        $customerResource = $this->customerFactory->create();
                        $customerResource->saveAttribute($customerRes, 'comp_company_id');
                    }
                    $linksData[$item['id']] = [
                        'position' => isset($item['position']) ? $item['position'] : 0
                    ];
                }
                $links[$linkType] = $linksData;
            } else {
                $links[$linkType] = [];
            }


            $model->setData('links', $links);
        }
    }
}
