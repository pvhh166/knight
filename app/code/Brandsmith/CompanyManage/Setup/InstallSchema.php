<?php


namespace Brandsmith\CompanyManage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        $table_brandsmith_companymanage_company = $setup->getConnection()->newTable($setup->getTable('brandsmith_companymanage_company'));

        $table_brandsmith_companymanage_company->addColumn(
            'company_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true],
            'Entity ID'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_code'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_name'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_phone',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_phone'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_street',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_street'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_city'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_country_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_country_id'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_region',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_region'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_region_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'company_region_id'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'company_postcode',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'company_postcode'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
            ],
            'created_at'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
            ],
            'updated_at'
        );

        $table_brandsmith_companymanage_company->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'position'
        );

        $table_brandsmith_companymanage_company->addIndex(
            $installer->getIdxName('brandsmith_companymanage_company', ['company_code']),
            ['company_id']
        );

        $table_brandsmith_companymanage_company->addIndex(
            $setup->getIdxName(
                $installer->getTable('brandsmith_companymanage_company'),
                ['company_name', 'company_code'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['company_name', 'company_code'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'Company Table'
        );

        $table_brandsmith_companymanage_customer = $setup->getConnection()->newTable($setup->getTable('brandsmith_companymanage_customer'));

        $table_brandsmith_companymanage_customer->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false, 'primary' => true],
            'Entity ID'
        );

        $table_brandsmith_companymanage_customer->addColumn(
            'company_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'company_id'
        );

        $table_brandsmith_companymanage_customer->addColumn(
            'related_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Related Product ID'
        );

        $table_brandsmith_companymanage_customer->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'position'
        );

        $table_brandsmith_companymanage_customer->addIndex(
            $installer->getIdxName('brandsmith_companymanage_customer', ['related_id']),
            ['related_id']
        );

        $table_brandsmith_companymanage_customer->addForeignKey(
            $installer->getFkName('brandsmith_companymanage_customer', 'company_id', 'brandsmith_companymanage_company', 'company_id'),
            'company_id',
            $installer->getTable('brandsmith_companymanage_company'),
            'company_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $table_brandsmith_companymanage_customer->addForeignKey(
            $installer->getFkName('brandsmith_companymanage_customer', 'related_id', 'customer_entity', 'entity_id'),
            'related_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $setup->getConnection()->createTable($table_brandsmith_companymanage_customer);

        $setup->getConnection()->createTable($table_brandsmith_companymanage_company);

        $installer->endSetup();
    }
}
