<?php


namespace Brandsmith\CompanyManage\Model;

use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\CompanyManage\Api\Data\CompanyInterfaceFactory;
use Brandsmith\CompanyManage\Api\Data\CompanyInterface;
use Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory;

class Company extends \Magento\Framework\Model\AbstractModel
{

    protected $companyDataFactory;

    protected $_eventPrefix = 'brandsmith_companymanage_company';
    protected $dataObjectHelper;


    protected $_customerCollectionFactory;

    protected $_historyPointCollectionFactory;

    protected $requestHttp;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CompanyInterfaceFactory $companyDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\CompanyManage\Model\ResourceModel\Company $resource
     * @param \Brandsmith\CompanyManage\Model\ResourceModel\Company\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CompanyInterfaceFactory $companyDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Brandsmith\CompanyManage\Model\ResourceModel\Company $resource,
        \Brandsmith\CompanyManage\Model\ResourceModel\Company\Collection $resourceCollection,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyPointCollectionFactory,
        array $data = []
    ) {
        $this->companyDataFactory = $companyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_historyPointCollectionFactory = $historyPointCollectionFactory;
        $this->requestHttp = $requestHttp;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve company model with company data
     * @return CompanyInterface
     */
    public function getDataModel()
    {
        $companyData = $this->getData();
        
        $companyDataObject = $this->companyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $companyDataObject,
            $companyData,
            CompanyInterface::class
        );
        
        return $companyDataObject;
    }

    /**
     * Retrieve post related Customer
     * @return \Magento\Catalog\Model\ResourceModel\Customer\CollectionFactory
     */
    public function getRelatedCustomers()
    {
        if (!$this->hasData('related_customers')) {
            $collection = $this->_customerCollectionFactory->create();

            if ($this->getStoreId()) {
                $collection->addStoreFilter($this->getStoreId());
            }

            $collection->getSelect()->joinLeft(
                ['rl' => $this->getResource()->getTable('brandsmith_companymanage_customer')],
                'e.entity_id = rl.related_id',
                ['position']
            )->where(
                'rl.company_id = ?',
                $this->getId()
            );

            $this->setData('related_customers', $collection);
        }

        return $this->getData('related_customers');
    }

    /**
     * Retrieve post related Customer
     * @return \Magento\Catalog\Model\ResourceModel\Customer\CollectionFactory
     */
    public function getHistoryPointCustomerByCompany()
    {
        if (!$this->hasData('related_customers_history_point')) {
            $collection = $this->_historyPointCollectionFactory->create();

            $collection->getSelect()->joinLeft(
                ['rl' => $this->getResource()->getTable('brandsmith_companymanage_customer')],
                '`main_table`.customer_id = rl.related_id',
                ['position']
            )->where(
                'rl.company_id = ?',
                $this->getId()
            );
            $collection->getSelect()->joinLeft(
                ['rl_cus' => $this->getResource()->getTable('customer_entity')],
                '`main_table`.customer_id = rl_cus.entity_id',
                ['email']
            );
            $collection->getSelect()->joinLeft(
                ['rl_sales' => $this->getResource()->getTable('sales_order_grid')],
                '`main_table`.order_increment_id = rl_sales.increment_id',
                ['status']
            );

            $this->setData('related_customers_history_point', $collection);
        }

        return $this->getData('related_customers_history_point');
    }


    public function getCompanyId() {
        $params = $this->requestHttp->getParams();

        if(isset($params['company_id'])) {
            return $params['company_id'];
        }
        return 0;
    }
}
