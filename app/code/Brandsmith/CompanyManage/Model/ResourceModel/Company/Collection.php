<?php


namespace Brandsmith\CompanyManage\Model\ResourceModel\Company;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Brandsmith\CompanyManage\Model\Company::class,
            \Brandsmith\CompanyManage\Model\ResourceModel\Company::class
        );
    }
}
