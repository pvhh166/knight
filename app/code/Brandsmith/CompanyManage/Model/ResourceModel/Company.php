<?php


namespace Brandsmith\CompanyManage\Model\ResourceModel;

class Company extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    private $historyFactory;

    public function __construct(
        \Brandsmith\RewardPoints\Model\HistoryFactory $historyFactory,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
         $connectionName = null
    ) {
        $this->historyFactory = $historyFactory;
        parent::__construct($context, $connectionName);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('brandsmith_companymanage_company', 'company_id');
    }

    /**
     * Assign post to store views, categories, related posts, etc.
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        //$logger->info($links1);
        $links = $object->getData('links');
        /* Save related post & product links */
        if ($links) {
            if (is_array($links)) {
                $linkType = 'customer';
                if (isset($links[$linkType]) && is_array($links[$linkType])) {
                    $linksData = $links[$linkType];
                    $lookup = 'lookupRelated' . ucfirst($linkType) . 'Ids';
                    $oldIds = $this->$lookup($object->getId());
                    $this->_updateLinks(
                        $object,
                        array_keys($linksData),
                        $oldIds,
                        'brandsmith_companymanage_' . $linkType,
                        'related_id',
                        $linksData
                    );
                }
            }
        }

        /* Update reward history customer code */

        //$modelHistory = $this->historyFactory->create();


        return parent::_afterSave($object);
    }

    /**
     * Update post connections
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @param  Array $newRelatedIds
     * @param  Array $oldRelatedIds
     * @param  String $tableName
     * @param  String  $field
     * @param  Array  $rowData
     * @return void
     */
    protected function _updateLinks(
        \Magento\Framework\Model\AbstractModel $object,
        array $newRelatedIds,
        array $oldRelatedIds,
        $tableName,
        $field,
        $rowData = []
    ) {
        $table = $this->getTable($tableName);

        $insert = $newRelatedIds;
        $delete = $oldRelatedIds;

        if ($delete) {
            $where = ['company_id = ?' => (int)$object->getId(), $field.' IN (?)' => $delete];

            $this->getConnection()->delete($table, $where);
        }

        if ($insert) {
            $data = [];

            foreach ($insert as $id) {
                $id = (int)$id;
                $data[] = array_merge(
                    ['company_id' => (int)$object->getId(), $field => $id],
                    (isset($rowData[$id]) && is_array($rowData[$id])) ? $rowData[$id] : []
                );
            }

            $this->getConnection()->insertMultiple($table, $data);
        }
    }

    /**
     * Get related customer ids to which specified item is assigned
     *
     * @param int $postId
     * @return array
     */
    public function lookupRelatedCustomerIds($companyId)
    {
        return $this->_lookupIds($companyId, 'brandsmith_companymanage_customer', 'related_id');
    }

    /**
     * Get ids to which specified item is assigned
     * @param  int $postId
     * @param  string $tableName
     * @param  string $field
     * @return array
     */
    protected function _lookupIds($companyId, $tableName, $field)
    {
        $adapter = $this->getConnection();

        $select = $adapter->select()->from(
            $this->getTable($tableName),
            $field
        )->where(
            'company_id = ?',
            (int)$companyId
        );

        return $adapter->fetchCol($select);
    }
}
