<?php


namespace Brandsmith\CompanyManage\Model\ResourceModel;

class Customer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('brandsmith_companymanage_customer', 'customer_id');
    }
}
