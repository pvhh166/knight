<?php


namespace Brandsmith\CompanyManage\Model;

use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Brandsmith\CompanyManage\Model\ResourceModel\Customer as ResourceCustomer;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Brandsmith\CompanyManage\Api\Data\CustomerSearchResultsInterfaceFactory;
use Brandsmith\CompanyManage\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\CompanyManage\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Brandsmith\CompanyManage\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;

class CustomerRepository implements CustomerRepositoryInterface
{

    protected $dataCustomerFactory;

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $customerCollectionFactory;

    protected $customerFactory;

    protected $dataObjectProcessor;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    protected $_filterBuilder;
    protected $_searchCriteriaBuilder;

    private $companyRepository;


    /**
     * @param ResourceCustomer $resource
     * @param CustomerFactory $customerFactory
     * @param CustomerInterfaceFactory $dataCustomerFactory
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param CustomerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCustomer $resource,
        CustomerFactory $customerFactory,
        CustomerInterfaceFactory $dataCustomerFactory,
        CustomerCollectionFactory $customerCollectionFactory,
        CustomerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Brandsmith\CompanyManage\Model\CompanyRepository $companyRepository
    ) {
        $this->resource = $resource;
        $this->customerFactory = $customerFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCustomerFactory = $dataCustomerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_filterBuilder = $filterBuilder;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->companyRepository = $companyRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Brandsmith\CompanyManage\Api\Data\CustomerInterface $customer
    ) {
        /* if (empty($customer->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $customer->setStoreId($storeId);
        } */
        
        $customerData = $this->extensibleDataObjectConverter->toNestedArray(
            $customer,
            [],
            \Brandsmith\CompanyManage\Api\Data\CustomerInterface::class
        );
        
        $customerModel = $this->customerFactory->create()->setData($customerData);
        
        try {
            $this->resource->save($customerModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the customer: %1',
                $exception->getMessage()
            ));
        }
        return $customerModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($customerId)
    {
        $customer = $this->customerFactory->create();
        $this->resource->load($customer, $customerId);
        if (!$customer->getId()) {
            throw new NoSuchEntityException(__('Customer with id "%1" does not exist.', $customerId));
        }
        return $customer->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyCodeByCustomerId($customerId)
    {
        $_filter = [ $this->_filterBuilder->setField('related_id')->setConditionType('eq')->setValue($customerId)->create() ];
        $list = $this->getList($this->_searchCriteriaBuilder->addFilters($_filter)->create())->getItems();
        if(is_array($list)) {
            foreach ($list as $item) {
                $companyData = $this->companyRepository->getById($item->getCompanyId());
                return $companyData->getCompanyCode();
            }
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyNameByCustomerId($customerId)
    {
        $_filter = [ $this->_filterBuilder->setField('related_id')->setConditionType('eq')->setValue($customerId)->create() ];
        $list = $this->getList($this->_searchCriteriaBuilder->addFilters($_filter)->create())->getItems();
        if(is_array($list)) {
            foreach ($list as $item) {
                $companyData = $this->companyRepository->getById($item->getCompanyId());
                return $companyData->getCompanyName();
            }
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyIdByCustomerId($customerId)
    {
        $_filter = [ $this->_filterBuilder->setField('related_id')->setConditionType('eq')->setValue($customerId)->create() ];
        $list = $this->getList($this->_searchCriteriaBuilder->addFilters($_filter)->create())->getItems();
        if(is_array($list)) {
            foreach ($list as $item) {
                $companyData = $this->companyRepository->getById($item->getCompanyId());
                return $companyData->getCompanyId();
            }
        }
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->customerCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Brandsmith\CompanyManage\Api\Data\CustomerInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Brandsmith\CompanyManage\Api\Data\CustomerInterface $customer
    ) {
        try {
            $customerModel = $this->customerFactory->create();
            $this->resource->load($customerModel, $customer->getCustomerId());
            $this->resource->delete($customerModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Customer: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($customerId)
    {
        return $this->delete($this->getById($customerId));
    }
}
