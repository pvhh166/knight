<?php


namespace Brandsmith\CompanyManage\Model;

use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\CompanyManage\Api\Data\CustomerInterface;
use Brandsmith\CompanyManage\Api\Data\CustomerInterfaceFactory;

class Customer extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'brandsmith_companymanage_customer';
    protected $customerDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\CompanyManage\Model\ResourceModel\Customer $resource
     * @param \Brandsmith\CompanyManage\Model\ResourceModel\Customer\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CustomerInterfaceFactory $customerDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Brandsmith\CompanyManage\Model\ResourceModel\Customer $resource,
        \Brandsmith\CompanyManage\Model\ResourceModel\Customer\Collection $resourceCollection,
        array $data = []
    ) {
        $this->customerDataFactory = $customerDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve customer model with customer data
     * @return CustomerInterface
     */
    public function getDataModel()
    {
        $customerData = $this->getData();
        
        $customerDataObject = $this->customerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $customerDataObject,
            $customerData,
            CustomerInterface::class
        );
        
        return $customerDataObject;
    }
}
