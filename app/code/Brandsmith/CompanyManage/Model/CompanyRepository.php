<?php


namespace Brandsmith\CompanyManage\Model;

use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Brandsmith\CompanyManage\Api\CompanyRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Brandsmith\CompanyManage\Model\ResourceModel\Company\CollectionFactory as CompanyCollectionFactory;
use Brandsmith\CompanyManage\Api\Data\CompanyInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\CompanyManage\Model\ResourceModel\Company as ResourceCompany;
use Brandsmith\CompanyManage\Api\Data\CompanySearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;


class CompanyRepository implements CompanyRepositoryInterface
{

    protected $dataObjectHelper;

    private $collectionProcessor;

    protected $dataObjectProcessor;

    protected $dataCompanyFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $companyCollectionFactory;

    protected $searchResultsFactory;

    protected $extensionAttributesJoinProcessor;

    protected $companyFactory;

    private $storeManager;
    protected $_filterBuilder;
    protected $_searchCriteriaBuilder;



    /**
     * @param ResourceCompany $resource
     * @param CompanyFactory $companyFactory
     * @param CompanyInterfaceFactory $dataCompanyFactory
     * @param CompanyCollectionFactory $companyCollectionFactory
     * @param CompanySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCompany $resource,
        CompanyFactory $companyFactory,
        CompanyInterfaceFactory $dataCompanyFactory,
        CompanyCollectionFactory $companyCollectionFactory,
        CompanySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->resource = $resource;
        $this->companyFactory = $companyFactory;
        $this->companyCollectionFactory = $companyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCompanyFactory = $dataCompanyFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_filterBuilder = $filterBuilder;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Brandsmith\CompanyManage\Api\Data\CompanyInterface $company
    ) {
        /* if (empty($company->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $company->setStoreId($storeId);
        } */
        
        $companyData = $this->extensibleDataObjectConverter->toNestedArray(
            $company,
            [],
            \Brandsmith\CompanyManage\Api\Data\CompanyInterface::class
        );
        
        $companyModel = $this->companyFactory->create()->setData($companyData);
        
        try {
            $this->resource->save($companyModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the company: %1',
                $exception->getMessage()
            ));
        }
        return $companyModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($companyId)
    {
        $company = $this->companyFactory->create();
        $this->resource->load($company, $companyId);
        if (!$company->getId()) {
            throw new NoSuchEntityException(__('Company with id "%1" does not exist.', $companyId));
        }
        return $company->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getByCode($companyCode)
    {
        $_filter = [ $this->_filterBuilder->setField('company_code')->setConditionType('eq')->setValue($companyCode)->create() ];
        $list = $this->getList($this->_searchCriteriaBuilder->addFilters($_filter)->create())->getItems();
        if(is_array($list)) {
            foreach ($list as $item) {
                return $item->getCompanyId();
            }
        }
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->companyCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Brandsmith\CompanyManage\Api\Data\CompanyInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Brandsmith\CompanyManage\Api\Data\CompanyInterface $company
    ) {
        try {
            $companyModel = $this->companyFactory->create();
            $this->resource->load($companyModel, $company->getCompanyId());
            $this->resource->delete($companyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Company: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($companyId)
    {
        return $this->delete($this->getById($companyId));
    }
}
