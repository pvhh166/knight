<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Brandsmith\CompanyManage\Model\Company\Point;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Brandsmith\CompanyManage\Model\CustomerRepository;
use Brandsmith\RewardPoints\Model\ResourceModel\History\Collection;
use Brandsmith\RewardPoints\Model\ResourceModel\History\CollectionFactory;

/**
 * Class DataProvider
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_customerRepository;

    protected $request;

    protected $requestHttp;

    protected $_coreRegistry;

    protected $companyFactory;
    /**
     * @param string                $name
     * @param string                $primaryFieldName
     * @param string                $requestFieldName
     * @param Reporting             $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface      $request
     * @param FilterBuilder         $filterBuilder
     * @param array                 $meta
     * @param array                 $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Reporting $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        CustomerRepository $customerRepository,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Magento\Framework\Registry $coreRegistry,
        \Brandsmith\CompanyManage\Model\CompanyFactory $companyFactory,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->_customerRepository = $customerRepository;
        $this->request = $request;
        $this->requestHttp = $requestHttp;
        $this->_coreRegistry = $coreRegistry;
        $this->companyFactory = $companyFactory;
        $this->collection = $collectionFactory->create();
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }
    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $params = $this->requestHttp->getParams();
        $companyModel = $this->companyFactory->create();
        $companyId = $companyModel->getCompanyId();
        
        $data = parent::getData();
        /*$items = $data['items'];
        $newData = [];
        
        if(is_array($items)) {
            foreach ($items as $item) {
                if($item['customer_id'] == 10)
                    $newData[] = $item;
            }
        }*/

        //$data['items'] = $newData;
        return $data;
    }
}
