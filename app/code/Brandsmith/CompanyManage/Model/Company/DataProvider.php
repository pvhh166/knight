<?php


namespace Brandsmith\CompanyManage\Model\Company;

use Brandsmith\CompanyManage\Model\ResourceModel\Company\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $dataPersistor;

    protected $collection;

    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $data = $model->getData();
            $data['data'] = ['links' => []];
            $data['data'] = ['linkshistory' => []];

            /* Prepare related customers */
            $collection = $model->getRelatedCustomers();
            $items = [];
            foreach ($collection as $item) {
                $items[] = [
                    'id' => $item->getId(),
                    'email' => $item->getEmail(),
                    'firstname' => $item->getFirstname(),
                    'lastname' => $item->getLastname(),
                ];

            }

            $data['data']['links']['customer'] = $items;

            /* Prepare history points */
            $collectionPoints = $model->getHistoryPointCustomerByCompany();
            $items_history = [];
            foreach ($collectionPoints as $item) {
                $items_history[] = [
                    'id' => $item->getId(),
                    'customer_name' => $item->getCustomerName(),
                    'email' => $item->getEmail(),
                    'points' => $item->getPoints(),
                    'rule_name' => $item->getRuleName(),
                    'order_id' => (string)$item->getOrderIncrementId(),
                    'status' => $item->getStatus(),
                    'modifier_name' => $item->getModifierName(),
                    'reason' => $item->getReason(),
                    'created_at' => $item->getCreatedAt(),
                ];
            }

            $data['data']['linkshistory']['history'] = $items_history;

            $this->loadedData[$model->getId()] = $data;
        }
        $data = $this->dataPersistor->get('brandsmith_companymanage_company');
        
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('brandsmith_companymanage_company');
        }
        
        return $this->loadedData;
    }
}
