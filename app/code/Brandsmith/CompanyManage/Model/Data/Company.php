<?php


namespace Brandsmith\CompanyManage\Model\Data;

use Brandsmith\CompanyManage\Api\Data\CompanyInterface;

class Company extends \Magento\Framework\Api\AbstractExtensibleObject implements CompanyInterface
{

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId()
    {
        return $this->_get(self::COMPANY_ID);
    }

    /**
     * Set company_id
     * @param string $companyId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyId($companyId)
    {
        return $this->setData(self::COMPANY_ID, $companyId);
    }

    /**
     * Get company_code
     * @return string|null
     */
    public function getCompanyCode()
    {
        return $this->_get(self::COMPANY_CODE);
    }

    /**
     * Set company_code
     * @param string $companyCode
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCode($companyCode)
    {
        return $this->setData(self::COMPANY_CODE, $companyCode);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\CompanyManage\Api\Data\CompanyExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get company_name
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->_get(self::COMPANY_NAME);
    }

    /**
     * Set company_name
     * @param string $companyName
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyName($companyName)
    {
        return $this->setData(self::COMPANY_NAME, $companyName);
    }

    /**
     * Get company_phone
     * @return string|null
     */
    public function getCompanyPhone()
    {
        return $this->_get(self::COMPANY_PHONE);
    }

    /**
     * Set company_phone
     * @param string $companyPhone
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyPhone($companyPhone)
    {
        return $this->setData(self::COMPANY_PHONE, $companyPhone);
    }

    /**
     * Get company_street
     * @return string|null
     */
    public function getCompanyStreet()
    {
        return $this->_get(self::COMPANY_STREET);
    }

    /**
     * Set company_street
     * @param string $companyStreet
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyStreet($companyStreet)
    {
        return $this->setData(self::COMPANY_STREET, $companyStreet);
    }

    /**
     * Get company_city
     * @return string|null
     */
    public function getCompanyCity()
    {
        return $this->_get(self::COMPANY_CITY);
    }

    /**
     * Set company_city
     * @param string $companyCity
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCity($companyCity)
    {
        return $this->setData(self::COMPANY_CITY, $companyCity);
    }

    /**
     * Get company_country_id
     * @return string|null
     */
    public function getCompanyCountryId()
    {
        return $this->_get(self::COMPANY_COUNTRY_ID);
    }

    /**
     * Set company_country_id
     * @param string $companyCountryId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyCountryId($companyCountryId)
    {
        return $this->setData(self::COMPANY_COUNTRY_ID, $companyCountryId);
    }

    /**
     * Get company_region
     * @return string|null
     */
    public function getCompanyRegion()
    {
        return $this->_get(self::COMPANY_REGION);
    }

    /**
     * Set company_region
     * @param string $companyRegion
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyRegion($companyRegion)
    {
        return $this->setData(self::COMPANY_REGION, $companyRegion);
    }

    /**
     * Get company_region_id
     * @return string|null
     */
    public function getCompanyRegionId()
    {
        return $this->_get(self::COMPANY_REGION_ID);
    }

    /**
     * Set company_region_id
     * @param string $companyRegionId
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyRegionId($companyRegionId)
    {
        return $this->setData(self::COMPANY_REGION_ID, $companyRegionId);
    }

    /**
     * Get company_postcode
     * @return string|null
     */
    public function getCompanyPostcode()
    {
        return $this->_get(self::COMPANY_POSTCODE);
    }

    /**
     * Set company_postcode
     * @param string $companyPostcode
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCompanyPostcode($companyPostcode)
    {
        return $this->setData(self::COMPANY_POSTCODE, $companyPostcode);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get position
     * @return string|null
     */
    public function getPosition()
    {
        return $this->_get(self::POSITION);
    }

    /**
     * Set position
     * @param string $position
     * @return \Brandsmith\CompanyManage\Api\Data\CompanyInterface
     */
    public function setPosition($position)
    {
        return $this->setData(self::POSITION, $position);
    }
}
