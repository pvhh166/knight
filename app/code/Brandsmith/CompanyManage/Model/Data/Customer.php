<?php


namespace Brandsmith\CompanyManage\Model\Data;

use Brandsmith\CompanyManage\Api\Data\CustomerInterface;

class Customer extends \Magento\Framework\Api\AbstractExtensibleObject implements CustomerInterface
{

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId()
    {
        return $this->_get(self::COMPANY_ID);
    }

    /**
     * Set company_id
     * @param string $companyId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setCompanyId($companyId)
    {
        return $this->setData(self::COMPANY_ID, $companyId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\CompanyManage\Api\Data\CustomerExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get related_id
     * @return string|null
     */
    public function getRelatedId()
    {
        return $this->_get(self::RELATED_ID);
    }

    /**
     * Set related_id
     * @param string $relatedId
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setRelatedId($relatedId)
    {
        return $this->setData(self::RELATED_ID, $relatedId);
    }

    /**
     * Get position
     * @return string|null
     */
    public function getPosition()
    {
        return $this->_get(self::POSITION);
    }

    /**
     * Set position
     * @param string $position
     * @return \Brandsmith\CompanyManage\Api\Data\CustomerInterface
     */
    public function setPosition($position)
    {
        return $this->setData(self::POSITION, $position);
    }
}
