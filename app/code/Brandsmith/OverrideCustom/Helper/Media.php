<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\OverrideCustom\Helper;

use Magento\Catalog\Helper\Image;
use Magento\Framework\App\Area;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Shopping cart item render block
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 *
 * @method \Magento\Checkout\Block\Cart\Item\Renderer setProductName(string)
 * @method \Magento\Checkout\Block\Cart\Item\Renderer setDeleteUrl(string)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Media extends \Magento\Swatches\Helper\Media
{
    private $imageConfig;

    /**
     * Merged config from view.xml
     *
     * @return array
     */
    public function getImageConfig()
    {
        /*$imageConfig = [];
        foreach ($this->getRegisteredThemes() as $theme) {
            $config = $this->viewConfig->getViewConfig([
                'area' => Area::AREA_FRONTEND,
                'themeModel' => $theme,
            ]);
            $imageConfig = array_merge(
                $imageConfig,
                $config->getMediaEntities('Magento_Catalog', Image::MEDIA_TYPE_CONFIG_NODE) */
        if (!$this->imageConfig) {
            $this->imageConfig = $this->viewConfig->getViewConfig()->getMediaEntities(
                'Magento_Catalog',
                Image::MEDIA_TYPE_CONFIG_NODE
            );
        }
        //return $imageConfig;
        return $this->imageConfig;
    }
}
