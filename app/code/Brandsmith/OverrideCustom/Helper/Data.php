<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\OverrideCustom\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 *
 * @author Brandsmith Core Team <support@brandsmith.co.nz>
 */
class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {

        parent::__construct($context);
    }

    public function getCategoryIdStock()
    {
        return $this->scopeConfig->getValue(
            'brandsmith_customstock/cusstocks/customstock',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getTextShowing()
    {
        return $this->scopeConfig->getValue(
            'brandsmith_customstock/cusstocks/customstocktext',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
