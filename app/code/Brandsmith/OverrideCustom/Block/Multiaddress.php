<?php

namespace Brandsmith\OverrideCustom\Block;
use Magento\Framework\View\Element\AbstractBlock;

class Multiaddress extends AbstractBlock
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('Magento_Multishipping::multiaddress.phtml');
        }

        return parent::_prepareLayout();
    }
}
