<?php

namespace Brandsmith\OverrideCustom\Block;
use AddressFinder\AddressFinder\Block\Plugin;

class B2bregister extends Plugin
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('b2bregister.phtml');
        }

        return parent::_prepareLayout();
    }
}
