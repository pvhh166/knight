<?php

namespace Brandsmith\OverrideCustom\Block\Customer\Wishlist\Item\Column;
use Magento\Catalog\Block\Product\Context;

class Info extends \Magento\Wishlist\Block\Customer\Wishlist\Item\Column\Info
{
    protected $repository;

    protected $productFactory;

    public function __construct(\Magento\Catalog\Block\Product\Context $context,
                                \Magento\Framework\App\Http\Context $httpContext,
                                \Magento\Catalog\Api\ProductAttributeRepositoryInterface $repository,
                                \Magento\Catalog\Model\ResourceModel\ProductFactory $productFactory,
                                array $data = [])
    {
        $this->repository = $repository;
        $this->productFactory = $productFactory;
        parent::__construct($context, $httpContext, $data);
    }

    public function getAttributeById($attributeId) {
        $attribute = $this->repository->get($attributeId);
        return $attribute;
    }

    public function getOptionLabelById($attributeCode, $optionId)
    {
        $poductReource=$this->productFactory->create();
        $attribute = $poductReource->getAttribute($attributeCode);
        if ($attribute->usesSource()) {
            return  $option_Text = $attribute->getSource()->getOptionText($optionId);
        }
    }
}