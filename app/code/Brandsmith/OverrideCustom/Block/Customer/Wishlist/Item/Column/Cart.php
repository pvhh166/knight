<?php

namespace Brandsmith\OverrideCustom\Block\Customer\Wishlist\Item\Column;

class Cart extends \Magento\Wishlist\Block\Customer\Wishlist\Item\Column\Cart
{
    protected $_productRepository;
    protected $_superCart;
    protected $_helpPrice;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
                                \Magento\Framework\App\Http\Context $httpContext,
                                \Magento\Catalog\Model\ProductRepository $ProductRepository,
                                \Brandsmith\SuperProduct\Controller\Index\Cart $superCart,
                                \Magento\Framework\Pricing\Helper\Data $_helpPrice,
                                array $data = [])
    {
        $this->_productRepository = $ProductRepository;
        $this->_superCart = $superCart;
        $this->_helpPrice = $_helpPrice;
        parent::__construct($context, $httpContext, $data);
    }

    public function getSuperProductPrice($item)
    {
        $_super_custom_price = 0;
        $_options = $item->getOptions();
        if($_options) {
            foreach ($_options as $_option) {
                $option_config = $_option->getValue();
                $_param = json_decode($option_config, true);
            }
        }
        if(is_array($_param)) {
            $_super_product_id = $_param['super_product_id'];
            $_super_product_qty = $_param['product_qty'];
            $_super_product_attributes = $_param['super_attributes'];
            $_super_product_addon_qty = '';
            if(!empty($_param['addon_qty'])) {
                $_super_product_addon_qty = $_param['addon_qty'];
            }

            $_super_product = $this->_productRepository->getById($_super_product_id);
            $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);
            $_super_custom_price = $this->_superCart->getTotalPrice($_getChildProduct, $_super_product_addon_qty);
        }
        return $_super_custom_price;
    }

    public function getSuperProductPriceHtml($item)
    {
        $_super_custom_price = $this->getSuperProductPrice($item);
        $finalPrice = $this->_helpPrice->currency($_super_custom_price, true, false);
        return $finalPrice;
    }
}