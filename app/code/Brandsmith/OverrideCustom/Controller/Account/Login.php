<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\OverrideCustom\Controller\Account;

class Login extends \Magento\Customer\Controller\Account\Login
{

    public function execute()
    {
        if ($this->session->isLoggedIn()) {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }
        $referer = $this->_redirect->getRefererUrl();
        $redirectUrl = $this->session->getHttpReferrer();

        if(isset($referer) && $referer == $redirectUrl) {
            $this->session->setHttpReferrer($referer);
        }else {
            $this->session->unsHttpReferrer($referer);
        }

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setHeader('Login-Required', 'true');
        return $resultPage;
    }
}
