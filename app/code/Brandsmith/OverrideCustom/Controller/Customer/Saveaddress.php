<?php

namespace Brandsmith\OverrideCustom\Controller\Customer;

use Magento\Framework\DataObject;
use Bss\B2bRegistration\Helper\CreateAccount;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Saveaddress extends \Magento\Framework\App\Action\Action
{
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var \Magento\Framework\Json\Helper\Data $helper
     */
    protected $helper;

    /**
     * @var CreateAccount
     */
    protected $helperCreateAccount;

    /**
     * @var AddressInterfaceFactory
     */
    protected $addressDataFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Json\Helper\Data $helper,
        CreateAccount $helperCreateAccount,
        AddressInterfaceFactory $addressDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->helperCreateAccount = $helperCreateAccount;
        $this->addressDataFactory = $addressDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->customerSession = $customerSession;
        $this->addressRepository = $addressRepository;
        parent::__construct($context);
    }

    /**
     * Popup action
     * Shows tracking info if it's present, otherwise redirects to 404
     *
     * @return void
     * @throws NotFoundException
     */
    public function execute()
    {
        try {
            $result = new DataObject();
            $params = $this->helper->jsonDecode($this->getRequest()->getContent());
            $customerId = $this->customerSession->getCustomerId();
            $region = isset($params['region']) ? $params['region'] : '';
            $regionId = isset($params['region_id']) ? $params['region_id'] : '';

            $delivery = ($params['custom_attributes']['delivery_home']) ? 1 : 0;
            $deliveryhome_email = ($params['custom_attributes']['delivery_home_email']) ? $params['custom_attributes']['delivery_home_email'] : '';
            $deliveryhome_mobile = ($params['custom_attributes']['delivery_home_mobile']) ? $params['custom_attributes']['delivery_home_mobile'] : '';
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $addresss = $objectManager->get('\Magento\Customer\Model\AddressFactory');
            $address = $addresss->create();

            $address->setCustomerId($customerId)
                ->setData('delivery_home',$delivery)
                ->setData('delivery_home_email',$deliveryhome_email)
                ->setData('delivery_home_mobile',$deliveryhome_mobile)
                ->setFirstname($params['firstname'])
                ->setLastname($params['lastname'])
                ->setCountryId($params['country_id'])
                ->setRegionId($regionId)
                ->setCity($params['city'])
                ->setPostcode($params['postcode'])
                ->setCustomerId($customerId)
                ->setStreet($params['street'])
                ->setCompany($params['company'])
                ->setTelephone($params['telephone']);
            $address->save();

            $result->setData('success', true);
        } catch (\Exception $e) {
            $result->setData('success', false);
        }

        return $this->resultJsonFactory->create()->setData($result->getData());
    }
}
