<?php
namespace Brandsmith\OverrideCustom\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddExtraDataToTransportInvoice implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transport = $observer->getTransport();
        $_order = $transport['order'];
        $po_number =  $_order->getData('bold_order_comment');
        $po_number_html = '';
        if(!empty($po_number))
            $po_number_html = '<div><strong>PO Number:</strong> <br/>'.$po_number.'</div><br/>';

        $payment_html = $transport['payment_html'];
        $payment_html = str_replace("Purchase Order Number","",$payment_html);
        $payment_html = $payment_html.$po_number_html;
        $transport['payment_html'] = $payment_html;
    }
}