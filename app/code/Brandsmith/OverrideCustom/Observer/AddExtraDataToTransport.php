<?php
namespace Brandsmith\OverrideCustom\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddExtraDataToTransport implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transport = $observer->getTransport();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\SessionFactory')->create();
        $orderCmt = $checkoutSession->getOrderCmt();

        $po_number = $checkoutSession->getPoNumber();
        $po_number_html = '';
        if(!empty($po_number))
            $po_number_html = '<div><strong>PO Number:</strong> <br/>'.$po_number.'</div><br/>';

        $order_cm = '';
        if(!empty($orderCmt)) {
            $order_cm = '<div><strong>Order Comment:</strong><br/>';
            $order_cm .= $orderCmt;
            $order_cm .= '</div>';
        }

        $checkoutSession->unsOrderCmt();
        $checkoutSession->unsPoNumber();

        $payment_html = $transport->getPaymentHtml();
        $payment_html = str_replace("Purchase Order Number","",$payment_html);
        $payment_html = $payment_html.$po_number_html.$order_cm;
        $transport->setPaymentHtml($payment_html);
    }
}