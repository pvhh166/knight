<?php

namespace Brandsmith\OverrideCustom\Model\Sales\Total\Quote;

class Tax extends \Magento\Tax\Model\Sales\Total\Quote\Tax

{

    /**
     * Custom Collect tax totals for quote address
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @return $this
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $customDiscount = $quote->getRewardPointsAmount();
        $gst = $total->getTaxAmount();
        if($customDiscount > 0) {
            $customDiscount = $customDiscount * 0.15;
            $gst = $gst - $customDiscount;
            $total->setTotalAmount('tax', $gst);
            $total->setBaseTotalAmount('tax', $gst);
        }
        return $this;
    }

}