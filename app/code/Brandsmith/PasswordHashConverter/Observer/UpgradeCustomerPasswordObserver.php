<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Brandsmith\PasswordHashConverter\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Model\CustomerRegistry;
use Brandsmith\PasswordHashConverter\Model\PasswordHash;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class RewardSalesConvertQuoteToOrder
 * @author Brandsmith Commerce OÜ Core Team <support@brandsmith.co.nz>
 */
class UpgradeCustomerPasswordObserver implements ObserverInterface
{
    protected $encryptor;

    protected $WpPasswordHasher;

    protected $customerRegistry;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    public function __construct(
        EncryptorInterface $encryptor,
        CustomerRegistry $customerRegistry,
        CustomerRepository $customerRepository,
        PasswordHash $WpPasswordHasher
    ) {
        $this->encryptor = $encryptor;
        $this->WpPasswordHasher = $WpPasswordHasher;
        $this->customerRegistry = $customerRegistry;
        $this->customerRepository = $customerRepository;
    }
    /**
     * Setup Reward Points Amount to Order(magento default logic to convert quote to order does not work ~2.1.7)
     *
     * @param  Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $requestParams = $observer->getEvent()->getData('request')->getParams();
        if(isset($requestParams['login'])) {
            $username = $requestParams['login']['username'];
            $password = $requestParams['login']['password'];
            try {
                /** @var \Magento\Customer\Api\Data\CustomerInterface */
                $customer = $this->customerRepository->get($username);
                $customerSecure = $this->customerRegistry->retrieveSecureData($customer->getId());
                $hash = $customerSecure->getPasswordHash();
                if ($this->WpPasswordHasher->CheckPassword($password, $hash)) {
                    $customerSecure->setPasswordHash($this->encryptor->getHash($password, true));
                    $this->customerRepository->save($customer);
                }
            } catch (\Exception $e) {

            }
        }
    }
}
