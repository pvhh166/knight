<?php


namespace Brandsmith\ProductBadge\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BadgesRepositoryInterface
{

    /**
     * Save Badges
     * @param \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
    );

    /**
     * Retrieve Badges
     * @param string $badgesId
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($badgesId);

    /**
     * Retrieve Badges matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Badges
     * @param \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
    );

    /**
     * Delete Badges by ID
     * @param string $badgesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($badgesId);
}
