<?php


namespace Brandsmith\ProductBadge\Api\Data;

interface BadgesInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const BADGES_ID = 'badges_id';
    const IMAGE = 'image';
    const DESCRIPTION = 'description';

    /**
     * Get badges_id
     * @return string|null
     */
    public function getBadgesId();

    /**
     * Set badges_id
     * @param string $badgesId
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setBadgesId($badgesId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface $extensionAttributes
    );

    /**
     * Get description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set description
     * @param string $description
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setDescription($description);

    /**
     * Get image
     * @return string|null
     */
    public function getImage();

    /**
     * Set image
     * @param string $image
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setImage($image);
}
