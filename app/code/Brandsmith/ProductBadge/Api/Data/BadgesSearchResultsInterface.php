<?php


namespace Brandsmith\ProductBadge\Api\Data;

interface BadgesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Badges list.
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \Brandsmith\ProductBadge\Api\Data\BadgesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
