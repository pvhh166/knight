<?php

namespace Brandsmith\ProductBadge\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Brandsmith\ProductBadge\Model\ResourceModel\Badges\CollectionFactory;

class ProductBadges extends \Magento\Framework\View\Element\Template
{
    /**
     * @param Context $context
     * @param Data $catalogData
     * @param array $data
     */
    private $_collection;

    protected $storeManager;

    protected $_registry;

    protected $attributes;

    protected $_filesystem ;

    protected $_imageFactory;

    public function __construct(
        Context $context,
        CollectionFactory $_collectionFactory,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Entity\AttributeFactory $attributes,
        \Magento\Framework\Filesystem $filesystem,         
        \Magento\Framework\Image\AdapterFactory $imageFactory,  
        array $data = [])
    {
        $this->_collection = $_collectionFactory->create();
        $this->storeManager = $storeManager;
        $this->_registry = $registry;
        $this->attributes = $attributes;
        $this->_filesystem = $filesystem;               
        $this->_imageFactory = $imageFactory; 
        parent::__construct($context, $data);
    }

    public function getProductBadges() {
        $product = $this->_registry->registry('current_product');
        $data = explode(',',$product->getData('product_badge'));
        $badges = [];
        foreach($data as $value) {
            $attr = $product->getResource()->getAttribute('product_badge');
            if ($attr->usesSource()) {
                $option_value = $attr->getSource()->getOptionText($value);
                $badges[] = $option_value;
            }
        }

        $collection = $this->_collection;
        $collection->addFieldToFilter('main_table.name',array("in"=>$badges));

        return $collection;
    }

    public function getProductBadgeUrl($images) {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl. 'product_badge/' .$images;
    }

    // pass imagename, width and height
    public function resizeBadge($image, $width = null, $height = null){
        
        $absolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('product_badge/').$image;
        if (!file_exists($absolutePath)) return false;
        $imageResized = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('product_badge/resized/'.$width.'/').$image;
        if (!file_exists($imageResized)) { // Only resize image if not already exists.
            //create image factory...
            $imageResize = $this->_imageFactory->create();         
            $imageResize->open($absolutePath);
            $imageResize->constrainOnly(TRUE);         
            $imageResize->keepTransparency(TRUE);         
            $imageResize->keepFrame(FALSE);         
            $imageResize->keepAspectRatio(TRUE);         
            $imageResize->resize($width,$height);  
            //destination folder                
            $destination = $imageResized ;    
            //save image      
            $imageResize->save($destination);         
        } 
        $resizedURL = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'product_badge/resized/'.$width.'/'.$image;
        return $resizedURL;
    } 

    public function getMultiselectlist($resourcename, $attributename){
        $attributesList = $this->attributes->loadByCode($resourcename, $attributename);
        return $attributesList;
    }

}