<?php


namespace Brandsmith\ProductBadge\Model\ResourceModel;

class Badges extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('brandsmith_productbadge_badges', 'badges_id');
    }
}
