<?php


namespace Brandsmith\ProductBadge\Model\ResourceModel\Badges;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Brandsmith\ProductBadge\Model\Badges::class,
            \Brandsmith\ProductBadge\Model\ResourceModel\Badges::class
        );
    }
}
