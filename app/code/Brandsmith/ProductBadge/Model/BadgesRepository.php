<?php


namespace Brandsmith\ProductBadge\Model;

use Brandsmith\ProductBadge\Api\Data\BadgesInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\ProductBadge\Model\ResourceModel\Badges as ResourceBadges;
use Brandsmith\ProductBadge\Api\BadgesRepositoryInterface;
use Brandsmith\ProductBadge\Model\ResourceModel\Badges\CollectionFactory as BadgesCollectionFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\CouldNotSaveException;
use Brandsmith\ProductBadge\Api\Data\BadgesSearchResultsInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class BadgesRepository implements BadgesRepositoryInterface
{

    protected $badgesFactory;

    protected $dataBadgesFactory;

    protected $resource;

    protected $extensionAttributesJoinProcessor;

    protected $extensibleDataObjectConverter;
    protected $badgesCollectionFactory;

    protected $dataObjectProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $searchResultsFactory;


    /**
     * @param ResourceBadges $resource
     * @param BadgesFactory $badgesFactory
     * @param BadgesInterfaceFactory $dataBadgesFactory
     * @param BadgesCollectionFactory $badgesCollectionFactory
     * @param BadgesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceBadges $resource,
        BadgesFactory $badgesFactory,
        BadgesInterfaceFactory $dataBadgesFactory,
        BadgesCollectionFactory $badgesCollectionFactory,
        BadgesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->badgesFactory = $badgesFactory;
        $this->badgesCollectionFactory = $badgesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataBadgesFactory = $dataBadgesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
    ) {
        /* if (empty($badges->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $badges->setStoreId($storeId);
        } */
        
        $badgesData = $this->extensibleDataObjectConverter->toNestedArray(
            $badges,
            [],
            \Brandsmith\ProductBadge\Api\Data\BadgesInterface::class
        );
        
        $badgesModel = $this->badgesFactory->create()->setData($badgesData);
        
        try {
            $this->resource->save($badgesModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the badges: %1',
                $exception->getMessage()
            ));
        }
        return $badgesModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($badgesId)
    {
        $badges = $this->badgesFactory->create();
        $this->resource->load($badges, $badgesId);
        if (!$badges->getId()) {
            throw new NoSuchEntityException(__('Badges with id "%1" does not exist.', $badgesId));
        }
        return $badges->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->badgesCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Brandsmith\ProductBadge\Api\Data\BadgesInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Brandsmith\ProductBadge\Api\Data\BadgesInterface $badges
    ) {
        try {
            $badgesModel = $this->badgesFactory->create();
            $this->resource->load($badgesModel, $badges->getBadgesId());
            $this->resource->delete($badgesModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Badges: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($badgesId)
    {
        return $this->delete($this->getById($badgesId));
    }
}
