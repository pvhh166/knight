<?php


namespace Brandsmith\ProductBadge\Model;

use Magento\Framework\Api\DataObjectHelper;
use Brandsmith\ProductBadge\Api\Data\BadgesInterface;
use Brandsmith\ProductBadge\Api\Data\BadgesInterfaceFactory;

class Badges extends \Magento\Framework\Model\AbstractModel
{

    protected $badgesDataFactory;

    protected $_eventPrefix = 'brandsmith_productbadge_badges';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BadgesInterfaceFactory $badgesDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Brandsmith\ProductBadge\Model\ResourceModel\Badges $resource
     * @param \Brandsmith\ProductBadge\Model\ResourceModel\Badges\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BadgesInterfaceFactory $badgesDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Brandsmith\ProductBadge\Model\ResourceModel\Badges $resource,
        \Brandsmith\ProductBadge\Model\ResourceModel\Badges\Collection $resourceCollection,
        array $data = []
    ) {
        $this->badgesDataFactory = $badgesDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve badges model with badges data
     * @return BadgesInterface
     */
    public function getDataModel()
    {
        $badgesData = $this->getData();
        
        $badgesDataObject = $this->badgesDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $badgesDataObject,
            $badgesData,
            BadgesInterface::class
        );
        
        return $badgesDataObject;
    }
}
