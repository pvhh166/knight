<?php


namespace Brandsmith\ProductBadge\Model\Data;

use Brandsmith\ProductBadge\Api\Data\BadgesInterface;

class Badges extends \Magento\Framework\Api\AbstractExtensibleObject implements BadgesInterface
{

    /**
     * Get badges_id
     * @return string|null
     */
    public function getBadgesId()
    {
        return $this->_get(self::BADGES_ID);
    }

    /**
     * Set badges_id
     * @param string $badgesId
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setBadgesId($badgesId)
    {
        return $this->setData(self::BADGES_ID, $badgesId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Brandsmith\ProductBadge\Api\Data\BadgesExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get description
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * Set description
     * @param string $description
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get image
     * @return string|null
     */
    public function getImage()
    {
        return $this->_get(self::IMAGE);
    }

    /**
     * Set image
     * @param string $image
     * @return \Brandsmith\ProductBadge\Api\Data\BadgesInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }
}
