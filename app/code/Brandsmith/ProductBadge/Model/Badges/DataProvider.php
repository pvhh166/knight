<?php


namespace Brandsmith\ProductBadge\Model\Badges;

use Magento\Framework\App\Request\DataPersistorInterface;
use Brandsmith\ProductBadge\Model\ResourceModel\Badges\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $loadedData;
    protected $dataPersistor;

    protected $collection;
    protected  $_storeManager;


    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->_storeManager=$storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    /*public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }
        $data = $this->dataPersistor->get('brandsmith_productbadge_badges');
        
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('brandsmith_productbadge_badges');
        }
        
        return $this->loadedData;
    }
    */
    public function getData()
    {
        $baseurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $item) {
            $temp = $item->getData();
            if ($temp['image']):
                $img = [];
                $img[0]['image'] = $temp['image'];
                $img[0]['url'] = $baseurl . 'product_badge/' . $temp['image'];
                $temp['image'] = $img;
            endif;
            $data = $this->dataPersistor->get('brandsmith_productbadge_badges');
            if (!empty($data)) {
                $item = $this->collection->getNewEmptyItem();
                $item->setData($data);
                $this->loadedData[$item->getLabelId()] = $item->getData();
                $this->dataPersistor->clear('brandsmith_productbadge_badges');
            } else {
                if ($items):
                    if ($item->getData('image') != null) {
                        $t2[$item->getId()] = $temp;
                        return $t2;
                    } else {
                        return $this->loadedData;
                    }
                endif;
            }
            return $this->loadedData;
        }
    }
}
