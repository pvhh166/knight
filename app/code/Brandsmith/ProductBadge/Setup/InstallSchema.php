<?php


namespace Brandsmith\ProductBadge\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_brandsmith_productbadge_badges = $setup->getConnection()->newTable($setup->getTable('brandsmith_productbadge_badges'));

        $table_brandsmith_productbadge_badges->addColumn(
            'badges_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_brandsmith_productbadge_badges->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'name'
        );

        $table_brandsmith_productbadge_badges->addColumn(
            'description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'description'
        );

        $table_brandsmith_productbadge_badges->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1000,
            [],
            'image'
        );

        $setup->getConnection()->createTable($table_brandsmith_productbadge_badges);
    }
}
