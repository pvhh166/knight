<?php
 
namespace Brandsmith\ShippingMods\Plugin;
 
class ApplyShipping
{
	private $logger;
	
	private $business;
	
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
		$this->logger = $logger;
		$this->business = true;
    }
 
    public function aroundCollectCarrierRates(
        \Magento\Shipping\Model\Shipping $subject,
        \Closure $proceed,
        $carrierCode,
        $request
    )
    {
		$this->logger->critical($carrierCode);

		if ($carrierCode == 'KNIGHT' || $carrierCode == 'KNIGHTR') {
			
			try {
				$shipping = $request->getData();
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'http://103.8.232.130:3045/');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['city'=>$shipping['dest_city'], 'postcode'=>$shipping['dest_postcode'], 'street'=>$shipping['dest_street']]));

				$result = curl_exec($ch);
				curl_close($ch);
				
				$response = json_decode($result, true);
				
				if(isset($response['business'])){
					$this->logger->critical($response['business']);
					if ($carrierCode == 'KNIGHT' && $response['business']) 
					{
						return $proceed($carrierCode, $request);
					} else if ($carrierCode == 'KNIGHT' && !$response['business']) 
					{
						return false;
					} else if ($carrierCode == 'KNIGHTR' && $response['business'])
					{
						return false;
					} else if ($carrierCode == 'KNIGHTR' && !$response['business']) 
					{
						return $proceed($carrierCode, $request);
					}
				}
			
			} catch (Exception $e) {
				if ($carrierCode == 'KNIGHT') {
					return $proceed($carrierCode, $request);
				} else if ($carrierCode == 'KNIGHTR')
					return false;
			}
		}
           // To enable the shipping method
            return $proceed($carrierCode, $request);
    }
}