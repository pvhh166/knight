<?php
/**
 * Copyright © 2017 Brandsmith. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Brandsmith\QuickOrder\Block\Cart;

class ProductListing extends Group\AbstractGroup {
	/**
	 *
	 * @var array
	 */
	protected $_template = 'cart/productlisting.phtml';
}
