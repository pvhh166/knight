<?php
/**
 * Copyright © 2017 Brandsmith. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\QuickOrder\Controller\Cart;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ProductListing extends \Magento\Framework\App\Action\Action implements OrderInterface
{

    /**
     *
     * @var \Magento\Framework\View\Result\Page
     */
    protected $resultPageFactory;

    /**
     *
     * @param Context $context            
     * @param \Magento\Customer\Model\Session $customerSession            
     * @param PageFactory $resultPageFactory            
     */
    public function __construct(Context $context, \Magento\Customer\Model\Session $customerSession, PageFactory $resultPageFactory)
    {
        $this->_customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Redirect to the Quick Order UI page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $customerSessionId = $this->_customerSession->getCustomerId();
        $customerType = $this->_objectManager->create('Magento\Customer\Model\Customer')
            ->load($this->_customerSession->getCustomerId())
            ->getCustomerType();
        if ($customerType == 1) {
            return $this->resultRedirectFactory->create()->setPath('customer/account');
        }
        if (! ($customerSessionId)) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()
            ->getTitle()
            ->set(__('Quick Order'));
        return $resultPage;
    }
}
