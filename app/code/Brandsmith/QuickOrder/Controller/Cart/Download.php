<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Brandsmith\QuickOrder\Controller\Cart;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Framework\App\Action\Action
{
	/**
	* @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
	*/
	protected $_productCollectionFactory;

	protected $resultRawFactory;

    protected $csvWriter;

    protected $fileFactory;

    protected $directoryList;

	public function __construct(
		Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvWriter,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
	) {
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->csvWriter = $csvWriter;
        $this->resultRawFactory = $resultRawFactory;
        $this->fileFactory = $fileFactory;
        $this->directoryList = $directoryList;
		parent::__construct($context);
	}

	public function execute()
	{  
		$data = [
            ['Sku','Qty'],
        ];

        $fileDirectory = \Magento\Framework\App\Filesystem\DirectoryList::MEDIA;
        $fileName = "TemplateCSV.csv";
        $filePath =  $this->directoryList->getPath($fileDirectory) . "/" . $fileName;

		$productCollection =  $this->_productCollectionFactory->create()
		->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
		->addAttributeToSelect('*')->setPageSize(3);
		foreach ($productCollection as $product) {
			$row = [
				$product->getSku(),
				rand(1, 3)
			];
			$data[] = $row;
			
		}
		 $this->csvWriter
            ->setEnclosure('"')
            ->setDelimiter(',')
            ->saveData($filePath ,$data);

        $this->fileFactory->create(
            $fileName,
            [
                'type'  => "filename",
                'value' => $fileName,
                'rm'    => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA,
            'application/csv',
            null
        );

        $resultRaw = $this->resultRawFactory->create();

        return $resultRaw;

	}
	
}