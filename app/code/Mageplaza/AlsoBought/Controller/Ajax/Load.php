<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Controller\Ajax;

use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Mageplaza\AlsoBought\Helper\Data as HelperData;
use Psr\Log\LoggerInterface;

/**
 * Class Load
 * @package Mageplaza\AlsoBought\Controller\Ajax
 */
class Load extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Load constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $logger
     * @param Cart $cart
     * @param HelperData $helper
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $logger,
        Cart $cart,
        HelperData $helper,
        CategoryFactory $categoryFactory
    )
    {
        $this->logger            = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->cart              = $cart;
        $this->helper            = $helper;
        $this->categoryFactory   = $categoryFactory;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $result     = ['status' => false];
        $productIds = [];
        $resultPage = $this->resultPageFactory->create();
        $params     = HelperData::jsonDecode($this->getRequest()->getContent());
        try {
            $this->helper->setData('action', $params['action']);
            $this->helper->setData('entity_id', $params['entity_id']);
            if ($params['action'] == 'catalog_product_view' && $params['entity_id']) {
                $productIds[] = $params['entity_id'];
            } else if ($params['action'] == 'catalog_category_view' && $params['entity_id']) {
                $category          = $this->categoryFactory->create()->load($params['entity_id']);
                $productCollection = $category->getProductCollection()->addAttributeToSelect('*');
                foreach ($productCollection as $product) {
                    $productIds[] = $product->getId();
                }
            } else if ($params['action'] == 'checkout_cart_index') {
                $cartQuote = $this->cart->getQuote();
                if ($cartQuote->getItemsCount()) {
                    foreach ($cartQuote->getAllVisibleItems() as $item) {
                        $productIds[] = $item->getProductId();
                    }
                }
            }

            if (!empty($productIds)) {
                $html              = $resultPage->getLayout()
                    ->createBlock('Mageplaza\AlsoBought\Block\Product\ProductList\ProductList')
                    ->setTemplate('Mageplaza_AlsoBought::product/list/items.phtml')
                    ->setProductIds($productIds)
                    ->setRequestDefault($params)
                    ->toHtml();
                $result['content'] = $html;
                if (isset($result['content'])) {
                    $result['status'] = true;
                }
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());

            return $resultRedirect;
        }

        return $this->getResponse()->representJson(HelperData::jsonEncode($result));
    }
}
