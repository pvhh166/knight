<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Mageplaza\AlsoBought\Model\ResourceModel\AlsoBoughtFactory;

/**
 * Class Reindex
 * @package Mageplaza\AlsoBought\Controller\Adminhtml\Index
 */
class Reindex extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mageplaza_AlsoBought::alsobought';

    /**
     * @var \Mageplaza\AlsoBought\Model\AlsoBought
     */
    private $alsoBoughtFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param AlsoBoughtFactory $alsoBoughtFactory
     */
    public function __construct(
        Context $context,
        AlsoBoughtFactory $alsoBoughtFactory
    )
    {
        $this->alsoBoughtFactory = $alsoBoughtFactory;

        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $this->alsoBoughtFactory->create()->reIndex();
            $this->messageManager->addSuccess(__('Update Success'));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong.'));
        }

        return $resultRedirect->setPath(
            'adminhtml/system_config/edit/section/alsobought'
        );
    }
}
