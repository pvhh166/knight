<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Cron;

use Mageplaza\AlsoBought\Helper\Data;
use Mageplaza\AlsoBought\Model\ResourceModel\AlsoBoughtFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Update
 * @package Mageplaza\AlsoBought\Cron
 */
class Update
{
    /**
     * @var AlsoBoughtFactory
     */
    private $alsoBoughtFactory;

    /**
     * @var Data
     */
    private $alsoBoughtDataHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     * @param AlsoBoughtFactory $alsoBoughtFactory
     * @param Data $alsoBoughtDataHelper
     */
    public function __construct(
        LoggerInterface $logger,
        AlsoBoughtFactory $alsoBoughtFactory,
        Data $alsoBoughtDataHelper
    )
    {
        $this->logger               = $logger;
        $this->alsoBoughtFactory    = $alsoBoughtFactory;
        $this->alsoBoughtDataHelper = $alsoBoughtDataHelper;
    }

    /**
     * Update pearson value
     *
     * @return void
     */
    public function execute()
    {
        $helper = $this->alsoBoughtDataHelper;
        if ($helper->isEnabled()) {
            try {
                $this->alsoBoughtFactory->create()->pearsonUpdate();
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }
}
