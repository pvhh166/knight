<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Mageplaza\AlsoBought\Helper\Data;
use Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought\CollectionFactory as AlsoBoughtCollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Class AlsoBought
 * @package Mageplaza\AlsoBought\Model\ResourceModel
 */
class AlsoBought extends AbstractDb
{
    /**
     * @var CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var AlsoBoughtCollectionFactory
     */
    private $alsoBoughtCollectionFactory;

    /**
     * @var Data
     */
    private $alsoBoughtDataHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AlsoBought constructor.
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param AlsoBoughtCollectionFactory $alsoBoughtCollectionFactory
     * @param Data $alsoBoughtDataHelper
     * @param LoggerInterface $logger
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        AlsoBoughtCollectionFactory $alsoBoughtCollectionFactory,
        Data $alsoBoughtDataHelper,
        LoggerInterface $logger,
        $connectionName = null
    )
    {
        $this->orderCollectionFactory      = $orderCollectionFactory;
        $this->alsoBoughtCollectionFactory = $alsoBoughtCollectionFactory;
        $this->alsoBoughtDataHelper        = $alsoBoughtDataHelper;
        $this->logger                      = $logger;

        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('mageplaza_wbab_product_link', 'id');
    }

    /**
     * Reset data also bought get from order
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function reIndex()
    {
        $helper             = $this->alsoBoughtDataHelper;
        $processOrderStatus = $helper->getProcessOrderStatus();
        $this->alsoBoughtCollectionFactory->create()->clear();
        if (empty($processOrderStatus)) {
            return false;
        }
        $productIds   = [];
        $productCount = [];
        $dataInsert   = [];
        $this->resetMageplazaAlsobought();
        $items = $this->getDataItemOrder()->addFieldToFilter('status', ['in' => $processOrderStatus]);

        if ($items->getSize()) {
            $orderAll = [];
            foreach ($items as $item) {
                $this->setMageplazaAlsobought($item->getOrderId());
                if (!empty($item)) {
                    $productId = $item->getProductId();
                    $orderId   = $item->getOrderId();
                    if (!in_array($productId, $productIds)) {
                        $productCount[$productId] = 1;
                        $productIds[]             = $productId;
                    } else {
                        $productCount[$productId]++;
                    }
                    $orderAll[$orderId][] = $productId;
                }
            }
            $orderAll       = array_map("unserialize", array_unique(array_map("serialize", $orderAll)));
            $checkProductId = [];
            $dataProductId  = [];
            foreach ($orderAll as $data) {
                $data  = array_values(array_unique($data));
                $count = count($data);
                if (!empty($data) && $count > 1) {
                    for ($i = 0; $i < $count; $i++) {
                        if (!array_key_exists($data[$i], $dataProductId)) {
                            $isProduct = true;
                            $select    = $this->getConnection()->select()
                                ->from($this->getTable('catalog_product_entity'), 'entity_id')
                                ->where('entity_id = :entity_id');
                            $bind      = [
                                'entity_id' => $data[$i]
                            ];
                            $productId = $this->getConnection()->fetchOne($select, $bind);
                            if (!$productId) {
                                $isProduct = false;
                            }
                            $dataProductId[$data[$i]] = $isProduct;
                        }
                        if ($dataProductId[$data[$i]]) {
                            for ($j = $i + 1; $j < $count; $j++) {
                                $combo = 0;
                                foreach ($orderAll as $productIdArr) {
                                    if (in_array($data[$i], $productIdArr) && in_array($data[$j], $productIdArr)) {
                                        $combo++;
                                    }
                                }
                                $checkData = $data[$i] . $data[$j];
                                if (in_array($checkData, $checkProductId)) {
                                    continue;
                                }
                                $checkProductId[] = $checkData;
                                $checkProductId[] = $data[$j] . $data[$i];
                                $dataInsert[]     = [
                                    'product_id'                 => $data[$i],
                                    'linked_product_id'          => $data[$j],
                                    'total_order_product'        => $productCount[$data[$i]],
                                    'total_order_linked_product' => $productCount[$data[$j]],
                                    'total_order_combo'          => $combo
                                ];
                            }
                        }
                    }
                }
            }
            if (!empty($dataInsert)) {
                $this->getConnection()->insertMultiple($this->getMainTable(), $dataInsert);
            }
        }
    }

    /**
     * Join sales_order & sales_order_item get data
     *
     * @return mixed
     */
    private function getDataItemOrder()
    {
        $orderItemTable  = $this->getTable('sales_order_item');
        $orderCollection = $this->orderCollectionFactory->create();
        $orderCollection->join(
            ['oit' => $orderItemTable],
            "main_table.entity_id = oit.order_id"
        );
        $orderCollection->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(
                [
                    'status'     => 'status',
                    'product_id' => 'oit.product_id',
                    'order_id'   => 'oit.order_id'
                ]
            );

        return $orderCollection;
    }

    /**
     * Update pearson by cron
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function pearsonUpdate()
    {
        if ($this->getCountOrder()) {
            $a            = 'total_order_combo';
            $b            = 'total_order_product - total_order_combo';
            $c            = 'total_order_linked_product - total_order_combo';
            $d            = $this->getCountOrder() . ' + total_order_combo - total_order_product - total_order_linked_product';
            $numerator    = 'CAST(' . $a . '*(' . $d . ') AS SIGNED)-CAST((' . $b . ')*(' . $c . ') AS SIGNED)';
            $denominators = '(' . $a . '+' . $b . ')*(' . $c . '+' . $d . ')*(' . $a . '+' . $c . ')*(' . $b . '+' . $d . ')';
            $sql          = "UPDATE " . $this->getMainTable() . " SET pearson = ((" . $numerator . ")/(SQRT(" . $denominators . ")))";
            $this->getConnection()->query($sql);
        }
    }

    /**
     * Get count order on site
     *
     * @return int
     */
    private function getCountOrder()
    {
        $collection  = $this->orderCollectionFactory->create();
        $countSelect = $collection->getSelectCountSql();

        return (int)$this->getConnection()->fetchOne($countSelect);
    }

    /**
     * Update column mageplaza_alsobought_status by id
     *
     * @param string $id
     * @return void
     */
    private function setMageplazaAlsobought($id = null)
    {
        $orderTable = $this->getTable('sales_order');
        $bind       = ['mageplaza_alsobought_status' => 1];
        $where      = ['entity_id = ?' => $id];
        $this->getConnection()->update($orderTable, $bind, $where);
    }

    /**
     * Update column mageplaza_alsobought_status after run
     *
     * @return void
     */
    private function resetMageplazaAlsobought()
    {
        $orderTable = $this->getTable('sales_order');
        $bind       = ['mageplaza_alsobought_status' => 1];
        $this->getConnection()->update($orderTable, $bind);
    }

    /**
     * Update total product & total product linked by product id
     *
     * @param $productId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateTotalProduct($productId)
    {
        $sql1 = "UPDATE " . $this->getMainTable() . " SET total_order_product = (total_order_product + 1) WHERE product_id = " . $productId;
        $this->getConnection()->query($sql1);
        $sql2 = "UPDATE " . $this->getMainTable() . " SET total_order_linked_product = (total_order_linked_product + 1) WHERE linked_product_id = " . $productId;
        $this->getConnection()->query($sql2);
    }

    /**
     * Update total combo by product ids
     *
     * @param $productIds
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateTotalCombo($productIds)
    {
        $productIds = array_unique($productIds);
        $count      = count($productIds);
        $connection = $this->getConnection();
        for ($i = 0; $i < $count; $i++) {
            for ($j = $i + 1; $j < $count; $j++) {
                $select      = $connection->select()
                    ->from($this->getMainTable(), 'link_id')
                    ->where('linked_product_id = :linked_product_id')
                    ->where('product_id = :product_id');
                $bind        = [
                    'linked_product_id' => $productIds[$i],
                    'product_id'        => $productIds[$j]
                ];
                $bindReverse = [
                    'linked_product_id' => $productIds[$j],
                    'product_id'        => $productIds[$i]
                ];
                $linkId      = $connection->fetchOne($select, $bind);
                if (!$linkId) {
                    $linkId = $connection->fetchOne($select, $bindReverse);
                }
                if ($linkId) {
                    $combo      = $connection->select()
                        ->from($this->getMainTable(), 'total_order_combo')
                        ->where('link_id = ?', $linkId);
                    $totalCombo = (int)$connection->fetchOne($combo);
                    $bind       = ['total_order_combo' => $totalCombo + 1];
                    $where      = ['link_id = ?' => (int)$linkId];

                    $connection->update($this->getMainTable(), $bind, $where);
                } else {
                    $bind = [
                        'linked_product_id'          => $productIds[$i],
                        'product_id'                 => $productIds[$j],
                        'total_order_product'        => $this->getTotalProduct($productIds[$j]),
                        'total_order_linked_product' => $this->getTotalProduct($productIds[$i]),
                        'total_order_combo'          => 1
                    ];
                    $connection->insert($this->getMainTable(), $bind);
                }
            }
        }
    }

    /**
     * Update total combo by product ids
     *
     * @param $productId
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getTotalProduct($productId)
    {
        $connection         = $this->getConnection();
        $totalProduct       = $connection->select()
            ->from($this->getMainTable(), 'total_order_product')
            ->where('product_id = ?', $productId);
        $totalProductUpdate = (int)$connection->fetchOne($totalProduct);
        if (!$totalProductUpdate) {
            $totalProduct       = $connection->select()
                ->from($this->getMainTable(), 'total_order_linked_product')
                ->where('linked_product_id = ?', $productId);
            $totalProductUpdate = (int)$connection->fetchOne($totalProduct);
        }
        if (!$totalProductUpdate) {
            $totalProductUpdate = 1;
        }

        return $totalProductUpdate;
    }
}
