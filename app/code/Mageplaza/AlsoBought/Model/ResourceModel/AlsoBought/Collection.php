<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Mageplaza\AlsoBought\Model\AlsoBought', 'Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought');
    }

    /**
     * Get all product also bought from DB by passed id.
     *
     * @param string $productId
     * @return mixed
     */
    public function getProductListById($productId)
    {
        $fields     = ['product_id', 'linked_product_id'];
        $conditions = [$productId, $productId];
        $collection = $this->addFieldToFilter($fields, $conditions)->setOrder('pearson', 'DESC');

        return $collection;
    }

    /**
     * Truncate table mageplaza_wbab_product_link
     *
     * @return void
     */
    public function clear()
    {
        $connection = $this->getConnection();
        $tableName  = $this->getMainTable();
        $connection->truncateTable($tableName);
    }
}
