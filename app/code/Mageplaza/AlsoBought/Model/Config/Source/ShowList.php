<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class ShowList
 * @package Mageplaza\AlsoBought\Model\Config\Source
 */
class ShowList implements ArrayInterface
{
    const SHOW_PRICE  = 'price';
    const SHOW_CART   = 'addtocart';
    const SHOW_REVIEW = 'review';
    const SHOW_ADD    = 'addto';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return [
            self::SHOW_PRICE  => __('Price'),
            self::SHOW_CART   => __('Add to cart'),
            self::SHOW_REVIEW => __('Review'),
            self::SHOW_ADD    => __('Add to Wish List & Add to Compare')
        ];
    }
}
