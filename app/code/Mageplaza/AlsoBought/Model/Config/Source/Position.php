<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Position
 * @package Mageplaza\AlsoBought\Model\Config\Source
 */
class Position implements ArrayInterface
{
    const POS_TOP    = 1;
    const POS_BOTTOM = 2;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return [
            self::POS_TOP    => __('At the top of the page'),
            self::POS_BOTTOM => __('At the bottom of the page')
        ];
    }
}
