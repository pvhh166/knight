<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class AlsoBought
 * @package Mageplaza\AlsoBought\Model
 */
class AlsoBought extends AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought');
    }
}
