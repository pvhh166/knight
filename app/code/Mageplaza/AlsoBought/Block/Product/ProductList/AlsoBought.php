<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Block\Product\ProductList;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mageplaza\AlsoBought\Helper\Data;

/**
 * Class AlsoBought
 * @package Mageplaza\AlsoBought\Block\Product\ProductList
 */
class AlsoBought extends Template
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Data
     */
    private $alsoBoughtDataHelper;

    /**
     * AlsoBought constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Data $alsoBoughtDataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Data $alsoBoughtDataHelper,
        array $data = []
    )
    {
        $this->registry             = $registry;
        $this->alsoBoughtDataHelper = $alsoBoughtDataHelper;

        parent::__construct($context, $data);
    }

    /**
     * Get Also Bought Position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->alsoBoughtDataHelper->getConfig('position');
    }

    /**
     * Get Data send ajax
     *
     * @return mixed
     */
    public function getAjaxData()
    {
        $entityId = '';
        $action   = $this->getRequest()->getFullActionName();
        if ($action == 'catalog_product_view') {
            $entityId = $this->registry->registry('current_product')->getId();
        } else if ($action == 'catalog_category_view') {
            $entityId = $this->registry->registry('current_category')->getId();
        }

        $params = [
            'url'             => $this->getUrl('alsobought/ajax/load'),
            'originalRequest' => [
                'action'    => $action,
                'entity_id' => $entityId
            ]
        ];

        return json_encode($params);
    }
}
