<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Block\Product\ProductList;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Block\Product\Context;
use Magento\Widget\Block\BlockInterface;
use Mageplaza\AlsoBought\Helper\Data;
use Mageplaza\AlsoBought\Model\AlsoBoughtFactory;

/**
 * Class ProductList
 * @package Mageplaza\AlsoBought\Block\Product\ProductList
 */
class ProductList extends AbstractProduct implements BlockInterface
{
    /**
     * @var \Mageplaza\AlsoBought\Model\ResourceModel\AlsoBought\CollectionFactory
     */
    private $alsoBoughtCollectionFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Data
     */
    private $alsoBoughtDataHelper;

    /**
     * ProductList constructor.
     * @param Context $context
     * @param AlsoBoughtFactory $alsoBoughtCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Data $alsoBoughtDataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        AlsoBoughtFactory $alsoBoughtCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Data $alsoBoughtDataHelper,
        array $data = []
    )
    {
        $this->productRepository           = $productRepository;
        $this->alsoBoughtCollectionFactory = $alsoBoughtCollectionFactory;
        $this->alsoBoughtDataHelper        = $alsoBoughtDataHelper;

        parent::__construct($context, $data);
    }

    /**
     * Get heading label
     *
     * @return string
     */
    public function getTitleBlock()
    {
        return $this->alsoBoughtDataHelper->getConfig('heading_label');
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductCollection()
    {
        $collection           = [];
        $alsoBoughtCollection = $this->alsoBoughtCollectionFactory->create();
        $productIds           = $this->getProductIds();
        $limit                = $this->alsoBoughtDataHelper->getConfigGeneral('limit_product');
        $count                = 0;
        $productRender        = [];
        foreach ($productIds as $productId) {
            if ($productId) {
                $productList = $alsoBoughtCollection->getCollection()->getProductListById($productId);
                foreach ($productList as $productAlsoBought) {
                    $productAlsoBoughtId = $productAlsoBought->getProductId();
                    if ($productAlsoBoughtId == $productId) {
                        $productAlsoBoughtId = $productAlsoBought->getLinkedProductId();
                    }

                    if (in_array($productAlsoBoughtId, $productIds) || in_array($productAlsoBoughtId, $productRender)) {
                        continue;
                    }

                    $product = $this->productRepository->getById($productAlsoBoughtId);
                    if (!$product || !$product->isVisibleInSiteVisibility()) {
                        continue;
                    }
                    $productRender[] = $productAlsoBoughtId;
                    $collection[]    = $product;
                    $count++;
                    if ($limit && $count >= $limit) {
                        break;
                    }
                }
            }
        }

        if ($limit) {
            $collection = array_slice($collection, 0, $limit);
        }

        return $collection;
    }

    /**
     * Get layout config
     *
     * @return int
     */
    public function getLayoutSlider()
    {
        $layout = $this->alsoBoughtDataHelper->getConfig('layout');
        if ($layout && $layout == 1) {
            return true;
        }

        return false;
    }

    /**
     * Get Show list Config
     *
     * @return array
     */
    public function getShowList()
    {
        if ($this->alsoBoughtDataHelper->getConfig('show_list')) {
            return explode(',', $this->alsoBoughtDataHelper->getConfig('show_list'));
        }

        return [];
    }
}
