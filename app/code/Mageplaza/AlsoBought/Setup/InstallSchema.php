<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Mageplaza\AlsoBought\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn($installer->getTable('sales_order'), 'mageplaza_alsobought_status', [
            'type'     => Table::TYPE_SMALLINT,
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
            'comment'  => 'Mageplaza Alsobought Status'
        ]);

        if (!$installer->tableExists('mageplaza_wbab_product_link')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('mageplaza_wbab_product_link'))
                ->addColumn('link_id', Table::TYPE_INTEGER, null, [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ], 'Link ID')
                ->addColumn('product_id', Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Product ID')
                ->addColumn('linked_product_id', Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Linked Product ID')
                ->addColumn('total_order_product', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Total Order Product')
                ->addColumn('total_order_linked_product', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Total Order Product')
                ->addColumn('total_order_combo', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Total Order Product')
                ->addColumn('pearson', Table::TYPE_FLOAT, '10,2', ['default' => '0'], 'Pearson')
                ->addIndex($installer->getIdxName('mageplaza_wbab_product_link', ['pearson']), ['pearson'])
                ->addForeignKey(
                    $installer->getFkName(
                        'mageplaza_wbab_product_link',
                        'linked_product_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'linked_product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'mageplaza_wbab_product_link',
                        'product_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
