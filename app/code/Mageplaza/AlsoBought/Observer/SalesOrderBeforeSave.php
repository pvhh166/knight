<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Observer;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Mageplaza\AlsoBought\Helper\Data;
use Mageplaza\AlsoBought\Model\ResourceModel\AlsoBoughtFactory;

/**
 * Class SalesOrderBeforeSave
 * @package Mageplaza\AlsoBought\Observer
 */
class SalesOrderBeforeSave implements ObserverInterface
{
    /**
     * @var Data
     */
    private $alsoBoughtDataHelper;

    /**
     * @var AlsoBoughtFactory
     */
    private $alsoBoughtFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Constructor
     *
     * @param AlsoBoughtFactory $alsoBoughtFactory
     * @param Data $alsoBoughtDataHelper
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        AlsoBoughtFactory $alsoBoughtFactory,
        Data $alsoBoughtDataHelper,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->alsoBoughtDataHelper = $alsoBoughtDataHelper;
        $this->alsoBoughtFactory    = $alsoBoughtFactory;
        $this->productRepository    = $productRepository;
    }

    /**
     * Update also bought by order before save
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $processOrderStatus = $this->alsoBoughtDataHelper->getProcessOrderStatus();
        $order              = $observer->getEvent()->getOrder();
        if ($order instanceof \Magento\Framework\Model\AbstractModel && !empty($processOrderStatus)) {
            $status = $order->getStatus();
            if (in_array($status, $processOrderStatus) && !$order->getMageplazaAlsoboughtStatus()) {
                $order->setMageplazaAlsoboughtStatus(true);
                $this->updateAlsoBought($order);
            }
        }

        return $this;
    }

    /**
     * Update total combo also bought by order
     *
     * @param null $order
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function updateAlsoBought($order = null)
    {
        if ($order) {
            $items      = $order->getAllVisibleItems();
            $productIds = [];
            foreach ($items as $item) {
                if ($item->getProductType() == 'simple') {
                    $product = $this->productRepository->getById($item->getProductId());
                    if (!$product || !$product->isVisibleInSiteVisibility()) {
                        continue;
                    }
                }
                $this->alsoBoughtFactory->create()->updateTotalProduct($item->getProductId());
                $productIds[] = $item->getProductId();
            }
            $this->alsoBoughtFactory->create()->updateTotalCombo($productIds);
        }
    }
}
