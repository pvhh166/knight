<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Helper;

use Mageplaza\Core\Helper\AbstractData;

/**
 * Class Data
 * @package Mageplaza\AlsoBought\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'alsobought';

    /**
     * Get Config
     *
     * @param $field
     * @param null $storeId
     * @return bool|mixed
     */
    public function getConfig($field, $storeId = null)
    {
        if (!$this->isEnabled($storeId)) {
            return false;
        }
        $action = $this->getData('action');
        if ($this->_request->getFullActionName() == 'catalog_product_view' || $action == 'catalog_product_view') {
            return $this->getProductPageConfig($field, $storeId);
        }
        if ($this->_request->getFullActionName() == 'catalog_category_view' || $action == 'catalog_category_view') {
            return $this->getCatalogPageConfig($field, $storeId);
        }
        if ($this->_request->getFullActionName() == 'checkout_cart_index' || $action == 'checkout_cart_index') {
            return $this->getCartPageConfig($field, $storeId);
        }

        return false;
    }

    /**
     * Get Product Page Config
     *
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getProductPageConfig($field, $storeId = null)
    {
        if ($this->getModuleConfig('configuration_product_page/enabled', $storeId)) {
            return $this->getModuleConfig('configuration_product_page/' . $field, $storeId);
        }

        return null;
    }

    /**
     * Get Catalog Page Config
     *
     * @param $field
     * @param null $storeId
     * @return mixed|null
     */
    public function getCatalogPageConfig($field, $storeId = null)
    {
        if ($this->getModuleConfig('configuration_catalog_page/enabled', $storeId)) {
            return $this->getModuleConfig('configuration_catalog_page/' . $field, $storeId);
        }

        return null;
    }

    /**
     * Get Cart Page Config
     *
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getCartPageConfig($field, $storeId = null)
    {
        if ($this->getModuleConfig('configuration_cart_page/enabled', $storeId)) {
            return $this->getModuleConfig('configuration_cart_page/' . $field, $storeId);
        }

        return null;
    }

    /**
     * Get Process Order Status Config
     *
     * @return mixed
     */
    public function getProcessOrderStatus()
    {
        if ($this->getConfigGeneral('process_order_status') && $this->isEnabled()) {
            return explode(',', $this->getConfigGeneral('process_order_status'));
        }

        return false;
    }
}
