<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AlsoBought\Console\Command;

use Magento\Framework\App\State;
use Mageplaza\AlsoBought\Model\ResourceModel\AlsoBoughtFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Reindex
 * @package Mageplaza\AlsoBought\Console\Command
 */
class Reindex extends Command
{
    /**
     * @var \Mageplaza\AlsoBought\Model\AlsoBought
     */
    private $alsoBoughtFactory;

    /**
     * @var State
     */
    private $appState;

    /**
     * Reindex constructor.
     * @param AlsoBoughtFactory $alsoBoughtFactory
     * @param State $appState
     * @param null $name
     */
    public function __construct(
        AlsoBoughtFactory $alsoBoughtFactory,
        State $appState,
        $name = null
    )
    {
        $this->alsoBoughtFactory = $alsoBoughtFactory;
        $this->appState          = $appState;

        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('alsobought:reindex')
            ->setDescription('Reindex Who Bought This Item Also Bought');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->appState->setAreaCode('frontend');
            $this->alsoBoughtFactory->create()->reIndex();
            $output->writeln('<info>Successfully!</info>');
        } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
        }
    }
}
