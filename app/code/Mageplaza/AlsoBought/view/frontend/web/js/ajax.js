/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AlsoBought
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'mage/storage',
    'jquery/ui',
    'mageplaza/core/owl.carousel'
], function ($, storage) {
    'use strict';

    $.widget('mageplaza.alsobought_block', {
        options: {
            ajaxData: {}
        },
        _create: function () {
            if (this.element.length > 0) {
                this._loadAjax();
            }
        },

        _blink: function () {
            $('#mageplaza-alsobought-slider').owlCarousel({
                items: 5,
                autoplay: true,
                autoplayTimeout: 2000,
                lazyLoad: true,
                dots: false,
                responsive: {
                    0: {
                        items: 2
                    },
                    640: {
                        items: 3
                    },
                    1024: {
                        items: 5
                    }
                }
            });
        },

        _loadAjax: function () {
            var _this = this;
            var url = this.options.ajaxData.url,
                payLoad = this.options.ajaxData.originalRequest;

            storage.post(url, JSON.stringify(payLoad), false)
                .done(function (response) {
                    if (!response.status) {
                        return;
                    }
                    _this.element.empty().append(response.content);
                    _this._blink();
                    _this.element.trigger('contentUpdated');
                });
        }
    });

    return $.mageplaza.alsobought_block;
});