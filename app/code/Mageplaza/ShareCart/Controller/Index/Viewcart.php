<?php
namespace Mageplaza\ShareCart\Controller\Index;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;

class Viewcart extends \Magento\Framework\App\Action\Action
{
	protected $custsession;
	protected $_responseFactory;
	protected $_url;

	protected $cookieManager;
    protected $cookieMetadataFactory;
    protected $sessionManager;
	const COOKIE_REDIRECT = 'SHARE_CART_URL';
	const GONE_TO = 'GONE_TO';

	public function __construct(
		\Magento\Framework\App\Action\Context $context, 
		\Magento\Customer\Model\Session $custsession,
		CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
		\Magento\Framework\App\ResponseFactory $responseFactory
	)
    {
		$this->custsession = $custsession;
		$this->_responseFactory = $responseFactory;
		$this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
		parent::__construct($context);
    }

	public function execute()
	{
		$this->_view->loadLayout();
    	$this->_view->renderLayout();
    	$urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
		$currentShareUrl = $urlInterface->getCurrentUrl();
		$shareCartUrl = $this->cookieManager->getCookie(self::COOKIE_REDIRECT);
		$goneTo = $this->cookieManager->getCookie(self::GONE_TO);

        if(!$shareCartUrl && !$goneTo){
            $metadata = $this->cookieMetadataFactory
            			->createPublicCookieMetadata()
            			->setDuration(1800)
            			->setPath($this->sessionManager->getCookiePath())
            			->setDomain($this->sessionManager->getCookieDomain());

	        $this->cookieManager->setPublicCookie(
	            self::COOKIE_REDIRECT,
	            $currentShareUrl,
	            $metadata
	        );
        }
	}
}