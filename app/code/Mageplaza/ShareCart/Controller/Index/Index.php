<?php

/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ShareCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ShareCart\Controller\Index;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\ShareCart\Helper\Data;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Class Index
 * @package Mageplaza\ShareCart\Controller\Index
 */
class Index extends Action
{
    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Data
     */
    protected $helper;

    protected $serializer;

    protected $custaddcart;

    protected $checkoutSession;

    protected $savecartdetailFactory;
    protected $formKey;
    protected $resultFactory;

    protected $cookieManager;
    protected $cookieMetadataFactory;
    protected $sessionManager;

    const COOKIE_NAME = 'SHARE_CART_ID';
    /**
     * Index constructor.
     * @param Context $context
     * @param CartRepositoryInterface $cartRepository
     * @param Cart $cart
     * @param ProductRepository $productRepository
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $cartRepository,
        FormKey $formKey,
        Cart $cart,
        Session $checkoutSession,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magecomp\Savecartpro\Controller\Customer\Custaddcart $custaddcart,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        SavecartprodetailFactory $savecartdetailFactory,
        ResultFactory $resultFactory,
        Data $helper
    )
    {
        $this->cartRepository     = $cartRepository;
        $this->formKey = $formKey;
        $this->cart               = $cart;
        $this->checkoutSession = $checkoutSession;
        $this->_productRepository = $productRepository;
        $this->_storeManager      = $storeManager;
        $this->savecartdetailFactory = $savecartdetailFactory;
        $this->resultFactory = $resultFactory;
        $this->helper             = $helper;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->custaddcart = $custaddcart;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;

        return parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialize = $objectManager->create('\Magento\Framework\Serialize\Serializer\Serialize');
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->helper->isEnabled()) {
            $quoteId = base64_decode($this->getRequest()->getParam('key'), true);
            if ($quoteId) {
                $shareCartId = $this->getShareCartCookie();

                if(!$shareCartId){
                    $this->setShareCartCookie($quoteId,72000);
                }

                $quotedetailCollection = $this->savecartdetailFactory->create()->getCollection()->addFieldToFilter('savecart_id', $quoteId);

                $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
                foreach ($allItems as $item) {
                    $itemId = $item->getItemId();
                    $this->cart->removeItem($itemId);
                }
                if (count($quotedetailCollection->getData()) > 0) {
                    foreach ($quotedetailCollection->getData() as $col) {
                        $configdata = $serialize->unserialize($col['quote_config_prd_data']);
                        $obj_product = $objectManager->create('Magento\Catalog\Model\Product');
                        array_push($configdata, 'form_key', $this->formKey->getFormKey());
                        $_product = $obj_product->load($col['quote_prd_id']);
                        if($_product->getTypeId() == 'super') {
                            $this->custaddcart->addSuperProductToCart($this->cart, $configdata['super_options']);
                        }else{
                            $this->cart->addProduct($_product, $configdata);
                        }
                    }

                    $this->cart->save();
                }else{
                    $this->messageManager->addError(__('There no products in your save cart'));
                }
            }
        }

        $resultRedirect->setPath('checkout/cart/');
        return $resultRedirect;
    }

    /**
     * Get form key cookie
     *
     * @return string
     */
    public function getShareCartCookie()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    /**
     * @param string $value
     * @param int $duration
     * @return void
     */
    public function setShareCartCookie($value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $value,
            $metadata
        );
    }
}