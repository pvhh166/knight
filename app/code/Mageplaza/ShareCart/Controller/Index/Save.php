<?php
namespace Mageplaza\ShareCart\Controller\Index;

use Magento\Checkout\Model\Session;
use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\ShareCart\Helper\Data;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Save extends Action
{
    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Data
     */
    protected $helper;

    protected $serializer;

    protected $custaddcart;
    protected $cartsession;

    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $jsonFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param CartRepositoryInterface $cartRepository
     * @param Cart $cart
     * @param ProductRepository $productRepository
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $cartRepository,
        Cart $cart,
        Session $session,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magecomp\Savecartpro\Controller\Customer\Custaddcart $custaddcart,
        Data $helper,
        SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        JsonFactory $jsonFactory
    )
    {
        $this->cartRepository     = $cartRepository;
        $this->cart               = $cart;
        $this->cartsession = $session;
        $this->_productRepository = $productRepository;
        $this->_storeManager      = $storeManager;
        $this->helper             = $helper;
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->custaddcart = $custaddcart;
        $this->jsonFactory = $jsonFactory;

        return parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialize = $objectManager->create('\Magento\Framework\Serialize\Serializer\Json');
        try {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $data = $this->_request->getParams();
            $quote = $this->cartsession->getQuote();
            $customer_id = $quote->getCustomerId();

            $cartname = $data['cartname'];
            if($cartname!="") {

                if ($customer_id != null) {
                    // Quote Save To Main Tabel
                    $savecartmodel = $this->modelsavecart->create()
                        ->setCartName($cartname)
                        ->setCustomerId($customer_id)
                        ->setCreatedAt(date('Y-m-d H:i:s'))
                        ->save();
                    $saveCartId = $savecartmodel->getId();
                    // Quote Data Save To Detail Tabel
                    $visitems = $this->cartsession->getQuote()->getAllVisibleItems();

                    foreach ($visitems as $item) {
                        foreach ($item->getOptions() as $option) {
                            $itemOptions = $serialize->unserialize($option['value']);
                            if(is_array($itemOptions)) {
                                $itemOptions['product_simple_sku'] = $item->getProduct()->getSku();
                                if($item->getProduct()->getTypeId() == 'super') {
                                    $addOptions = $item->getOptionByCode('super_custom_option');
                                    if ($addOptions) {
                                        $options = $serialize->unserialize($addOptions->getValue());
                                        $itemOptions['super_options'] = $options;
                                    }
                                }
                            }
                            $savecartdetailmodel = $this->modelsavecartdetail->create()
                                ->setSavecartId($savecartmodel->getId())
                                ->setQuotePrdId($item->getProductId())
                                ->setQuotePrdQty($item->getQty())
                                ->setQuotePrdPrice($item->getPrice())
                                ->setQuoteConfigPrdData(serialize($itemOptions))
                                ->save();
                            break;
                        }
                    }
                    
                    $message = "Your cart has been successfully saved.";
                    $this->messageManager->addSuccess($message);
                    $response = array();
                    $resultJson = $this->jsonFactory->create();
                    $response['sharecart_id'] = base64_encode($saveCartId);
                    $response['sharecart_url'] = $this->_url->getUrl('sharecart/index/viewcart',['key'=>base64_encode($saveCartId)]);
                    $resultJson->setData($response);
                    return $resultJson;

                } else {
                    $message = "Before saving cart you must have login";
                    $this->messageManager->addError($message);
                    $accUrl = $this->_url->getUrl('customer/account/login');
                    $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                    $this->setRefererUrl($accUrl);
                }
            }

        }catch(Exception $e)
        {
            $this->messageManager->addError($e->getMessage());
            $accUrl = $this->_url->getUrl('savecart/index/view');
            $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
            $this->setRefererUrl($accUrl);
        }
    }
}