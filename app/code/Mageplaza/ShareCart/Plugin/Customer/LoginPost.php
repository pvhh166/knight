<?php
namespace Mageplaza\ShareCart\Plugin\Customer;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;

class LoginPost
{
    protected $cookieManager;
    protected $cookieMetadataFactory;
    protected $sessionManager;
    const COOKIE_REDIRECT = 'SHARE_CART_URL';
    const GONE_TO = 'GONE_TO';

    public function __construct(
        Context $context,
        Session $customerSession,
        SessionManagerInterface $sessionManager,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        PageFactory $resultPageFactory
    ) {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->session = $customerSession;
        $this->sessionManager = $sessionManager;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->resultFactory = $context->getResultFactory();

    }

        public function afterExecute()
        {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            if ($this->session->isLoggedIn()) {
                $shareCartUrl = $this->cookieManager->getCookie(self::COOKIE_REDIRECT);
                
                if($shareCartUrl){
                    $resultRedirect->setUrl($shareCartUrl);
                    // delete cookie
                    $metadata = $this->cookieMetadataFactory
                    ->createPublicCookieMetadata()
                    ->setDuration(1800)
                    ->setPath($this->sessionManager->getCookiePath())
                    ->setDomain($this->sessionManager->getCookieDomain());

                    $this->cookieManager->deleteCookie(
                        self::COOKIE_REDIRECT,
                        $metadata
                    );
                    // set gone to
                    $metadata = $this->cookieMetadataFactory
                        ->createPublicCookieMetadata()
                        ->setDuration(60)
                        ->setPath($this->sessionManager->getCookiePath())
                        ->setDomain($this->sessionManager->getCookieDomain());

                    $this->cookieManager->setPublicCookie(
                        self::GONE_TO,
                        1,
                        $metadata
                    );
                }else{
                    $resultRedirect->setPath('customer/account');
                }

                return $resultRedirect;
            }else{
                $resultRedirect->setPath('customer/account/login');

                return $resultRedirect;
            }
        }
    }
?>