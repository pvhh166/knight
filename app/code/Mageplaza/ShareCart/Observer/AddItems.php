<?php

namespace Mageplaza\ShareCart\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Checkout\Model\Session;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;

class AddItems implements \Magento\Framework\Event\ObserverInterface
{

    protected $cookieManager;
    protected $cookieMetadataFactory;
    protected $sessionManager;
    protected $custaddcart;
    protected $checkoutSession;
    protected $formKey;
    protected $cart;
    protected $customerSession;

    const COOKIE_NAME = 'SHARE_CART_ID';

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        Session $checkoutSession,
        FormKey $formKey,
        Cart $cart,
        SavecartprodetailFactory $savecartdetailFactory,
        \Magecomp\Savecartpro\Controller\Customer\Custaddcart $custaddcart
    ) {
        $this->customerSession = $customerSession;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->checkoutSession = $checkoutSession;
        $this->formKey = $formKey;
        $this->cart               = $cart;
        $this->savecartdetailFactory = $savecartdetailFactory;
        $this->custaddcart = $custaddcart;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $isCustomerLoggedIn = $this->customerSession->isLoggedIn();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialize = $objectManager->create('\Magento\Framework\Serialize\Serializer\Serialize');

        if ($isCustomerLoggedIn) {
            $event = $observer->getEvent();
            $shareCartId = $this->cookieManager->getCookie(self::COOKIE_NAME);
            if($shareCartId){
                $quotedetailCollection = $this->savecartdetailFactory->create()->getCollection()->addFieldToFilter('savecart_id', $shareCartId);
                $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
                foreach ($allItems as $item) {
                    $itemId = $item->getItemId();
                    $this->cart->removeItem($itemId);
                }
                if (count($quotedetailCollection->getData()) > 0) {
                    foreach ($quotedetailCollection->getData() as $col) {
                        $configdata = $serialize->unserialize($col['quote_config_prd_data']);
                        $obj_product = $objectManager->create('Magento\Catalog\Model\Product');
                        array_push($configdata, 'form_key', $this->formKey->getFormKey());
                        $_product = $obj_product->load($col['quote_prd_id']);
                        if($_product->getTypeId() == 'super') {
                            $this->custaddcart->addSuperProductToCart($this->cart, $configdata['super_options']);
                        }else{
                            $this->cart->addProduct($_product, $configdata);
                        }
                    }
                    $this->cart->save();
                }
                $this->deleteShareCartCookie(7200);
            }
        }
    }

    /**
     * @return void
     */
    public function deleteShareCartCookie($duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->deleteCookie(
            self::COOKIE_NAME,
            $metadata
        );
    }
}