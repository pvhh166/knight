# CHANGELOG

## 1.3.0 (2010-08-30)
 - Fixed issue with image layers when full page cache is enabled
 - Fixed issue with layers flickering on page load
 - Load layer image size from theme config instead of module system configuration 

## 1.2.1 (2010-08-30)
 - Fixed issue with product image and options alignment in Outlook
 
## 1.2.0 (2010-08-07)
 - New Feature: Product image in a new order email and order detail page

## 1.1.12 (2018-07-31)
 - Fix: Don't show 0.0000 custom option value price on frontend
