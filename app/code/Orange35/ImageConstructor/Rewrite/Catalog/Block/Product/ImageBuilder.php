<?php

namespace Orange35\ImageConstructor\Rewrite\Catalog\Block\Product;

use Magento\Catalog\Model\Product\Image\NotLoadInfoImageException;

class ImageBuilder extends \Magento\Catalog\Block\Product\ImageBuilder
{
    public function create()
    {
        /** @var \Magento\Catalog\Helper\Image $helper */
        $helper = $this->helperFactory->create()
            ->init($this->product, $this->imageId);

        $template = $helper->getFrame()
            ? 'Orange35_ImageConstructor::product/image.phtml'
            : 'Orange35_ImageConstructor::product/image_with_borders.phtml';

        try {
            $imagesize = $helper->getResizedImageInfo();
        } catch (NotLoadInfoImageException $exception) {
            $imagesize = [$helper->getWidth(), $helper->getHeight()];
        }

        $data = [
            'data' => [
                'template' => $template,
                'image_url' => $helper->getUrl(),
                'width' => $helper->getWidth(),
                'height' => $helper->getHeight(),
                'label' => $helper->getLabel(),
                'ratio' =>  $this->getRatio($helper),
                'custom_attributes' => $this->getCustomAttributes(),
                'resized_image_width' => $imagesize[0],
                'resized_image_height' => $imagesize[1],
            ],
        ];

        return $this->imageFactory->create($data);
    }
}
