<?php

namespace Orange35\ImageConstructor\Plugin\Wishlist\Block\Customer\Wishlist\Item\Column;

use Magento\Wishlist\Block\Customer\Wishlist\Item\Column\Image as ColumnImage;
use Magento\Catalog\Block\Product\Image as ProductImage;
use Orange35\ImageConstructor\Helper\Image as ImageHelper;

class Image
{
    /** @var ImageHelper */
    private $imageHeper;

    public function __construct(ImageHelper $imageHeper)
    {
        $this->imageHeper = $imageHeper;
    }

    public function afterGetImage(ColumnImage $column, ProductImage $image, $product, $imageId)
    {
        $item = $column->getItem();

        return $image->setData('layers', $this->imageHeper->getLayers($item, $imageId));
    }
}
