define([
  'jquery',
  'underscore'
], function ($, _) {
  "use strict";

  $.widget('orange35.imageConstructor', {
    options: {
      nodes: {
        galleryPlaceholder: '[data-gallery-role="gallery-placeholder"]',
        gallery: '[data-gallery-role="gallery"]',
        form: '#product_addtocart_form',
        imageContainer: '.fotorama__stage__frame',
        zoomImage: '.fotorama__img--full'
      }
    },

    _create: function () {
      this.timer = null;
      this.requestAnimationFrame = (window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame).bind(window);

      this.get('galleryPlaceholder').one('gallery:loaded', _.bind(this.galleryOnLoad, this));
    },

    galleryOnLoad: function (event) {
      this.gallery = this.get('galleryPlaceholder').data('gallery');
      this.mainImage = _.find(this.gallery.returnCurrentImages(), function (image) {
        return image.isMain;
      });

      this.get('form').on('change', _.bind(this.formOnChange, this));
      this.get('gallery').on('fotorama:load', _.debounce(_.bind(this.onFotoramaLoad, this), 50));
    },

    get: function (alias, context) {
      return $(this.options.nodes[alias], context);
    },

    formOnChange: function () {
      this.update();
    },

    onFotoramaLoad: function (event) {
      this.update();
    },

    /**
     * Render layers once after set of form updates
     */
    update: function () {
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(this.renderLayers.bind(this), 10);
    },

    basename: function (path) {
      return path.substring(path.lastIndexOf('/') + 1);
    },

    getMainImageFrame: function () {
      return this.get('imageContainer').filter('[href$="' + this.basename(this.mainImage.full) + '"]');
    },

    renderLayers: function () {
      var layers = this.getChosenLayers();

      this.preloadLayerImages(layers, (function() {
        var $frame = this.getMainImageFrame();
        var $images = $frame.find('img').not('[data-o35-layer]');
        var render = (function () {
          $images.map((function(index, image){
            this.renderImageLayers($(image), layers, $frame);
          }).bind(this));
          this.hideProgress();
        }).bind(this);

        this.requestAnimationFrame(render);
      }).bind(this));
    },

    preloadLayerImages: function (layers, callback) {
      var i, count = layers.length, layer, image, allLoaded = true;

      for (i = 0; i < layers.length; i++) {
        if (!layers[i].imageLoaded) {
          allLoaded = false;
          break;
        }
      }

      if (!allLoaded) {
        this.showProgress();
      }

      for (i = 0; i < layers.length; i++) {
        layer = layers[i];
        if (layer.imageLoaded) {
          count--;
          continue;
        }
        image = new Image();
        image.onload = (function (layer) {
          layer.imageLoaded = true;
          if (--count === 0) {
            return callback.call();
          }
        }).bind(this, layer);
        image.src = layer.mediumImage;
      }
      if (count === 0) {
        return callback.call();
      }
    },

    showProgress: function () {
      $('.fotorama__spinner').show();
    },

    hideProgress: function () {
      $('.fotorama__spinner').hide();
    },

    renderImageLayers: function ($image, layers, $frame) {
      var layer, $layer, i,
        isLarge = $image.is(this.options.nodes.zoomImage),
        type = isLarge ? 'large' : 'medium',
        fragment = document.createDocumentFragment();

      $frame.find('img').filter('[data-o35-layer=' + type + ']').remove();
      for (i = 0; i < layers.length; i++) {
        layer = layers[i];
        $layer = $image.clone()
          .attr('data-o35-layer', type)
          .attr('data-id', layer.valueId)
          .attr('src', isLarge ? layer.largeImage : layer.mediumImage);
        fragment.appendChild($layer.get(0));
      }
      $frame.get(0).appendChild(fragment);
    },

    getChosenLayers: function () {
      var layers = [], layer;
      var data = this.get('form').serializeArray();
      for (var i = 0; i < data.length; i++) {
        if (0 === data[i].name.indexOf('options[') && data[i].value !== "") {
          layer = _.findWhere(this.options.layers, {valueId: data[i].value});
          if (layer && !_.isNull(layer.largeImage)) {
            layers.push(layer);
          }
        }
      }
      _.sortBy(layers, function (l) {
        return l.sortOrderOption;
      });
      return layers;
    }
  });

  return $.orange35.imageConstructor;
});
