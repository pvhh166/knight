<?php

/**
 * Product:       Xtento_CustomTrackers
 * ID:            AYgt7YrQLbqNh1RXmMPdQJ21sBShSq5ATMo3rS/iIw8=
 * Last Modified: 2016-02-25T21:32:52+00:00
 * File:          app/code/Xtento/CustomTrackers/Helper/Module.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\CustomTrackers\Helper;

class Module extends \Xtento\XtCore\Helper\AbstractModule
{
    protected $edition = 'CE';
    protected $module = 'Xtento_CustomTrackers';
    protected $extId = 'MTWOXtento_CustomTrackers991229';
    protected $configPath = 'customtrackers/general/';

    // Module specific functionality below
}
