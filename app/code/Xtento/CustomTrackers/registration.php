<?php

/**
 * Product:       Xtento_CustomTrackers
 * ID:            AYgt7YrQLbqNh1RXmMPdQJ21sBShSq5ATMo3rS/iIw8=
 * Last Modified: 2015-10-12T14:25:06+00:00
 * File:          app/code/Xtento/CustomTrackers/registration.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Xtento_CustomTrackers',
    __DIR__
);
