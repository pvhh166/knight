<?php

/**
 * Product:       Xtento_CustomTrackers
 * ID:            AYgt7YrQLbqNh1RXmMPdQJ21sBShSq5ATMo3rS/iIw8=
 * Last Modified: 2015-07-12T09:56:59+00:00
 * File:          app/code/Xtento/CustomTrackers/Model/System/Config/Source/DefaultCarriers.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\CustomTrackers\Model\System\Config\Source;

class DefaultCarriers extends \Xtento\XtCore\Model\System\Config\Source\Shipping\Carriers
{
    /**
     * Get all default carriers - and not our custom ones
     *
     * @return array
     */
    public function toOptionArray()
    {
        $carriers = parent::toOptionArray();
        foreach ($carriers as $key => $carrier) {
            if (preg_match('/^tracker/', $carrier['value'])) {
                unset($carriers[$key]);
            }
        }
        return $carriers;
    }
}
