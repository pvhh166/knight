<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_OrderStatus
 */


namespace Amasty\OrderStatus\Model\Order\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Amasty\OrderStatus\Model\ResourceModel\Status\CollectionFactory;

class Status
{
    protected $orderConfig;

    protected $scopeConfig;

    protected $amastyStatusCollectionFactory;

    public function __construct(
        \Magento\Sales\Model\Order\Config $orderConfig,
        ScopeConfigInterface $scopeConfig,
        CollectionFactory $collectionFactory
    ) {
        $this->orderConfig = $orderConfig;
        $this->scopeConfig = $scopeConfig;
        $this->amastyStatusCollectionFactory = $collectionFactory;
    }

    public function afterLoad($subject, $result)
    {
        if (!$result->getLabel()) {
            $code = $subject->getStatus();
            $statusesCollection = $this->amastyStatusCollectionFactory->create();

            if ($statusesCollection->getSize() > 0) {
                $hideState = ($this->scopeConfig->getValue('amostatus/general/hide_state')) ? true : false;
                $statusLabel = '';

                foreach ($this->orderConfig->getStates() as $state => $node) {
                    $stateLabel = trim((string)$node->getText());

                    foreach ($statusesCollection as $status) {
                        if ($status->getData('is_active') && !$status->getData('is_system')) {
                            // checking if we should apply status to the current state
                            $parentStates = array();

                            if ($status->getParentState()) {
                                $parentStates = explode(',', $status->getParentState());
                            }

                            if (!$parentStates || in_array($state, $parentStates)) {
                                $elementName = $state . '_' . $status->getAlias();

                                if ($code == $elementName) {
                                    $statusLabel = ($hideState ? '' : $stateLabel . ': ') . __($status->getStatus());
                                    break(2);
                                }
                            }
                        }
                    }
                }
            }

            $status->setLabel($statusLabel);
            $status->setStoreLabel($statusLabel);
            $result = $status;
        }

        return $result;
    }
}
