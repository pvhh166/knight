<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */


namespace Amasty\ProductAttachment\Plugin;

use Amasty\ProductAttachment\Model\ConfigProvider;

class TabPosition
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        ConfigProvider $configProvider
    ) {

        $this->configProvider = $configProvider;
    }

    public function afterGetGroupChildNames(\Magento\Catalog\Block\Product\View\Description $block, $result)
    {
        if (!$this->configProvider->isEnabled() || !$this->configProvider->isBlockEnabled()) {
            return $result;
        }

        $position = $this->configProvider->getBlockPosition();
        $sibling = $this->configProvider->getBlockSiblingTab();

        if (!in_array($sibling, $block->getChildNames())) {
            $sibling = '-';
        }
        $key = array_search('amfile.attachment', $result);
        if ($key !== false) {
            unset($result[$key]);
        } else {
            $tabBlock = $block->getLayout()->createBlock(
                \Amasty\ProductAttachment\Block\Product\AttachmentsTab::class,
                'amfile.attachment',
                [
                    'group' => 'detailed_info'
                ]
            )->setTitle($this->configProvider->getBlockTitle());
            $block->setChild('amfile.attachment', $tabBlock);
        }
        $myResult = [];
        $tabAdded = false;
        foreach ($result as $key => $item) {
            if ($position == 'before' && $tabAdded === false
                && ($item == $sibling || $sibling == '-')
            ) {
                $myResult[] = 'amfile.attachment';
                $tabAdded = true;
            }
            $myResult[] = $item;
            if ($position == 'after' && $tabAdded === false
                && ($item == $sibling || $sibling == '-')
            ) {
                $myResult[] = 'amfile.attachment';
                $tabAdded = true;
            }
        }

        return $myResult;
    }
}
