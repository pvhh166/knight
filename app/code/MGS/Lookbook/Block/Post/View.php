<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */
namespace MGS\Lookbook\Block\Post;

use Magento\Store\Model\ScopeInterface;
use  MGS\Lookbook\Block\AbstractLookbook;

/**
 * Blog post view
 */
class View extends AbstractLookbook implements \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * Retrieve identities
     *
     * @return string
     */
    public function getIdentities()
    {
        return $this->getPost()->getIdentities();
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $post = $this->getPost();
        if ($post) {
            $this->_addBreadcrumbs($post->getName(), 'lookbook_post');
            $this->pageConfig->addBodyClass('lookbook-post-' . $post->getIdentifier());
            $this->pageConfig->getTitle()->set($post->getName());
            $this->pageConfig->setKeywords($post->getName());
            $this->pageConfig->setDescription($this->stripTags($post->getContent()));
            /*$this->pageConfig->addRemotePageAsset(
                $post->getCanonicalUrl(),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );*/

            $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
            if ($pageMainTitle) {
                $pageMainTitle->setPageTitle(
                    $this->escapeHtml($post->getName())
                );
            }
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @param  string $title
     * @param  string $key
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs($title = null, $key = null)
    {
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbsBlock->addCrumb(
            'home',
            [
                'label' => __('Lookbook'),
                'title' => __('Go to Lookbook'),
                'link' => $this->_storeManager->getStore()->getBaseUrl().'lookbook'
            ]
        );

        $breadcrumbsBlock->addCrumb($key, [
            'label' => $title ,
            'title' => $title
        ]);

    }
}
