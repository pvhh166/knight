<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MGS\Lookbook\Block\Post;

/**
 * Widget to display link to CMS page
 */
class Lookbooks extends \MGS\Lookbook\Block\AbstractLookbook implements \Magento\Widget\Block\BlockInterface
{

    /**
     * @var \MGS\Lookbook\Model\ResourceModel\Lookbook\CollectionFactory
     */
    protected $lookbookCollectionFactory;
    protected $_lookbooks;
    protected $_coreRegistry;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\Context $productContext,
        \MGS\Lookbook\Helper\Data $_helper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $_productCollectionFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Framework\Registry $coreRegistry,
        \MGS\Lookbook\Model\ResourceModel\Lookbook\CollectionFactory $lookbookCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $productContext, $_helper, $_productCollectionFactory, $urlHelper,$coreRegistry, $data);
        $this->lookbookCollectionFactory = $lookbookCollectionFactory;

    }

    public function getLookbooks(){
        //get values of current page
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 6;

        $lookbooks = $this->lookbookCollectionFactory->create()
            ->addFieldToFilter('status', 1)
            ->setOrder('position','ASC');
        $lookbooks->setPageSize($pageSize);
        $lookbooks->setCurPage($page);

        return $lookbooks;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        //$this->pageConfig->getTitle()->set(__('News'));
        if ($this->getLookbooks()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'lookbook.pager'
            )->setAvailableLimit(array(6=>6,12=>12,24=>24))->setShowPerPage(true)->setCollection(
                $this->getLookbooks()
            );
            $this->setChild('pager', $pager);
            $this->getLookbooks()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
