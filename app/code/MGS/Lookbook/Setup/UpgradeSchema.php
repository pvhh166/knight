<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */

namespace MGS\Lookbook\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Blog schema update
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $setup->startSetup();

        $version = $context->getVersion();
        $connection = $setup->getConnection();


        if (version_compare($version, '1.1.0') < 0) {
            /* Add custom field to lookbook table */
            $connection->addColumn(
                $setup->getTable('mgs_lookbook'),
                'content',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '2M',
                    [],
                    'comment' =>'Description Content'
                ]
            );

            $connection->addColumn(
                $setup->getTable('mgs_lookbook'),
                'tags',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '500',
                    [],
                    'comment' =>'Tags Content'
                ]
            );
            $connection->addColumn(
                $setup->getTable('mgs_lookbook'),
                'identifier',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '255',
                    [],
                    'comment' =>'Identifiers Content'
                ]
            );
        }

        if (version_compare($version, '1.1.1') < 0) {
            /* Add custom field to lookbook table */
            $connection->addColumn(
                $setup->getTable('mgs_lookbook'),
                'position',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => '11',
                    [],
                    'comment' =>'Sort'
                ]
            );
        }
        $setup->endSetup();
    }
}
