<?php
namespace Magecomp\Savecartpro\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Customer\Model\Session;

class Custcartlist extends Template
{
	protected $modelsavecart;
	protected $customerSession;
	protected $modelsavecartdetail;
	
	public function __construct(Context $context,
		SavecartproFactory $modelsavecart,
		Session $customerSession, 
		SavecartprodetailFactory $modelsavecartdetail,
        array $data = [])
    {
		$this->modelsavecart = $modelsavecart;
		$this->customerSession = $customerSession;
		$this->modelsavecartdetail = $modelsavecartdetail;
        parent::__construct($context, $data);
    }
	
	public function CustomerCartList()
	{
		try
		{
			$cust_id = $this->customerSession->getCustomer()->getId();	
			$quoteCollection = $this->modelsavecart->create()
							 ->getCollection()
							 ->addFieldToFilter('customer_id',$cust_id)
							 ->addFieldToFilter('cart_name',['neq'=>'Auto Save']);
			return $quoteCollection;
			
		}
		catch(\Exception $e)
		{
			$om = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $om->get('Psr\Log\LoggerInterface');
			$storeManager->info($e->getMessage());
		}	
	}
	public function getTotalPrice($savecart_id )
	{
		if($savecart_id != '' && $savecart_id > 0)
		{
			$quotedetailCollection = $this->modelsavecartdetail->create()
							 ->getCollection()
							 ->addFieldToFilter('savecart_id',$savecart_id);

			$filtercollection = array();
			$products = array();
			$total = 0;
			foreach($quotedetailCollection as $quotedetail)
			{
				$total += $quotedetail->getQuotePrdPrice() * $quotedetail->getQuotePrdQty();
			}


			return $total;
		}
		return 0;
	}

    public function hasSuperProduct($savecart_id)
    {
        if($savecart_id != '' && $savecart_id > 0)
        {
            $quotedetailCollection = $this->modelsavecartdetail->create()
                ->getCollection()
                ->addFieldToFilter('savecart_id',$savecart_id);

            foreach($quotedetailCollection as $quotedetail)
            {
                $om = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $om->create('Magento\Catalog\Model\Product')->load($quotedetail->getQuotePrdId());
                if($product->getTypeId() == 'super'){
                    return true;
                }
            }

        }
        return false;
    }

	public function getUnitPriceFormat($price)
	{
		$om = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
    	$priceHelper = $om->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
    	return $priceHelper->currency($price, true, false);
	}

}