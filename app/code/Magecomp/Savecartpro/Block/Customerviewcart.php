<?php
namespace Magecomp\Savecartpro\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;

class Customerviewcart extends Template
{
    protected $_request;
    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $product;
    protected $_productRepository;
    protected $custsession;

    public function __construct(Context $context,
        SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Customer\Model\Session $custsession,
        array $data = []

        )
    {
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->_productRepository = $ProductRepository;
        $this->custsession = $custsession;
        parent::__construct($context, $data);
    }

    public function getMainCartData()
    {
        $data = $this->_request->getParams();
        $quoteid = $data['qid'];
        if($quoteid != '' && $quoteid > 0)
        {
            return $this->modelsavecart->create()->load($quoteid);
        }
    }

    public function getDetailCartData()
    {
        $data = $this->_request->getParams();
        $quoteid = $data['qid'];
        if($quoteid != '' && $quoteid > 0)
        {
            $quotedetailCollection = $this->modelsavecartdetail->create()
                             ->getCollection()
                             ->addFieldToFilter('savecart_id',$quoteid);

            $filtercollection = array();
            $products = array();
            foreach($quotedetailCollection as $quotedetail)
            {
                if(!in_array($quotedetail->getQuotePrdId(), $products))
                {
                    $filtercollection[] = $quotedetail;
                    $products[] =  $quotedetail->getQuotePrdId();
                }
            }


            return $quotedetailCollection;
        }
    }

    public function getProductName($productid)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($productid);
        return $product->getName();
    }
    public function getProductUrl($cartdata)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($cartdata->getQuotePrdId());
        $url = $product->getProductUrl();
        if($product->getTypeId() == 'configurable') {
           $savecartdetail_id = $cartdata->getSavecartDetailId();
            $url .= '?savecart_id='.$savecartdetail_id."&qty=".$cartdata->getQuotePrdQty();
           $options = $cartdata->getQuoteConfigPrdData();
            $itemOptions = unserialize($options);
            if (array_key_exists('super_attribute', $itemOptions)) {
                $super_att = $itemOptions['super_attribute'];
                if(is_array($super_att)) {
                    foreach ($super_att as $key=>$val) {
                        $url .= '&'.$key."=".$val;
                    }
                }
            }

        }else {
            if($product->getTypeId() == 'super') {
                $url .= '?savecart_id='.$cartdata->getSavecartDetailId();
            }else {
                $url .= '?savecart_id='.$cartdata->getSavecartDetailId()."&qty=".$cartdata->getQuotePrdQty();
            }
        }

        return $url;
    }
    public function isSupperProduct($productid)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($productid);
        if($product->getTypeId() == 'super'){
            return true;
        }else{
            return false;
        }
    }

    public function getAddonHtmlQty($options)
    {
        $itemOptions = unserialize($options);
        $html_price = '';
        if (array_key_exists('super_options', $itemOptions)) {
            $super_att = $itemOptions['super_options']['super_attributes'];
            if (is_array($super_att)) {
                $html_price .= '<div class="qty-wrapper">';
                foreach ($super_att as $addon =>$add_qty) {
                    $html_price .= '<div class="field qty">';
                    $html_price .= '<span>'.$add_qty['product_quantity'].'</span>';
                    $html_price .= '</div>';
                }
                $html_price .= '</div>';
            }
        }
        return $html_price;
    }

	public function getProductImage($productid)
	{
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($productid);
       
        $_imageHelper = $om->get('Magento\Catalog\Helper\Image');
        $imageURL = $_imageHelper->init($product, 'image', ['type'=>'image'])->keepAspectRatio(true)->resize(900,900)->getUrl();
        return $imageURL;
    }
    public function getProductImageCustom($productid, $alloption)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($productid);

        $_imageHelper = $om->get('Magento\Catalog\Helper\Image');
        $imageURL = $_imageHelper->init($product, 'image', ['type'=>'image'])->keepAspectRatio(true)->resize(900,900)->getUrl();
        $itemOptions = unserialize($alloption);
        if($product->getTypeId() == 'configurable')
        {
            if (array_key_exists('product_simple_sku', $itemOptions)) {
                $productRepository = $om->get('\Magento\Catalog\Model\ProductRepository');
                $productObj = $productRepository->get($itemOptions['product_simple_sku']);
                if($productObj) {
                    $width = 250;
                    $height = 250;
                    $imageURL = $_imageHelper->init($productObj, 'product_page_image_small')
                        ->constrainOnly(FALSE)
                        ->keepAspectRatio(TRUE)
                        ->keepFrame(FALSE)
                        ->resize($width, $height)
                        ->getUrl();
                }
            }
        }
        if($product->getTypeId() == 'super')
        {
            if (array_key_exists('super_options', $itemOptions)) {
                $superOptions = $itemOptions['super_options'];
                $imageURL = $superOptions['super_product_img'];
            }
        }

        return $imageURL;
    }

    public function getAllOptions($productid,$alloption)
    {
        $html = '';
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $om->create('Magento\Catalog\Model\Product')->load($productid);
        $storeManager = $om->get('Psr\Log\LoggerInterface');
        $itemOptions = unserialize($alloption);

        if (array_key_exists('options', $itemOptions)) {
            $c_opt = $itemOptions['options'];
            $customOptions = $om->create('Magento\Catalog\Model\Product\Option')->getProductOptionCollection($product);
            foreach ($customOptions as $co) {
                foreach ($c_opt as $ckey => $cvalue) {
                    if ($ckey == $co['option_id']) {
                        $html .= "<span>" . $co['title'] . "</span> :";
                    }
                    foreach ($co->getValues() as $value) {
                        if ($value['option_type_id'] == $cvalue) {
                            $html .= $value['title'] . "<br>";
                        }
                    }
                }
            }
        }
        if (array_key_exists('super_attribute', $itemOptions)) {
            $super_att = $itemOptions['super_attribute'];
            foreach ($super_att as $key => $value) {
                $attributeId = $key;
                $eavModel = $om->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');
                $eavModel->load($attributeId);
                $attLabel = $eavModel->getFrontendLabel();
                $attributeCode = $eavModel->getAttributeCode();

                $optionId = $value;
                $attribute = $product->getResource()->getAttribute($attributeCode);
                if ($attribute->usesSource()) {
                    $optionText = $attribute->getSource()->getOptionText($optionId);
                }
                $html .= "<span>" . $attLabel . '</span> : ' . $optionText . '</br>';
            }
        }
        if (array_key_exists('bundle_option', $itemOptions)) {
            $super_att = $itemOptions['bundle_option'];
            foreach ($super_att as $key => $value) {

                $selectionCollection = $product->getTypeInstance(true)
                    ->getSelectionsCollection(
                        $product->getTypeInstance(true)->getOptionsIds($product),
                        $product
                    );

                $optionsCollection = $product->getTypeInstance(true)
                    ->getOptionsCollection($product);
                foreach ($selectionCollection as $selctopt) {
                    $storeManager->info(print_r($selectionCollection->getData(), true));
                    if ($selctopt['option_id'] == $key and $selctopt['selection_id'] == $value) {
                        foreach ($optionsCollection as $options) {
                            if ($options['option_id'] == $key) {
                                $product = $om->create('Magento\Catalog\Model\Product')->load($selctopt['entity_id']);
                                $html .= "<span>" . $options['title'] . '</span> : ' . $product->getName() . '</br>';
                            }
                        }
                    }
                }
            }
        }
        if (array_key_exists('super_options', $itemOptions)) {
            $attributesOptions = $itemOptions['super_options'];
            $attributes = $attributesOptions['super_attributes_defined'];
            foreach($attributes as $item) {
                $html .= "<div class='product-child clearfix'><div class='b-left'><span class='c-name'>" . $item['product_name'] . "</span></br>";
                $html .= "<span class='c-sku'>" . $item['product_simple_sku'] . "</span></br>";
                if(is_array($item['option_selected'])) {
                    $html .= "<div class='option-child'>";
                    foreach($item['option_selected'] as $key=>$value) {
                       $html .= "<span>" . $key . '</span> : ' . $value . '</br>';
                    }
                    $html .= "</div>";
                }
                $html .= '<span class="sm-block">Component Qty: ' . $item['product_quantity'] . '</span>';
                $html .="</div>";
                $html .= "<div class='product-child-image'><img src='". $item['product_thumbnail_img']."' /></div></div>";
            }
        }


        return $html;
    }
    public function getUnitPriceFormat($price)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $priceHelper = $om->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
        return $priceHelper->currency($price, true, false);
    }

    public function getTotalSpPrice($_param)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $_super_product_id = $_param['super_product_id'];
        $_super_product_qty = $_param['product_qty'];
        $_super_product_attributes = $_param['super_attributes'];
        $_super_product_addon_qty = '';
        if(!empty($_param['addon_qty'])) {
            $_super_product_addon_qty = $_param['addon_qty'];
        }

        $_super_product = $this->_productRepository->getById($_super_product_id);
        $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);

        $_totalCustomPrice = 0;
        foreach ($_getChildProduct as $_item) {
            $_addon_qty = null;
            $_item_id = $_item['simple_product_id'];
            $_default_product_id = $_item['default_product_id'];

            // Count Total Qty
            $_default_qty = $_item['default_qty'];
            if(!empty($_super_product_addon_qty)) {
                if (array_key_exists($_default_product_id, $_super_product_addon_qty)) {
                    $_addon_qty = $_super_product_addon_qty[$_default_product_id];
                }
            }

            if($_addon_qty != null) {
                $_total_qty = $_addon_qty;
            } else {
                $_total_qty = $_default_qty;
            }
            $_product = $om->create('\Magento\Catalog\Model\Product')->load($_item_id);
            $_product_final_price = min($_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),$_product->getPriceModel()->getFinalPrice($_super_product_qty*$_total_qty,$_product));
            $_totalCustomPrice += $_product_final_price*$_total_qty;
        }
        return $_totalCustomPrice;
    }

    public function getTotalItemsPrice($productid , $productqty , $subtotal = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productid);
        //$price = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue() * $productqty;
        $price = $product->getPriceModel()->getFinalPrice(1,$product);
        if($subtotal){
           $price =  $productqty * $price;
        }
        return $price;
    }

    public function isCustomerLogged()
    {
        $isCustomerLogged = false;
        if($this->custsession->isLoggedIn()){
            $isCustomerLogged = true;
        }
        return $isCustomerLogged;
    }

}
