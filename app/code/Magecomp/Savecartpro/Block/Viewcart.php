<?php
namespace Magecomp\Savecartpro\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Checkout\Model\Session;

class Viewcart extends Template
{
	protected $cartsession;
	public function __construct(Context $context,
		Session $session,  
        array $data = [])
    {
		$this->cartsession = $session;
        parent::__construct($context, $data);
    }
	
	public function getCartAllItems()
	{
		return $this->cartsession->getQuote()->getAllVisibleItems();
	}
}