<?php
namespace Magecomp\Savecartpro\Controller\Index;

use Magento\Checkout\Model\Session;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Checkout\Model\Cart;
use Magento\Quote\Model\QuoteFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $messageManager;
    protected $_responseFactory;
    protected $_url;
    protected $_request;
    protected $cartsession;
    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $_modelCart;
    protected $serialize;
    protected $quoteFactory;
    protected $product;
    protected $_customersession;
    protected $resultFactory;
    protected $jsonFactory;
    protected $_objectManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Customer\Model\Session $customer_session,
        Session $session,
        SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        Cart $modelCart,
        QuoteFactory $quoteFactory,
        ResultFactory $resultFactory,
        ProductFactory $product,
        JsonFactory $jsonFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager
    )
    {
        $this->_responseFactory = $responseFactory;
        $this->cartsession = $session;
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->_modelCart = $modelCart;
        $this->quoteFactory = $quoteFactory;
        $this->product = $product;
        $this->_customersession = $customer_session;
        $this->resultFactory = $resultFactory;
        $this->jsonFactory = $jsonFactory;
        $this->_objectManager = $objectmanager;
        parent::__construct($context);
    }

    public function execute()
    {
        $serialize = $this->_objectManager->create('\Magento\Framework\Serialize\Serializer\Json');
        try {
            $storeManager = $this->_objectManager->get('Psr\Log\LoggerInterface');
            $storeManager->info('Magecomp Log');
            $data = $this->_request->getParams();
            $quote = $this->cartsession->getQuote();
            $customer_id = $quote->getCustomerId();

            $cartname = $data['cartname'];
            if($cartname!="") {

                if ($customer_id != null) {
                    // Quote Save To Main Tabel
                    $savecartmodel = $this->modelsavecart->create()
                        ->setCartName($cartname)
                        ->setCustomerId($customer_id)
                        ->setCreatedAt(date('Y-m-d H:i:s'))
                        ->save();

                    // Quote Data Save To Detail Tabel
                    $visitems = $this->cartsession->getQuote()->getAllVisibleItems();

                    //$serial = $om->get('\Magento\Framework\Serialize\Serializer\Json');

                    foreach ($visitems as $item) {
                        foreach ($item->getOptions() as $option) {
                            $itemOptions = $serialize->unserialize($option['value']);
                            if(is_array($itemOptions)) {
                                $itemOptions['product_simple_sku'] = $item->getProduct()->getSku();
                                if($item->getProduct()->getTypeId() == 'super') {
                                    $addOptions = $item->getOptionByCode('super_custom_option');
                                    if ($addOptions) {
                                        $options = $serialize->unserialize($addOptions->getValue());
                                        $itemOptions['super_options'] = $options;
                                    }
                                }
                            }
                            $savecartdetailmodel = $this->modelsavecartdetail->create()
                                ->setSavecartId($savecartmodel->getId())
                                ->setQuotePrdId($item->getProductId())
                                ->setQuotePrdQty($item->getQty())
                                ->setQuotePrdPrice($item->getPrice())
                                ->setQuoteConfigPrdData(serialize($itemOptions))
                                ->save();
                            break;
                            //itemOptions contain all the custom option of an item
                        }
                    }
                    $this->_modelCart->truncate();// Clear Shopping Cart
                    $this->cartsession->clearQuote();// Clear Checkout Session
                    // Clear Shopping Cart
                    $visitems = $this->cartsession->getQuote()->getAllVisibleItems();
                    foreach ($visitems as $item) {
                        $itemId = $item->getItemId();
                        $this->_modelCart->removeItem($itemId)->save();
                    }

                    $message = "Your cart has been successfully saved.";
                    $this->messageManager->addSuccess($message);
                    $response = array();
                    $resultJson = $this->jsonFactory->create();
                    $response['redirectUrl'] = $this->_url->getUrl('savecartpro/customer/cartlist');
                    $resultJson->setData($response);
                    return $resultJson;

                } else {
                    $message = "Before saving cart you must have login";
                    $this->messageManager->addError($message);
                    $accUrl = $this->_url->getUrl('customer/account/login');
                    $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                    $this->setRefererUrl($accUrl);
                }
            }else{
                $message = "Cart name not blank";
                $this->messageManager->addError($message);
                $response = array();
                $resultJson = $this->jsonFactory->create();
                $response['redirectUrl'] = $this->_url->getUrl('checkout/cart/index');
            }
        }catch(Exception $e){
            $storeManager = $this->_objectManager->get('Psr\Log\LoggerInterface');
            $storeManager->info('Magecomp Log'.$e->getMessage());
			$this->messageManager->addError($e->getMessage());
			$accUrl = $this->_url->getUrl('savecart/index/view');
       		$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
			$this->setRefererUrl($accUrl);
		}
}
}
