<?php

namespace Magecomp\Savecartpro\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Setup\Exception;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\QuoteFactory;
use Magento\Framework\Controller\ResultFactory;

class Savenewquote extends \Magento\Framework\App\Action\Action
{

    protected $request;
    protected $formKey;
    protected $cart;
    protected $savecartdetailFactory;
    protected $redirect;
    protected $resultRedirect;
    protected $checkoutSession;
    protected $quoteFactory;
    protected $custaddcart;

    public function __construct(Context $context,
                                Http $request,
                                FormKey $formKey,
                                Cart $cart,
                                SavecartprodetailFactory $savecartdetailFactory,
                                \Magento\Framework\App\Response\RedirectInterface $redirect,
                                Session $checkoutSession,
                                \Magecomp\Savecartpro\Controller\Customer\Custaddcart $custaddcart,
                                QuoteFactory $quoteFactory,
                                ResultFactory $resultFactory
                                )
    {
        $this->request = $request;
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->savecartdetailFactory = $savecartdetailFactory;
        $this->redirect = $redirect;
        $this->checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;
        $this->resultRedirect = $resultFactory;
        $this->custaddcart = $custaddcart;

        parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialize = $objectManager->create('\Magento\Framework\Serialize\Serializer\Serialize');
        try {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $param = $this->getRequest()->getParams();
            $id = substr($param['qid'], 3,-3);
            $quotedetailCollection = $this->savecartdetailFactory->create()->getCollection()->addFieldToFilter('savecart_id', $id);

            $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
            foreach ($allItems as $item) {
                $itemId = $item->getItemId();
                $this->cart->removeItem($itemId);
            }
            if (count($quotedetailCollection->getData()) > 0) {
                foreach ($quotedetailCollection->getData() as $col) {
                    $configdata = $serialize->unserialize($col['quote_config_prd_data']);
                    $obj_product = $om->create('Magento\Catalog\Model\Product');
                    array_push($configdata, 'form_key', $this->formKey->getFormKey());
                    $_product = $obj_product->load($col['quote_prd_id']);
                    if($_product->getTypeId() == 'super') {
                        $this->custaddcart->addSuperProductToCart($this->cart, $configdata['super_options']);
                    }else{
                        $this->cart->addProduct($_product, $configdata);
                    }
                }

                $this->cart->save();
                $this->messageManager->addSuccess(__('Successfull Message'));
            }
            else
            {
                $this->messageManager->addError(__('There no products in your save cart'));
            }

            $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);

        } catch (Exception $exception) {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $om->get('Psr\Log\LoggerInterface');
            $storeManager->info('Add to Cart Error' . $exception->getMessage());
        }
        $resultRedirect->setPath('checkout/cart/');
        return $resultRedirect;

    }
}
