<?php
namespace Magecomp\Savecartpro\Controller\Index;


use Magento\Framework\App\Action\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magecomp\Savecartpro\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
class Sendmail extends \Magento\Framework\App\Action\Action
{
    protected $transportBuilder;
    protected $scopeConfig;
    protected $saveHelper;
    public function __construct(Context $context,
                                TransportBuilder $transportBuilder,
                                Data $mchelper,
                                ScopeConfigInterface $scopeConfig)
    {
        $this->transportBuilder = $transportBuilder;
        $this->saveHelper=$mchelper;
        $this->scopeConfig=$scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $param = $this->_request->getParams();

        try{
        $report = [
            'fromName' => $param['fromName'],
            'mailContent' => $param['mailContent'],
            'subject'=>$param['mailSubject'],
        ];

        $sender=$this->scopeConfig->getValue('savecartpro/general/emailsender',ScopeInterface::SCOPE_STORE);
        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($report);
        $transport = $this->transportBuilder
            ->setTemplateIdentifier('savecartpro_general_templateemail')
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
            ->setTemplateVars(['data' => $postObject])
            ->setFrom($sender)
            ->addTo([$param['mailTo']])
            ->getTransport();
            $message = "Your cart email has been sent successfully.";
            $this->messageManager->addSuccess($message);
            $transport->sendMessage();
            $response = "success";
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($response);
            return $resultJson;

        } catch (\Exception $e) {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $om->get('Psr\Log\LoggerInterface');
            $storeManager->info('Savecartpro Mail Error :'.$e->getMessage());
            $response = "Error";
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($response);
        }
    }
}