<?php
namespace Magecomp\Savecartpro\Controller\Index;

use Magento\Checkout\Model\Session;
use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Checkout\Model\Cart;
use Magento\Quote\Model\QuoteFactory;
use Magento\Catalog\Model\Product;
use Magento\Setup\Exception;
use Magento\Framework\Controller\Result\JsonFactory;

class Updatesavedcart extends \Magento\Framework\App\Action\Action
{
    protected $messageManager;
    protected $_responseFactory;
    protected $_url;
    protected $_request;
    protected $cartsession;
    protected $modelsavecart;
    protected $modelsavecartdetail;
    protected $_modelCart;
    protected $quoteFactory;
    protected $product;
    protected $jsonFactory;
    protected $_objectManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        Session $session,
        SavecartproFactory $modelsavecart,
        SavecartprodetailFactory $modelsavecartdetail,
        Cart $modelCart,
        QuoteFactory $quoteFactory,
        Product $product,
        JsonFactory $jsonFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager
    )
    {
        $this->_responseFactory = $responseFactory;
        $this->cartsession = $session;
        $this->modelsavecart = $modelsavecart;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->_modelCart = $modelCart;
        $this->quoteFactory = $quoteFactory;
        $this->product = $product;
        $this->jsonFactory = $jsonFactory;
        $this->_objectManager = $objectmanager;
        parent::__construct($context);
    }

    public function execute()
    {
        $serialize = $this->_objectManager->create('\Magento\Framework\Serialize\Serializer\Json');
        $serialize2 = $this->_objectManager->create('\Magento\Framework\Serialize\Serializer\Serialize');
        $resultRedirect = $this->resultRedirectFactory->create();
        try
        {
            $data = $this->_request->getParams();
            $quote = $this->_modelCart->getQuote();
            $cartid=$data['savecartid'];
            $quotedetailCollection = $this->modelsavecartdetail->create()
                ->getCollection()
                ->addFieldToFilter('savecart_id',$cartid);
            // Quote Data Save To Detail Tabel
            $quoteid=$quote->getId();
            $quote = $this->quoteFactory->create()->load($quoteid);
            $visitems = $quote->getAllVisibleItems();

            foreach ($visitems as $item)
            {
                $flag=false;
                if(isset($item)) {
                    foreach ($item->getOptions() as $option) {
                        try {
                            if ($option['value']) {
                                $itemOptions = $serialize->unserialize($option['value']);
                                break;
                            }
                        } catch (Exception $e) {
                            $this->messageManager->addError($e->getMessage());
                            $accUrl = $this->_url->getUrl('savecartpro/index/view');
                            //$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                            //$this->setRefererUrl($accUrl);
                            $resultRedirect->setUrl($accUrl);
                            return $resultRedirect;
                        }
                    }
                }
                foreach ($quotedetailCollection as $cartitem) {
                    $objItem = $cartitem;
                    $currentQty = $cartitem->getQuotePrdQty();
                    $Configdata = $serialize2->unserialize($cartitem->getQuoteConfigPrdData());
                    
                    if ($cartitem->getQuotePrdId() == $item->getProductId()) {
                        $product = $this->product->load($cartitem->getQuotePrdId());
                        if($product->getTypeId()=="simple"){
                            if(isset($itemOptions['options'])) {
                                $customopt1 = $itemOptions['options'];
                                $customopt2 = $Configdata['options'];
                            }
                        }
                        if($product->getTypeId()=="configurable"){
                            if(isset($itemOptions['super_attribute'])) {
                                $customopt1 = $itemOptions['super_attribute'];
                                $customopt2 = $Configdata['super_attribute'];
                            }
                        }
                        if($product->getTypeId()=="bundle"){
                            if(isset($itemOptions['bundle_option'])) {
                                $customopt1 = $itemOptions['bundle_option'];
                                $customopt2 = $Configdata['bundle_option'];
                            }
                        }
                        if(isset($customopt1) and isset($customopt2)) {
                            if(is_array($customopt1) and is_array($customopt2))
                            {
                                $array_cmp = array_diff_assoc($customopt1, $customopt2);
                                if (count($array_cmp) == 0) {
                                    $flag = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if($cartitem->getQuotePrdId() == $item->getProductId()){
                                $flag=true;
                                break;
                            }

                        }
                    }
                }

                if ($flag) {
                    $finaQty=$item->getQty() + $currentQty;
                    $Configdata['qty']=$finaQty;
                    $objItem->setQuotePrdQty($currentQty)
                        ->setQuoteConfigPrdData(serialize($Configdata))
                        ->save();
                }else{
                    foreach ($item->getOptions() as $option) {
                        $itemOptions = $serialize->unserialize($option['value']);
                        if($item->getProduct()->getTypeId() == 'super') {
                            $addOptions = $item->getOptionByCode('super_custom_option');
                            if ($addOptions) {
                                $options = $serialize->unserialize($addOptions->getValue());
                                $itemOptions['super_options'] = $options;
                            }
                        }
                        $savecartdetailmodel = $this->modelsavecartdetail->create()
                            ->setSavecartId($cartid)
                            ->setQuotePrdId($item->getProductId())
                            ->setQuotePrdQty($item->getQty())
                            ->setQuotePrdPrice($item->getPrice())
                            ->setQuoteConfigPrdData(serialize($itemOptions))
                            ->save();
                        break;
                        //itemOptions contain all the custom option of an item
                    }
                }
            }
            
            $this->_modelCart->truncate();// Clear Shopping Cart
            $this->cartsession->clearQuote();// Clear Checkout Session
            // Clear Shopping Cart
            $visitems = $this->cartsession->getQuote()->getAllVisibleItems();
            foreach($visitems as $item)
            {
                $itemId = $item->getItemId();
                //$this->_modelCart->removeItem($itemId)->save();
            }
            $message = "Your cart has been successfully saved.";
            $this->messageManager->addSuccess($message);
            $response = array();
            $resultJson = $this->jsonFactory->create();
            $response['redirectUrl'] = $this->_url->getUrl('savecartpro/customer/cartlist');
            $resultJson->setData($response);
            return $resultJson;

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $accUrl = $this->_url->getUrl('savecartpro/index/view');
            // $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
            //$this->setRefererUrl($accUrl);
            $resultRedirect->setUrl($accUrl);
            return $resultRedirect;
        }
    }
}
