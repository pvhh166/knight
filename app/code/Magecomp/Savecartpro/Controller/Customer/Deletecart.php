<?php
namespace Magecomp\Savecartpro\Controller\Customer;

use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;

class Deletecart extends \Magento\Framework\App\Action\Action
{
	protected $messageManager;
	protected $custsession;
	protected $_responseFactory;
	protected $_url;
	protected $_request;
	protected $modelsavecart;
	protected $modelsavecartdetail;
	

	public function __construct(\Magento\Framework\App\Action\Context $context, 
	\Magento\Customer\Model\Session $custsession,
	\Magento\Framework\App\ResponseFactory $responseFactory,
	SavecartproFactory $modelsavecart,
	SavecartprodetailFactory $modelsavecartdetail)
    {
		$this->custsession = $custsession;
		$this->_responseFactory = $responseFactory;
		$this->modelsavecart = $modelsavecart;
		$this->modelsavecartdetail = $modelsavecartdetail;
		parent::__construct($context);
    }

	public function execute()
	{
		if($this->custsession->isLoggedIn())
		{
			try
			{
				$data = $this->_request->getParams();
				
				$quoteid = $data['qid'];
				
				// Delete Data From Subtable
				$quotedetailCollection = $this->modelsavecartdetail->create()
							 		   ->getCollection()
							 		   ->addFieldToFilter('savecart_id',$quoteid);
				
				foreach($quotedetailCollection as $quotedetail)
				{
					$mymodel = $this->modelsavecartdetail->create();
					$mymodel->setId($quotedetail->getSavecartDetailId())->delete();

				}
				
				// Delete Data From Main
				$quoteCollection = $this->modelsavecart->create()
							 ->getCollection()
							 ->addFieldToFilter('savecart_id',$quoteid);
				
				foreach($quoteCollection as $mainquote)
				{
					$mymodel = $this->modelsavecart->create();
					$mymodel->setId($mainquote->getSavecartId())->delete();
					$mymodel->save();
				}
				
				
				$message = "Your cart has been successfully deleted";
       			$this->messageManager->addSuccess($message);
				$accUrl = $this->_url->getUrl('savecartpro/customer/cartlist');
       			$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
				$this->setRefererUrl($accUrl);
			
			}
			catch(\Exception $e)
			{
				$this->messageManager->addError($e->getMessage());
				$accUrl = $this->_url->getUrl('savecartpro/customer/cartlist');
				$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
				$this->setRefererUrl($accUrl);
			}
		}
		else
		{
			$accUrl = $this->_url->getUrl('customer/account/login');
       		$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
			$this->setRefererUrl($accUrl);
		}
	}
}