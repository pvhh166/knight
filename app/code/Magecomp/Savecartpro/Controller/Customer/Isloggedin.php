<?php
namespace Magecomp\Savecartpro\Controller\Customer;

use Magento\Framework\Controller\Result\JsonFactory;

class Isloggedin extends \Magento\Framework\App\Action\Action
{
    protected $custsession;
    protected $_responseFactory;
    protected $_url;
    protected $jsonFactory;
    protected $messageManager;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Customer\Model\Session $custsession,
                                \Magento\Framework\App\ResponseFactory $responseFactory,
                                    JsonFactory $jsonFactory
                                )
    {
        $this->custsession = $custsession;
        $this->_responseFactory = $responseFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $urlInterface = $objectManager->get('\Magento\Framework\UrlInterface');
        $response = [
            'redirectUrl' => ""
        ];
        $resultJson = $this->jsonFactory->create();
        if(!$customerSession->isLoggedIn()) {
            $response['redirectUrl'] = $this->_url->getUrl('customer/account/login');
            $resultJson->setData($response);
            return $resultJson;
        }
        else{
            $resultJson->setData($response);
            return $resultJson;
        }
    }
}