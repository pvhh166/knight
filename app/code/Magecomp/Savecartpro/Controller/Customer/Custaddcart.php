<?php

namespace Magecomp\Savecartpro\Controller\Customer;

use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Orange35\ImageConstructor\Model\Order\Item\Exception;
use Magento\Checkout\Model\Cart as CustomerCart;

class Custaddcart extends \Magento\Framework\App\Action\Action
{
    protected $messageManager;
    protected $custsession;
    protected $_responseFactory;
    protected $_url;
    protected $_request;
    protected $modelsavecartdetail;
    protected $_productRepository;
    protected $_superCart;
    protected $_cart;
    protected $_dateTimes;
    protected $serializer;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Customer\Model\Session $custsession,
                                \Magento\Framework\App\ResponseFactory $responseFactory,
                                \Magento\Catalog\Model\ProductRepository $ProductRepository,
                                \Brandsmith\SuperProduct\Controller\Index\Cart $superCart,
                                CustomerCart $cart,
                                \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
                                \Magento\Framework\Serialize\Serializer\Json $serializer = null,
                                SavecartprodetailFactory $modelsavecartdetail)
    {
        $this->custsession = $custsession;
        $this->_responseFactory = $responseFactory;
        $this->modelsavecartdetail = $modelsavecartdetail;
        $this->_productRepository = $ProductRepository;
        $this->_superCart = $superCart;
        $this->_cart = $cart;
        $this->_dateTimes = $dateTime;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        parent::__construct($context);
    }

    public function AddQuoteToCart($data)
    {
        try {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $formkey = $objectManager->get('Magento\Framework\Data\Form\FormKey');
            //$cart = $objectManager->get('Magento\Checkout\Model\Cart');
            $cart = $this->_cart;
            $quotedetailCollection = $this->modelsavecartdetail->create()
                ->getCollection()
                ->addFieldToFilter('savecart_id', $data['cart_id']);

            foreach ($quotedetailCollection->getData() as $col) {
                $obj_product = $objectManager->create('Magento\Catalog\Model\Product');

                $Configdata = unserialize($col['quote_config_prd_data']);
                $Configdata['qty'] = $col['quote_prd_qty'];
                array_push($Configdata, 'form_key', $formkey->getFormKey());
                $_product = $obj_product->load($col['quote_prd_id']);
                $type = $_product->getTypeId();
                if($type == 'super') {
                    $this->addSuperProductToCart($cart, $Configdata['super_options']);
                }else {
                    $cart->addProduct($_product, $Configdata);
                }

            }
            //die;
            $cart->save();
            return 1;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function addSuperProductToCart($cart, $Configdata)
    {
        $_param = $Configdata;
        try{
            $_super_product_id = $_param['super_product_id'];
            $_super_product_qty = $_param['product_qty'];
            $_super_product_attributes = $_param['super_attributes'];
            $_super_product_addon_qty = '';
            if(!empty($_param['addon_qty'])) {
                $_super_product_addon_qty = $_param['addon_qty'];
            }

            $_super_product = $this->_productRepository->getById($_super_product_id);
            $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);

            // Add Quote for Super Product First
            $quote = $this->_objectManager->get('\Magento\Checkout\Model\Cart');
            $_param_super_product = array(
                'product' => $_super_product_id,
                'qty' => $_super_product_qty
            );

            $_super_custom_price = $this->_superCart->getTotalPrice($_getChildProduct, $_super_product_addon_qty);
            $_super_product->setPrice($_super_custom_price);
            $cart->addProduct($_super_product, $_param_super_product);

            // Add Quote for Child Super Product
            foreach ($_getChildProduct as $_item) {
                $_addon_qty = null;
                $_item_id = $_item['simple_product_id'];
                $_default_product_id = $_item['default_product_id'];

                // Count Total Qty
                $_default_qty = $_item['default_qty'];
                if(!empty($_super_product_addon_qty)) {
                    if (array_key_exists($_default_product_id, $_super_product_addon_qty)) {
                        $_addon_qty = $_super_product_addon_qty[$_default_product_id];
                    }
                }

                if($_addon_qty != null) {
                    // This Quantity defined by customer
                    $_total_qty = $_addon_qty;
                } else {
                    // This Quantity defined by admin
                    $_total_qty = $_default_qty;
                }

                $_product = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($_item_id);
                $_params = array(
                    'product' => $_item_id,
                    'qty' => $_total_qty
                );

                $_product->setPrice(0);
                $cart->addProduct($_product, $_params);
            }

            $cart->save();

            //  Update Quote Item - add Item Option
            //  Add Product Name - Quantity to additional_options
            foreach($_param as $key => $data) {
                if($key == 'super_attributes') {
                    foreach ($_param[$key] as $_i => $_value) {

                        $_productAddOn = $_super_product->getTypeInstance()->getProductAddOn($_super_product_id, $_value['product_id']);
                        $_productSku = null;
                        if( !empty($_productAddOn->getData()) ) {
                            //  Get Add-on Information
                            $_addon_data = $_productAddOn->getData();
                            $_productName = $_addon_data['name'];
                            $_productQuantity = $_addon_data['qty'];
                            $_productSku = $_addon_data['sku'];
                        } else {
                            //  Get Base Information
                            $_baseProduct = $_super_product->getTypeInstance()->getBaseProductById($_super_product_id, $_value['product_id']);
                            $_productName = $_baseProduct->getName();
                            $_productQuantity = $_baseProduct->getQty();
                            $_productSku = $_baseProduct->getSku();
                        }

                        // Update Sku Simple product
                        $_simpleSkuForParam = $this->_superCart->setSimpleSkuForParam($_value, $_getChildProduct);
                        $_imageForAddOn = $this->_superCart->setImageForAddOn($_value, $_getChildProduct);
                        $_param[$key][$_i]['product_name'] = $_productName;
                        $_param[$key][$_i]['product_sku'] = $_productSku;
                        $_param[$key][$_i]['product_simple_sku'] = $_simpleSkuForParam;
                        $_param[$key][$_i]['product_thumbnail_img'] = $_imageForAddOn;
                        $_param[$key][$_i]['product_quantity'] = $_productQuantity;
                    }
                }
            }

            //$this->_logger->debug("super_attributes: " . json_encode($_param['super_attributes']) );

            // Add New Param Attribute for MiniCart
            $_param['super_attributes_defined'] = $_param['super_attributes'];

            // Update Attribute Option Selected From Code to Label
            foreach ($_param['super_attributes_defined'] as $key => $value) {
                if ( $_param['super_attributes_defined'][$key]['product_type'] == 'configurable' ) {
                    $attributes_defined_option_selected = $_param['super_attributes_defined'][$key]['option_selected'];
                    $_convertAttributes = $_super_product->getTypeInstance()->getConvertAttributes($attributes_defined_option_selected);
                    $_param['super_attributes_defined'][$key]['option_selected'] = $_convertAttributes;
                }
            }

            $_getAllItems = $quote->getQuote()->getAllItems();
            //$_getAllItems = $this->_cart->getItems();

            $parent_item_id = null;
            $_gmtTimestamp = $this->_dateTimes->gmtTimestamp();
            foreach($_getAllItems as $item) {
                // parent_item_id

                if($item->getProductId() == $_super_product_id ) {
                    if(empty($item->getAdditionalData())) {
                        $item->addOption([
                            'product_id' => $_super_product_id,
                            'code'  => 'super_custom_option',
                            'value' => $this->serializer->serialize($_param)
                        ]);

                        $item->addOption([
                            'product_id' => $_super_product_id,
                            'code'  => 'super_custom_request',
                            'value' => $this->serializer->serialize(array('gmtTimestamp' => $_gmtTimestamp))
                        ]);

                        $item->saveItemOptions();
                        //$item->setCustomPrice($_super_custom_price);
                        $item->setOriginalCustomPrice($_super_custom_price);
                        $item->getProduct()->setIsSuperMode(true);
                        $item->setAdditionalData($item->getItemId());
                        $item->save();
                        $parent_item_id = $item->getAdditionalData();
                    }
                }

                if($item->getProductId() != $_super_product_id && empty($item->getParentItemId()) ) {
                    $item->addOption([
                        'product_id' => $item->getProductId(),
                        'code'  => 'child_custom_request',
                        'value' => $this->serializer->serialize(array('gmtTimestamp' => $_gmtTimestamp, 'product_id' => $item->getProductId(), 'super_product' => $_super_product_id))
                    ]);
                    $item->setParentItemId($parent_item_id);
                    // $item->setCustomPrice(0);
                    $item->save();
                }
            }

        }catch (Exception $e) {
            $this->messageManager->addSuccess("Can\'t add product to cart.");
        }
    }


    public function execute()
    {
        if ($this->custsession->isLoggedIn()) {
            try {
                $data = $this->_request->getParams();
                $om = \Magento\Framework\App\ObjectManager::getInstance();
                if ($data['submit_cart_action'] == 'add_cart') // Customer Want To Added Product To Cart
                {
                    $result = $this->AddQuoteToCart($data);
                    if ($result) {
                        $quote = $om->get('Magento\Quote\Model\Quote');
                        $quote->setTotalsCollectedFlag(false)->collectTotals()->save();

                        $checkout = $om->get('Magento\Checkout\Model\Session');
                        $checkout->getQuote()->collectTotals()->save();

                        $message = "Your saved cart has been successfully added to cart";
                        $this->messageManager->addSuccess($message);

                        $accUrl = $this->_url->getUrl('checkout/cart/');
                        $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                        $this->setRefererUrl($accUrl);
                    } else {
                        $this->messageManager->addError($result);
                        $accUrl = $this->_url->getUrl('savecartpro/customer/viewcart') . 'qid/' . $data['cart_id'];
                        $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                        $this->setRefererUrl($accUrl);
                    }
                }

                if ($data['submit_cart_action'] == 'update_cart') // Customer Want To Update Quort Data
                {
                    $om = \Magento\Framework\App\ObjectManager::getInstance();

                    $product = $om->create('Magento\Catalog\Model\Product');

                    foreach ($data['cartitems'] as $key => $val) {

                        $quotedetail = $this->modelsavecartdetail->create()->load($key);
                        $configdata = unserialize($quotedetail->getQuoteConfigPrdData());
                        $configdata['qty'] = $val;
                        $quotedetail->setQuoteConfigPrdData(serialize($configdata));
                        $quotedetail->setQuotePrdQty($val);
                        $quotedetail->save();
                    }

                    $message = "Your cart has been successfully updated.";
                    $this->messageManager->addSuccess($message);
                    $accUrl = $this->_url->getUrl('savecartpro/customer/viewcart') . 'qid/' . $data['cart_id'];
                    $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                    $this->setRefererUrl($accUrl);
                }
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $accUrl = $this->_url->getUrl('savecartpro/customer/viewcart') . 'qid/' . $data['cart_id'];
                $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                $this->setRefererUrl($accUrl);
            }
        } else {
            $accUrl = $this->_url->getUrl('customer/account/login');
            $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
            $this->setRefererUrl($accUrl);
        }
    }
}
