<?php
namespace Magecomp\Savecartpro\Controller\Customer;

class Cartlist extends \Magento\Framework\App\Action\Action
{
	protected $custsession;
	protected $_responseFactory;
	protected $_url;

	public function __construct(\Magento\Framework\App\Action\Context $context,
	\Magento\Customer\Model\Session $custsession,
	\Magento\Framework\App\ResponseFactory $responseFactory)
    {
		$this->custsession = $custsession;
		$this->_responseFactory = $responseFactory;
		parent::__construct($context);
    }

	public function execute()
	{
		if($this->custsession->isLoggedIn())
		{
			$this->_view->loadLayout();
        	$this->_view->renderLayout();
		}
		else
		{
			$accUrl = $this->_url->getUrl('customer/account/login');
       		$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
			$this->setRefererUrl($accUrl);
		}
	}
}
