<?php
namespace Magecomp\Savecartpro\Controller\Customer;

use Magecomp\Savecartpro\Model\SavecartproFactory;
use Magecomp\Savecartpro\Model\SavecartprodetailFactory;

class Deleteviewcart extends \Magento\Framework\App\Action\Action
{
	protected $messageManager;
	protected $custsession;
	protected $_responseFactory;
	protected $_url;
	protected $_request;
	protected $modelsavecartdetail;
	
	public function __construct(\Magento\Framework\App\Action\Context $context, 
	\Magento\Customer\Model\Session $custsession,
	\Magento\Framework\App\ResponseFactory $responseFactory,
	SavecartprodetailFactory $modelsavecartdetail)
    {
		$this->custsession = $custsession;
		$this->_responseFactory = $responseFactory;
		$this->modelsavecartdetail = $modelsavecartdetail;
		parent::__construct($context);
    }

	public function execute()
	{
		if($this->custsession->isLoggedIn())
		{
			try
			{
				$data = $this->_request->getParams();
				
				$quoteid = $data['qid'];
				//Delete Other record, first get data
				$subquoteid = 0;
				$productid = 0;
				$subquotedetail = $this->modelsavecartdetail->create()->load($quoteid);
				$subquoteid = $subquotedetail->getSavecartId();
				$productid = $subquotedetail->getQuotePrdId();
				
				// Delete Data From Subtable
				$quotedetailCollection = $this->modelsavecartdetail->create()
							 		   ->getCollection()
							 		   ->addFieldToFilter('savecart_id',$subquoteid)
									   ->addFieldToFilter('quote_prd_id',$productid)
									   ->addFieldToFilter('savecart_detail_id',array('nin' => $quoteid));
									   
				foreach($quotedetailCollection as $quotedetail)
				{
					$mymodel = $this->modelsavecartdetail->create();
					$mymodel->setId($quotedetail->getSavecartDetailId())->delete();
				}
				
				// Delete Data From Subtable
				$mymodel = $this->modelsavecartdetail->create();
				$mymodel->setId($quoteid)->delete();
				
				$message = "Your cart item has been successfully deleted";
       			$this->messageManager->addSuccess($message);
				$accUrl = $this->_url->getUrl('savecartpro/customer/viewcart').'qid/'.$subquoteid;
       			$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
				$this->setRefererUrl($accUrl);
			}
			catch(\Exception $e)
			{
				$this->messageManager->addError($e->getMessage());
				$accUrl = $this->_url->getUrl('savecartpro/customer/cartlist');
				$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
				$this->setRefererUrl($accUrl);
			}
		}
		else
		{
			$accUrl = $this->_url->getUrl('customer/account/login');
       		$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();   
			$this->setRefererUrl($accUrl);
		}
	}
}