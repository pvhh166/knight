<?php
namespace Magecomp\Savecartpro\Controller\Customer;

use Magecomp\Savecartpro\Model\SavecartprodetailFactory;
use Magento\Checkout\Model\Cart;
class Addcart extends \Magento\Framework\App\Action\Action
{
	protected $messageManager;
	protected $custsession;
	protected $_responseFactory;
	protected $_url;
	protected $_request;
	protected $modelsavecartdetail;
    protected $cart;
    protected $formkey;

	public function __construct(\Magento\Framework\App\Action\Context $context,
	\Magento\Customer\Model\Session $custsession,
	\Magento\Framework\App\ResponseFactory $responseFactory,
	SavecartprodetailFactory $modelsavecartdetail,
                                Cart $cart
                               )
    {
		$this->custsession = $custsession;
		$this->_responseFactory = $responseFactory;
		$this->modelsavecartdetail = $modelsavecartdetail;
		$this->cart = $cart;
		parent::__construct($context);
    }

	public function execute()
	{
		if($this->custsession->isLoggedIn())
		{
			try {

                $om = \Magento\Framework\App\ObjectManager::getInstance();
                $serialize = $om->create('\Magento\Framework\Serialize\Serializer\Serialize');
                $data = $this->_request->getParams();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                //$serial = $objectManager->get('\Magento\Framework\Serialize\Serializer\Json');
                $formkey = $objectManager->get('\Magento\Framework\Data\Form\FormKey');
                if (count($data['selectcart'])>0)
                {
                    foreach ($data['selectcart'] as $quotedata)
                    {
                        $quotedetailCollection = $this->modelsavecartdetail->create()
                            ->getCollection()
                            ->addFieldToFilter('savecart_id', $quotedata);

                        foreach ($quotedetailCollection->getData() as $col) {
                            $obj_product = $objectManager->create('Magento\Catalog\Model\Product');
                            $Configdata = $serialize->unserialize($col['quote_config_prd_data']);
                            array_push($Configdata, 'form_key', $formkey->getFormKey());
                            $_product = $obj_product->load($col['quote_prd_id']);
                            $this->cart->addProduct($_product, $Configdata);
                        }
                        $this->cart->save();
                    }
                    $message = "Your cart has been successfully added to cart";
                    $this->messageManager->addSuccess($message);
                    $accUrl = $this->_url->getUrl('checkout/cart/');
                    $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                    $this->setRefererUrl($accUrl);
                }
                else
                {
                    $message = "Please select any cart";
                    $this->messageManager->addError($message);
                    $accUrl = $this->_url->getUrl('savecartpro/customer/cartlist');
                    $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
                    $this->setRefererUrl($accUrl);
                }
            }
			catch(\Exception $e)
			{
				$this->messageManager->addError($e->getMessage());
				$accUrl = $this->_url->getUrl('savecartpro/customer/cartlist');
				$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
				$this->setRefererUrl($accUrl);
			}
		}
		else
		{
			$accUrl = $this->_url->getUrl('customer/account/login');
       		$this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
			$this->setRefererUrl($accUrl);
		}
	}
}
