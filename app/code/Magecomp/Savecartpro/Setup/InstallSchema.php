<?php
namespace Magecomp\Savecartpro\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		$installer = $setup;
        $installer->startSetup();

		// Savecartpro Main Table
		$table = $installer->getConnection()
            ->newTable($installer->getTable('savecart'))
			->addColumn(
                'savecart_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true],
                'Savecartpro ID'
			)
			->addColumn(
                'cart_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Cart Name'
            )
			->addColumn(
                'customer_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Customer Id'
            )
			->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            );
        $installer->getConnection()->createTable($table);
		
		// Savecartpro Detail Table
		$table = $installer->getConnection()
            ->newTable($installer->getTable('savecart_detail'))
			->addColumn(
                'savecart_detail_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true],
                'Savecartpro Detail ID'
			)
			->addColumn(
                'savecart_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Save Cart Id'
            )
			->addColumn(
                'quote_prd_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Quote Product Id'
            )
			->addColumn(
                'quote_prd_qty',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Quote Product Qty'
            )
			->addColumn(
                'quote_prd_price',
                 \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                 null,
                [],
                'Quote Product Price'
            )
            ->addColumn('quote_config_prd_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1000,
                [],
                'Quote Configurable Product Data'
            );
        $installer->getConnection()->createTable($table);
    }
}