<?php
namespace Magecomp\Savecartpro\Model;

use Magento\Framework\Model\AbstractModel;

class Savecartpro extends AbstractModel
{
    public function _construct() {
         $this->_init('Magecomp\Savecartpro\Model\ResourceModel\Savecartpro');
    }     
}
