<?php
namespace Magecomp\Savecartpro\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Savecartprodetail extends AbstractDb
{
	protected function _construct()
	{
        /* Custom Table Name */
         $this->_init('savecart_detail','savecart_detail_id');
    }     
}
