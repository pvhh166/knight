<?php
namespace Magecomp\Savecartpro\Model\ResourceModel\Savecartprodetail;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected function _construct()
	{
        $this->_init('Magecomp\Savecartpro\Model\Savecartprodetail','Magecomp\Savecartpro\Model\ResourceModel\Savecartprodetail');
    }     
}
