<?php
namespace Magecomp\Savecartpro\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Savecartpro extends AbstractDb
{
	protected function _construct()
	{
        /* Custom Table Name */
         $this->_init('savecart','savecart_id');
    }     
}
