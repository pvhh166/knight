<?php
namespace Magecomp\Savecartpro\Model\ResourceModel\Savecartpro;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected function _construct()
	{
        $this->_init('Magecomp\Savecartpro\Model\Savecartpro', 'Magecomp\Savecartpro\Model\ResourceModel\Savecartpro');
    }     
}
