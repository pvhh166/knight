<?php
namespace Magecomp\Savecartpro\Model;

use Magento\Framework\Model\AbstractModel;

class Savecartprodetail extends AbstractModel
{
    public function _construct() {
         $this->_init('Magecomp\Savecartpro\Model\ResourceModel\Savecartprodetail');
    }     
}
