<?php
namespace Magecomp\Savecartpro\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Model\Session;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
	protected $cartsession;
    public function __construct(Context $context, 
		Session $session)
    {
		$this->cartsession = $session;
        parent::__construct($context);
    }

	public function IsActive()
	{
		return $this->scopeConfig->getValue('savecart/general/enable', ScopeInterface::SCOPE_STORE);	
	}
	
	public function IsCartWithSimpleItems()
	{
		$visitems = $this->cartsession->getQuote()->getAllVisibleItems();
		foreach($visitems as $item)
		{
			if($item->getProductType() != 'simple')
			{
				return 0;
			}
		}
		return 1;
	}

	public function getEmailTemplate()
    {
        return $this->scopeConfig->getValue('savecartpro/general/templateemail', ScopeInterface::SCOPE_STORE);
    }
    public function getEmailSender()
    {
        return $this->scopeConfig->getValue('savecartpro/general/sender_email_identity', ScopeInterface::SCOPE_STORE);
    }
    public function generateRandomString()
    {
        try{
            $randomString  = substr(str_shuffle("0123456789"), 0, 3);
            return $randomString;
        }
        catch(\Exception $e) {
        }
    }
}