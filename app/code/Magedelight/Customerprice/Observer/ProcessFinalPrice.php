<?php

/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@Magedelight.com>
 *
 * @category Magedelight
 * @package Magedelight_Customerprice
 * @copyright Copyright (c) 2017 Mage Delight (http://www.Magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@Magedelight.com>
 */

namespace Magedelight\Customerprice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magedelight\Customerprice\Model\Calculation\Calculator\CalculatorInterface;
use Magento\Catalog\Model\Product\Type\Price as CatalogPrice;

class ProcessFinalPrice implements ObserverInterface
{
    protected $logger;
    
    protected $helper;

    protected $customerPrice;

    protected $priceModel;

    protected $customerSession;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magedelight\Customerprice\Helper\Data $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magedelight\Customerprice\Helper\Data $helper,
        \Magedelight\Customerprice\Model\Customerprice $customerPrice,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Catalog\Model\Product\Type\Price $priceModel,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CalculatorInterface $catalogPriceCalculator
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->customerPrice = $customerPrice;
        $this->customerSession = $customerSession;
        $this->priceModel = $priceModel;
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
        $this->catalogPriceCalculator = $catalogPriceCalculator;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return \Magedelight\Customerprice\Observer\ProcessFinalPrice
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $qty = $observer->getEvent()->getQty();
        $ppcFinalPrice = null;
        if ($this->helper->isCustomerPriceAllow()) {
            // set tier price first
            $this->setNewTierPrice($product);
            $tierFinalPrice = $this->priceModel->getBasePrice($product,$qty);
            $product->setFinalPrice($tierFinalPrice);
            // end set tier price first
            $oldFinalPrice = $product->getData('final_price');
            foreach ($product->getPriceInfo()->getPrices() as $price) {
                if ($price->getPriceCode() == 'ppc_rule_price') {
                    $ppcFinalPrice = $price->getValue();
                }
            }
            if ($ppcFinalPrice) {
                $newFinalPrice = min(
                    $oldFinalPrice,
                    $this->convertCurrentToBase($ppcFinalPrice)
                );
                if ($newFinalPrice !== $oldFinalPrice) {
                    $product->setPpcPrice(1);
                }
                $product->setData('final_price', $newFinalPrice);
            } else {
                if ($this->helper->isAdvanced()) {
                    $discount = $this->catalogPriceCalculator->calculate($oldFinalPrice, $product);
                    if ($discount) {
                        $product->setData('final_price', $discount);
                    }
                }
            }
        }
    }
    
    private function convertCurrentToBase($amount = 0, $store = null, $currency = null)
    {
        if ($store == null) {
            $store = $this->storeManager->getStore()->getStoreId();
        }
        $rate = $this->priceCurrency->convert($amount, $store, $currency);
        $rate = $this->priceCurrency->convert($amount, $store) / $amount;
        $amount = $amount / $rate;
        return $this->priceCurrency->round($amount);
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getCurrentWebsiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * @param $product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function setNewTierPrice($product)
    {
        $res = [];
        $newArray = [];
        $customerId = $this->customerSession->create()->getCustomer()->getId();
        if ($customerId) {
            // Tier Price Collection For Customer Price
            $tierPriceCollection = $this->customerPrice->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('product_id', ['eq' => $product->getId()])
                    ->addFieldToFilter('customer_id', ['eq' => $customerId]);

            // Create an array of customer tier price
            if ($tierPriceCollection->getSize()) {
                $newTierPrice = $tierPriceCollection->getData();
                $groupId = $this->customerSession->create()->getCustomerGroupId();
                $websiteId = $this->getCurrentWebsiteId();
                foreach ($newTierPrice as $price) {
                    $res['website_id'] = $websiteId;
                    $res['all_groups'] = 0;
                    $res['cust_group'] = $groupId;
                    $res['price'] = (float)$price['new_price'];
                    $res['price_qty'] = (float)$price['qty'] * 1;
                    $res['website_price'] = (float)$price['new_price'];
                    $res['value'] = (float)$price['new_price'];
                    $newArray[] = $res;
                }

                // Merge Group Price Or Customer Price
                $newTier = array_merge($newArray, $product->getTierPrice());
                $price = array_column($newTier, 'price');
                $priceQty = array_column($newTier, 'price_qty');
                array_multisort($price, SORT_ASC, $newTier);
                //array_multisort($priceQty, SORT_ASC, $newTier);

                // Create unique array for group and customer price
                $finalTierPrice = $this->uniqueArray($newTier, 'price_qty');
                // Set New Tier Price
                $product->setData('tier_price', $finalTierPrice);
            }
        }
    }

    private function uniqueArray($array, $key)
    {
        $temp_array = [];
        $i = 0;
        $key_array = [];

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}
