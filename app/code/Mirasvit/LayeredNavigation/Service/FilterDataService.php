<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-navigation
 * @version   1.0.57
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\LayeredNavigation\Service;

use Mirasvit\LayeredNavigation\Api\Service\FilterDataServiceInterface;
use Magento\Framework\Search\Adapter\Mysql\Mapper;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory;
use Magento\Framework\Search\Adapter\Mysql\Aggregation\DataProviderContainer;
use Magento\Framework\Search\Adapter\Mysql\Aggregation\Builder\Container;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Framework\ObjectManagerInterface;
use Mirasvit\LayeredNavigation\Api\Service\SliderServiceInterface;

class FilterDataService implements FilterDataServiceInterface
{
    /**
     * FilterDataService constructor.
     * @param Mapper $mapper
     * @param TemporaryStorageFactory $temporaryStorageFactory
     * @param DataProviderContainer $dataProviderContainer
     * @param Container $aggregationContainer
     * @param ModuleManager $moduleManager
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Mapper $mapper,
        TemporaryStorageFactory $temporaryStorageFactory,
        DataProviderContainer $dataProviderContainer,
        Container $aggregationContainer,
        ModuleManager $moduleManager,
        ObjectManagerInterface $objectManager
    )
    {
        $this->mapper = $mapper;
        $this->temporaryStorageFactory = $temporaryStorageFactory;
        $this->dataProviderContainer = $dataProviderContainer;
        $this->aggregationContainer = $aggregationContainer;
        $this->moduleManager = $moduleManager;
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterBucketData($request, $attributeCode)
    {
        
        $responseBucket = $this->getElasticBucket($request, $attributeCode);
        return $responseBucket;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param string $attributeCode
     * @return array
     */
    protected function getElasticBucket($request, $attributeCode)
    {
       
        $client = $this->objectManager->create('Amasty\ElasticSearch\Model\Client\ClientRepositoryInterface')->get();
        $requestQuery = $this->objectManager->create('Amasty\ElasticSearch\Model\Search\GetRequestQuery')->execute($request);
        $getAggregations = $this->objectManager->create('Amasty\ElasticSearch\Model\Search\GetResponse\GetAggregations');
        $dataProvider = $this->objectManager->create('Magento\Framework\Search\Dynamic\DataProviderInterface');
        $elasticResponse = $client->search($requestQuery);

        $aggregations = $getAggregations->execute($request, $elasticResponse);
        $bucketAggregation = $request->getAggregation();
        $currentBucket = $this->getCurrentBucket($bucketAggregation, $attributeCode);
        if (!$currentBucket) {
            return [];
        }
        $minMaxSliderData = false;
        

        $responseBucket = $this->getCurrentBucketValue($aggregations, $attributeCode);
        if ($attributeCode == 'price') {
            $minMaxSliderData[SliderServiceInterface::SLIDER_DATA . $attributeCode]
                = $this->getCurrentBucketValue($elasticResponse['aggregations'], $attributeCode);
            $minMaxSliderData[SliderServiceInterface::SLIDER_DATA . $attributeCode]['value']
                = SliderServiceInterface::SLIDER_DATA . $attributeCode;
        }
        if ($minMaxSliderData && is_array($minMaxSliderData)) {
            return array_merge($minMaxSliderData, $responseBucket);
        }
        return $responseBucket;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param string $attributeCode
     * @return array
     */
    protected function getBucket($request, $attributeCode)
    {
        $query = $this->mapper->buildQuery($request);

        $temporaryStorage = $this->temporaryStorageFactory->create();
        $table = $temporaryStorage->storeDocumentsFromSelect($query);
        $dataProvider = $this->dataProviderContainer->get($request->getIndex());

        $bucketAggregation = $request->getAggregation();

        $currentBucket = $this->getCurrentBucket($bucketAggregation, $attributeCode);

        if (!$currentBucket) {
            return [];
        }

        $aggregationBuilder = $this->aggregationContainer->get($currentBucket->getType());
        $responseBucket = $aggregationBuilder->build(
            $dataProvider,
            $request->getDimensions(),
            $currentBucket,
            $table
        );

        return $responseBucket;
    }

    /**
     * @param array $bucketAggregation
     * @param string $attributeCode
     * @return \Magento\Framework\Search\Request\Aggregation\TermBucket
     */
    protected function getCurrentBucket($bucketAggregation, $attributeCode)
    {
        $attributeCode = $attributeCode . self::BUCKET;
        $currentBucket = false;
        foreach ($bucketAggregation as $requestBucket) {
            if ($requestBucket->getName() == $attributeCode) {
                $currentBucket = $requestBucket;
                break;
            }
        }

        return $currentBucket;
    }

    /**
     * @param array $bucketAggregation
     * @param string $attributeCode
     * @return \Magento\Framework\Search\Request\Aggregation\TermBucket
     */
    protected function getCurrentBucketValue($bucketAggregation, $attributeCode)
    {
        $attributeCode = $attributeCode . self::BUCKET;
        foreach ($bucketAggregation as $key => $requestBucket) {
            if ($key == $attributeCode) {
                return $requestBucket;
            }
        }

        return [];
    }


}