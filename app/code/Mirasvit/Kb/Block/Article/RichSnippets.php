<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-kb
 * @version   1.0.76
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Kb\Block\Article;

use Magento\Catalog\Model\Layer\Resolver as LayerResolver;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Theme\Block\Html\Header\Logo;

class RichSnippets extends Template
{
    /**
     * @var  \Magento\Theme\Block\Html\Header\Logo
     */
    protected $logo;

    /**
     * @var LayerResolver
     */
    private $layerResolver;

    /**
     * @var Registry
     */
    private $registry;

    public function __construct(
        Logo $logo,
        LayerResolver $layerResolver,
        Registry $registry,
        Context $context
    ) {
        $this->logo                     = $logo;
        $this->layerResolver            = $layerResolver;
        $this->registry                 = $registry;

        parent::__construct($context);
    }

    /**
     * @return bool|string
     */
    protected function _toHtml()
    {
        $data = $this->getJsonData();

        if (!$data) {
            return false;
        }

        return '<script type="application/ld+json">' . \Zend_Json::encode($data) . '</script>';
    }


    /**
     * @return bool|array
     */
    public function getJsonData()
    {
        $article = $this->registry->registry('current_article');

        if (!$article || !$article->getIsRsEnabled()) {
            return null;
        }
        $articleMetaTitle = $article->getMetaTitle();
        $articleMetaDescription = $article->getMetaDescription();
        $createdAt = $article->getCreatedAt();
        $authorName = $article->getUser()->getName();
        $articleName = $article->getName();
        $updatedAt = $article->getUpdatedAt();

        $storeLogo = $this->logo->getLogoSrc();

        return [
            "@context"         => "http://schema.org",
            "@type"            => "Article",
            "url"              => $this->_urlBuilder->escape($this->_urlBuilder->getCurrentUrl()),
            "name"             => $articleMetaTitle,
            "description"      => $articleMetaDescription,
            "author" => [
                "@type"        => "Person",
                "name"         => $authorName,
            ],
            "publisher"        => [
                "@type"        => "Organization",
                "name"         => $authorName,
                "logo"             => [
                    "@type"        => "ImageObject",
                    "url"          => $storeLogo,
                ],
            ],
            "datePublished"    => $createdAt,
            "headline"         => $articleName,
            "dateModified"     => $updatedAt,
            "image"            => $storeLogo,
            "mainEntityOfPage" => "WebPage",
        ];
    }
}