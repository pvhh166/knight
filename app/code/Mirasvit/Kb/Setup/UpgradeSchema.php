<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-kb
 * @version   1.0.76
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Kb\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var array<string,mixed>
     */
    private $pool;

    public function __construct(
        UpgradeSchema\Upgrade101 $upgrade1_0_1,
        UpgradeSchema\Upgrade102 $upgrade1_0_2,
        UpgradeSchema\Upgrade103 $upgrade1_0_3,
        UpgradeSchema\Upgrade104 $upgrade1_0_4,
        UpgradeSchema\Upgrade105 $upgrade1_0_5,
        UpgradeSchema\Upgrade106 $upgrade1_0_6,
        UpgradeSchema\Upgrade107 $upgrade1_0_7,
        UpgradeSchema\Upgrade108 $upgrade1_0_8,
        UpgradeSchema\Upgrade109 $upgrade1_0_9,
        UpgradeSchema\Upgrade1010 $upgrade1_0_10
    ) {
        $this->pool = [
            '1.0.1' => $upgrade1_0_1,
            '1.0.2' => $upgrade1_0_2,
            '1.0.3' => $upgrade1_0_3,
            '1.0.4' => $upgrade1_0_4,
            '1.0.5' => $upgrade1_0_5,
            '1.0.6' => $upgrade1_0_6,
            '1.0.7' => $upgrade1_0_7,
            '1.0.8' => $upgrade1_0_8,
            '1.0.9' => $upgrade1_0_9,
            '1.0.10' => $upgrade1_0_10,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        foreach ($this->pool as $version => $upgrade) {
            if (version_compare($context->getVersion(), $version) < 0) {
                $upgrade->upgrade($setup, $context);
            }
        }

        $setup->endSetup();
    }
}