<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-core
 * @version   1.2.119
 * @copyright Copyright (C) 2021 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Core\Block\Adminhtml\Config;

use Magento\Backend\Block\Template;
use Mirasvit\Core\Model\Config;
use Mirasvit\Core\Service\PackageService;

class UpdateNotifier extends Template
{
    protected $_template = 'Mirasvit_Core::config/update-notifier.phtml';

    private   $config;

    private   $packageService;

    public function __construct(
        Config $config,
        PackageService $packageService,
        Template\Context $context
    ) {
        $this->config         = $config;
        $this->packageService = $packageService;

        parent::__construct($context);
    }

    public function getNumberOfOutDates()
    {
        $packages = $this->packageService->getPackageList();
        $count    = 0;

        foreach ($packages as $package) {
            if ($package->getUrl() && $package->isOld()) {
                $count++;
            }
        }

        return $count;
    }

    public function getNumberOfTotal()
    {
        $packages = $this->packageService->getPackageList();
        $count    = 0;

        foreach ($packages as $package) {
            if ($package->getUrl()) {
                $count++;
            }
        }

        return $count;
    }

    public function toHtml()
    {
        if ($this->config->isMarketplace()) {
            return '';
        }

        return parent::toHtml();
    }
}
