<?php
/**
 * @author      Olegnax
 * @package     Olegnax_DeferJS
 * @copyright   Copyright (c) 2019 Olegnax (http://olegnax.com/). All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Olegnax\DeferJS\Model\Config\Backend;

use Magento\Config\Model\Config\Backend\Serialized as BackendSerialized;

/** @noinspection ClassNameCollisionInspection */

class Serialized extends BackendSerialized
{

    /**
     * @return $this
     */
    public function beforeSave()
    {
        if (is_array($this->getValue())) {
            /** @var array $value */
            $value = $this->getValue();
            if (array_key_exists('__empty', $value)) {
                /** @noinspection PhpIllegalStringOffsetInspection */
                unset($value['__empty']);
            }
            /** @noinspection PhpParamsInspection */
            $this->setValue($value);
        }
        parent::beforeSave();

        return $this;
    }

}
