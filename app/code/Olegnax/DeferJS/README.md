# Olegnax Defer Parsing of JavaScript Extension for Magento 2

This simple extension can help you to reduce the time before your customer can see and interact with your content which as result improves customer shopping experience and search rankings.

- Defer parsing of JavaScript helps improve initial page load time
- Improve customer shopping experience
- Improve SERP ranking (your website�s SEO)
- Defer parsing of JavaScript by moving it to the bottom of the page. So your page content loaded and shown before parsing of JavaScript.
- Minify deferred scripts to reduce their size as additional page load speed improvement.(Coming soon)