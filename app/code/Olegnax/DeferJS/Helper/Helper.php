<?php
/**
 * @author      Olegnax
 * @package     Olegnax_DeferJS
 * @copyright   Copyright (c) 2019 Olegnax (http://olegnax.com/). All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Olegnax\DeferJS\Helper;

use Olegnax\Core\Helper\Helper as CoreHelper;

/** @noinspection ClassNameCollisionInspection */

class Helper extends CoreHelper
{
    const CONFIG_MODULE = 'ox_deferjs';
}
