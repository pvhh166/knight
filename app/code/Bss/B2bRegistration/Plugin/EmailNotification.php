<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_B2bRegistration
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\B2bRegistration\Plugin;

use Magento\Customer\Api\Data\CustomerInterface;
use Bss\B2bRegistration\Helper\Data;
use Magento\Customer\Model\EmailNotificationInterface;

class EmailNotification
{
    /**
     * EmailNotification constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Customer\Model\EmailNotification $subject
     * @param callable $proceed
     * @param CustomerInterface $customer
     * @param string $type
     * @param string $backUrl
     * @param int $storeId
     * @param null $sendemailStoreId
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundNewAccount(
        \Magento\Customer\Model\EmailNotification $subject,
        callable $proceed,
        CustomerInterface $customer,
        $type,
        $backUrl = '',
        $storeId = 0,
        $sendemailStoreId = null
    ) {
        $enable = $this->helper->isEnable();
        $isAutoApproval = $this->helper->isAutoApproval();
        if (($type == EmailNotificationInterface::NEW_ACCOUNT_EMAIL_REGISTERED ||
                $type == EmailNotificationInterface::NEW_ACCOUNT_EMAIL_CONFIRMED
            ) &&
            $enable &&
            !$isAutoApproval
        ) {
            return false;
        } else {
            return $proceed($customer, $type, $backUrl, $storeId, $sendemailStoreId);
        }
    }
}
