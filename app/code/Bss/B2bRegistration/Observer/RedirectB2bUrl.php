<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_B2bRegistration
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\B2bRegistration\Observer;

use Magento\Framework\Event\ObserverInterface;

class RedirectB2bUrl implements ObserverInterface
{
    /**
     * @var \Bss\B2bRegistration\Helper\Data
     */
    private $helper;

    /**
     * RedirectB2bUrl constructor.
     * @param \Bss\B2bRegistration\Helper\Data $helper
     */
    public function __construct(
        \Bss\B2bRegistration\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Redirect to B2b account craeate Page
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $enable = $this->helper->isEnable();
        if ($enable) {
            $request = $observer->getData('request');
            $bbUrl = $this->helper->getB2bUrl();
            $urlRequest = $request->getOriginalPathInfo();
            $urlRequest = str_replace('/', '', $urlRequest);
            $urlRequest = str_replace('.html', '', $urlRequest);
            if ($bbUrl == $urlRequest) {
                $controllerRequest = $observer->getData('controller_action')->getRequest();
                $controllerRequest->initForward();
                $controllerRequest->setModuleName('b2b');
                $controllerRequest->setControllerName('account');
                $controllerRequest->setActionName('create');
                $controllerRequest->setDispatched(false);
            }
        }
    }
}
