<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_B2bRegistration
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\B2bRegistration\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\UrlFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Bss\B2bRegistration\Helper\Data;
use Bss\B2bRegistration\Model\Config\Source\CustomerAttribute;
use Bss\B2bRegistration\Helper\Email as BssHelperEmail;
use Bss\B2bRegistration\Helper\CreateAccount;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreatePost extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var Magento\Framework\App\Action\Context
     */
    protected $context;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var UrlFactory
     */
    protected $urlFactory;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var BssHelperEmail
     */
    protected $helperEmail;

    /**
     * @var CreateAccount
     */
    protected $helperCreateAccount;

    /**
     * @var AddressInterfaceFactory
     */
    protected $addressDataFactory;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;


    /**
     * CreatePost constructor.
     * @param Context $context
     * @param AccountManagementInterface $accountManagement
     * @param UrlFactory $urlFactory
     * @param Validator $formKeyValidator
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param AccountRedirect $accountRedirect
     * @param BssHelperEmail $helperEmail
     * @param CreateAccount $helperCreateAccount
     */
    public function __construct(
        Context $context,
        AccountManagementInterface $accountManagement,
        UrlFactory $urlFactory,
        Validator $formKeyValidator,
        Data $helper,
        LoggerInterface $logger,
        AccountRedirect $accountRedirect,
        BssHelperEmail $helperEmail,
        CreateAccount $helperCreateAccount,
        AddressInterfaceFactory $addressDataFactory,
        AddressRepositoryInterface $addressRepository,
        DataObjectHelper $dataObjectHelper
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->accountManagement = $accountManagement;
        $this->urlFactory = $urlFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->logger = $logger;
        $this->accountRedirect = $accountRedirect;
        $this->helperEmail = $helperEmail;
        $this->helperCreateAccount = $helperCreateAccount;
        $this->addressDataFactory = $addressDataFactory;
        $this->addressRepository = $addressRepository;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * Add address to customer during create account
     * @return \Magento\Customer\Api\Data\AddressInterface |$addressDataObject;
     */
    protected function extractAddress()
    {
        if (!$this->getRequest()->getPost('create_address')) {
            return null;
        }
        $addressForm = $this->helperCreateAccount->getFormFactory()->create(
            'customer_address',
            'customer_register_address'
        );
        $allowedAttributes = $addressForm->getAllowedAttributes();
        $addressData = [];
        $regionDataObject = $this->helperCreateAccount->getRegionDataFactory();
        foreach ($allowedAttributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $value = $this->getRequest()->getParam($attributeCode);
            if ($value === null) {
                continue;
            }
            switch ($attributeCode) {
                case 'region_id':
                    $regionDataObject->setRegionId($value);
                    break;
                case 'region':
                    $regionDataObject->setRegion($value);
                    break;
                default:
                    $addressData[$attributeCode] = $value;
            }
        }
        $addressDataObject = $this->helperCreateAccount->getDataAddressFactory();
        $this->helper->getDataObject()->populateWithArray(
            $addressDataObject,
            $addressData,
            \Magento\Customer\Api\Data\AddressInterface::class
        );
        $addressDataObject->setRegion($regionDataObject);

        $addressDataObject->setIsDefaultBilling(
            $this->getRequest()->getParam('default_billing', false)
        )->setIsDefaultShipping(
            $this->getRequest()->getParam('default_shipping', false)
        );
        return $addressDataObject;
    }

    /**
     * Make sure that password and password confirmation matched
     * @param string $password
     * @param string $confirmation
     * @return void
     * @throws InputException
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    /**
     * Create B2b account Action
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->checkLogin();

        if (!$this->getRequest()->isPost() || !$this->formKeyValidator->validate($this->getRequest())) {
            $url = $this->urlFactory->create()->getUrl('*/*/create', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }

        $autoApproval = $this->helper->isAutoApproval();
        $this->helperCreateAccount->getCustomerSession()->regenerateId();

        try {
           // $address = $this->extractAddress();
           // $addresses = $address === null ? [] : [$address];

            $params = $this->getRequest()->getParams();
            $addresses = [];
            $region = isset($params['comp_region']) ? $params['comp_region'] : null;
            $regionId = isset($params['comp_region_id']) ? $params['comp_region_id'] : null;
            $addressData = array(
                'prefix' => '',
                'firstname' => $params['firstname'],
                'middlename' => '',
                'lastname' => $params['lastname'],
                'suffix' => '',
                'company' => $params['comp_company'],
                'street' => array(
                    '0' => $params['comp_street'], // this is mandatory
                    '1' => $params['comp_street_2'],
                    '2' => $params['comp_street_3']
                ),
                'city' => $params['comp_city'],
                'country_id' => $params['comp_country_id'], // two letters country code
                'region' => $region,
                'region_id' => $regionId, // can be empty '' if no region_id
                'postcode' => $params['comp_postcode'],
                'telephone' => $params['comp_phone'],
                'default_billing' => 1,
                'default_shipping' => 1
            );
            
            $addressData['region'] = [
                'region' => $region,
                'region_id' => $regionId,
            ];

            $addressDataObject = $this->addressDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $addressDataObject,
                $addressData,
                \Magento\Customer\Api\Data\AddressInterface::class
            );
            
            $addresses[] = $addressDataObject;

            $customer = $this->helper->getCustomerExtractor()->extract(
                'customer_account_create',
                $this->_request
            );
            //$customer->setAddresses($addresses);
            $password = $this->getRequest()->getParam('password');
            $confirmation = $this->getRequest()->getParam('password_confirmation');
            $redirectUrl = $this->helperCreateAccount->getCustomerSession()->getBeforeAuthUrl();
            $this->checkPasswordConfirmation($password, $confirmation);
            $customer = $this->accountManagement
                ->createAccount($customer, $password, $redirectUrl);
            $this->helperCreateAccount->getCustomerSession()->setBssSaveAccount('true');
            $this->subcribeCustomer($customer);
            $this->saveGroupAttribute($customer);

            $this->_eventManager->dispatch(
                'bss_customer_register_success',
                ['account_controller' => $this, 'customer' => $customer]
            );

            $customer->setCustomAttribute("comp_region", $region);

            if (!$autoApproval) {
                $url = $this->setPendingCustomer($customer);
                $resultRedirect->setUrl($this->_redirect->success($url));
            } else {
                // set status is B2b Approval and redirect to success url
                $customer->setCustomAttribute("b2b_activasion_status", CustomerAttribute::B2B_APPROVAL);
                $this->helperCreateAccount->getCustomerRepository()->save($customer);
                $this->helperCreateAccount->getCustomerSession()->setCustomerDataAsLoggedIn($customer);
                $this->messageManager->addSuccessMessage($this->getSuccessMessage());
                $resultRedirect = $this->callBackUrl($resultRedirect);
            }

            $this->saveCustomerAddress($addressData , $customer);

            return $resultRedirect;
        } catch (StateException $e) {
            $url = $this->urlFactory->create()->getUrl('customer/account/forgotpassword');
            // @codingStandardsIgnoreStart
            $message = __(
                'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.',
                $url
            );
            // @codingStandardsIgnoreEnd
            $this->messageManager->addError($message);
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t save the customer.'));
        }

        $this->helperCreateAccount->getCustomerSession()->setCustomerFormData($this->getRequest()->getPostValue());
        $defaultUrl = $this->urlFactory->create()->getUrl('b2b/account/create', ['_secure' => true]);
        $resultRedirect->setUrl($this->_redirect->error($defaultUrl));
        return $resultRedirect;
    }

    /**
     * @param object $customer
     * @return void
     */
    protected function setCustomerStatusConfirm($customer)
    {
        $autoApproval = $this->helper->isAutoApproval();
        if ($autoApproval) {
            $customer->setCustomAttribute("b2b_activasion_status", CustomerAttribute::B2B_APPROVAL);
        } else {
            $customer->setCustomAttribute("b2b_activasion_status", CustomerAttribute::B2B_PENDING);
        }
    }

    /**
     * Check Customer Login
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function checkLogin()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->helperCreateAccount->getCustomerSession()->isLoggedIn()) {
            $resultRedirect->setPath('customer/account/index');
            return $resultRedirect;
        }
    }

    /**
     * Check subcribe customer
     * @param object $customer
     * @return void
     */
    protected function subcribeCustomer($customer)
    {
        if ($this->getRequest()->getParam('is_subscribed', false)) {
            $this->helperCreateAccount->getSubscriberFactory()->subscribeCustomerById($customer->getId());
        }
    }

    /**
     * Save B2b Customer Group
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return void
     */
    protected function saveGroupAttribute($customer)
    {
        try {
            $customerGroupId = $this->helper->getCustomerGroup();
            $tax = $this->getRequest()->getPostValue('taxvat');
            $gender = $this->getRequest()->getPostValue('gender');

            if ($tax) {
                $customer->setTaxvat($tax);
            }
            if ($gender) {
                $customer->setGender($gender);
            }
            $customer->setGroupId($customerGroupId);
            $this->helperCreateAccount->getCustomerRepository()->save($customer);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }

    /**
     * @return \Magento\Framework\Phrase
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getSuccessMessage()
    {
        if ($this->helperCreateAccount->getAddressHelper()->isVatValidationEnabled()) {
            if ($this->helperCreateAccount->getAddressHelper()
                    ->getTaxCalculationAddressType() == Address::TYPE_SHIPPING
            ) {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your shipping address for proper VAT calculation.',
                    $this->urlFactory->create()->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            } else {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your billing address for proper VAT calculation.',
                    $this->urlFactory->create()->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            }
        } else {
            $storeName = $this->helper->getStoreName();
            $message = __('Thank you for registering with %1.', $storeName);
        }
        return $message;
    }

    /**
     * @param $customer
     * @return string
     * @throws InputException
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    protected function setPendingCustomer($customer)
    {
        $customerEmail = $customer->getEmail();
        $emailTemplate = $this->helper->getAdminEmailTemplate();
        $fromEmail = $this->helper->getAdminEmailSender();
        $recipient = $this->helper->getAdminEmail();
        $recipient = str_replace(' ', '', $recipient);
        $recipient = (explode(',', $recipient));
        $emailVar = [
            'varEmail'  => $customerEmail
        ];
        $storeId = $this->helper->getStoreId();
        $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
        if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
            $this->setCustomerStatusConfirm($customer);
            $this->helperCreateAccount->getCustomerRepository()->save($customer);
            $emailUrl = $this->helper->getEmailConfirmUrl($customer->getEmail());
            // @codingStandardsIgnoreStart
            $this->messageManager->addSuccessMessage(
                __(
                    'You must confirm your account. Please check your email for the confirmation link or <a href="%1">click here</a> for a new link.',
                    $emailUrl
                )
            );
            // @codingStandardsIgnoreEnd
            $this->helperEmail->sendEmail($fromEmail, $recipient, $emailTemplate, $storeId, $emailVar);
        } else {
            $customer->setCustomAttribute("b2b_activasion_status", CustomerAttribute::B2B_PENDING);
            $this->helperCreateAccount->getCustomerRepository()->save($customer);
            $message = $this->helper->getPendingMess();
            $this->messageManager->addSuccessMessage($message);
            $this->helperEmail->sendEmail($fromEmail, $recipient, $emailTemplate, $storeId, $emailVar);
        }
        $url = $this->urlFactory->create()->getUrl('customer/account/login', ['_secure' => true]);
        return $url;
    }

    /**
     * @param $resultRedirect
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\Controller\Result\Redirect
     */
    protected function callBackUrl($resultRedirect)
    {
        $requestedRedirect = $this->accountRedirect->getRedirectCookie();
        if (!$this->helperCreateAccount->getScopeConfig()->getValue('customer/startup/redirect_dashboard') &&
            $requestedRedirect
        ) {
            $resultRedirect->setUrl($this->_redirect->success($requestedRedirect));
            $this->accountRedirect->clearRedirectCookie();
            return $resultRedirect;
        }
        return $this->accountRedirect->getRedirect();
    }

    public function saveCustomerAddress($addresses,$customer)
    {
        $address = $this->addressDataFactory->create();
        $address->setCustomerId($customer->getId())
                ->setFirstname($addresses['firstname'])
                ->setLastname($addresses['lastname'])
                ->setCompany($addresses['company'])
                ->setCountryId($addresses['country_id'])
                ->setRegionId($addresses['region_id'])
                ->setCity($addresses['city'])
                ->setPostcode($addresses['postcode'])
                ->setCustomerId($customer->getId())
                ->setStreet($addresses['street'])
                ->setTelephone($addresses['telephone'])
                ->setIsDefaultBilling($addresses['default_billing'])
                ->setIsDefaultShipping($addresses['default_shipping']);
        try{
            $this->addressRepository->save($address);
        }catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t save the customer.'));
        }
    }

}
