<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     * @var \Bss\Faqs\Helper\ModuleConfig
     */
    public $moduleConfig;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    public $resultForwardFactory;

    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    public $faqHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Bss\Faqs\Helper\ModuleConfig $moduleConfig
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Bss\Faqs\Helper\ModuleConfig $moduleConfig,
        \Bss\Faqs\Helper\FaqHelper $faqHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->moduleConfig = $moduleConfig;
        $this->coreRegistry = $coreRegistry;
        $this->faqHelper = $faqHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        if (!$this->moduleConfig->isModuleEnable()) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        } else {
            $resultPage = $this->resultPageFactory->create();
            $this->_view->getPage()->getConfig()->getTitle()->set(__('FAQs Main Page'));
            return $resultPage;
        }
    }
}
