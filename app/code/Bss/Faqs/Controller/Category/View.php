<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Category;
 
use Magento\Framework\App\Action\Context;
 
class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * @var \Bss\Faqs\Helper\ModuleConfig
     */
    private $moduleConfig;

    /**
     * View constructor.
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Bss\Faqs\Helper\ModuleConfig $moduleConfig
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Bss\Faqs\Helper\FaqHelper $faqHelper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Bss\Faqs\Helper\ModuleConfig $moduleConfig
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->faqHelper = $faqHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->coreRegistry = $coreRegistry;
        $this->moduleConfig = $moduleConfig;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $url = $this->getRequest()->getParam('url');
        $id = $this->getRequest()->getParam('id');
        if ($url != null) {
            $this->coreRegistry->register('param_type', 'url');
            $this->coreRegistry->register('param_value', $url);
            $category = $this->faqHelper->getCategoryByUrl($url, 'time');
        } else {
            $this->coreRegistry->register('param_type', 'id');
            $this->coreRegistry->register('param_value', $id);
            $category = $this->faqHelper->getCategoryById($id, 'time');
        }
        if (!$this->moduleConfig->isModuleEnable() || $category === null) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        } else {
            $this->coreRegistry->register('category_id', $category['faq_category_id']);
            $resultPage = $this->resultPageFactory->create();
            $this->_view->getPage()->getConfig()->getTitle()->set(__('FAQs Category'));
            return $resultPage;
        }
    }
}
