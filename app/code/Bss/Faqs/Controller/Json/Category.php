<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Json;
 
use Magento\Framework\App\Action\Context;
 
class Category extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Bss\Faqs\Helper\ModuleConfig
     */
    private $moduleConfig;

    /**
     * Category constructor.
     * @param Context $context
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     * @param \Bss\Faqs\Helper\ModuleConfig $moduleConfig
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        \Bss\Faqs\Helper\FaqHelper $faqHelper,
        \Bss\Faqs\Helper\ModuleConfig $moduleConfig,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->faqHelper = $faqHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->moduleConfig = $moduleConfig;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $sort = $this->getRequest()->getParam('sort_order', 'time');
            $category = [];
            if ($this->getRequest()->getParam('type') == 'id') {
                $category = $this->faqHelper->getCategoryById($this->getRequest()->getParam('value'), $sort);
            } elseif ($this->getRequest()->getParam('type') == 'url') {
                $category = $this->faqHelper->getCategoryByUrl($this->getRequest()->getParam('value'), $sort);
            }
            $result = [];
            $result['main_content'] = [];
            $result['main_content'][] = $category;
            $result['sidebar'] = $this->faqHelper->getSidebarData();
            return $resultJson->setData($result);
        } else {
            return $resultJson->setData(null);
        }
    }
}
