<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Json;
 
use Magento\Framework\App\Action\Context;
 
class ProductFaqSubmit extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;

    /**
     * ProductFaqSubmit constructor.
     * @param Context $context
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        \Bss\Faqs\Helper\FaqHelper $faqHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
    ) {
        $this->faqHelper = $faqHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->date = $date;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax() && $this->formKeyValidator->validate($this->getRequest())) {
            $question = $this->getRequest()->getParam('question');
            $productId = $this->getRequest()->getParam('product_id');
            $customer = $this->getRequest()->getParam('username');
            $submitNewQuestion = $this->faqHelper->submitNewQuestion(
                $question,
                $productId,
                $customer,
                $this->date->date('Y-m-d')
            );
            
            return $result->setData($submitNewQuestion);
        } else {
            return $result->setData(__('Error'));
        }
    }
}
