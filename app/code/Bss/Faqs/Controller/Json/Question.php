<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Json;
 
use Magento\Framework\App\Action\Context;
 
class Question extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Bss\Faqs\Helper\ModuleConfig
     */
    private $moduleConfig;

    /**
     * Question constructor.
     * @param Context $context
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     * @param \Bss\Faqs\Helper\ModuleConfig $moduleConfig
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        \Bss\Faqs\Helper\FaqHelper $faqHelper,
        \Bss\Faqs\Helper\ModuleConfig $moduleConfig,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->faqHelper = $faqHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->moduleConfig = $moduleConfig;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $request = $this->getRequest();
            if ($this->getRequest()->getParam('type') == 'id') {
                $question = $this->faqHelper->getFaqById($request->getParam('value'));
            } elseif ($this->getRequest()->getParam('type') == 'url') {
                $question = $this->faqHelper->getFaqByUrl($request->getParam('value'));
            }
            $question['cate_info'] = $this->faqHelper->getCategoryImage($request->getParam('category_id', null));
            return $resultJson->setData($question);
        } else {
            return $resultJson->setData(null);
        }
    }
}
