<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Adminhtml\Faqs;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

class Save extends Action
{

    /**
     * @var \Bss\Faqs\Model\ResourceModel\Faqs
     */
    private $faqResource;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;

    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Bss\Faqs\Model\ResourceModel\Faqs $faqResource
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Bss\Faqs\Model\ResourceModel\Faqs $faqResource,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Bss\Faqs\Helper\FaqHelper $faqHelper
    ) {
        parent::__construct($context);
        $this->faqResource = $faqResource;
        $this->date = $date;
        $this->session = $context->getSession();
        $this->faqHelper = $faqHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            try {
                if ($data['time'] == '') {
                    $data['time'] = $this->date->date('Y-m-d');
                }
                if (array_key_exists('category_id', $data)) {
                    $categoryList = explode(';', $data['category_id']);
                } else {
                    $categoryList = null;
                    $data['category_id'] = '';
                }
                if ($data['use_real_vote_data'] == '1' && $data['faq_id'] != null) {
                    $voteData = $this->faqHelper->getFaqVoting($data['faq_id'], null);
                    $data['helpful_vote'] = $voteData['like'];
                    $data['unhelpful_vote'] = $voteData['unlike'];
                }
                if (array_key_exists('url_key', $data)
                    && $data['url_key'] != ""
                    && !$this->faqHelper->checkFaqUrl($data['url_key'], $data['faq_id'])) {
                    $this->messageManager->addErrorMessage(__("URL already exist"));
                    $this->session->setFaqData($data);
                } else {
                    $faqId = $this->faqResource->saveFaq(
                        [
                            "title" => $data["title"],
                            "faq_id" => $data["faq_id"],
                            "category_id" => $data["category_id"],
                            "answer" => $data["answer"],
                            "is_show_full_answer" => $data["is_show_full_answer"],
                            "short_answer" => $data["short_answer"],
                            "customer" => $data["customer"],
                            "is_most_frequently" => $data["is_most_frequently"],
                            "tag" => $data["tag"],
                            "time" => $data["time"],
                            "store_id" => $data["store_id"],
                            "related_faq_id" => $data["related_faq_id"],
                            "url_key" => $data["url_key"],
                            "use_real_vote_data" => $data["use_real_vote_data"],
                            "helpful_vote" => ($data['use_real_vote_data'] == '0') ? $data["helpful_vote"] : 0,
                            "unhelpful_vote" => ($data['use_real_vote_data'] == '0') ? $data["unhelpful_vote"] : 0,
                            "product_id" => $data["product_id"],
                            "is_check_all_product" => $data["is_check_all_product"]
                        ]
                    );
                    if ($categoryList != null) {
                        $this->faqHelper->updateCategoryFaqList(
                            $categoryList,
                            $data['category_id_oldvalue'],
                            $faqId
                        );
                    }
                    $this->messageManager->addSuccessMessage(__('The FAQ has been saved.'));
                    $this->_redirect('adminhtml/*/');
                    return;
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->session->setFaqData($data);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
                $this->session->setFaqData($data);
            }
        }
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl($this->getUrl('*')));
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_Faqs::faqs_save');
    }
}
