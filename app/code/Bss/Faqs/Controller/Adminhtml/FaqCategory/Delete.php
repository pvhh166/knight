<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Adminhtml\FaqCategory;

use Magento\Backend\App\Action;

class Delete extends Action
{

    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Bss\Faqs\Helper\FaqHelper $faqHelper
    ) {
        parent::__construct($context);
        $this->faqHelper = $faqHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $delId = $this->getRequest()->getParam('faq_category_id', 0);
        if ($delId) {
            try {
                $this->faqHelper->deleteFaqCategory($delId);
                $this->messageManager->addSuccessMessage(__('You deleted the FAQ Category.'));
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('We can\'t delete FAQ Category right now.'));
                $this->_redirect('adminhtml/*/edit/', ['id' => $delId]);
                return;
            }
        }
        $this->_redirect('adminhtml/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_Faqs::faqCategory_delete');
    }
}
