<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Controller\Adminhtml\FaqCategory;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

class Save extends Action
{
    /**
     * @var \Bss\Faqs\Model\ResourceModel\FaqCategory
     */
    private $faqCategoryResource;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;

    /**
     * @var \Bss\Faqs\Helper\FaqHelper
     */
    private $faqHelper;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Bss\Faqs\Model\ResourceModel\FaqCategory $faqCategoryResource
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Bss\Faqs\Helper\FaqHelper $faqHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Bss\Faqs\Model\ResourceModel\FaqCategory $faqCategoryResource,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Bss\Faqs\Helper\FaqHelper $faqHelper
    ) {
        parent::__construct($context);
        $this->faqCategoryResource = $faqCategoryResource;
        $this->date = $date;
        $this->session = $context->getSession();
        $this->faqHelper = $faqHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $data['image'] = $this->faqHelper->saveImage($data['old_image'], $this->messageManager);
        if ($data) {
            try {
                if (array_key_exists('faq_id', $data)) {
                    $faqList = explode(';', $data['faq_id']);
                } else {
                    $faqList = null;
                    $data['faq_id'] = '';
                }
                if (array_key_exists('url_key', $data) && $data['url_key'] != ''
                    && !$this->faqHelper->checkCategoryUrl($data['url_key'], $data['faq_category_id'])) {
                    $this->messageManager->addErrorMessage(__("URL already exist"));
                    $this->session->setFaqCategoryData($data);
                } else {
                    if ($data["url_key"] == '') {
                        $data['url_key'] = null;
                    }
                    $categoryId = $this->faqCategoryResource->saveFaqCategory(
                        [
                            "faq_category_id" => $data["faq_category_id"],
                            "title" => $data["title"],
                            "faq_id" => $data["faq_id"],
                            "image" => $data["image"],
                            "show_in_mainpage" => $data["show_in_mainpage"],
                            "url_key" => $data["url_key"],
                            "color" => $data["color"],
                            "title_color" => $data["title_color"]
                        ]
                    );
                    if ($faqList != null) {
                        $this->faqHelper->updateFaqCategoryList(
                            $faqList,
                            $data['faq_id_oldvalue'],
                            $categoryId
                        );
                    }
                    $this->messageManager->addSuccessMessage(
                        __('The Category "' . $data['title'] . '" has been saved.')
                    );
                    $this->_redirect('adminhtml/*/');
                    return;
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->session->setFaqCategoryData($data);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
                $this->session->setFaqCategoryData($data);
            }
        }
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl($this->getUrl('*')));
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_Faqs::faqCategory_save');
    }
}
