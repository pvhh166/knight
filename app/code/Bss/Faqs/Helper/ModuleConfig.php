<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Helper;

class ModuleConfig extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Config\scopeConfigInterface
     */
    private $scope;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var int $storeId
     */
    private $storeId;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    private $localeFormat;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    private $currency;

    /**
     * ModuleConfig constructor.
     * @param \Magento\Framework\App\Config\scopeConfigInterface $scope
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\scopeConfigInterface $scope,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scope = $scope;
        $this->storeManager = $storeManager;
        $this->localeFormat = $localeFormat;
        $this->jsonEncoder = $jsonEncoder;
        $this->currency = $currency;
    }

    /**
     * @return boolean
     */
    public function isModuleEnable()
    {
        return $this->scope->getValue(
            'faqs_config/Bss_Faqs/Enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return boolean
     */
    public function isAddRoboto()
    {
        return $this->scope->getValue(
            'faqs_config/Bss_Faqs/add_roboto',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return boolean
     */
    public function isAddAwesome()
    {
        return $this->scope->getValue(
            'faqs_config/Bss_Faqs/add_awesome',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeId = $this->storeManager->getStore()->getId();
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    /**
     * @return boolean
     */
    public function isShowMostFaq()
    {
        return $this->scope->getValue(
            'faqs_config/display/most_faq_show',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return int
     */
    public function mostFaqNumber()
    {
        return $this->scope->getValue(
            'faqs_config/display/most_faq_number',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return boolean
     */
    public function isShowRelatedFaq()
    {
        return $this->scope->getValue(
            'faqs_config/display/related_faq_show',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return int
     */
    public function questionPerCategory()
    {
        return $this->scope->getValue(
            'faqs_config/display/question_per_category',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return int
     */
    public function questionDisplay()
    {
        return $this->scope->getValue(
            'faqs_config/display/question_display',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->scope->getValue(
            'faqs_config/display/background_color',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getSortBy()
    {
        return $this->scope->getValue(
            'faqs_config/display/faq_sort_by',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return boolean
     */
    public function isRequireLoginVoting()
    {
        return $this->scope->getValue(
            'faqs_config/display/require_login_vote',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return boolean
     */
    public function fullQuestionProductPage()
    {
        return $this->scope->getValue(
            'faqs_config/display/full_question_product_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getMessageSearchResult()
    {
        return $this->scope->getValue(
            'faqs_config/message/search',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getMessageTagResult()
    {
        return $this->scope->getValue(
            'faqs_config/message/tag',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getMessageUnhelpful()
    {
        return $this->scope->getValue(
            'faqs_config/message/unhelpful',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }
}
