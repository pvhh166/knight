<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Helper;

class FaqHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Faq Category factory
     */
    private $faqCategoryFactory;

    /**
     * Faq factory
     */
    private $faqsFactory;

    /**
     * Uploader
     */
    private $uploader;

    /**
     * Filesystem
     */
    private $filesystem;

    /**
     * Store Manager
     */
    private $storeManager;

    /**
     * Voting Data Factory
     * @var \Bss\Faqs\Model\FaqsVoteFactory $faqsVote
     */
    private $faqsVote;

    /**
     * Vote Resource Model
     * @var \Bss\Faqs\Model\ResourceModel\FaqsVote $voteFaqResource
     */
    private $voteFaqResource;

    /**
     * Faq Resource Model
     * @var \Bss\Faqs\Model\ResourceModel\Faqs $faqsResource
     */
    private $faqsResource;

    /**
     * Faq Category Resource Model
     * @var \Bss\Faqs\Model\ResourceModel\FaqCategory $faqCategoryResource
     */
    private $faqCategoryResource;

    /**
     * Date Lib
     * @var \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    private $date;

    /**
     * Module Config
     * @var ModuleConfig $moduleConfig
     */
    private $moduleConfig;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    private $filterProvider;

    /**
     * FaqHelper constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploader
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Bss\Faqs\Model\FaqsFactory $faqsFactory
     * @param \Bss\Faqs\Model\FaqCategoryFactory $faqCategoryFactory
     * @param \Bss\Faqs\Model\FaqsVoteFactory $faqsVote
     * @param \Bss\Faqs\Model\ResourceModel\FaqsVote $voteFaqResource
     * @param \Bss\Faqs\Model\ResourceModel\Faqs $faqsResource
     * @param \Bss\Faqs\Model\ResourceModel\FaqCategory $faqCategoryResource
     * @param ModuleConfig $moduleConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Bss\Faqs\Model\FaqsFactory $faqsFactory,
        \Bss\Faqs\Model\FaqCategoryFactory $faqCategoryFactory,
        \Bss\Faqs\Model\FaqsVoteFactory $faqsVote,
        \Bss\Faqs\Model\ResourceModel\FaqsVote $voteFaqResource,
        \Bss\Faqs\Model\ResourceModel\Faqs $faqsResource,
        \Bss\Faqs\Model\ResourceModel\FaqCategory $faqCategoryResource,
        ModuleConfig $moduleConfig
    ) {
        $this->faqsFactory = $faqsFactory;
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->faqsVote = $faqsVote;
        $this->voteFaqResource = $voteFaqResource;
        $this->faqsResource = $faqsResource;
        $this->faqCategoryResource = $faqCategoryResource;
        $this->date = $date;
        $this->moduleConfig = $moduleConfig;
        $this->filterProvider = $filterProvider;
        parent::__construct($context);
    }

    /**
     * @return int Store Id
     */
    public function getStoreId()
    {
        if (!empty($this->storeManager)) {
            return $this->storeManager->getStore()->getId();
        }
    }

    /**
     * @param bool $storeScope
     * @param bool $mostFaq
     * @param null $sortBy
     * @return \Magento\Framework\DataObject[]
     */
    public function getFaqList($storeScope = false, $mostFaq = false, $sortBy = null)
    {
        $faqsCollection = $this->faqsFactory->create()->getCollection();
        if ($sortBy === null) {
            $sortBy = $this->moduleConfig->getSortBy();
        }
        $mostFaqNumber = $this->moduleConfig->mostFaqNumber();

        if ($storeScope) {
            $faqsCollection->addFieldToFilter('store_id', $this->getStoreId());
        }

        if ($mostFaq) {
            $faqsCollection->addFieldToFilter('is_most_frequently', '1')->setPageSize($mostFaqNumber)->setCurPage(1);
        }

        if ($sortBy === 'time') {
            $faqsCollection->setOrder($sortBy, 'DESC');
        } elseif ($sortBy === 'title') {
            $faqsCollection->setOrder($sortBy, 'ASC');
        } elseif ($sortBy === 'helpful_vote') {
            $faqsCollection->setOrder($sortBy, 'DESC');
        }

        return $faqsCollection->getItems();
    }

    /**
     * @return array All Faq Data
     */
    public function getAllData()
    {
        $result = [];
        $questionPerCategory = $this->moduleConfig->questionPerCategory();
        $categories = $this->faqCategoryFactory->create()->getCollection()->addFieldToFilter('show_in_mainpage', '1');
        $faqs = $this->getFaqList(true);

        foreach ($categories as $catekey => $category) {
            $result[$catekey] = $category->getData();
            $result[$catekey]['faq'] = [];
            $result[$catekey]['counter'] = 0;
        }
        foreach ($faqs as $faq) {
            if ($faq->getIsShowFullAnswer() == '1') {
                $faq->setShortAnswer($this->filterProvider->getPageFilter()->filter($faq->getAnswer()));
            }
            $faq->setTime($this->date->date('Y M d', $faq->getTime()));
            foreach ($categories as $catekey => $category) {
                if ($result[$catekey]['counter'] >= $questionPerCategory) {
                    continue;
                }
                if (array_search($faq->getFaqId(), explode(';', $category['faq_id'])) !== false) {
                    $result[$catekey]['faq'][] = $faq->getData();
                    $result[$catekey]['counter'] ++;
                }
            }
        }
        foreach ($result as $catekey => $category) {
            if (empty($category['faq'])) {
                unset($result[$catekey]);
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getSidebarData()
    {
        $result = ['tag' => [], 'category' => [], 'most_faq' => []];
        $mostFaqNumber = $this->moduleConfig->mostFaqNumber();
        $faqsCollection = $this->faqsFactory->create()->getCollection()
        ->addFieldToFilter('store_id', $this->getStoreId())->getItems();

        $categoryCollection = $this->faqCategoryFactory->create()->getCollection()
        ->addFieldToFilter('faq_id', ['neq' => ''])
        ->addFieldToFilter('show_in_mainpage', '1')
        ->getItems();
        foreach ($categoryCollection as $key => $category) {
            foreach (explode(';', $category['faq_id']) as $key => $subfaq) {
                if (array_key_exists($subfaq, $faqsCollection)) {
                    $result['category'][] = $category->getData();
                    break;
                }
            }
        }

        foreach ($faqsCollection as $key => $item) {
            $tagList = $item->getTag();
            foreach (explode(',', $tagList) as $tag) {
                if ($tag != '' && !in_array($tag, $result['tag'])) {
                    $result['tag'][] = $tag;
                }
            }
        }

        $mostFaqCollection = $this->getFaqList(true, true);
        foreach ($mostFaqCollection as $key => $mostfaq) {
            $mostFaqNumber --;
            $result['most_faq'][] = $mostfaq->getData();
            if ($mostFaqNumber == 0) {
                break;
            }
        }
        return $result;
    }

    /**
     * @param string $url
     * @return array|null
     */
    public function getFaqByUrl($url)
    {
        $collection = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId());
        $faq = $collection->getItemByColumnValue('url_key', $url);
        if ($faq === null) {
            return null;
        }
        $result = $faq->getData();
        $result['answer'] = $this->filterProvider->getPageFilter()->filter($result['answer']);
        $result['time'] = $this->date->date('Y M d', $faq->getTime());
        if ($result['related_faq_id'] != null) {
            $relatedList = explode(';', $result['related_faq_id']);
            $result['related_faq_id'] = [];
            foreach ($relatedList as $key => $related) {
                $relatedFaq = $collection->getItemByColumnValue('faq_id', $related);
                if ($relatedFaq !== null) {
                    $result['related_faq_id'][$related] = $relatedFaq->getData();
                    $result['related_faq_id'][$related]['time'] = $this->date->date('Y M d', $relatedFaq->getTime());
                }
            }
        } else {
            $result['related_faq_id'] = [];
        }
        return $result;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getFaqById($id)
    {
        $collection = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId());
        $faq = $collection->getItemByColumnValue('faq_id', $id);
        if ($faq === null) {
            return null;
        }
        $result = $faq->getData();
        $result['answer'] = $this->filterProvider->getPageFilter()->filter($result['answer']);
        $result['time'] = $this->date->date('Y M d', $faq->getTime());
        if ($result['related_faq_id'] != null) {
            $relatedList = explode(';', $result['related_faq_id']);
            $result['related_faq_id'] = [];
            foreach ($relatedList as $key => $related) {
                $relatedFaq = $collection->getItemByColumnValue('faq_id', $related);
                if ($relatedFaq !== null) {
                    $result['related_faq_id'][$related] = $relatedFaq->getData();
                    $result['related_faq_id'][$related]['time'] = $this->date->date('Y M d', $relatedFaq->getTime());
                }
            }
        } else {
            $result['related_faq_id'] = [];
        }
        return $result;
    }

    /**
     * @param string $url
     * @param int $id
     * @return bool
     */
    public function checkFaqUrl($url, $id)
    {
        return $this->faqsFactory->create()->getCollection()
        ->addFieldToFilter('faq_id', ['neq' => $id])->getItemByColumnValue('url_key', $url) == null;
    }

    /**
     * @param string $url
     * @param int $id
     * @return bool
     */
    public function checkCategoryUrl($url, $id)
    {
        return $this->faqCategoryFactory->create()->getCollection()
        ->addFieldToFilter('faq_category_id', ['neq' => $id])->getItemByColumnValue('url_key', $url) == null;
    }

    /**
     * @param int $cateId
     * @return array
     */
    public function getCategoryImage($cateId)
    {
        $result = [];
        $category = $this->faqCategoryFactory->create()->load($cateId);
        if ($cateId != null) {
            $result['id'] = $cateId;
            if ($category != null) {
                $result['image'] = $category->getData('image');
                $result['color'] = $category->getData('color');
                $result['title_color'] = $category->getData('title_color');
                $result['title'] = $category->getTitle();
            } else {
                $result['image'] = '';
                $result['color'] = '';
                $result['title_color'] = '';
                $result['title'] = '';
            }
        } else {
            $result['id'] = '';
            $result['image'] = '';
            $result['color'] = '';
            $result['title_color'] = '';
            $result['title'] = '';
        }
        return $result;
    }

    /**
     * @param string $url
     * @param string $sort
     * @return mixed|null
     */
    public function getCategoryByUrl($url, $sort)
    {
        $category = $this->faqCategoryFactory->create()->getCollection()->getItemByColumnValue('url_key', $url);
        $faqs = $this->getFaqList(true, false, $sort);
        if ($category != null) {
            $result = $category->getData();
            $result['faq'] = [];
            foreach ($faqs as $faq) {
                if ($faq->getIsShowFullAnswer() == '1') {
                    $faq->setShortAnswer($this->filterProvider->getPageFilter()->filter($faq->getAnswer()));
                }
                $faq->setTime($this->date->date('Y M d', $faq->getTime()));
                if (array_search($faq->getFaqId(), explode(';', $category['faq_id'])) !== false) {
                    $result['faq'][] = $faq->getData();
                }
            }
            return $result;
        } else {
            return null;
        }
    }

    /**
     * @param int $id
     * @param string $sort
     * @return mixed|null
     */
    public function getCategoryById($id, $sort)
    {
        $category = $this->faqCategoryFactory->create()->load($id);
        $faqs = $this->getFaqList(true, false, $sort);
        if ($category != null) {
            $result = $category->getData();
            $result['faq'] = [];
            foreach ($faqs as $faq) {
                if ($faq->getIsShowFullAnswer() == '1') {
                    $faq->setShortAnswer($this->filterProvider->getPageFilter()->filter($faq->getAnswer()));
                }
                $faq->setTime($this->date->date('Y M d', $faq->getTime()));
                if (array_search($faq->getFaqId(), explode(';', $category['faq_id'])) !== false) {
                    $result['faq'][] = $faq->getData();
                }
            }
            return $result;
        } else {
            return null;
        }
    }

    /**
     * @param int $categoryId
     * @return mixed
     */
    public function getFaqByCategory($categoryId)
    {
        return $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
        ->addFieldToFilter('category_id', ['like' => '%' . $categoryId . ';%']);
    }

    /**
     * @param array $tagList
     * @param string $sortBy
     * @return array
     */
    public function getQuestionByTag($tagList, $sortBy)
    {
        $result = [];
        $collection = $this->faqsFactory->create()->getCollection()->addFieldToFilter('category_id', ['neq' => ''])
        ->addFieldToFilter('store_id', $this->getStoreId());
        foreach ($collection as $key => $item) {
            foreach ($tagList as $tagk => $tag) {
                $result[$key] = $item->getData();
                if ($tag != "" && strpos($item->getTag(), $tag) === false) {
                    unset($result[$key]);
                    break;
                }
            }
        }
        $faqSort = $this->getFaqList(true, false, $sortBy);
        $sortResult = [];
        foreach ($faqSort as $key => $value) {
            if (array_key_exists($value->getFaqId(), $result)) {
                $sortResult[] = $value->getData();
            }
        }
        return $sortResult;
    }

    /**
     * @param array $results
     * @return int
     */
    public function countingResult($results)
    {
        $counter = 0;
        foreach ($results as $key => $result) {
            foreach ($result['faq'] as $faq) {
                $counter ++;
            }
        }
        return $counter;
    }

    /**
     * @param array $idList
     * @param array $oldList
     * @param int $faqId
     * @return void
     */
    public function updateCategoryFaqList($idList, $oldList, $faqId)
    {
        $categoryCollection = $this->faqCategoryFactory->create()->getCollection()->getItems();
        $newId = [];
        foreach ($idList as $key => $savingId) {
            if ($savingId != '' && !in_array($savingId, explode(";", $oldList))) {
                $newId[] = $savingId;
            }
        }
        $delId = [];
        foreach (explode(";", $oldList) as $key => $storedId) {
            if ($storedId != '' && !in_array($storedId, $idList)) {
                $delId[] = $storedId;
            }
        }
        foreach ($newId as $key => $addingId) {
            if ($categoryCollection[$addingId]['faq_id'] != "") {
                $faqList = explode(';', $categoryCollection[$addingId]['faq_id']);
                $faqList[] = $faqId;
                $result = implode(';', $faqList);
            } else {
                $result = $faqId;
            }
            $this->faqCategoryResource->updateFaqList($addingId, $result);
        }
        foreach ($delId as $key => $removeId) {
            $faqList = explode(';', $categoryCollection[$removeId]['faq_id']);
            unset($faqList[array_search($faqId, $faqList)]);
            $result = implode(';', $faqList);
            $this->faqCategoryResource->updateFaqList($removeId, $result);
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function checkKeyword($key)
    {
        return strlen($key) > 2;
    }

    /**
     * @param string $keyword
     * @param int $categoryId
     * @param string $sortBy
     * @return array
     */
    public function getSearchData($keyword, $categoryId, $sortBy)
    {
        $result = [];
        $sortResult = [];
        if ($categoryId == '0') {
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('title', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                $result[$value->getFaqId()] = $value->getFaqId();
            }
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('answer', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                $result[$value->getFaqId()] = $value->getFaqId();
            }
        } else {
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('title', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                if (in_array($categoryId, explode(';', $value['category_id']))) {
                    $result[$value->getFaqId()] = $value->getFaqId();
                }
            }
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('answer', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                if (in_array($categoryId, explode(';', $value['category_id']))) {
                    $result[$value->getFaqId()] = $value->getFaqId();
                }
            }
        }
        $faqSort = $this->getFaqList(true, false, $sortBy);
        foreach ($faqSort as $key => $value) {
            if (array_key_exists($value->getFaqId(), $result)) {
                $sortResult[] = $value->getData();
            }
        }
        return $sortResult;
    }

    /**
     * @param string $keyword
     * @param int $categoryId
     * @return array
     */
    public function searchInAnswer($keyword, $categoryId)
    {
        $result = [];
        if ($categoryId == '0') {
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('answer', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                $result[$key] = $value->getData();
            }
        } else {
            $faqs = $this->faqsFactory->create()->getCollection()->addFieldToFilter('store_id', $this->getStoreId())
            ->addFieldToFilter('category_id', ['neq' => ''])
            ->addFieldToFilter('answer', ['like' => '%' . $keyword . '%'])->getItems();
            foreach ($faqs as $key => $value) {
                if (in_array($categoryId, explode(';', $value['category_id']))) {
                    $result[$key] = $value->getData();
                }
            }
        }
        return $result;
    }

    /**
     * @param array $idList
     * @param array $oldList
     * @param int $cateId
     * @return void
     */
    public function updateFaqCategoryList($idList, $oldList, $cateId)
    {
        $faqsCollection = $this->faqsFactory->create()->getCollection()->getItems();
        $newId = [];
        foreach ($idList as $key => $savingId) {
            if ($savingId != '' && !in_array($savingId, explode(";", $oldList))) {
                $newId[] = $savingId;
            }
        }
        $delId = [];
        foreach (explode(";", $oldList) as $key => $storedId) {
            if ($storedId != '' && !in_array($storedId, $idList)) {
                $delId[] = $storedId;
            }
        }
        foreach ($newId as $key => $addingId) {
            if ($faqsCollection[$addingId]['category_id'] != "") {
                $catelist = explode(';', $faqsCollection[$addingId]['category_id']);
                $catelist[] = $cateId;
                $result = implode(';', $catelist);
            } else {
                $result = $cateId;
            }
            $this->faqsResource->updateCategoryList($addingId, $result);
        }
        foreach ($delId as $key => $removeId) {
            $catelist = explode(';', $faqsCollection[$removeId]['category_id']);
            unset($catelist[array_search($cateId, $catelist)]);
            $result = implode(';', $catelist);
            $this->faqsResource->updateCategoryList($removeId, $result);
        }
    }

    /**
     * @param int $delId
     * @return void
     */
    public function deleteFaq($delId)
    {
        $categoryCollection = $this->faqCategoryFactory->create()->getCollection();
        foreach ($categoryCollection as $key => $category) {
            $faqList = explode(';', $category->getFaqId());
            if (array_search($delId, $faqList) !== false) {
                unset($faqList[array_search($delId, $faqList)]);
                $result = implode(';', $faqList);
                $this->faqCategoryResource->updateFaqList($category->getFaqCategoryId(), $result);
            }
        }

        $faqsCollection = $this->faqsFactory->create()->getCollection();
        foreach ($faqsCollection as $key => $faq) {
            $relatedList = explode(';', $faq->getRelatedFaqId());
            if (array_search($delId, $relatedList) !== false) {
                unset($relatedList[array_search($delId, $relatedList)]);
                $result = implode(';', $relatedList);
                $this->faqsResource->updateFaqRelatedList($faq->getFaqId(), $result);
            }
        }
        $this->faqsResource->deleteFaq($delId);
        $this->voteFaqResource->deleteFaqVote($delId);
    }

    /**
     * @param int $delId
     * @return void
     */
    public function deleteFaqCategory($delId)
    {
        $faqsCollection = $this->faqsFactory->create()->getCollection();
        foreach ($faqsCollection as $key => $faq) {
            $cateList = explode(';', $faq->getCategoryId());
            if (array_search($delId, $cateList) !== false) {
                unset($cateList[array_search($delId, $cateList)]);
                $result = implode(';', $cateList);
                $this->faqsResource->updateCategoryList($faq->getFaqId(), $result);
            }
        }
        $this->faqCategoryResource->deleteFaqCategory($delId);
    }

    /**
     * @param string $oldUrl
     * @param \Magento\Framework\Message\Manager $messageManager
     * @return string
     */
    public function saveImage($oldUrl, \Magento\Framework\Message\Manager $messageManager)
    {
        try {
            $fieldName = 'cate_uploader';
            $baseMediaPath = 'bss/FaqCategory/';
            $uploader = $this->uploader->create(['fileId' => $fieldName ]);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $mediaDirectory = $this->filesystem->getDirectoryRead(
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            );
            $result = $uploader->save($mediaDirectory->getAbsolutePath($baseMediaPath));

            $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
            $result['path'] = str_replace('\\', '/', $result['path']);
            $result['url'] = $this->storeManager
                    ->getStore()
                    ->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ) . $this->getFilePath($baseMediaPath, $result['file']);
            $result['name'] = $result['file'];
            $data['bss_image'] = $baseMediaPath.$result['file'];
            $messageManager->addSuccessMessage('Image was uploaded!');
            return  $result['url'];
        } catch (\Exception $e) {
            return $oldUrl;
        }
    }

    /**
     * @param string $path
     * @param string $imageName
     * @return string
     */
    public function getFilePath($path, $imageName)
    {
        return rtrim($path, '/') . '/' . ltrim($imageName, '/');
    }

    /**
     * @param int $faqId
     * @return array
     */
    public function getFaqVoting($faqId)
    {
        $customerId = $this->customerSession->getCustomerId();
        $result = [];
        $result['like'] = 0;
        $result['unlike'] = 0;
        $result['state'] = 0;
        $faq = $this->faqsFactory->create()->load($faqId);
        if ($faq->getUseRealVoteData() == '1') {
            $voteCollection = $this->faqsVote->create()->getCollection()->addFieldToFilter('faq_id', $faqId);
            foreach ($voteCollection as $key => $value) {
                if ($value->getVoteValue() < 0) {
                    $result['unlike']++;
                } elseif ($value->getVoteValue() > 0) {
                    $result['like']++;
                }
                if ($customerId != null && $value->getUserId() == $customerId) {
                    $result['state'] = $value->getVoteValue();
                }
                if ($customerId == null) {
                    $result['state'] = $this->customerSession->getData('bss_faq_vote_' . $faqId);
                }
            }
        } else {
            $result['like'] = $faq->getHelpfulVote();
            $result['unlike'] = $faq->getUnhelpfulVote();
            if ($customerId != null) {
                $vote = $this->faqsVote->create()->getCollection()->addFieldToFilter('faq_id', $faqId)
                ->getItemByColumnValue('user_id', $customerId);
                $result['state'] = $vote->getVoteValue();
            }
            if ($customerId == null) {
                $result['state'] = $this->customerSession->getData('bss_faq_vote_' . $faqId);
            }
        }
        return $result;
    }

    /**
     * @param int $type
     * @param int $faqId
     * @return int
     */
    public function updateVote($type, $faqId)
    {
        $customerId = $this->customerSession->getCustomerId();
        if ($customerId == null) {
            return $this->notLoginVoting($type, $faqId);
        } else {
            $voteRow = $this->faqsVote->create()->getCollection()->addFieldToFilter('faq_id', $faqId)
            ->getItemByColumnValue('user_id', $customerId);
            $faqsInfo = $this->faqsFactory->create()->load($faqId);
            $upVote = $faqsInfo->getHelpfulVote();
            $downVote = $faqsInfo->getUnhelpfulVote();
            if ($voteRow != null) {
                if ($voteRow->getVoteValue() * $type < 0) {
                    $this->voteFaqResource->updateVote($type, $faqId, $customerId);
                    if ($type > 0) {
                        $upVote ++;
                        $downVote --;
                    } else {
                        $upVote --;
                        $downVote ++;
                    }
                    $this->faqsResource->updateVote($faqId, $upVote, $downVote);
                    return $type * 2;
                } else {
                    $this->voteFaqResource->deleteVote($faqId, $customerId);
                    if ($type > 0) {
                        $upVote --;
                    } else {
                        $downVote --;
                    }
                    $this->faqsResource->updateVote($faqId, $upVote, $downVote);
                    return $type * (-1);
                }
            } else {
                $this->voteFaqResource->addVote($type, $faqId, $customerId);
                if ($type > 0) {
                    $upVote ++;
                } else {
                    $downVote ++;
                }
                $this->faqsResource->updateVote($faqId, $upVote, $downVote);
                return $type;
            }
        }
    }

    /**
     * @param int $type
     * @param int $faqId
     * @return int
     */
    public function notLoginVoting($type, $faqId)
    {
        $type = (int)$type;
        if ($this->moduleConfig->isRequireLoginVoting() == '0') {
            $sessionVote = $this->customerSession->getData('bss_faq_vote_' . $faqId);

            $faqsInfo = $this->faqsFactory->create()->load($faqId);
            $upVote = $faqsInfo->getHelpfulVote();
            $downVote = $faqsInfo->getUnhelpfulVote();

            if ($sessionVote == null || $sessionVote == '0') {
                $this->customerSession->setData('bss_faq_vote_' . $faqId, $type);
                if ($type > 0) {
                    $upVote ++;
                } else {
                    $downVote ++;
                }

                $voteRow = $this->faqsVote->create()->getCollection()
                ->addFieldToFilter('faq_id', $faqId)
                ->getItemByColumnValue('user_id', -1 + $type);
                $voteNumber = ($voteRow) ? $voteRow->getVoteValue() : 0;
                $voteNumber += $type;

                $this->faqsResource->updateVote($faqId, $upVote, $downVote);
                if ($voteRow != null) {
                    $this->voteFaqResource->updateVote($voteNumber, $faqId, -1 + $type);
                } else {
                    $this->voteFaqResource->addVote($voteNumber, $faqId, -1 + $type);
                }
                return $type;
            } else {
                if ($sessionVote * $type < 0) {
                    $this->customerSession->setData('bss_faq_vote_' . $faqId, $type);

                    $upVoteRow = $this->faqsVote->create()->getCollection()
                    ->addFieldToFilter('faq_id', $faqId)
                    ->getItemByColumnValue('user_id', '0');

                    $downVoteRow = $this->faqsVote->create()->getCollection()
                    ->addFieldToFilter('faq_id', $faqId)
                    ->getItemByColumnValue('user_id', '-2');

                    $upVoteNumber = ($upVoteRow) ? $upVoteRow->getVoteValue() : 0;
                    $downVoteNumber = ($downVoteRow) ? $downVoteRow->getVoteValue() : 0;

                    $upVote += $type;
                    $downVote -= $type;
                    $upVoteNumber += $type;
                    $downVoteNumber += $type;

                    $this->faqsResource->updateVote($faqId, $upVote, $downVote);

                    if ($upVoteRow != null) {
                        $this->voteFaqResource->updateVote($upVoteNumber, $faqId, '0');
                    } else {
                        $this->voteFaqResource->addVote($upVoteNumber, $faqId, '0');
                    }
                    if ($downVoteRow != null) {
                        $this->voteFaqResource->updateVote($downVoteNumber, $faqId, '-2');
                    } else {
                        $this->voteFaqResource->addVote($downVoteNumber, $faqId, '-2');
                    }
                    return $type;
                } else {
                    $this->customerSession->setData('bss_faq_vote_' . $faqId, '0');
                    if ($type > 0) {
                        $upVote --;
                    } else {
                        $downVote --;
                    }
                    $this->faqsResource->updateVote($faqId, $upVote, $downVote);

                    $voteRow = $this->faqsVote->create()->getCollection()
                    ->addFieldToFilter('faq_id', $faqId)
                    ->getItemByColumnValue('user_id', -1 + $type);
                    $voteNumber = ($voteRow) ? $voteRow->getVoteValue() : 0;
                    $voteNumber -= $type;

                    $this->voteFaqResource->updateVote($voteNumber, $faqId, -1 + $type);
                    return $type * (-1);
                }
            }
        } else {
            return 0;
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function getProductFaqData($id)
    {
        $result = ['user' => $this->customerSession->getCustomer()->getEmail(), 'faq' => []];
        $faqsCollection = $this->faqsFactory->create()->getCollection()
        ->addFieldToFilter('category_id', ['neq' => ''])
        ->addFieldToFilter('store_id', $this->getStoreId())->getItems();
        foreach ($faqsCollection as $key => $faq) {
            if ($faq->getIsCheckAllProduct() == '1') {
                $result['faq'][$key] = $faq->getData();
            } else {
                if (in_array($id, explode(';', $faq->getProductId()))) {
                    $result['faq'][$key] = $faq->getData();
                }
            }
        }
        return $result;
    }

    /**
     * @param string $question
     * @param int $productId
     * @param string $customer
     * @param string $time
     * @return \Magento\Framework\Phrase
     */
    public function submitNewQuestion($question, $productId, $customer, $time)
    {
        $data = [
            'title' => $question,
            'product_id' => $productId,
            'customer' => $customer,
            'answer' => __('This question is not answered!'),
            'time' => $time,
            'is_most_frequently' => 0,
            'store_id' => $this->getStoreId(),
            'is_check_all_product' => 0,
            'category_id' => ''
        ];
        $this->faqsResource->addFaq($data);
        return __('Thanks for your submit');
    }
}
