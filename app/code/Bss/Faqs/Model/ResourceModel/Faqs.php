<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Faqs extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init('bss_faqs', 'faq_id');
    }

    /**
     * @param array $data
     * @return int
     */
    public function saveFaq($data)
    {
        unset($data['form_key']);
        if ($data['faq_id'] == "") {
            $result = $this->addFaq($data);
        } else {
            $result = $this->updateFaq($data);
        }
        return $result;
    }

    /**
     * @param array $data
     * @return int
     */
    public function addFaq($data)
    {
        $connection = $this->getConnection();
        unset($data['faq_id']);
        $connection->insert($this->getMainTable(), $data);
        return $connection->lastInsertId($this->getMainTable());
    }

    /**
     * @param array $data
     * @return int
     */
    public function updateFaq($data)
    {
        $connection = $this->getConnection();
        $idClause = $data['faq_id'];
        unset($data['faq_id']);
        $connection->update($this->getMainTable(), $data, ['faq_id=?' => $idClause]);
        return $idClause;
    }

    /**
     * @param int $faqId
     * @param int $cateId
     * @return void
     */
    public function updateCategoryList($faqId, $cateId)
    {
        $connection = $this->getConnection();
        $connection->update($this->getMainTable(), ['category_id' => $cateId], ['faq_id=?' => $faqId]);
    }

    /**
     * @param int $faqId
     * @param array $rList
     * @return void
     */
    public function updateFaqRelatedList($faqId, $rList)
    {
        $connection = $this->getConnection();
        $connection->update($this->getMainTable(), ['related_faq_id' => $rList], ['faq_id=?' => $faqId]);
    }

    /**
     * @param int $faqId
     * @return void
     */
    public function deleteFaq($faqId)
    {
        $this->getConnection()->delete($this->getMainTable(), ['faq_id=?' => $faqId]);
    }

    /**
     * @param int $faqId
     * @param int $up
     * @param int $down
     * @return void
     */
    public function updateVote($faqId, $up, $down)
    {
        $connection = $this->getConnection();
        if ($up != '-1' && $down != '-1') {
            $connection->update(
                $this->getMainTable(),
                ['helpful_vote' => $up, 'unhelpful_vote' => $down],
                ['faq_id=?' => $faqId]
            );
        }
    }
}
