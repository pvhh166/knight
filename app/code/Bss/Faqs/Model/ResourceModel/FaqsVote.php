<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class FaqsVote extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init('bss_faqs_voting', 'vote_id');
    }

    /**
     * @param int $type
     * @param int $faqId
     * @param int $customerId
     * @return void
     */
    public function addVote($type, $faqId, $customerId)
    {
        $connection = $this->getConnection();
        $connection->insert(
            $this->getMainTable(),
            ['vote_value' => $type, 'faq_id' => $faqId, 'user_id' => $customerId]
        );
    }

    /**
     * @param int $type
     * @param int $faqId
     * @param int $customerId
     * @return void
     */
    public function updateVote($type, $faqId, $customerId)
    {
        $connection = $this->getConnection();
        $connection->update(
            $this->getMainTable(),
            ['vote_value' => $type],
            ['faq_id=?' => $faqId, 'user_id=?' => $customerId]
        );
    }

    /**
     * @param int $faqId
     * @param int $customerId
     * @return void
     */
    public function deleteVote($faqId, $customerId)
    {
        $this->getConnection()->delete($this->getMainTable(), ['faq_id=?' => $faqId, 'user_id=?' => $customerId]);
    }

    /**
     * @param int $faqId
     * @return void
     */
    public function deleteFaqVote($faqId)
    {
        $this->getConnection()->delete($this->getMainTable(), ['faq_id=?' => $faqId]);
    }
}
