<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Faqs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class FaqCategory extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init('bss_faq_category', 'faq_category_id');
    }

    /**
     * @param array $data
     * @return int
     */
    public function saveFaqCategory($data)
    {
        if ($data['faq_category_id'] == "") {
            $result = $this->addFaqCategory($data);
        } else {
            $result = $this->updateFaqCategory($data);
        }
        return $result;
    }

    /**
     * @param array $data
     * @return int
     */
    public function addFaqCategory($data)
    {
        $connection = $this->getConnection();
        unset($data['faq_category_id']);
        $connection->insert($this->getMainTable(), $data);
        return $connection->lastInsertId($this->getMainTable());
    }

    /**
     * @param array $data
     * @return int
     */
    public function updateFaqCategory($data)
    {
        $connection = $this->getConnection();
        $idClause = $data['faq_category_id'];
        unset($data['faq_category_id']);
        $connection->update($this->getMainTable(), $data, ['faq_category_id=?' => $idClause]);
        return $idClause;
    }

    /**
     * @param int $cateId
     * @param int $faqId
     * @return void
     */
    public function updateFaqList($cateId, $faqId)
    {
        $connection = $this->getConnection();
        $connection->update($this->getMainTable(), ['faq_id' => $faqId], ['faq_category_id=?' => $cateId]);
    }

    /**
     * @param int $faqId
     * @return void
     */
    public function deleteFaqCategory($faqId)
    {
        $connection = $this->getConnection();
        $connection->delete($this->getMainTable(), ['faq_category_id=?' => $faqId]);
        $connection->update('bss_faqs', ['category_id' => '0'], ['category_id=?' => $faqId]);
    }
}
