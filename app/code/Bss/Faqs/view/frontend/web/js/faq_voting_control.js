/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Faqs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

define([
    'jquery',
    'underscore'
], function ($, _) {
    'use strict';

    $.widget('bss.faq_search', {
        _create: function () {
            var $widget = this;
            $widget._updateVotingData();
            $widget.element.on('click', '.vote_button.like', function () {
                return $widget._like();
            });
            $widget.element.on('click', '.vote_button.unlike', function () {
                return $widget._unLike();
            });
        },
        _updateVotingData: function () {
            $.ajax({
                type: 'post',
                url: this.options.baseUrl + 'faqs/json/voteview',
                data: {
                    'faqId': this.options.faqId,
                    'form_key': $.cookie('form_key')
                },
                dataType: 'json'
            }).done(function ($vote) {
                $('.vote_button.like .counter').html($vote['like']);
                $('.vote_button.unlike .counter').html($vote['unlike']);
                if ($vote['state'] > 0) {
                    $('.vote_button.like').addClass('active');
                } else if ($vote['state'] < 0) {
                    $('.vote_button.unlike').addClass('active');
                }
            });
        },
        _like: function () {
            var $widget = this;
            $.ajax({
                type: 'post',
                url: this.options.baseUrl + 'faqs/json/vote',
                data: {
                    'type': '1',
                    'faqId': $widget.options.faqId,
                    'form_key': $.cookie('form_key')
                },
                dataType: 'json'
            }).done(function ($vote) {
                if ($vote != null) {
                    $('.vote_button.like .counter').html($vote['like']);
                    $('.vote_button.unlike .counter').html($vote['unlike']);
                    if ($vote['state'] > 0) {
                        $('.vote_button.like').addClass('active');
                        $('.vote_button.unlike').removeClass('active');
                        $('.faq_vote_notify').attr('id', 'helpful').html($widget.options.likeNotify);
                    } else {
                        $('.vote_button').removeClass('active');
                        $('.faq_vote_notify').attr('id', '').html('');
                    }
                } else {
                    $('.faq_vote_notify').attr('id', 'warning').html($widget.options.loginRequiredNotify);
                }
            });
        },
        _unLike: function () {
            var $widget = this;
            $.ajax({
                type: 'post',
                url: this.options.baseUrl + 'faqs/json/vote',
                data: {
                    'type': '-1',
                    'faqId': $widget.options.faqId,
                    'form_key': $.cookie('form_key')
                },
                dataType: 'json'
            }).done(function ($vote) {
                if ($vote != null) {
                    $('.vote_button.like .counter').html($vote['like']);
                    $('.vote_button.unlike .counter').html($vote['unlike']);
                    if ($vote['state'] < 0) {
                        $('.vote_button.like').removeClass('active');
                        $('.vote_button.unlike').addClass('active');
                        $('.faq_vote_notify').attr('id', 'unhelpful').html($widget.options.unlikeNotify);
                    } else {
                        $('.vote_button').removeClass('active');
                        $('.faq_vote_notify').attr('id', '').html('');
                    }
                } else {
                    $('.faq_vote_notify').attr('id', 'warning').html($widget.options.loginRequiredNotify);
                }
            });
        }
    });
    return $.bss.faq_search;
});
