<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_AdvancedHidePrice
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedHidePrice\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade Data script
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * // @codingStandardsIgnoreFile
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    protected $resourceConfig;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Bss\AdvancedHidePrice\Model\ResourceModel\ConvertData
     */
    protected $convertData;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * UpgradeData constructor.
     * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resourceConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Bss\AdvancedHidePrice\Model\ResourceModel\ConvertData $collection
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface  $resourceConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Bss\AdvancedHidePrice\Model\ResourceModel\ConvertData $convertData,
        \Magento\Framework\Serialize\SerializerInterface $serializer
    ) {
        $this->resourceConfig = $resourceConfig;
        $this->storeManager = $storeManager;
        $this->convertData = $convertData;
        $this->serializer = $serializer;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.5', '<')) {
            $oldConfigData = $this->convertData->convertOldData();
            if (!empty($oldConfigData)) {
                foreach ($oldConfigData as $result) {
                    if (preg_match('/^((s|i|d|b|a|O|C):|N;)/', $result['value'])) {
                        $convert =$this->serializer->serialize(unserialize($result['value']));
                        $sql = "UPDATE ".'core_config_data'." SET value ='".$convert."' WHERE config_id = ".$result['config_id'];
                        $this->convertData->convertOldDataToNew($sql);
                    }
                }
            }
        }
        $setup->endSetup();
    }
}
