/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_OneStepCheckout
 * @author    Extension Team
 * @copyright Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */

define([
    'jquery',
    'Magento_Checkout/js/view/summary/item/details',
    'mage/translate',
    'ko',
    'underscore',
    'Magento_Customer/js/customer-data',
    'Bss_OneStepCheckout/js/action/update-item',
    'Magento_Checkout/js/model/quote',
    'Brandsmith_RewardPoints/js/action/set-discount',
    'mage/url'
], function ($, Component, $t, ko, _, customerData, updateItemAction, quote, setDiscountAction, url) {
    'use strict';

    var isApplied = ko.observable(null);
    return Component.extend({
        defaults: {
            template: 'Bss_OneStepCheckout/summary/item/details'
        },

        titleQtyBox: ko.observable($t('Qty')),

        /**
         * @param {Object} item
         * @returns void
         */
        updateQty: function (item) {
            updateItemAction(item).done(
                function (response) {
                    var totals = response.totals,
                        data = JSON.parse(this.data),
                        itemId = data.itemId,
                        itemsOrigin = [],
                        quoteItemData = window.checkoutConfig.quoteItemData;
                    if (!response.status) {
                        var originItem = _.find(quoteItemData, function (index) {
                            return index.item_id == itemId;
                        });
                        $.each(totals.items, function(index) {
                            if (this.item_id == originItem.item_id) {
                                this.qty = originItem.qty;
                            }
                            itemsOrigin[index] = this;
                        });
                        totals.items = itemsOrigin;
                    } else {
                        customerData.reload('cart');
                    }
                    //console.log(totals);
                    //quote.setTotals(totals);

                    if(response.reward_percent.length > 0) {
                        if($('#block-reward').length) {
                            $('#block-reward').show();
                            setDiscountAction("discount", isApplied);
                        }else {
                            window.location.replace(url.build('onestepcheckout.html'));
                        }
                    }else {
                        //$('#block-reward').hide();
                        //setDiscountAction("reward", isApplied);
                        window.location.replace(url.build('onestepcheckout.html'));
                    }
                    $('.wccf_price_label').html(response.reward_percent);
                }
            );
        },

        /**
         * @param {*} itemId
         * @returns {String}
         */
        getProductUrl: function (itemId) {
            if (_.isUndefined(customerData.get('cart')())) {
                customerData.reload('cart');
            }
            var productUrl = 'javascript:void(0)',
                cartData = customerData.get('cart')(),
                items = cartData.items;

            var item = _.find(items, function (item) {
                return item.item_id == itemId;
            });

            if (!_.isUndefined(item) && item.product_has_url) {
                productUrl = item.product_url;
            }
            return productUrl;
        },

        /**
         * @param {*} itemId
         * @returns {String}
         */
        getType: function (itemId) {
            if (_.isUndefined(customerData.get('cart')())) {
                customerData.reload('cart');
            }
            var productType = 'default',
                cartData = customerData.get('cart')(),
                items = cartData.items;

            var item = _.find(items, function (item) {
                return item.item_id == itemId;
            });

            if (!_.isUndefined(item)) {
                productType = item.product_type;
                // console.log(item);
            }
            return productType;
        },
        /**
         * @param {*} itemId
         * @returns {String}
         */
        getSku: function (itemId) {
            if (_.isUndefined(customerData.get('cart')())) {
                customerData.reload('cart');
            }
            var productSku = '',
                cartData = customerData.get('cart')(),
                items = cartData.items;

            var item = _.find(items, function (item) {
                return item.item_id == itemId;
            });

            if (!_.isUndefined(item)) {
                productSku = item.product_sku;
            }
            return productSku;
        },
        getOptionsSuper: function (parent) {
            //console.log(parent.options);
            if (_.isUndefined(customerData.get('cart')())) {
                customerData.reload('cart');
            }
            var productType = 'default',
                cartData = customerData.get('cart')(),
                items = cartData.items;

            var item = _.find(items, function (item) {
                return item.item_id == parent.item_id;
            });

            if (!_.isUndefined(item)) {
                productType = item.product_type;
            }
            if(productType == 'super') {
                var options = item.options;
                return options;
            }
        }
    });
});
