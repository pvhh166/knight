<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\OneStepCheckout\Model;

use Bss\OneStepCheckout\Api\UpdateItemManagementInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Bss\OneStepCheckout\Api\Data\UpdateItemDetailsInterfaceFactory;
use Magento\Quote\Api\ShippingMethodManagementInterface;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Framework\Escaper;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class UpdateItemManagement
 *
 * @package Bss\OneStepCheckout\Model
 */
class UpdateItemManagement implements UpdateItemManagementInterface
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var UpdateItemDetailsInterfaceFactory
     */
    private $updateItemDetails;

    /**
     * @var ShippingMethodManagementInterface
     */
    private $shippingMethodManagement;

    /**
     * @var PaymentMethodManagementInterface
     */
    private $paymentMethodManagement;

    /**
     * @var CartTotalRepositoryInterface
     */
    private $cartTotalRepository;

    /**
     * @var Escaper
     */
    private $escaper;

    private $ruleValidator;

    private $storeManager;

    private $earningRuleFactory;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @param CartRepositoryInterface $cartRepository
     * @param UpdateItemDetailsInterfaceFactory $updateItemDetails
     * @param ShippingMethodManagementInterface $shippingMethodManagement
     * @param PaymentMethodManagementInterface $paymentMethodManagement
     * @param CartTotalRepositoryInterface $cartTotalRepository
     * @param Escaper $escaper
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        UpdateItemDetailsInterfaceFactory $updateItemDetails,
        ShippingMethodManagementInterface $shippingMethodManagement,
        PaymentMethodManagementInterface $paymentMethodManagement,
        CartTotalRepositoryInterface $cartTotalRepository,
        Escaper $escaper,
        \Brandsmith\RewardPoints\Model\Validator $ruleValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Brandsmith\RewardPoints\Model\Rule\EarningFactory $earningRuleFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->cartRepository = $cartRepository;
        $this->updateItemDetails = $updateItemDetails;
        $this->shippingMethodManagement = $shippingMethodManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartTotalRepository = $cartTotalRepository;
        $this->escaper = $escaper;
        $this->ruleValidator = $ruleValidator;
        $this->storeManager = $storeManager;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * {@inheritdoc}
     */
    public function update($cartId, \Magento\Quote\Api\Data\EstimateAddressInterface $address, $itemId, $qty)
    {
        $message = '';
        $status = false;
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);
        if (!$quote) {
            throw new NoSuchEntityException(__('This quote does not exist.'));
        }
        $quoteItem = $quote->getItemById($itemId);
        if (!$quoteItem) {
            throw new NoSuchEntityException(__('We can\'t find the quote item.'));
        }
        try {
            if (!$qty || $qty <= 0) {
                $quote->removeItem($itemId);
                $this->cartRepository->save($quote);
                $status = true;
                $message = __(
                    '%1 was removed in your shopping cart.',
                    $this->escaper->escapeHtml($quoteItem->getProduct()->getName())
                );
            } else {
                $quoteItem->setQty($qty);
                if ($quoteItem->getHasError()) {
                    throw new CouldNotSaveException(__($quoteItem->getMessage()));
                } else {
                    $quoteItem->save();
                    $status = true;
                    $message = __(
                        '%1 was updated in your shopping cart.',
                        $this->escaper->escapeHtml($quoteItem->getProduct()->getName())
                    );
                }
            }
            /** Update Reward points **/
            $quote->collectTotals();
            $amount = 0;
            $rewardPercent = "";
            $store = $this->storeManager->getStore($quote->getStoreId());

            $this->ruleValidator->setCustomerGroupId($quote->getCustomer()->getGroupId())
                ->setWebsiteId($store->getWebsiteId());

            $items = $quote->getItems();
            $ids = [];
            foreach ($items as $item) {
                $ids = array_merge($ids, $this->ruleValidator->process($item, $quote));
            }
            $ids = array_unique($ids);

            if(count($ids)) { // If Subtotal still match to receive reward point
                foreach ($ids as $ruleId) {
                    // @var \Brandsmith\RewardPoints\Model\Rule\Earning $rule
                    $rule = $this->earningRuleFactory->create()->load($ruleId);
                    $name = $rule->getName();
                    $percent = explode("%",$name)[0];
                    $rewardPercent = "-".$percent."%";
                }
                $this->cartRepository->save($quote);
            }else {
                $quote->setRewardPointsAmount(0);
                $this->cartRepository->save($quote->collectTotals());
                //$this->checkoutSession->replaceQuote($quote);
            }

            /** End **/
            

        } catch (LocalizedException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('We can\'t update the item right now.'));
        }
        return $this->getUpdateCartDetails($quote, $address, $cartId, $message, $status, $rewardPercent);
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\EstimateAddressInterface $address
     * @param int $quoteId
     * @param string $message
     * @param bool $status
     * @return \Bss\OneStepCheckout\Api\Data\UpdateItemDetailsInterface
     */
    private function getUpdateCartDetails($quote, $address, $quoteId, $message, $status, $rewardPercent)
    {
        $cartDetails = $this->updateItemDetails->create();
        $paymentMethods = $this->paymentMethodManagement->getList($quoteId);
        $totals = $this->cartTotalRepository->get($quoteId);
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress && $shippingAddress->getCustomerAddressId()) {
            $shippingMethods = $this->shippingMethodManagement->estimateByAddressId(
                $quoteId,
                $shippingAddress->getCustomerAddressId()
            );
        } else {
            $shippingMethods = $this->shippingMethodManagement->estimateByAddress($quoteId, $address);
        }
        $cartDetails->setShippingMethods($shippingMethods);
        $cartDetails->setPaymentMethods($paymentMethods);
        $cartDetails->setTotals($totals);
        $cartDetails->setMessage($message);
        $cartDetails->setStatus($status);
        $cartDetails->setRewardPercent($rewardPercent);

        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            $cartDetails->setHasError(true);
        }

        return $cartDetails;
    }
}
