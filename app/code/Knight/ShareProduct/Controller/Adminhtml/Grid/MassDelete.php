<?php

namespace Knight\ShareProduct\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Knight\ShareProduct\Model\ResourceModel\Shareproduct\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Knight\ShareProduct\Model\ShareproductFactory;
use Knight\ShareProduct\Model\ShareproductdetailFactory;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    protected $modelshareproduct;
    protected $modelshareproductdetail;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ShareproductFactory $modelshareproduct,
        ShareproductdetailFactory $modelshareproductdetail
    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->modelshareproduct = $modelshareproduct;
        $this->modelshareproductdetail = $modelshareproductdetail;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $item) {
            $model = $this->modelshareproduct->create();
            $model->setId($item->getShareproductId())->delete();
            $modeldetail = $this->modelshareproductdetail->create();
            $modeldetail->setId($item->getShareproductId())->delete();
        }
        
        $this->messageManager->addSuccessMessage(__('A total of %1 element(s) have been deleted.', $collectionSize));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}