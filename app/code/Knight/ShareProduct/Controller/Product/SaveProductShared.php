<?php
namespace Knight\ShareProduct\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

class SaveProductShared extends \Magento\Framework\App\Action\Action
{
    protected $_customerSession;
    protected $modelsaveshareproduct;
    protected $modelsaveshareproductdetail;
    protected $modelshareproduct;
    protected $sharehelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Magento\Customer\Model\Session $customerSession,
        \Knight\ShareProduct\Model\ShareproductFactory $modelsaveshareproduct,
        \Knight\ShareProduct\Model\ShareproductdetailFactory $modelsaveshareproductdetail,
        \Knight\ShareProduct\Model\Shareproduct $modelshareproduct,
        \Magento\Framework\Pricing\Helper\Data $helperPricing,
        \Knight\ShareProduct\Helper\Data $sharehelper
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_customerSession = $customerSession;
        $this->modelsaveshareproduct = $modelsaveshareproduct;
        $this->modelsaveshareproductdetail = $modelsaveshareproductdetail;
        $this->modelshareproduct = $modelshareproduct;
        $this->_helperPricing = $helperPricing;
        $this->sharehelper = $sharehelper;
    }

    
    public function execute()
    {
        $_param = $this->getRequest()->getParams();
        if($_param['validate'] == 0 || isset($_param['super_attributes']) && empty($_param['super_attributes']) ) {
            $_product_collection = $this->updateArrayInArray($_param['product_collection']);

            $message = __(  "Please select required option(s)" );

            $_response = array (
                'errors' => true,
                'message' => $message,
                'product_require' => $_product_collection
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }

        $_super_product_id = $_param['super_product_id'];
        $_super_product_attributes = $_param['super_attributes'];
        $_super_product_addon_qty = '';

        $_super_product_image = ($_param['super_product_img']) ? $_param['super_product_img'] : '';
        if(!empty($_param['addon_qty'])) {
            $_super_product_addon_qty = $_param['addon_qty'];
        }

        $_super_product = $this->_productRepository->getById($_super_product_id);

        $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);
        $customer_id = $this->getCustomerId();

        foreach($_param as $key => $data) {
            if($key == 'super_attributes') {
                foreach ($_param[$key] as $_i => $_value) {
                    $_productAddOn = $_super_product->getTypeInstance()->getProductAddOn($_super_product_id, $_value['product_id']);
                    $_productSku = null;
                    if( !empty($_productAddOn->getData()) ) {
                        //  Get Add-on Information
                        $_addon_data = $_productAddOn->getData();
                        $_productName = $_addon_data['name'];

                        if(!empty($_super_product_addon_qty)) {
                            if (array_key_exists($_value['product_id'], $_super_product_addon_qty)) {
                                $_productQuantity = $_super_product_addon_qty[$_value['product_id']];
                            }
                        }

                        $_productSku = $_addon_data['sku'];
                    } else {
                        //  Get Base Information
                        $_baseProduct = $_super_product->getTypeInstance()->getBaseProductById($_super_product_id, $_value['product_id']);
                        $_productName = $_baseProduct->getName();
                        if(!empty($_super_product_addon_qty)) {
                            if (array_key_exists($_value['product_id'], $_super_product_addon_qty)) {
                                $_productQuantity = $_super_product_addon_qty[$_value['product_id']];
                            }
                        }
                        $_productSku = $_baseProduct->getSku();
                    }

                    // Update Sku Simple product
                    $_simpleSkuForParam = $this->setSimpleSkuForParam($_value, $_getChildProduct);
                    $_imageForAddOn = $this->setImageForAddOn($_value, $_getChildProduct);
                    $_param[$key][$_i]['product_name'] = $_productName;
                    $_param[$key][$_i]['product_sku'] = $_productSku;
                    $_param[$key][$_i]['product_simple_sku'] = $_simpleSkuForParam;
                    $_param[$key][$_i]['product_thumbnail_img'] = $_imageForAddOn;
                    $_param[$key][$_i]['product_quantity'] = $_productQuantity;
                }
            }
        }

        //if ($customer_id != null) {
        try{
            $modelsaveshareproduct = $this->modelsaveshareproduct->create()
                ->setShareproductName($_super_product->getProductUrl())
                ->setCustomerId($customer_id)
                ->setShareImage($_super_product_image)
                ->setShareAction('N/A')
                ->setCreatedAt(date('Y-m-d H:i:s'))
                ->save();

            $modelsaveshareproductdetail = $this->modelsaveshareproductdetail->create()
                ->setShareproductId($modelsaveshareproduct->getId())
                ->setShareproductConfigPrdData($this->serializer->serialize($_param))
                ->save();

            $sharepd_link = $_super_product->getProductUrl().'?share='.$modelsaveshareproduct->getId();
            if($modelsaveshareproduct->getId()){
                $modelsaveshareproductupdate = $this->modelshareproduct->load($modelsaveshareproduct->getId());
                $modelsaveshareproductupdate->setShareproductName($sharepd_link);
                $modelsaveshareproductupdate->save();
            }


            $sharepd_link = $_super_product->getProductUrl().'?share='.$modelsaveshareproduct->getId();
            $_response = array (
                'errors' => false,
                'sharepd_id' => $modelsaveshareproduct->getId(),
                'sharepd_link' => $sharepd_link,
                'product_name' => $_super_product->getName(),
                'sharepd_id' => $modelsaveshareproduct->getId(),
                'product_image' => $_super_product_image,
                'message' => "Generate successfully"
            );

            if($this->sharehelper->isFacebookEnable()){
                $_response['link_facbook'] = $sharepd_link;
            }

            if($this->sharehelper->isTwitterEnable()){
                $_response['link_twitter'] = 'http://twitter.com/intent/tweet?&url='. urlencode($sharepd_link);
            }

            if($this->sharehelper->isLinkedinEnable()){
                $_response['link_linkedin'] = 'https://www.linkedin.com/sharing/share-offsite/?url='. urlencode($sharepd_link);
            }

            if($this->sharehelper->isPinterestEnable()){
                $_response['link_pinterest'] = 'https://pinterest.com/pin/create/button/?url='. urlencode($sharepd_link) .'&media='.urlencode($_super_product_image).'&description='. urlencode($_super_product->getName());
            }

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        } catch (\Exception $e) {
            $_response['errors'] = array (
                'errors' => false,
                'message' => $e->getMessage()
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }
        //} else {
            // $_response = array (
            //     'errors' => true,
            //     'message' => "You must have login to share this product"
            // );

            // $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            // $resultJson->setData($_response);
            // return $resultJson;
        //}
    }

    public function updateArrayInArray($_array)
    {
        $_newArray = array();

        foreach ($_array as $_key => $_value) {
            $_newArray['product_id_'.$_key] = $_value['product_id'];
        }

        return array_unique($_newArray);
    }

    public function getCustomerId() {
        return $this->_customerSession->getCustomer()->getId();
    }

    public function setSimpleSkuForParam($_productInfo, $_getChildProduct)
    {
        $_simpleSku = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_simpleSku = $_item['simple_product_sku'];
            }
        }

        return $_simpleSku;
    }

     public function setImageForAddOn($_productInfo, $_getChildProduct)
    {
        $_thumbnailImg = null;
        foreach ($_getChildProduct as $_item) {
            if($_item['default_product_id'] == $_productInfo['product_id']) {
                $_thumbnailImg = $_item['product_thumbnail_img'];
            }
        }
        return $_thumbnailImg;
    }
}