<?php
namespace Knight\ShareProduct\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

class UpdateAction extends \Magento\Framework\App\Action\Action
{
	protected $modelshareproduct;
	protected $_customerSession;
    protected $modelsaveshareproduct;
    protected $modelsaveshareproductdetail;

	public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Magento\Customer\Model\Session $customerSession,
        \Knight\ShareProduct\Model\ShareproductFactory $modelsaveshareproduct,
        \Knight\ShareProduct\Model\ShareproductdetailFactory $modelsaveshareproductdetail,
        \Magento\Framework\Pricing\Helper\Data $helperPricing,
        \Knight\ShareProduct\Model\Shareproduct $modelshareproduct
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_customerSession = $customerSession;
        $this->modelsaveshareproduct = $modelsaveshareproduct;
        $this->modelsaveshareproductdetail = $modelsaveshareproductdetail;
        $this->modelshareproduct = $modelshareproduct;
        $this->_helperPricing = $helperPricing;
    }

    
	public function execute()
    {
    	$_param = $this->getRequest()->getParams();

    	if(isset($_param['type']) && isset($_param['shareid'])){
    		$shareid = $_param['shareid'];
            $model = $this->modelshareproduct->load($shareid);
            $sharedatatype = $model->getShareAction();
            $pos = strpos($sharedatatype , $_param['type']);

            if ($pos === false) {
                $shareaction = (!empty($sharedatatype) && $sharedatatype != 'N/A')? $sharedatatype . ', '. $_param['type'] : $_param['type'];
                $model->setShareAction($shareaction);
                $model->save();
            }
    	}
    }
}