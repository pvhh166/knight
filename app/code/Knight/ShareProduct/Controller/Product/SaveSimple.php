<?php
namespace Knight\ShareProduct\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

class SaveSimple extends \Magento\Framework\App\Action\Action
{
    protected $_customerSession;
    protected $modelsaveshareproduct;
    protected $modelsaveshareproductdetail;
    protected $imageHelperFactory;
    protected $modelshareproduct;
    protected $sharehelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $ProductRepository,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProTypeModel,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $modelConfigurableType,
        \Magento\Customer\Model\Session $customerSession,
        \Knight\ShareProduct\Model\ShareproductFactory $modelsaveshareproduct,
        \Knight\ShareProduct\Model\ShareproductdetailFactory $modelsaveshareproductdetail,
        \Magento\Framework\Pricing\Helper\Data $helperPricing,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Knight\ShareProduct\Model\Shareproduct $modelshareproduct,
        \Knight\ShareProduct\Helper\Data $sharehelper
    ) {
        parent::__construct($context);
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_productRepository = $ProductRepository;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->_quote = $quote;
        $this->_cartItemFactory = $cartItemFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->_dateTimes = $dateTime;
        $this->_modelConfigurableType = $modelConfigurableType;
        $this->_customerSession = $customerSession;
        $this->modelsaveshareproduct = $modelsaveshareproduct;
        $this->modelsaveshareproductdetail = $modelsaveshareproductdetail;
        $this->_helperPricing = $helperPricing;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->modelshareproduct = $modelshareproduct;
        $this->sharehelper = $sharehelper;
    }

    
    public function execute()
    {
        $_param = $this->getRequest()->getParams();
        $customer_id = $this->getCustomerId();

        //if ($customer_id != null) {
        $_product = $this->_productRepository->getById($_param['product']);
        $prdUrl = $_product->getProductUrl();
        $_param['product_url'] = $prdUrl;
        $_param['product_type'] = $_product->getTypeId();

        if(!isset($_param['qty']) && empty($_param['qty'])){
            $_param['qty'] = 1;
        }

        $product_image = $this->imageHelperFactory->create()
                              ->init($_product, 'product_page_image_medium')->getUrl();
        if($_param['product_type'] == "configurable"){
            $associateProduct = $this->sharehelper->getAssociateProduct($_param['product'],$_param['super_attribute']);
            $product_image = $this->imageHelperFactory->create()
                              ->init($associateProduct, 'product_page_image_medium')->getUrl();
        }
     
        try{
            $modelsaveshareproduct = $this->modelsaveshareproduct->create()
                ->setShareproductName($prdUrl)
                ->setCustomerId($customer_id)
                ->setShareImage($product_image)
                ->setShareAction('N/A')
                ->setCreatedAt(date('Y-m-d H:i:s'))
                ->save();

            $modelsaveshareproductdetail = $this->modelsaveshareproductdetail->create()
                ->setShareproductId($modelsaveshareproduct->getId())
                ->setShareproductConfigPrdData($this->serializer->serialize($_param))
                ->save();

            $_response = array (
                'errors' => false,
                'sharepd_id' => $modelsaveshareproduct->getId(),
                'sharepd_link' => $_param['product_url'].'?share='.$modelsaveshareproduct->getId(),
                'product_name' => $_product->getName(),
                'sharepd_id' => $modelsaveshareproduct->getId(),
                'product_image' => $product_image,
                'message' => "Generate successfully"
            );
            $sharepd_link = $_param['product_url'].'?share='.$modelsaveshareproduct->getId();
            
            if($modelsaveshareproduct->getId()){
                $modelshareproductupdate = $this->modelshareproduct->load($modelsaveshareproduct->getId());
                $modelshareproductupdate->setShareproductName($sharepd_link);
                $modelshareproductupdate->save();
            }

            if($this->sharehelper->isFacebookEnable()){
                $_response['link_facbook'] = $sharepd_link;
            }

            if($this->sharehelper->isTwitterEnable()){
                $_response['link_twitter'] = 'http://twitter.com/intent/tweet?text='. urlencode($_product->getName()) .'&url='. urlencode($sharepd_link);
            }

            if($this->sharehelper->isLinkedinEnable()){
                $_response['link_linkedin'] = 'https://www.linkedin.com/sharing/share-offsite/?url='. urlencode($sharepd_link);
            }

            if($this->sharehelper->isPinterestEnable()){
                $_response['link_pinterest'] = 'https://pinterest.com/pin/create/button/?url='. urlencode($sharepd_link) .'&media='.urlencode($product_image).'&description='. urlencode($_product->getName());
            }

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        } catch (\Exception $e) {
            $_response['errors'] = array (
                'errors' => false,
                'message' => $e->getMessage()
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }
        // } else {
        //     $_response = array (
        //         'errors' => true,
        //         'message' => "You have to login to share this product"
        //     );
            
        //     $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        //     $resultJson->setData($_response);
        //     return $resultJson;
        // }
    }

    public function updateArrayInArray($_array)
    {
        $_newArray = array();

        foreach ($_array as $_key => $_value) {
            $_newArray['product_id_'.$_key] = $_value['product_id'];
        }

        return array_unique($_newArray);
    }

    public function getCustomerId() {
        return $this->_customerSession->getCustomer()->getId();
    }
}