<?php
namespace Knight\ShareProduct\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Knight\ShareProduct\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
class Sendmail extends \Magento\Framework\App\Action\Action
{
    protected $transportBuilder;
    protected $scopeConfig;
    protected $helper;
    protected $modelsaveshareproduct;
    protected $modelsaveshareproductdetail;

    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        Data $helper,
        \Knight\ShareProduct\Model\Shareproduct $modelsaveshareproduct,
        \Knight\ShareProduct\Model\Shareproductdetail $modelsaveshareproductdetail,
        ScopeConfigInterface $scopeConfig)
    {
        $this->transportBuilder = $transportBuilder;
        $this->helper= $helper;
        $this->modelsaveshareproduct = $modelsaveshareproduct;
        $this->modelsaveshareproductdetail = $modelsaveshareproductdetail;
        $this->scopeConfig=$scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $param = $this->_request->getParams();
        $_response = array (
            'errors' => false
        );
        if(isset($param['sharepd_id']) && $param['sharepd_id']){
            $shareproduct_config_prd_data = $this->helper->getSuperCustomOption($param['sharepd_id'],null);
        }
        try{
            $postObject = [
                'sender_name' => $param['your-name'],
                'recipient_name' => $param['recipient-name'],
                'message' => $param['your-comment'],
                'product_url' => $param['product_url'],
                'product_name' => $param['product_name'],
                'sharepd_id' => $param['sharepd_id']
            ];

            $sender = $this->scopeConfig->getValue('shareproduct/general/emailsender',ScopeInterface::SCOPE_STORE);
            $option = ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => 1];
            $emailTemplateId = $this->scopeConfig->getValue('shareproduct/general/templateemail',ScopeInterface::SCOPE_STORE);

            $transport = $this->transportBuilder
                ->setTemplateIdentifier($emailTemplateId)
                ->setTemplateOptions($option)
                ->setTemplateVars($postObject)
                ->setFrom($sender)
                ->addTo($param['recipient-email'])
                ->getTransport();
            $message = "Product link email has been sent successfully.";
            $transport->sendMessage();
            $_response['errors'] = false;
            $_response['message'] = $message;

            if(isset($param['sharepd_id'])){

                $modelsaveshareproduct = $this->modelsaveshareproduct->load($param['sharepd_id']);
                $shareAct = $modelsaveshareproduct->getShareAction();
                
                $type = 'Email';
                $maillog = $this->addData($shareAct, $type);
                if($maillog){
                    $modelsaveshareproduct->setShareAction($maillog);
                }

                $recipient_name = $modelsaveshareproduct->getRecipientName();
                $recipient_namelog = $this->addData($recipient_name, $param['recipient-name']);

                if($recipient_namelog){
                    $modelsaveshareproduct->setRecipientName($recipient_namelog);
                }

                $recipient_email = $modelsaveshareproduct->getRecipientEmail();
                $recipient_emaillog = $this->addData($recipient_email, $param['recipient-email']);
                
                if($recipient_emaillog){
                    $modelsaveshareproduct->setRecipientEmail($recipient_emaillog);
                }

                $message_email = $modelsaveshareproduct->getSentMessage();
                $message_emaillog = $this->addData($message_email, $param['your-comment']);
                
                if($message_emaillog){
                    $modelsaveshareproduct->setSentMessage($message_emaillog);
                }

                $modelsaveshareproduct->setSentName($param['your-name']);
                $modelsaveshareproduct->setSentEmail($param['your-email']);
                $modelsaveshareproduct->save();
            }
            
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;

        } catch (\Exception $e) {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $om->get('Psr\Log\LoggerInterface');
            $storeManager->info('Send Mail Error :'.$e->getMessage());
            $_response['errors'] = array (
                'errors' => false,
                'message' => $e->getMessage()
            );
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($_response);
            return $resultJson;
        }
    }

    public function addData($currentdata, $newdata){
        $pos = strpos($currentdata , $newdata);
        $shareaction = false;
        if ($pos === false) {
            $shareaction = (!empty($currentdata) && $currentdata != 'N/A')? $currentdata . ', '.$newdata : $newdata;
        }

        return $shareaction;
    }
}