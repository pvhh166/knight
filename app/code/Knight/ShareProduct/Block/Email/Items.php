<?php
namespace Knight\ShareProduct\Block\Email;

class Items extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	protected $_registry;
	protected $_helper;
	public $_storeManager;
	public $_scopeConfig;

	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\Registry $registry,
		\Knight\ShareProduct\Helper\Data $helper,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
		$this->_customerSession = $customerSession;
		$this->_registry = $registry;
		$this->_helper = $helper;
		$this->_storeManager = $storeManager;
	}
	
}