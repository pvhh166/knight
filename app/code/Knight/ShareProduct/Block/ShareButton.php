<?php
namespace Knight\ShareProduct\Block;

use Magento\Store\Model\ScopeInterface;


class ShareButton extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	protected $_registry;
	protected $_helper;
	public $_storeManager;
	public $_scopeConfig;

	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\Registry $registry,
		\Knight\ShareProduct\Helper\Data $helper,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
		$this->_customerSession = $customerSession;
		$this->_registry = $registry;
		$this->_helper = $helper;
		$this->_storeManager = $storeManager;
	}

    public function getCustomerId()
    {
        return $this->_customerSession->getCustomer()->getId();
    }

    public function isEnable()
    {
		return $this->_helper->IsActive();
	}

	public function socialShareEnable()
    {
		return $this->_helper->IsActive();
	}

	public function getCurrentProduct()
	{
        return $this->_registry->registry('current_product'); 
	}

	public function getStoreurl()
	{
		return $this->_storeManager->getStore()->getBaseUrl();
	}

	public function getProductType()
	{
		return $this->getCurrentProduct()->getTypeId();
	}

	public function getImageDownload()
    {
        return $this->getUrl('superproduct/index/image');
    }

    public function isFacebookEnable()
    {
       return $this->_helper->isFacebookEnable();
    }

    public function isTwitterEnable()
    {
       return $this->_helper->isTwitterEnable();
    }

    public function isLinkedinEnable()
    {
       return $this->_helper->isLinkedinEnable();
    }

    public function isGoogleEnable()
    {
       return $this->_helper->isLinkedinEnable();
    }

    public function isPinterestEnable()
    {
       return $this->_helper->isPinterestEnable();
    }

    public function getFbAppId(){
        return $this->_helper->getFbAppId();
    }
}

