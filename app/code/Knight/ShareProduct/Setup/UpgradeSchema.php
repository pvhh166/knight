<?php
namespace Knight\ShareProduct\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	 public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$installer = $setup;
        $setup->startSetup();

        $version = $context->getVersion();
        $connection = $setup->getConnection();

        if (version_compare($version, '1.0.2') < 0) {
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'sent_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Sent Name',
                ]
            );
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'sent_email',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Sent Email',
                ]
            );
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'recipient_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Recipient Name',
                ]
            );
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'recipient_email',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Recipient Email',
                ]
            );
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'sent_message',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Sent Message',
                ]
            );
        }

        if (version_compare($version, '1.0.3') < 0) {
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'share_action',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Share Actions',
                ]
            );
        }

        if (version_compare($version, '1.0.4') < 0) {
            $connection->addColumn(
                $setup->getTable('shareproduct'),
                'share_image',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Share Image',
                ]
            );
        }
    }
}