<?php
namespace Knight\ShareProduct\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('shareproduct'))
            ->addColumn(
                'shareproduct_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true],
                'Share Product ID'
            )
            ->addColumn(
                'shareproduct_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Product Name'
            )
            ->addColumn(
                'customer_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Customer Id'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            );
        $installer->getConnection()->createTable($table);
        
        $table = $installer->getConnection()
            ->newTable($installer->getTable('shareproduct_detail'))
            ->addColumn(
                'shareproduct_detail_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true],
                'Share Product Detail ID'
            )
            ->addColumn(
                'shareproduct_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [],
                'Share Product Id'
            )
            ->addColumn('shareproduct_config_prd_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1000,
                [],
                'Share Configurable Product Data'
            );
        $installer->getConnection()->createTable($table);
    }
}