<?php
namespace Knight\ShareProduct\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Model\Session;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $cartsession;
    protected $modelsaveshareproduct;
    protected $modelsaveshareproductdetail;
    protected $_ruleCollectionFactory;
    protected $_productRepository;
    protected $_productloader;
    /**
     * @var \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory
     */
    protected $_productruleCollectionFactory;

    public function __construct(
        Context $context, 
        Session $session,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProTypeModel,
        \Knight\ShareProduct\Model\ShareproductFactory $modelsaveshareproduct,
        \Knight\ShareProduct\Model\ShareproductdetailFactory $modelsaveshareproductdetail,
        \Brandsmith\SuperProduct\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Brandsmith\SuperProduct\Model\ResourceModel\Productrule\CollectionFactory $productruleCollectionFactory
    )
    {
        $this->cartsession = $session;
        $this->_productRepository = $productRepository;
        $this->_productloader = $_productloader;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->modelsaveshareproduct = $modelsaveshareproduct;
        $this->modelsaveshareproductdetail = $modelsaveshareproductdetail;
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
        $this->_productruleCollectionFactory = $productruleCollectionFactory;
        parent::__construct($context);
    }

    public function IsActive()
    {
        return $this->scopeConfig->getValue('shareproduct/general/enable', ScopeInterface::SCOPE_STORE);    
    }
    
    public function isFacebookEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_facebook/enable', ScopeInterface::SCOPE_STORE);
    }

    public function isTwitterEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_twitter/enable', ScopeInterface::SCOPE_STORE);
    }

    public function isLinkedinEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_linkedin/enable', ScopeInterface::SCOPE_STORE);
    }

    public function isGoogleEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_google/enable', ScopeInterface::SCOPE_STORE);
    }

    public function isPinterestEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_pinterest/enable', ScopeInterface::SCOPE_STORE);
    }

    public function getFbAppId()
    {
        return $this->scopeConfig->getValue('shareproduct/sc_facebook/appid', ScopeInterface::SCOPE_STORE); 
    }

    public function IsCartWithSimpleItems()
    {
        $visitems = $this->cartsession->getQuote()->getAllVisibleItems();
        foreach($visitems as $item)
        {
            if($item->getProductType() != 'simple')
            {
                return 0;
            }
        }
        return 1;
    }

    public function getEmailTemplate()
    {
        return $this->scopeConfig->getValue('shareproduct/general/templateemail', ScopeInterface::SCOPE_STORE);
    }
    public function getEmailSender()
    {
        return $this->scopeConfig->getValue('shareproduct/general/sender_email_identity', ScopeInterface::SCOPE_STORE);
    }
    public function generateRandomString()
    {
        try{
            $randomString  = substr(str_shuffle("0123456789"), 0, 3);
            return $randomString;
        }
        catch(\Exception $e) {
        }
    }

    public function getCampainIsEnable()
    {
        return $this->scopeConfig->getValue('shareproduct/campains_gta/enable_campains', ScopeInterface::SCOPE_STORE);
    }

    public function getCampainScript()
    {
        return $this->scopeConfig->getValue('shareproduct/campains_gta/campains_script', ScopeInterface::SCOPE_STORE);
    }

    public function getAssociateProduct($parentId , $selectedOpt){
        $product = $this->_productRepository->getById($parentId);
        $associateProduct = $this->_configurableProTypeModel->getProductByAttributes($selectedOpt, $product);
        return $associateProduct;
    }

    public function getSuperCustomOption($_itemId, $_productId)
    {
        
        $shareProductCollection = $this->modelsaveshareproductdetail->create()
                                        ->getCollection()
                                        ->addFieldToFilter('shareproduct_id',$_itemId);
        $shareproduct_config_prd_data = $shareProductCollection->getFirstItem()->getData('shareproduct_config_prd_data');
        $shareproduct_config_prd_data = json_decode($shareproduct_config_prd_data, true);

        return $shareproduct_config_prd_data;
    }

    public function getChildSuperAttributes($_itemId, $_productId)
    {
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_productId);
        $_superAttributes = $_superCustomOption['super_attributes'];
        
        $_optionSelected = null;
        
        if(is_array($_superAttributes) && $_superAttributes ){
            foreach ($_superAttributes as $_itemAttributes) {
                if($_itemAttributes['product_id'] == $_productId) {
                    $_optionSelected = $_itemAttributes['option_selected'];
                }
            }
        }
        
        
        return $_optionSelected;
    }

    public function getChildSuperAttributesDisable($_itemId, $_productId) {
        $shareproduct_config_prd_data = $this->getSuperCustomOption($_itemId, $_productId);
        $_optionDisableSelect = [];
        if( array_key_exists('super_attributes_disable', $shareproduct_config_prd_data) ) {
            $data_disable =  $shareproduct_config_prd_data['super_attributes_disable'];
            if(isset($data_disable) && $data_disable){
                foreach ($data_disable as $_itemAttributes) {
                    
                    if($_itemAttributes['productId'] == $_productId) {
                        foreach ($_itemAttributes['options_disable'] as $items) {
                            array_push($_optionDisableSelect, $items);
                        }
                    }
                }
            }
        }
        

        
        return $_optionDisableSelect;
    }

    public function getChildSuperSimple($_itemId, $_childProductId)
    {
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_childProductId);
        $_superAttributes = $_superCustomOption['super_attributes'];
        $_simpleSelected = false;
        if(is_array($_superCustomOption) && $_superCustomOption){
            foreach ($_superAttributes as $_itemAttributes) {
                if($_itemAttributes['product_id'] == $_childProductId) {
                    $_simpleSelected = true;
                }
            }
        }

        return $_simpleSelected;
    }

    public function getProductChosenRuleDetail( $_itemId, $_superProductId, $_skuDependency)
    {
        $condition = 1;
        $_productRuleDetail = $this->getCheckProductRule($_superProductId, $_skuDependency, $condition);
        $_productRuleDetailData['rule'] = false;
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
        
        if(count($_productRuleDetail) > 0) {
            // Return only one Rule
            if(count($_productRuleDetail) < 2) {
                $_productRuleDetailFirstItem = $_productRuleDetail->getFirstItem();
                $_duplicateRule = $_productRuleDetailFirstItem->getDuplicateRule();
                if($_duplicateRule) {
                    $_productRuleDetailData['rule'] = false;
                    return $_productRuleDetailData;
                }

                $_productId = $_productRuleDetailFirstItem->getProductId();
                $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);

                $_superAttributes = $_superCustomOption['super_attributes'];
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_productRuleDetailData['rule'] = true;
                    }
                }
            }

            // Return more than one Rule
            if(count($_productRuleDetail) > 1) {
                $_productRuleCollection = $_productRuleDetail->getData();
                foreach($_productRuleCollection as $_productRule) {
                    $_productId = $_productRule['product_id'];
                    $_sku = $_productRule['sku'];
                    $_duplicateRule = $this->getCheckDuplicateRule($_superProductId, $_sku, $_skuDependency);
                    if($_duplicateRule) {
                        $_productRuleDetailData['rule'] = false;
                        return $_productRuleDetailData;
                    }
                    $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                    $_superAttributes = $_superCustomOption['super_attributes'];
                    
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_productRuleDetailData['rule'] = true;
                        }
                    }
                }
            }
        }

        return $_productRuleDetailData;
    }

    public function getCheckProductRule($_superProductId, $_skuDependency, $condition)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_skuDependency);
        $collection->addFieldToFilter('condition', $condition);
        $collection->load();
        return $collection;
    }

    public function getChildSuperCustomQuantity($_itemId, $_childProductId)
    {
        $_superCustomOption = $this->getSuperCustomOption($_itemId, $_childProductId);
        $_customQuantity = false;
        if(is_array($_superCustomOption) && $_superCustomOption){
            if( !array_key_exists('addon_qty', $_superCustomOption) ) {
                return false;
            }

            
            $_superAttribute = $_superCustomOption['addon_qty'];
            foreach ($_superAttribute as $key => $value) {
                if($key == $_childProductId) {
                    $_customQuantity = $value;
                }
            }
        }
        
        return $_customQuantity;
    }

    public function getCheckDuplicateRule($_superProductId, $_sku, $_skuDependency)
    {
        $collection = $this->_productruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku', $_skuDependency);
        $collection->addFieldToFilter('sku_dep', $_sku);
        $collection->load();
        return count($collection) > 0 ? true : false;
    }

    public function getProductNotChosenRuleDetail($_itemId, $_superProductId, $_skuDependency)
    {
        $condition = 0;
        $_productRuleDetail = $this->getCheckProductRule($_superProductId, $_skuDependency, $condition);

        $_productRuleDetailData['rule'] = false;

        if(count($_productRuleDetail) > 0) {
            // Return only one Rule
            if(count($_productRuleDetail) < 2) {
                $_productRuleDetailFirstItem = $_productRuleDetail->getFirstItem();
                $_productId = $_productRuleDetailFirstItem->getProductId();
                $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                $_superAttributes = $_superCustomOption['super_attributes'];
                
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_productRuleDetailData['rule'] = true;
                    }
                }
            }

            // Return more than one Rule
            if(count($_productRuleDetail) > 1) {
                $_productRuleCollection = $_productRuleDetail->getData();
                foreach($_productRuleCollection as $_productRule) {
                    $_productId = $_productRule['product_id'];
                    $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                    

                    $_superAttributes = $_superCustomOption['super_attributes'];
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_productRuleDetailData['rule'] = true;
                        }
                    }
                }
            }
        }

        return $_productRuleDetailData;
    }

    public function checkAttributeRule($_itemId, $_superProductId, $_productSkuDep,$_attributeCodeDep,$_optionIdDep=null   
    )
    {
        $_productSku = $_attributeCode = $_optionId = null;
        $_attributesRule = $this->getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId);
        $_attributesRuleData = $_attributesRule->getData();
        
        $_rule = false;
        if( !empty($_attributesRuleData) ) {
            
            foreach ($_attributesRuleData as $_attributesRuleItem) {
                $_productSku = $_attributesRuleItem['sku'];
                $_product = $this->getProductBySku($_productSku);
                $_productId = $_product->getId();
                $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);
                $_superAttributes = $_superCustomOption['super_attributes'];
                foreach ($_superAttributes as $_itemAttributes) {
                    if($_itemAttributes['product_id'] == $_productId) {
                        $_optionSelected = $_itemAttributes['option_selected'];
                        $_attributeCode = $_attributesRuleItem['attribute'];
                        if( array_key_exists($_attributeCode, $_optionSelected) ) {
                            if(isset($_optionSelected[$_attributeCode])) {
                                if($_attributesRuleItem['option'] == $_optionSelected[$_attributeCode] && $_attributesRuleItem['condition_dep'] != 1) {
                                    $_rule = true;
                                }

                                if($_attributesRuleItem['option'] != $_optionSelected[$_attributeCode]) {
                                    $_ruleWithConditionWithoutOptionId = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, null, $_productSku, $_attributeCode, null, 1);
                                    if(count($_ruleWithConditionWithoutOptionId->getData()) > 1) {
                                        $_ruleWithCondition = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, null, $_productSku, $_attributeCode, $_attributesRuleItem['option'], 1);
                                        $_ruleWithConditionFullRule = $this->getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionSelected[$_attributeCode], 1);
                                        if(!empty($_ruleWithCondition->getData()) && count($_ruleWithConditionFullRule->getData()) != 1 ) {
                                            $_rule = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if( empty($_attributesRuleData) ) {
            $_optionIdDep = $_productSku = $_attributeCode = $_optionId = null;
            $_attributesRule = $this->getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId);
            $_attributesRuleData = $_attributesRule->getData();

            if( !empty($_attributesRuleData) ) {
                foreach ($_attributesRuleData as $_attributesRuleItem) {
                    $_productSku = $_attributesRuleItem['sku'];
                    $_product = $this->getProductBySku($_productSku);
                    $_productId = $_product->getId();
                    $_superCustomOption = $this->getSuperCustomOption($_itemId, $_superProductId);

                    $_superAttributes = $_superCustomOption['super_attributes'];
                    foreach ($_superAttributes as $_itemAttributes) {
                        if($_itemAttributes['product_id'] == $_productId) {
                            $_optionSelected = $_itemAttributes['option_selected'];
                            if(isset($_optionSelected[$_attributeCodeDep])) {
                                if($_optionSelected[$_attributeCodeDep] == $_attributesRuleItem['option']) {
                                    $_rule = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $_rule;
    }

    public function getAttributesRule($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId)
    {
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_productSkuDep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCodeDep);
        if($_optionIdDep) $collection->addFieldToFilter('option_dep', $_optionIdDep);
        if($_productSku) $collection->addFieldToFilter('sku', $_productSku);
        if($_attributeCode) $collection->addFieldToFilter('attribute', $_attributeCode);
        if($_optionId) $collection->addFieldToFilter('option', $_optionId);

        $collection->load();
        
        return $collection;
    }

    public function getProductBySku($sku)
    {
        try {
            $_product = $this->_productRepository->get($sku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $_product = false;
        }
        return $_product;
    }

    public function getAttributesRuleWithCondition($_superProductId, $_productSkuDep, $_attributeCodeDep, $_optionIdDep, $_productSku, $_attributeCode, $_optionId, $_conditionDep)
    {
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_productSkuDep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCodeDep);
        if($_optionIdDep) $collection->addFieldToFilter('option_dep', $_optionIdDep);
        if($_productSku) $collection->addFieldToFilter('sku', $_productSku);
        if($_attributeCode) $collection->addFieldToFilter('attribute', $_attributeCode);
        if($_optionId) $collection->addFieldToFilter('option', $_optionId);
        $collection->addFieldToFilter('condition_dep', $_conditionDep);

        $collection->load();
        return $collection;
    }
    public function checkAttributeCodehasInRule($_superProductId,$_sku_dep,$_attributeCode,$_optionIdDep=null){
        $collection = $this->_ruleCollectionFactory->create();
        $collection->addFieldToFilter('super_product_id', $_superProductId);
        $collection->addFieldToFilter('sku_dep', $_sku_dep);
        $collection->addFieldToFilter('attribute_dep', $_attributeCode);
        if($_optionIdDep){
            $collection->addFieldToFilter('option_dep', $_optionIdDep);
        }

        $collection->load();
        return (count($collection)> 0)? true : false;
    }
    
    public function getTotalSpPrice($paramshareId)
    {
        $_param = $this->getSuperCustomOption($paramshareId, null);
        $_super_product_id = $_param['super_product_id'];
        $_super_product_qty = 1;
        $_super_product_attributes = $_param['super_attributes'];
        $_super_product_addon_qty = '';
        if(!empty($_param['addon_qty'])) {
            $_super_product_addon_qty = $_param['addon_qty'];
        }

        $_super_product = $this->_productRepository->getById($_super_product_id);
        $_getChildProduct = $_super_product->getTypeInstance()->getChildProduct($_super_product_id, $_super_product_attributes);

        $_totalCustomPrice = 0;
        foreach ($_getChildProduct as $_item) {
            $_addon_qty = null;
            $_item_id = $_item['simple_product_id'];
            $_default_product_id = $_item['default_product_id'];
            // Count Total Qty
            $_default_qty = $_item['default_qty'];
            if(!empty($_super_product_addon_qty)) {
                if (array_key_exists($_default_product_id, $_super_product_addon_qty)) {
                    $_addon_qty = $_super_product_addon_qty[$_default_product_id];
                }
            }

            if($_addon_qty != null) {
                $_total_qty = $_addon_qty;
            } else {
                $_total_qty = $_default_qty;
            }
            $_product = $this->_productloader->create()->load($_item_id);
            $_product_final_price = min($_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),$_product->getPriceModel()->getFinalPrice($_super_product_qty*$_total_qty,$_product));
            $_totalCustomPrice += $_product_final_price*$_total_qty;
        }
        return $_totalCustomPrice;
    }
}