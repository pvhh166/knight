<?php
namespace Knight\ShareProduct\Model;

class Shareproduct extends \Magento\Framework\Model\AbstractModel
{
	public function _construct() {
        $this->_init('Knight\ShareProduct\Model\ResourceModel\Shareproduct');
    }      
}