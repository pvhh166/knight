<?php
namespace Knight\ShareProduct\Model\ResourceModel\Shareproduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected $_idFieldName = 'shareproduct_id';
	
	protected function _construct()
	{
        $this->_init('Knight\ShareProduct\Model\Shareproduct', 'Knight\ShareProduct\Model\ResourceModel\Shareproduct');
    }     
}
