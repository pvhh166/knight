<?php
namespace Knight\ShareProduct\Model\ResourceModel\Shareproductdetail;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected function _construct()
	{
        $this->_init('Knight\ShareProduct\Model\Shareproductdetail','Knight\ShareProduct\Model\ResourceModel\Shareproductdetail');
    }     
}
