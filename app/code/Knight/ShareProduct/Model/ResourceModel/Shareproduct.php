<?php
namespace Knight\ShareProduct\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Shareproduct extends AbstractDb
{
	protected function _construct()
	{
         $this->_init('shareproduct','shareproduct_id');
    }     
}