<?php
namespace Knight\ShareProduct\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Shareproductdetail extends AbstractDb
{
	protected function _construct()
	{
         $this->_init('shareproduct_detail','shareproduct_detail_id');
    }     
}
