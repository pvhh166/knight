<?php
namespace Knight\FixDuplicateOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderPlacebefore implements ObserverInterface
{
    protected $checkoutSession;


    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();

        $order = $observer->getEvent()->getOrder();
        $quoteID = $order->getQuoteId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();  
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('sales_order');
        $selectedTime = date('Y-m-d h:i:s');
        $endTime = strtotime("-600 seconds", strtotime($selectedTime));
        $last15Sec = date('Y-m-d h:i:s', $endTime);

        $sql = "SELECT entity_id,increment_id,status FROM `".$tableName."` WHERE `quote_id` = ".$quoteID." and `created_at` >= '$last15Sec'";
        $result = $connection->fetchRow($sql);
        if($result){
            $this->checkoutSession->setLastQuoteId($quoteID);
            $this->checkoutSession->setLastSuccessQuoteId($quoteID);
            $this->checkoutSession->setLastOrderId($result['entity_id']);
            $this->checkoutSession->setLastRealOrderId($result['increment_id']);
            $this->checkoutSession->setLastOrderStatus($result['status']);
            exit();
        }
    }
}