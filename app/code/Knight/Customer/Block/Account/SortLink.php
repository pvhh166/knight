<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Knight\Customer\Block\Account;

/**
 * Class for sortable links.
 */
class SortLink extends \Magento\Customer\Block\Account\SortLink implements SortLinkInterface
{
    /**
     * {@inheritdoc}
     */
    public function getClassIcon()
    {
        return $this->getData(self::CLASSICON);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        $highlight = '';

        if ($this->getIsHighlighted()) {
            $highlight = ' current';
        }

        $classIcon = 'fas fa-circle fontawesomeicon';
		if ($this->getClassIcon()) {
            $classIcon = $this->getClassIcon();
        }

        if ($this->isCurrent()) {
            $html = '<li class="col-lg-3 col-md-3 col-sm-3 current">';
            $html .= '<div class="loginPanel">';
            $html .= '<div class="loginIcon"><i class="'.$classIcon.'"></i></div>';

            $html .= '<strong>'
                . $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getLabel()))
                . '</strong>'; 
            $html .= '</div></li>';
        } else {
            $html = '<li class="col-lg-3 col-md-3 col-sm-3' . $highlight . '"><div class="loginPanel"><a href="' . $this->escapeHtml($this->getHref()) . '"';

            
            $html .= $this->getTitle()
                ? ' title="' . $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getTitle())) . '"'
                : '';
            $html .= $this->getAttributesHtml() . '>';

            if ($this->getIsHighlighted()) {
                $html .= '<strong>';
            }
            $html .= '<div class="loginIcon"><i class="'.$classIcon.'"></i></div>';
            $html .= $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getLabel()));

            if ($this->getIsHighlighted()) {
                $html .= '</strong>';
            }

            $html .= '</a></div></li>';
        }

        return $html;
    }
}
