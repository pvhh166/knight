<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Knight\Customer\Block\Account;

/**
 * Interface for sortable links.
 * @api
 * @since 100.2.0
 */
interface SortLinkInterface
{
    /**#@+
     * Constant for confirmation status
     */
    const CLASSICON = 'classIcon';
    /**#@-*/

    /**
     * Get sort order for block.
     *
     * @return int
     * @since 100.2.0
     */
    public function getSortOrder();
    /**
     * Get sort order for block.
     *
     * @return int
     * @since 100.2.0
     */
    public function getClassIcon();
}
