<?php

declare(strict_types=1);

namespace Knight\HomeDelivery\Model\Address;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Eav\Api\AttributeOptionManagementInterface;

/**
 * Provides customer address data.
 */
class CustomAttributesProcessor
{
	public function filterNotVisibleAttributes(array $attributes): array
    {
        $attributesMetadata = $this->addressMetadata->getAllAttributesMetadata();
        foreach ($attributesMetadata as $attributeMetadata) {
            if (!$attributeMetadata->isVisible()) {
                unset($attributes[$attributeMetadata->getAttributeCode()]);
            }
        }

        return $this->setLabelsForAttributes($attributes);
    }
}