<?php

namespace Knight\HomeDelivery\Model\Data;

/**
 * Description of Address
 *
 * @author Japhet Perez
 */
class Address extends \Magento\Customer\Model\Data\Address implements \Knight\HomeDelivery\Api\Data\AddressInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function getHomeDelivery()
    {
        return $this->_get('home_delivery');
    }

    /**
     * {@inheritdoc}
     */
    public function setHomeDelivery($homeDelivery)
    {
        return $this->setData('home_delivery', $homeDelivery);
    }

    public function getHomeDeliveryEmail()
    {
        return $this->_get('home_delivery_email');
    }

    public function setHomeDeliveryEmail($homeDeliveryEmail)
    {
        return $this->setData('home_delivery_email', $homeDeliveryEmail);
    }

    public function getHomeDeliveryMobile()
    {
        return $this->_get('home_delivery_mobile');
    }

    public function setHomeDeliveryMobile($homeDeliveryMobile)
    {
        return $this->setData('home_delivery_mobile', $homeDeliveryMobile);
    }
    
}