<?php
namespace Knight\HomeDelivery\Api\Data;

use Magento\Customer\Api\Data\AddressInterface as MainAddressInterface;

/**
 * Description of AddressInterface
 *
 * @author Japhet Perez
 */
interface AddressInterface extends MainAddressInterface
{
    const DELIVERY_HOME = 'delivery_home';
    const DELIVERY_HOME_EMAIL = 'delivery_home_email';
    const DELIVERY_HOME_MOBILE = 'delivery_home_mobile';
    
    /*
     * Get Address Reference
     * @return string
     */
    public function getHomeDelivery();
    
    /*
     * Set Address Reference
     * @param string $addressReference
     * @return $this
     */
    public function setHomeDelivery($homeDelivery);

    /*
     * Get Address Reference
     * @return string
     */
    public function getHomeDeliveryEmail();
    
    /*
     * Set Address Reference
     * @param string $addressReference
     * @return $this
     */
    public function setHomeDeliveryEmail($homeDeliveryEmail);

    /*
     * Get Address Reference
     * @return string
     */
    public function getHomeDeliveryMobile();
    
    /*
     * Set Address Reference
     * @param string $addressReference
     * @return $this
     */
    public function setHomeDeliveryMobile($homeDeliveryMobile);
}