define([
    'jquery',
    'ko',
    'underscore',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Bss_OneStepCheckout/js/model/payment-service-default',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/action/select-billing-address',
    'Bss_OneStepCheckout/js/model/shipping-save-processor/payload-extender',  // fix 2.2.0
    'Magento_Customer/js/model/customer' // fix 2.1.x
], function(
     $,
    ko,
    _,
    quote,
    resourceUrlManager,
    storage,
    paymentService,
    methodConverter,
    errorProcessor,
    selectBillingAddressAction,
    payloadExtender,
    customer
) {
    'use strict';

    return {
        /**
         * @return {jQuery.Deferred}
         */
        saveShippingInformation: function () {
            var payload;

            var delivery_home = $('[name="custom_attributes[delivery_home]"]').is(":checked") ? 1 : 0,
                delivery_home_email = $('[name="custom_attributes[delivery_home_email]"]').val();

            if (!quote.billingAddress()) {
                selectBillingAddressAction(quote.shippingAddress());
            }

            var billingAddress = quote.billingAddress();

            if (!customer.isLoggedIn()) {
                if (billingAddress) {
                    if (!_.isUndefined(billingAddress.street)) {
                        if (billingAddress.street.length == 0) {
                            delete billingAddress.street;
                        }
                    } else {
                        delete billingAddress.street;
                    }
                }
            }

            payload = {
                addressInformation: {
                    'shipping_address': quote.shippingAddress(),
                    'billing_address': quote.billingAddress(),
                    'shipping_method_code': quote.shippingMethod()['method_code'],
                    'shipping_carrier_code': quote.shippingMethod()['carrier_code'],
                    'extension_attributes': {
                        delivery_home: delivery_home,
                        delivery_home_email: delivery_home_email
                    }
                }
            };
            
            if ($('#shipping #shipping_arrival_date').length || $('#opc-shipping_method #shipping_arrival_date').length) {
                payload.addressInformation['extension_attributes']['shipping_arrival_date'] = $('#shipping_arrival_date').val();
            }
            if ($('#shipping #shipping_arrival_comments').length || $('#opc-shipping_method #shipping_arrival_comments').length) {
                payload.addressInformation['extension_attributes']['shipping_arrival_comments'] = $('#shipping_arrival_comments').val();
            }
            if ($('#shipping #delivery_time_slot').length || $('#opc-shipping_method #delivery_time_slot').length) {
                payload.addressInformation['extension_attributes']['delivery_time_slot'] = $('#delivery_time_slot').val();
            }

            return storage.post(
                resourceUrlManager.getUrlForSetShippingInformation(quote),
                JSON.stringify(payload)
            ).done(
                function (response) {
                    quote.setTotals(response.totals);
                    paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                }
            );
        }
    };
});