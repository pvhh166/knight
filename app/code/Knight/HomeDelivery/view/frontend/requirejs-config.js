var config = {
    "map": {
        "*": {
            "Magento_Checkout/js/model/shipping-save-processor/default" : "Knight_HomeDelivery/js/shipping-save-processor",
            "Bss_OneStepCheckout/js/model/shipping-save-processor/default": "Knight_HomeDelivery/js/shipping-save-processor",
            "Bss_OneStepCheckout/js/view/shipping" : "Knight_HomeDelivery/js/view/shipping"
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Knight_HomeDelivery/js/action/set-shipping-information-mixin' : true
            },
            'Magento_Checkout/js/view/shipping': {
                'Knight_HomeDelivery/js/view/shipping': true
            }
        }
    }
};