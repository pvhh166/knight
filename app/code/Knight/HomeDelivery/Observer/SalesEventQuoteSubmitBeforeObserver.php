<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Knight\HomeDelivery\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class SalesEventQuoteSubmitBeforeObserver implements ObserverInterface
{
    /**
     * Set gift messages to order from quote address
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();

        $order = $observer->getEvent()->getOrder();

        $order->setData('delivery_home', $quote->getShippingAddress()->getDeliveryHome());
        $order->setData('delivery_home_email', $quote->getShippingAddress()->getDeliveryHomeEmail());
        $order->setData('delivery_home_mobile', $quote->getShippingAddress()->getDeliveryHomeMobile());
        // save to sales order address
        $order->getShippingAddress()->setData('delivery_home', $quote->getShippingAddress()->getDeliveryHome());
        $order->getShippingAddress()->setData('delivery_home_email', $quote->getShippingAddress()->getDeliveryHomeEmail());
        $order->getShippingAddress()->setData('delivery_home_mobile', $quote->getShippingAddress()->getDeliveryHomeMobile());
        return $this;
    }
}