<?php

namespace Knight\HomeDelivery\Block\Address;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;


class Edit extends \Magento\Customer\Block\Address\Edit
{

	/**
     * Return the name of the region for the address being edited.
     *
     * @return string region name
     */
    public function getDeliveryHome()
    {
        $addressId = $this->getAddress()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Address')->load($addressId);
        
        $home_delivery = $customerObj->getData('delivery_home');
        return $home_delivery ;
    }


    public function getDeliveryHomeEmail()
    {
        $addressId = $this->getAddress()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Address')->load($addressId);
        $home_delivery_email = $customerObj->getData('delivery_home_email');

        return $home_delivery_email;
    }

    public function getDeliveryHomeMobile()
    {
        $addressId = $this->getAddress()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Address')->load($addressId);
        $delivery_home_mobile = $customerObj->getData('delivery_home_mobile');

        return $delivery_home_mobile;
    }
}