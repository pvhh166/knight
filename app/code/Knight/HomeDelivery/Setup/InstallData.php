<?php
namespace Knight\HomeDelivery\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Model\Order;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

/**
 * Class InstallData
 *
 * @package Knight\HomeDelivery\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * Sales setup factory
     *
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * Quote setup factory
     *
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    private $customerSetupFactory;
     /**
     * @var Config
     */
    private $eavConfig;
 
     /**
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;
 
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    
    /**
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        Config $eavConfig,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        EavSetupFactory $eavSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->eavConfig            = $eavConfig;
        $this->_eavSetupFactory     = $eavSetupFactory;
        $this->attributeSetFactory  = $attributeSetFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        $salesSetup->addAttribute(
            Order::ENTITY,
            'delivery_home',
            [
                'type'     => Table::TYPE_SMALLINT,
                'visible' => false,
                'nullable' => false,
                'default' => 0,
                'required' => false
            ]
        )->addAttribute(
            Order::ENTITY,
            'delivery_home_email',
            [
                'type'     => Table::TYPE_TEXT,
                'visible' => false,
                'nullable' => true,
                'required' => false
            ]
        )->addAttribute(
            Order::ENTITY,
            'delivery_home_mobile',
            [
                'type'     => Table::TYPE_TEXT,
                'visible' => false,
                'nullable' => true,
                'required' => false
            ]
        );

        $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
        $quoteSetup->addAttribute(
            'quote',
            'delivery_home',
            [
                'type'     => Table::TYPE_SMALLINT,
                'visible' => false,
                'nullable' => false,
                'default' => 0,
                'required' => false
            ]
        )->addAttribute(
            'quote',
            'delivery_home_email',
            [
                'type'     => Table::TYPE_TEXT,
                'visible' => false,
                'nullable' => true,
                'required' => false
            ]
        )->addAttribute(
            'quote',
            'delivery_home_mobile',
            [
                'type'     => Table::TYPE_TEXT,
                'visible' => false,
                'nullable' => true,
                'required' => false
            ]
        );

        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute('customer_address', 'delivery_home_email', [
                'type'         => 'varchar',
                'label'        => 'Delivery Home Email',
                'input'        => 'text',
                'required'     => false,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'system'=> false,
                'group'=> 'General',
                'global' => true,
                'visible_on_front' => true,
            ]
        )->addAttribute('customer_address', 'delivery_home_mobile', [
                'type'         => 'varchar',
                'label'        => 'Contact Mobile Number',
                'input'        => 'text',
                'required'     => false,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'system'=> false,
                'group'=> 'General',
                'global' => true,
                'visible_on_front' => true,
            ]
        )->addAttribute(
            'customer_address',
            'delivery_home',
            [
                'type'         => 'varchar',
                'label'        => 'Delivery Home',
                'input'        => 'text',
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'system'=> false,
                'group'=> 'General',
                'global' => true,
                'visible_on_front' => true,
            ]
        );

        $customAttribute = $this->eavConfig->getAttribute('customer_address', 'delivery_home_email');
 
        $customAttribute->setData(
            'used_in_forms',
            ['customer_address_edit','customer_register_address']
        );

        $customAttribute->save();

        $customAttribute = $this->eavConfig->getAttribute('customer_address', 'delivery_home');
        $customAttribute->setData(
            'used_in_forms',
            ['customer_address_edit','customer_register_address']
        );
        
        $customAttribute->save();

        $customAttribute = $this->eavConfig->getAttribute('customer_address', 'delivery_home_mobile');
        $customAttribute->setData(
            'used_in_forms',
            ['customer_address_edit','customer_register_address']
        );
        
        $customAttribute->save();
    }
}
