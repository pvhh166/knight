<?php
namespace Knight\HomeDelivery\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'delivery_home_email',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Delivery Home Email',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote_address'),
            'delivery_home_email',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Delivery Home Email',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_address'),
            'delivery_home_email',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Delivery Home Email',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote_address'),
            'delivery_home',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false,
                'comment' => 'Delivery Home',
                'default' => 0,
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_address'),
            'delivery_home',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false,
                'comment' => 'Delivery Home',
                'default' => 0,
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'delivery_home_mobile',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Contact Mobile Number',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote_address'),
            'delivery_home_mobile',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Contact Mobile Number',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_address'),
            'delivery_home_mobile',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Contact Mobile Number',
            ]
        );

        $installer->endSetup();
    }
}
