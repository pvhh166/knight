<?php
namespace Knight\HomeDelivery\Plugin\Model\Checkout;

class ShippingInformationManagementPlugin
{

    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $quote = $this->quoteRepository->getActive($cartId);
        $delivery = $extAttributes->getDeliveryHome();
        $delivery_email = $extAttributes->getDeliveryHomeEmail();
        $delivery_mobile = $extAttributes->getDeliveryHomeMobile();
        if(empty($delivery) && empty($delivery_email) && empty($delivery_mobile)) {
            $addressId = $addressInformation->getShippingAddress()->getData('customer_address_id');

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\Address')->load($addressId);
            $delivery_email = $customerObj->getData('delivery_home_email');
            $delivery_mobile = $customerObj->getData('delivery_home_mobile');
            $delivery = $customerObj->getData('delivery_home');
        }
        

        $quote->setData('delivery_home',$delivery);
        $quote->setData('delivery_home_email',$delivery_email);
        $quote->setData('delivery_home_mobile',$delivery_mobile);
        $quote->getShippingAddress()->setData('delivery_home',$delivery);
        $quote->getShippingAddress()->setData('delivery_home_email',$delivery_email);
        $quote->getShippingAddress()->setData('delivery_home_mobile',$delivery_mobile);
    }
}