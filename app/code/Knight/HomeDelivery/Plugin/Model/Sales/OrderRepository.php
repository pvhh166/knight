<?php
namespace Knight\HomeDelivery\Plugin\Model\Sales;

class OrderRepository
{
    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $entity
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $entity
    ) {

        $extensionAttributes = $entity->getExtensionAttributes();
        
        if ($extensionAttributes) {
            $extensionAttributes->setDeliveryHome($entity->getDeliveryHome());
            $extensionAttributes->setDeliveryHomeEmail($entity->getDeliveryHomeEmail());
            $extensionAttributes->setDeliveryHomeMobile($entity->getDeliveryHomeMobile());
            $entity->setExtensionAttributes($extensionAttributes);
        }

        return $entity;
    }
}