<?php
namespace Knight\HomeDelivery\Plugin\Model\Quote;

class ShippingAddressManagement
{
    protected $logger;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();

        if (!empty($extAttributes)) {
         
            try {
                $addressInformation->setDeliveryHome($extAttributes->getDeliveryHome());
                $addressInformation->setDeliveryHomeEmail($extAttributes->getDeliveryHomeEmail());
                $addressInformation->setDeliveryHomeMobile($extAttributes->getDeliveryHomeMobile());
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
        
    }
}