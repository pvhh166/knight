<?php
namespace Knight\HomeDelivery\Plugin\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor;

class LayoutProcessorPlugin
{
    protected $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function afterProcess(LayoutProcessor $subject, array $jsLayout)
    {
        $delivery_home = 'delivery_home';
        $contact_email = 'delivery_home_email';
        $contact_mobile = 'delivery_home_mobile';

        $is_home_deliveryField = [
            // 'component' => 'Magento_Ui/js/form/element/abstract',
            'component' => 'Knight_HomeDelivery/js/view/checkout/shipping/home_delivery',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'Knight_HomeDelivery/checkout/shipping/delivery_home',
                'elementTmpl' => 'ui/form/element/checkbox',
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $delivery_home,
            'label' => 'This is a home delivery address',
            'provider' => 'checkoutProvider',
            'sortOrder' => 75,
            'validation' => [
                'required-entry' => false
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        $contact_emailField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'tooltip' => [
                    'description' => 'For delivery Question.
                    Required for Home Delivery.',
                ],
            ],

            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $contact_email,
            'label' => 'Contact Email Address',
            'provider' => 'checkoutProvider',
            'sortOrder' => 300,
            'validation' => [
                'required-entry' => false
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        $contact_mobileField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'Knight_HomeDelivery/form/element/contactmobile',
                'elementTmpl' => 'ui/form/element/input',
            ],

            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $contact_mobile,
            'label' => 'Contact Mobile Number',
            'provider' => 'checkoutProvider',
            'sortOrder' => 180,
            'validation' => [
                'required-entry' => false,
                'max_text_length' => 12,
                'validate-number' => true,
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$contact_email] = $contact_emailField;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$contact_mobile] = $contact_mobileField;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$delivery_home] = $is_home_deliveryField;
        // create custom validation
        $email_addtional_validate = [
            'component' => 'Knight_HomeDelivery/js/view/checkout/shipping/email_validation',
            'sortOrder' => 301,
            'visible' => true,
        ];

        return $jsLayout;
    }
}