<?php

/**
 * Used in creating options for Yes|No config value selection
 *
 */
namespace Magenest\Promobar\Model\Config\Source;

class TypeButton
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $buttonCollection = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magenest\Promobar\Model\ResourceModel\Button\Collection::class);
        $options = [];
        $options += ['0' => __('---Select Button---')];
        foreach($buttonCollection as $button){
            if($button->getStatus()==1) {
                $options += [
                    json_encode($button->getData()) => __($button->getData('title')),
                ];
            }
        }
        return $options;
    }
}
