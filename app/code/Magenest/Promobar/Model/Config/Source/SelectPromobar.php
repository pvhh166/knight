<?php

namespace Magenest\Promobar\Model\Config\Source;

class SelectPromobar implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $barCollection = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magenest\Promobar\Model\ResourceModel\Promobar\Collection::class);
        $options = [];
        $options += ['0' => __('--Select Promo Bar--')];
        foreach($barCollection as $bar){
            if($bar->getStatus()==0) {
                $options += [
                    $bar->getData('promobar_id') => __($bar->getData('title')),
                ];
            }
        }
        return $options;
    }
}
