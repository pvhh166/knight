/**
 * Copyright ©  Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'Webkul_AdvanceSeoManager/js/model/productrules',
    'mage/translate'
    ],
    function (_, Component, ko, $, seoRules, $t) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/catalog/product/seo.html',
                    productName: '',
                    productSku: '',
                    productDescription:'',
                    productImages: 0,
                    metaDescription: '',
                    metaKeywords: '',
                    metaTitle:'',
                    focusKeyword:'',
                    positionLeft: "0%",
                    seoIssues: [],
                    seoWarnings: [],
                    seoSuccess: [],
                    seoManagerNotice:'',
                    imports: {
                        productName: '${ $.provider }:data.product.name',
                        productDescription: '${ $.provider }:data.product.description',
                        productSku: '${ $.provider }:data.product.sku',
                        metaTitle: '${ $.provider }:data.product.meta_title',
                        metaKeywords: '${ $.provider }:data.product.meta_keyword',
                        metaDescription: '${ $.provider }:data.product.meta_description'
                    },
                },

                /**
                 * execution starts here
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    this.initSubscribers();
                    $("body").on(
                        "keyup",
                        ".focus_keyword",
                        function () {
                            self.focusKeyword($(this).val());
                        }
                    );
                },

                /**
                 * initialize observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'productName productDescription productImages productSku metaTitle metaKeywords metaDescription positionLeft seoIssues seoWarnings seoSuccess focusKeyword seoManagerNotice'
                    );

                    return this;
                },

                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    if (window.seoConfiguration.mode == "new") {
                        self.seoManagerNotice($t("please save the product for best possible results"));
                    }
                    self.focusKeyword(window.seoConfiguration.product.focus_keyword);
                    seoRules({seoObject:self});

                    self.productName.subscribe(
                        function (name) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.productDescription.subscribe(
                        
                        function (productDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.productSku.subscribe(
                        function (productSku) {
               
                        }
                    );

                    self.metaTitle.subscribe(
                        function (metaTitle) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaKeywords.subscribe(
                        function (metaKeywords) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaDescription.subscribe(
                        function (metaDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.focusKeyword.subscribe(
                        function (focusKeyword) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.productImages.subscribe(
                        function (productImages) {
                            seoRules({seoObject:self});
                        }
                    );
                },


            }
        );
    }
);
