define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'mage/translate',
    'Webkul_AdvanceSeoManager/js/components/on-page-seo'
    ],
    function (_, Component, ko, $, $t, onPageSeo) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/snippet/cms/serp.html',
                    snippetTitle:'',
                    snippetDescription:'',
                    pageName:'',
                    pageDescription:'',
                    pageUrl:'',
                    focusKeyword: '',
                    metaKeywords:'',
                    metaTitle:'',
                    metaDescription:'',
                    mobilePageUrl:'',
                    imports: {
                        pageName: '${ $.provider }:data.title',
                        pageDescription: '${ $.provider }:data.content',
                        metaTitle: '${ $.provider }:data.meta_title',
                        metaKeywords: '${ $.provider }:data.meta_keywords',
                        metaDescription: '${ $.provider }:data.meta_description'
                    },
                },

                /**
                 * execution starts
                 */
                initialize: function () {
                    var self = this;
                    this._super();
            
                    self.initSubscribers();
                    self.tabularization();

                    $("body").on(
                        "keyup",
                        "input[name='meta_title']",
                        function () {
                            self.metaTitle($(this).val());
                        }
                    );

                    $("body").on(
                        "keyup",
                        "input[name='meta_keywords']",
                        function () {
                            self.metaKeywords($(this).val());
                        }
                    );

                    $("body").on(
                        "keyup",
                        "input[name='meta_description']",
                        function () {
                            self.metaDescription($(this).val());
                        }
                    );
            
                    $("body").on(
                        "keyup",
                        ".focus_keyword",
                        function () {
                            self.focusKeyword($(this).val());
                        }
                    );
                },

                /**
                 * init observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'pageName pageDescription metaTitle metaKeywords metaDescription snippetTitle snippetDescription pageUrl focusKeyword mobilePageUrl'
                    );

                    return this;
                },

                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    self.pageUrl(self.snippetUrlForSerp());
                    self.mobilePageUrl(self.snippetUrlForMobileSerp());
                    if (self.metaTitle()) {
                        self.snippetTitle(self.snippetTitleForSerp(self.metaTitle()));
                    } else {
                        self.snippetTitle(self.snippetTitleForSerp(self.pageName()));
                    }

                    if (self.metaDescription()) {
                        self.snippetDescription(self.snippetDescriptionForSerp(self.metaDescription()));
                    } else {
                        self.snippetDescription(self.snippetDescriptionForSerp(self.pageDescription()));
                    }

                    self.metaTitle.subscribe(
                        function (metaTitle) {
                            if (metaTitle) {
                                self.snippetTitle(self.snippetTitleForSerp(metaTitle));
                            } else {
                                self.snippetTitle(self.snippetTitleForSerp(self.pageName()));
                            }
                        }
                    );

                    self.metaDescription.subscribe(
                        function (metaDescription) {
                            if (metaDescription) {
                                self.snippetDescription(self.snippetDescriptionForSerp(metaDescription));
                            } else {
                                self.snippetDescription(self.snippetDescriptionForSerp(self.pageDescription()));
                            }
                        }
                    );
            
                    self.focusKeyword.subscribe(
                        function (focusKeyword) {
                            self.updateMetaKeywords(focusKeyword);
                            self.snippetDescription(self.snippetDescriptionForSerp(self.snippetDescription()));
        
                        }
                    );


           
                },

                /**
                 * update serp snippet title
                 */
                snippetTitleForSerp: function (title) {
                    if (title.length <= 70) {
                        return title;
                    } else {
                        title = title.substr(0,69)+" ...";
                        return title;
                    }
                },

                /**
                 * update serp snippet description
                 */
                snippetDescriptionForSerp: function (desc) {
                    var self = this;
                    if (desc.length <= 156) {
                        return self.highlightFocusKeyword(desc);
                    } else {
                        desc = desc.substr(0,156)+" ...";
                        return self.highlightFocusKeyword(desc);
                    }
            
                },

                /**
                 * highlight focus keyword
                 */
                highlightFocusKeyword: function (desc) {
                    var self = this;
                    var keyword = self.focusKeyword();
                    desc = desc.replace(/(<([^>]+)>)/ig,"");
                    if (keyword) {
                        var rx = new RegExp(keyword, 'gim');
                        var finalDesc = desc.replace(rx, "<strong>"+keyword+"</strong>");
                        return finalDesc;
                    }
            

                    return desc;
                },

                /**
                 * add focus keyword in the meta keyword
                 */
                updateMetaKeywords: function (focusKeyword) {
                    var self = this;
                    if (self.metaKeywords()) {
                        var rx = new RegExp(focusKeyword, 'gi');
                        var matches = self.metaKeywords().match(rx);
                        if (!matches) {
                            var metaKeywords = self.metaKeywords();
                            metaKeywords += ","+focusKeyword;
                            $("textarea[name='meta_keywords']").val(metaKeywords);
                        }
                    } else {
                        $("textarea[name='meta_keywords']").val(focusKeyword);
                    }
                },

                /**
                 * ur to show in serp snippet
                 */
                snippetUrlForSerp: function () {
                    return window.seoConfiguration.page.pageUrl.substr(0,85)+" ...";
                },

                /**
                 * ur to show in mobile serp snippet
                 */
                snippetUrlForMobileSerp: function () {
                    return window.seoConfiguration.page.pageUrl.substr(0,40)+" ...";
                },

                /**
                 * switch between mobile and descktop
                 */
                tabularization: function () {
                    var self = this;
                    $('body').on(
                        "click",
                        '.seo-preview-container .seo-tab-title',
                        function () {
                            if (!$(this).hasClass('active-preview')) {
                                $(".seo-preview-container .seo-tab-title").removeClass("active-preview");
                                $(this).addClass("active-preview");
                                var id = $(this).attr('id');
                                $(".seo-preview-container .seo-tabs").hide();
                                $(".seo-preview-container ."+id+".seo-tabs").show();
                            }
                        }
                    );
                }
            }
        );
    }
);