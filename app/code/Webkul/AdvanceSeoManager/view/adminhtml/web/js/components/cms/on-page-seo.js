/**
 * Copyright ©  Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'Webkul_AdvanceSeoManager/js/model/cms/cmsrules',
    'mage/translate'
    ],
    function (_, Component, ko, $, seoRules, $t) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/cms/seo.html',
                    pageName: '',
                    pageDescription:'',
                    metaDescription: '',
                    metaKeywords: '',
                    metaTitle:'',
                    focusKeyword:'',
                    positionLeft: "0%",
                    seoIssues: [],
                    seoWarnings: [],
                    seoSuccess: [],
                    seoManagerNotice:'',
                    imports: {
                        pageName: '${ $.provider }:data.title',
                        pageDescription: '${ $.provider }:data.content',
                        metaTitle: '${ $.provider }:data.meta_title',
                        metaKeywords: '${ $.provider }:data.meta_keywords',
                        metaDescription: '${ $.provider }:data.meta_description'
                    },
                },

                /**
                 * execution starts here
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    this.initSubscribers();
                    $("body").on(
                        "keyup",
                        ".focus_keyword",
                        function () {
                            self.focusKeyword($(this).val());
                        }
                    );
                },

                /**
                 * initialize observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'pageName pageDescription metaTitle metaKeywords metaDescription positionLeft seoIssues seoWarnings seoSuccess focusKeyword seoManagerNotice'
                    );

                    return this;
                },

                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    if (window.seoConfiguration.mode == "new") {
                        self.seoManagerNotice($t("please save the page for best possible results"));
                    }
                    self.focusKeyword(window.seoConfiguration.page.focus_keyword);
                    seoRules({seoObject:self});

                    self.pageName.subscribe(
                        function (name) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.pageDescription.subscribe(
                        function (pageDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaTitle.subscribe(
                        function (metaTitle) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaKeywords.subscribe(
                        function (metaKeywords) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaDescription.subscribe(
                        function (metaDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.focusKeyword.subscribe(
                        function (focusKeyword) {
                            seoRules({seoObject:self});
                        }
                    );
                },


            }
        );
    }
);
