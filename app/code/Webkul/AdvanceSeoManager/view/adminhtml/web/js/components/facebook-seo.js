define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'mage/translate'
    ],
    function (_, Component, ko, $, $t) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/snippet/facebook-seo.html',
                    productName: '',
                    productSku: '',
                    facebookTitle:'',
                    facebookDescription:'',
                    facebookImg:{},
                    productDescription:'',
                    facebookSnippetTitle:'',
                    facebookSnippetDescription:'',
                    descStatusMessages: [],
                    titleStatusMessages:[],
                    imgStatusMessages:[],
                    facebookImage:'',
                    productUrl:'',
                    product: self.seoConfiguration.product,
                    imports: {
                        productName: '${ $.provider }:data.product.name',
                        productDescription: '${ $.provider }:data.product.description',
                        productSku: '${ $.provider }:data.product.sku',
                        metaTitle: '${ $.provider }:data.product.meta_title',
                        metaKeywords: '${ $.provider }:data.product.meta_keyword',
                        metaDescription: '${ $.provider }:data.product.meta_description'
                    },
                },

                /**
                 * execution starts here
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    this.initSubscribers();
                    if (window.seoConfiguration.product.productImage || window.seoConfiguration.product.facebook_image) {
                        if (window.seoConfiguration.product.facebook_image) {
                            self.facebookImage(window.seoConfiguration.product.facebook_image);
                        } else {
                            self.facebookImage(window.seoConfiguration.product.productImage);
                        }
                    }
                    $('body').on(
                        "change",
                        ".facebook_image",
                        function (e) {
                            var file = e.target.files[0];
                
                            var _URL = window.URL || window.webkitURL;
                            var url = _URL.createObjectURL(e.target.files[0]);
                            var img = new Image();
                            img.onload = function () {
                                file.width = this.width;
                                file.height = this.height;
                                if (file.width <= 400) {
                                    $(".fb-img-container").addClass("fb-small-image");
                                } else {
                                    $(".fb-img-container").removeClass("fb-small-image");
                                }
                                self.facebookImg(file);
                            };
                            
                            img.src = url;
                            self.facebookImage(url);
                
                        }
                    );
            
                },

                /**
                 * initialize observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'productName productDescription productImages productSku metaTitle metaKeywords metaDescription facebookTitle facebookDescription facebookImg facebookSnippetDescription facebookSnippetTitle titleStatusMessages descStatusMessages imgStatusMessages facebookImage productUrl'
                    );

                    return this;
                },
        
                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    self.productUrl(self.getProductUrl());
                    self.facebookTitle(self.product.facebook_title);
                    self.facebookDescription(self.product.facebook_description);
                    if (!self.facebookTitle()) {
                        self.facebookSnippetTitle(self.filterMetaTitle(self.productName()));
                    } else {
                        self.facebookSnippetTitle(self.filterMetaTitle(self.facebookTitle()));
                    }


                    if (!self.facebookDescription()) {
                        self.facebookSnippetDescription(self.filterMetaDescription(self.productDescription()));
                    } else {
                        self.facebookSnippetDescription(self.filterMetaDescription(self.facebookDescription()));
                    }

                    self.productName.subscribe(
                        function (name) {
                            if (!self.facebookTitle()) {
                                self.facebookSnippetTitle(self.filterMetaTitle(name));
                            }
                        }
                    );

                    self.productDescription.subscribe(
                        function (productDescription) {
                            if (!self.facebookDescription()) {
                                self.facebookSnippetDescription(self.filterMetaDescription(productDescription));
                            }
                        }
                    );

                    self.facebookTitle.subscribe(
                        function (fbTitle) {
                            self.facebookSnippetTitle(self.filterMetaTitle(fbTitle));
                        }
                    );

                    self.facebookDescription.subscribe(
                        function (fbDesc) {
                            self.facebookSnippetDescription(self.filterMetaDescription(fbDesc));
                        }
                    );

                    self.facebookImg.subscribe(
                        function (image) {
                            self.filterImg(image);
                        }
                    );
                    self.facebookTitle(self.product.facebook_title);
                    self.facebookDescription(self.product.facebook_description);
                },

                /**
                 * filter meta description
                 */
                filterMetaDescription: function (metaDesc) {
                    var self = this;
           
                    metaDesc = metaDesc.replace(/(<([^>]+)>)/ig,"");
                    self.descStatusMessages([]);
                    if (metaDesc) {
                        if (!self.facebookDescription()) {
                            self.descStatusMessages.push({error: $t("consider adding specific meta description for facebook sharing")});
                        }
                        var length = metaDesc.length;
                        if (length >= 300) {
                            self.descStatusMessages.push({error: $t("more than 300 characters will be not visible on facebook")});
                            return metaDesc.substr(0, 300)+" ...";
                        } else if (length <= 300 && length >= 110) {
                            self.descStatusMessages.push({success: $t("description is in between 110 - 300 characters which is great")});
                            return metaDesc;
                        } else if (length < 110) {
                            self.descStatusMessages.push({error: $t("description is too small only "+length+" characters, write more about the post, upto 300 characters")});
                            return metaDesc;
                        }
                    } else {
                        self.descStatusMessages.push({error: $t("no descriptuion defined, give a description to your facebook post")});
                    }
                    return '';
                },

                /**
                 * filter meta title
                 */
                filterMetaTitle: function (metaTitle) {
                    var self = this;
                    self.titleStatusMessages([])
                    if (metaTitle) {
                        if (!self.facebookTitle()) {
                            self.titleStatusMessages.push({error: $t("consider adding specific meta title for facebook sharing")});
                        }
                        var length = metaTitle.length;
                        if (length > 40) {
                            self.titleStatusMessages.push({error : $t("more than 40 characters will be not visible on facebook")});

                            return metaTitle.substr(0, 40)+" ...";
                        } else {
                            self.titleStatusMessages.push({success : $t("title is perfect")});
                            return metaTitle;
                        }
                    } else {
                        self.titleStatusMessages.push({error: $t("no title defined, give a title to your facebook post")});
                    }
                    return '';
                },

                /**
                 * filter image
                 */
                filterImg: function (image) {
                    var self = this;
                    self.imgStatusMessages([]);
                    var size = parseInt(image.size)/(1024*1024);
                    if (size <= 8) {
                        if (image.width >= 1200 && image.height >= 630) {
                            self.imgStatusMessages.push({success: $t("your image size is good")});
                        } else if (image.width >= 600 && image.height >= 315) {
                            self.imgStatusMessages.push({success: $t("your image size is good, you can upload upto 1200 x 630 pixels")});
                        } else {
                            self.imgStatusMessages.push({error: $t("your image size is "+image.width+" x "+image.height+" pixels, try to upload image above 600 x 315 pixels")});
                        }
                    } else {
                        self.facebookImage("");
                        self.imgStatusMessages.push({error: $t("your uploaded image size is: "+ size+" MB, upload image less than 8 MB")});
                    }
                },

                /**
                 * get product page url
                 */
                getProductUrl: function () {
                    return window.seoConfiguration.baseUrl;
                }


            }
        );
    }
);
