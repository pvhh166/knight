/**
 * Copyright ©  Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'Webkul_AdvanceSeoManager/js/model/category/categoryrules',
    'mage/translate'
    ],
    function (_, Component, ko, $, seoRules, $t) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/catalog/category/seo.html',
                    categoryName: '',
                    categoryDescription:'',
                    metaDescription: '',
                    metaKeywords: '',
                    metaTitle:'',
                    focusKeyword:'',
                    positionLeft: "0%",
                    seoIssues: [],
                    seoWarnings: [],
                    seoSuccess: [],
                    seoManagerNotice:'',
                    imports: {
                        categoryName: '${ $.provider }:data.name',
                        categoryDescription: '${ $.provider }:data.description',
                        metaTitle: '${ $.provider }:data.meta_title',
                        metaKeywords: '${ $.provider }:data.meta_keywords',
                        metaDescription: '${ $.provider }:data.meta_description'
                    },
                },

                /**
                 * execution starts here
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    this.initSubscribers();
                    $("body").on(
                        "keyup",
                        ".focus_keyword",
                        function () {
                            self.focusKeyword($(this).val());
                        }
                    );
                },

                /**
                 * initialize observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'categoryName categoryDescription metaTitle metaKeywords metaDescription positionLeft seoIssues seoWarnings seoSuccess focusKeyword seoManagerNotice'
                    );

                    return this;
                },

                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    if (window.seoConfiguration.mode == "new") {
                        self.seoManagerNotice($t("please save the category for best possible results"));
                    }
                    self.focusKeyword(window.seoConfiguration.category.focus_keyword);
                    seoRules({seoObject:self});

                    self.categoryName.subscribe(
                        function (name) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.categoryDescription.subscribe(
                        function (categoryDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaTitle.subscribe(
                        function (metaTitle) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaKeywords.subscribe(
                        function (metaKeywords) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.metaDescription.subscribe(
                        function (metaDescription) {
                            seoRules({seoObject:self});
                        }
                    );

                    self.focusKeyword.subscribe(
                        function (focusKeyword) {
                            seoRules({seoObject:self});
                        }
                    );
                },


            }
        );
    }
);
