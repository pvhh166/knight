define(
    [
    'underscore',
    'uiComponent',
    'ko',
    'jquery',
    'mage/translate'
    ],
    function (_, Component, ko, $, $t) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Webkul_AdvanceSeoManager/snippet/twitter-seo.html',
                    productName: '',
                    productSku: '',
                    twitterTitle:'',
                    twitterDescription:'',
                    productDescription:'',
                    twitterSnippetTitle:'',
                    twitterSnippetDescription:'',
                    descStatusMessages: [],
                    titleStatusMessages:[],
                    imgStatusMessages:[],
                    twitterImage:'',
                    twitterImg:{},
                    twitterSnippetDateTime:'',
                    largeSummaryCard: '',
                    product: self.seoConfiguration.product,
                    productUrl:'',
                    imports: {
                        productName: '${ $.provider }:data.product.name',
                        productDescription: '${ $.provider }:data.product.description',
                        productSku: '${ $.provider }:data.product.sku',
                        metaTitle: '${ $.provider }:data.product.meta_title',
                        metaKeywords: '${ $.provider }:data.product.meta_keyword',
                        metaDescription: '${ $.provider }:data.product.meta_description'
                    },
                },

                /**
                 * execution starts here
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    this.initSubscribers();
                    if (window.seoConfiguration.product.twitter_image || window.seoConfiguration.product.productImage) {
                        if (window.seoConfiguration.product.twitter_image) {
                            self.twitterImage(window.seoConfiguration.product.twitter_image);
                        } else {
                            self.twitterImage(window.seoConfiguration.product.productImage);
                        }
                    }
                    $('body').on(
                        "change",
                        ".twitter_image",
                        function (e) {
                            var file = e.target.files[0];
                
                            var _URL = window.URL || window.webkitURL;
                            var url = _URL.createObjectURL(e.target.files[0]);
                            var img = new Image();
                            img.onload = function () {
                                file.width = this.width;
                                file.height = this.height;
                                self.twitterImg(file);;
                            };
                            img.src = url;
                            self.twitterImage(url);
                
                        }
                    );

                    if (!window.seoConfiguration.largeSummaryCard) {
                        self.largeSummaryCard("summary-small");
                    }
            

                    self.twitterSnippetDateTime("7:30 PM - Oct 16, 2016");
                },

                /**
                 * initialize observers
                 */
                initObservable: function () {
                    this._super().observe(
                        'productName productDescription productImages productSku metaTitle metaKeywords metaDescription twitterTitle twitterDescription twitterSnippetDescription twitterSnippetTitle titleStatusMessages imgStatusMessages descStatusMessages twitterImage twitterImg twitterSnippetDateTime productUrl largeSummaryCard'
                    );

                    return this;
                },
        
                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    self.productUrl(self.getProductUrl());
                    self.twitterTitle(self.product.twitter_title);
                    self.twitterDescription(self.product.twitter_description);
                    if (!self.twitterTitle()) {
                        self.twitterSnippetTitle(self.filterMetaTitle(self.productName()));
                    } else {
                        self.twitterSnippetTitle(self.filterMetaTitle(self.twitterTitle()));
                    }


                    if (!self.twitterDescription()) {
                        self.twitterSnippetDescription(self.filterMetaDescription(self.productDescription()));
                    } else {
                        self.twitterSnippetDescription(self.filterMetaDescription(self.twitterDescription()));
                    }

                    self.productName.subscribe(
                        function (name) {
                            if (!self.twitterTitle()) {
                                self.twitterSnippetTitle(self.filterMetaTitle(name));
                            }
                        }
                    );

                    self.productDescription.subscribe(
                        function (productDescription) {
                            if (!self.twitterDescription()) {
                                self.twitterSnippetDescription(self.filterMetaDescription(productDescription));
                            }
                        }
                    );

                    self.twitterTitle.subscribe(
                        function (fbTitle) {
                            self.twitterSnippetTitle(self.filterMetaTitle(fbTitle));
                        }
                    );

                    self.twitterDescription.subscribe(
                        function (fbDesc) {
                            self.twitterSnippetDescription(self.filterMetaDescription(fbDesc));
                        }
                    );

                    self.twitterImg.subscribe(
                        function (image) {
                            self.filterImg(image);
                        }
                    );
                },

                /**
                 * filter meta description
                 */
                filterMetaDescription: function (metaDesc) {
                    var self = this;
                    metaDesc = metaDesc.replace(/(<([^>]+)>)/ig,"");
                    self.descStatusMessages([]);
                    if (metaDesc) {
                        if (!self.twitterDescription()) {
                            self.descStatusMessages.push({error: $t("consider adding specific meta description for twitter sharing")});
                        }
                        var length = metaDesc.length;
                        if (length >= 300) {
                            self.descStatusMessages.push({error: $t("more than 300 characters will be not visible on twitter")});
                            return metaDesc.substr(0, 300)+" ...";
                        } else if (length <= 300 && length >= 110) {
                            self.descStatusMessages.push({success: $t("description is in between 110 - 300 characters which is great")});
                            return metaDesc;
                        } else if (length < 110) {
                            self.descStatusMessages.push({error: $t("description is too small only "+length+" characters, write more about the post, upto 300 characters")});
                            return metaDesc;
                        }
                    } else {
                        self.descStatusMessages.push({error: $t("no descriptuion defined, give a description to your twitter post")});
                    }
                    return '';
                },

                /**
                 * filter meta title
                 */
                filterMetaTitle: function (metaTitle) {
                    var self = this;
                    self.titleStatusMessages([])
                    if (metaTitle) {
                        if (!self.twitterTitle()) {
                            self.titleStatusMessages.push({error: $t("consider adding specific meta title for twitter sharing")});
                        }
                        var length = metaTitle.length;
                        if (length > 40) {
                            self.titleStatusMessages.push({error : $t("more than 40 characters will be not visible on twitter")});

                            return metaTitle.substr(0, 40)+" ...";
                        } else {
                            self.titleStatusMessages.push({success : $t("title is perfect")});
                            return metaTitle;
                        }
                    } else {
                        self.titleStatusMessages.push({error: $t("no title defined, give a title to your twitter post")});
                    }
                    return '';
                },

                /**
                 * filter image
                 */
                filterImg: function (image) {
                    var self = this;
                    self.imgStatusMessages([]);
                    var size = parseInt(image.size)/(1024*1024);
                    if (size <= 5) {
                        if ((image.width >= 300 && image.width <= 4096) && image.height >= 157 && image.height <= 4096) {
                            self.imgStatusMessages.push({success: $t("your image size is good")});
                        } else {
                            self.twitterImage("");
                            self.imgStatusMessages.push({error: $t("your image size is "+image.width+" x "+image.height+" pixels, try to upload image above 300 x 157 pixels and less 4096 x 4096")});
                        }
                    } else {
                        self.twitterImage("");
                        self.imgStatusMessages.push({error: $t("your uploaded image size is: "+ size+" MB, upload image less than 8 MB")});
                    }
                },

                /**
                 * get product page url
                 */
                getProductUrl: function () {
                    return window.seoConfiguration.baseUrl;
                }


            }
        );
    }
);
