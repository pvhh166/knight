<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Helper;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;

/**
 * AdvanceSeoManager json-ld configuration helper.
 */
class JsonLd extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    const PRODUCT_SETTINGS_PATH = 'seomanager/general_product_schema/';
    const CATEGORY_SETTINGS_PATH = 'seomanager/general_category_schema/';
    const CMS_SETTINGS_PATH = 'seomanager/general_cms_schema/';

    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Customer session.
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_country;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    protected static $staticScode;

    /**
     * @param Magento\Framework\App\Helper\Context        $context
     * @param Magento\Directory\Model\Currency            $currency
     * @param Magento\Customer\Model\Session              $customerSession
     * @param Magento\Framework\UrlInterface              $url
     * @param Magento\Catalog\Model\ResourceModel\Product $product
     * @param Magento\Store\Model\StoreManagerInterface   $_storeManager
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\App\Helper\Context $context,
        FormKeyValidator $formKeyValidator,
        DateTime $date,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magento\Directory\Model\RegionFactory $regionFactory
    ) {
        
        $this->_date = $date;
        $this->_customerSession = $customerSession;
        $this->_objectManager = $objectManager;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_country  = $country;
        $this->_regionFactory = $regionFactory;
        parent::__construct($context);
        self::$staticScode = $this->scopeConfig;
    }

    /**
     * function to get Config Data.
     *
     * @return string
     */
    public function getConfigValue($field = false)
    {
        if ($field) {
            return self::$staticScode
                ->getValue(
                    $field,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
        } else {
            return;
        }
    }

    /**
     * get product rich snippet data
     *
     * @return array
     */
    public function getProductRichSnippetData()
    {
        if ($this->getConfigValue(self::PRODUCT_SETTINGS_PATH."enable")) {
            return [
                'weight' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_weight"),
                'category' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_category"),
                'brand' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_brand"),
                'brand_attribute_code' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."brand_attribute_code"),
                'image' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_image"),
                'logo' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_logo"),
                'review' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_review"),
                'aggregateRating' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_aggregate_rating"),
                'offers' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_offers"),
                'sku' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_sku"),
                'url' => $this->getConfigValue(self::PRODUCT_SETTINGS_PATH."show_url"),
                'product' => true
            ];
        }

        return [];
    }

    /**
     * get category rich snippet data
     *
     * @return array
     */
    public function getCategoryRichSnippetData()
    {
        if ($this->getConfigValue(self::CATEGORY_SETTINGS_PATH."enable")) {
            return [
                'breadcrumbs' => $this->getConfigValue(self::CATEGORY_SETTINGS_PATH."show_breadcrumbs"),
                'search' => $this->getConfigValue(self::CATEGORY_SETTINGS_PATH."show_search")
            ];
        }
        return [];
    }

    /**
     * get cms rich snippet data
     *
     * @return array
     */
    public function getCmsRichSnippetData()
    {
        if ($this->getConfigValue(self::CMS_SETTINGS_PATH."enable")) {
            return [
                'requiredData' => true,
                'search' => $this->getConfigValue(self::CMS_SETTINGS_PATH."show_search"),
                'address' => $this->getConfigValue(self::CMS_SETTINGS_PATH."show_address"),
                'contacts' => $this->getConfigValue(self::CMS_SETTINGS_PATH."show_contact_points"),
                'image' => $this->getConfigValue(self::CMS_SETTINGS_PATH."show_image")
            ];
        }
        return [];
    }
}
