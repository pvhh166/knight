<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Helper;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;

/**
 * AdvanceSeoManager helper.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const STORE_INFO = "general/store_information/";
    const CONTACT_INFO = "trans_email/";
    const CONICAL_URL_SETTINGS = "seomanager/conical_url/";
    const PING_SERVICE = "seomanager/ping_service/";
    const SOCIAL_SHARING = "seomanager/social_rich_snippet/";

    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Customer session.
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_country;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * @var \Magento\Theme\Block\Html\Header\Logo
     */
    protected $logo;

    /**
     * @var \Webkul\AdvanceSeoManager\Model\System\Config\Source\PingServiceOptions
     */
    protected $pingServices;

    /**
     * @param Magento\Framework\App\Helper\Context        $context
     * @param Magento\Directory\Model\Currency            $currency
     * @param Magento\Customer\Model\Session              $customerSession
     * @param Magento\Framework\UrlInterface              $url
     * @param Magento\Catalog\Model\ResourceModel\Product $product
     * @param Magento\Store\Model\StoreManagerInterface   $_storeManager
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\App\Helper\Context $context,
        FormKeyValidator $formKeyValidator,
        DateTime $date,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        \Webkul\AdvanceSeoManager\Model\System\Config\Source\PingServicesOptions $pingServices
    ) {
        
        $this->_date = $date;
        $this->_customerSession = $customerSession;
        $this->_objectManager = $objectManager;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_country  = $country;
        $this->_regionFactory = $regionFactory;
        $this->logo = $logo;
        $this->pingServices = $pingServices;
        parent::__construct($context);
    }

    /**
     * is module enable
     *
     * @return boolean
     */
    public function getIsEnable()
    {
        return $this->scopeConfig->getValue(
            "seomanager/general_settings/enable",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get store name
     *
     * @return string
     */
    public function getStoreName()
    {

        return $this->scopeConfig->getValue(self::STORE_INFO.'name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * get store address
     *
     * @return string
     */
    public function getStorePostalCode()
    {

        return $this->scopeConfig->getValue(
            self::STORE_INFO.'postcode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get store address
     *
     * @return string
     */
    public function getStoreLogo()
    {

        return $this->logo->getLogoSrc();
    }

    /**
     * get store address
     *
     * @return string
     */
    public function getStoreStreetAddress()
    {

        $streetLine1 = $this->getStreetLine1();
        $streetLine2 = $this->getStreetLine2();
        if ($streetLine1 || ($streetLine1 && $streetLine2)) {
            $street = $streetLine1;
            if ($streetLine2) {
                $street .= ', '.$streetLine2;
            }
            return $street;
        }
        return null;
    }

    /**
     * get phone number
     *
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return $this->scopeConfig->getValue(
            self::STORE_INFO.'phone',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get street line 1
     *
     * @return string|null
     */
    public function getStreetLine1()
    {
        return $this->scopeConfig->getValue(
            self::STORE_INFO.'street_line1',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get street line 2
     *
     * @return string|null
     */
    public function getStreetLine2()
    {
        return $this->scopeConfig->getValue(
            self::STORE_INFO.'street_line2',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get locale code
     *
     * @return string
     */
    public function getLocaleCode()
    {
        return $this->scopeConfig->getValue(
            'general/locale/code',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId()
        );
    }

    /**
     * get currenct page url
     *
     * @return string
     */
    public function getCurrentPageUrl()
    {
        return $this->_storeManager->getStore()->getCurrentUrl(false);
    }

    /**
     * get country and region
     *
     * @return string|null
     */
    public function getAddressLocality()
    {
        $countryCode = $this->scopeConfig->getValue(
            self::STORE_INFO.'country_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $regionId = $this->scopeConfig->getValue(
            self::STORE_INFO.'region_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($countryCode) {
            $countryName = $this->getCountryName($countryCode);
            $regionName = $this->getRegionName($regionId);
            return $countryName.', '.$regionName;
        }
        return '';
    }

    /**
     * get region name
     *
     * @param  mixed $region
     * @return string
     */
    public function getRegionName($region)
    {
        if (is_numeric($region)) {
            return $this->_regionFactory->create()->load($region)->getName();
        }

        return $region;
    }

    /**
     * get country name
     *
     * @param  string $countryCode
     * @return string
     */
    public function getCountryName($countryCode)
    {
        $countries = $this->getCountryList();
        foreach ($countries as $country) {
            if ($country['value'] == $countryCode) {
                return $country['label'];
            }
        }
        return "";
    }

    /**
     * getCountryList function to get country list
     *
     * @return array
     */
    public function getCountryList()
    {
        $countries = $this->_country->toOptionArray(false, 'US');
        unset($countries[0]);
        return $countries;
    }

    /**
     * owner contact details
     *
     * @return array
     */
    public function getOwnerContact()
    {
        $email = $this->scopeConfig->getValue(
            self::CONTACT_INFO.'ident_general/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($email) {
            return [
                'name' => $this->scopeConfig->getValue(
                    self::CONTACT_INFO.'ident_general/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'email' => $email
            ];
        }
        return [];
    }

    /**
     * owner contact details
     *
     * @return array
     */
    public function getSalesContact()
    {
        $email = $this->scopeConfig->getValue(
            self::CONTACT_INFO.'ident_sales/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($email) {
            return [
                'name' => $this->scopeConfig->getValue(
                    self::CONTACT_INFO.'ident_sales/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'email' => $email
            ];
        }
        return [];
    }

    /**
     * get product conical url status
     *
     * @return boolean
     */
    public function getProductConicalUrlStatus()
    {
        return $this->scopeConfig->getValue(
            self::CONICAL_URL_SETTINGS.'enable_for_products',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get category conical url status
     *
     * @return boolean
     */
    public function getCategoryConicalUrlStatus()
    {
        return $this->scopeConfig->getValue(
            self::CONICAL_URL_SETTINGS.'enable_for_categories',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get ping services list
     *
     * @return array
     */
    public function getPingServices()
    {
        return $this->pingServices->toOptionArray();
    }

    /**
     * is auto ping enable
     *
     * @return boolean
     */
    public function getIsAutoPingEnable()
    {
        return $this->scopeConfig->getValue(
            self::PING_SERVICE.'enable_auto_ping',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get auto ping service
     *
     * @return boolean
     */
    public function getAutoPingService()
    {
        return $this->scopeConfig->getValue(
            self::PING_SERVICE.'auto_ping_service',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get auto ping service
     *
     * @return boolean
     */
    public function getAutoPingCustomService()
    {
        return $this->scopeConfig->getValue(
            self::PING_SERVICE.'custom_ping_service',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get facebook sharing status
     *
     * @return boolean
     */
    public function getFacebookSharingStatus()
    {
        return $this->scopeConfig->getValue(
            self::SOCIAL_SHARING.'enable_for_facebook',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get twitter sharing status
     *
     * @return boolean
     */
    public function getTwitterSharingStatus()
    {
        return $this->scopeConfig->getValue(
            self::SOCIAL_SHARING.'enable_for_twitter',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

     /**
      * get twitter summary card type
      *
      * @return boolean
      */
    public function getLargeSummaryCardStatus()
    {
        return $this->scopeConfig->getValue(
            self::SOCIAL_SHARING.'enable_larger_summary_card',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
