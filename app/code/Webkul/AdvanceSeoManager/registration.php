<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_AdvanceSeoManager',
    __DIR__
);
