<?php
namespace Webkul\AdvanceSeoManager\Model\RichSnippet;

class JsonLdBuilder implements \Webkul\AdvanceSeoManager\Api\JsonLdBuilderInterface
{

    /**
     * @var array
     */
    protected $handlers = [];
    /**
     * construct
     *
     * @param array $handlers
     */
    public function __construct(
        $handlers = []
    ) {
        $this->handlers = $handlers;
    }
    
    public function generate()
    {
        return '';
    }

    /**
     * json ld builder
     *
     * @param  stringh                       $jsonLdType
     * @param  Magento\Catalog\Model\Product $typeData
     * @return string
     */
    public function build($jsonLdType, $typeData)
    {
        try {
            $jsonLd = $this->handlers[$jsonLdType]->build($typeData);
            return $jsonLd;
        } catch (\Exception $e) {
            return [$e->getMessage()];
        }

        return [];
    }
}
