<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 *
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context;

class Category extends AbstractContext
{

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Webkul\AdvanceSeoManager\Helper\JsonLd $jsonLdHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category\DataPoolInterface $categoryDataPool
    ) {
        $this->productFactory = $productFactory;
        $this->jsonLdHelper = $jsonLdHelper;
        $this->jsonHelper = $jsonHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->categoryDataPool = $categoryDataPool;
    }

    /**
     * {@inheritdoc}
     */
    public function build($contextData)
    {
        $jsonLdData = $this->createJsonLdFormatArray($contextData);
        $jsonLdString = '';
        $standAloneJsonLd = [];
        if (isset($jsonLdData["standAlone"])) {
            $standAloneJsonLd = $jsonLdData["standAlone"];
            unset($jsonLdData["standAlone"]);
        }
        
        if (count($jsonLdData) > 0) {
            $jsonLdData['@context'] = "http://schema.org";
            $jsonLdString .= $this->getJsonLdFormat($this->jsonHelper->jsonEncode($jsonLdData));
        }
        
        if (count($standAloneJsonLd) > 0) {
            foreach ($standAloneJsonLd as $saj) {
                $saj['@context'] = "http://schema.org";
                $jsonLdString .= $this->getJsonLdFormat($this->jsonHelper->jsonEncode($saj));
            }
        }
        return $jsonLdString;
    }

    /**
     * {@inheritdoc}
     */
    public function createJsonLdFormatArray($contextData)
    {
        $richSnippetFormat = $this->jsonLdHelper->getCategoryRichSnippetData();
        $data = [];
        if ($contextData && $contextData->getId()) {
            $data = $this->getRichCategorySnippet($contextData, $richSnippetFormat);
            return $data;
        }
        return [];
    }

    /**
     * get category page rich snippet
     *
     * @param  Magento\Catalog\Model\Category $contextData
     * @param  array                          $richSnippetFormat
     * @return array
     */
    public function getRichCategorySnippet($contextData, $richSnippetFormat)
    {
        try {
            $result = [];
            if (count($richSnippetFormat) > 0) {
                foreach ($richSnippetFormat as $providerCode => $status) {
                    if ($status) {
                        // @TODO implement exceptions catching
                        $result = array_replace_recursive(
                            $result,
                            $this->categoryDataPool->get($providerCode)->getData($contextData)
                        );
                    }
                }
            }
            return $result;
        } catch (\Exception $e) {
            // @TODO log errors
            return [];
        }
    }
}
