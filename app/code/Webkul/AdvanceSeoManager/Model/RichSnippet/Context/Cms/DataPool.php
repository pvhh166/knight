<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

class DataPool implements DataPoolInterface
{
    /**
     * @var DataPoolInterface[] | TMap
     */
    private $executors;

    /**
     * @param TMapFactory $tmapFactory
     * @param array       $executors
     */
    public function __construct(
        TMapFactory $tmapFactory,
        array $executors = []
    ) {
       
        $this->executors = $tmapFactory->createSharedObjectsMap(
            [
                'array' => $executors,
                'type' => CmsDataInterface::class
            ]
        );
    }

    /**
     * Returns cms data provider object
     *
     * @param  string $dataProviderCode
     * @return CmsDataInterface
     * @throws NotFoundException
     */
    public function get($dataProviderCode)
    {
        if (!isset($this->executors[$dataProviderCode])) {
            throw new NotFoundException(
                __('cms Executor for %1 is not defined.', $dataProviderCode)
            );
        }

        return $this->executors[$dataProviderCode];
    }

    /**
     * Returns all cms data provider objects
     *
     * @return CmsDataInterface
     * @throws NotFoundException
     */
    public function getAll()
    {
        return $this->executors;
    }
}
