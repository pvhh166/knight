<?php

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

class Contacts implements CmsDataInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $_helper;
    
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Webkul\AdvanceSeoManager\Helper\Data $helper
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->_helper = $helper;
    }

    /**
     * get contact data
     *
     * @param  \Magento\Cms\model\Page $page
     * @return array
     */
    public function getData($page)
    {
        if ($this->_helper->getPhoneNumber()) {
            $contactPoints = [
                "@type" => "ContactPoint",
                "telephone" => $this->_helper->getPhoneNumber(),
                "contactType" => "customer service"
            ];
            // @TODO add contact list
            return ["contactPoint"=>$contactPoints];
        } else {
            return [];
        }
    }
}
