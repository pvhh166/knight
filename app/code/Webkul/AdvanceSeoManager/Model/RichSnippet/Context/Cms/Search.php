<?php

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

class Search implements CmsDataInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $_helper;
    
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Webkul\AdvanceSeoManager\Helper\Data $helper
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->_helper = $helper;
    }

    /**
     * get serach data
     *
     * @param  \Magento\Cms\model\Page $page
     * @return array
     */
    public function getData($page)
    {
        return [
            "standAlone" => [
                "WebSite" => [
                    "@type" => "WebSite",
                    "url" => $this->urlBuilder->getUrl(),
                    "potentialAction" => [
                        "@type" => "SearchAction",
                        "target" => $this->urlBuilder->getUrl(
                            "catalogsearch/result/",
                            ['_secure' => true]
                        )."?q={search_term_string}",
                        "query-input" => "required name=search_term_string"
                    ]
                ]
            ]
        ];
    }
}
