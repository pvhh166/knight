<?php

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

class Address implements CmsDataInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $_helper;
    
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Webkul\AdvanceSeoManager\Helper\Data $helper
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->_helper = $helper;
    }

    /**
     * get breadcrumbs
     *
     * @param  \Magento\Cms\model\Page $page
     * @return array
     */
    public function getData($page)
    {
        return [
            "address" =>    [
                "@type" => "PostalAddress",
                "addressLocality" => $this->_helper->getAddressLocality(),
                "postalCode" => $this->_helper->getStorePostalCode(),
                "streetAddress" => $this->_helper->getStoreStreetAddress()
            ]
        ];
    }
}
