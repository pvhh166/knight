<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

interface DataPoolInterface
{

    /**
     * get cms data
     *
     * @param  string
     * @return object
     */
    public function get($providerCode);

    /**
     * get all cms data objects
     *
     * @param  string
     * @return object
     */
    public function getAll();
}
