<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Cms;

interface CmsDataInterface
{

    /**
     * get cms page data
     *
     * @param  \Magento\Cms\model\Page $page
     * @return array
     */
    public function getData($page);
}
