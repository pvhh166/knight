<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 *
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Product;

class Category implements ProductDataInterface
{

    /**
     * @var Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;
    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * inherit
     */
    public function getData($product)
    {
        $name = '';
        $categoryIds = $product->getCategoryIds();
        if (count($categoryIds)) {
            foreach ($categoryIds as $cid) {
                $category = $this->categoryFactory->create()->load($cid);
                $name = $category->getName();
            }
        }
        return $name;
    }
}
