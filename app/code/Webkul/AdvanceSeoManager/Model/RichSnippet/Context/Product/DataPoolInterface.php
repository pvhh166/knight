<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Product;

interface DataPoolInterface
{

    /**
     * get product data
     *
     * @param  string
     * @return object
     */
    public function get($providerCode);
}
