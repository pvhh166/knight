<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 *
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Product;

class RequiredDetails implements ProductDataInterface
{

    /**
     * inherit
     */
    public function getData($product)
    {
        $productData = [];
        $description = $product->getMetaDescription();
        if (!$description) {
            $description = substr(strip_tags($product->getData('description')), 0, 156);
        }
        return $productData = [
            "@context" => "http://schema.org",
            "@type" => "Product",
            "description" => $description,
            "name" => $product->getName()
        ];
    }
}
