<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Product;

interface ProductDataInterface
{

    /**
     * get product data
     *
     * @param  Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getData($product);
}
