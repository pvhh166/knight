<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Product;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

class DataPool implements DataPoolInterface
{
    /**
     * @var DataPoolInterface[] | TMap
     */
    private $executors;

    /**
     * @param TMapFactory $tmapFactory
     * @param array       $executors
     */
    public function __construct(
        TMapFactory $tmapFactory,
        array $executors = []
    ) {
       
        $this->executors = $tmapFactory->createSharedObjectsMap(
            [
                'array' => $executors,
                'type' => ProductDataInterface::class
            ]
        );
    }

    /**
     * Returns product data provider object
     *
     * @param  string $dataProviderCode
     * @return ProductDataInterface
     * @throws NotFoundException
     */
    public function get($dataProviderCode)
    {
        if (!isset($this->executors[$dataProviderCode])) {
            throw new NotFoundException(
                __('Command Executor for %1 is not defined.', $dataProviderCode)
            );
        }

        return $this->executors[$dataProviderCode];
    }
}
