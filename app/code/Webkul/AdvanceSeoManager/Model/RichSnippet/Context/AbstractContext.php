<?php
namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context;

abstract class AbstractContext
{

    protected $productFactory;
    protected $jsonLdHelper;
    protected $_categoryFactory;
    protected $imageHelperFactory;
    protected $productDataPool;
    protected $categoryDataPool;
    protected $cmsDataPool;

    /**
     * wrap json ld data in script tag
     *
     * @param  string $json
     * @return string
     */
    protected function getJsonLdFormat($json)
    {
        return sprintf("<script type='application/ld+json'>%s</script>", $json);
    }

    /**
     * create json ld format data
     *
     * @param  array $data
     * @return array
     */
    abstract public function createJsonLdFormatArray($data);

    /**
     * build json ld data
     *
     * @param  array $contextData
     * @return string
     */
    abstract public function build($contextData);
}
