<?php

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category;

class Breadcrumb implements CategoryDataInterface
{

    /**
     * get breadcrumbs
     *
     * @param  Magento\Catalog\Model\Category $category
     * @return array
     */
    public function getData($category)
    {
        return [];
    }
}
