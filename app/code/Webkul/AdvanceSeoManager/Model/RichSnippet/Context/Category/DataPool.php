<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

class DataPool implements DataPoolInterface
{
    /**
     * @var DataPoolInterface[] | TMap
     */
    private $executors;

    /**
     * @param TMapFactory $tmapFactory
     * @param array       $executors
     */
    public function __construct(
        TMapFactory $tmapFactory,
        array $executors = []
    ) {
       
        $this->executors = $tmapFactory->createSharedObjectsMap(
            [
                'array' => $executors,
                'type' => CategoryDataInterface::class
            ]
        );
    }

    /**
     * Returns Category data provider object
     *
     * @param  string $dataProviderCode
     * @return CategoryDataInterface
     * @throws NotFoundException
     */
    public function get($dataProviderCode)
    {
        if (!isset($this->executors[$dataProviderCode])) {
            throw new NotFoundException(
                __('category Executor for %1 is not defined.', $dataProviderCode)
            );
        }

        return $this->executors[$dataProviderCode];
    }

    /**
     * Returns all Category data provider objects
     *
     * @return CategoryDataInterface
     * @throws NotFoundException
     */
    public function getAll()
    {
        return $this->executors;
    }
}
