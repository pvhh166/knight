<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category;

interface DataPoolInterface
{

    /**
     * get Category data
     *
     * @param  string
     * @return object
     */
    public function get($providerCode);

    /**
     * get Category data
     *
     * @param  string
     * @return object
     */
    public function getAll();
}
