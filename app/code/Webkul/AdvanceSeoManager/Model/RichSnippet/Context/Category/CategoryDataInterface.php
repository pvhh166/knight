<?php

 namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category;

interface CategoryDataInterface
{

    /**
     * get product data
     *
     * @param  Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getData($product);
}
