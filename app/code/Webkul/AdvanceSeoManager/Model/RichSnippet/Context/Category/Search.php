<?php

namespace Webkul\AdvanceSeoManager\Model\RichSnippet\Context\Category;

class Search implements CategoryDataInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * get breadcrumbs
     *
     * @param  Magento\Catalog\Model\Category $category
     * @return array
     */
    public function getData($category)
    {
        return [
            "standAlone" => [
                "WebSite" => [
                    "@type" => "WebSite",
                    "url" => $this->urlBuilder->getUrl(),
                    "potentialAction" => [
                        "@type" => "SearchAction",
                        "target" => $this->urlBuilder->getUrl(
                            "catalogsearch/result/",
                            ['_secure' => true]
                        )."?q={search_term_string}",
                        "query-input" => "required name=search_term_string"
                    ]
                ]
            ]
        ];
    }
}
