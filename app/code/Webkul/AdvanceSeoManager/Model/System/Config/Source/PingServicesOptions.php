<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\AdvanceSeoManager\Model\System\Config\Source;

/**
 * Used in creating options for getting product type value.
 */
class PingServicesOptions
{

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $manager;
    /**
     * Construct.
     *
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $config
     */
    public function __construct(
        \Magento\Framework\Module\Manager $manager
    ) {
        $this->manager = $manager;
    }
    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data = [
            ['value' => 'http://ping.blogs.yandex.ru', 'label' => __('Yandex')],
            ['value' => 'http://blogsearch.google.com/ping/RPC2', 'label' => __('Google')],
            ['value' => 'http://api.my.yahoo.com/RPC2', 'label' => __('Yahoo')],
            ['value' => 'http://ping.feedburner.com', 'label' => __('Feed Burner')],
            ['value' => 'http://ping.weblogs.se/', 'label' => __('Web Logs')],
            ['value' => 'http://pingomatic.com/ping/', 'label' => __('Pingomatic')],
            ['value' => 'all', 'label' => __('Ping All')],
            ['value' => 'custom', 'label' => __('Ping Custom Url')],
        ];
        return $data;
    }
}
