<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\AdvanceSeoManager\Model;

use Magento\Framework\DataObject as MagentoDataObject;

class Sitemap extends \Magento\Sitemap\Model\Sitemap
{
    /**
     * Initialize sitemap items
     *
     * @return void
     */
    protected function _initSitemapItems()
    {
        /**
 * @var $helper \Magento\Sitemap\Helper\Data
*/
        $helper = $this->_sitemapData;
        $storeId = $this->getStoreId();
        $siteMapType = $this->_request->getParam("sitemap_type");
        if ($siteMapType == "category") {
            $this->_sitemapItems[] = new MagentoDataObject(
                [
                    'changefreq' => $helper->getCategoryChangefreq($storeId),
                    'priority' => $helper->getCategoryPriority($storeId),
                    'collection' => $this->_categoryFactory->create()->getCollection($storeId),
                ]
            );
        } elseif ($siteMapType == "product") {
            $this->_sitemapItems[] = new MagentoDataObject(
                [
                    'changefreq' => $helper->getProductChangefreq($storeId),
                    'priority' => $helper->getProductPriority($storeId),
                    'collection' => $this->_productFactory->create()->getCollection($storeId),
                ]
            );
        } else {
            $this->_sitemapItems[] = new MagentoDataObject(
                [
                    'changefreq' => $helper->getCategoryChangefreq($storeId),
                    'priority' => $helper->getCategoryPriority($storeId),
                    'collection' => $this->_categoryFactory->create()->getCollection($storeId),
                ]
            );
            $this->_sitemapItems[] = new MagentoDataObject(
                [
                    'changefreq' => $helper->getProductChangefreq($storeId),
                    'priority' => $helper->getProductPriority($storeId),
                    'collection' => $this->_productFactory->create()->getCollection($storeId),
                ]
            );
        }

        $this->_sitemapItems[] = new MagentoDataObject(
            [
                'changefreq' => $helper->getPageChangefreq($storeId),
                'priority' => $helper->getPagePriority($storeId),
                'collection' => $this->_cmsFactory->create()->getCollection($storeId),
            ]
        );

        $this->_tags = [
            self::TYPE_INDEX => [
                self::OPEN_TAG_KEY => '<?xml version="1.0" encoding="UTF-8"?>' .
                PHP_EOL .
                '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                PHP_EOL,
                self::CLOSE_TAG_KEY => '</sitemapindex>',
            ],
            self::TYPE_URL => [
                self::OPEN_TAG_KEY => '<?xml version="1.0" encoding="UTF-8"?>' .
                PHP_EOL .
                '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' .
                ' xmlns:content="http://www.google.com/schemas/sitemap-content/1.0"' .
                ' xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">' .
                PHP_EOL,
                self::CLOSE_TAG_KEY => '</urlset>',
            ],
        ];
    }
}
