<?php

namespace Webkul\AdvanceSeoManager\Api;

interface JsonLdBuilderInterface
{
    const CONTEXT = "http://www.schema.org";
    /**
     * @api
     *
     * generate jsonld string
     *
     * @return string
     */
    public function generate();
}
