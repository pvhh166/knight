<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Controller\Adminhtml\Ping;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\InputException;

class Service extends Action
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webkul_AdvanceSeoManager::ping_service';

    /**
     * @var Webkul\AdvanceSeoManager\Model\PingService
     */
    protected $_pingService;

    /**
     * __construct
     *
     * @param Context                                    $context
     * @param Webkul\AdvanceSeoManager\Model\PingService $pingService
     */
    public function __construct(
        Context $context,
        \Webkul\AdvanceSeoManager\Model\PingService $pingService
    ) {
        $this->_pingService = $pingService;
        parent::__construct($context);
    }

    /**
     * ping url.
     *
     * @return json
     */
    public function execute()
    {
        try {
            $serviceData = $this->getRequest()->getPost('ping');
            $responseContent = $this->_pingService->send($serviceData);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $responseContent = [
                'success' => false,
                'error_message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $responseContent = [
                'success' => false,
                'error_message' => __('there is some error in ping service.')
            ];
        }
        
        /**
         * @var \Magento\Framework\Controller\Result\Json $resultJson
         */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($responseContent);
        return $resultJson;
    }
   
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
