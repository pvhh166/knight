<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\AdvanceSeoManager\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class Head extends \Magento\Framework\View\Element\Template
{
   
    const SUMMARY_CARD_LARGE = 'summary_large_image';
    const SUMMARY_CARD = 'summary';
    /**
     * product page
     */
    /**
     * product page constant
     */
    const PRODUCT_PAGE = "product";

    /**
     * category page
     */
    /**
     * category page constant
     */
    const CATEGORY_PAGE = "category";

    /**
     * cms page
     */
    /**
     * cms page constant
     */
    const CMS_PAGE = "cms";

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $helper;

    /**
     * jsonld creator
     *
     * @var \Webkul\AdvanceSeoManager\Model\RichSnippet\JsonLdBuilder
     */
    protected $jsonLdBuilder;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var Magento\Catalog\Api\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $_cmsPage;

    /**
     * @var Magento\Catalog\Helper\Image
     */
    protected $_catalogHelper;
    
    public function __construct(
        Context $context,
        \Webkul\AdvanceSeoManager\Helper\Data $helper,
        \Webkul\AdvanceSeoManager\Api\JsonLdBuilderInterface $jsonLdBuilder,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Filesystem\Io\File $file,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Cms\Model\Page $page,
        \Magento\Catalog\Helper\Image $catalogHelper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->jsonLdBuilder = $jsonLdBuilder;
        $this->_productRepository = $productRepository;
        $this->jsonHelper = $jsonHelper;
        $this->registry = $registry;
        $this->file = $file;
        $this->_cms = $page;
        $this->_catalogHelper = $catalogHelper;
        parent::__construct($context, $data);
        $this->_mediaDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * get canonical url for product, category or cms pages
     *
     * @return string|boolean
     */
    public function getConicalUrl()
    {
        if ($this->getCurrentPage() == self::PRODUCT_PAGE) {
            if ($this->helper->getProductConicalUrlStatus()) {
                return $this->getProduct()->getProductUrl();
            }
        }

        if ($this->getCurrentPage() == self::CATEGORY_PAGE) {
            if ($this->helper->getCategoryConicalUrlStatus()) {
                return $this->getProduct()->getCategoryUrl();
                ;
            }
        }

        return false;
    }

    /**
     * get facebook sharing status
     *
     * @return boolean
     */
    public function getFacebookSharingStatus()
    {
        return $this->helper->getFacebookSharingStatus();
    }

    /**
     * get twitter sharing status
     *
     * @return boolean
     */
    public function getTwitterSharingStatus()
    {
        return $this->helper->getTwitterSharingStatus();
    }

    /**
     * get google sharing status
     *
     * @return boolean
     */
    public function getGoogleSharingStatus()
    {
        return false;
    }

     /**
      * get twitter sharing status
      *
      * @return boolean
      */
    public function getSummaryCard()
    {
        $summaryCard = $this->helper->getLargeSummaryCardStatus();
        if ($summaryCard) {
            return self::SUMMARY_CARD_LARGE;
        }

        return self::SUMMARY_CARD;
    }

    /**
     * get product image
     *
     * @return void
     */
    public function getProductThumbnailImage()
    {
        return $this->getImageUrl($this->getProduct(), "product_page_image_medium");
    }

    /**
     * get product image
     *
     * @return void
     */
    public function getProductLargeImage()
    {
        return $this->getImageUrl($this->getProduct());
    }

        /**
         * getImageUrl get Product Image.
         *
         * @param Magento\Catalog\Model\product $_product
         * @param float                         $resize
         * @param string                        $imageType
         * @param bool                          $keepFrame
         *
         * @return string
         */
    public function getImageUrl(
        $_product,
        $imageType = 'product_page_image_large',
        $keepFrame = true,
        $resize = null
    ) {
        return $this->_catalogHelper
            ->init($_product, $imageType)
            ->keepFrame($keepFrame)
            ->resize($resize)
            ->getUrl();
    }

    /**
     * get current page info
     *
     * @return string
     */
    public function getCurrentPage()
    {
        if ($this->registry->registry("current_product")) {
            return self::PRODUCT_PAGE;
        } elseif ($this->registry->registry("current_category")) {
            return self::CATEGORY_PAGE;
        } else {
            return self::CMS_PAGE;
        }
    }

    /**
     * get product data for josn ld creation
     *
     * @return array
     */
    public function getProduct()
    {
        try {
            return $this->registry->registry("current_product");
        } catch (\Exception $e) {
            return ['error'];
        }
    }

    /**
     * get category data for josn ld creation
     *
     * @return array
     */
    public function getCategory()
    {
        try {
            return $this->registry->registry("current_category");
        } catch (\Exception $e) {
            return ['error'];
        }
    }

    /**
     * get twitter card title
     *
     * @return string
     */
    public function getTwitterCardTitle()
    {
        $product = $this->getProduct();
        if ($product->getData("twitter_title")) {
            return $product->getData("twitter_title");
        }
        if ($product->getData("meta_title")) {
            return $product->getData("meta_title");
        }

        return substr(strip_tags($product->getData('name')), 0, 40);
    }

    /**
     * get twitter card description
     *
     * @return string
     */
    public function getTwitterCardDesc()
    {
        $product = $this->getProduct();
        if ($product->getData("twitter_description")) {
            return $product->getData("twitter_description");
        }
        if ($product->getData("meta_description")) {
            return $product->getData("meta_description");
        }

        return substr(strip_tags($product->getData('description')), 0, 300);
    }

    /**
     * get twitter card image
     *
     * @return string
     */
    public function getTwitterCardImage()
    {
        $product = $this->getProduct();
        $twtargetPath = $this->_mediaDirectory->getAbsolutePath('productseo/images/twitter/'.$product->getId().'/');
        $twitterImageName = $product->getData("twitter_image");
        if ($twitterImageName && $this->file->fileExists($twtargetPath.$twitterImageName)) {
            return $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
            'productseo/images/twitter/'.$product->getId().'/'.$twitterImageName;
        } else {
            return $this->getProductLargeImage();
        }
    }

    /**
     * get facebook og image
     *
     * @return string
     */
    public function getFacebookOGImage()
    {
        $product = $this->getProduct();
        $fbtargetPath = $this->_mediaDirectory->getAbsolutePath('productseo/images/facebook/'.$product->getId().'/');
        $facebookImageName = $product->getData("facebook_image");
        if ($facebookImageName && $this->file->fileExists($fbtargetPath.$facebookImageName)) {
            return $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
            'productseo/images/facebook/'.$product->getId().'/'.$facebookImageName;
        } else {
            return $this->getProductLargeImage();
        }
    }

    /**
     * get facebook og description
     *
     * @return string
     */
    public function getFacebookOGDesc()
    {
        $product = $this->getProduct();
        if ($product->getData("facebook_description")) {
            return $product->getData("facebook_description");
        }
        if ($product->getData("meta_description")) {
            return $product->getData("meta_description");
        }

        return substr(strip_tags($product->getData('description')), 0, 300);
    }

    /**
     * get facebook og title
     *
     * @return string
     */
    public function getFacebookOGTitle()
    {
        $product = $this->getProduct();
        if ($product->getData("facebook_title")) {
            return $product->getData("facebook_title");
        }
        if ($product->getData("meta_title")) {
            return $product->getData("meta_title");
        }

        return substr(strip_tags($product->getData('name')), 0, 40);
    }

    /**
     * get currenct language code
     *
     * @return string
     */
    public function getCurrentLanguage()
    {
        $localeCode = $this->helper->getLocaleCode();
        if ($localeCode) {
            $localeCodeArr = explode("_", $localeCode);
            $localeCode = implode("-", $localeCodeArr);
            return strtolower($localeCode);
        } else {
            return 'en-us';
        }
    }

    /**
     * get current page url
     *
     * @return string
     */
    public function getCurrentPageUrl()
    {
        return $this->helper->getCurrentPageUrl();
    }
}
