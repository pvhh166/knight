<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\AdvanceSeoManager\Block\Catalog\Product;

class Sharing extends \Webkul\AdvanceSeoManager\Block\Head
{

    /**
     * get twitter share url
     *
     * @return string
     */
    public function getTweetUrl()
    {
        $url = sprintf("https://twitter.com/share?url=%s", $this->getProduct()->getProductUrl());

        return $url;
    }

    /**
     * get twitter share url
     *
     * @return string
     */
    public function getFbShareUrl()
    {
        $url = sprintf("http://www.facebook.com/share.php?u=%s", $this->getProduct()->getProductUrl());

        return $url;
    }

    /**
     * get google share url
     *
     * @return string
     */
    public function getGoogleShareUrl()
    {
        $url = sprintf("https://plus.google.com/share?url=%s", $this->getProduct()->getProductUrl());

        return $url;
    }
}
