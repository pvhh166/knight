<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml\Cms\Pages;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class DataProvider extends \Magento\Backend\Block\Template
{

    /**
     * @var string
     */
    protected $_template = 'Webkul_AdvanceSeoManager::provider.phtml';

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\jsonLd
     */
    protected $jsonLdHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Magento\Cms\Helper\Page
     */
    protected $cmsHelper;

    /**
     * Constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\AdvanceSeoManager\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\AdvanceSeoManager\Helper\JsonLd $jsonLdHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Cms\Helper\Page $cmsHelper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->jsonLdHelper = $jsonLdHelper;
        $this->jsonHelper = $jsonHelper;
        $this->registry = $registry;
        $this->storeManager = $context->getStoreManager();
        $this->cmsHelper = $cmsHelper;
        parent::__construct($context, $data);
        $this->_mediaDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * get configuration data
     *
     * @return void
     */
    public function getConfigurationData()
    {
        $data = [];
        $page = $this->getPage();
        $frontendStoreId = $this->storeManager->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
       
        $data['type'] = "page";
        $data['mode'] = "new";
        $data['page'] = [];
        $data['richSnippets'] = false;
        
        $data['page']['pageUrl'] = $this->storeManager->getStore($frontendStoreId)->getBaseUrl();
        if ($page && $page->getId()) {
            $data['mode'] = "edit";
            $data['page']['focus_keyword'] = $page->getData("cms_focus_keyword");
            $data['page']['pageUrl'] = $this->cmsHelper->getPageUrl($page->getId());
            $page->setStoreId($frontendStoreId);
        }
        $richSnippets = $this->jsonLdHelper->getCmsRichSnippetData();
        if (count($richSnippets) > 0) {
            $data['richSnippets'] = $richSnippets;
        }
        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * get page
     *
     * @return array
     */
    public function getPage()
    {
        try {
            return $this->registry->registry("cms_page");
        } catch (\Exception $e) {
            return null;
        }
    }
}
