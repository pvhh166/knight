<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml\Catalog\Category;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class DataProvider extends \Magento\Backend\Block\Template
{

    /**
     * @var string
     */
    protected $_template = 'Webkul_AdvanceSeoManager::provider.phtml';

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\jsonLd
     */
    protected $jsonLdHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var Magento\Catalog\Helper\Image
     */
    protected $_catalogHelper;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Magento\Catalog\Model\Category
     */
    protected $categoryFactory;

    /**
     * Constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\AdvanceSeoManager\Helper\Data $helper,
        \Webkul\AdvanceSeoManager\Helper\JsonLd $jsonLdHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Image $catalogHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->jsonLdHelper = $jsonLdHelper;
        $this->_catalogHelper = $catalogHelper;
        $this->jsonHelper = $jsonHelper;
        $this->registry = $registry;
        $this->storeManager = $context->getStoreManager();
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context, $data);
        $this->_mediaDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * get configuration data
     *
     * @return void
     */
    public function getConfigurationData()
    {
        $data = [];
        $category = $this->getCategory();
        $frontendStoreId = $this->storeManager->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
       
        $data['type'] = "category";
        $data['mode'] = "new";
        $data['category'] = [];
        $data['richSnippets'] = false;
        
        $data['category']['categoryUrl'] = $this->storeManager->getStore($frontendStoreId)->getBaseUrl();
        $data['category']['categoryImagesCount'] = 0;
        $data['category']['categoryImage'] = '';
        if ($category && $category->getEntityId()) {
            $data['mode'] = "edit";
            $data['category']['focus_keyword'] = $category->getData("category_focus_keyword");
            $data['category']['categoryUrl'] = $category->setStoreId($frontendStoreId)->getUrl();
            $category->setStoreId($frontendStoreId);
        }
        $richSnippets = $this->jsonLdHelper->getCategoryRichSnippetData();
        if (count($richSnippets) > 0) {
            $data['richSnippets'] = $richSnippets;
        }
        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * get category
     *
     * @return array
     */
    public function getCategory()
    {
        try {
            return $this->registry->registry("current_category");
        } catch (\Exception $e) {
            return null;
        }
    }
}
