<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml\Catalog\Product;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class DataProvider extends \Magento\Backend\Block\Template
{

    /**
     * @var string
     */
    protected $_template = 'Webkul_AdvanceSeoManager::provider.phtml';

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Webkul\AdvanceSeoManager\Helper\jsonLd
     */
    protected $jsonLdHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var Magento\Catalog\Helper\Image
     */
    protected $_catalogHelper;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\AdvanceSeoManager\Helper\Data $helper,
        \Webkul\AdvanceSeoManager\Helper\JsonLd $jsonLdHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Filesystem\Io\File $file,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Image $catalogHelper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->jsonLdHelper = $jsonLdHelper;
        $this->_catalogHelper = $catalogHelper;
        $this->jsonHelper = $jsonHelper;
        $this->registry = $registry;
        $this->file = $file;
        $this->storeManager = $context->getStoreManager();
        parent::__construct($context, $data);
        $this->_mediaDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * get configuration data
     *
     * @return void
     */
    public function getConfigurationData()
    {
        $data = [];
        $product = $this->getProduct();
        $frontendStoreId = $this->storeManager->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
        $twtargetPath = $this->_mediaDirectory->getAbsolutePath('productseo/images/twitter/'.$product->getId().'/');
        $fbtargetPath = $this->_mediaDirectory->getAbsolutePath('productseo/images/facebook/'.$product->getId().'/');
        $data['type'] = "product";
        $data['mode'] = "new";
        $data['product'] = [];
        $data['richSnippets'] = false;
        $data['product']['focus_keyword'] = '';
        $data['product']['facebook_title'] = '';
        $data['product']['facebook_image'] = '';
        $data['product']['facebook_description'] = '';
        $data['product']['twitter_title'] = '';
        $data['product']['twitter_image'] = '';
        $data['product']['twitter_description'] = '';
        $data['baseUrl'] = $this->storeManager->getStore($frontendStoreId)->getBaseUrl();
        $data['product']['productUrl'] = $this->storeManager->getStore($frontendStoreId)->getBaseUrl();
        $data['product']['productImagesCount'] = 0;
        $data['product']['productImage'] = '';
        if ($product && $product->getEntityId()) {
            $data['mode'] = "edit";
            $data['product']['focus_keyword'] = $product->getData("focus_keyword");
            $data['product']['facebook_title'] = $product->getData("facebook_title");
            $data['product']['facebook_description'] = $product->getData("facebook_description");
            $facebookImageName = $product->getData("facebook_image");
            if ($facebookImageName && $this->file->fileExists($fbtargetPath.$facebookImageName)) {
                $data['product']['facebook_image'] = $this->storeManager->getStore($frontendStoreId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
                'productseo/images/facebook/'.$product->getId().'/'.$facebookImageName;
            }
            
            $data['product']['twitter_title'] = $product->getData("twitter_title");
            $data['product']['twitter_description'] = $product->getData("twitter_description");
            $twitterImageName = $product->getData("twitter_image");
            if ($twitterImageName && $this->file->fileExists($twtargetPath.$twitterImageName)) {
                $data['product']['twitter_image'] = $this->storeManager->getStore($frontendStoreId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
                'productseo/images/twitter/'.$product->getId().'/'.$twitterImageName;
            }
            
            $product->setStoreId($frontendStoreId);
            $data['product']['productUrl'] = $product->getProductUrl();
            $images = $product->getMediaGalleryImages();
            $data['product']['productImagesCount'] = count($images);
            if (count($images) > 0) {
                foreach ($images as $image) {
                    if ($image->getMediaType() == 'image') {
                        $data['product']['productImage'] = $image->getUrl();
                    }
                }
            }
        }

        $richSnippets = $this->jsonLdHelper->getProductRichSnippetData();
        if (count($richSnippets) > 0) {
            $data['richSnippets'] = $richSnippets;
        }
        $data['twitter'] = (bool)$this->helper->getTwitterSharingStatus();
        $data['largeSummaryCard'] = (bool)$this->helper->getLargeSummaryCardStatus();

        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * get product data for josn ld creation
     *
     * @return array
     */
    public function getProduct()
    {
        try {
            return $this->registry->registry("current_product");
        } catch (\Exception $e) {
            return null;
        }
    }
}
