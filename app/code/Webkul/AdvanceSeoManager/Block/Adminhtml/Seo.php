<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml;

class Seo extends \Magento\Backend\Block\Template
{

    /**
     * @var string
     */
    protected $_template = 'Webkul_AdvanceSeoManager::catalog/product/seo.phtml';
}
