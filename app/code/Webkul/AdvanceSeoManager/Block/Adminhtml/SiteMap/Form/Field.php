<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml\SiteMap\Form;
 
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
 
class Field extends \Magento\Backend\Block\Template
{

    protected $_template = 'Webkul_AdvanceSeoManager::sitemap/form/field.phtml';

    /**
     * @var \Magento\Sitemap\Model\Sitemap
     */
    protected $sitemapFactory;
    /**
     * Constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Sitemap\Model\SitemapFactory $sitemapFactory,
        array $data = []
    ) {
        $this->sitemapFactroy = $sitemapFactory;
        parent::__construct($context, $data);
    }

    /**
     * get current sitemap type
     *
     * @return string
     */
    public function getSitemapType()
    {
        $stitemapId = $this->_request->getParam("sitemap_id");
        if ($stitemapId) {
            $sitemap = $this->sitemapFactroy->create()->load($stitemapId);
            if ($sitemap->getSitemapId()) {
                return $sitemap->getSitemapType();
            }
        }
        return '';
    }

    /**
     * get sitemap types
     *
     * @return array
     */
    public function getSitemapTypes()
    {
        return [
            'all' => 'All Types',
            'product' => 'Product',
            'category' => 'Category'
        ];
    }
}
