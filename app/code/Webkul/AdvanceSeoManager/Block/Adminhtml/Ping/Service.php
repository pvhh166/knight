<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AdvanceSeoManager
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\AdvanceSeoManager\Block\Adminhtml\Ping;
 
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
 
class Service extends \Magento\Config\Block\System\Config\Form\Field
{
    const SERVICE_TEMPLATE = 'Webkul_AdvanceSeoManager::system/config/ping/service.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Filesystem\Io\File $file,
        \Webkul\AdvanceSeoManager\Helper\Data $helper,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Driver\File $driver,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->file = $file;
        $this->helper = $helper;
        $this->directoryList = $directoryList;
        $this->driver = $driver;
    }

    /**
     * Set template to itself
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::SERVICE_TEMPLATE);
        }
        return $this;
    }

    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxPingUrl()
    {
        return $this->getUrl('seomanager/ping/service'); //hit controller by ajax call on button click.
    }

    /**
     * Return clear ping log ajax url for button
     *
     * @return string
     */
    public function getAjaxClearPingUrl()
    {
        return $this->getUrl('seomanager/ping/Clear'); //hit controller by ajax call on button click.
    }

    /**
     * Get the button and scripts contents
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * get button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setData(
            [
                'id' => 'ping_service_button',
                'label' => __('Ping Service'),
            ]
        );

        return $button->toHtml();
    }

    /**
     * get ping services array
     *
     * @return array
     */
    public function getPingServices()
    {
        return $this->helper->getPingServices();
    }

    public function getPingLog()
    {
        $dir = $this->directoryList;
        $logFile = $dir->getPath(DirectoryList::LOG).'/ping-logger.log';
        if ($this->file->fileExists($logFile)) {
            return trim(
                htmlentities(
                    $this->driver->fileGetContents($logFile)
                ),
                "\t."
            );
        }

        return "";
    }
}
