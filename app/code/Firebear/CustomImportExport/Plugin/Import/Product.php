<?php
/**
 * Product
 *
 * @copyright Copyright © 2019 Firebear Studio. All rights reserved.
 * @author    Firebear Studio <fbeardev@gmail.com>
 */

namespace Firebear\CustomImportExport\Plugin\Import;

use Closure;
use Magento\Catalog\Model\Product\Visibility;

/**
 * Class Product
 * @package Firebear\CustomImportExport\Plugin\Import
 */
class Product
{
    /**
     * @param \Firebear\ImportExport\Model\Import\Product $model
     * @param \Closure $proceed
     * @param $data
     *
     * @return mixed
     */
    public function aroundCustomBunchesData(
        \Firebear\ImportExport\Model\Import\Product $model,
        Closure $proceed,
        $rowData
    ) {
        $tierPrice = [];
        if (isset($rowData['stockist'])) {
            $tierPrice[] = 'Stockist,1,' . $rowData['stockist'] . 'All';
        }
        if (isset($rowData['distributor'])) {
            $tierPrice[] = 'Distributor,1,' . $rowData['distributor'] . 'All';
        }
        if (isset($rowData['supporter'])) {
            $tierPrice[] = 'Supporter,1,' . $rowData['supporter'] . 'All';
        }
		if (isset($rowData['nzfw10'])) {
			$tierPrice[] = 'NZFW10,1,' . $rowData['nzfw10'] . 'All';
		}
		if (isset($rowData['metso'])) {
			$tierPrice[] = 'METSO,1,' . $rowData['metso'] . 'All';
		}
		if (isset($rowData['ropd'])) {
			$tierPrice[] = 'ROPD,1,' . $rowData['ropd'] . 'All';
		}
		if (isset($rowData['old'])) {
			$tierPrice[] = 'OLD,1,' . $rowData['old'] . 'All';
		}
		if (isset($rowData['cgoffice'])) {
			$tierPrice[] = 'CGOffice,1,' . $rowData['cgoffice'] . 'All';
		}
		if (isset($rowData['metalons'])) {
			$tierPrice[] = 'MetalonS,1,' . $rowData['metalons'] . 'All';
		}
		if (isset($rowData['metalond'])) {
			$tierPrice[] = 'MetalonD,1,' . $rowData['metalond'] . 'All';
		}
		if (isset($rowData['of2u'])) {
			$tierPrice[] = 'OF2U,1,' . $rowData['of2u'] . 'All';
		}
		if (isset($rowData['offirst'])) {
			$tierPrice[] = 'OFFirst,1,' . $rowData['offirst'] . 'All';
		}
		if (isset($rowData['richmondopd'])) {
			$tierPrice[] = 'RichmondOPD,1,' . $rowData['richmondopd'] . 'All';
		}
		if (isset($rowData['arcon'])) {
			$tierPrice[] = 'Arcon,1,' . $rowData['arcon'] . 'All';
		}
		if (isset($rowData['uno'])) {
			$tierPrice[] = 'Uno,1,' . $rowData['uno'] . 'All';
		}
		if (isset($rowData['offirstcorp'])) {
			$tierPrice[] = 'OFFirstCorp,1,' . $rowData['offirstcorp'] . 'All';
		}
        $rowData['tier_prices'] = implode('|', $tierPrice);
        return $proceed($rowData);
    }
}
