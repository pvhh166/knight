<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for custom carrier tracker search results.
 * @api
 */
interface TrackerSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get custom carrier tracker list.
     *
     * @return \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface[]
     */
    public function getItems();

    /**
     * Set custom carrier tracker list.
     *
     * @param \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
