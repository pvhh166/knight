<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Custom carrier tracker CRUD interface.
 * @api
 */
interface TrackerRepositoryInterface
{
    /**
     * Save custom carrier tracker.
     *
     * @param  \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface $tracker
     * @return \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\TrackerInterface $tracker);

    /**
     * Retrieve custom carrier tracker.
     *
     * @param int $trackerId
     * @return \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($trackerId);

    /**
     * Retrieve customer carrier trackers matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageVision\CustomCarrierTrackers\Api\Data\TrackerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete custom carrier tracker.
     *
     * @param \MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface $tracker
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\TrackerInterface $tracker);

    /**
     * Delete custom carrier tracker by ID.
     *
     * @param int $trackerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($trackerId);
}
