<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Plugin\Shipping\Helper;

use MageVision\CustomCarrierTrackers\Helper\Config;
use MageVision\CustomCarrierTrackers\Helper\Data as TrackerHelper;
use Magento\Sales\Model\Order\Shipment\Track;

class Data
{
    /**
     * @var Config
     */
    protected $configHelper;
    
    /**
     * @var TrackerHelper
     */
    protected $dataHelper;

    /**
     * @param Config $configHelper
     * @param TrackerHelper $dataHelper
     */
    public function __construct(
        Config $configHelper,
        TrackerHelper $dataHelper
    ) {
        $this->configHelper = $configHelper;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Shipping tracking popup URL getter
     *
     * @param \Magento\Shipping\Helper\Data $subject
     * @param \Closure $proceed
     * @param \Magento\Sales\Model\AbstractModel $model
     * @return string
     */
    public function aroundGetTrackingPopupUrlBySalesModel(
        \Magento\Shipping\Helper\Data $subject,
        \Closure $proceed,
        $model
    ) {
        $result = $proceed($model);
        if ($this->configHelper->isEnabled()) {
            if ($model instanceof Track) {
                if ($trackingUrl = $this->dataHelper->getTrackingUrl(
                    $model->getCarrierCode(),
                    $model->getNumber(),
                    $model->getParentId()
                )) {
                    return $trackingUrl;
                }
            }
        }
        return $result;
    }
}
