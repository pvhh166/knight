<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Plugin\Shipping\Model\Order;

use Magento\Shipping\Model\CarrierFactory;

class Track
{
    /**
     * @var CarrierFactory
     */
    protected $carrierFactory;

    /**
     * @param CarrierFactory $carrierFactory
     */
    public function __construct(
        CarrierFactory $carrierFactory
    ) {
        $this->carrierFactory = $carrierFactory;
    }

    /**
     * Retrieve detail for shipment track
     *
     * @param \Magento\Shipping\Model\Order\Track $subject
     * @param array $result
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function afterGetNumberDetail(\Magento\Shipping\Model\Order\Track $subject, $result)
    {
        $carrierInstance = $this->carrierFactory->create($subject->getCarrierCode());
        if (!$carrierInstance) {
            $result['carrier_code'] = $subject->getCarrierCode();
            return $result;
        }
        return $result;
    }
}
