<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Plugin\Shipping\Block\Adminhtml\Order\Tracking;

use MageVision\CustomCarrierTrackers\Helper\Config;
use MageVision\CustomCarrierTrackers\Model\TrackerFactory;

class View
{
    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var TrackerFactory
     */
    protected $trackerFactory;

    /**
     * @param Config $configHelper
     * @param TrackerFactory $trackerFactory
     */
    public function __construct(
        Config $configHelper,
        TrackerFactory $trackerFactory
    ) {
        $this->configHelper = $configHelper;
        $this->trackerFactory = $trackerFactory;
    }

    /**
     * @param \Magento\Shipping\Block\Adminhtml\Order\Tracking\View $subject
     * @param \Closure $proceed
     * @param string $code
     * @return \Magento\Framework\Phrase|string|bool
     */
    public function aroundGetCarrierTitle(
        \Magento\Shipping\Block\Adminhtml\Order\Tracking\View $subject,
        \Closure $proceed,
        $code
    ) {
        $result = $proceed($code);
        if ($this->configHelper->isEnabled()) {
            $customCarrier = $this->trackerFactory->create()->loadByCarrierCode($code);
            if ($customCarrier->getTrackerId()) {
                return $customCarrier->getCarrierTitle();
            }
        }
        return $result;
    }
}
