<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Plugin\Shipping\Block\Adminhtml\Order;

use MageVision\CustomCarrierTrackers\Helper\Config;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface as TrackerRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use MageVision\CustomCarrierTrackers\Model\Tracker;

class Tracking
{
    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param Config $configHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param TrackerRepository $trackerRepository
     */
    public function __construct(
        Config $configHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        TrackerRepository $trackerRepository
    ) {
        $this->configHelper = $configHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->trackerRepository = $trackerRepository;
    }

    /**
     * Add custom carriers
     *
     * @param \Magento\Shipping\Block\Adminhtml\Order\Tracking $subject
     * @param array $result
     *
     * @return array
     */
    public function afterGetCarriers(\Magento\Shipping\Block\Adminhtml\Order\Tracking $subject, $result)
    {
        if ($this->configHelper->isEnabled()) {
            $customCarrierTrackers = $this->trackerRepository->getList(
                $this->searchCriteriaBuilder->addFilter('is_active', Tracker::STATUS_ENABLED)->create()
            );

            foreach ($customCarrierTrackers->getItems() as $customCarrierTracker) {
                $result[$customCarrierTracker->getCarrierCode()] = $customCarrierTracker->getCarrierTitle();
            }

            $disabledDefaultCarriers = explode(',', $this->configHelper->getDisabledDefaultCarriers());
            foreach ($result as $code => $carrier) {
                if (in_array($code, $disabledDefaultCarriers)) {
                    unset($result[$code]);
                }
            }
        }
        return $result;
    }
}
