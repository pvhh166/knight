<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Block\Adminhtml\Tracker\Edit;

use Magento\Backend\Block\Widget\Context;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var TrackerRepositoryInterface
     */
    protected $trackerRepository;

    /**
     * @param Context $context
     * @param TrackerRepositoryInterface $trackerRepository
     */
    public function __construct(
        Context $context,
        TrackerRepositoryInterface $trackerRepository
    ) {
        $this->context = $context;
        $this->trackerRepository = $trackerRepository;
    }

    /**
     * Return Tracker ID
     *
     * @return int|null
     */
    public function getTrackerId()
    {
        try {
            return $this->trackerRepository->getById(
                $this->context->getRequest()->getParam('tracker_id')
            )->getTrackerId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
