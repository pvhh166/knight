<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Tracker extends Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'MageVision_CustomCarrierTrackers';
        $this->_controller = 'adminhtml_tracker';
        $this->_headerText = __('Manage Custom Carrier Trackers');
        $this->_addButtonLabel = __('Add Carrier Tracker');
        parent::_construct();
    }
}
