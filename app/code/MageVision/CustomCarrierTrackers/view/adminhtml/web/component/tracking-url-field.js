/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */

define([
    'jquery',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/lib/validation/validator'
], function ($, Abstract, validator) {
    'use strict';

    validator.addRule(
        'validate-tracking-url',
        function (value) {
            value = (value || '').replace(/^\s+/, '').replace(/\s+$/, '');
            return value.match(/#TRACKNUM#/) && (/^(http|https):\/\/(([A-Z0-9]([A-Z0-9_-]*[A-Z0-9]|))(\.[A-Z0-9]([A-Z0-9_-]*[A-Z0-9]|))*)(:(\d+))?(\/[A-Z0-9~](([A-Z0-9_~-]|\.)*[A-Z0-9~]|))*\/?(.*)?$/i).test(value);
        },
        $.mage.__('Please enter a valid URL. Protocol (http:// or https://) and #TRACKNUM# variable are required.')
    );

    return Abstract;
});
