<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Helper;

use Magento\Framework\App\Helper\Context;
use MageVision\CustomCarrierTrackers\Helper\Config;
use MageVision\CustomCarrierTrackers\Model\TrackerFactory;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const MODULE_NAME = 'Custom Carrier Trackers';

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var TrackerFactory
     */
    protected $trackerFactory;
    
    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ModuleListInterface
     */
    protected $moduleList;

    /**
     * @param Context $context
     * @param Config $configHelper
     * @param TrackerFactory $trackerFactory
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param ModuleListInterface $moduleList
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        TrackerFactory $trackerFactory,
        ShipmentRepositoryInterface $shipmentRepository,
        OrderRepositoryInterface $orderRepository,
        ModuleListInterface $moduleList
    ) {
        $this->configHelper = $configHelper;
        $this->trackerFactory = $trackerFactory;
        $this->shipmentRepository = $shipmentRepository;
        $this->orderRepository = $orderRepository;
        $this->moduleList = $moduleList;
        parent::__construct($context);
    }

    /**
     * Retrieve tracking url
     *
     * @param  string $carrierCode
     * @param  string  $trackingNumber
     * @param  int $shipId
     * @return string
     */
    public function getTrackingUrl($carrierCode, $trackingNumber, $shipId)
    {
        $trackingUrl = null;
        if ($this->configHelper->isEnabled()) {
            $trackingUrl = $this->_getTrackingUrl($carrierCode, $trackingNumber, $shipId);
        }

        return $trackingUrl;
    }
    
    /**
     * Retrieve tracking url with replaced variables
     *
     * @param  string $carrierCode
     * @param  string  $trackingNumber
     * @param  int  $shipId
     * @return string
     */
    protected function _getTrackingUrl($carrierCode, $trackingNumber, $shipId)
    {
        $trackingUrl = null;
        $customCarrier = $this->trackerFactory->create()->loadByCarrierCode($carrierCode);
        if ($customCarrier->getTrackerId()) {
            $customTrackingUrl = $customCarrier->getTrackingUrl();
            $trackingUrl = str_replace('#TRACKNUM#', $trackingNumber, $customTrackingUrl);
            if ($this->hasTrackingUrlExtraVariables($customTrackingUrl)) {
                $shipment = $this->shipmentRepository->get($shipId);
                if ($shipment) {
                    $orderId = $shipment->getOrderId();
                    $order = $this->orderRepository->get($orderId);
                    $trackingUrl = $this->replaceExtraVariables($trackingUrl, $order);
                }
            }
        }
        return $trackingUrl;
    }
    
    /**
     * Retrieve tracking url with replaced extra variables
     *
     * @param  string $trackingUrl
     * @param  $order \Magento\Sales\Model\Order
     * @return string
     */
    protected function replaceExtraVariables($trackingUrl, $order)
    {
        $address = $order->getShippingAddress();
        if ($address) {
            if (preg_match('#FIRSTNAME#', $trackingUrl)) {
                $trackingUrl = str_replace('#FIRSTNAME#', $address->getFirstname(), $trackingUrl);
            }
            if (preg_match('#LASTNAME#', $trackingUrl)) {
                $trackingUrl = str_replace('#LASTNAME#', $address->getLastname(), $trackingUrl);
            }
            if (preg_match('#COUNTRYCODE#', $trackingUrl)) {
                $trackingUrl = str_replace('#COUNTRYCODE#', $address->getCountryId(), $trackingUrl);
            }
            if (preg_match('#POSTCODE#', $trackingUrl)) {
                $trackingUrl = str_replace('#POSTCODE#', $address->getPostcode(), $trackingUrl);
            }
        }
        return $trackingUrl;
    }
    
    /**
     * Check tracking url for extra variables
     *
     * @param  string $customTrackingUrl
     * @return string
     */
    protected function hasTrackingUrlExtraVariables($customTrackingUrl)
    {
        if (preg_match('#FIRSTNAME#', $customTrackingUrl) || preg_match('#LASTNAME#', $customTrackingUrl)
                || preg_match('#COUNTRYCODE#', $customTrackingUrl) || preg_match('#POSTCODE#', $customTrackingUrl)) {
            return true;
        }
        return false;
    }

    /**
     * Returns extension version.
     *
     * @return string
     */
    public function getExtensionVersion()
    {
        $moduleInfo = $this->moduleList->getOne($this->getModuleName());
        return $moduleInfo['setup_version'];
    }

    /**
     * Returns module's name
     *
     * @return string
     */
    public function getModuleName()
    {
        $classArray = explode('\\', get_class($this));

        return count($classArray) > 2 ? "{$classArray[0]}_{$classArray[1]}" : '';
    }

    /**
     * Returns extension's name
     *
     * @return string
     */
    public function getExtensionName()
    {
        return self::MODULE_NAME;
    }
}
