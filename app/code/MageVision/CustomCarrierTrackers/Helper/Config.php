<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{
    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) $this->scopeConfig->getValue('cct/general/enabled');
    }

    /**
     * @return array
     */
    public function getDisabledDefaultCarriers()
    {
        return $this->scopeConfig->getValue('cct/general/disabled_default_carriers');
    }
}
