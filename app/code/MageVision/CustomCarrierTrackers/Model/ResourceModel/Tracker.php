<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\DataObject;

class Tracker extends AbstractDb
{
    /**
     * DB connection
     *
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magevision_custom_carrier_trackers', 'tracker_id');
        $this->connection = $this->getConnection();
    }

    /**
     * Load carrier tracker from DB by carrier code
     *
     * @param string $carrierCode
     * @return array
     */
    public function loadByCarrierCode($carrierCode)
    {
        $select = $this->connection->select()->from(
            $this->getMainTable()
        )->where(
            'carrier_code = :carrier_code'
        );

        $binds = [':carrier_code' => $carrierCode];

        $result = $this->connection->fetchRow($select, $binds);

        if (!$result) {
            return [];
        }

        return $result;
    }

    /**
     * Check for unique of carrier code
     *
     * @param DataObject $tracker
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsUniqueCarrierCode(DataObject $tracker)
    {
        $select = $this->connection->select()->from(
            ['cct' => $this->getMainTable()]
        )->where(
            'cct.carrier_code = ?',
            $tracker->getData('carrier_code')
        );

        if ($tracker->getTrackerId()) {
            $select->where('cct.tracker_id <> ?', $tracker->getTrackerId());
        }

        if ($this->connection->fetchRow($select)) {
            return false;
        }

        return true;
    }
}
