<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\EntityManager\EntityManager;
use MageVision\CustomCarrierTrackers\Model\TrackerRegistry;
use MageVision\CustomCarrierTrackers\Model\Tracker as TrackerModel;
use MageVision\CustomCarrierTrackers\Model\TrackerFactory;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterfaceFactory;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerSearchResultsInterfaceFactory;

class TrackerRepository implements TrackerRepositoryInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TrackerFactory
     */
    protected $trackerFactory;

    /**
     * @var TrackerSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var TrackerRegistry
     */
    protected $trackerRegistry;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var TrackerInterfaceFactory
     */
    protected $trackerDataFactory;

    /**
     * @param EntityManager $entityManager
     * @param TrackerFactory $trackerFactory
     * @param TrackerInterfaceFactory $trackerDataFactory
     * @param TrackerSearchResultsInterfaceFactory $searchResultsFactory
     * @param TrackerRegistry $trackerRegistry
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     */
    public function __construct(
        EntityManager $entityManager,
        TrackerFactory $trackerFactory,
        TrackerInterfaceFactory $trackerDataFactory,
        TrackerSearchResultsInterfaceFactory $searchResultsFactory,
        TrackerRegistry $trackerRegistry,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor
    ) {
        $this->entityManager = $entityManager;
        $this->trackerFactory = $trackerFactory;
        $this->trackerDataFactory = $trackerDataFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->trackerRegistry = $trackerRegistry;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
    }

    /**
     * Save custom carrier tracker data
     *
     * @param TrackerInterface $tracker
     * @return Tracker
     */
    public function save(TrackerInterface $tracker)
    {
        /** @var TrackerModel $trackerModel */
        $trackerModel = $this->trackerFactory->create();
        if ($trackerId = $tracker->getTrackerId()) {
            $this->entityManager->load($trackerModel, $trackerId);
        }
        $trackerModel->addData(
            $this->dataObjectProcessor->buildOutputDataArray($tracker, TrackerInterface::class)
        );

        $trackerModel->beforeSave();
        $this->entityManager->save($trackerModel);
        $tracker = $this->getTrackerDataObject($trackerModel);
        $this->trackerRegistry->push($tracker);
        return $tracker;
    }

    /**
     * Load custom carrier tracker data by given tracker id Identity
     *
     * @param string $trackerId
     * @return Tracker
     */
    public function getById($trackerId)
    {
        return $this->trackerRegistry->retrieve($trackerId);
    }

    /**
     * Load custom carrier tracker data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \MageVision\CustomCarrierTrackers\Model\ResourceModel\Tracker\Collection
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $collection = $this->trackerFactory->create()->getCollection();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * Delete Custom carrier tracker
     *
     * @param TrackerInterface $tracker
     * @return bool
     */
    public function delete(TrackerInterface $tracker)
    {
        return $this->deleteById($tracker->getTrackerId());
    }

    /**
     * Delete custom carrier tracker by given tracker Id Identity
     *
     * @param string $trackerId
     * @return bool
     */
    public function deleteById($trackerId)
    {
        $tracker = $this->trackerRegistry->retrieve($trackerId);
        $this->entityManager->delete($tracker);
        $this->trackerRegistry->remove($trackerId);
        return true;
    }

    /**
     * Retrieves tracker data object using Tracker Model
     *
     * @param TrackerModel $tracker
     * @return TrackerInterface
     */
    protected function getTrackerDataObject(TrackerModel $tracker)
    {
        /** @var TrackerInterface $trackerDataFactory */
        $trackerDataObject = $this->trackerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $trackerDataObject,
            $tracker->getData(),
            TrackerInterface::class
        );
        $trackerDataObject->setTrackerId($tracker->getTrackerId());

        return $trackerDataObject;
    }
}
