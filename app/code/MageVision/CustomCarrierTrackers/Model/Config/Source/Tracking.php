<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model\Config\Source;

use Magento\Shipping\Model\Config;
use Magento\Framework\Option\ArrayInterface;

class Tracking implements ArrayInterface
{
    /**
     * @var Config
     */
    protected $shippingConfig;

    /**
     * @param Config $shippingConfig
     */
    public function __construct(
        Config $shippingConfig
    ) {
        $this->shippingConfig = $shippingConfig;
    }

    /**
     * Return array of default tracking carriers.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $carriersArray = [];
        $carriers = $this->shippingConfig->getAllCarriers();
        foreach ($carriers as $code => $carrier) {
            if ($carrier->isTrackingAvailable()) {
                $carriersArray[] = ['value' => $code, 'label' => $carrier->getConfigData('title')];
            }
        }
        return $carriersArray;
    }
}
