<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface;
use MageVision\CustomCarrierTrackers\Model\ResourceModel\Tracker as ResourceTracker;

class Tracker extends AbstractModel implements TrackerInterface
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 0;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ResourceTracker|null $resource
     * @param ResourceTracker\Collection|null $resourceCollection
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ResourceTracker $resource = null,
        ResourceTracker\Collection $resourceCollection = null
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(ResourceTracker::class);
    }

    /**
     * Retrieve tracker id
     *
     * @return int
     */
    public function getTrackerId()
    {
        return $this->getData(self::TRACKER_ID);
    }

    /**
     * Retrieve carrier code
     *
     * @return string
     */
    public function getCarrierCode()
    {
        return (string)$this->getData(self::CODE);
    }

    /**
     * Retrieve carrier title
     *
     * @return string
     */
    public function getCarrierTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Retrieve tracking url
     *
     * @return string
     */
    public function getTrackingUrl()
    {
        return $this->getData(self::URL);
    }

    /**
     * Retrieve custom carrier tracker creation time
     *
     * @return string
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Retrieve custom carrier tracker update time
     *
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Get Is active
     *
     * @return bool
     */
    public function getIsActive()
    {
        return (bool)$this->getData(self::IS_ACTIVE);
    }

    /**
     * Set Tracker ID
     *
     * @param int $id
     * @return TrackerInterface
     */
    public function setTrackerId($id)
    {
        return $this->setData(self::TRACKER_ID, $id);
    }

    /**
     * Set carrier code
     *
     * @param string $carrierCode
     * @return TrackerInterface
     */
    public function setCarrierCode($carrierCode)
    {
        return $this->setData(self::CODE, $carrierCode);
    }

    /**
     * Set carrier title
     *
     * @param string $title
     * @return TrackerInterface
     */
    public function setCarrierTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set tracking url
     *
     * @param string $trackingUrl
     * @return TrackerInterface
     */
    public function setTrackingUrl($trackingUrl)
    {
        return $this->setData(self::URL, $trackingUrl);
    }

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return TrackerInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return TrackerInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Set is active
     *
     * @param bool|int $isActive
     * @return TrackerInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Load carrier tracker from DB by carrier code
     *
     * @param string $carrierCode
     * @return $this
     */
    public function loadByCarrierCode($carrierCode)
    {
        $this->addData($this->getResource()->loadByCarrierCode($carrierCode));
        return $this;
    }

    /**
     * Check for unique of carrier code
     *
     * @param DataObject $tracker
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsUniqueCarrierCode(DataObject $tracker)
    {
        return $this->getResource()->getIsUniqueCarrierCode($tracker);
    }

    /**
     * Prepare tracker's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Perform operations before object save
     *
     * @return $this
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $this->setTrackingUrl(trim($this->getTrackingUrl()));
        //create the carrier_code from the carrier_title
        $carrierCode = strtolower(preg_replace('/\s+/', '', $this->getCarrierTitle()));
        $this->setCarrierCode('cct_'.$carrierCode);

        if (!$this->getIsUniqueCarrierCode($this)) {
            throw new LocalizedException(
                __('A custom carrier tracker with the same title already exists.')
            );
        }
        return $this;
    }
}
