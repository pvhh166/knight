<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model\Tracker;

use MageVision\CustomCarrierTrackers\Model\ResourceModel\Tracker\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $trackerCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $trackerCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $trackerCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var $tracker \MageVision\CustomCarrierTrackers\Model\Tracker */
        foreach ($items as $tracker) {
            $this->loadedData[$tracker->getTrackerId()] = $tracker->getData();
        }
        $data = $this->dataPersistor->get('custom_carrier_tracker');
        if (!empty($data)) {
            $tracker = $this->collection->getNewEmptyItem();
            $tracker->setData($data);
            $this->loadedData[$tracker->getTrackerId()] = $tracker->getData();
            $this->dataPersistor->clear('custom_carrier_tracker');
        }

        return $this->loadedData;
    }
}
