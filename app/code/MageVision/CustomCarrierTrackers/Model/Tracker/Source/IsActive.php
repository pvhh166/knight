<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Model\Tracker\Source;

use Magento\Framework\Data\OptionSourceInterface;
use MageVision\CustomCarrierTrackers\Model\Tracker;

class IsActive implements OptionSourceInterface
{
    /**
     * @var Tracker
     */
    protected $tracker;

    /**
     * Constructor
     *
     * @param Tracker $tracker
     */
    public function __construct(
        Tracker $tracker
    ) {
        $this->tracker = $tracker;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->tracker->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
