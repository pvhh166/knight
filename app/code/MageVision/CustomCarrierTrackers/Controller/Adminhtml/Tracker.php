<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface as TrackerRepository;

abstract class Tracker extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MageVision_CustomCarrierTrackers::custom_carrier_trackers';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var TrackerRepository
     */
    protected $trackerRepository;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param TrackerRepository $trackerRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        TrackerRepository $trackerRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->trackerRepository = $trackerRepository;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('MageVision_CustomCarrierTrackers::magevision')
            ->addBreadcrumb(__('Custom Carrier Trackers'), __('Custom Carrier Trackers'))
            ->addBreadcrumb(__('Manage Custom Carrier Trackers'), __('Manage Custom Carrier Trackers'));
        return $resultPage;
    }
}
