<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use MageVision\CustomCarrierTrackers\Model\Tracker;

class MassDisable extends AbstractMassAction
{
    /**
     * Execute action
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction($collection)
    {
        $count = 0;
        foreach ($collection as $tracker) {
            $trackerDataObject = $this->trackerRepository->getById($tracker->getTrackerId());
            $trackerDataObject->setIsActive(Tracker::STATUS_DISABLED);
            $this->trackerRepository->save($trackerDataObject);
            $count++;
        }

        if ($count) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been disabled.', $count));
        }
    }
}
