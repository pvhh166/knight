<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Api\DataObjectHelper;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface as TrackerRepository;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterfaceFactory;
use MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

class Save extends Tracker
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var TrackerInterfaceFactory
     */
    private $trackerDataFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param TrackerRepository $trackerRepository
     * @param DataPersistorInterface $dataPersistor
     * @param DataObjectHelper $dataObjectHelper
     * @param TrackerInterfaceFactory $trackerDataFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        TrackerRepository $trackerRepository,
        DataPersistorInterface $dataPersistor,
        DataObjectHelper $dataObjectHelper,
        TrackerInterfaceFactory $trackerDataFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->trackerDataFactory = $trackerDataFactory;
        parent::__construct($context, $resultPageFactory, $trackerRepository);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('tracker_id');
            if (empty($data['tracker_id'])) {
                $data['tracker_id'] = null;
            }
            try {
                $trackerDataObject = $id
                    ? $this->trackerRepository->getById($id)
                    : $this->trackerDataFactory->create();

                $this->dataObjectHelper->populateWithArray(
                    $trackerDataObject,
                    $data,
                    TrackerInterface::class
                );

                $tracker = $this->trackerRepository->save($trackerDataObject);
                $this->messageManager->addSuccessMessage(__('The carrier tracker has been saved.'));
                $this->dataPersistor->clear('custom_carrier_tracker');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['tracker_id' => $tracker->getTrackerId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the carrier tracker.'));
            }

            $this->dataPersistor->set('custom_carrier_tracker', $data);
            return $resultRedirect->setPath('*/*/edit', ['tracker_id' => $this->getRequest()->getParam('tracker_id')]);
        }
        return $resultRedirect->setPath('*/*/index');
    }
}
