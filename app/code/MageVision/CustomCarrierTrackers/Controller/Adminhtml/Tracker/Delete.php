<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

class Delete extends Tracker
{
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int) $this->getRequest()->getParam('tracker_id');
        if ($id) {
            try {
                $this->trackerRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The carrier tracker has been deleted.'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['tracker_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a carrier tracker to delete.'));
        return $resultRedirect->setPath('*/*/index');
    }
}
