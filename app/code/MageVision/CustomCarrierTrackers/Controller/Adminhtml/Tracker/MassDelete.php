<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

class MassDelete extends AbstractMassAction
{
    /**
     * Execute action
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction($collection)
    {
        $trackersDeleted = 0;
        foreach ($collection->getAllIds() as $trackerId) {
            $this->trackerRepository->deleteById($trackerId);
            $trackersDeleted++;
        }

        if ($trackersDeleted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $trackersDeleted));
        }
    }
}
