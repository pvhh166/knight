<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use Magento\Framework\Exception\NoSuchEntityException;

use MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

class Edit extends Tracker
{
    /**
     * Edit Carrier Tracker
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = (int) $this->getRequest()->getParam('tracker_id');
        if ($id) {
            try {
                $tracker = $this->trackerRepository->getById($id);
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong while editing the custom carrier tracker.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index');
            }
        }
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->setActiveMenu('MageVision_CustomCarrierTrackers::custom_carrier_trackers')
            ->getConfig()->getTitle()->prepend(
                $id ? $tracker->getCarrierTitle() : __('New Carrier Tracker')
            );

        return $resultPage;
    }
}
