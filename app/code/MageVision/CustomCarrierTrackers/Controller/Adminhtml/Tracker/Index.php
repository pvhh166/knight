<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface as TrackerRepository;
use MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

class Index extends Tracker
{
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param TrackerRepository $trackerRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        TrackerRepository $trackerRepository
    ) {
        parent::__construct($context, $resultPageFactory, $trackerRepository);
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->setActiveMenu('MageVision_CustomCarrierTrackers::custom_carrier_trackers')
            ->getConfig()->getTitle()->prepend(__('Manage Custom Carrier Trackers'));

        return $resultPage;
    }
}
