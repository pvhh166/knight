<?php
/**
 * MageVision Custom Carrier Trackers Extension
 *
 * @category     MageVision
 * @package      MageVision_CustomCarrierTrackers
 * @author       MageVision Team
 * @copyright    Copyright (c) 2017 MageVision (http://www.magevision.com)
 * @license      LICENSE_MV.txt or http://www.magevision.com/license-agreement/
 */
namespace MageVision\CustomCarrierTrackers\Controller\Adminhtml\Tracker;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Backend\App\Action;
use MageVision\CustomCarrierTrackers\Api\TrackerRepositoryInterface as TrackerRepository;
use MageVision\CustomCarrierTrackers\Api\Data\TrackerInterface;

class InlineEdit extends Action
{
    /**
     * @var TrackerRepository
     */
    private $trackerRepository;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @param Context $context
     * @param TrackerRepository $trackerRepository
     * @param JsonFactory $jsonFactory
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        Context $context,
        TrackerRepository $trackerRepository,
        JsonFactory $jsonFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        parent::__construct($context);
        $this->trackerRepository = $trackerRepository;
        $this->jsonFactory = $jsonFactory;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $trackerId) {
                    /** @var \MageVision\CustomCarrierTrackers\Model\Tracker $tracker */
                    $tracker = $this->trackerRepository->getById($trackerId);
                    try {
                        $this->dataObjectHelper->populateWithArray(
                            $tracker,
                            $postItems[$trackerId],
                            TrackerInterface::class
                        );

                        $this->trackerRepository->save($tracker);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithTrackerId(
                            $tracker,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add tracker title to error message
     *
     * @param TrackerInterface $tracker
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithTrackerId(TrackerInterface $tracker, $errorText)
    {
        return '[Tracker ID: ' . $tracker->getTrackerId() . '] ' . $errorText;
    }
}
