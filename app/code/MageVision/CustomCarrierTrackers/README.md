# [MageVision](https://www.magevision.com/) Custom Carrier Trackers Extension for Magento 2

## Overview
The Custom Carrier Trackers extension gives you the ability to add new custom tracking carriers. As is well known Magento provides default shipping carriers only for DHL, UPS, USPS and FedEx, which sometimes are not enough. So with this extension you will solve that problem. Add as many new tracking carriers as you need with a fully configurable tracking carrier title and URL. The default Magento tracking methods can be disabled. Give the possibility to your customers to track their shipment directly from their account section and the shipment email. Easy manage and a full overview of all your custom carrier trackers in Magento admin. 

## Key Features
	* Unlimited new completely custom carrier trackers
	* Each carrier tracker is fully configurable. Title, tracking url, enable/disabled
	* The tracking number gets replaced in the tracking URL automatically, just use the variable #TRACKNUM# 
    * Additional extra variables #FIRSTNAME#, #LASTNAME#, #COUNTRYCODE#, #POSTCODE#
	* Customers can track their order directly from the shipment email 
	* Customers can track their order directly from their account section
	* Default Magento shipping carriers can be disabled 
	* Store owners also can track shipments from Magento admin directly
    * Custom carrier trackers quick inline edit in Magento admin grid
    * Custom carrier trackers Magento admin grid for a better overview

	
## Other Features
	* Developed by a Magento Certified Developer
	* Meets Magento standard development practices
    * Single License is valid for 1 live Magento installation and unlimited test Magento installations
	* Simple installation
	* 100% open source

## Compatibility
Magento Community Edition 2.1
	
## Installing the Extension
	* Backup your web directory and store database
	* Download the extension
		1. Sign in to your account
		2. Navigate to menu My Account → My Downloads
		3. Find the extension and click to download it
	* Extract the downloaded ZIP file in a temporary directory
	* Upload the extracted folders and files of the extension to base (root) Magento directory. Do not replace the whole folders, but merge them.  If you have downloaded the extension from Magento Marketplace, then create the following folder path app/code/MageVision/CustomCarrierTrackers and upload there the extracted folders and files.
        * Connect via SSH to your Magento server as, or switch to, the Magento file system owner and run the following commands from the (root) Magento directory:
            1. cd path_to_the_magento_root_directory 
            2. php -f bin/magento module:enable MageVision_CustomCarrierTrackers
            3. php -f bin/magento setup:upgrade
            4. php -f bin/magento setup:di:compile
            5. php -f bin/magento setup:static-content:deploy
        * Log out from Magento admin and log in again
		
## How to Use

Activate the extension and disable default carrier trackers. 
Stores -> Configuration -> MageVision -> Custom Carrier Trackers

Custom Carrier Trackers Grid
MageVision -> Custom Carrier Trackers

Create a new shipment for an order adding one or more custom tracking carriers. Send the tracking information to the customer. In the email the tracking numbers are clickable and you can track directly the shipment. Also in the customer account section under -> My Orders -> Order View -> Order Shipments.

The extension overrides the default Magento templates for New Shippment and Shippment Guest. If you have already customized them, you have to go to the magento admin under Marketing->Email Templates, find those templates and replace the line

{{block class='Magento\\Framework\\View\\Element\\Template' area='frontend' template='Magento_Sales::email/shipment/track.phtml' shipment=$shipment order=$order}}
with
{{block class='Magento\\Framework\\View\\Element\\Template' area='frontend' template='MageVision_CustomCarrierTrackers::email/shipment/track.phtml' shipment=$shipment order=$order}} 

An example tracking URL used for a custom carrier tracker with a replaceable tracking number variable would be: 
https://www.fedex.com/apps/fedextrack/?action=altref&trackingnumber=1111111111&dest_cntry=de&dest_postal=22222

Additional example with extra variables:
https://www.fedex.com/apps/fedextrack/?action=altref&trackingnumber=#TRACKNUM#&dest_cntry=#COUNTRYCODE#&dest_postal=#POSTCODE#

Additional extra variables that can be used #FIRSTNAME#, #LASTNAME#, #COUNTRYCODE# and #POSTCODE#.

## Support
If you need support or have any questions directly related to a [MageVision](https://www.magevision.com/) extension, please contact us at [support@magevision.com](mailto:support@magevision.com)