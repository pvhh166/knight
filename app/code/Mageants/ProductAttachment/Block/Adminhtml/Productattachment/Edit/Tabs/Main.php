<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Block\Adminhtml\Productattachment\Edit\Tabs;

/**
 * ProductAttachment Main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @param \Magento\Backend\Block\Template\Context 
     * @param \Magento\Framework\Registry 
     * @param \Magento\Framework\Data\FormFactory
     * @param array
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) 
    {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */ 
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('productattachment_data');
        $isElementDisabled = false;
        $form = $this->_formFactory->create(
            ['data' => [
                            'id' => 'edit_form', 
                            'enctype' => 'multipart/form-data', 
                            'action' => $this->getData('action'), 
                            'method' => 'POST'
                        ]
            ]
        );
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Attachment Information')]);

        if ($model->getId()) {
            $fieldset->addField('attach_id', 'hidden', ['name' => 'attach_id']);
        }

        $fieldset->addField('product_id', 'hidden', ['name' => 'product_id']);

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'id' => 'title',
                'label' => __('Description'),
                'title' => __('Title'),
                'required' => true,
                'after_element_html'=>'<small>Enter Attachment Short Description.</small>'
            ]
        );
        
        $fieldset->addField(
            'astatus',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'astatus',
                'id' => 'astatus',
                'required' => false,
                'options' => ['Enabled' => __('Enabled'), 'Disabled' => __('Disabled')],
            ]
        );
        
        $fieldset->addField(
            'icon',
            'image',
            [
                'title' => __('image'),
                'label' => __('Thumbnail'),
                'name' => 'icon',
                'note' => 'Allow image type: jpg, jpeg, gif, png, icon.'
            ]
        );
        
        //$req=
        $fieldset->addField(
            'file',
            'file',
            [
                'title' => __('File'),
                'label' => __('Choose File'),
                'name' => 'file',
                'required' => $this->isFileRequired(),
                'after_element_html'=>'<small>'.$model->getData('file').'</small>'
            ]
        );

        $fieldset->addField(
            'sort_order',
            'text',
            [
                'title' => __('Sort Order'),
                'label' => __('Sort Order'),
                'name' => 'sort_order'
            ]
        );

        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function isFileRequired()
    {
        $model = $this->_coreRegistry->registry('productattachment_data');
        if(empty($model->getData('file')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /**
     * Get tab Label
     *
     * @return $string
     */ 
    public function getTabLabel()
    {
        return __('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return $string
     */
    public function getTabTitle()
    {
        return __('Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
