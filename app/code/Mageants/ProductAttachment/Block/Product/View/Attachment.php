<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Block\Product\View;
use Magento\Catalog\Block\Product\AbstractProduct;
/**
 * Attachment class in product view
 */
class Attachment extends AbstractProduct
{
    /**
     * Attach Product
     *
     * @var \Mageants\ProductAttachment\Model\AttachProduct
     */
    protected $_attachProduct;
    /**
     * Product Attachment
     *
     * @var \Mageants\ProductAttachment\Model\ProductAttachment
     */
    protected $_productAttachment;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Mageants\ProductAttachment\Model\AttachProduct $attachProduct
     * @param \Mageants\ProductAttachment\Model\ProductAttachment $productAttachment
     * @param array data[]
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Mageants\ProductAttachment\Model\AttachProduct $attachProduct,
        \Mageants\ProductAttachment\Model\ProductAttachment $productAttachment,
        array $data = [])
    {
        
        $this->_attachProduct=$attachProduct;
        $this->_productAttachment=$productAttachment;
        parent::__construct($context, $data);
        
    }

    /**
     * Check available attachment and return collection
     */
    public function CheckAttachAvailable($id)
    {
        $model = $this->_attachProduct->getCollection()->addFieldToFilter("product_id", $id);
        $attach_id=array();
        foreach($model as $attach)
        {
            $attach_id[]=$attach['attach_id'];
        }
        $collection=$this->_productAttachment->getCollection()
                    ->addFieldToFilter("astatus", "Enabled")
                    ->addFieldToFilter('attach_id', ['in' => $attach_id]);
        $collection->setOrder('sort_order', 'ASC');
        return $collection;
    }
}
