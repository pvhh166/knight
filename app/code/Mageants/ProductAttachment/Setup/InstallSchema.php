<?php 
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Store\Model\StoreManagerInterface;
/**
 * InstallSchema for create Table
 */
class InstallSchema implements InstallSchemaInterface
{
    protected $StoreManager;     
    /**     * Init     *     * @param EavSetupFactory $eavSetupFactory     */    
    public function __construct(StoreManagerInterface $StoreManager)   
    {        
        $this->StoreManager=$StoreManager;    
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create Database Table
         */
        $installer = $setup;
        $installer->startSetup();
        $service_url = 'https://www.mageants.com/index.php/rock/register/live?ext_name=Mageants_ProductAttachment&dom_name='.$this->StoreManager->getStore()->getBaseUrl();
        $curl = curl_init($service_url);     

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_FOLLOWLOCATION =>true,
            CURLOPT_ENCODING=>'',
            CURLOPT_USERAGENT => 'Mozilla/5.0'
        ));
        
        $curl_response = curl_exec($curl);
        curl_close($curl);

        if (!$installer->tableExists('product_attachment')) {
            
        $table = $installer->getConnection()
            ->newTable(
                $installer->getTable('product_attachment')
            )->addColumn(
                'attach_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Attach Id'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable'  => false,],
                'Attach Title'
            )
            ->addColumn(
                'icon',
                Table::TYPE_TEXT,
                255,
                ['nullable'  => true,],
                'Attach Icon'
            )
            ->addColumn(
                'file',
                Table::TYPE_TEXT,
                255,
                ['nullable'  => false,],
                'Attach File'
            )
            ->addColumn(
                'astatus',
                Table::TYPE_TEXT,
                255,
                [],
                'Status'
            )
            ->addColumn(
                'sort_order',
                Table::TYPE_INTEGER,
                25,
                [],
                'Sort Order'
            )
            ->addColumn(
                'product_id',
                Table::TYPE_TEXT,
                255,
                [],
                'product Id'
            )
            ->addIndex(  
                $setup->getIdxName(  
                    $setup->getTable('product_attachment'),  
                    ['title', 'file', 'astatus'],  
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT  
                ),  
                ['title', 'file', 'astatus'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment(
                'Mageants Product Attachment  Table'
            );
        $setup->getConnection()->createTable($table);
        }    
        
        if (!$installer->tableExists('attach_product')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('attach_product'))
                ->addColumn('attach_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true])
                ->addColumn('product_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true], 'Magento Product Id')
                ->addForeignKey(
                    $installer->getFkName(
                        'product_attachment',
                        'attach_id',
                        'attach_product',
                        'attach_id'
                    ),
                    'attach_id',
                    $installer->getTable('product_attachment'),
                    'attach_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'attach_product',
                        'attach_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Mageants Product Attachment relation table');

            $installer->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}
