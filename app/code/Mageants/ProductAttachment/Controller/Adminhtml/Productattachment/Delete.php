<?php
/**
 * @attach Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */

namespace Mageants\ProductAttachment\Controller\Adminhtml\Productattachment;
use Magento\Backend\App\Action\Context;
use Mageants\ProductAttachment\Model\ProductAttachment;
/** 
 * Delete class
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var Contact $attachModel
     */
    protected $_attachModel;

    /**
     * @param Context $context
     * @param attach Model $attach_model
     */
    public function __construct(Context $context,ProductAttachment $attachModel)
    {
        $this->_attachModel = $attachModel;
        parent::__construct($context);
    }
   
    /**
     * Delete Action
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('attach_id');
        try {
                $image = $this->_attachModel->load($id);
                $image->delete();
                $this->messageManager->addSuccess(
                    __('Delete successfully !')
                );
        } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}

