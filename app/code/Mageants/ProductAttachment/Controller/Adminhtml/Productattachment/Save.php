<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Controller\Adminhtml\Productattachment;
use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\MediaStorage\Model\File\UploaderFactory;

/**
 * save Attach Action
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * Upload Factory
     *
     * @var Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * Field Id
     *
     * @var $string='image'
     */
    protected $fileId = 'icon';

    /**
     * Js Helper
     *
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;

    /**
     * Managet Store
     *
     * @var \Mageants\ProductAttachment\Model\ProductAttachment
     */
    protected $_manageAttachment;

    /**
     * @param \Magento\Backend\Block\Template\Context
     * @param \Magento\Backend\Helper\Js
     * @param \Magento\MediaStorage\Model\File\UploaderFactory
     * @param \Magento\Framework\App\Filesystem\DirectoryList
     * @param \Mageants\ProductAttachment\Model\ProductAttachment
     */
    public function __construct(Action\Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        UploaderFactory $uploaderFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Mageants\ProductAttachment\Model\ProductAttachment $manageAttachment)
    {
        $this->_jsHelper = $jsHelper;
        $this->uploaderFactory = $uploaderFactory;
        $this->directory_list = $directory_list;
        $this->_manageAttachment = $manageAttachment;
        parent::__construct($context);
    }


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mageants_ProductAttachment::save');
    }

    /**
     * perform execute method for save Action
     *
     * @return $resultRedirect
     */
    public function execute()
    {
        $data =$this->getRequest()->getPostValue();
        /*
         *
        */
        $resultRedirect = $this->resultRedirectFactory->create();
        if($data)
        {
            $model=$this->_manageAttachment;
            if(isset($data['icon']['delete']))
            {
                $data['icon']="";
            }
            else
            {
                if(isset($_FILES['icon']['name']) && $_FILES['icon']['name'] != '') {
                    $imagename=$this->uploadImage();
                    $data['icon']="Mageants".$imagename;

                }
                else{

                    if(isset($data['icon']))
                    {
                        $data['icon'] = $data['icon']['value'];
                    }
                }
            }

            if(isset($data['file']['delete']))
            {
                $data['file']="";
            }
            else
            {
                if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    $imagename=$this->uploadFile();
                    $data['file']="rockfile".$imagename;
                }
                else{

                    if(isset($data['file']))
                    {
                        $data['file'] = $data['file']['value'];
                    }
                }
            }
            if (isset($data['products']))
            {
                $productIds =str_replace("&", ",", $data['products']);
                $data['product_id']=$productIds;
            }

            $model->setData($data);
            if(isset($data['attach_id']))
            {
                $model->setId($data['attach_id']);
            }

            try {
                $model->save();
                $this->saveProducts($model, $data);

                $this->messageManager->addSuccess(__('You saved this Record.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['attach_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the record.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['attach_id' => $this->getRequest()->getParam('attach_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * upload imege file
     *
     * @return $void
     */
    public function uploadImage()
    {
        $destinationPath = $this->getDestinationPath();

        try {
            $uploader = $this->uploaderFactory->create(['fileId' => 'icon'])
                ->setAllowCreateFolders(true);
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png','icon'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $result=$uploader->save($destinationPath);

              return $result['file'];
                if (!$uploader->save($destinationPath)) {
                throw new LocalizedException(
                    __('File cannot be saved to path: $1', $destinationPath)
                );
                }
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __($e->getMessage())
            );
        }
    }

    /**
     * upload imege file
     *
     * @return $void
     */
    public function uploadFile()
    {
        $destinationPath = $this->getDestinationFilePath();
        try {
            $uploader = $this->uploaderFactory->create(['fileId' => 'file'])
                ->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $result=$uploader->save($destinationPath);

              return $result['file'];
                if (!$uploader->save($destinationPath)) {
                throw new LocalizedException(
                    __('File cannot be saved to path: $1', $destinationPath)
                );
                }
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __($e->getMessage())
            );
        }
    }

    /**
     * Get Destination Path
     *
     * @return $directory_list
     */
    public function getDestinationFilePath()
    {
       return $this->directory_list->getPath('media')."/rockfile/";

    }

    /**
     * Get Destination Path
     *
     * @return $directory_list
     */
    public function getDestinationPath()
    {
       return $this->directory_list->getPath('media')."/Mageants/";

    }

    /**
     * save product for Attachment
     *
     * @return $void
     */
    public function saveProducts($model, $post)
    {
        if (isset($post['products'])) {
            $productIds = $this->_jsHelper->decodeGridSerializedInput($post['products']);
            try {

                $oldProducts = (array) $model->getProducts($model);
                $newProducts = (array) $productIds;

                $this->_resources = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();

                $table = $this->_resources->getTableName(\Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment::TBL_ATT_PRODUCT);
                $insert = array_diff($newProducts, $oldProducts);
                $delete = array_diff($oldProducts, $newProducts);

                if ($delete) {
                    $where = ['attach_id = ?' => (int)$model->getId(), 'product_id IN (?)' => $delete];
                    $connection->delete($table, $where);
                }

                if ($insert) {
                    $data = [];
                    foreach ($insert as $product_id) {
                        $data[] = ['attach_id' => (int)$model->getId(), 'product_id' => (int)$product_id];
                    }
                    $connection->insertMultiple($table, $data);
                }
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Store.'));
            }
        }
    }
}
