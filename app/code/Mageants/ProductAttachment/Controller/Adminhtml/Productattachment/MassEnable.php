<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Controller\Adminhtml\Productattachment;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;

/**
 * MassEnable for Attachment
 */
class MassEnable  extends \Magento\Backend\App\Action
{
    /**
     * Filter
     *
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * collection factory
     *
     * @var Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context 
     * @param \Magento\Ui\Component\MassAction\Filter
     * @param \Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment\CollectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    
    /**
     * perform execute method for massEnable
     *
     * @return $resultRedirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        
        foreach ($collection as $item) {
            $item->setData("astatus", "Enabled");
        }
        $collection->save();
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $collectionSize));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
