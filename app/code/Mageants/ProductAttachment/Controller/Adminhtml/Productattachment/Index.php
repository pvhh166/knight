<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Controller\Adminhtml\Productattachment;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * index Action for Attachment
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * result page Factory
     *
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context 
     * @param Magento\Framework\View\Result\PageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) 
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mageants_ProductAttachment::ProductAttachment');
    }
    
    /**
     * Execute method for Attachment index action
     *
     * @return $resultPage
     */ 
    public function execute()
    {
        /**
         * render The admin grid page
         */            
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mageants_ProductAttachment::productattachment');
        $resultPage->addBreadcrumb(__('Attachment'), __('Attachment'));
        $resultPage->addBreadcrumb(__('Manage Attachment'), __('Manage Attachment'));
        $resultPage->getConfig()->getTitle()->prepend(__('Attachment'));
        return $resultPage;
    }
}
