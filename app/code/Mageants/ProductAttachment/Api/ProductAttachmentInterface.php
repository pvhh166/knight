<?php
/**
 * Copyright © Brandsmith Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Mageants\ProductAttachment\Api;

interface ProductAttachmentInterface
{
    /**
     * Attach product document
     *
     * @param  mixed $manageAttachment
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     * @throws \Magento\Framework\Exception\CouldNotSaveException The specified coupon could not be added.
     */
    public function productAttachment($manageAttachment);
	
	/**
	Data structure for API V1/attachment/productattachment
	{"manageAttachment":
                        {
                           "title": "Attachment Description",
                            "astatus": "Enabled",
                            "sort_order": "0",
                            "file": "rockfile\/d\/o\/doc20181123094346.pdf",
                            "products": ["KG_STU501815_BF_BECS","KG_STU501815_BF_BEAL","KG_STU501815_BF_BESL","KG_STU501815_BF_BEBL","KG_STU501815_BF_BEBU"]
                        }
                    }
	**/
	
}
