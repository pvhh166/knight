<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Model;
use Magento\Framework\Model\AbstractModel;
use Mageants\ProductAttachment\Api\ProductAttachmentInterface;
/**
 * Product Attachment Model
 */
class ProductAttachment extends AbstractModel implements  ProductAttachmentInterface
{

    /**
     * init Attachment
     */
    protected function _construct()
    {
        $this->_init('Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment');
    }

    /**
     * fetch product for Attachment
     *
     * @param \Mageants\ProductAttachment\Model\ProductAttachment  $object
     * @return $string
     */
    public function getProducts(\Mageants\ProductAttachment\Model\ProductAttachment $object)
    {
        $tbl = $this->getResource()->getTable(\Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment::TBL_ATT_PRODUCT);
        $select = $this->getResource()->getConnection()->select()->from(
            $tbl,
            ['product_id']
        )
        ->where(
            'attach_id = ?',
            (int)$object->getId()
        );
        return $this->getResource()->getConnection()->fetchCol($select);
    }

    public function productAttachment($manageAttachment){
        $data = $manageAttachment;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if (isset($data['products']))
        {
            //$skus = explode('&', $data['products']);
            $productIds = [];
            foreach ($data['products'] as $sku) {
                $id = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($sku);
                if($id)
                array_push( $productIds,$id);
            }
            $products = implode("&",$productIds);
            $productIds = implode(",",$productIds);
           // $productIds =str_replace("&", ",", $data['products']);
            $data['products'] = $products;
            $data['product_id']=$productIds;
        }
        $model = $objectManager->create('Mageants\ProductAttachment\Model\ProductAttachment');
        $model->setData($data);
        $model->save();
        $saveController = $objectManager->create('Mageants\ProductAttachment\Controller\Adminhtml\Productattachment\Save');
        $saveController->saveProducts($model, $data);

            return(true);
    }
}
