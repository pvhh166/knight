<?php
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Model;
use Magento\Framework\Model\AbstractModel;
use Mageants\ProductAttachment\Api\Attachproductinterface;

/**
 * AttachProduct Model
 */
class AttachProduct extends AbstractModel
{
    /**
     * init Attach Product
     */
    protected function _construct()
    {
        $this->_init('Mageants\ProductAttachment\Model\ResourceModel\AttachProduct');
    }


}
