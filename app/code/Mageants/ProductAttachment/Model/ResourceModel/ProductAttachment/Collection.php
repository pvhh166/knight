<?php 
/**
 * @category Mageants ProductAttachment
 * @package Mageants_ProductAttachment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */
namespace Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * collection for Product Attachment
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'attach_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Mageants\ProductAttachment\Model\ProductAttachment', 'Mageants\ProductAttachment\Model\ResourceModel\ProductAttachment');
    }
}
